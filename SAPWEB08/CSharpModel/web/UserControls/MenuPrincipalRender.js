function MenuPrincipal($)
{
  

	var renderFn = Mustache.compile('<div class="ui vertical menu">  <div class="item">    <div class="ui input"><input type="text" placeholder="Search..."></div>  </div>  <div class="item">    Programa    <div class="Programa">      <a class="active item">Search</a>      <a class="item">Add</a>      <a class="item">Remove</a>    </div>  </div>  <a class="item">    <i class="{{WWPrograma}}"></i> Browse  </a>  <a class="item">    Messages  </a>  <div class="ui dropdown item">    More    <i class="dropdown icon"></i>    <div class="menu">      <a class="item"><i class="WWPrograma"></i> Programa/a>      <a class="item"><i class="globe icon"></i> Choose Language</a>      <a class="item"><i class="settings icon"></i> Account Settings</a>    </div>  </div></div>');
	var $container;
	this.show = function()
	{
		$container = $(this.getContainerControl());

		// Raise before show scripts

		if (this.IsPostBack)
			this.setHtml(renderFn(this));
		this.renderChildContainers();
		this.toggleVisibility();


		// Raise after show scripts
	}

	this.Scripts = [ 'Semantic-UI/Semantic-UI-master/dist/components/accordion.js' ,'Semantic-UI/Semantic-UI-master/dist/components/accordion.min.js' ,'Semantic-UI/Semantic-UI-master/dist/components/api.js' ,'Semantic-UI/Semantic-UI-master/dist/components/api.min.js' ,'Semantic-UI/Semantic-UI-master/dist/components/checkbox.js' ,'Semantic-UI/Semantic-UI-master/dist/components/checkbox.min.js' ,'Semantic-UI/Semantic-UI-master/dist/components/dimmer.js' ,'Semantic-UI/Semantic-UI-master/dist/components/dimmer.min.js' ,'Semantic-UI/Semantic-UI-master/dist/components/dropdown.js' ,'Semantic-UI/Semantic-UI-master/dist/components/dropdown.min.js' ,'Semantic-UI/Semantic-UI-master/dist/components/embed.js' ,'Semantic-UI/Semantic-UI-master/dist/components/embed.min.js' ,'Semantic-UI/Semantic-UI-master/dist/components/form.js' ,'Semantic-UI/Semantic-UI-master/dist/components/form.min.js' ,'Semantic-UI/Semantic-UI-master/dist/components/modal.js' ,'Semantic-UI/Semantic-UI-master/dist/components/modal.min.js' ,'Semantic-UI/Semantic-UI-master/dist/components/nag.js' ,'Semantic-UI/Semantic-UI-master/dist/components/nag.min.js' ,'Semantic-UI/Semantic-UI-master/dist/components/popup.js' ,'Semantic-UI/Semantic-UI-master/dist/components/popup.min.js' ,'Semantic-UI/Semantic-UI-master/dist/components/progress.js' ,'Semantic-UI/Semantic-UI-master/dist/components/progress.min.js' ,'Semantic-UI/Semantic-UI-master/dist/components/rating.js' ,'Semantic-UI/Semantic-UI-master/dist/components/rating.min.js' ,'Semantic-UI/Semantic-UI-master/dist/components/search.js' ,'Semantic-UI/Semantic-UI-master/dist/components/search.min.js' ,'Semantic-UI/Semantic-UI-master/dist/components/shape.js' ,'Semantic-UI/Semantic-UI-master/dist/components/shape.min.js' ,'Semantic-UI/Semantic-UI-master/dist/components/sidebar.js' ,'Semantic-UI/Semantic-UI-master/dist/components/sidebar.min.js' ,'Semantic-UI/Semantic-UI-master/dist/components/site.js' ,'Semantic-UI/Semantic-UI-master/dist/components/site.min.js' ,'Semantic-UI/Semantic-UI-master/dist/components/sticky.js' ,'Semantic-UI/Semantic-UI-master/dist/components/sticky.min.js' ,'Semantic-UI/Semantic-UI-master/dist/components/tab.js' ,'Semantic-UI/Semantic-UI-master/dist/components/tab.min.js' ,'Semantic-UI/Semantic-UI-master/dist/components/transition.js' ,'Semantic-UI/Semantic-UI-master/dist/components/transition.min.js' ,'Semantic-UI/Semantic-UI-master/dist/components/video.js' ,'Semantic-UI/Semantic-UI-master/dist/components/video.min.js' ,'Semantic-UI/Semantic-UI-master/dist/components/visibility.js' ,'Semantic-UI/Semantic-UI-master/dist/components/visibility.min.js' ,'Semantic-UI/Semantic-UI-master/dist/semantic.js' ,'Semantic-UI/Semantic-UI-master/dist/semantic.min.js' ,'Semantic-UI/Semantic-UI-master/examples/assets/library/iframe-content.js' ,'Semantic-UI/Semantic-UI-master/examples/assets/library/iframe.js' ,'Semantic-UI/Semantic-UI-master/examples/assets/library/jquery.min.js' ,'Semantic-UI/Semantic-UI-master/examples/assets/show-examples.js' ,'Semantic-UI/Semantic-UI-master/gulpfile.js' ,'Semantic-UI/Semantic-UI-master/karma.conf.js' ,'Semantic-UI/Semantic-UI-master/src/definitions/behaviors/api.js' ,'Semantic-UI/Semantic-UI-master/src/definitions/behaviors/form.js' ,'Semantic-UI/Semantic-UI-master/src/definitions/behaviors/visibility.js' ,'Semantic-UI/Semantic-UI-master/src/definitions/globals/site.js' ,'Semantic-UI/Semantic-UI-master/src/definitions/modules/accordion.js' ,'Semantic-UI/Semantic-UI-master/src/definitions/modules/checkbox.js' ,'Semantic-UI/Semantic-UI-master/src/definitions/modules/dimmer.js' ,'Semantic-UI/Semantic-UI-master/src/definitions/modules/dropdown.js' ,'Semantic-UI/Semantic-UI-master/src/definitions/modules/embed.js' ,'Semantic-UI/Semantic-UI-master/src/definitions/modules/modal.js' ,'Semantic-UI/Semantic-UI-master/src/definitions/modules/nag.js' ,'Semantic-UI/Semantic-UI-master/src/definitions/modules/popup.js' ,'Semantic-UI/Semantic-UI-master/src/definitions/modules/progress.js' ,'Semantic-UI/Semantic-UI-master/src/definitions/modules/rating.js' ,'Semantic-UI/Semantic-UI-master/src/definitions/modules/search.js' ,'Semantic-UI/Semantic-UI-master/src/definitions/modules/shape.js' ,'Semantic-UI/Semantic-UI-master/src/definitions/modules/sidebar.js' ,'Semantic-UI/Semantic-UI-master/src/definitions/modules/sticky.js' ,'Semantic-UI/Semantic-UI-master/src/definitions/modules/tab.js' ,'Semantic-UI/Semantic-UI-master/src/definitions/modules/transition.js' ,'Semantic-UI/Semantic-UI-master/tasks/admin/components/create.js' ,'Semantic-UI/Semantic-UI-master/tasks/admin/components/init.js' ,'Semantic-UI/Semantic-UI-master/tasks/admin/components/update.js' ,'Semantic-UI/Semantic-UI-master/tasks/admin/distributions/create.js' ,'Semantic-UI/Semantic-UI-master/tasks/admin/distributions/init.js' ,'Semantic-UI/Semantic-UI-master/tasks/admin/distributions/update.js' ,'Semantic-UI/Semantic-UI-master/tasks/admin/publish.js' ,'Semantic-UI/Semantic-UI-master/tasks/admin/register.js' ,'Semantic-UI/Semantic-UI-master/tasks/admin/release.js' ,'Semantic-UI/Semantic-UI-master/tasks/build.js' ,'Semantic-UI/Semantic-UI-master/tasks/build/assets.js' ,'Semantic-UI/Semantic-UI-master/tasks/build/css.js' ,'Semantic-UI/Semantic-UI-master/tasks/build/javascript.js' ,'Semantic-UI/Semantic-UI-master/tasks/check-install.js' ,'Semantic-UI/Semantic-UI-master/tasks/clean.js' ,'Semantic-UI/Semantic-UI-master/tasks/collections/admin.js' ,'Semantic-UI/Semantic-UI-master/tasks/collections/build.js' ,'Semantic-UI/Semantic-UI-master/tasks/collections/internal.js' ,'Semantic-UI/Semantic-UI-master/tasks/collections/rtl.js' ,'Semantic-UI/Semantic-UI-master/tasks/config/admin/github.js' ,'Semantic-UI/Semantic-UI-master/tasks/config/admin/oauth.example.js' ,'Semantic-UI/Semantic-UI-master/tasks/config/admin/release.js' ,'Semantic-UI/Semantic-UI-master/tasks/config/admin/templates/component-package.js' ,'Semantic-UI/Semantic-UI-master/tasks/config/admin/templates/css-package.js' ,'Semantic-UI/Semantic-UI-master/tasks/config/admin/templates/less-package.js' ,'Semantic-UI/Semantic-UI-master/tasks/config/defaults.js' ,'Semantic-UI/Semantic-UI-master/tasks/config/docs.js' ,'Semantic-UI/Semantic-UI-master/tasks/config/npm/gulpfile.js' ,'Semantic-UI/Semantic-UI-master/tasks/config/project/config.js' ,'Semantic-UI/Semantic-UI-master/tasks/config/project/install.js' ,'Semantic-UI/Semantic-UI-master/tasks/config/project/release.js' ,'Semantic-UI/Semantic-UI-master/tasks/config/tasks.js' ,'Semantic-UI/Semantic-UI-master/tasks/config/user.js' ,'Semantic-UI/Semantic-UI-master/tasks/docs/build.js' ,'Semantic-UI/Semantic-UI-master/tasks/docs/metadata.js' ,'Semantic-UI/Semantic-UI-master/tasks/docs/serve.js' ,'Semantic-UI/Semantic-UI-master/tasks/install.js' ,'Semantic-UI/Semantic-UI-master/tasks/rtl/build.js' ,'Semantic-UI/Semantic-UI-master/tasks/rtl/watch.js' ,'Semantic-UI/Semantic-UI-master/tasks/version.js' ,'Semantic-UI/Semantic-UI-master/tasks/watch.js' ,'Semantic-UI/Semantic-UI-master/test/coverage/PhantomJS 1.9.2 (Linux)/prettify.js' ,'Semantic-UI/Semantic-UI-master/test/helpers/jasmine-clog.js' ,'Semantic-UI/Semantic-UI-master/test/helpers/jasmine-jquery.js' ,'Semantic-UI/Semantic-UI-master/test/helpers/jasmine-sinon.js' ,'Semantic-UI/Semantic-UI-master/test/helpers/jquery-events.js' ,'Semantic-UI/Semantic-UI-master/test/helpers/sinon.js' ,'Semantic-UI/Semantic-UI-master/test/meteor/assets.js' ,'Semantic-UI/Semantic-UI-master/test/meteor/fonts.js' ,'Semantic-UI/Semantic-UI-master/test/modules/accordion.spec.js' ,'Semantic-UI/Semantic-UI-master/test/modules/checkbox.spec.js' ,'Semantic-UI/Semantic-UI-master/test/modules/dropdown.spec.js' ,'Semantic-UI/Semantic-UI-master/test/modules/modal.spec.js' ,'Semantic-UI/Semantic-UI-master/test/modules/module.spec.js' ,'Semantic-UI/Semantic-UI-master/test/modules/popup.spec.js' ,'Semantic-UI/Semantic-UI-master/test/modules/search.spec.js' ,'Semantic-UI/Semantic-UI-master/test/modules/shape.spec.js' ,'Semantic-UI/Semantic-UI-master/test/modules/sidebar.spec.js' ,'Semantic-UI/Semantic-UI-master/test/modules/tab.spec.js' ,'Semantic-UI/Semantic-UI-master/test/modules/transition.spec.js' ,'Semantic-UI/Semantic-UI-master/test/modules/video.spec.js'  ];




	this.toggleVisibility = function () {
		if (this.Visible) {
			$container.show();
		}
		else {
			$container.hide();
		}
	};

	var childContainers = {};
	this.renderChildContainers = function () {
		$container
			.find("[data-slot][data-parent='" + this.ContainerName + "']")
			.each((function (i, slot) {
				var $slot = $(slot),
					slotName = $slot.attr('data-slot'),
					slotContentEl;

				slotContentEl = childContainers[slotName];
				if (!slotContentEl) {				
					slotContentEl = this.getChildContainer(slotName)
					childContainers[slotName] = slotContentEl;
					slotContentEl.parentNode.removeChild(slotContentEl);
				}
				$slot.append(slotContentEl);
				$(slotContentEl).show();
			}).closure(this));
	};

}