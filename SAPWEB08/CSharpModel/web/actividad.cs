/*
               File: Actividad
        Description: Actividad
             Author: GeneXus C# Generator version 16_0_0-127771
       Generated on: 1/5/2019 14:35:15.82
       Program type: Callable routine
          Main DBMS: SQL Server
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Xml.Serialization;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class actividad : GXDataArea, System.Web.SessionState.IRequiresSessionState
   {
      protected void INITENV( )
      {
         if ( GxWebError != 0 )
         {
            return  ;
         }
      }

      protected void INITTRN( )
      {
         initialize_properties( ) ;
         entryPointCalled = false;
         gxfirstwebparm = GetNextPar( );
         gxfirstwebparm_bkp = gxfirstwebparm;
         gxfirstwebparm = DecryptAjaxCall( gxfirstwebparm);
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         if ( StringUtil.StrCmp(gxfirstwebparm, "dyncall") == 0 )
         {
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            dyncall( GetNextPar( )) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxExecAct_"+"gxLoad_8") == 0 )
         {
            A4Indicadorid = (int)(NumberUtil.Val( GetNextPar( ), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A4Indicadorid", StringUtil.LTrim( StringUtil.Str( (decimal)(A4Indicadorid), 9, 0)));
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxLoad_8( A4Indicadorid) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxEvt") == 0 )
         {
            setAjaxEventMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxfirstwebparm = GetNextPar( );
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxfullajaxEvt") == 0 )
         {
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxfirstwebparm = GetNextPar( );
         }
         else
         {
            if ( ! IsValidAjaxCall( false) )
            {
               GxWebError = 1;
               return  ;
            }
            gxfirstwebparm = gxfirstwebparm_bkp;
         }
         if ( ! entryPointCalled && ! ( isAjaxCallMode( ) || isFullAjaxMode( ) ) )
         {
            Gx_mode = gxfirstwebparm;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!")), context));
            if ( StringUtil.StrCmp(gxfirstwebparm, "viewer") != 0 )
            {
               AV7Actividadid = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV7Actividadid", StringUtil.LTrim( StringUtil.Str( (decimal)(AV7Actividadid), 9, 0)));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vACTIVIDADID", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV7Actividadid), "ZZZZZZZZ9"), context));
            }
         }
         if ( context.isSpaRequest( ) )
         {
            enableJsOutput();
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.GetCookie( "GX_SESSION_ID"))) )
         {
            gxcookieaux = context.SetCookie( "GX_SESSION_ID", Encrypt64( Crypto.GetEncryptionKey( ), Crypto.GetServerKey( )), "", (DateTime)(DateTime.MinValue), "", 0);
         }
         GXKey = Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
         toggleJsOutput = isJsOutputEnabled( );
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         init_web_controls( ) ;
         if ( toggleJsOutput )
         {
            if ( context.isSpaRequest( ) )
            {
               enableJsOutput();
            }
         }
         if ( ! context.isSpaRequest( ) )
         {
            Form.Meta.addItem("generator", "GeneXus C# 16_0_0-127771", 0) ;
            Form.Meta.addItem("description", "Actividad", 0) ;
         }
         context.wjLoc = "";
         context.nUserReturn = 0;
         context.wbHandled = 0;
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
         }
         GX_FocusControl = edtActividaddescripcion_Internalname;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         wbErr = false;
         context.SetDefaultTheme("Carmine");
         if ( ! context.IsLocalStorageSupported( ) )
         {
            context.PushCurrentUrl();
         }
      }

      public actividad( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsDataStore1 = context.GetDataStore("DataStore1");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("Carmine");
      }

      public actividad( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsDataStore1 = context.GetDataStore("DataStore1");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( String aP0_Gx_mode ,
                           int aP1_Actividadid )
      {
         this.Gx_mode = aP0_Gx_mode;
         this.AV7Actividadid = aP1_Actividadid;
         executePrivate();
      }

      void executePrivate( )
      {
         isStatic = false;
         webExecute();
      }

      protected override void createObjects( )
      {
      }

      public override void webExecute( )
      {
         if ( initialized == 0 )
         {
            createObjects();
            initialize();
         }
         INITENV( ) ;
         INITTRN( ) ;
         if ( ( GxWebError == 0 ) && ! isAjaxCallMode( ) )
         {
            MasterPageObj = (GXMasterPage) ClassLoader.GetInstance("rwdmasterpage", "GeneXus.Programs.rwdmasterpage", new Object[] {new GxContext( context.handle, context.DataStores, context.HttpContext)});
            MasterPageObj.setDataArea(this,false);
            ValidateSpaRequest();
            MasterPageObj.webExecute();
            if ( ( GxWebError == 0 ) && context.isAjaxRequest( ) )
            {
               enableOutput();
               if ( ! context.isAjaxRequest( ) )
               {
                  context.GX_webresponse.AppendHeader("Cache-Control", "no-store");
               }
               if ( ! context.WillRedirect( ) )
               {
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               }
               else
               {
                  if ( context.isAjaxRequest( ) )
                  {
                     disableOutput();
                  }
                  RenderHtmlHeaders( ) ;
                  context.Redirect( context.wjLoc );
                  context.DispatchAjaxCommands();
               }
            }
         }
         this.cleanup();
      }

      protected void fix_multi_value_controls( )
      {
      }

      protected void Draw( )
      {
         if ( context.isAjaxRequest( ) )
         {
            disableOutput();
         }
         if ( ! GxWebStd.gx_redirect( context) )
         {
            disable_std_buttons( ) ;
            enableDisable( ) ;
            set_caption( ) ;
            /* Form start */
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "Section", "left", "top", " "+"data-gx-base-lib=\"bootstrapv3\""+" "+"data-abstract-form"+" ", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, divMaintable_Internalname, 1, 0, "px", 0, "px", "WWAdvancedContainer", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12 col-sm-8 col-sm-offset-2", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, divTitlecontainer_Internalname, 1, 0, "px", 0, "px", "TableTop", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12", "left", "top", "", "", "div");
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTitle_Internalname, "Actividad", "", "", lblTitle_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "Title", 0, "", 1, 1, 0, "HLP_Actividad.htm");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12", "left", "top", "", "", "div");
            ClassString = "ErrorViewer";
            StyleString = "";
            GxWebStd.gx_msg_list( context, "", context.GX_msglist.DisplayMode, StyleString, ClassString, "", "false");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12 col-sm-8 col-sm-offset-2", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, divFormcontainer_Internalname, 1, 0, "px", 0, "px", "FormContainer", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, divToolbarcell_Internalname, 1, 0, "px", 0, "px", "col-xs-12 col-sm-9 col-sm-offset-3 ToolbarCellClass", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "gx-action-group ActionGroup", "left", "top", " "+"data-gx-actiongroup-type=\"toolbar\""+" ", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "btn-group", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "gx-button", "left", "top", "", "", "div");
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 21,'',false,'',0)\"";
            ClassString = "BtnFirst";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtn_first_Internalname, "", "", bttBtn_first_Jsonclick, 5, "", "", StyleString, ClassString, bttBtn_first_Visible, 0, "standard", "'"+""+"'"+",false,"+"'"+"EFIRST."+"'", TempTags, "", context.GetButtonType( ), "HLP_Actividad.htm");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "gx-button", "left", "top", "", "", "div");
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 23,'',false,'',0)\"";
            ClassString = "BtnPrevious";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtn_previous_Internalname, "", "", bttBtn_previous_Jsonclick, 5, "", "", StyleString, ClassString, bttBtn_previous_Visible, 0, "standard", "'"+""+"'"+",false,"+"'"+"EPREVIOUS."+"'", TempTags, "", context.GetButtonType( ), "HLP_Actividad.htm");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "gx-button", "left", "top", "", "", "div");
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 25,'',false,'',0)\"";
            ClassString = "BtnNext";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtn_next_Internalname, "", "", bttBtn_next_Jsonclick, 5, "", "", StyleString, ClassString, bttBtn_next_Visible, 0, "standard", "'"+""+"'"+",false,"+"'"+"ENEXT."+"'", TempTags, "", context.GetButtonType( ), "HLP_Actividad.htm");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "gx-button", "left", "top", "", "", "div");
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 27,'',false,'',0)\"";
            ClassString = "BtnLast";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtn_last_Internalname, "", "", bttBtn_last_Jsonclick, 5, "", "", StyleString, ClassString, bttBtn_last_Visible, 0, "standard", "'"+""+"'"+",false,"+"'"+"ELAST."+"'", TempTags, "", context.GetButtonType( ), "HLP_Actividad.htm");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "gx-button", "left", "top", "", "", "div");
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 29,'',false,'',0)\"";
            ClassString = "BtnSelect";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtn_select_Internalname, "", "Select", bttBtn_select_Jsonclick, 5, "Select", "", StyleString, ClassString, bttBtn_select_Visible, 0, "standard", "'"+""+"'"+",false,"+"'"+"ESELECT."+"'", TempTags, "", 2, "HLP_Actividad.htm");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12 FormCellAdvanced", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "form-group gx-form-group", "left", "top", ""+" data-gx-for=\""+edtActividaddescripcion_Internalname+"\"", "", "div");
            /* Attribute/Variable Label */
            GxWebStd.gx_label_element( context, edtActividaddescripcion_Internalname, "Descripción", "col-sm-3 AttributeLabel", 1, true);
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-sm-9 gx-attribute", "left", "top", "", "", "div");
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 34,'',false,'',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtActividaddescripcion_Internalname, A48Actividaddescripcion, "", TempTags+" onchange=\""+""+";gx.evt.onchange(this, event)\" "+" onblur=\""+";gx.evt.onblur(this,34);\"", 0, 1, edtActividaddescripcion_Enabled, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "", "300", -1, 0, "", "", -1, true, "", "'"+""+"'"+",false,"+"'"+""+"'", 0, "HLP_Actividad.htm");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12 FormCell", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "form-group gx-form-group", "left", "top", ""+" data-gx-for=\""+edttask_Internalname+"\"", "", "div");
            /* Attribute/Variable Label */
            GxWebStd.gx_label_element( context, edttask_Internalname, "Task", "col-sm-3 AttributeLabel", 1, true);
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-sm-9 gx-attribute", "left", "top", "", "", "div");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 39,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edttask_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A49task), 18, 0, ".", "")), ((edttask_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( (decimal)(A49task), "ZZZZZZZZZZZZZZZZZ9")) : context.localUtil.Format( (decimal)(A49task), "ZZZZZZZZZZZZZZZZZ9")), TempTags+" onchange=\""+"gx.num.valid_integer( this,',');"+";gx.evt.onchange(this, event)\" "+" onblur=\""+";gx.evt.onblur(this,39);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edttask_Jsonclick, 0, "Attribute", "", "", "", "", 1, edttask_Enabled, 0, "number", "1", 18, "chr", 1, "row", 18, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_Actividad.htm");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12 FormCell", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "form-group gx-form-group", "left", "top", ""+" data-gx-for=\""+edtverificacion_Internalname+"\"", "", "div");
            /* Attribute/Variable Label */
            GxWebStd.gx_label_element( context, edtverificacion_Internalname, "Verificación", "col-sm-3 AttributeLabel", 1, true);
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-sm-9 gx-attribute", "left", "top", "", "", "div");
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 44,'',false,'',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtverificacion_Internalname, A50verificacion, "", TempTags+" onchange=\""+""+";gx.evt.onchange(this, event)\" "+" onblur=\""+";gx.evt.onblur(this,44);\"", 0, 1, edtverificacion_Enabled, 0, 80, "chr", 3, "row", StyleString, ClassString, "", "", "200", -1, 0, "", "", -1, true, "", "'"+""+"'"+",false,"+"'"+""+"'", 0, "HLP_Actividad.htm");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12 FormCell", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "form-group gx-form-group", "left", "top", ""+" data-gx-for=\""+edtfrecuencia_Internalname+"\"", "", "div");
            /* Attribute/Variable Label */
            GxWebStd.gx_label_element( context, edtfrecuencia_Internalname, "Frecuencia", "col-sm-3 AttributeLabel", 1, true);
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-sm-9 gx-attribute", "left", "top", "", "", "div");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 49,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtfrecuencia_Internalname, A51frecuencia, StringUtil.RTrim( context.localUtil.Format( A51frecuencia, "")), TempTags+" onchange=\""+""+";gx.evt.onchange(this, event)\" "+" onblur=\""+";gx.evt.onblur(this,49);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtfrecuencia_Jsonclick, 0, "Attribute", "", "", "", "", 1, edtfrecuencia_Enabled, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_Actividad.htm");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12 FormCell", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "form-group gx-form-group", "left", "top", ""+" data-gx-for=\""+edtfuente_Internalname+"\"", "", "div");
            /* Attribute/Variable Label */
            GxWebStd.gx_label_element( context, edtfuente_Internalname, "Fuente", "col-sm-3 AttributeLabel", 1, true);
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-sm-9 gx-attribute", "left", "top", "", "", "div");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 54,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtfuente_Internalname, A52fuente, StringUtil.RTrim( context.localUtil.Format( A52fuente, "")), TempTags+" onchange=\""+""+";gx.evt.onchange(this, event)\" "+" onblur=\""+";gx.evt.onblur(this,54);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtfuente_Jsonclick, 0, "Attribute", "", "", "", "", 1, edtfuente_Enabled, 0, "text", "", 80, "chr", 1, "row", 100, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_Actividad.htm");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12 FormCell", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "form-group gx-form-group", "left", "top", ""+" data-gx-for=\""+edtsupuestos_Internalname+"\"", "", "div");
            /* Attribute/Variable Label */
            GxWebStd.gx_label_element( context, edtsupuestos_Internalname, "Supuestos", "col-sm-3 AttributeLabel", 1, true);
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-sm-9 gx-attribute", "left", "top", "", "", "div");
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 59,'',false,'',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtsupuestos_Internalname, A53supuestos, "", TempTags+" onchange=\""+""+";gx.evt.onchange(this, event)\" "+" onblur=\""+";gx.evt.onblur(this,59);\"", 0, 1, edtsupuestos_Enabled, 0, 80, "chr", 3, "row", StyleString, ClassString, "", "", "200", -1, 0, "", "", -1, true, "", "'"+""+"'"+",false,"+"'"+""+"'", 0, "HLP_Actividad.htm");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12 FormCell", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "form-group gx-form-group", "left", "top", ""+" data-gx-for=\""+edtIndicadorid_Internalname+"\"", "", "div");
            /* Attribute/Variable Label */
            GxWebStd.gx_label_element( context, edtIndicadorid_Internalname, "Indicador", "col-sm-3 AttributeLabel", 1, true);
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-sm-9 gx-attribute", "left", "top", "", "", "div");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 64,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtIndicadorid_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A4Indicadorid), 9, 0, ".", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(A4Indicadorid), "ZZZZZZZZ9")), TempTags+" onchange=\""+"gx.num.valid_integer( this,',');"+";gx.evt.onchange(this, event)\" "+" onblur=\""+";gx.evt.onblur(this,64);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtIndicadorid_Jsonclick, 0, "Attribute", "", "", "", "", 1, edtIndicadorid_Enabled, 1, "number", "1", 9, "chr", 1, "row", 9, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_Actividad.htm");
            /* Static images/pictures */
            ClassString = "gx-prompt Image";
            StyleString = "";
            sImgUrl = (String)(context.GetImagePath( "prompt.gif", "", context.GetTheme( )));
            GxWebStd.gx_bitmap( context, imgprompt_4_Internalname, sImgUrl, imgprompt_4_Link, "", "", context.GetTheme( ), imgprompt_4_Visible, 1, "", "", 0, 0, 0, "", 0, "", 0, 0, 0, "", "", StyleString, ClassString, "", "", "", "", "", "", "", 1, false, false, context.GetImageSrcSet( sImgUrl), "HLP_Actividad.htm");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12 FormCell", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "form-group gx-form-group", "left", "top", ""+" data-gx-for=\""+edtresultado_Internalname+"\"", "", "div");
            /* Attribute/Variable Label */
            GxWebStd.gx_label_element( context, edtresultado_Internalname, "resultado", "col-sm-3 AttributeLabel", 1, true);
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-sm-9 gx-attribute", "left", "top", "", "", "div");
            /* Multiple line edit */
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtresultado_Internalname, A62resultado, "", "", 0, 1, edtresultado_Enabled, 0, 80, "chr", 5, "row", StyleString, ClassString, "", "", "400", -1, 0, "", "", -1, true, "", "'"+""+"'"+",false,"+"'"+""+"'", 0, "HLP_Actividad.htm");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12", "Center", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "gx-action-group Confirm", "left", "top", " "+"data-gx-actiongroup-type=\"toolbar\""+" ", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "gx-button", "left", "top", "", "", "div");
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 74,'',false,'',0)\"";
            ClassString = "BtnEnter";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtn_enter_Internalname, "", "Confirm", bttBtn_enter_Jsonclick, 5, "Confirm", "", StyleString, ClassString, bttBtn_enter_Visible, bttBtn_enter_Enabled, "standard", "'"+""+"'"+",false,"+"'"+"EENTER."+"'", TempTags, "", context.GetButtonType( ), "HLP_Actividad.htm");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "gx-button", "left", "top", "", "", "div");
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 76,'',false,'',0)\"";
            ClassString = "BtnCancel";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtn_cancel_Internalname, "", "Cancel", bttBtn_cancel_Jsonclick, 1, "Cancel", "", StyleString, ClassString, bttBtn_cancel_Visible, 1, "standard", "'"+""+"'"+",false,"+"'"+"ECANCEL."+"'", TempTags, "", context.GetButtonType( ), "HLP_Actividad.htm");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "gx-button", "left", "top", "", "", "div");
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 78,'',false,'',0)\"";
            ClassString = "BtnDelete";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtn_delete_Internalname, "", "Delete", bttBtn_delete_Jsonclick, 5, "Delete", "", StyleString, ClassString, bttBtn_delete_Visible, bttBtn_delete_Enabled, "standard", "'"+""+"'"+",false,"+"'"+"EDELETE."+"'", TempTags, "", context.GetButtonType( ), "HLP_Actividad.htm");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "Center", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            fix_multi_value_controls( ) ;
         }
         /* Execute Exit event if defined. */
      }

      protected void UserMain( )
      {
         standaloneStartup( ) ;
      }

      protected void UserMainFullajax( )
      {
         INITENV( ) ;
         INITTRN( ) ;
         UserMain( ) ;
         Draw( ) ;
         SendCloseFormHiddens( ) ;
      }

      protected void standaloneStartup( )
      {
         standaloneStartupServer( ) ;
         disable_std_buttons( ) ;
         enableDisable( ) ;
         Process( ) ;
      }

      protected void standaloneStartupServer( )
      {
         /* Execute Start event if defined. */
         context.wbGlbDoneStart = 0;
         /* Execute user event: Start */
         E11022 ();
         context.wbGlbDoneStart = 1;
         assign_properties_default( ) ;
         if ( AnyError == 0 )
         {
            if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
            {
               /* Read saved SDTs. */
               /* Read variables values. */
               A48Actividaddescripcion = cgiGet( edtActividaddescripcion_Internalname);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A48Actividaddescripcion", A48Actividaddescripcion);
               if ( ( ( context.localUtil.CToN( cgiGet( edttask_Internalname), ".", ",") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edttask_Internalname), ".", ",") > Convert.ToDecimal( 999999999999999999L )) ) )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "TASK");
                  AnyError = 1;
                  GX_FocusControl = edttask_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  wbErr = true;
                  A49task = 0;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A49task", StringUtil.LTrim( StringUtil.Str( (decimal)(A49task), 18, 0)));
               }
               else
               {
                  A49task = (long)(context.localUtil.CToN( cgiGet( edttask_Internalname), ".", ","));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A49task", StringUtil.LTrim( StringUtil.Str( (decimal)(A49task), 18, 0)));
               }
               A50verificacion = cgiGet( edtverificacion_Internalname);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A50verificacion", A50verificacion);
               A51frecuencia = cgiGet( edtfrecuencia_Internalname);
               n51frecuencia = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A51frecuencia", A51frecuencia);
               n51frecuencia = (String.IsNullOrEmpty(StringUtil.RTrim( A51frecuencia)) ? true : false);
               A52fuente = cgiGet( edtfuente_Internalname);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A52fuente", A52fuente);
               A53supuestos = cgiGet( edtsupuestos_Internalname);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A53supuestos", A53supuestos);
               if ( ( ( context.localUtil.CToN( cgiGet( edtIndicadorid_Internalname), ".", ",") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtIndicadorid_Internalname), ".", ",") > Convert.ToDecimal( 999999999 )) ) )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "INDICADORID");
                  AnyError = 1;
                  GX_FocusControl = edtIndicadorid_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  wbErr = true;
                  A4Indicadorid = 0;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A4Indicadorid", StringUtil.LTrim( StringUtil.Str( (decimal)(A4Indicadorid), 9, 0)));
               }
               else
               {
                  A4Indicadorid = (int)(context.localUtil.CToN( cgiGet( edtIndicadorid_Internalname), ".", ","));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A4Indicadorid", StringUtil.LTrim( StringUtil.Str( (decimal)(A4Indicadorid), 9, 0)));
               }
               A62resultado = cgiGet( edtresultado_Internalname);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A62resultado", A62resultado);
               /* Read saved values. */
               Z3Actividadid = (int)(context.localUtil.CToN( cgiGet( "Z3Actividadid"), ".", ","));
               Z48Actividaddescripcion = cgiGet( "Z48Actividaddescripcion");
               Z49task = (long)(context.localUtil.CToN( cgiGet( "Z49task"), ".", ","));
               Z50verificacion = cgiGet( "Z50verificacion");
               Z51frecuencia = cgiGet( "Z51frecuencia");
               n51frecuencia = (String.IsNullOrEmpty(StringUtil.RTrim( A51frecuencia)) ? true : false);
               Z52fuente = cgiGet( "Z52fuente");
               Z53supuestos = cgiGet( "Z53supuestos");
               Z4Indicadorid = (int)(context.localUtil.CToN( cgiGet( "Z4Indicadorid"), ".", ","));
               IsConfirmed = (short)(context.localUtil.CToN( cgiGet( "IsConfirmed"), ".", ","));
               IsModified = (short)(context.localUtil.CToN( cgiGet( "IsModified"), ".", ","));
               Gx_mode = cgiGet( "Mode");
               N4Indicadorid = (int)(context.localUtil.CToN( cgiGet( "N4Indicadorid"), ".", ","));
               AV7Actividadid = (int)(context.localUtil.CToN( cgiGet( "vACTIVIDADID"), ".", ","));
               A3Actividadid = (int)(context.localUtil.CToN( cgiGet( "ACTIVIDADID"), ".", ","));
               n3Actividadid = false;
               AV11Insert_Indicadorid = (int)(context.localUtil.CToN( cgiGet( "vINSERT_INDICADORID"), ".", ","));
               AV13Pgmname = cgiGet( "vPGMNAME");
               Gx_mode = cgiGet( "vMODE");
               /* Read subfile selected row values. */
               /* Read hidden variables. */
               GXKey = Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
               forbiddenHiddens = "hsh" + "Actividad";
               forbiddenHiddens = forbiddenHiddens + context.localUtil.Format( (decimal)(AV11Insert_Indicadorid), "ZZZZZZZZ9");
               forbiddenHiddens = forbiddenHiddens + StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"));
               forbiddenHiddens = forbiddenHiddens + context.localUtil.Format( (decimal)(A3Actividadid), "ZZZZZZZZ9");
               hsh = cgiGet( "hsh");
               if ( ! GXUtil.CheckEncryptedHash( forbiddenHiddens, hsh, GXKey) )
               {
                  GXUtil.WriteLog("actividad:[SecurityCheckFailed value for]"+"Insert_Indicadorid:"+context.localUtil.Format( (decimal)(AV11Insert_Indicadorid), "ZZZZZZZZ9"));
                  GXUtil.WriteLog("actividad:[SecurityCheckFailed value for]"+"Gx_mode:"+StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!")));
                  GXUtil.WriteLog("actividad:[SecurityCheckFailed value for]"+"Actividadid:"+context.localUtil.Format( (decimal)(A3Actividadid), "ZZZZZZZZ9"));
                  GxWebError = 1;
                  context.HttpContext.Response.StatusDescription = 403.ToString();
                  context.HttpContext.Response.StatusCode = 403;
                  context.WriteHtmlText( "<title>403 Forbidden</title>") ;
                  context.WriteHtmlText( "<h1>403 Forbidden</h1>") ;
                  context.WriteHtmlText( "<p /><hr />") ;
                  GXUtil.WriteLog("send_http_error_code " + 403.ToString());
                  AnyError = 1;
                  return  ;
               }
               standaloneNotModal( ) ;
            }
            else
            {
               standaloneNotModal( ) ;
               if ( StringUtil.StrCmp(gxfirstwebparm, "viewer") == 0 )
               {
                  Gx_mode = "DSP";
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!")), context));
                  A3Actividadid = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  n3Actividadid = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A3Actividadid", StringUtil.LTrim( StringUtil.Str( (decimal)(A3Actividadid), 9, 0)));
                  getEqualNoModal( ) ;
                  Gx_mode = "DSP";
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!")), context));
                  disable_std_buttons( ) ;
                  standaloneModal( ) ;
               }
               else
               {
                  if ( StringUtil.StrCmp(Gx_mode, "DSP") == 0 )
                  {
                     sMode2 = Gx_mode;
                     Gx_mode = "UPD";
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!")), context));
                     Gx_mode = sMode2;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!")), context));
                  }
                  standaloneModal( ) ;
                  if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
                  {
                     getByPrimaryKey( ) ;
                     if ( RcdFound2 == 1 )
                     {
                        if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
                        {
                           /* Confirm record */
                           CONFIRM_020( ) ;
                           if ( AnyError == 0 )
                           {
                              GX_FocusControl = bttBtn_enter_Internalname;
                              context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                           }
                        }
                     }
                     else
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_noinsert", ""), 1, "");
                        AnyError = 1;
                     }
                  }
               }
            }
         }
      }

      protected void Process( )
      {
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            /* Read Transaction buttons. */
            sEvt = cgiGet( "_EventName");
            EvtGridId = cgiGet( "_EventGridId");
            EvtRowId = cgiGet( "_EventRowId");
            if ( StringUtil.Len( sEvt) > 0 )
            {
               sEvtType = StringUtil.Left( sEvt, 1);
               sEvt = StringUtil.Right( sEvt, (short)(StringUtil.Len( sEvt)-1));
               if ( StringUtil.StrCmp(sEvtType, "M") != 0 )
               {
                  if ( StringUtil.StrCmp(sEvtType, "E") == 0 )
                  {
                     sEvtType = StringUtil.Right( sEvt, 1);
                     if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                     {
                        sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                        if ( StringUtil.StrCmp(sEvt, "START") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: Start */
                           E11022 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "AFTER TRN") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: After Trn */
                           E12022 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "ENTER") == 0 )
                        {
                           context.wbHandled = 1;
                           if ( StringUtil.StrCmp(Gx_mode, "DSP") != 0 )
                           {
                              btn_enter( ) ;
                           }
                           /* No code required for Cancel button. It is implemented as the Reset button. */
                        }
                     }
                     else
                     {
                     }
                  }
                  context.wbHandled = 1;
               }
            }
         }
      }

      protected void AfterTrn( )
      {
         if ( trnEnded == 1 )
         {
            /* Execute user event: After Trn */
            E12022 ();
            trnEnded = 0;
            standaloneNotModal( ) ;
            standaloneModal( ) ;
            if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  )
            {
               /* Clear variables for new insertion. */
               InitAll022( ) ;
               standaloneNotModal( ) ;
               standaloneModal( ) ;
            }
         }
      }

      public override String ToString( )
      {
         return "" ;
      }

      public GxContentInfo GetContentInfo( )
      {
         return (GxContentInfo)(null) ;
      }

      protected void disable_std_buttons( )
      {
         bttBtn_delete_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_delete_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_delete_Visible), 5, 0)), true);
         bttBtn_first_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_first_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_first_Visible), 5, 0)), true);
         bttBtn_previous_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_previous_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_previous_Visible), 5, 0)), true);
         bttBtn_next_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_next_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_next_Visible), 5, 0)), true);
         bttBtn_last_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_last_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_last_Visible), 5, 0)), true);
         bttBtn_select_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_select_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_select_Visible), 5, 0)), true);
         if ( ( StringUtil.StrCmp(Gx_mode, "DSP") == 0 ) || ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 ) )
         {
            bttBtn_delete_Visible = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_delete_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_delete_Visible), 5, 0)), true);
            if ( StringUtil.StrCmp(Gx_mode, "DSP") == 0 )
            {
               bttBtn_enter_Visible = 0;
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_enter_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_enter_Visible), 5, 0)), true);
            }
            DisableAttributes022( ) ;
         }
      }

      protected void set_caption( )
      {
         if ( ( IsConfirmed == 1 ) && ( AnyError == 0 ) )
         {
            if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_confdelete", ""), 0, "", true);
            }
            else
            {
               GX_msglist.addItem(context.GetMessage( "GXM_mustconfirm", ""), 0, "", true);
            }
         }
      }

      protected void CONFIRM_020( )
      {
         BeforeValidate022( ) ;
         if ( AnyError == 0 )
         {
            if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
            {
               OnDeleteControls022( ) ;
            }
            else
            {
               CheckExtendedTable022( ) ;
               CloseExtendedTableCursors022( ) ;
            }
         }
         if ( AnyError == 0 )
         {
            IsConfirmed = 1;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "IsConfirmed", StringUtil.LTrim( StringUtil.Str( (decimal)(IsConfirmed), 4, 0)));
         }
      }

      protected void ResetCaption020( )
      {
      }

      protected void E11022( )
      {
         /* Start Routine */
         if ( ! new isauthorized(context).executeUdp(  AV13Pgmname) )
         {
            CallWebObject(formatLink("notauthorized.aspx") + "?" + UrlEncode(StringUtil.RTrim(AV13Pgmname)));
            context.wjLocDisableFrm = 1;
         }
         AV9TrnContext.FromXml(AV10WebSession.Get("TrnContext"), null, "TransactionContext", "SAPWEB08");
         AV11Insert_Indicadorid = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV11Insert_Indicadorid", StringUtil.LTrim( StringUtil.Str( (decimal)(AV11Insert_Indicadorid), 9, 0)));
         if ( ( StringUtil.StrCmp(AV9TrnContext.gxTpr_Transactionname, AV13Pgmname) == 0 ) && ( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) )
         {
            AV14GXV1 = 1;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14GXV1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV14GXV1), 8, 0)));
            while ( AV14GXV1 <= AV9TrnContext.gxTpr_Attributes.Count )
            {
               AV12TrnContextAtt = ((SdtTransactionContext_Attribute)AV9TrnContext.gxTpr_Attributes.Item(AV14GXV1));
               if ( StringUtil.StrCmp(AV12TrnContextAtt.gxTpr_Attributename, "Indicadorid") == 0 )
               {
                  AV11Insert_Indicadorid = (int)(NumberUtil.Val( AV12TrnContextAtt.gxTpr_Attributevalue, "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV11Insert_Indicadorid", StringUtil.LTrim( StringUtil.Str( (decimal)(AV11Insert_Indicadorid), 9, 0)));
               }
               AV14GXV1 = (int)(AV14GXV1+1);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14GXV1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV14GXV1), 8, 0)));
            }
         }
      }

      protected void E12022( )
      {
         /* After Trn Routine */
         if ( ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 ) && ! AV9TrnContext.gxTpr_Callerondelete )
         {
            CallWebObject(formatLink("wwactividad.aspx") );
            context.wjLocDisableFrm = 1;
         }
         context.setWebReturnParms(new Object[] {});
         context.setWebReturnParmsMetadata(new Object[] {});
         context.wjLocDisableFrm = 1;
         context.nUserReturn = 1;
         pr_datastore1.close(1);
         pr_datastore1.close(2);
         returnInSub = true;
         if (true) return;
      }

      protected void ZM022( short GX_JID )
      {
         if ( ( GX_JID == 7 ) || ( GX_JID == 0 ) )
         {
            if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
            {
               Z48Actividaddescripcion = T00023_A48Actividaddescripcion[0];
               Z49task = T00023_A49task[0];
               Z50verificacion = T00023_A50verificacion[0];
               Z51frecuencia = T00023_A51frecuencia[0];
               Z52fuente = T00023_A52fuente[0];
               Z53supuestos = T00023_A53supuestos[0];
               Z4Indicadorid = T00023_A4Indicadorid[0];
            }
            else
            {
               Z48Actividaddescripcion = A48Actividaddescripcion;
               Z49task = A49task;
               Z50verificacion = A50verificacion;
               Z51frecuencia = A51frecuencia;
               Z52fuente = A52fuente;
               Z53supuestos = A53supuestos;
               Z4Indicadorid = A4Indicadorid;
            }
         }
         if ( GX_JID == -7 )
         {
            Z3Actividadid = A3Actividadid;
            Z48Actividaddescripcion = A48Actividaddescripcion;
            Z49task = A49task;
            Z50verificacion = A50verificacion;
            Z51frecuencia = A51frecuencia;
            Z52fuente = A52fuente;
            Z53supuestos = A53supuestos;
            Z4Indicadorid = A4Indicadorid;
            Z62resultado = A62resultado;
         }
      }

      protected void standaloneNotModal( )
      {
         imgprompt_4_Link = ((StringUtil.StrCmp(Gx_mode, "DSP")==0) ? "" : "javascript:"+"gx.popup.openPrompt('"+"gx0090.aspx"+"',["+"{Ctrl:gx.dom.el('"+"INDICADORID"+"'), id:'"+"INDICADORID"+"'"+",IOType:'out'}"+"],"+"null"+","+"'', false"+","+"false"+");");
         bttBtn_delete_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_delete_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_delete_Enabled), 5, 0)), true);
         if ( ! (0==AV7Actividadid) )
         {
            A3Actividadid = AV7Actividadid;
            n3Actividadid = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A3Actividadid", StringUtil.LTrim( StringUtil.Str( (decimal)(A3Actividadid), 9, 0)));
         }
         if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) && ! (0==AV11Insert_Indicadorid) )
         {
            edtIndicadorid_Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtIndicadorid_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtIndicadorid_Enabled), 5, 0)), true);
         }
         else
         {
            edtIndicadorid_Enabled = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtIndicadorid_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtIndicadorid_Enabled), 5, 0)), true);
         }
      }

      protected void standaloneModal( )
      {
         if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) && ! (0==AV11Insert_Indicadorid) )
         {
            A4Indicadorid = AV11Insert_Indicadorid;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A4Indicadorid", StringUtil.LTrim( StringUtil.Str( (decimal)(A4Indicadorid), 9, 0)));
         }
         if ( StringUtil.StrCmp(Gx_mode, "DSP") == 0 )
         {
            bttBtn_enter_Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_enter_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_enter_Enabled), 5, 0)), true);
         }
         else
         {
            bttBtn_enter_Enabled = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_enter_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_enter_Enabled), 5, 0)), true);
         }
         if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) && ( Gx_BScreen == 0 ) )
         {
            AV13Pgmname = "Actividad";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13Pgmname", AV13Pgmname);
            /* Using cursor T00024 */
            pr_datastore1.execute(2, new Object[] {A4Indicadorid});
            A62resultado = T00024_A62resultado[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A62resultado", A62resultado);
            pr_datastore1.close(2);
         }
      }

      protected void Load022( )
      {
         /* Using cursor T00025 */
         pr_datastore1.execute(3, new Object[] {n3Actividadid, A3Actividadid});
         if ( (pr_datastore1.getStatus(3) != 101) )
         {
            RcdFound2 = 1;
            A48Actividaddescripcion = T00025_A48Actividaddescripcion[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A48Actividaddescripcion", A48Actividaddescripcion);
            A49task = T00025_A49task[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A49task", StringUtil.LTrim( StringUtil.Str( (decimal)(A49task), 18, 0)));
            A50verificacion = T00025_A50verificacion[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A50verificacion", A50verificacion);
            A51frecuencia = T00025_A51frecuencia[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A51frecuencia", A51frecuencia);
            n51frecuencia = T00025_n51frecuencia[0];
            A52fuente = T00025_A52fuente[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A52fuente", A52fuente);
            A53supuestos = T00025_A53supuestos[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A53supuestos", A53supuestos);
            A62resultado = T00025_A62resultado[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A62resultado", A62resultado);
            A4Indicadorid = T00025_A4Indicadorid[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A4Indicadorid", StringUtil.LTrim( StringUtil.Str( (decimal)(A4Indicadorid), 9, 0)));
            ZM022( -7) ;
         }
         pr_datastore1.close(3);
         OnLoadActions022( ) ;
      }

      protected void OnLoadActions022( )
      {
         AV13Pgmname = "Actividad";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13Pgmname", AV13Pgmname);
      }

      protected void CheckExtendedTable022( )
      {
         Gx_BScreen = 1;
         standaloneModal( ) ;
         AV13Pgmname = "Actividad";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13Pgmname", AV13Pgmname);
         /* Using cursor T00024 */
         pr_datastore1.execute(2, new Object[] {A4Indicadorid});
         if ( (pr_datastore1.getStatus(2) == 101) )
         {
            GX_msglist.addItem("No matching 'Indicador'.", "ForeignKeyNotFound", 1, "INDICADORID");
            AnyError = 1;
            GX_FocusControl = edtIndicadorid_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         A62resultado = T00024_A62resultado[0];
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A62resultado", A62resultado);
         pr_datastore1.close(2);
      }

      protected void CloseExtendedTableCursors022( )
      {
         pr_datastore1.close(2);
      }

      protected void enableDisable( )
      {
      }

      protected void gxLoad_8( int A4Indicadorid )
      {
         /* Using cursor T00026 */
         pr_datastore1.execute(4, new Object[] {A4Indicadorid});
         if ( (pr_datastore1.getStatus(4) == 101) )
         {
            GX_msglist.addItem("No matching 'Indicador'.", "ForeignKeyNotFound", 1, "INDICADORID");
            AnyError = 1;
            GX_FocusControl = edtIndicadorid_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         A62resultado = T00026_A62resultado[0];
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A62resultado", A62resultado);
         GxWebStd.set_html_headers( context, 0, "", "");
         context.GX_webresponse.AddString("[[");
         context.GX_webresponse.AddString("\""+GXUtil.EncodeJSConstant( A62resultado)+"\"");
         context.GX_webresponse.AddString("]");
         if ( (pr_datastore1.getStatus(4) == 101) )
         {
            context.GX_webresponse.AddString(",");
            context.GX_webresponse.AddString("101");
         }
         context.GX_webresponse.AddString("]");
         pr_datastore1.close(4);
      }

      protected void GetKey022( )
      {
         /* Using cursor T00027 */
         pr_datastore1.execute(5, new Object[] {n3Actividadid, A3Actividadid});
         if ( (pr_datastore1.getStatus(5) != 101) )
         {
            RcdFound2 = 1;
         }
         else
         {
            RcdFound2 = 0;
         }
         pr_datastore1.close(5);
      }

      protected void getByPrimaryKey( )
      {
         /* Using cursor T00023 */
         pr_datastore1.execute(1, new Object[] {n3Actividadid, A3Actividadid});
         if ( (pr_datastore1.getStatus(1) != 101) )
         {
            ZM022( 7) ;
            RcdFound2 = 1;
            A3Actividadid = T00023_A3Actividadid[0];
            n3Actividadid = T00023_n3Actividadid[0];
            A48Actividaddescripcion = T00023_A48Actividaddescripcion[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A48Actividaddescripcion", A48Actividaddescripcion);
            A49task = T00023_A49task[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A49task", StringUtil.LTrim( StringUtil.Str( (decimal)(A49task), 18, 0)));
            A50verificacion = T00023_A50verificacion[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A50verificacion", A50verificacion);
            A51frecuencia = T00023_A51frecuencia[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A51frecuencia", A51frecuencia);
            n51frecuencia = T00023_n51frecuencia[0];
            A52fuente = T00023_A52fuente[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A52fuente", A52fuente);
            A53supuestos = T00023_A53supuestos[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A53supuestos", A53supuestos);
            A4Indicadorid = T00023_A4Indicadorid[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A4Indicadorid", StringUtil.LTrim( StringUtil.Str( (decimal)(A4Indicadorid), 9, 0)));
            Z3Actividadid = A3Actividadid;
            sMode2 = Gx_mode;
            Gx_mode = "DSP";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!")), context));
            Load022( ) ;
            if ( AnyError == 1 )
            {
               RcdFound2 = 0;
               InitializeNonKey022( ) ;
            }
            Gx_mode = sMode2;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!")), context));
         }
         else
         {
            RcdFound2 = 0;
            InitializeNonKey022( ) ;
            sMode2 = Gx_mode;
            Gx_mode = "DSP";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!")), context));
            standaloneModal( ) ;
            Gx_mode = sMode2;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!")), context));
         }
         pr_datastore1.close(1);
      }

      protected void getEqualNoModal( )
      {
         GetKey022( ) ;
         if ( RcdFound2 == 0 )
         {
         }
         else
         {
         }
         getByPrimaryKey( ) ;
      }

      protected void move_next( )
      {
         RcdFound2 = 0;
         /* Using cursor T00028 */
         pr_datastore1.execute(6, new Object[] {n3Actividadid, A3Actividadid});
         if ( (pr_datastore1.getStatus(6) != 101) )
         {
            while ( (pr_datastore1.getStatus(6) != 101) && ( ( T00028_A3Actividadid[0] < A3Actividadid ) ) )
            {
               pr_datastore1.readNext(6);
            }
            if ( (pr_datastore1.getStatus(6) != 101) && ( ( T00028_A3Actividadid[0] > A3Actividadid ) ) )
            {
               A3Actividadid = T00028_A3Actividadid[0];
               n3Actividadid = T00028_n3Actividadid[0];
               RcdFound2 = 1;
            }
         }
         pr_datastore1.close(6);
      }

      protected void move_previous( )
      {
         RcdFound2 = 0;
         /* Using cursor T00029 */
         pr_datastore1.execute(7, new Object[] {n3Actividadid, A3Actividadid});
         if ( (pr_datastore1.getStatus(7) != 101) )
         {
            while ( (pr_datastore1.getStatus(7) != 101) && ( ( T00029_A3Actividadid[0] > A3Actividadid ) ) )
            {
               pr_datastore1.readNext(7);
            }
            if ( (pr_datastore1.getStatus(7) != 101) && ( ( T00029_A3Actividadid[0] < A3Actividadid ) ) )
            {
               A3Actividadid = T00029_A3Actividadid[0];
               n3Actividadid = T00029_n3Actividadid[0];
               RcdFound2 = 1;
            }
         }
         pr_datastore1.close(7);
      }

      protected void btn_enter( )
      {
         nKeyPressed = 1;
         GetKey022( ) ;
         if ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )
         {
            /* Insert record */
            GX_FocusControl = edtActividaddescripcion_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            Insert022( ) ;
            if ( AnyError == 1 )
            {
               GX_FocusControl = "";
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            }
         }
         else
         {
            if ( RcdFound2 == 1 )
            {
               if ( A3Actividadid != Z3Actividadid )
               {
                  A3Actividadid = Z3Actividadid;
                  n3Actividadid = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A3Actividadid", StringUtil.LTrim( StringUtil.Str( (decimal)(A3Actividadid), 9, 0)));
                  GX_msglist.addItem(context.GetMessage( "GXM_getbeforeupd", ""), "CandidateKeyNotFound", 1, "");
                  AnyError = 1;
               }
               else if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
               {
                  delete( ) ;
                  AfterTrn( ) ;
                  GX_FocusControl = edtActividaddescripcion_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               }
               else
               {
                  /* Update record */
                  Update022( ) ;
                  GX_FocusControl = edtActividaddescripcion_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               }
            }
            else
            {
               if ( A3Actividadid != Z3Actividadid )
               {
                  /* Insert record */
                  GX_FocusControl = edtActividaddescripcion_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  Insert022( ) ;
                  if ( AnyError == 1 )
                  {
                     GX_FocusControl = "";
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  }
               }
               else
               {
                  if ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 )
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_recdeleted", ""), 1, "");
                     AnyError = 1;
                  }
                  else
                  {
                     /* Insert record */
                     GX_FocusControl = edtActividaddescripcion_Internalname;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                     Insert022( ) ;
                     if ( AnyError == 1 )
                     {
                        GX_FocusControl = "";
                        context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                     }
                  }
               }
            }
         }
         AfterTrn( ) ;
         if ( ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 ) || ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 ) )
         {
            if ( AnyError == 0 )
            {
               context.nUserReturn = 1;
            }
         }
      }

      protected void btn_delete( )
      {
         if ( A3Actividadid != Z3Actividadid )
         {
            A3Actividadid = Z3Actividadid;
            n3Actividadid = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A3Actividadid", StringUtil.LTrim( StringUtil.Str( (decimal)(A3Actividadid), 9, 0)));
            GX_msglist.addItem(context.GetMessage( "GXM_getbeforedlt", ""), 1, "");
            AnyError = 1;
         }
         else
         {
            delete( ) ;
            AfterTrn( ) ;
            GX_FocusControl = edtActividaddescripcion_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         if ( AnyError != 0 )
         {
         }
      }

      protected void CheckOptimisticConcurrency022( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
         {
            /* Using cursor T00022 */
            pr_datastore1.execute(0, new Object[] {n3Actividadid, A3Actividadid});
            if ( (pr_datastore1.getStatus(0) == 103) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_lock", new   object[]  {"ACTIVIDAD"}), "RecordIsLocked", 1, "");
               AnyError = 1;
               return  ;
            }
            Gx_longc = false;
            if ( (pr_datastore1.getStatus(0) == 101) || ( StringUtil.StrCmp(Z48Actividaddescripcion, T00022_A48Actividaddescripcion[0]) != 0 ) || ( Z49task != T00022_A49task[0] ) || ( StringUtil.StrCmp(Z50verificacion, T00022_A50verificacion[0]) != 0 ) || ( StringUtil.StrCmp(Z51frecuencia, T00022_A51frecuencia[0]) != 0 ) || ( StringUtil.StrCmp(Z52fuente, T00022_A52fuente[0]) != 0 ) )
            {
               Gx_longc = true;
            }
            if ( Gx_longc || ( StringUtil.StrCmp(Z53supuestos, T00022_A53supuestos[0]) != 0 ) || ( Z4Indicadorid != T00022_A4Indicadorid[0] ) )
            {
               if ( StringUtil.StrCmp(Z48Actividaddescripcion, T00022_A48Actividaddescripcion[0]) != 0 )
               {
                  GXUtil.WriteLog("actividad:[seudo value changed for attri]"+"Actividaddescripcion");
                  GXUtil.WriteLogRaw("Old: ",Z48Actividaddescripcion);
                  GXUtil.WriteLogRaw("Current: ",T00022_A48Actividaddescripcion[0]);
               }
               if ( Z49task != T00022_A49task[0] )
               {
                  GXUtil.WriteLog("actividad:[seudo value changed for attri]"+"task");
                  GXUtil.WriteLogRaw("Old: ",Z49task);
                  GXUtil.WriteLogRaw("Current: ",T00022_A49task[0]);
               }
               if ( StringUtil.StrCmp(Z50verificacion, T00022_A50verificacion[0]) != 0 )
               {
                  GXUtil.WriteLog("actividad:[seudo value changed for attri]"+"verificacion");
                  GXUtil.WriteLogRaw("Old: ",Z50verificacion);
                  GXUtil.WriteLogRaw("Current: ",T00022_A50verificacion[0]);
               }
               if ( StringUtil.StrCmp(Z51frecuencia, T00022_A51frecuencia[0]) != 0 )
               {
                  GXUtil.WriteLog("actividad:[seudo value changed for attri]"+"frecuencia");
                  GXUtil.WriteLogRaw("Old: ",Z51frecuencia);
                  GXUtil.WriteLogRaw("Current: ",T00022_A51frecuencia[0]);
               }
               if ( StringUtil.StrCmp(Z52fuente, T00022_A52fuente[0]) != 0 )
               {
                  GXUtil.WriteLog("actividad:[seudo value changed for attri]"+"fuente");
                  GXUtil.WriteLogRaw("Old: ",Z52fuente);
                  GXUtil.WriteLogRaw("Current: ",T00022_A52fuente[0]);
               }
               if ( StringUtil.StrCmp(Z53supuestos, T00022_A53supuestos[0]) != 0 )
               {
                  GXUtil.WriteLog("actividad:[seudo value changed for attri]"+"supuestos");
                  GXUtil.WriteLogRaw("Old: ",Z53supuestos);
                  GXUtil.WriteLogRaw("Current: ",T00022_A53supuestos[0]);
               }
               if ( Z4Indicadorid != T00022_A4Indicadorid[0] )
               {
                  GXUtil.WriteLog("actividad:[seudo value changed for attri]"+"Indicadorid");
                  GXUtil.WriteLogRaw("Old: ",Z4Indicadorid);
                  GXUtil.WriteLogRaw("Current: ",T00022_A4Indicadorid[0]);
               }
               GX_msglist.addItem(context.GetMessage( "GXM_waschg", new   object[]  {"ACTIVIDAD"}), "RecordWasChanged", 1, "");
               AnyError = 1;
               return  ;
            }
         }
      }

      protected void Insert022( )
      {
         BeforeValidate022( ) ;
         if ( AnyError == 0 )
         {
            CheckExtendedTable022( ) ;
         }
         if ( AnyError == 0 )
         {
            ZM022( 0) ;
            CheckOptimisticConcurrency022( ) ;
            if ( AnyError == 0 )
            {
               AfterConfirm022( ) ;
               if ( AnyError == 0 )
               {
                  BeforeInsert022( ) ;
                  if ( AnyError == 0 )
                  {
                     /* Using cursor T000210 */
                     pr_datastore1.execute(8, new Object[] {A48Actividaddescripcion, A49task, A50verificacion, n51frecuencia, A51frecuencia, A52fuente, A53supuestos, A4Indicadorid});
                     A3Actividadid = T000210_A3Actividadid[0];
                     n3Actividadid = T000210_n3Actividadid[0];
                     pr_datastore1.close(8);
                     dsDataStore1.SmartCacheProvider.SetUpdated("ACTIVIDAD") ;
                     if ( AnyError == 0 )
                     {
                        /* Start of After( Insert) rules */
                        /* End of After( Insert) rules */
                        if ( AnyError == 0 )
                        {
                           /* Save values for previous() function. */
                           GX_msglist.addItem(context.GetMessage( "GXM_sucadded", ""), "SuccessfullyAdded", 0, "", true);
                           ResetCaption020( ) ;
                        }
                     }
                  }
                  else
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                     AnyError = 1;
                  }
               }
            }
            else
            {
               Load022( ) ;
            }
            EndLevel022( ) ;
         }
         CloseExtendedTableCursors022( ) ;
      }

      protected void Update022( )
      {
         BeforeValidate022( ) ;
         if ( AnyError == 0 )
         {
            CheckExtendedTable022( ) ;
         }
         if ( AnyError == 0 )
         {
            CheckOptimisticConcurrency022( ) ;
            if ( AnyError == 0 )
            {
               AfterConfirm022( ) ;
               if ( AnyError == 0 )
               {
                  BeforeUpdate022( ) ;
                  if ( AnyError == 0 )
                  {
                     /* Using cursor T000211 */
                     pr_datastore1.execute(9, new Object[] {A48Actividaddescripcion, A49task, A50verificacion, n51frecuencia, A51frecuencia, A52fuente, A53supuestos, A4Indicadorid, n3Actividadid, A3Actividadid});
                     pr_datastore1.close(9);
                     dsDataStore1.SmartCacheProvider.SetUpdated("ACTIVIDAD") ;
                     if ( (pr_datastore1.getStatus(9) == 103) )
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_lock", new   object[]  {"ACTIVIDAD"}), "RecordIsLocked", 1, "");
                        AnyError = 1;
                     }
                     DeferredUpdate022( ) ;
                     if ( AnyError == 0 )
                     {
                        /* Start of After( update) rules */
                        /* End of After( update) rules */
                        if ( AnyError == 0 )
                        {
                           if ( ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 ) || ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 ) )
                           {
                              if ( AnyError == 0 )
                              {
                                 context.nUserReturn = 1;
                              }
                           }
                        }
                     }
                     else
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                        AnyError = 1;
                     }
                  }
               }
            }
            EndLevel022( ) ;
         }
         CloseExtendedTableCursors022( ) ;
      }

      protected void DeferredUpdate022( )
      {
      }

      protected void delete( )
      {
         BeforeValidate022( ) ;
         if ( AnyError == 0 )
         {
            CheckOptimisticConcurrency022( ) ;
         }
         if ( AnyError == 0 )
         {
            OnDeleteControls022( ) ;
            AfterConfirm022( ) ;
            if ( AnyError == 0 )
            {
               BeforeDelete022( ) ;
               if ( AnyError == 0 )
               {
                  /* No cascading delete specified. */
                  /* Using cursor T000212 */
                  pr_datastore1.execute(10, new Object[] {n3Actividadid, A3Actividadid});
                  pr_datastore1.close(10);
                  dsDataStore1.SmartCacheProvider.SetUpdated("ACTIVIDAD") ;
                  if ( AnyError == 0 )
                  {
                     /* Start of After( delete) rules */
                     /* End of After( delete) rules */
                     if ( AnyError == 0 )
                     {
                        if ( ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 ) || ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 ) )
                        {
                           if ( AnyError == 0 )
                           {
                              context.nUserReturn = 1;
                           }
                        }
                     }
                  }
                  else
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                     AnyError = 1;
                  }
               }
            }
         }
         sMode2 = Gx_mode;
         Gx_mode = "DLT";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!")), context));
         EndLevel022( ) ;
         Gx_mode = sMode2;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!")), context));
      }

      protected void OnDeleteControls022( )
      {
         standaloneModal( ) ;
         if ( AnyError == 0 )
         {
            /* Delete mode formulas */
            AV13Pgmname = "Actividad";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13Pgmname", AV13Pgmname);
            /* Using cursor T000213 */
            pr_datastore1.execute(11, new Object[] {A4Indicadorid});
            A62resultado = T000213_A62resultado[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A62resultado", A62resultado);
            pr_datastore1.close(11);
         }
         if ( AnyError == 0 )
         {
            /* Using cursor T000214 */
            pr_datastore1.execute(12, new Object[] {n3Actividadid, A3Actividadid});
            if ( (pr_datastore1.getStatus(12) != 101) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_del", new   object[]  {"Plan"}), "CannotDeleteReferencedRecord", 1, "");
               AnyError = 1;
            }
            pr_datastore1.close(12);
            /* Using cursor T000215 */
            pr_datastore1.execute(13, new Object[] {n3Actividadid, A3Actividadid});
            if ( (pr_datastore1.getStatus(13) != 101) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_del", new   object[]  {"Actividad Para Plan"}), "CannotDeleteReferencedRecord", 1, "");
               AnyError = 1;
            }
            pr_datastore1.close(13);
            /* Using cursor T000216 */
            pr_datastore1.execute(14, new Object[] {n3Actividadid, A3Actividadid});
            if ( (pr_datastore1.getStatus(14) != 101) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_del", new   object[]  {"FYActividad"}), "CannotDeleteReferencedRecord", 1, "");
               AnyError = 1;
            }
            pr_datastore1.close(14);
         }
      }

      protected void EndLevel022( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
         {
            pr_datastore1.close(0);
         }
         if ( AnyError == 0 )
         {
            BeforeComplete022( ) ;
         }
         if ( AnyError == 0 )
         {
            pr_datastore1.close(1);
            pr_datastore1.close(11);
            context.CommitDataStores("actividad",pr_default);
            if ( AnyError == 0 )
            {
               ConfirmValues020( ) ;
            }
            /* After transaction rules */
            /* Execute 'After Trn' event if defined. */
            trnEnded = 1;
         }
         else
         {
            pr_datastore1.close(1);
            pr_datastore1.close(11);
            context.RollbackDataStores("actividad",pr_default);
         }
         IsModified = 0;
         if ( AnyError != 0 )
         {
            context.wjLoc = "";
            context.nUserReturn = 0;
         }
      }

      public void ScanStart022( )
      {
         /* Scan By routine */
         /* Using cursor T000217 */
         pr_datastore1.execute(15);
         RcdFound2 = 0;
         if ( (pr_datastore1.getStatus(15) != 101) )
         {
            RcdFound2 = 1;
            A3Actividadid = T000217_A3Actividadid[0];
            n3Actividadid = T000217_n3Actividadid[0];
         }
         /* Load Subordinate Levels */
      }

      protected void ScanNext022( )
      {
         /* Scan next routine */
         pr_datastore1.readNext(15);
         RcdFound2 = 0;
         if ( (pr_datastore1.getStatus(15) != 101) )
         {
            RcdFound2 = 1;
            A3Actividadid = T000217_A3Actividadid[0];
            n3Actividadid = T000217_n3Actividadid[0];
         }
      }

      protected void ScanEnd022( )
      {
         pr_datastore1.close(15);
      }

      protected void AfterConfirm022( )
      {
         /* After Confirm Rules */
      }

      protected void BeforeInsert022( )
      {
         /* Before Insert Rules */
      }

      protected void BeforeUpdate022( )
      {
         /* Before Update Rules */
      }

      protected void BeforeDelete022( )
      {
         /* Before Delete Rules */
      }

      protected void BeforeComplete022( )
      {
         /* Before Complete Rules */
      }

      protected void BeforeValidate022( )
      {
         /* Before Validate Rules */
      }

      protected void DisableAttributes022( )
      {
         edtActividaddescripcion_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtActividaddescripcion_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtActividaddescripcion_Enabled), 5, 0)), true);
         edttask_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edttask_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edttask_Enabled), 5, 0)), true);
         edtverificacion_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtverificacion_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtverificacion_Enabled), 5, 0)), true);
         edtfrecuencia_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtfrecuencia_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtfrecuencia_Enabled), 5, 0)), true);
         edtfuente_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtfuente_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtfuente_Enabled), 5, 0)), true);
         edtsupuestos_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtsupuestos_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtsupuestos_Enabled), 5, 0)), true);
         edtIndicadorid_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtIndicadorid_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtIndicadorid_Enabled), 5, 0)), true);
         edtresultado_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtresultado_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtresultado_Enabled), 5, 0)), true);
      }

      protected void send_integrity_lvl_hashes022( )
      {
      }

      protected void assign_properties_default( )
      {
      }

      protected void ConfirmValues020( )
      {
      }

      public override void RenderHtmlHeaders( )
      {
         GxWebStd.gx_html_headers( context, 0, "", "", Form.Meta, Form.Metaequiv, true);
      }

      public override void RenderHtmlOpenForm( )
      {
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.WriteHtmlText( "<title>") ;
         context.SendWebValue( Form.Caption) ;
         context.WriteHtmlTextNl( "</title>") ;
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         if ( StringUtil.Len( sDynURL) > 0 )
         {
            context.WriteHtmlText( "<BASE href=\""+sDynURL+"\" />") ;
         }
         define_styles( ) ;
         MasterPageObj.master_styles();
         if ( ( ( context.GetBrowserType( ) == 1 ) || ( context.GetBrowserType( ) == 5 ) ) && ( StringUtil.StrCmp(context.GetBrowserVersion( ), "7.0") == 0 ) )
         {
            context.AddJavascriptSource("json2.js", "?"+context.GetBuildNumber( 127771), false);
         }
         context.AddJavascriptSource("jquery.js", "?"+context.GetBuildNumber( 127771), false);
         context.AddJavascriptSource("gxtimezone.js", "?"+context.GetBuildNumber( 127771), false);
         context.AddJavascriptSource("gxgral.js", "?"+context.GetBuildNumber( 127771), false);
         context.AddJavascriptSource("gxcfg.js", "?20191514351813", false);
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.WriteHtmlText( Form.Headerrawhtml) ;
         context.CloseHtmlHeader();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         FormProcess = " data-HasEnter=\"true\" data-Skiponenter=\"false\"";
         context.WriteHtmlText( "<body ") ;
         bodyStyle = "" + "background-color:" + context.BuildHTMLColor( Form.Backcolor) + ";color:" + context.BuildHTMLColor( Form.Textcolor) + ";";
         bodyStyle = bodyStyle + "-moz-opacity:0;opacity:0;";
         if ( ! ( String.IsNullOrEmpty(StringUtil.RTrim( Form.Background)) ) )
         {
            bodyStyle = bodyStyle + " background-image:url(" + context.convertURL( Form.Background) + ")";
         }
         context.WriteHtmlText( " "+"class=\"form-horizontal Form\""+" "+ "style='"+bodyStyle+"'") ;
         context.WriteHtmlText( FormProcess+">") ;
         context.skipLines(1);
         context.WriteHtmlTextNl( "<form id=\"MAINFORM\" autocomplete=\"off\" name=\"MAINFORM\" method=\"post\" tabindex=-1  class=\"form-horizontal Form\" data-gx-class=\"form-horizontal Form\" novalidate action=\""+formatLink("actividad.aspx") + "?" + UrlEncode(StringUtil.RTrim(Gx_mode)) + "," + UrlEncode("" +AV7Actividadid)+"\">") ;
         GxWebStd.gx_hidden_field( context, "_EventName", "");
         GxWebStd.gx_hidden_field( context, "_EventGridId", "");
         GxWebStd.gx_hidden_field( context, "_EventRowId", "");
         context.WriteHtmlText( "<input type=\"submit\" title=\"submit\" style=\"display:none\" disabled>") ;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, "FORM", "Class", "form-horizontal Form", true);
         toggleJsOutput = isJsOutputEnabled( );
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
      }

      protected void send_integrity_footer_hashes( )
      {
         GXKey = Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
         forbiddenHiddens = "hsh" + "Actividad";
         forbiddenHiddens = forbiddenHiddens + context.localUtil.Format( (decimal)(AV11Insert_Indicadorid), "ZZZZZZZZ9");
         forbiddenHiddens = forbiddenHiddens + StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"));
         forbiddenHiddens = forbiddenHiddens + context.localUtil.Format( (decimal)(A3Actividadid), "ZZZZZZZZ9");
         GxWebStd.gx_hidden_field( context, "hsh", GetEncryptedHash( forbiddenHiddens, GXKey));
         GXUtil.WriteLog("actividad:[SendSecurityCheck value for]"+"Insert_Indicadorid:"+context.localUtil.Format( (decimal)(AV11Insert_Indicadorid), "ZZZZZZZZ9"));
         GXUtil.WriteLog("actividad:[SendSecurityCheck value for]"+"Gx_mode:"+StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!")));
         GXUtil.WriteLog("actividad:[SendSecurityCheck value for]"+"Actividadid:"+context.localUtil.Format( (decimal)(A3Actividadid), "ZZZZZZZZ9"));
      }

      protected void SendCloseFormHiddens( )
      {
         /* Send hidden variables. */
         /* Send saved values. */
         send_integrity_footer_hashes( ) ;
         GxWebStd.gx_hidden_field( context, "Z3Actividadid", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z3Actividadid), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "Z48Actividaddescripcion", Z48Actividaddescripcion);
         GxWebStd.gx_hidden_field( context, "Z49task", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z49task), 18, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "Z50verificacion", Z50verificacion);
         GxWebStd.gx_hidden_field( context, "Z51frecuencia", Z51frecuencia);
         GxWebStd.gx_hidden_field( context, "Z52fuente", Z52fuente);
         GxWebStd.gx_hidden_field( context, "Z53supuestos", Z53supuestos);
         GxWebStd.gx_hidden_field( context, "Z4Indicadorid", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z4Indicadorid), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "IsConfirmed", StringUtil.LTrim( StringUtil.NToC( (decimal)(IsConfirmed), 4, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "IsModified", StringUtil.LTrim( StringUtil.NToC( (decimal)(IsModified), 4, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "Mode", StringUtil.RTrim( Gx_mode));
         GxWebStd.gx_hidden_field( context, "gxhash_Mode", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!")), context));
         GxWebStd.gx_hidden_field( context, "N4Indicadorid", StringUtil.LTrim( StringUtil.NToC( (decimal)(A4Indicadorid), 9, 0, ".", "")));
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vTRNCONTEXT", AV9TrnContext);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vTRNCONTEXT", AV9TrnContext);
         }
         GxWebStd.gx_hidden_field( context, "vACTIVIDADID", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV7Actividadid), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "gxhash_vACTIVIDADID", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV7Actividadid), "ZZZZZZZZ9"), context));
         GxWebStd.gx_hidden_field( context, "ACTIVIDADID", StringUtil.LTrim( StringUtil.NToC( (decimal)(A3Actividadid), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "vINSERT_INDICADORID", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV11Insert_Indicadorid), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "vPGMNAME", StringUtil.RTrim( AV13Pgmname));
         GxWebStd.gx_hidden_field( context, "vMODE", StringUtil.RTrim( Gx_mode));
         GxWebStd.gx_hidden_field( context, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!")), context));
      }

      public override void RenderHtmlCloseForm( )
      {
         SendCloseFormHiddens( ) ;
         GxWebStd.gx_hidden_field( context, "GX_FocusControl", GX_FocusControl);
         SendAjaxEncryptionKey();
         SendSecurityToken(sPrefix);
         SendComponentObjects();
         SendServerCommands();
         SendState();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         context.WriteHtmlTextNl( "</form>") ;
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         include_jscripts( ) ;
      }

      public override short ExecuteStartEvent( )
      {
         standaloneStartup( ) ;
         gxajaxcallmode = (short)((isAjaxCallMode( ) ? 1 : 0));
         return gxajaxcallmode ;
      }

      public override void RenderHtmlContent( )
      {
         context.WriteHtmlText( "<div") ;
         GxWebStd.ClassAttribute( context, "gx-ct-body"+" "+(String.IsNullOrEmpty(StringUtil.RTrim( Form.Class)) ? "form-horizontal Form" : Form.Class)+"-fx");
         context.WriteHtmlText( ">") ;
         Draw( ) ;
         context.WriteHtmlText( "</div>") ;
      }

      public override void DispatchEvents( )
      {
         Process( ) ;
      }

      public override bool HasEnterEvent( )
      {
         return true ;
      }

      public override GXWebForm GetForm( )
      {
         return Form ;
      }

      public override String GetSelfLink( )
      {
         return formatLink("actividad.aspx") + "?" + UrlEncode(StringUtil.RTrim(Gx_mode)) + "," + UrlEncode("" +AV7Actividadid) ;
      }

      public override String GetPgmname( )
      {
         return "Actividad" ;
      }

      public override String GetPgmdesc( )
      {
         return "Actividad" ;
      }

      protected void InitializeNonKey022( )
      {
         A4Indicadorid = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A4Indicadorid", StringUtil.LTrim( StringUtil.Str( (decimal)(A4Indicadorid), 9, 0)));
         A48Actividaddescripcion = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A48Actividaddescripcion", A48Actividaddescripcion);
         A49task = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A49task", StringUtil.LTrim( StringUtil.Str( (decimal)(A49task), 18, 0)));
         A50verificacion = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A50verificacion", A50verificacion);
         A51frecuencia = "";
         n51frecuencia = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A51frecuencia", A51frecuencia);
         n51frecuencia = (String.IsNullOrEmpty(StringUtil.RTrim( A51frecuencia)) ? true : false);
         A52fuente = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A52fuente", A52fuente);
         A53supuestos = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A53supuestos", A53supuestos);
         A62resultado = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A62resultado", A62resultado);
         Z48Actividaddescripcion = "";
         Z49task = 0;
         Z50verificacion = "";
         Z51frecuencia = "";
         Z52fuente = "";
         Z53supuestos = "";
         Z4Indicadorid = 0;
      }

      protected void InitAll022( )
      {
         A3Actividadid = 0;
         n3Actividadid = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A3Actividadid", StringUtil.LTrim( StringUtil.Str( (decimal)(A3Actividadid), 9, 0)));
         InitializeNonKey022( ) ;
      }

      protected void StandaloneModalInsert( )
      {
      }

      protected void define_styles( )
      {
         AddThemeStyleSheetFile("", context.GetTheme( )+".css", "?"+GetCacheInvalidationToken( ));
         bool outputEnabled = isOutputEnabled( ) ;
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         idxLst = 1;
         while ( idxLst <= Form.Jscriptsrc.Count )
         {
            context.AddJavascriptSource(StringUtil.RTrim( ((String)Form.Jscriptsrc.Item(idxLst))), "?20191514351827", true);
            idxLst = (int)(idxLst+1);
         }
         if ( ! outputEnabled )
         {
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
         }
         /* End function define_styles */
      }

      protected void include_jscripts( )
      {
         context.AddJavascriptSource("messages.eng.js", "?"+GetCacheInvalidationToken( ), false);
         context.AddJavascriptSource("gxdec.js", "?"+context.GetBuildNumber( 127771), false);
         context.AddJavascriptSource("actividad.js", "?20191514351828", false);
         /* End function include_jscripts */
      }

      protected void init_default_properties( )
      {
         lblTitle_Internalname = "TITLE";
         divTitlecontainer_Internalname = "TITLECONTAINER";
         bttBtn_first_Internalname = "BTN_FIRST";
         bttBtn_previous_Internalname = "BTN_PREVIOUS";
         bttBtn_next_Internalname = "BTN_NEXT";
         bttBtn_last_Internalname = "BTN_LAST";
         bttBtn_select_Internalname = "BTN_SELECT";
         divToolbarcell_Internalname = "TOOLBARCELL";
         edtActividaddescripcion_Internalname = "ACTIVIDADDESCRIPCION";
         edttask_Internalname = "TASK";
         edtverificacion_Internalname = "VERIFICACION";
         edtfrecuencia_Internalname = "FRECUENCIA";
         edtfuente_Internalname = "FUENTE";
         edtsupuestos_Internalname = "SUPUESTOS";
         edtIndicadorid_Internalname = "INDICADORID";
         edtresultado_Internalname = "RESULTADO";
         divFormcontainer_Internalname = "FORMCONTAINER";
         bttBtn_enter_Internalname = "BTN_ENTER";
         bttBtn_cancel_Internalname = "BTN_CANCEL";
         bttBtn_delete_Internalname = "BTN_DELETE";
         divMaintable_Internalname = "MAINTABLE";
         Form.Internalname = "FORM";
         imgprompt_4_Internalname = "PROMPT_4";
      }

      public override void initialize_properties( )
      {
         context.SetDefaultTheme("Carmine");
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         init_default_properties( ) ;
         Form.Headerrawhtml = "";
         Form.Background = "";
         Form.Textcolor = 0;
         Form.Backcolor = (int)(0xFFFFFF);
         Form.Caption = "Actividad";
         bttBtn_delete_Enabled = 0;
         bttBtn_delete_Visible = 1;
         bttBtn_cancel_Visible = 1;
         bttBtn_enter_Enabled = 1;
         bttBtn_enter_Visible = 1;
         edtresultado_Enabled = 0;
         imgprompt_4_Visible = 1;
         imgprompt_4_Link = "";
         edtIndicadorid_Jsonclick = "";
         edtIndicadorid_Enabled = 1;
         edtsupuestos_Enabled = 1;
         edtfuente_Jsonclick = "";
         edtfuente_Enabled = 1;
         edtfrecuencia_Jsonclick = "";
         edtfrecuencia_Enabled = 1;
         edtverificacion_Enabled = 1;
         edttask_Jsonclick = "";
         edttask_Enabled = 1;
         edtActividaddescripcion_Enabled = 1;
         bttBtn_select_Visible = 1;
         bttBtn_last_Visible = 1;
         bttBtn_next_Visible = 1;
         bttBtn_previous_Visible = 1;
         bttBtn_first_Visible = 1;
         context.GX_msglist.DisplayMode = 1;
         if ( context.isSpaRequest( ) )
         {
            enableJsOutput();
         }
      }

      protected void dynload_actions( )
      {
         /* End function dynload_actions */
      }

      protected void init_web_controls( )
      {
         /* End function init_web_controls */
      }

      public void Valid_Indicadorid( int GX_Parm1 ,
                                     String GX_Parm2 )
      {
         A4Indicadorid = GX_Parm1;
         A62resultado = GX_Parm2;
         /* Using cursor T000213 */
         pr_datastore1.execute(11, new Object[] {A4Indicadorid});
         if ( (pr_datastore1.getStatus(11) == 101) )
         {
            GX_msglist.addItem("No matching 'Indicador'.", "ForeignKeyNotFound", 1, "INDICADORID");
            AnyError = 1;
            GX_FocusControl = edtIndicadorid_Internalname;
         }
         A62resultado = T000213_A62resultado[0];
         pr_datastore1.close(11);
         dynload_actions( ) ;
         if ( AnyError == 1 )
         {
            A62resultado = "";
         }
         isValidOutput.Add(A62resultado);
         isValidOutput.Add(context.GX_msglist.ToJavascriptSource());
         isValidOutput.Add(context.httpAjaxContext.ajax_rsp_get_hiddens( ));
         context.GX_webresponse.AddString(isValidOutput.ToJSonString());
         wbTemp = context.ResponseContentType( "application/json");
      }

      public override bool SupportAjaxEvent( )
      {
         return true ;
      }

      public override void InitializeDynEvents( )
      {
         setEventMetadata("ENTER","{handler:'UserMainFullajax',iparms:[{postForm:true},{av:'Gx_mode',fld:'vMODE',pic:'@!',hsh:true},{av:'AV7Actividadid',fld:'vACTIVIDADID',pic:'ZZZZZZZZ9',hsh:true}]");
         setEventMetadata("ENTER",",oparms:[]}");
         setEventMetadata("REFRESH","{handler:'Refresh',iparms:[{av:'Gx_mode',fld:'vMODE',pic:'@!',hsh:true},{av:'AV7Actividadid',fld:'vACTIVIDADID',pic:'ZZZZZZZZ9',hsh:true},{av:'AV11Insert_Indicadorid',fld:'vINSERT_INDICADORID',pic:'ZZZZZZZZ9'},{av:'A3Actividadid',fld:'ACTIVIDADID',pic:'ZZZZZZZZ9'}]");
         setEventMetadata("REFRESH",",oparms:[]}");
         setEventMetadata("AFTER TRN","{handler:'E12022',iparms:[{av:'Gx_mode',fld:'vMODE',pic:'@!',hsh:true},{av:'AV9TrnContext',fld:'vTRNCONTEXT',pic:''}]");
         setEventMetadata("AFTER TRN",",oparms:[]}");
         return  ;
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
         pr_datastore1.close(1);
         pr_datastore1.close(11);
      }

      public override void initialize( )
      {
         sPrefix = "";
         wcpOGx_mode = "";
         Z48Actividaddescripcion = "";
         Z50verificacion = "";
         Z51frecuencia = "";
         Z52fuente = "";
         Z53supuestos = "";
         scmdbuf = "";
         gxfirstwebparm = "";
         gxfirstwebparm_bkp = "";
         GXKey = "";
         PreviousTooltip = "";
         PreviousCaption = "";
         Form = new GXWebForm();
         GX_FocusControl = "";
         lblTitle_Jsonclick = "";
         ClassString = "";
         StyleString = "";
         TempTags = "";
         bttBtn_first_Jsonclick = "";
         bttBtn_previous_Jsonclick = "";
         bttBtn_next_Jsonclick = "";
         bttBtn_last_Jsonclick = "";
         bttBtn_select_Jsonclick = "";
         A48Actividaddescripcion = "";
         A50verificacion = "";
         A51frecuencia = "";
         A52fuente = "";
         A53supuestos = "";
         sImgUrl = "";
         A62resultado = "";
         bttBtn_enter_Jsonclick = "";
         bttBtn_cancel_Jsonclick = "";
         bttBtn_delete_Jsonclick = "";
         AV13Pgmname = "";
         forbiddenHiddens = "";
         hsh = "";
         sMode2 = "";
         sEvt = "";
         EvtGridId = "";
         EvtRowId = "";
         sEvtType = "";
         AV9TrnContext = new SdtTransactionContext(context);
         AV10WebSession = context.GetSession();
         AV12TrnContextAtt = new SdtTransactionContext_Attribute(context);
         Z62resultado = "";
         T00024_A62resultado = new String[] {""} ;
         T00025_A3Actividadid = new int[1] ;
         T00025_n3Actividadid = new bool[] {false} ;
         T00025_A48Actividaddescripcion = new String[] {""} ;
         T00025_A49task = new long[1] ;
         T00025_A50verificacion = new String[] {""} ;
         T00025_A51frecuencia = new String[] {""} ;
         T00025_n51frecuencia = new bool[] {false} ;
         T00025_A52fuente = new String[] {""} ;
         T00025_A53supuestos = new String[] {""} ;
         T00025_A62resultado = new String[] {""} ;
         T00025_A4Indicadorid = new int[1] ;
         T00026_A62resultado = new String[] {""} ;
         T00027_A3Actividadid = new int[1] ;
         T00027_n3Actividadid = new bool[] {false} ;
         T00023_A3Actividadid = new int[1] ;
         T00023_n3Actividadid = new bool[] {false} ;
         T00023_A48Actividaddescripcion = new String[] {""} ;
         T00023_A49task = new long[1] ;
         T00023_A50verificacion = new String[] {""} ;
         T00023_A51frecuencia = new String[] {""} ;
         T00023_n51frecuencia = new bool[] {false} ;
         T00023_A52fuente = new String[] {""} ;
         T00023_A53supuestos = new String[] {""} ;
         T00023_A4Indicadorid = new int[1] ;
         T00028_A3Actividadid = new int[1] ;
         T00028_n3Actividadid = new bool[] {false} ;
         T00029_A3Actividadid = new int[1] ;
         T00029_n3Actividadid = new bool[] {false} ;
         T00022_A3Actividadid = new int[1] ;
         T00022_n3Actividadid = new bool[] {false} ;
         T00022_A48Actividaddescripcion = new String[] {""} ;
         T00022_A49task = new long[1] ;
         T00022_A50verificacion = new String[] {""} ;
         T00022_A51frecuencia = new String[] {""} ;
         T00022_n51frecuencia = new bool[] {false} ;
         T00022_A52fuente = new String[] {""} ;
         T00022_A53supuestos = new String[] {""} ;
         T00022_A4Indicadorid = new int[1] ;
         T000210_A3Actividadid = new int[1] ;
         T000210_n3Actividadid = new bool[] {false} ;
         T000213_A62resultado = new String[] {""} ;
         T000214_A13Planid = new int[1] ;
         T000215_A23ActividadParaPlanid = new int[1] ;
         T000216_A21FYActividadid = new int[1] ;
         T000217_A3Actividadid = new int[1] ;
         T000217_n3Actividadid = new bool[] {false} ;
         sDynURL = "";
         FormProcess = "";
         bodyStyle = "";
         isValidOutput = new GxUnknownObjectCollection();
         pr_datastore1 = new DataStoreProvider(context, new GeneXus.Programs.actividad__datastore1(),
            new Object[][] {
                new Object[] {
               T00022_A3Actividadid, T00022_A48Actividaddescripcion, T00022_A49task, T00022_A50verificacion, T00022_A51frecuencia, T00022_n51frecuencia, T00022_A52fuente, T00022_A53supuestos, T00022_A4Indicadorid
               }
               , new Object[] {
               T00023_A3Actividadid, T00023_A48Actividaddescripcion, T00023_A49task, T00023_A50verificacion, T00023_A51frecuencia, T00023_n51frecuencia, T00023_A52fuente, T00023_A53supuestos, T00023_A4Indicadorid
               }
               , new Object[] {
               T00024_A62resultado
               }
               , new Object[] {
               T00025_A3Actividadid, T00025_A48Actividaddescripcion, T00025_A49task, T00025_A50verificacion, T00025_A51frecuencia, T00025_n51frecuencia, T00025_A52fuente, T00025_A53supuestos, T00025_A62resultado, T00025_A4Indicadorid
               }
               , new Object[] {
               T00026_A62resultado
               }
               , new Object[] {
               T00027_A3Actividadid
               }
               , new Object[] {
               T00028_A3Actividadid
               }
               , new Object[] {
               T00029_A3Actividadid
               }
               , new Object[] {
               T000210_A3Actividadid
               }
               , new Object[] {
               }
               , new Object[] {
               }
               , new Object[] {
               T000213_A62resultado
               }
               , new Object[] {
               T000214_A13Planid
               }
               , new Object[] {
               T000215_A23ActividadParaPlanid
               }
               , new Object[] {
               T000216_A21FYActividadid
               }
               , new Object[] {
               T000217_A3Actividadid
               }
            }
         );
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.actividad__default(),
            new Object[][] {
            }
         );
         AV13Pgmname = "Actividad";
      }

      private short GxWebError ;
      private short gxcookieaux ;
      private short IsConfirmed ;
      private short IsModified ;
      private short AnyError ;
      private short nKeyPressed ;
      private short initialized ;
      private short RcdFound2 ;
      private short GX_JID ;
      private short Gx_BScreen ;
      private short gxajaxcallmode ;
      private short wbTemp ;
      private int wcpOAV7Actividadid ;
      private int Z3Actividadid ;
      private int Z4Indicadorid ;
      private int N4Indicadorid ;
      private int A4Indicadorid ;
      private int AV7Actividadid ;
      private int trnEnded ;
      private int bttBtn_first_Visible ;
      private int bttBtn_previous_Visible ;
      private int bttBtn_next_Visible ;
      private int bttBtn_last_Visible ;
      private int bttBtn_select_Visible ;
      private int edtActividaddescripcion_Enabled ;
      private int edttask_Enabled ;
      private int edtverificacion_Enabled ;
      private int edtfrecuencia_Enabled ;
      private int edtfuente_Enabled ;
      private int edtsupuestos_Enabled ;
      private int edtIndicadorid_Enabled ;
      private int imgprompt_4_Visible ;
      private int edtresultado_Enabled ;
      private int bttBtn_enter_Visible ;
      private int bttBtn_enter_Enabled ;
      private int bttBtn_cancel_Visible ;
      private int bttBtn_delete_Visible ;
      private int bttBtn_delete_Enabled ;
      private int A3Actividadid ;
      private int AV11Insert_Indicadorid ;
      private int AV14GXV1 ;
      private int idxLst ;
      private long Z49task ;
      private long A49task ;
      private String sPrefix ;
      private String wcpOGx_mode ;
      private String scmdbuf ;
      private String gxfirstwebparm ;
      private String gxfirstwebparm_bkp ;
      private String Gx_mode ;
      private String GXKey ;
      private String PreviousTooltip ;
      private String PreviousCaption ;
      private String GX_FocusControl ;
      private String edtActividaddescripcion_Internalname ;
      private String divMaintable_Internalname ;
      private String divTitlecontainer_Internalname ;
      private String lblTitle_Internalname ;
      private String lblTitle_Jsonclick ;
      private String ClassString ;
      private String StyleString ;
      private String divFormcontainer_Internalname ;
      private String divToolbarcell_Internalname ;
      private String TempTags ;
      private String bttBtn_first_Internalname ;
      private String bttBtn_first_Jsonclick ;
      private String bttBtn_previous_Internalname ;
      private String bttBtn_previous_Jsonclick ;
      private String bttBtn_next_Internalname ;
      private String bttBtn_next_Jsonclick ;
      private String bttBtn_last_Internalname ;
      private String bttBtn_last_Jsonclick ;
      private String bttBtn_select_Internalname ;
      private String bttBtn_select_Jsonclick ;
      private String edttask_Internalname ;
      private String edttask_Jsonclick ;
      private String edtverificacion_Internalname ;
      private String edtfrecuencia_Internalname ;
      private String edtfrecuencia_Jsonclick ;
      private String edtfuente_Internalname ;
      private String edtfuente_Jsonclick ;
      private String edtsupuestos_Internalname ;
      private String edtIndicadorid_Internalname ;
      private String edtIndicadorid_Jsonclick ;
      private String sImgUrl ;
      private String imgprompt_4_Internalname ;
      private String imgprompt_4_Link ;
      private String edtresultado_Internalname ;
      private String bttBtn_enter_Internalname ;
      private String bttBtn_enter_Jsonclick ;
      private String bttBtn_cancel_Internalname ;
      private String bttBtn_cancel_Jsonclick ;
      private String bttBtn_delete_Internalname ;
      private String bttBtn_delete_Jsonclick ;
      private String AV13Pgmname ;
      private String forbiddenHiddens ;
      private String hsh ;
      private String sMode2 ;
      private String sEvt ;
      private String EvtGridId ;
      private String EvtRowId ;
      private String sEvtType ;
      private String sDynURL ;
      private String FormProcess ;
      private String bodyStyle ;
      private bool entryPointCalled ;
      private bool toggleJsOutput ;
      private bool wbErr ;
      private bool n51frecuencia ;
      private bool n3Actividadid ;
      private bool returnInSub ;
      private bool Gx_longc ;
      private String Z48Actividaddescripcion ;
      private String Z50verificacion ;
      private String Z51frecuencia ;
      private String Z52fuente ;
      private String Z53supuestos ;
      private String A48Actividaddescripcion ;
      private String A50verificacion ;
      private String A51frecuencia ;
      private String A52fuente ;
      private String A53supuestos ;
      private String A62resultado ;
      private String Z62resultado ;
      private IGxSession AV10WebSession ;
      private GxUnknownObjectCollection isValidOutput ;
      private IGxDataStore dsDataStore1 ;
      private IGxDataStore dsDefault ;
      private IDataStoreProvider pr_datastore1 ;
      private String[] T00024_A62resultado ;
      private int[] T00025_A3Actividadid ;
      private bool[] T00025_n3Actividadid ;
      private String[] T00025_A48Actividaddescripcion ;
      private long[] T00025_A49task ;
      private String[] T00025_A50verificacion ;
      private String[] T00025_A51frecuencia ;
      private bool[] T00025_n51frecuencia ;
      private String[] T00025_A52fuente ;
      private String[] T00025_A53supuestos ;
      private String[] T00025_A62resultado ;
      private int[] T00025_A4Indicadorid ;
      private String[] T00026_A62resultado ;
      private int[] T00027_A3Actividadid ;
      private bool[] T00027_n3Actividadid ;
      private int[] T00023_A3Actividadid ;
      private bool[] T00023_n3Actividadid ;
      private String[] T00023_A48Actividaddescripcion ;
      private long[] T00023_A49task ;
      private String[] T00023_A50verificacion ;
      private String[] T00023_A51frecuencia ;
      private bool[] T00023_n51frecuencia ;
      private String[] T00023_A52fuente ;
      private String[] T00023_A53supuestos ;
      private int[] T00023_A4Indicadorid ;
      private int[] T00028_A3Actividadid ;
      private bool[] T00028_n3Actividadid ;
      private int[] T00029_A3Actividadid ;
      private bool[] T00029_n3Actividadid ;
      private int[] T00022_A3Actividadid ;
      private bool[] T00022_n3Actividadid ;
      private String[] T00022_A48Actividaddescripcion ;
      private long[] T00022_A49task ;
      private String[] T00022_A50verificacion ;
      private String[] T00022_A51frecuencia ;
      private bool[] T00022_n51frecuencia ;
      private String[] T00022_A52fuente ;
      private String[] T00022_A53supuestos ;
      private int[] T00022_A4Indicadorid ;
      private int[] T000210_A3Actividadid ;
      private bool[] T000210_n3Actividadid ;
      private String[] T000213_A62resultado ;
      private int[] T000214_A13Planid ;
      private int[] T000215_A23ActividadParaPlanid ;
      private int[] T000216_A21FYActividadid ;
      private IDataStoreProvider pr_default ;
      private int[] T000217_A3Actividadid ;
      private bool[] T000217_n3Actividadid ;
      private GXWebForm Form ;
      private SdtTransactionContext AV9TrnContext ;
      private SdtTransactionContext_Attribute AV12TrnContextAtt ;
   }

   public class actividad__datastore1 : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
         ,new ForEachCursor(def[2])
         ,new ForEachCursor(def[3])
         ,new ForEachCursor(def[4])
         ,new ForEachCursor(def[5])
         ,new ForEachCursor(def[6])
         ,new ForEachCursor(def[7])
         ,new ForEachCursor(def[8])
         ,new UpdateCursor(def[9])
         ,new UpdateCursor(def[10])
         ,new ForEachCursor(def[11])
         ,new ForEachCursor(def[12])
         ,new ForEachCursor(def[13])
         ,new ForEachCursor(def[14])
         ,new ForEachCursor(def[15])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmT00025 ;
          prmT00025 = new Object[] {
          new Object[] {"@Actividadid",SqlDbType.Int,9,0}
          } ;
          Object[] prmT00024 ;
          prmT00024 = new Object[] {
          new Object[] {"@Indicadorid",SqlDbType.Int,9,0}
          } ;
          Object[] prmT00026 ;
          prmT00026 = new Object[] {
          new Object[] {"@Indicadorid",SqlDbType.Int,9,0}
          } ;
          Object[] prmT00027 ;
          prmT00027 = new Object[] {
          new Object[] {"@Actividadid",SqlDbType.Int,9,0}
          } ;
          Object[] prmT00023 ;
          prmT00023 = new Object[] {
          new Object[] {"@Actividadid",SqlDbType.Int,9,0}
          } ;
          Object[] prmT00028 ;
          prmT00028 = new Object[] {
          new Object[] {"@Actividadid",SqlDbType.Int,9,0}
          } ;
          Object[] prmT00029 ;
          prmT00029 = new Object[] {
          new Object[] {"@Actividadid",SqlDbType.Int,9,0}
          } ;
          Object[] prmT00022 ;
          prmT00022 = new Object[] {
          new Object[] {"@Actividadid",SqlDbType.Int,9,0}
          } ;
          Object[] prmT000210 ;
          prmT000210 = new Object[] {
          new Object[] {"@Actividaddescripcion",SqlDbType.VarChar,300,0} ,
          new Object[] {"@task",SqlDbType.Decimal,18,0} ,
          new Object[] {"@verificacion",SqlDbType.VarChar,200,0} ,
          new Object[] {"@frecuencia",SqlDbType.VarChar,50,0} ,
          new Object[] {"@fuente",SqlDbType.VarChar,100,0} ,
          new Object[] {"@supuestos",SqlDbType.VarChar,200,0} ,
          new Object[] {"@Indicadorid",SqlDbType.Int,9,0}
          } ;
          Object[] prmT000211 ;
          prmT000211 = new Object[] {
          new Object[] {"@Actividaddescripcion",SqlDbType.VarChar,300,0} ,
          new Object[] {"@task",SqlDbType.Decimal,18,0} ,
          new Object[] {"@verificacion",SqlDbType.VarChar,200,0} ,
          new Object[] {"@frecuencia",SqlDbType.VarChar,50,0} ,
          new Object[] {"@fuente",SqlDbType.VarChar,100,0} ,
          new Object[] {"@supuestos",SqlDbType.VarChar,200,0} ,
          new Object[] {"@Indicadorid",SqlDbType.Int,9,0} ,
          new Object[] {"@Actividadid",SqlDbType.Int,9,0}
          } ;
          Object[] prmT000212 ;
          prmT000212 = new Object[] {
          new Object[] {"@Actividadid",SqlDbType.Int,9,0}
          } ;
          Object[] prmT000214 ;
          prmT000214 = new Object[] {
          new Object[] {"@Actividadid",SqlDbType.Int,9,0}
          } ;
          Object[] prmT000215 ;
          prmT000215 = new Object[] {
          new Object[] {"@Actividadid",SqlDbType.Int,9,0}
          } ;
          Object[] prmT000216 ;
          prmT000216 = new Object[] {
          new Object[] {"@Actividadid",SqlDbType.Int,9,0}
          } ;
          Object[] prmT000217 ;
          prmT000217 = new Object[] {
          } ;
          Object[] prmT000213 ;
          prmT000213 = new Object[] {
          new Object[] {"@Indicadorid",SqlDbType.Int,9,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("T00022", "SELECT [id] AS Actividadid, [descripcion], [task], [verificacion], [frecuencia], [fuente], [supuestos], [idIndicador] AS Indicadorid FROM dbo.[Actividad] WITH (UPDLOCK) WHERE [id] = @Actividadid ",true, GxErrorMask.GX_NOMASK, false, this,prmT00022,1,0,true,false )
             ,new CursorDef("T00023", "SELECT [id] AS Actividadid, [descripcion], [task], [verificacion], [frecuencia], [fuente], [supuestos], [idIndicador] AS Indicadorid FROM dbo.[Actividad] WITH (NOLOCK) WHERE [id] = @Actividadid ",true, GxErrorMask.GX_NOMASK, false, this,prmT00023,1,0,true,false )
             ,new CursorDef("T00024", "SELECT [resultado] FROM dbo.[Indicador] WITH (NOLOCK) WHERE [id] = @Indicadorid ",true, GxErrorMask.GX_NOMASK, false, this,prmT00024,1,0,true,false )
             ,new CursorDef("T00025", "SELECT TM1.[id] AS Actividadid, TM1.[descripcion], TM1.[task], TM1.[verificacion], TM1.[frecuencia], TM1.[fuente], TM1.[supuestos], T2.[resultado], TM1.[idIndicador] AS Indicadorid FROM (dbo.[Actividad] TM1 WITH (NOLOCK) INNER JOIN dbo.[Indicador] T2 WITH (NOLOCK) ON T2.[id] = TM1.[idIndicador]) WHERE TM1.[id] = @Actividadid ORDER BY TM1.[id]  OPTION (FAST 100)",true, GxErrorMask.GX_NOMASK, false, this,prmT00025,100,0,true,false )
             ,new CursorDef("T00026", "SELECT [resultado] FROM dbo.[Indicador] WITH (NOLOCK) WHERE [id] = @Indicadorid ",true, GxErrorMask.GX_NOMASK, false, this,prmT00026,1,0,true,false )
             ,new CursorDef("T00027", "SELECT [id] AS Actividadid FROM dbo.[Actividad] WITH (NOLOCK) WHERE [id] = @Actividadid  OPTION (FAST 1)",true, GxErrorMask.GX_NOMASK, false, this,prmT00027,1,0,true,false )
             ,new CursorDef("T00028", "SELECT TOP 1 [id] AS Actividadid FROM dbo.[Actividad] WITH (NOLOCK) WHERE ( [id] > @Actividadid) ORDER BY [id]  OPTION (FAST 1)",true, GxErrorMask.GX_NOMASK, false, this,prmT00028,1,0,true,true )
             ,new CursorDef("T00029", "SELECT TOP 1 [id] AS Actividadid FROM dbo.[Actividad] WITH (NOLOCK) WHERE ( [id] < @Actividadid) ORDER BY [id] DESC  OPTION (FAST 1)",true, GxErrorMask.GX_NOMASK, false, this,prmT00029,1,0,true,true )
             ,new CursorDef("T000210", "INSERT INTO dbo.[Actividad]([descripcion], [task], [verificacion], [frecuencia], [fuente], [supuestos], [idIndicador]) VALUES(@Actividaddescripcion, @task, @verificacion, @frecuencia, @fuente, @supuestos, @Indicadorid); SELECT SCOPE_IDENTITY()", GxErrorMask.GX_NOMASK,prmT000210)
             ,new CursorDef("T000211", "UPDATE dbo.[Actividad] SET [descripcion]=@Actividaddescripcion, [task]=@task, [verificacion]=@verificacion, [frecuencia]=@frecuencia, [fuente]=@fuente, [supuestos]=@supuestos, [idIndicador]=@Indicadorid  WHERE [id] = @Actividadid", GxErrorMask.GX_NOMASK,prmT000211)
             ,new CursorDef("T000212", "DELETE FROM dbo.[Actividad]  WHERE [id] = @Actividadid", GxErrorMask.GX_NOMASK,prmT000212)
             ,new CursorDef("T000213", "SELECT [resultado] FROM dbo.[Indicador] WITH (NOLOCK) WHERE [id] = @Indicadorid ",true, GxErrorMask.GX_NOMASK, false, this,prmT000213,1,0,true,false )
             ,new CursorDef("T000214", "SELECT TOP 1 [id] AS Planid FROM dbo.[Plan] WITH (NOLOCK) WHERE [idActividad] = @Actividadid ",true, GxErrorMask.GX_NOMASK, false, this,prmT000214,1,0,true,true )
             ,new CursorDef("T000215", "SELECT TOP 1 [id] AS ActividadParaPlanid FROM dbo.[ActividadParaPlan] WITH (NOLOCK) WHERE [idActividad] = @Actividadid ",true, GxErrorMask.GX_NOMASK, false, this,prmT000215,1,0,true,true )
             ,new CursorDef("T000216", "SELECT TOP 1 [id] AS FYActividadid FROM dbo.[FYActividad] WITH (NOLOCK) WHERE [idActividad] = @Actividadid ",true, GxErrorMask.GX_NOMASK, false, this,prmT000216,1,0,true,true )
             ,new CursorDef("T000217", "SELECT [id] AS Actividadid FROM dbo.[Actividad] WITH (NOLOCK) ORDER BY [id]  OPTION (FAST 100)",true, GxErrorMask.GX_NOMASK, false, this,prmT000217,100,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getVarchar(2) ;
                ((long[]) buf[2])[0] = rslt.getLong(3) ;
                ((String[]) buf[3])[0] = rslt.getVarchar(4) ;
                ((String[]) buf[4])[0] = rslt.getVarchar(5) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(5);
                ((String[]) buf[6])[0] = rslt.getVarchar(6) ;
                ((String[]) buf[7])[0] = rslt.getVarchar(7) ;
                ((int[]) buf[8])[0] = rslt.getInt(8) ;
                return;
             case 1 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getVarchar(2) ;
                ((long[]) buf[2])[0] = rslt.getLong(3) ;
                ((String[]) buf[3])[0] = rslt.getVarchar(4) ;
                ((String[]) buf[4])[0] = rslt.getVarchar(5) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(5);
                ((String[]) buf[6])[0] = rslt.getVarchar(6) ;
                ((String[]) buf[7])[0] = rslt.getVarchar(7) ;
                ((int[]) buf[8])[0] = rslt.getInt(8) ;
                return;
             case 2 :
                ((String[]) buf[0])[0] = rslt.getVarchar(1) ;
                return;
             case 3 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getVarchar(2) ;
                ((long[]) buf[2])[0] = rslt.getLong(3) ;
                ((String[]) buf[3])[0] = rslt.getVarchar(4) ;
                ((String[]) buf[4])[0] = rslt.getVarchar(5) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(5);
                ((String[]) buf[6])[0] = rslt.getVarchar(6) ;
                ((String[]) buf[7])[0] = rslt.getVarchar(7) ;
                ((String[]) buf[8])[0] = rslt.getVarchar(8) ;
                ((int[]) buf[9])[0] = rslt.getInt(9) ;
                return;
             case 4 :
                ((String[]) buf[0])[0] = rslt.getVarchar(1) ;
                return;
             case 5 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 6 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 7 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 8 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 11 :
                ((String[]) buf[0])[0] = rslt.getVarchar(1) ;
                return;
             case 12 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 13 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 14 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 15 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 1 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 2 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 3 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 4 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 5 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 6 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 7 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 8 :
                stmt.SetParameter(1, (String)parms[0]);
                stmt.SetParameter(2, (long)parms[1]);
                stmt.SetParameter(3, (String)parms[2]);
                if ( (bool)parms[3] )
                {
                   stmt.setNull( 4 , SqlDbType.VarChar );
                }
                else
                {
                   stmt.SetParameter(4, (String)parms[4]);
                }
                stmt.SetParameter(5, (String)parms[5]);
                stmt.SetParameter(6, (String)parms[6]);
                stmt.SetParameter(7, (int)parms[7]);
                return;
             case 9 :
                stmt.SetParameter(1, (String)parms[0]);
                stmt.SetParameter(2, (long)parms[1]);
                stmt.SetParameter(3, (String)parms[2]);
                if ( (bool)parms[3] )
                {
                   stmt.setNull( 4 , SqlDbType.VarChar );
                }
                else
                {
                   stmt.SetParameter(4, (String)parms[4]);
                }
                stmt.SetParameter(5, (String)parms[5]);
                stmt.SetParameter(6, (String)parms[6]);
                stmt.SetParameter(7, (int)parms[7]);
                if ( (bool)parms[8] )
                {
                   stmt.setNull( 8 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(8, (int)parms[9]);
                }
                return;
             case 10 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 11 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 12 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 13 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 14 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
       }
    }

    public String getDataStoreName( )
    {
       return "DATASTORE1";
    }

 }

 public class actividad__default : DataStoreHelperBase, IDataStoreHelper
 {
    public ICursor[] getCursors( )
    {
       cursorDefinitions();
       return new Cursor[] {
     };
  }

  private static CursorDef[] def;
  private void cursorDefinitions( )
  {
     if ( def == null )
     {
        def= new CursorDef[] {
        };
     }
  }

  public void getResults( int cursor ,
                          IFieldGetter rslt ,
                          Object[] buf )
  {
     switch ( cursor )
     {
     }
  }

  public void setParameters( int cursor ,
                             IFieldSetter stmt ,
                             Object[] parms )
  {
     switch ( cursor )
     {
     }
  }

}

}
