using System;
using GeneXus.Builder;
using System.IO;
public class bldDevelopermenu : GxBaseBuilder
{
   string cs_path = "." ;
   public bldDevelopermenu( ) : base()
   {
   }

   public override int BeforeCompile( )
   {
      return 0 ;
   }

   public override int AfterCompile( )
   {
      int ErrCode ;
      ErrCode = 0;
      if ( ! File.Exists(@"bin\client.exe.config") || checkTime(@"bin\client.exe.config",cs_path + @"\client.exe.config") )
      {
         File.Copy( cs_path + @"\client.exe.config", @"bin\client.exe.config", true);
      }
      return ErrCode ;
   }

   static public int Main( string[] args )
   {
      bldDevelopermenu x = new bldDevelopermenu() ;
      x.SetMainSourceFile( "bldDevelopermenu.cs");
      x.LoadVariables( args);
      return x.CompileAll( );
   }

   public override ItemCollection GetSortedBuildList( )
   {
      ItemCollection sc = new ItemCollection() ;
      sc.Add( @"bin\GeneXus.Programs.Common.dll", cs_path + @"\genexus.programs.common.rsp");
      return sc ;
   }

   public override TargetCollection GetRuntimeBuildList( )
   {
      TargetCollection sc = new TargetCollection() ;
      sc.Add( @"appmasterpage", "dll");
      sc.Add( @"recentlinks", "dll");
      sc.Add( @"promptmasterpage", "dll");
      sc.Add( @"rwdmasterpage", "dll");
      sc.Add( @"rwdrecentlinks", "dll");
      sc.Add( @"rwdpromptmasterpage", "dll");
      sc.Add( @"gx0020", "dll");
      sc.Add( @"gx0090", "dll");
      sc.Add( @"gx0010", "dll");
      sc.Add( @"gx0030", "dll");
      sc.Add( @"gx0040", "dll");
      sc.Add( @"gx0070", "dll");
      sc.Add( @"gx00b0", "dll");
      sc.Add( @"gx00f0", "dll");
      sc.Add( @"gx00j0", "dll");
      sc.Add( @"gx00d0", "dll");
      sc.Add( @"gx0050", "dll");
      sc.Add( @"gx0060", "dll");
      sc.Add( @"gx0080", "dll");
      sc.Add( @"gx00g0", "dll");
      sc.Add( @"gx00p0", "dll");
      sc.Add( @"gx00a0", "dll");
      sc.Add( @"gx00c0", "dll");
      sc.Add( @"gx00e0", "dll");
      sc.Add( @"gx00m0", "dll");
      sc.Add( @"gx00h0", "dll");
      sc.Add( @"gx00l0", "dll");
      sc.Add( @"gx00i0", "dll");
      sc.Add( @"gx00k0", "dll");
      sc.Add( @"gx00n0", "dll");
      sc.Add( @"gx00o0", "dll");
      sc.Add( @"home", "dll");
      sc.Add( @"home", "dll");
      sc.Add( @"notauthorized", "dll");
      sc.Add( @"tabbedview", "dll");
      sc.Add( @"viewprograma", "dll");
      sc.Add( @"wwprograma", "dll");
      sc.Add( @"programageneral", "dll");
      sc.Add( @"programaproyectowc", "dll");
      sc.Add( @"viewproyecto", "dll");
      sc.Add( @"wwproyecto", "dll");
      sc.Add( @"proyectogeneral", "dll");
      sc.Add( @"proyectoindicadorwc", "dll");
      sc.Add( @"proyectoproyecto_comunidadwc", "dll");
      sc.Add( @"proyectoplanwc", "dll");
      sc.Add( @"proyectoproyectopaiswc", "dll");
      sc.Add( @"gx00q1", "dll");
      sc.Add( @"viewindicador", "dll");
      sc.Add( @"wwindicador", "dll");
      sc.Add( @"indicadorgeneral", "dll");
      sc.Add( @"indicadoractividadwc", "dll");
      sc.Add( @"ucantidadparticipantes", "dll");
      sc.Add( @"ucantidadactividad", "dll");
      sc.Add( @"indactividadparaplan", "dll");
      sc.Add( @"indfyactividad", "dll");
      sc.Add( @"viewplan", "dll");
      sc.Add( @"wwplan", "dll");
      sc.Add( @"plangeneral", "dll");
      sc.Add( @"planinforme_actividadwc", "dll");
      sc.Add( @"viewinforme_actividad", "dll");
      sc.Add( @"wwinforme_actividad", "dll");
      sc.Add( @"informe_actividadgeneral", "dll");
      sc.Add( @"informe_actividadcontacto_informe_actividadwc", "dll");
      sc.Add( @"informe_actividadfotowc", "dll");
      sc.Add( @"informe_actividadbeneficario_informe_actividadwc", "dll");
      sc.Add( @"viewactividad", "dll");
      sc.Add( @"wwactividad", "dll");
      sc.Add( @"actividadgeneral", "dll");
      sc.Add( @"actividadfyactividadwc", "dll");
      sc.Add( @"actividadactividadparaplanwc", "dll");
      sc.Add( @"actividadplanwc", "dll");
      sc.Add( @"viewpersona", "dll");
      sc.Add( @"wwpersona", "dll");
      sc.Add( @"personageneral", "dll");
      sc.Add( @"personabeneficiariowc", "dll");
      sc.Add( @"personaresponsablewc", "dll");
      sc.Add( @"viewusuario", "dll");
      sc.Add( @"wwusuario", "dll");
      sc.Add( @"usuariogeneral", "dll");
      sc.Add( @"webpanelmenuprincipal", "dll");
      sc.Add( @"contacto_informe_actividad", "dll");
      sc.Add( @"actividad", "dll");
      sc.Add( @"beneficiario", "dll");
      sc.Add( @"persona", "dll");
      sc.Add( @"programa", "dll");
      sc.Add( @"fyactividad", "dll");
      sc.Add( @"informe_actividad", "dll");
      sc.Add( @"puesto", "dll");
      sc.Add( @"indicador", "dll");
      sc.Add( @"foto", "dll");
      sc.Add( @"tipoasistente", "dll");
      sc.Add( @"usuario", "dll");
      sc.Add( @"pais", "dll");
      sc.Add( @"actividadparaplan", "dll");
      sc.Add( @"comunidad", "dll");
      sc.Add( @"plan", "dll");
      sc.Add( @"estado", "dll");
      sc.Add( @"fyactividadplancantpart", "dll");
      sc.Add( @"contacto", "dll");
      sc.Add( @"proyecto_comunidad", "dll");
      sc.Add( @"tipobeneficiario", "dll");
      sc.Add( @"responsable", "dll");
      sc.Add( @"fyactividadplancantact", "dll");
      sc.Add( @"beneficario_informe_actividad", "dll");
      sc.Add( @"proyecto", "dll");
      return sc ;
   }

   public override ItemCollection GetResBuildList( )
   {
      ItemCollection sc = new ItemCollection() ;
      sc.Add( @"bin\messages.eng.dll", cs_path + @"\messages.eng.txt");
      return sc ;
   }

   public override bool ToBuild( String obj )
   {
      if (checkTime(obj, cs_path + @"\bin\GxClasses.dll" ))
         return true;
      if ( obj == @"bin\GeneXus.Programs.Common.dll" )
      {
         if (checkTime(obj, cs_path + @"\GxWebStd.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\SoapParm.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\GxObjectCollection.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\GxFullTextSearchReindexer.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\GxModelInfoProvider.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\genexus.programs.sdt.rsp" ))
            return true;
         if (checkTime(obj, cs_path + @"\gxdomainpage.cs" ))
            return true;
      }
      if ( obj == @"bin\messages.eng.dll" )
      {
         if (checkTime(obj, cs_path + @"\messages.eng.txt" ))
            return true;
      }
      return false ;
   }

}

