/*
               File: Contacto
        Description: Contacto
             Author: GeneXus C# Generator version 16_0_0-127771
       Generated on: 1/3/2019 18:1:42.48
       Program type: Callable routine
          Main DBMS: SQL Server
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Xml.Serialization;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class contacto : GXDataArea, System.Web.SessionState.IRequiresSessionState
   {
      protected void INITENV( )
      {
         if ( GxWebError != 0 )
         {
            return  ;
         }
      }

      protected void INITTRN( )
      {
         initialize_properties( ) ;
         entryPointCalled = false;
         gxfirstwebparm = GetNextPar( );
         gxfirstwebparm_bkp = gxfirstwebparm;
         gxfirstwebparm = DecryptAjaxCall( gxfirstwebparm);
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         if ( StringUtil.StrCmp(gxfirstwebparm, "dyncall") == 0 )
         {
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            dyncall( GetNextPar( )) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxEvt") == 0 )
         {
            setAjaxEventMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxfirstwebparm = GetNextPar( );
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxfullajaxEvt") == 0 )
         {
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxfirstwebparm = GetNextPar( );
         }
         else
         {
            if ( ! IsValidAjaxCall( false) )
            {
               GxWebError = 1;
               return  ;
            }
            gxfirstwebparm = gxfirstwebparm_bkp;
         }
         if ( context.isSpaRequest( ) )
         {
            enableJsOutput();
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.GetCookie( "GX_SESSION_ID"))) )
         {
            gxcookieaux = context.SetCookie( "GX_SESSION_ID", Encrypt64( Crypto.GetEncryptionKey( ), Crypto.GetServerKey( )), "", (DateTime)(DateTime.MinValue), "", 0);
         }
         GXKey = Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
         toggleJsOutput = isJsOutputEnabled( );
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         init_web_controls( ) ;
         if ( toggleJsOutput )
         {
            if ( context.isSpaRequest( ) )
            {
               enableJsOutput();
            }
         }
         if ( ! context.isSpaRequest( ) )
         {
            Form.Meta.addItem("generator", "GeneXus C# 16_0_0-127771", 0) ;
            Form.Meta.addItem("description", "Contacto", 0) ;
         }
         context.wjLoc = "";
         context.nUserReturn = 0;
         context.wbHandled = 0;
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
         }
         GX_FocusControl = edtContactoid_Internalname;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         wbErr = false;
         context.SetDefaultTheme("Carmine");
         if ( ! context.IsLocalStorageSupported( ) )
         {
            context.PushCurrentUrl();
         }
      }

      public contacto( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsDataStore1 = context.GetDataStore("DataStore1");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("Carmine");
      }

      public contacto( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsDataStore1 = context.GetDataStore("DataStore1");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( )
      {
         executePrivate();
      }

      void executePrivate( )
      {
         isStatic = false;
         webExecute();
      }

      protected override void createObjects( )
      {
      }

      public override void webExecute( )
      {
         if ( initialized == 0 )
         {
            createObjects();
            initialize();
         }
         INITENV( ) ;
         INITTRN( ) ;
         if ( ( GxWebError == 0 ) && ! isAjaxCallMode( ) )
         {
            MasterPageObj = (GXMasterPage) ClassLoader.GetInstance("rwdmasterpage", "GeneXus.Programs.rwdmasterpage", new Object[] {new GxContext( context.handle, context.DataStores, context.HttpContext)});
            MasterPageObj.setDataArea(this,false);
            ValidateSpaRequest();
            MasterPageObj.webExecute();
            if ( ( GxWebError == 0 ) && context.isAjaxRequest( ) )
            {
               enableOutput();
               if ( ! context.isAjaxRequest( ) )
               {
                  context.GX_webresponse.AppendHeader("Cache-Control", "no-store");
               }
               if ( ! context.WillRedirect( ) )
               {
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               }
               else
               {
                  if ( context.isAjaxRequest( ) )
                  {
                     disableOutput();
                  }
                  RenderHtmlHeaders( ) ;
                  context.Redirect( context.wjLoc );
                  context.DispatchAjaxCommands();
               }
            }
         }
         this.cleanup();
      }

      protected void fix_multi_value_controls( )
      {
      }

      protected void Draw( )
      {
         if ( context.isAjaxRequest( ) )
         {
            disableOutput();
         }
         if ( ! GxWebStd.gx_redirect( context) )
         {
            disable_std_buttons( ) ;
            enableDisable( ) ;
            set_caption( ) ;
            /* Form start */
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "Section", "left", "top", " "+"data-gx-base-lib=\"bootstrapv3\""+" "+"data-abstract-form"+" ", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, divMaintable_Internalname, 1, 0, "px", 0, "px", "WWAdvancedContainer", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12 col-sm-8 col-sm-offset-2", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, divTitlecontainer_Internalname, 1, 0, "px", 0, "px", "TableTop", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12", "left", "top", "", "", "div");
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTitle_Internalname, "Contacto", "", "", lblTitle_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "Title", 0, "", 1, 1, 0, "HLP_Contacto.htm");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12", "left", "top", "", "", "div");
            ClassString = "ErrorViewer";
            StyleString = "";
            GxWebStd.gx_msg_list( context, "", context.GX_msglist.DisplayMode, StyleString, ClassString, "", "false");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12 col-sm-8 col-sm-offset-2", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, divFormcontainer_Internalname, 1, 0, "px", 0, "px", "FormContainer", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, divToolbarcell_Internalname, 1, 0, "px", 0, "px", "col-xs-12 col-sm-9 col-sm-offset-3 ToolbarCellClass", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "gx-action-group ActionGroup", "left", "top", " "+"data-gx-actiongroup-type=\"toolbar\""+" ", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "btn-group", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "gx-button", "left", "top", "", "", "div");
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 21,'',false,'',0)\"";
            ClassString = "BtnFirst";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtn_first_Internalname, "", "", bttBtn_first_Jsonclick, 5, "", "", StyleString, ClassString, bttBtn_first_Visible, 1, "standard", "'"+""+"'"+",false,"+"'"+"EFIRST."+"'", TempTags, "", context.GetButtonType( ), "HLP_Contacto.htm");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "gx-button", "left", "top", "", "", "div");
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 23,'',false,'',0)\"";
            ClassString = "BtnPrevious";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtn_previous_Internalname, "", "", bttBtn_previous_Jsonclick, 5, "", "", StyleString, ClassString, bttBtn_previous_Visible, 1, "standard", "'"+""+"'"+",false,"+"'"+"EPREVIOUS."+"'", TempTags, "", context.GetButtonType( ), "HLP_Contacto.htm");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "gx-button", "left", "top", "", "", "div");
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 25,'',false,'',0)\"";
            ClassString = "BtnNext";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtn_next_Internalname, "", "", bttBtn_next_Jsonclick, 5, "", "", StyleString, ClassString, bttBtn_next_Visible, 1, "standard", "'"+""+"'"+",false,"+"'"+"ENEXT."+"'", TempTags, "", context.GetButtonType( ), "HLP_Contacto.htm");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "gx-button", "left", "top", "", "", "div");
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 27,'',false,'',0)\"";
            ClassString = "BtnLast";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtn_last_Internalname, "", "", bttBtn_last_Jsonclick, 5, "", "", StyleString, ClassString, bttBtn_last_Visible, 1, "standard", "'"+""+"'"+",false,"+"'"+"ELAST."+"'", TempTags, "", context.GetButtonType( ), "HLP_Contacto.htm");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "gx-button", "left", "top", "", "", "div");
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 29,'',false,'',0)\"";
            ClassString = "BtnSelect";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtn_select_Internalname, "", "Select", bttBtn_select_Jsonclick, 4, "Select", "", StyleString, ClassString, bttBtn_select_Visible, 1, "standard", "'"+""+"'"+",false,"+"'"+"ESELECT."+"'", TempTags, "gx.popup.openPrompt('"+"gx00j0.aspx"+"',["+"{Ctrl:gx.dom.el('"+"CONTACTOID"+"'), id:'"+"CONTACTOID"+"'"+",IOType:'out',isKey:true,isLastKey:true}"+"],"+"null"+","+"'', false"+","+"true"+");"+"return false;", 2, "HLP_Contacto.htm");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12 FormCellAdvanced", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "form-group gx-form-group", "left", "top", ""+" data-gx-for=\""+edtContactoid_Internalname+"\"", "", "div");
            /* Attribute/Variable Label */
            GxWebStd.gx_label_element( context, edtContactoid_Internalname, "id Contacto", "col-sm-3 AttributeLabel", 1, true);
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-sm-9 gx-attribute", "left", "top", "", "", "div");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 34,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtContactoid_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A18Contactoid), 9, 0, ".", "")), ((edtContactoid_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( (decimal)(A18Contactoid), "ZZZZZZZZ9")) : context.localUtil.Format( (decimal)(A18Contactoid), "ZZZZZZZZ9")), TempTags+" onchange=\""+"gx.num.valid_integer( this,',');"+";gx.evt.onchange(this, event)\" "+" onblur=\""+";gx.evt.onblur(this,34);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContactoid_Jsonclick, 0, "Attribute", "", "", "", "", 1, edtContactoid_Enabled, 0, "number", "1", 9, "chr", 1, "row", 9, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_Contacto.htm");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12 FormCell", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "form-group gx-form-group", "left", "top", ""+" data-gx-for=\""+edtprimerNombre_Internalname+"\"", "", "div");
            /* Attribute/Variable Label */
            GxWebStd.gx_label_element( context, edtprimerNombre_Internalname, "primer Nombre", "col-sm-3 AttributeLabel", 1, true);
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-sm-9 gx-attribute", "left", "top", "", "", "div");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 39,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtprimerNombre_Internalname, A124primerNombre, StringUtil.RTrim( context.localUtil.Format( A124primerNombre, "")), TempTags+" onchange=\""+""+";gx.evt.onchange(this, event)\" "+" onblur=\""+";gx.evt.onblur(this,39);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtprimerNombre_Jsonclick, 0, "Attribute", "", "", "", "", 1, edtprimerNombre_Enabled, 0, "text", "", 20, "chr", 1, "row", 20, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_Contacto.htm");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12 FormCell", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "form-group gx-form-group", "left", "top", ""+" data-gx-for=\""+edtsegundoNombre_Internalname+"\"", "", "div");
            /* Attribute/Variable Label */
            GxWebStd.gx_label_element( context, edtsegundoNombre_Internalname, "segundo Nombre", "col-sm-3 AttributeLabel", 1, true);
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-sm-9 gx-attribute", "left", "top", "", "", "div");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 44,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtsegundoNombre_Internalname, A125segundoNombre, StringUtil.RTrim( context.localUtil.Format( A125segundoNombre, "")), TempTags+" onchange=\""+""+";gx.evt.onchange(this, event)\" "+" onblur=\""+";gx.evt.onblur(this,44);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtsegundoNombre_Jsonclick, 0, "Attribute", "", "", "", "", 1, edtsegundoNombre_Enabled, 0, "text", "", 20, "chr", 1, "row", 20, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_Contacto.htm");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12 FormCell", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "form-group gx-form-group", "left", "top", ""+" data-gx-for=\""+edtContactoapellidoPaterno_Internalname+"\"", "", "div");
            /* Attribute/Variable Label */
            GxWebStd.gx_label_element( context, edtContactoapellidoPaterno_Internalname, "apellido Paterno", "col-sm-3 AttributeLabel", 1, true);
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-sm-9 gx-attribute", "left", "top", "", "", "div");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 49,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtContactoapellidoPaterno_Internalname, A126ContactoapellidoPaterno, StringUtil.RTrim( context.localUtil.Format( A126ContactoapellidoPaterno, "")), TempTags+" onchange=\""+""+";gx.evt.onchange(this, event)\" "+" onblur=\""+";gx.evt.onblur(this,49);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContactoapellidoPaterno_Jsonclick, 0, "Attribute", "", "", "", "", 1, edtContactoapellidoPaterno_Enabled, 0, "text", "", 20, "chr", 1, "row", 20, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_Contacto.htm");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12 FormCell", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "form-group gx-form-group", "left", "top", ""+" data-gx-for=\""+edtContactoapellidoMaterno_Internalname+"\"", "", "div");
            /* Attribute/Variable Label */
            GxWebStd.gx_label_element( context, edtContactoapellidoMaterno_Internalname, "apellido Materno", "col-sm-3 AttributeLabel", 1, true);
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-sm-9 gx-attribute", "left", "top", "", "", "div");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 54,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtContactoapellidoMaterno_Internalname, A127ContactoapellidoMaterno, StringUtil.RTrim( context.localUtil.Format( A127ContactoapellidoMaterno, "")), TempTags+" onchange=\""+""+";gx.evt.onchange(this, event)\" "+" onblur=\""+";gx.evt.onblur(this,54);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContactoapellidoMaterno_Jsonclick, 0, "Attribute", "", "", "", "", 1, edtContactoapellidoMaterno_Enabled, 0, "text", "", 20, "chr", 1, "row", 20, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_Contacto.htm");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12 FormCell", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "form-group gx-form-group", "left", "top", ""+" data-gx-for=\""+edtnumero_Internalname+"\"", "", "div");
            /* Attribute/Variable Label */
            GxWebStd.gx_label_element( context, edtnumero_Internalname, "numero", "col-sm-3 AttributeLabel", 1, true);
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-sm-9 gx-attribute", "left", "top", "", "", "div");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 59,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtnumero_Internalname, A128numero, StringUtil.RTrim( context.localUtil.Format( A128numero, "")), TempTags+" onchange=\""+""+";gx.evt.onchange(this, event)\" "+" onblur=\""+";gx.evt.onblur(this,59);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtnumero_Jsonclick, 0, "Attribute", "", "", "", "", 1, edtnumero_Enabled, 0, "text", "", 30, "chr", 1, "row", 30, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_Contacto.htm");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12 FormCell", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "form-group gx-form-group", "left", "top", ""+" data-gx-for=\""+edtContactocorreo_Internalname+"\"", "", "div");
            /* Attribute/Variable Label */
            GxWebStd.gx_label_element( context, edtContactocorreo_Internalname, "correo", "col-sm-3 AttributeLabel", 1, true);
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-sm-9 gx-attribute", "left", "top", "", "", "div");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 64,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtContactocorreo_Internalname, A129Contactocorreo, StringUtil.RTrim( context.localUtil.Format( A129Contactocorreo, "")), TempTags+" onchange=\""+""+";gx.evt.onchange(this, event)\" "+" onblur=\""+";gx.evt.onblur(this,64);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContactocorreo_Jsonclick, 0, "Attribute", "", "", "", "", 1, edtContactocorreo_Enabled, 0, "text", "", 80, "chr", 1, "row", 100, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_Contacto.htm");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12", "Center", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "gx-action-group Confirm", "left", "top", " "+"data-gx-actiongroup-type=\"toolbar\""+" ", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "gx-button", "left", "top", "", "", "div");
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 69,'',false,'',0)\"";
            ClassString = "BtnEnter";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtn_enter_Internalname, "", "Confirm", bttBtn_enter_Jsonclick, 5, "Confirm", "", StyleString, ClassString, bttBtn_enter_Visible, bttBtn_enter_Enabled, "standard", "'"+""+"'"+",false,"+"'"+"EENTER."+"'", TempTags, "", context.GetButtonType( ), "HLP_Contacto.htm");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "gx-button", "left", "top", "", "", "div");
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 71,'',false,'',0)\"";
            ClassString = "BtnCancel";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtn_cancel_Internalname, "", "Cancel", bttBtn_cancel_Jsonclick, 1, "Cancel", "", StyleString, ClassString, bttBtn_cancel_Visible, 1, "standard", "'"+""+"'"+",false,"+"'"+"ECANCEL."+"'", TempTags, "", context.GetButtonType( ), "HLP_Contacto.htm");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "gx-button", "left", "top", "", "", "div");
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 73,'',false,'',0)\"";
            ClassString = "BtnDelete";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtn_delete_Internalname, "", "Delete", bttBtn_delete_Jsonclick, 5, "Delete", "", StyleString, ClassString, bttBtn_delete_Visible, bttBtn_delete_Enabled, "standard", "'"+""+"'"+",false,"+"'"+"EDELETE."+"'", TempTags, "", context.GetButtonType( ), "HLP_Contacto.htm");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "Center", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            fix_multi_value_controls( ) ;
         }
         /* Execute Exit event if defined. */
      }

      protected void UserMain( )
      {
         standaloneStartup( ) ;
      }

      protected void UserMainFullajax( )
      {
         INITENV( ) ;
         INITTRN( ) ;
         UserMain( ) ;
         Draw( ) ;
         SendCloseFormHiddens( ) ;
      }

      protected void standaloneStartup( )
      {
         standaloneStartupServer( ) ;
         disable_std_buttons( ) ;
         enableDisable( ) ;
         Process( ) ;
      }

      protected void standaloneStartupServer( )
      {
         /* Execute Start event if defined. */
         context.wbGlbDoneStart = 0;
         context.wbGlbDoneStart = 1;
         assign_properties_default( ) ;
         if ( AnyError == 0 )
         {
            if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
            {
               /* Read saved SDTs. */
               /* Read variables values. */
               if ( ( ( context.localUtil.CToN( cgiGet( edtContactoid_Internalname), ".", ",") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtContactoid_Internalname), ".", ",") > Convert.ToDecimal( 999999999 )) ) )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "CONTACTOID");
                  AnyError = 1;
                  GX_FocusControl = edtContactoid_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  wbErr = true;
                  A18Contactoid = 0;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A18Contactoid", StringUtil.LTrim( StringUtil.Str( (decimal)(A18Contactoid), 9, 0)));
               }
               else
               {
                  A18Contactoid = (int)(context.localUtil.CToN( cgiGet( edtContactoid_Internalname), ".", ","));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A18Contactoid", StringUtil.LTrim( StringUtil.Str( (decimal)(A18Contactoid), 9, 0)));
               }
               A124primerNombre = cgiGet( edtprimerNombre_Internalname);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A124primerNombre", A124primerNombre);
               A125segundoNombre = cgiGet( edtsegundoNombre_Internalname);
               n125segundoNombre = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A125segundoNombre", A125segundoNombre);
               n125segundoNombre = (String.IsNullOrEmpty(StringUtil.RTrim( A125segundoNombre)) ? true : false);
               A126ContactoapellidoPaterno = cgiGet( edtContactoapellidoPaterno_Internalname);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A126ContactoapellidoPaterno", A126ContactoapellidoPaterno);
               A127ContactoapellidoMaterno = cgiGet( edtContactoapellidoMaterno_Internalname);
               n127ContactoapellidoMaterno = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A127ContactoapellidoMaterno", A127ContactoapellidoMaterno);
               n127ContactoapellidoMaterno = (String.IsNullOrEmpty(StringUtil.RTrim( A127ContactoapellidoMaterno)) ? true : false);
               A128numero = cgiGet( edtnumero_Internalname);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A128numero", A128numero);
               A129Contactocorreo = cgiGet( edtContactocorreo_Internalname);
               n129Contactocorreo = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A129Contactocorreo", A129Contactocorreo);
               n129Contactocorreo = (String.IsNullOrEmpty(StringUtil.RTrim( A129Contactocorreo)) ? true : false);
               /* Read saved values. */
               Z18Contactoid = (int)(context.localUtil.CToN( cgiGet( "Z18Contactoid"), ".", ","));
               Z124primerNombre = cgiGet( "Z124primerNombre");
               Z125segundoNombre = cgiGet( "Z125segundoNombre");
               n125segundoNombre = (String.IsNullOrEmpty(StringUtil.RTrim( A125segundoNombre)) ? true : false);
               Z126ContactoapellidoPaterno = cgiGet( "Z126ContactoapellidoPaterno");
               Z127ContactoapellidoMaterno = cgiGet( "Z127ContactoapellidoMaterno");
               n127ContactoapellidoMaterno = (String.IsNullOrEmpty(StringUtil.RTrim( A127ContactoapellidoMaterno)) ? true : false);
               Z128numero = cgiGet( "Z128numero");
               Z129Contactocorreo = cgiGet( "Z129Contactocorreo");
               n129Contactocorreo = (String.IsNullOrEmpty(StringUtil.RTrim( A129Contactocorreo)) ? true : false);
               IsConfirmed = (short)(context.localUtil.CToN( cgiGet( "IsConfirmed"), ".", ","));
               IsModified = (short)(context.localUtil.CToN( cgiGet( "IsModified"), ".", ","));
               Gx_mode = cgiGet( "Mode");
               Gx_mode = cgiGet( "vMODE");
               /* Read subfile selected row values. */
               /* Read hidden variables. */
               GXKey = Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
               standaloneNotModal( ) ;
            }
            else
            {
               standaloneNotModal( ) ;
               if ( StringUtil.StrCmp(gxfirstwebparm, "viewer") == 0 )
               {
                  Gx_mode = "DSP";
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                  A18Contactoid = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A18Contactoid", StringUtil.LTrim( StringUtil.Str( (decimal)(A18Contactoid), 9, 0)));
                  getEqualNoModal( ) ;
                  Gx_mode = "DSP";
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                  disable_std_buttons_dsp( ) ;
                  standaloneModal( ) ;
               }
               else
               {
                  Gx_mode = "INS";
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                  standaloneModal( ) ;
               }
            }
         }
      }

      protected void Process( )
      {
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            /* Read Transaction buttons. */
            sEvt = cgiGet( "_EventName");
            EvtGridId = cgiGet( "_EventGridId");
            EvtRowId = cgiGet( "_EventRowId");
            if ( StringUtil.Len( sEvt) > 0 )
            {
               sEvtType = StringUtil.Left( sEvt, 1);
               sEvt = StringUtil.Right( sEvt, (short)(StringUtil.Len( sEvt)-1));
               if ( StringUtil.StrCmp(sEvtType, "M") != 0 )
               {
                  if ( StringUtil.StrCmp(sEvtType, "E") == 0 )
                  {
                     sEvtType = StringUtil.Right( sEvt, 1);
                     if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                     {
                        sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                        if ( StringUtil.StrCmp(sEvt, "ENTER") == 0 )
                        {
                           context.wbHandled = 1;
                           btn_enter( ) ;
                           /* No code required for Cancel button. It is implemented as the Reset button. */
                        }
                        else if ( StringUtil.StrCmp(sEvt, "FIRST") == 0 )
                        {
                           context.wbHandled = 1;
                           btn_first( ) ;
                        }
                        else if ( StringUtil.StrCmp(sEvt, "PREVIOUS") == 0 )
                        {
                           context.wbHandled = 1;
                           btn_previous( ) ;
                        }
                        else if ( StringUtil.StrCmp(sEvt, "NEXT") == 0 )
                        {
                           context.wbHandled = 1;
                           btn_next( ) ;
                        }
                        else if ( StringUtil.StrCmp(sEvt, "LAST") == 0 )
                        {
                           context.wbHandled = 1;
                           btn_last( ) ;
                        }
                        else if ( StringUtil.StrCmp(sEvt, "SELECT") == 0 )
                        {
                           context.wbHandled = 1;
                           btn_select( ) ;
                        }
                        else if ( StringUtil.StrCmp(sEvt, "DELETE") == 0 )
                        {
                           context.wbHandled = 1;
                           btn_delete( ) ;
                        }
                        else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                        {
                           context.wbHandled = 1;
                           AfterKeyLoadScreen( ) ;
                        }
                     }
                     else
                     {
                     }
                  }
                  context.wbHandled = 1;
               }
            }
         }
      }

      protected void AfterTrn( )
      {
         if ( trnEnded == 1 )
         {
            trnEnded = 0;
            standaloneNotModal( ) ;
            standaloneModal( ) ;
            if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  )
            {
               /* Clear variables for new insertion. */
               InitAll0J19( ) ;
               standaloneNotModal( ) ;
               standaloneModal( ) ;
            }
         }
      }

      public override String ToString( )
      {
         return "" ;
      }

      public GxContentInfo GetContentInfo( )
      {
         return (GxContentInfo)(null) ;
      }

      protected void disable_std_buttons( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )
         {
            bttBtn_delete_Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_delete_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_delete_Enabled), 5, 0)), true);
         }
      }

      protected void disable_std_buttons_dsp( )
      {
         bttBtn_delete_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_delete_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_delete_Visible), 5, 0)), true);
         bttBtn_first_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_first_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_first_Visible), 5, 0)), true);
         bttBtn_previous_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_previous_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_previous_Visible), 5, 0)), true);
         bttBtn_next_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_next_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_next_Visible), 5, 0)), true);
         bttBtn_last_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_last_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_last_Visible), 5, 0)), true);
         bttBtn_select_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_select_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_select_Visible), 5, 0)), true);
         bttBtn_delete_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_delete_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_delete_Visible), 5, 0)), true);
         if ( StringUtil.StrCmp(Gx_mode, "DSP") == 0 )
         {
            bttBtn_enter_Visible = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_enter_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_enter_Visible), 5, 0)), true);
         }
         DisableAttributes0J19( ) ;
      }

      protected void set_caption( )
      {
         if ( ( IsConfirmed == 1 ) && ( AnyError == 0 ) )
         {
            if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_confdelete", ""), 0, "", true);
            }
            else
            {
               GX_msglist.addItem(context.GetMessage( "GXM_mustconfirm", ""), 0, "", true);
            }
         }
      }

      protected void ResetCaption0J0( )
      {
      }

      protected void ZM0J19( short GX_JID )
      {
         if ( ( GX_JID == 1 ) || ( GX_JID == 0 ) )
         {
            if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
            {
               Z124primerNombre = T000J3_A124primerNombre[0];
               Z125segundoNombre = T000J3_A125segundoNombre[0];
               Z126ContactoapellidoPaterno = T000J3_A126ContactoapellidoPaterno[0];
               Z127ContactoapellidoMaterno = T000J3_A127ContactoapellidoMaterno[0];
               Z128numero = T000J3_A128numero[0];
               Z129Contactocorreo = T000J3_A129Contactocorreo[0];
            }
            else
            {
               Z124primerNombre = A124primerNombre;
               Z125segundoNombre = A125segundoNombre;
               Z126ContactoapellidoPaterno = A126ContactoapellidoPaterno;
               Z127ContactoapellidoMaterno = A127ContactoapellidoMaterno;
               Z128numero = A128numero;
               Z129Contactocorreo = A129Contactocorreo;
            }
         }
         if ( GX_JID == -1 )
         {
            Z18Contactoid = A18Contactoid;
            Z124primerNombre = A124primerNombre;
            Z125segundoNombre = A125segundoNombre;
            Z126ContactoapellidoPaterno = A126ContactoapellidoPaterno;
            Z127ContactoapellidoMaterno = A127ContactoapellidoMaterno;
            Z128numero = A128numero;
            Z129Contactocorreo = A129Contactocorreo;
         }
      }

      protected void standaloneNotModal( )
      {
      }

      protected void standaloneModal( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )
         {
            bttBtn_delete_Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_delete_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_delete_Enabled), 5, 0)), true);
         }
         else
         {
            bttBtn_delete_Enabled = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_delete_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_delete_Enabled), 5, 0)), true);
         }
         if ( StringUtil.StrCmp(Gx_mode, "DSP") == 0 )
         {
            bttBtn_enter_Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_enter_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_enter_Enabled), 5, 0)), true);
         }
         else
         {
            bttBtn_enter_Enabled = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_enter_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_enter_Enabled), 5, 0)), true);
         }
      }

      protected void Load0J19( )
      {
         /* Using cursor T000J4 */
         pr_datastore1.execute(2, new Object[] {A18Contactoid});
         if ( (pr_datastore1.getStatus(2) != 101) )
         {
            RcdFound19 = 1;
            A124primerNombre = T000J4_A124primerNombre[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A124primerNombre", A124primerNombre);
            A125segundoNombre = T000J4_A125segundoNombre[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A125segundoNombre", A125segundoNombre);
            n125segundoNombre = T000J4_n125segundoNombre[0];
            A126ContactoapellidoPaterno = T000J4_A126ContactoapellidoPaterno[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A126ContactoapellidoPaterno", A126ContactoapellidoPaterno);
            A127ContactoapellidoMaterno = T000J4_A127ContactoapellidoMaterno[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A127ContactoapellidoMaterno", A127ContactoapellidoMaterno);
            n127ContactoapellidoMaterno = T000J4_n127ContactoapellidoMaterno[0];
            A128numero = T000J4_A128numero[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A128numero", A128numero);
            A129Contactocorreo = T000J4_A129Contactocorreo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A129Contactocorreo", A129Contactocorreo);
            n129Contactocorreo = T000J4_n129Contactocorreo[0];
            ZM0J19( -1) ;
         }
         pr_datastore1.close(2);
         OnLoadActions0J19( ) ;
      }

      protected void OnLoadActions0J19( )
      {
      }

      protected void CheckExtendedTable0J19( )
      {
         Gx_BScreen = 1;
         standaloneModal( ) ;
      }

      protected void CloseExtendedTableCursors0J19( )
      {
      }

      protected void enableDisable( )
      {
      }

      protected void GetKey0J19( )
      {
         /* Using cursor T000J5 */
         pr_datastore1.execute(3, new Object[] {A18Contactoid});
         if ( (pr_datastore1.getStatus(3) != 101) )
         {
            RcdFound19 = 1;
         }
         else
         {
            RcdFound19 = 0;
         }
         pr_datastore1.close(3);
      }

      protected void getByPrimaryKey( )
      {
         /* Using cursor T000J3 */
         pr_datastore1.execute(1, new Object[] {A18Contactoid});
         if ( (pr_datastore1.getStatus(1) != 101) )
         {
            ZM0J19( 1) ;
            RcdFound19 = 1;
            A18Contactoid = T000J3_A18Contactoid[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A18Contactoid", StringUtil.LTrim( StringUtil.Str( (decimal)(A18Contactoid), 9, 0)));
            A124primerNombre = T000J3_A124primerNombre[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A124primerNombre", A124primerNombre);
            A125segundoNombre = T000J3_A125segundoNombre[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A125segundoNombre", A125segundoNombre);
            n125segundoNombre = T000J3_n125segundoNombre[0];
            A126ContactoapellidoPaterno = T000J3_A126ContactoapellidoPaterno[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A126ContactoapellidoPaterno", A126ContactoapellidoPaterno);
            A127ContactoapellidoMaterno = T000J3_A127ContactoapellidoMaterno[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A127ContactoapellidoMaterno", A127ContactoapellidoMaterno);
            n127ContactoapellidoMaterno = T000J3_n127ContactoapellidoMaterno[0];
            A128numero = T000J3_A128numero[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A128numero", A128numero);
            A129Contactocorreo = T000J3_A129Contactocorreo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A129Contactocorreo", A129Contactocorreo);
            n129Contactocorreo = T000J3_n129Contactocorreo[0];
            Z18Contactoid = A18Contactoid;
            sMode19 = Gx_mode;
            Gx_mode = "DSP";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            standaloneModal( ) ;
            Load0J19( ) ;
            if ( AnyError == 1 )
            {
               RcdFound19 = 0;
               InitializeNonKey0J19( ) ;
            }
            Gx_mode = sMode19;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         }
         else
         {
            RcdFound19 = 0;
            InitializeNonKey0J19( ) ;
            sMode19 = Gx_mode;
            Gx_mode = "DSP";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            standaloneModal( ) ;
            Gx_mode = sMode19;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         }
         pr_datastore1.close(1);
      }

      protected void getEqualNoModal( )
      {
         GetKey0J19( ) ;
         if ( RcdFound19 == 0 )
         {
            Gx_mode = "INS";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         }
         else
         {
            Gx_mode = "UPD";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         }
         getByPrimaryKey( ) ;
      }

      protected void move_next( )
      {
         RcdFound19 = 0;
         /* Using cursor T000J6 */
         pr_datastore1.execute(4, new Object[] {A18Contactoid});
         if ( (pr_datastore1.getStatus(4) != 101) )
         {
            while ( (pr_datastore1.getStatus(4) != 101) && ( ( T000J6_A18Contactoid[0] < A18Contactoid ) ) )
            {
               pr_datastore1.readNext(4);
            }
            if ( (pr_datastore1.getStatus(4) != 101) && ( ( T000J6_A18Contactoid[0] > A18Contactoid ) ) )
            {
               A18Contactoid = T000J6_A18Contactoid[0];
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A18Contactoid", StringUtil.LTrim( StringUtil.Str( (decimal)(A18Contactoid), 9, 0)));
               RcdFound19 = 1;
            }
         }
         pr_datastore1.close(4);
      }

      protected void move_previous( )
      {
         RcdFound19 = 0;
         /* Using cursor T000J7 */
         pr_datastore1.execute(5, new Object[] {A18Contactoid});
         if ( (pr_datastore1.getStatus(5) != 101) )
         {
            while ( (pr_datastore1.getStatus(5) != 101) && ( ( T000J7_A18Contactoid[0] > A18Contactoid ) ) )
            {
               pr_datastore1.readNext(5);
            }
            if ( (pr_datastore1.getStatus(5) != 101) && ( ( T000J7_A18Contactoid[0] < A18Contactoid ) ) )
            {
               A18Contactoid = T000J7_A18Contactoid[0];
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A18Contactoid", StringUtil.LTrim( StringUtil.Str( (decimal)(A18Contactoid), 9, 0)));
               RcdFound19 = 1;
            }
         }
         pr_datastore1.close(5);
      }

      protected void btn_enter( )
      {
         nKeyPressed = 1;
         GetKey0J19( ) ;
         if ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )
         {
            /* Insert record */
            GX_FocusControl = edtContactoid_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            Insert0J19( ) ;
            if ( AnyError == 1 )
            {
               GX_FocusControl = "";
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            }
         }
         else
         {
            if ( RcdFound19 == 1 )
            {
               if ( A18Contactoid != Z18Contactoid )
               {
                  A18Contactoid = Z18Contactoid;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A18Contactoid", StringUtil.LTrim( StringUtil.Str( (decimal)(A18Contactoid), 9, 0)));
                  GX_msglist.addItem(context.GetMessage( "GXM_getbeforeupd", ""), "CandidateKeyNotFound", 1, "CONTACTOID");
                  AnyError = 1;
                  GX_FocusControl = edtContactoid_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               }
               else if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
               {
                  delete( ) ;
                  AfterTrn( ) ;
                  GX_FocusControl = edtContactoid_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               }
               else
               {
                  Gx_mode = "UPD";
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                  /* Update record */
                  Update0J19( ) ;
                  GX_FocusControl = edtContactoid_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               }
            }
            else
            {
               if ( A18Contactoid != Z18Contactoid )
               {
                  Gx_mode = "INS";
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                  /* Insert record */
                  GX_FocusControl = edtContactoid_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  Insert0J19( ) ;
                  if ( AnyError == 1 )
                  {
                     GX_FocusControl = "";
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  }
               }
               else
               {
                  if ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 )
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_recdeleted", ""), 1, "CONTACTOID");
                     AnyError = 1;
                     GX_FocusControl = edtContactoid_Internalname;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  }
                  else
                  {
                     Gx_mode = "INS";
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                     /* Insert record */
                     GX_FocusControl = edtContactoid_Internalname;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                     Insert0J19( ) ;
                     if ( AnyError == 1 )
                     {
                        GX_FocusControl = "";
                        context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                     }
                  }
               }
            }
         }
         AfterTrn( ) ;
      }

      protected void btn_delete( )
      {
         if ( A18Contactoid != Z18Contactoid )
         {
            A18Contactoid = Z18Contactoid;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A18Contactoid", StringUtil.LTrim( StringUtil.Str( (decimal)(A18Contactoid), 9, 0)));
            GX_msglist.addItem(context.GetMessage( "GXM_getbeforedlt", ""), 1, "CONTACTOID");
            AnyError = 1;
            GX_FocusControl = edtContactoid_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         else
         {
            delete( ) ;
            AfterTrn( ) ;
            GX_FocusControl = edtContactoid_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         if ( AnyError != 0 )
         {
            Gx_mode = "UPD";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         }
         else
         {
            getByPrimaryKey( ) ;
         }
         CloseOpenCursors();
      }

      protected void btn_get( )
      {
         nKeyPressed = 2;
         IsConfirmed = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "IsConfirmed", StringUtil.LTrim( StringUtil.Str( (decimal)(IsConfirmed), 4, 0)));
         getEqualNoModal( ) ;
         if ( RcdFound19 == 0 )
         {
            GX_msglist.addItem(context.GetMessage( "GXM_keynfound", ""), "PrimaryKeyNotFound", 1, "CONTACTOID");
            AnyError = 1;
            GX_FocusControl = edtContactoid_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         GX_FocusControl = edtprimerNombre_Internalname;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         standaloneNotModal( ) ;
         standaloneModal( ) ;
      }

      protected void btn_first( )
      {
         nKeyPressed = 2;
         IsConfirmed = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "IsConfirmed", StringUtil.LTrim( StringUtil.Str( (decimal)(IsConfirmed), 4, 0)));
         ScanStart0J19( ) ;
         if ( RcdFound19 == 0 )
         {
            GX_msglist.addItem(context.GetMessage( "GXM_norectobrow", ""), 0, "", true);
         }
         else
         {
            Gx_mode = "UPD";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         }
         GX_FocusControl = edtprimerNombre_Internalname;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         ScanEnd0J19( ) ;
         getByPrimaryKey( ) ;
         standaloneNotModal( ) ;
         standaloneModal( ) ;
      }

      protected void btn_previous( )
      {
         nKeyPressed = 2;
         IsConfirmed = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "IsConfirmed", StringUtil.LTrim( StringUtil.Str( (decimal)(IsConfirmed), 4, 0)));
         move_previous( ) ;
         if ( RcdFound19 == 0 )
         {
            GX_msglist.addItem(context.GetMessage( "GXM_norectobrow", ""), 0, "", true);
         }
         else
         {
            Gx_mode = "UPD";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         }
         GX_FocusControl = edtprimerNombre_Internalname;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         getByPrimaryKey( ) ;
         standaloneNotModal( ) ;
         standaloneModal( ) ;
      }

      protected void btn_next( )
      {
         nKeyPressed = 2;
         IsConfirmed = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "IsConfirmed", StringUtil.LTrim( StringUtil.Str( (decimal)(IsConfirmed), 4, 0)));
         move_next( ) ;
         if ( RcdFound19 == 0 )
         {
            GX_msglist.addItem(context.GetMessage( "GXM_norectobrow", ""), 0, "", true);
         }
         else
         {
            Gx_mode = "UPD";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         }
         GX_FocusControl = edtprimerNombre_Internalname;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         getByPrimaryKey( ) ;
         standaloneNotModal( ) ;
         standaloneModal( ) ;
      }

      protected void btn_last( )
      {
         nKeyPressed = 2;
         IsConfirmed = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "IsConfirmed", StringUtil.LTrim( StringUtil.Str( (decimal)(IsConfirmed), 4, 0)));
         ScanStart0J19( ) ;
         if ( RcdFound19 == 0 )
         {
            GX_msglist.addItem(context.GetMessage( "GXM_norectobrow", ""), 0, "", true);
         }
         else
         {
            while ( RcdFound19 != 0 )
            {
               ScanNext0J19( ) ;
            }
            Gx_mode = "UPD";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         }
         GX_FocusControl = edtprimerNombre_Internalname;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         ScanEnd0J19( ) ;
         getByPrimaryKey( ) ;
         standaloneNotModal( ) ;
         standaloneModal( ) ;
      }

      protected void btn_select( )
      {
         getEqualNoModal( ) ;
      }

      protected void CheckOptimisticConcurrency0J19( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
         {
            /* Using cursor T000J2 */
            pr_datastore1.execute(0, new Object[] {A18Contactoid});
            if ( (pr_datastore1.getStatus(0) == 103) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_lock", new   object[]  {"CONTACTO"}), "RecordIsLocked", 1, "");
               AnyError = 1;
               return  ;
            }
            Gx_longc = false;
            if ( (pr_datastore1.getStatus(0) == 101) || ( StringUtil.StrCmp(Z124primerNombre, T000J2_A124primerNombre[0]) != 0 ) || ( StringUtil.StrCmp(Z125segundoNombre, T000J2_A125segundoNombre[0]) != 0 ) || ( StringUtil.StrCmp(Z126ContactoapellidoPaterno, T000J2_A126ContactoapellidoPaterno[0]) != 0 ) || ( StringUtil.StrCmp(Z127ContactoapellidoMaterno, T000J2_A127ContactoapellidoMaterno[0]) != 0 ) || ( StringUtil.StrCmp(Z128numero, T000J2_A128numero[0]) != 0 ) )
            {
               Gx_longc = true;
            }
            if ( Gx_longc || ( StringUtil.StrCmp(Z129Contactocorreo, T000J2_A129Contactocorreo[0]) != 0 ) )
            {
               if ( StringUtil.StrCmp(Z124primerNombre, T000J2_A124primerNombre[0]) != 0 )
               {
                  GXUtil.WriteLog("contacto:[seudo value changed for attri]"+"primerNombre");
                  GXUtil.WriteLogRaw("Old: ",Z124primerNombre);
                  GXUtil.WriteLogRaw("Current: ",T000J2_A124primerNombre[0]);
               }
               if ( StringUtil.StrCmp(Z125segundoNombre, T000J2_A125segundoNombre[0]) != 0 )
               {
                  GXUtil.WriteLog("contacto:[seudo value changed for attri]"+"segundoNombre");
                  GXUtil.WriteLogRaw("Old: ",Z125segundoNombre);
                  GXUtil.WriteLogRaw("Current: ",T000J2_A125segundoNombre[0]);
               }
               if ( StringUtil.StrCmp(Z126ContactoapellidoPaterno, T000J2_A126ContactoapellidoPaterno[0]) != 0 )
               {
                  GXUtil.WriteLog("contacto:[seudo value changed for attri]"+"ContactoapellidoPaterno");
                  GXUtil.WriteLogRaw("Old: ",Z126ContactoapellidoPaterno);
                  GXUtil.WriteLogRaw("Current: ",T000J2_A126ContactoapellidoPaterno[0]);
               }
               if ( StringUtil.StrCmp(Z127ContactoapellidoMaterno, T000J2_A127ContactoapellidoMaterno[0]) != 0 )
               {
                  GXUtil.WriteLog("contacto:[seudo value changed for attri]"+"ContactoapellidoMaterno");
                  GXUtil.WriteLogRaw("Old: ",Z127ContactoapellidoMaterno);
                  GXUtil.WriteLogRaw("Current: ",T000J2_A127ContactoapellidoMaterno[0]);
               }
               if ( StringUtil.StrCmp(Z128numero, T000J2_A128numero[0]) != 0 )
               {
                  GXUtil.WriteLog("contacto:[seudo value changed for attri]"+"numero");
                  GXUtil.WriteLogRaw("Old: ",Z128numero);
                  GXUtil.WriteLogRaw("Current: ",T000J2_A128numero[0]);
               }
               if ( StringUtil.StrCmp(Z129Contactocorreo, T000J2_A129Contactocorreo[0]) != 0 )
               {
                  GXUtil.WriteLog("contacto:[seudo value changed for attri]"+"Contactocorreo");
                  GXUtil.WriteLogRaw("Old: ",Z129Contactocorreo);
                  GXUtil.WriteLogRaw("Current: ",T000J2_A129Contactocorreo[0]);
               }
               GX_msglist.addItem(context.GetMessage( "GXM_waschg", new   object[]  {"CONTACTO"}), "RecordWasChanged", 1, "");
               AnyError = 1;
               return  ;
            }
         }
      }

      protected void Insert0J19( )
      {
         BeforeValidate0J19( ) ;
         if ( AnyError == 0 )
         {
            CheckExtendedTable0J19( ) ;
         }
         if ( AnyError == 0 )
         {
            ZM0J19( 0) ;
            CheckOptimisticConcurrency0J19( ) ;
            if ( AnyError == 0 )
            {
               AfterConfirm0J19( ) ;
               if ( AnyError == 0 )
               {
                  BeforeInsert0J19( ) ;
                  if ( AnyError == 0 )
                  {
                     /* Using cursor T000J8 */
                     pr_datastore1.execute(6, new Object[] {A124primerNombre, n125segundoNombre, A125segundoNombre, A126ContactoapellidoPaterno, n127ContactoapellidoMaterno, A127ContactoapellidoMaterno, A128numero, n129Contactocorreo, A129Contactocorreo});
                     A18Contactoid = T000J8_A18Contactoid[0];
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A18Contactoid", StringUtil.LTrim( StringUtil.Str( (decimal)(A18Contactoid), 9, 0)));
                     pr_datastore1.close(6);
                     dsDataStore1.SmartCacheProvider.SetUpdated("CONTACTO") ;
                     if ( AnyError == 0 )
                     {
                        /* Start of After( Insert) rules */
                        /* End of After( Insert) rules */
                        if ( AnyError == 0 )
                        {
                           /* Save values for previous() function. */
                           GX_msglist.addItem(context.GetMessage( "GXM_sucadded", ""), "SuccessfullyAdded", 0, "", true);
                           ResetCaption0J0( ) ;
                        }
                     }
                  }
                  else
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                     AnyError = 1;
                  }
               }
            }
            else
            {
               Load0J19( ) ;
            }
            EndLevel0J19( ) ;
         }
         CloseExtendedTableCursors0J19( ) ;
      }

      protected void Update0J19( )
      {
         BeforeValidate0J19( ) ;
         if ( AnyError == 0 )
         {
            CheckExtendedTable0J19( ) ;
         }
         if ( AnyError == 0 )
         {
            CheckOptimisticConcurrency0J19( ) ;
            if ( AnyError == 0 )
            {
               AfterConfirm0J19( ) ;
               if ( AnyError == 0 )
               {
                  BeforeUpdate0J19( ) ;
                  if ( AnyError == 0 )
                  {
                     /* Using cursor T000J9 */
                     pr_datastore1.execute(7, new Object[] {A124primerNombre, n125segundoNombre, A125segundoNombre, A126ContactoapellidoPaterno, n127ContactoapellidoMaterno, A127ContactoapellidoMaterno, A128numero, n129Contactocorreo, A129Contactocorreo, A18Contactoid});
                     pr_datastore1.close(7);
                     dsDataStore1.SmartCacheProvider.SetUpdated("CONTACTO") ;
                     if ( (pr_datastore1.getStatus(7) == 103) )
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_lock", new   object[]  {"CONTACTO"}), "RecordIsLocked", 1, "");
                        AnyError = 1;
                     }
                     DeferredUpdate0J19( ) ;
                     if ( AnyError == 0 )
                     {
                        /* Start of After( update) rules */
                        /* End of After( update) rules */
                        if ( AnyError == 0 )
                        {
                           getByPrimaryKey( ) ;
                           GX_msglist.addItem(context.GetMessage( "GXM_sucupdated", ""), "SuccessfullyUpdated", 0, "", true);
                           ResetCaption0J0( ) ;
                        }
                     }
                     else
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                        AnyError = 1;
                     }
                  }
               }
            }
            EndLevel0J19( ) ;
         }
         CloseExtendedTableCursors0J19( ) ;
      }

      protected void DeferredUpdate0J19( )
      {
      }

      protected void delete( )
      {
         Gx_mode = "DLT";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         BeforeValidate0J19( ) ;
         if ( AnyError == 0 )
         {
            CheckOptimisticConcurrency0J19( ) ;
         }
         if ( AnyError == 0 )
         {
            OnDeleteControls0J19( ) ;
            AfterConfirm0J19( ) ;
            if ( AnyError == 0 )
            {
               BeforeDelete0J19( ) ;
               if ( AnyError == 0 )
               {
                  /* No cascading delete specified. */
                  /* Using cursor T000J10 */
                  pr_datastore1.execute(8, new Object[] {A18Contactoid});
                  pr_datastore1.close(8);
                  dsDataStore1.SmartCacheProvider.SetUpdated("CONTACTO") ;
                  if ( AnyError == 0 )
                  {
                     /* Start of After( delete) rules */
                     /* End of After( delete) rules */
                     if ( AnyError == 0 )
                     {
                        move_next( ) ;
                        if ( RcdFound19 == 0 )
                        {
                           InitAll0J19( ) ;
                           Gx_mode = "INS";
                           context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                        }
                        else
                        {
                           getByPrimaryKey( ) ;
                           Gx_mode = "UPD";
                           context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                        }
                        GX_msglist.addItem(context.GetMessage( "GXM_sucdeleted", ""), "SuccessfullyDeleted", 0, "", true);
                        ResetCaption0J0( ) ;
                     }
                  }
                  else
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                     AnyError = 1;
                  }
               }
            }
         }
         sMode19 = Gx_mode;
         Gx_mode = "DLT";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         EndLevel0J19( ) ;
         Gx_mode = sMode19;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
      }

      protected void OnDeleteControls0J19( )
      {
         standaloneModal( ) ;
         /* No delete mode formulas found. */
         if ( AnyError == 0 )
         {
            /* Using cursor T000J11 */
            pr_datastore1.execute(9, new Object[] {A18Contactoid});
            if ( (pr_datastore1.getStatus(9) != 101) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_del", new   object[]  {"Contacto_Informe_Actividad"}), "CannotDeleteReferencedRecord", 1, "");
               AnyError = 1;
            }
            pr_datastore1.close(9);
         }
      }

      protected void EndLevel0J19( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
         {
            pr_datastore1.close(0);
         }
         if ( AnyError == 0 )
         {
            BeforeComplete0J19( ) ;
         }
         if ( AnyError == 0 )
         {
            pr_datastore1.close(1);
            context.CommitDataStores("contacto",pr_default);
            if ( AnyError == 0 )
            {
               ConfirmValues0J0( ) ;
            }
            /* After transaction rules */
            /* Execute 'After Trn' event if defined. */
            trnEnded = 1;
         }
         else
         {
            pr_datastore1.close(1);
            context.RollbackDataStores("contacto",pr_default);
         }
         IsModified = 0;
         if ( AnyError != 0 )
         {
            context.wjLoc = "";
            context.nUserReturn = 0;
         }
      }

      public void ScanStart0J19( )
      {
         /* Using cursor T000J12 */
         pr_datastore1.execute(10);
         RcdFound19 = 0;
         if ( (pr_datastore1.getStatus(10) != 101) )
         {
            RcdFound19 = 1;
            A18Contactoid = T000J12_A18Contactoid[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A18Contactoid", StringUtil.LTrim( StringUtil.Str( (decimal)(A18Contactoid), 9, 0)));
         }
         /* Load Subordinate Levels */
      }

      protected void ScanNext0J19( )
      {
         /* Scan next routine */
         pr_datastore1.readNext(10);
         RcdFound19 = 0;
         if ( (pr_datastore1.getStatus(10) != 101) )
         {
            RcdFound19 = 1;
            A18Contactoid = T000J12_A18Contactoid[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A18Contactoid", StringUtil.LTrim( StringUtil.Str( (decimal)(A18Contactoid), 9, 0)));
         }
      }

      protected void ScanEnd0J19( )
      {
         pr_datastore1.close(10);
      }

      protected void AfterConfirm0J19( )
      {
         /* After Confirm Rules */
      }

      protected void BeforeInsert0J19( )
      {
         /* Before Insert Rules */
      }

      protected void BeforeUpdate0J19( )
      {
         /* Before Update Rules */
      }

      protected void BeforeDelete0J19( )
      {
         /* Before Delete Rules */
      }

      protected void BeforeComplete0J19( )
      {
         /* Before Complete Rules */
      }

      protected void BeforeValidate0J19( )
      {
         /* Before Validate Rules */
      }

      protected void DisableAttributes0J19( )
      {
         edtContactoid_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContactoid_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContactoid_Enabled), 5, 0)), true);
         edtprimerNombre_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtprimerNombre_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtprimerNombre_Enabled), 5, 0)), true);
         edtsegundoNombre_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtsegundoNombre_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtsegundoNombre_Enabled), 5, 0)), true);
         edtContactoapellidoPaterno_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContactoapellidoPaterno_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContactoapellidoPaterno_Enabled), 5, 0)), true);
         edtContactoapellidoMaterno_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContactoapellidoMaterno_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContactoapellidoMaterno_Enabled), 5, 0)), true);
         edtnumero_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtnumero_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtnumero_Enabled), 5, 0)), true);
         edtContactocorreo_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContactocorreo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContactocorreo_Enabled), 5, 0)), true);
      }

      protected void send_integrity_lvl_hashes0J19( )
      {
      }

      protected void assign_properties_default( )
      {
      }

      protected void ConfirmValues0J0( )
      {
      }

      public override void RenderHtmlHeaders( )
      {
         GxWebStd.gx_html_headers( context, 0, "", "", Form.Meta, Form.Metaequiv, true);
      }

      public override void RenderHtmlOpenForm( )
      {
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.WriteHtmlText( "<title>") ;
         context.SendWebValue( Form.Caption) ;
         context.WriteHtmlTextNl( "</title>") ;
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         if ( StringUtil.Len( sDynURL) > 0 )
         {
            context.WriteHtmlText( "<BASE href=\""+sDynURL+"\" />") ;
         }
         define_styles( ) ;
         MasterPageObj.master_styles();
         if ( ( ( context.GetBrowserType( ) == 1 ) || ( context.GetBrowserType( ) == 5 ) ) && ( StringUtil.StrCmp(context.GetBrowserVersion( ), "7.0") == 0 ) )
         {
            context.AddJavascriptSource("json2.js", "?"+context.GetBuildNumber( 127771), false);
         }
         context.AddJavascriptSource("jquery.js", "?"+context.GetBuildNumber( 127771), false);
         context.AddJavascriptSource("gxtimezone.js", "?"+context.GetBuildNumber( 127771), false);
         context.AddJavascriptSource("gxgral.js", "?"+context.GetBuildNumber( 127771), false);
         context.AddJavascriptSource("gxcfg.js", "?201913181445", false);
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.WriteHtmlText( Form.Headerrawhtml) ;
         context.CloseHtmlHeader();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         FormProcess = " data-HasEnter=\"true\" data-Skiponenter=\"false\"";
         context.WriteHtmlText( "<body ") ;
         bodyStyle = "" + "background-color:" + context.BuildHTMLColor( Form.Backcolor) + ";color:" + context.BuildHTMLColor( Form.Textcolor) + ";";
         bodyStyle = bodyStyle + "-moz-opacity:0;opacity:0;";
         if ( ! ( String.IsNullOrEmpty(StringUtil.RTrim( Form.Background)) ) )
         {
            bodyStyle = bodyStyle + " background-image:url(" + context.convertURL( Form.Background) + ")";
         }
         context.WriteHtmlText( " "+"class=\"form-horizontal Form\""+" "+ "style='"+bodyStyle+"'") ;
         context.WriteHtmlText( FormProcess+">") ;
         context.skipLines(1);
         context.WriteHtmlTextNl( "<form id=\"MAINFORM\" autocomplete=\"off\" name=\"MAINFORM\" method=\"post\" tabindex=-1  class=\"form-horizontal Form\" data-gx-class=\"form-horizontal Form\" novalidate action=\""+formatLink("contacto.aspx") +"\">") ;
         GxWebStd.gx_hidden_field( context, "_EventName", "");
         GxWebStd.gx_hidden_field( context, "_EventGridId", "");
         GxWebStd.gx_hidden_field( context, "_EventRowId", "");
         context.WriteHtmlText( "<input type=\"submit\" title=\"submit\" style=\"display:none\" disabled>") ;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, "FORM", "Class", "form-horizontal Form", true);
         toggleJsOutput = isJsOutputEnabled( );
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
      }

      protected void send_integrity_footer_hashes( )
      {
         GXKey = Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
      }

      protected void SendCloseFormHiddens( )
      {
         /* Send hidden variables. */
         /* Send saved values. */
         send_integrity_footer_hashes( ) ;
         GxWebStd.gx_hidden_field( context, "Z18Contactoid", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z18Contactoid), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "Z124primerNombre", Z124primerNombre);
         GxWebStd.gx_hidden_field( context, "Z125segundoNombre", Z125segundoNombre);
         GxWebStd.gx_hidden_field( context, "Z126ContactoapellidoPaterno", Z126ContactoapellidoPaterno);
         GxWebStd.gx_hidden_field( context, "Z127ContactoapellidoMaterno", Z127ContactoapellidoMaterno);
         GxWebStd.gx_hidden_field( context, "Z128numero", Z128numero);
         GxWebStd.gx_hidden_field( context, "Z129Contactocorreo", Z129Contactocorreo);
         GxWebStd.gx_hidden_field( context, "IsConfirmed", StringUtil.LTrim( StringUtil.NToC( (decimal)(IsConfirmed), 4, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "IsModified", StringUtil.LTrim( StringUtil.NToC( (decimal)(IsModified), 4, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "Mode", StringUtil.RTrim( Gx_mode));
         GxWebStd.gx_hidden_field( context, "vMODE", StringUtil.RTrim( Gx_mode));
      }

      public override void RenderHtmlCloseForm( )
      {
         SendCloseFormHiddens( ) ;
         GxWebStd.gx_hidden_field( context, "GX_FocusControl", GX_FocusControl);
         SendAjaxEncryptionKey();
         SendSecurityToken(sPrefix);
         SendComponentObjects();
         SendServerCommands();
         SendState();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         context.WriteHtmlTextNl( "</form>") ;
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         include_jscripts( ) ;
      }

      public override short ExecuteStartEvent( )
      {
         standaloneStartup( ) ;
         gxajaxcallmode = (short)((isAjaxCallMode( ) ? 1 : 0));
         return gxajaxcallmode ;
      }

      public override void RenderHtmlContent( )
      {
         context.WriteHtmlText( "<div") ;
         GxWebStd.ClassAttribute( context, "gx-ct-body"+" "+(String.IsNullOrEmpty(StringUtil.RTrim( Form.Class)) ? "form-horizontal Form" : Form.Class)+"-fx");
         context.WriteHtmlText( ">") ;
         Draw( ) ;
         context.WriteHtmlText( "</div>") ;
      }

      public override void DispatchEvents( )
      {
         Process( ) ;
      }

      public override bool HasEnterEvent( )
      {
         return true ;
      }

      public override GXWebForm GetForm( )
      {
         return Form ;
      }

      public override String GetSelfLink( )
      {
         return formatLink("contacto.aspx")  ;
      }

      public override String GetPgmname( )
      {
         return "Contacto" ;
      }

      public override String GetPgmdesc( )
      {
         return "Contacto" ;
      }

      protected void InitializeNonKey0J19( )
      {
         A124primerNombre = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A124primerNombre", A124primerNombre);
         A125segundoNombre = "";
         n125segundoNombre = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A125segundoNombre", A125segundoNombre);
         n125segundoNombre = (String.IsNullOrEmpty(StringUtil.RTrim( A125segundoNombre)) ? true : false);
         A126ContactoapellidoPaterno = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A126ContactoapellidoPaterno", A126ContactoapellidoPaterno);
         A127ContactoapellidoMaterno = "";
         n127ContactoapellidoMaterno = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A127ContactoapellidoMaterno", A127ContactoapellidoMaterno);
         n127ContactoapellidoMaterno = (String.IsNullOrEmpty(StringUtil.RTrim( A127ContactoapellidoMaterno)) ? true : false);
         A128numero = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A128numero", A128numero);
         A129Contactocorreo = "";
         n129Contactocorreo = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A129Contactocorreo", A129Contactocorreo);
         n129Contactocorreo = (String.IsNullOrEmpty(StringUtil.RTrim( A129Contactocorreo)) ? true : false);
         Z124primerNombre = "";
         Z125segundoNombre = "";
         Z126ContactoapellidoPaterno = "";
         Z127ContactoapellidoMaterno = "";
         Z128numero = "";
         Z129Contactocorreo = "";
      }

      protected void InitAll0J19( )
      {
         A18Contactoid = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A18Contactoid", StringUtil.LTrim( StringUtil.Str( (decimal)(A18Contactoid), 9, 0)));
         InitializeNonKey0J19( ) ;
      }

      protected void StandaloneModalInsert( )
      {
      }

      protected void define_styles( )
      {
         AddThemeStyleSheetFile("", context.GetTheme( )+".css", "?"+GetCacheInvalidationToken( ));
         bool outputEnabled = isOutputEnabled( ) ;
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         idxLst = 1;
         while ( idxLst <= Form.Jscriptsrc.Count )
         {
            context.AddJavascriptSource(StringUtil.RTrim( ((String)Form.Jscriptsrc.Item(idxLst))), "?2019131814416", true);
            idxLst = (int)(idxLst+1);
         }
         if ( ! outputEnabled )
         {
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
         }
         /* End function define_styles */
      }

      protected void include_jscripts( )
      {
         context.AddJavascriptSource("messages.eng.js", "?"+GetCacheInvalidationToken( ), false);
         context.AddJavascriptSource("contacto.js", "?2019131814416", false);
         /* End function include_jscripts */
      }

      protected void init_default_properties( )
      {
         lblTitle_Internalname = "TITLE";
         divTitlecontainer_Internalname = "TITLECONTAINER";
         bttBtn_first_Internalname = "BTN_FIRST";
         bttBtn_previous_Internalname = "BTN_PREVIOUS";
         bttBtn_next_Internalname = "BTN_NEXT";
         bttBtn_last_Internalname = "BTN_LAST";
         bttBtn_select_Internalname = "BTN_SELECT";
         divToolbarcell_Internalname = "TOOLBARCELL";
         edtContactoid_Internalname = "CONTACTOID";
         edtprimerNombre_Internalname = "PRIMERNOMBRE";
         edtsegundoNombre_Internalname = "SEGUNDONOMBRE";
         edtContactoapellidoPaterno_Internalname = "CONTACTOAPELLIDOPATERNO";
         edtContactoapellidoMaterno_Internalname = "CONTACTOAPELLIDOMATERNO";
         edtnumero_Internalname = "NUMERO";
         edtContactocorreo_Internalname = "CONTACTOCORREO";
         divFormcontainer_Internalname = "FORMCONTAINER";
         bttBtn_enter_Internalname = "BTN_ENTER";
         bttBtn_cancel_Internalname = "BTN_CANCEL";
         bttBtn_delete_Internalname = "BTN_DELETE";
         divMaintable_Internalname = "MAINTABLE";
         Form.Internalname = "FORM";
      }

      public override void initialize_properties( )
      {
         context.SetDefaultTheme("Carmine");
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         init_default_properties( ) ;
         Form.Headerrawhtml = "";
         Form.Background = "";
         Form.Textcolor = 0;
         Form.Backcolor = (int)(0xFFFFFF);
         Form.Caption = "Contacto";
         bttBtn_delete_Enabled = 1;
         bttBtn_delete_Visible = 1;
         bttBtn_cancel_Visible = 1;
         bttBtn_enter_Enabled = 1;
         bttBtn_enter_Visible = 1;
         edtContactocorreo_Jsonclick = "";
         edtContactocorreo_Enabled = 1;
         edtnumero_Jsonclick = "";
         edtnumero_Enabled = 1;
         edtContactoapellidoMaterno_Jsonclick = "";
         edtContactoapellidoMaterno_Enabled = 1;
         edtContactoapellidoPaterno_Jsonclick = "";
         edtContactoapellidoPaterno_Enabled = 1;
         edtsegundoNombre_Jsonclick = "";
         edtsegundoNombre_Enabled = 1;
         edtprimerNombre_Jsonclick = "";
         edtprimerNombre_Enabled = 1;
         edtContactoid_Jsonclick = "";
         edtContactoid_Enabled = 1;
         bttBtn_select_Visible = 1;
         bttBtn_last_Visible = 1;
         bttBtn_next_Visible = 1;
         bttBtn_previous_Visible = 1;
         bttBtn_first_Visible = 1;
         context.GX_msglist.DisplayMode = 1;
         if ( context.isSpaRequest( ) )
         {
            enableJsOutput();
         }
      }

      protected void dynload_actions( )
      {
         /* End function dynload_actions */
      }

      protected void init_web_controls( )
      {
         /* End function init_web_controls */
      }

      protected void AfterKeyLoadScreen( )
      {
         IsConfirmed = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "IsConfirmed", StringUtil.LTrim( StringUtil.Str( (decimal)(IsConfirmed), 4, 0)));
         getEqualNoModal( ) ;
         GX_FocusControl = edtprimerNombre_Internalname;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         standaloneNotModal( ) ;
         standaloneModal( ) ;
         /* End function AfterKeyLoadScreen */
      }

      public void Valid_Contactoid( int GX_Parm1 ,
                                    String GX_Parm2 ,
                                    String GX_Parm3 ,
                                    String GX_Parm4 ,
                                    String GX_Parm5 ,
                                    String GX_Parm6 ,
                                    String GX_Parm7 )
      {
         A18Contactoid = GX_Parm1;
         A124primerNombre = GX_Parm2;
         A125segundoNombre = GX_Parm3;
         n125segundoNombre = false;
         A126ContactoapellidoPaterno = GX_Parm4;
         A127ContactoapellidoMaterno = GX_Parm5;
         n127ContactoapellidoMaterno = false;
         A128numero = GX_Parm6;
         A129Contactocorreo = GX_Parm7;
         n129Contactocorreo = false;
         context.wbHandled = 1;
         AfterKeyLoadScreen( ) ;
         Draw( ) ;
         send_integrity_footer_hashes( ) ;
         dynload_actions( ) ;
         if ( AnyError == 1 )
         {
         }
         isValidOutput.Add(A124primerNombre);
         isValidOutput.Add(A125segundoNombre);
         isValidOutput.Add(A126ContactoapellidoPaterno);
         isValidOutput.Add(A127ContactoapellidoMaterno);
         isValidOutput.Add(A128numero);
         isValidOutput.Add(A129Contactocorreo);
         isValidOutput.Add(StringUtil.RTrim( Gx_mode));
         isValidOutput.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(Z18Contactoid), 9, 0, ".", "")));
         isValidOutput.Add(Z124primerNombre);
         isValidOutput.Add(Z125segundoNombre);
         isValidOutput.Add(Z126ContactoapellidoPaterno);
         isValidOutput.Add(Z127ContactoapellidoMaterno);
         isValidOutput.Add(Z128numero);
         isValidOutput.Add(Z129Contactocorreo);
         isValidOutput.Add(bttBtn_delete_Enabled);
         isValidOutput.Add(bttBtn_enter_Enabled);
         isValidOutput.Add(context.GX_msglist.ToJavascriptSource());
         isValidOutput.Add(context.httpAjaxContext.ajax_rsp_get_hiddens( ));
         context.GX_webresponse.AddString(isValidOutput.ToJSonString());
         wbTemp = context.ResponseContentType( "application/json");
      }

      public override bool SupportAjaxEvent( )
      {
         return true ;
      }

      public override void InitializeDynEvents( )
      {
         setEventMetadata("ENTER","{handler:'UserMainFullajax',iparms:[{postForm:true}]");
         setEventMetadata("ENTER",",oparms:[]}");
         setEventMetadata("REFRESH","{handler:'Refresh',iparms:[]");
         setEventMetadata("REFRESH",",oparms:[]}");
         return  ;
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
         pr_datastore1.close(1);
      }

      public override void initialize( )
      {
         sPrefix = "";
         Z124primerNombre = "";
         Z125segundoNombre = "";
         Z126ContactoapellidoPaterno = "";
         Z127ContactoapellidoMaterno = "";
         Z128numero = "";
         Z129Contactocorreo = "";
         scmdbuf = "";
         gxfirstwebparm = "";
         gxfirstwebparm_bkp = "";
         GXKey = "";
         PreviousTooltip = "";
         PreviousCaption = "";
         Form = new GXWebForm();
         GX_FocusControl = "";
         lblTitle_Jsonclick = "";
         ClassString = "";
         StyleString = "";
         TempTags = "";
         bttBtn_first_Jsonclick = "";
         bttBtn_previous_Jsonclick = "";
         bttBtn_next_Jsonclick = "";
         bttBtn_last_Jsonclick = "";
         bttBtn_select_Jsonclick = "";
         A124primerNombre = "";
         A125segundoNombre = "";
         A126ContactoapellidoPaterno = "";
         A127ContactoapellidoMaterno = "";
         A128numero = "";
         A129Contactocorreo = "";
         bttBtn_enter_Jsonclick = "";
         bttBtn_cancel_Jsonclick = "";
         bttBtn_delete_Jsonclick = "";
         Gx_mode = "";
         sEvt = "";
         EvtGridId = "";
         EvtRowId = "";
         sEvtType = "";
         T000J4_A18Contactoid = new int[1] ;
         T000J4_A124primerNombre = new String[] {""} ;
         T000J4_A125segundoNombre = new String[] {""} ;
         T000J4_n125segundoNombre = new bool[] {false} ;
         T000J4_A126ContactoapellidoPaterno = new String[] {""} ;
         T000J4_A127ContactoapellidoMaterno = new String[] {""} ;
         T000J4_n127ContactoapellidoMaterno = new bool[] {false} ;
         T000J4_A128numero = new String[] {""} ;
         T000J4_A129Contactocorreo = new String[] {""} ;
         T000J4_n129Contactocorreo = new bool[] {false} ;
         T000J5_A18Contactoid = new int[1] ;
         T000J3_A18Contactoid = new int[1] ;
         T000J3_A124primerNombre = new String[] {""} ;
         T000J3_A125segundoNombre = new String[] {""} ;
         T000J3_n125segundoNombre = new bool[] {false} ;
         T000J3_A126ContactoapellidoPaterno = new String[] {""} ;
         T000J3_A127ContactoapellidoMaterno = new String[] {""} ;
         T000J3_n127ContactoapellidoMaterno = new bool[] {false} ;
         T000J3_A128numero = new String[] {""} ;
         T000J3_A129Contactocorreo = new String[] {""} ;
         T000J3_n129Contactocorreo = new bool[] {false} ;
         sMode19 = "";
         T000J6_A18Contactoid = new int[1] ;
         T000J7_A18Contactoid = new int[1] ;
         T000J2_A18Contactoid = new int[1] ;
         T000J2_A124primerNombre = new String[] {""} ;
         T000J2_A125segundoNombre = new String[] {""} ;
         T000J2_n125segundoNombre = new bool[] {false} ;
         T000J2_A126ContactoapellidoPaterno = new String[] {""} ;
         T000J2_A127ContactoapellidoMaterno = new String[] {""} ;
         T000J2_n127ContactoapellidoMaterno = new bool[] {false} ;
         T000J2_A128numero = new String[] {""} ;
         T000J2_A129Contactocorreo = new String[] {""} ;
         T000J2_n129Contactocorreo = new bool[] {false} ;
         T000J8_A18Contactoid = new int[1] ;
         T000J11_A10Contacto_Informe_Actividadid = new int[1] ;
         T000J12_A18Contactoid = new int[1] ;
         sDynURL = "";
         FormProcess = "";
         bodyStyle = "";
         isValidOutput = new GxUnknownObjectCollection();
         pr_datastore1 = new DataStoreProvider(context, new GeneXus.Programs.contacto__datastore1(),
            new Object[][] {
                new Object[] {
               T000J2_A18Contactoid, T000J2_A124primerNombre, T000J2_A125segundoNombre, T000J2_n125segundoNombre, T000J2_A126ContactoapellidoPaterno, T000J2_A127ContactoapellidoMaterno, T000J2_n127ContactoapellidoMaterno, T000J2_A128numero, T000J2_A129Contactocorreo, T000J2_n129Contactocorreo
               }
               , new Object[] {
               T000J3_A18Contactoid, T000J3_A124primerNombre, T000J3_A125segundoNombre, T000J3_n125segundoNombre, T000J3_A126ContactoapellidoPaterno, T000J3_A127ContactoapellidoMaterno, T000J3_n127ContactoapellidoMaterno, T000J3_A128numero, T000J3_A129Contactocorreo, T000J3_n129Contactocorreo
               }
               , new Object[] {
               T000J4_A18Contactoid, T000J4_A124primerNombre, T000J4_A125segundoNombre, T000J4_n125segundoNombre, T000J4_A126ContactoapellidoPaterno, T000J4_A127ContactoapellidoMaterno, T000J4_n127ContactoapellidoMaterno, T000J4_A128numero, T000J4_A129Contactocorreo, T000J4_n129Contactocorreo
               }
               , new Object[] {
               T000J5_A18Contactoid
               }
               , new Object[] {
               T000J6_A18Contactoid
               }
               , new Object[] {
               T000J7_A18Contactoid
               }
               , new Object[] {
               T000J8_A18Contactoid
               }
               , new Object[] {
               }
               , new Object[] {
               }
               , new Object[] {
               T000J11_A10Contacto_Informe_Actividadid
               }
               , new Object[] {
               T000J12_A18Contactoid
               }
            }
         );
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.contacto__default(),
            new Object[][] {
            }
         );
      }

      private short GxWebError ;
      private short gxcookieaux ;
      private short IsConfirmed ;
      private short IsModified ;
      private short AnyError ;
      private short nKeyPressed ;
      private short initialized ;
      private short GX_JID ;
      private short RcdFound19 ;
      private short Gx_BScreen ;
      private short gxajaxcallmode ;
      private short wbTemp ;
      private int Z18Contactoid ;
      private int trnEnded ;
      private int bttBtn_first_Visible ;
      private int bttBtn_previous_Visible ;
      private int bttBtn_next_Visible ;
      private int bttBtn_last_Visible ;
      private int bttBtn_select_Visible ;
      private int A18Contactoid ;
      private int edtContactoid_Enabled ;
      private int edtprimerNombre_Enabled ;
      private int edtsegundoNombre_Enabled ;
      private int edtContactoapellidoPaterno_Enabled ;
      private int edtContactoapellidoMaterno_Enabled ;
      private int edtnumero_Enabled ;
      private int edtContactocorreo_Enabled ;
      private int bttBtn_enter_Visible ;
      private int bttBtn_enter_Enabled ;
      private int bttBtn_cancel_Visible ;
      private int bttBtn_delete_Visible ;
      private int bttBtn_delete_Enabled ;
      private int idxLst ;
      private String sPrefix ;
      private String scmdbuf ;
      private String gxfirstwebparm ;
      private String gxfirstwebparm_bkp ;
      private String GXKey ;
      private String PreviousTooltip ;
      private String PreviousCaption ;
      private String GX_FocusControl ;
      private String edtContactoid_Internalname ;
      private String divMaintable_Internalname ;
      private String divTitlecontainer_Internalname ;
      private String lblTitle_Internalname ;
      private String lblTitle_Jsonclick ;
      private String ClassString ;
      private String StyleString ;
      private String divFormcontainer_Internalname ;
      private String divToolbarcell_Internalname ;
      private String TempTags ;
      private String bttBtn_first_Internalname ;
      private String bttBtn_first_Jsonclick ;
      private String bttBtn_previous_Internalname ;
      private String bttBtn_previous_Jsonclick ;
      private String bttBtn_next_Internalname ;
      private String bttBtn_next_Jsonclick ;
      private String bttBtn_last_Internalname ;
      private String bttBtn_last_Jsonclick ;
      private String bttBtn_select_Internalname ;
      private String bttBtn_select_Jsonclick ;
      private String edtContactoid_Jsonclick ;
      private String edtprimerNombre_Internalname ;
      private String edtprimerNombre_Jsonclick ;
      private String edtsegundoNombre_Internalname ;
      private String edtsegundoNombre_Jsonclick ;
      private String edtContactoapellidoPaterno_Internalname ;
      private String edtContactoapellidoPaterno_Jsonclick ;
      private String edtContactoapellidoMaterno_Internalname ;
      private String edtContactoapellidoMaterno_Jsonclick ;
      private String edtnumero_Internalname ;
      private String edtnumero_Jsonclick ;
      private String edtContactocorreo_Internalname ;
      private String edtContactocorreo_Jsonclick ;
      private String bttBtn_enter_Internalname ;
      private String bttBtn_enter_Jsonclick ;
      private String bttBtn_cancel_Internalname ;
      private String bttBtn_cancel_Jsonclick ;
      private String bttBtn_delete_Internalname ;
      private String bttBtn_delete_Jsonclick ;
      private String Gx_mode ;
      private String sEvt ;
      private String EvtGridId ;
      private String EvtRowId ;
      private String sEvtType ;
      private String sMode19 ;
      private String sDynURL ;
      private String FormProcess ;
      private String bodyStyle ;
      private bool entryPointCalled ;
      private bool toggleJsOutput ;
      private bool wbErr ;
      private bool n125segundoNombre ;
      private bool n127ContactoapellidoMaterno ;
      private bool n129Contactocorreo ;
      private bool Gx_longc ;
      private String Z124primerNombre ;
      private String Z125segundoNombre ;
      private String Z126ContactoapellidoPaterno ;
      private String Z127ContactoapellidoMaterno ;
      private String Z128numero ;
      private String Z129Contactocorreo ;
      private String A124primerNombre ;
      private String A125segundoNombre ;
      private String A126ContactoapellidoPaterno ;
      private String A127ContactoapellidoMaterno ;
      private String A128numero ;
      private String A129Contactocorreo ;
      private GxUnknownObjectCollection isValidOutput ;
      private IGxDataStore dsDataStore1 ;
      private IGxDataStore dsDefault ;
      private IDataStoreProvider pr_datastore1 ;
      private int[] T000J4_A18Contactoid ;
      private String[] T000J4_A124primerNombre ;
      private String[] T000J4_A125segundoNombre ;
      private bool[] T000J4_n125segundoNombre ;
      private String[] T000J4_A126ContactoapellidoPaterno ;
      private String[] T000J4_A127ContactoapellidoMaterno ;
      private bool[] T000J4_n127ContactoapellidoMaterno ;
      private String[] T000J4_A128numero ;
      private String[] T000J4_A129Contactocorreo ;
      private bool[] T000J4_n129Contactocorreo ;
      private int[] T000J5_A18Contactoid ;
      private int[] T000J3_A18Contactoid ;
      private String[] T000J3_A124primerNombre ;
      private String[] T000J3_A125segundoNombre ;
      private bool[] T000J3_n125segundoNombre ;
      private String[] T000J3_A126ContactoapellidoPaterno ;
      private String[] T000J3_A127ContactoapellidoMaterno ;
      private bool[] T000J3_n127ContactoapellidoMaterno ;
      private String[] T000J3_A128numero ;
      private String[] T000J3_A129Contactocorreo ;
      private bool[] T000J3_n129Contactocorreo ;
      private int[] T000J6_A18Contactoid ;
      private int[] T000J7_A18Contactoid ;
      private int[] T000J2_A18Contactoid ;
      private String[] T000J2_A124primerNombre ;
      private String[] T000J2_A125segundoNombre ;
      private bool[] T000J2_n125segundoNombre ;
      private String[] T000J2_A126ContactoapellidoPaterno ;
      private String[] T000J2_A127ContactoapellidoMaterno ;
      private bool[] T000J2_n127ContactoapellidoMaterno ;
      private String[] T000J2_A128numero ;
      private String[] T000J2_A129Contactocorreo ;
      private bool[] T000J2_n129Contactocorreo ;
      private int[] T000J8_A18Contactoid ;
      private int[] T000J11_A10Contacto_Informe_Actividadid ;
      private IDataStoreProvider pr_default ;
      private int[] T000J12_A18Contactoid ;
      private GXWebForm Form ;
   }

   public class contacto__datastore1 : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
         ,new ForEachCursor(def[2])
         ,new ForEachCursor(def[3])
         ,new ForEachCursor(def[4])
         ,new ForEachCursor(def[5])
         ,new ForEachCursor(def[6])
         ,new UpdateCursor(def[7])
         ,new UpdateCursor(def[8])
         ,new ForEachCursor(def[9])
         ,new ForEachCursor(def[10])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmT000J4 ;
          prmT000J4 = new Object[] {
          new Object[] {"@Contactoid",SqlDbType.Int,9,0}
          } ;
          Object[] prmT000J5 ;
          prmT000J5 = new Object[] {
          new Object[] {"@Contactoid",SqlDbType.Int,9,0}
          } ;
          Object[] prmT000J3 ;
          prmT000J3 = new Object[] {
          new Object[] {"@Contactoid",SqlDbType.Int,9,0}
          } ;
          Object[] prmT000J6 ;
          prmT000J6 = new Object[] {
          new Object[] {"@Contactoid",SqlDbType.Int,9,0}
          } ;
          Object[] prmT000J7 ;
          prmT000J7 = new Object[] {
          new Object[] {"@Contactoid",SqlDbType.Int,9,0}
          } ;
          Object[] prmT000J2 ;
          prmT000J2 = new Object[] {
          new Object[] {"@Contactoid",SqlDbType.Int,9,0}
          } ;
          Object[] prmT000J8 ;
          prmT000J8 = new Object[] {
          new Object[] {"@primerNombre",SqlDbType.VarChar,20,0} ,
          new Object[] {"@segundoNombre",SqlDbType.VarChar,20,0} ,
          new Object[] {"@ContactoapellidoPaterno",SqlDbType.VarChar,20,0} ,
          new Object[] {"@ContactoapellidoMaterno",SqlDbType.VarChar,20,0} ,
          new Object[] {"@numero",SqlDbType.VarChar,30,0} ,
          new Object[] {"@Contactocorreo",SqlDbType.VarChar,100,0}
          } ;
          Object[] prmT000J9 ;
          prmT000J9 = new Object[] {
          new Object[] {"@primerNombre",SqlDbType.VarChar,20,0} ,
          new Object[] {"@segundoNombre",SqlDbType.VarChar,20,0} ,
          new Object[] {"@ContactoapellidoPaterno",SqlDbType.VarChar,20,0} ,
          new Object[] {"@ContactoapellidoMaterno",SqlDbType.VarChar,20,0} ,
          new Object[] {"@numero",SqlDbType.VarChar,30,0} ,
          new Object[] {"@Contactocorreo",SqlDbType.VarChar,100,0} ,
          new Object[] {"@Contactoid",SqlDbType.Int,9,0}
          } ;
          Object[] prmT000J10 ;
          prmT000J10 = new Object[] {
          new Object[] {"@Contactoid",SqlDbType.Int,9,0}
          } ;
          Object[] prmT000J11 ;
          prmT000J11 = new Object[] {
          new Object[] {"@Contactoid",SqlDbType.Int,9,0}
          } ;
          Object[] prmT000J12 ;
          prmT000J12 = new Object[] {
          } ;
          def= new CursorDef[] {
              new CursorDef("T000J2", "SELECT [id] AS Contactoid, [primerNombre], [segundoNombre], [apellidoPaterno], [apellidoMaterno], [numero], [correo] FROM dbo.[Contacto] WITH (UPDLOCK) WHERE [id] = @Contactoid ",true, GxErrorMask.GX_NOMASK, false, this,prmT000J2,1,0,true,false )
             ,new CursorDef("T000J3", "SELECT [id] AS Contactoid, [primerNombre], [segundoNombre], [apellidoPaterno], [apellidoMaterno], [numero], [correo] FROM dbo.[Contacto] WITH (NOLOCK) WHERE [id] = @Contactoid ",true, GxErrorMask.GX_NOMASK, false, this,prmT000J3,1,0,true,false )
             ,new CursorDef("T000J4", "SELECT TM1.[id] AS Contactoid, TM1.[primerNombre], TM1.[segundoNombre], TM1.[apellidoPaterno], TM1.[apellidoMaterno], TM1.[numero], TM1.[correo] FROM dbo.[Contacto] TM1 WITH (NOLOCK) WHERE TM1.[id] = @Contactoid ORDER BY TM1.[id]  OPTION (FAST 100)",true, GxErrorMask.GX_NOMASK, false, this,prmT000J4,100,0,true,false )
             ,new CursorDef("T000J5", "SELECT [id] AS Contactoid FROM dbo.[Contacto] WITH (NOLOCK) WHERE [id] = @Contactoid  OPTION (FAST 1)",true, GxErrorMask.GX_NOMASK, false, this,prmT000J5,1,0,true,false )
             ,new CursorDef("T000J6", "SELECT TOP 1 [id] AS Contactoid FROM dbo.[Contacto] WITH (NOLOCK) WHERE ( [id] > @Contactoid) ORDER BY [id]  OPTION (FAST 1)",true, GxErrorMask.GX_NOMASK, false, this,prmT000J6,1,0,true,true )
             ,new CursorDef("T000J7", "SELECT TOP 1 [id] AS Contactoid FROM dbo.[Contacto] WITH (NOLOCK) WHERE ( [id] < @Contactoid) ORDER BY [id] DESC  OPTION (FAST 1)",true, GxErrorMask.GX_NOMASK, false, this,prmT000J7,1,0,true,true )
             ,new CursorDef("T000J8", "INSERT INTO dbo.[Contacto]([primerNombre], [segundoNombre], [apellidoPaterno], [apellidoMaterno], [numero], [correo]) VALUES(@primerNombre, @segundoNombre, @ContactoapellidoPaterno, @ContactoapellidoMaterno, @numero, @Contactocorreo); SELECT SCOPE_IDENTITY()", GxErrorMask.GX_NOMASK,prmT000J8)
             ,new CursorDef("T000J9", "UPDATE dbo.[Contacto] SET [primerNombre]=@primerNombre, [segundoNombre]=@segundoNombre, [apellidoPaterno]=@ContactoapellidoPaterno, [apellidoMaterno]=@ContactoapellidoMaterno, [numero]=@numero, [correo]=@Contactocorreo  WHERE [id] = @Contactoid", GxErrorMask.GX_NOMASK,prmT000J9)
             ,new CursorDef("T000J10", "DELETE FROM dbo.[Contacto]  WHERE [id] = @Contactoid", GxErrorMask.GX_NOMASK,prmT000J10)
             ,new CursorDef("T000J11", "SELECT TOP 1 [id] AS Contacto_Informe_Actividadid FROM dbo.[Contacto-Informe_Actividad] WITH (NOLOCK) WHERE [idContacto] = @Contactoid ",true, GxErrorMask.GX_NOMASK, false, this,prmT000J11,1,0,true,true )
             ,new CursorDef("T000J12", "SELECT [id] AS Contactoid FROM dbo.[Contacto] WITH (NOLOCK) ORDER BY [id]  OPTION (FAST 100)",true, GxErrorMask.GX_NOMASK, false, this,prmT000J12,100,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getVarchar(2) ;
                ((String[]) buf[2])[0] = rslt.getVarchar(3) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(3);
                ((String[]) buf[4])[0] = rslt.getVarchar(4) ;
                ((String[]) buf[5])[0] = rslt.getVarchar(5) ;
                ((bool[]) buf[6])[0] = rslt.wasNull(5);
                ((String[]) buf[7])[0] = rslt.getVarchar(6) ;
                ((String[]) buf[8])[0] = rslt.getVarchar(7) ;
                ((bool[]) buf[9])[0] = rslt.wasNull(7);
                return;
             case 1 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getVarchar(2) ;
                ((String[]) buf[2])[0] = rslt.getVarchar(3) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(3);
                ((String[]) buf[4])[0] = rslt.getVarchar(4) ;
                ((String[]) buf[5])[0] = rslt.getVarchar(5) ;
                ((bool[]) buf[6])[0] = rslt.wasNull(5);
                ((String[]) buf[7])[0] = rslt.getVarchar(6) ;
                ((String[]) buf[8])[0] = rslt.getVarchar(7) ;
                ((bool[]) buf[9])[0] = rslt.wasNull(7);
                return;
             case 2 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getVarchar(2) ;
                ((String[]) buf[2])[0] = rslt.getVarchar(3) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(3);
                ((String[]) buf[4])[0] = rslt.getVarchar(4) ;
                ((String[]) buf[5])[0] = rslt.getVarchar(5) ;
                ((bool[]) buf[6])[0] = rslt.wasNull(5);
                ((String[]) buf[7])[0] = rslt.getVarchar(6) ;
                ((String[]) buf[8])[0] = rslt.getVarchar(7) ;
                ((bool[]) buf[9])[0] = rslt.wasNull(7);
                return;
             case 3 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 4 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 5 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 6 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 9 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 10 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 1 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 2 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 3 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 4 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 5 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 6 :
                stmt.SetParameter(1, (String)parms[0]);
                if ( (bool)parms[1] )
                {
                   stmt.setNull( 2 , SqlDbType.VarChar );
                }
                else
                {
                   stmt.SetParameter(2, (String)parms[2]);
                }
                stmt.SetParameter(3, (String)parms[3]);
                if ( (bool)parms[4] )
                {
                   stmt.setNull( 4 , SqlDbType.VarChar );
                }
                else
                {
                   stmt.SetParameter(4, (String)parms[5]);
                }
                stmt.SetParameter(5, (String)parms[6]);
                if ( (bool)parms[7] )
                {
                   stmt.setNull( 6 , SqlDbType.VarChar );
                }
                else
                {
                   stmt.SetParameter(6, (String)parms[8]);
                }
                return;
             case 7 :
                stmt.SetParameter(1, (String)parms[0]);
                if ( (bool)parms[1] )
                {
                   stmt.setNull( 2 , SqlDbType.VarChar );
                }
                else
                {
                   stmt.SetParameter(2, (String)parms[2]);
                }
                stmt.SetParameter(3, (String)parms[3]);
                if ( (bool)parms[4] )
                {
                   stmt.setNull( 4 , SqlDbType.VarChar );
                }
                else
                {
                   stmt.SetParameter(4, (String)parms[5]);
                }
                stmt.SetParameter(5, (String)parms[6]);
                if ( (bool)parms[7] )
                {
                   stmt.setNull( 6 , SqlDbType.VarChar );
                }
                else
                {
                   stmt.SetParameter(6, (String)parms[8]);
                }
                stmt.SetParameter(7, (int)parms[9]);
                return;
             case 8 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 9 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
       }
    }

    public String getDataStoreName( )
    {
       return "DATASTORE1";
    }

 }

 public class contacto__default : DataStoreHelperBase, IDataStoreHelper
 {
    public ICursor[] getCursors( )
    {
       cursorDefinitions();
       return new Cursor[] {
     };
  }

  private static CursorDef[] def;
  private void cursorDefinitions( )
  {
     if ( def == null )
     {
        def= new CursorDef[] {
        };
     }
  }

  public void getResults( int cursor ,
                          IFieldGetter rslt ,
                          Object[] buf )
  {
     switch ( cursor )
     {
     }
  }

  public void setParameters( int cursor ,
                             IFieldSetter stmt ,
                             Object[] parms )
  {
     switch ( cursor )
     {
     }
  }

}

}
