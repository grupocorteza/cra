/*
               File: ExportPlanInforme_ActividadWC
        Description: Export Plan Informe_Actividad WC
             Author: GeneXus C# Generator version 16_0_0-127771
       Generated on: 1/5/2019 14:56:49.96
       Program type: Callable routine
          Main DBMS: SQL Server
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Office;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class exportplaninforme_actividadwc : GXProcedure
   {
      public exportplaninforme_actividadwc( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsDataStore1 = context.GetDataStore("DataStore1");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("Carmine");
      }

      public exportplaninforme_actividadwc( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsDataStore1 = context.GetDataStore("DataStore1");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( int aP0_Planid ,
                           out String aP1_Filename ,
                           out String aP2_ErrorMessage )
      {
         this.AV15Planid = aP0_Planid;
         this.AV10Filename = "" ;
         this.AV11ErrorMessage = "" ;
         initialize();
         executePrivate();
         aP1_Filename=this.AV10Filename;
         aP2_ErrorMessage=this.AV11ErrorMessage;
      }

      public String executeUdp( int aP0_Planid ,
                                out String aP1_Filename )
      {
         this.AV15Planid = aP0_Planid;
         this.AV10Filename = "" ;
         this.AV11ErrorMessage = "" ;
         initialize();
         executePrivate();
         aP1_Filename=this.AV10Filename;
         aP2_ErrorMessage=this.AV11ErrorMessage;
         return AV11ErrorMessage ;
      }

      public void executeSubmit( int aP0_Planid ,
                                 out String aP1_Filename ,
                                 out String aP2_ErrorMessage )
      {
         exportplaninforme_actividadwc objexportplaninforme_actividadwc;
         objexportplaninforme_actividadwc = new exportplaninforme_actividadwc();
         objexportplaninforme_actividadwc.AV15Planid = aP0_Planid;
         objexportplaninforme_actividadwc.AV10Filename = "" ;
         objexportplaninforme_actividadwc.AV11ErrorMessage = "" ;
         objexportplaninforme_actividadwc.context.SetSubmitInitialConfig(context);
         objexportplaninforme_actividadwc.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objexportplaninforme_actividadwc);
         aP1_Filename=this.AV10Filename;
         aP2_ErrorMessage=this.AV11ErrorMessage;
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((exportplaninforme_actividadwc)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Design", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         AV14Random = (int)(NumberUtil.Random( )*10000);
         AV10Filename = "ExportPlanInforme_ActividadWC-" + StringUtil.Trim( StringUtil.Str( (decimal)(AV14Random), 8, 0)) + ".xlsx";
         AV9ExcelDocument.Open(AV10Filename);
         /* Execute user subroutine: 'CHECKSTATUS' */
         S111 ();
         if ( returnInSub )
         {
            this.cleanup();
            if (true) return;
         }
         AV9ExcelDocument.Clear();
         AV12CellRow = 1;
         AV13FirstColumn = 1;
         AV9ExcelDocument.get_Cells(AV12CellRow, AV13FirstColumn+0, 1, 1).Bold = 1;
         AV9ExcelDocument.get_Cells(AV12CellRow, AV13FirstColumn+0, 1, 1).Text = "Resultados";
         AV9ExcelDocument.get_Cells(AV12CellRow, AV13FirstColumn+1, 1, 1).Bold = 1;
         AV9ExcelDocument.get_Cells(AV12CellRow, AV13FirstColumn+1, 1, 1).Text = "Seguimiento";
         AV9ExcelDocument.get_Cells(AV12CellRow, AV13FirstColumn+2, 1, 1).Bold = 1;
         AV9ExcelDocument.get_Cells(AV12CellRow, AV13FirstColumn+2, 1, 1).Text = "Dificultades";
         AV9ExcelDocument.get_Cells(AV12CellRow, AV13FirstColumn+3, 1, 1).Bold = 1;
         AV9ExcelDocument.get_Cells(AV12CellRow, AV13FirstColumn+3, 1, 1).Text = "Mejorar";
         AV9ExcelDocument.get_Cells(AV12CellRow, AV13FirstColumn+4, 1, 1).Bold = 1;
         AV9ExcelDocument.get_Cells(AV12CellRow, AV13FirstColumn+4, 1, 1).Text = "Comentarios";
         AV9ExcelDocument.get_Cells(AV12CellRow, AV13FirstColumn+5, 1, 1).Bold = 1;
         AV9ExcelDocument.get_Cells(AV12CellRow, AV13FirstColumn+5, 1, 1).Text = "Fecha/Informe";
         AV9ExcelDocument.get_Cells(AV12CellRow, AV13FirstColumn+6, 1, 1).Bold = 1;
         AV9ExcelDocument.get_Cells(AV12CellRow, AV13FirstColumn+6, 1, 1).Text = "Fecha Actividad";
         /* Using cursor P000E2 */
         pr_datastore1.execute(0, new Object[] {AV15Planid});
         while ( (pr_datastore1.getStatus(0) != 101) )
         {
            A13Planid = P000E2_A13Planid[0];
            A41resultados = P000E2_A41resultados[0];
            A42seguimiento = P000E2_A42seguimiento[0];
            A43dificultades = P000E2_A43dificultades[0];
            A44mejorar = P000E2_A44mejorar[0];
            A45comentarios = P000E2_A45comentarios[0];
            n45comentarios = P000E2_n45comentarios[0];
            A46fecha = P000E2_A46fecha[0];
            A47fechaActividad = P000E2_A47fechaActividad[0];
            n47fechaActividad = P000E2_n47fechaActividad[0];
            A11Informe_Actividadid = P000E2_A11Informe_Actividadid[0];
            AV12CellRow = (int)(AV12CellRow+1);
            AV9ExcelDocument.get_Cells(AV12CellRow, AV13FirstColumn+0, 1, 1).Text = A41resultados;
            AV9ExcelDocument.get_Cells(AV12CellRow, AV13FirstColumn+1, 1, 1).Text = A42seguimiento;
            AV9ExcelDocument.get_Cells(AV12CellRow, AV13FirstColumn+2, 1, 1).Text = A43dificultades;
            AV9ExcelDocument.get_Cells(AV12CellRow, AV13FirstColumn+3, 1, 1).Text = A44mejorar;
            AV9ExcelDocument.get_Cells(AV12CellRow, AV13FirstColumn+4, 1, 1).Text = A45comentarios;
            GXt_dtime1 = DateTimeUtil.ResetTime( A46fecha ) ;
            AV9ExcelDocument.SetDateFormat(context, 8, 5, 1, 2, "/", ":", " ");
            AV9ExcelDocument.get_Cells(AV12CellRow, AV13FirstColumn+5, 1, 1).Date = GXt_dtime1;
            GXt_dtime1 = DateTimeUtil.ResetTime( A47fechaActividad ) ;
            AV9ExcelDocument.SetDateFormat(context, 8, 5, 1, 2, "/", ":", " ");
            AV9ExcelDocument.get_Cells(AV12CellRow, AV13FirstColumn+6, 1, 1).Date = GXt_dtime1;
            pr_datastore1.readNext(0);
         }
         pr_datastore1.close(0);
         AV9ExcelDocument.Save();
         /* Execute user subroutine: 'CHECKSTATUS' */
         S111 ();
         if ( returnInSub )
         {
            this.cleanup();
            if (true) return;
         }
         AV9ExcelDocument.Close();
         this.cleanup();
      }

      protected void S111( )
      {
         /* 'CHECKSTATUS' Routine */
         if ( AV9ExcelDocument.ErrCode != 0 )
         {
            AV10Filename = "";
            AV11ErrorMessage = AV9ExcelDocument.ErrDescription;
            AV9ExcelDocument.Close();
            returnInSub = true;
            if (true) return;
         }
      }

      public override void cleanup( )
      {
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         AV9ExcelDocument = new ExcelDocumentI();
         scmdbuf = "";
         P000E2_A13Planid = new int[1] ;
         P000E2_A41resultados = new String[] {""} ;
         P000E2_A42seguimiento = new String[] {""} ;
         P000E2_A43dificultades = new String[] {""} ;
         P000E2_A44mejorar = new String[] {""} ;
         P000E2_A45comentarios = new String[] {""} ;
         P000E2_n45comentarios = new bool[] {false} ;
         P000E2_A46fecha = new DateTime[] {DateTime.MinValue} ;
         P000E2_A47fechaActividad = new DateTime[] {DateTime.MinValue} ;
         P000E2_n47fechaActividad = new bool[] {false} ;
         P000E2_A11Informe_Actividadid = new int[1] ;
         A41resultados = "";
         A42seguimiento = "";
         A43dificultades = "";
         A44mejorar = "";
         A45comentarios = "";
         A46fecha = DateTime.MinValue;
         A47fechaActividad = DateTime.MinValue;
         GXt_dtime1 = (DateTime)(DateTime.MinValue);
         pr_datastore1 = new DataStoreProvider(context, new GeneXus.Programs.exportplaninforme_actividadwc__datastore1(),
            new Object[][] {
                new Object[] {
               P000E2_A13Planid, P000E2_A41resultados, P000E2_A42seguimiento, P000E2_A43dificultades, P000E2_A44mejorar, P000E2_A45comentarios, P000E2_n45comentarios, P000E2_A46fecha, P000E2_A47fechaActividad, P000E2_n47fechaActividad,
               P000E2_A11Informe_Actividadid
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private int AV15Planid ;
      private int AV14Random ;
      private int AV12CellRow ;
      private int AV13FirstColumn ;
      private int A13Planid ;
      private int A11Informe_Actividadid ;
      private String scmdbuf ;
      private DateTime GXt_dtime1 ;
      private DateTime A46fecha ;
      private DateTime A47fechaActividad ;
      private bool returnInSub ;
      private bool n45comentarios ;
      private bool n47fechaActividad ;
      private String AV11ErrorMessage ;
      private String AV10Filename ;
      private String A41resultados ;
      private String A42seguimiento ;
      private String A43dificultades ;
      private String A44mejorar ;
      private String A45comentarios ;
      private IGxDataStore dsDataStore1 ;
      private IGxDataStore dsDefault ;
      private IDataStoreProvider pr_datastore1 ;
      private int[] P000E2_A13Planid ;
      private String[] P000E2_A41resultados ;
      private String[] P000E2_A42seguimiento ;
      private String[] P000E2_A43dificultades ;
      private String[] P000E2_A44mejorar ;
      private String[] P000E2_A45comentarios ;
      private bool[] P000E2_n45comentarios ;
      private DateTime[] P000E2_A46fecha ;
      private DateTime[] P000E2_A47fechaActividad ;
      private bool[] P000E2_n47fechaActividad ;
      private int[] P000E2_A11Informe_Actividadid ;
      private String aP1_Filename ;
      private String aP2_ErrorMessage ;
      private ExcelDocumentI AV9ExcelDocument ;
   }

   public class exportplaninforme_actividadwc__datastore1 : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmP000E2 ;
          prmP000E2 = new Object[] {
          new Object[] {"@AV15Planid",SqlDbType.Int,9,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("P000E2", "SELECT [idPlan], [resultados], [seguimiento], [dificultades], [mejorar], [comentarios], [fecha], [fechaActividad], [id] FROM dbo.[Informe_Actividad] WITH (NOLOCK) WHERE [idPlan] = @AV15Planid ORDER BY [idPlan] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP000E2,100,0,false,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getVarchar(2) ;
                ((String[]) buf[2])[0] = rslt.getVarchar(3) ;
                ((String[]) buf[3])[0] = rslt.getVarchar(4) ;
                ((String[]) buf[4])[0] = rslt.getVarchar(5) ;
                ((String[]) buf[5])[0] = rslt.getVarchar(6) ;
                ((bool[]) buf[6])[0] = rslt.wasNull(6);
                ((DateTime[]) buf[7])[0] = rslt.getGXDate(7) ;
                ((DateTime[]) buf[8])[0] = rslt.getGXDate(8) ;
                ((bool[]) buf[9])[0] = rslt.wasNull(8);
                ((int[]) buf[10])[0] = rslt.getInt(9) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
       }
    }

    public String getDataStoreName( )
    {
       return "DATASTORE1";
    }

 }

}
