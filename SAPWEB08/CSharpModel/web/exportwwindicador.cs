/*
               File: ExportWWIndicador
        Description: Export WWIndicador
             Author: GeneXus C# Generator version 16_0_0-127771
       Generated on: 1/5/2019 14:2:39.60
       Program type: Callable routine
          Main DBMS: SQL Server
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Office;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class exportwwindicador : GXProcedure
   {
      public exportwwindicador( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsDataStore1 = context.GetDataStore("DataStore1");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("Carmine");
      }

      public exportwwindicador( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsDataStore1 = context.GetDataStore("DataStore1");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( String aP0_resultado ,
                           out String aP1_Filename ,
                           out String aP2_ErrorMessage )
      {
         this.AV15resultado = aP0_resultado;
         this.AV10Filename = "" ;
         this.AV11ErrorMessage = "" ;
         initialize();
         executePrivate();
         aP1_Filename=this.AV10Filename;
         aP2_ErrorMessage=this.AV11ErrorMessage;
      }

      public String executeUdp( String aP0_resultado ,
                                out String aP1_Filename )
      {
         this.AV15resultado = aP0_resultado;
         this.AV10Filename = "" ;
         this.AV11ErrorMessage = "" ;
         initialize();
         executePrivate();
         aP1_Filename=this.AV10Filename;
         aP2_ErrorMessage=this.AV11ErrorMessage;
         return AV11ErrorMessage ;
      }

      public void executeSubmit( String aP0_resultado ,
                                 out String aP1_Filename ,
                                 out String aP2_ErrorMessage )
      {
         exportwwindicador objexportwwindicador;
         objexportwwindicador = new exportwwindicador();
         objexportwwindicador.AV15resultado = aP0_resultado;
         objexportwwindicador.AV10Filename = "" ;
         objexportwwindicador.AV11ErrorMessage = "" ;
         objexportwwindicador.context.SetSubmitInitialConfig(context);
         objexportwwindicador.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objexportwwindicador);
         aP1_Filename=this.AV10Filename;
         aP2_ErrorMessage=this.AV11ErrorMessage;
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((exportwwindicador)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Design", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         AV14Random = (int)(NumberUtil.Random( )*10000);
         AV10Filename = "ExportWWIndicador-" + StringUtil.Trim( StringUtil.Str( (decimal)(AV14Random), 8, 0)) + ".xlsx";
         AV9ExcelDocument.Open(AV10Filename);
         /* Execute user subroutine: 'CHECKSTATUS' */
         S111 ();
         if ( returnInSub )
         {
            this.cleanup();
            if (true) return;
         }
         AV9ExcelDocument.Clear();
         AV12CellRow = 1;
         AV13FirstColumn = 1;
         AV9ExcelDocument.get_Cells(AV12CellRow, AV13FirstColumn+0, 1, 1).Bold = 1;
         AV9ExcelDocument.get_Cells(AV12CellRow, AV13FirstColumn+0, 1, 1).Text = "Pa�s";
         AV9ExcelDocument.get_Cells(AV12CellRow, AV13FirstColumn+1, 1, 1).Bold = 1;
         AV9ExcelDocument.get_Cells(AV12CellRow, AV13FirstColumn+1, 1, 1).Text = "Resultado";
         AV9ExcelDocument.get_Cells(AV12CellRow, AV13FirstColumn+2, 1, 1).Bold = 1;
         AV9ExcelDocument.get_Cells(AV12CellRow, AV13FirstColumn+2, 1, 1).Text = "Objetivo";
         AV9ExcelDocument.get_Cells(AV12CellRow, AV13FirstColumn+3, 1, 1).Bold = 1;
         AV9ExcelDocument.get_Cells(AV12CellRow, AV13FirstColumn+3, 1, 1).Text = "Proyectonombre";
         pr_datastore1.dynParam(0, new Object[]{ new Object[]{
                                              AV15resultado ,
                                              A62resultado } ,
                                              new int[]{
                                              TypeConstants.STRING, TypeConstants.STRING
                                              }
         } ) ;
         lV15resultado = StringUtil.Concat( StringUtil.RTrim( AV15resultado), "%", "");
         /* Using cursor P000C2 */
         pr_datastore1.execute(0, new Object[] {lV15resultado});
         while ( (pr_datastore1.getStatus(0) != 101) )
         {
            A25Proyectoid = P000C2_A25Proyectoid[0];
            A15Paisid = P000C2_A15Paisid[0];
            A62resultado = P000C2_A62resultado[0];
            A118Paisnombre = P000C2_A118Paisnombre[0];
            A63objetivo = P000C2_A63objetivo[0];
            A74Proyectonombre = P000C2_A74Proyectonombre[0];
            A4Indicadorid = P000C2_A4Indicadorid[0];
            A74Proyectonombre = P000C2_A74Proyectonombre[0];
            A118Paisnombre = P000C2_A118Paisnombre[0];
            AV12CellRow = (int)(AV12CellRow+1);
            AV9ExcelDocument.get_Cells(AV12CellRow, AV13FirstColumn+0, 1, 1).Text = A118Paisnombre;
            AV9ExcelDocument.get_Cells(AV12CellRow, AV13FirstColumn+1, 1, 1).Text = A62resultado;
            AV9ExcelDocument.get_Cells(AV12CellRow, AV13FirstColumn+2, 1, 1).Text = A63objetivo;
            AV9ExcelDocument.get_Cells(AV12CellRow, AV13FirstColumn+3, 1, 1).Text = A74Proyectonombre;
            pr_datastore1.readNext(0);
         }
         pr_datastore1.close(0);
         AV9ExcelDocument.Save();
         /* Execute user subroutine: 'CHECKSTATUS' */
         S111 ();
         if ( returnInSub )
         {
            this.cleanup();
            if (true) return;
         }
         AV9ExcelDocument.Close();
         this.cleanup();
      }

      protected void S111( )
      {
         /* 'CHECKSTATUS' Routine */
         if ( AV9ExcelDocument.ErrCode != 0 )
         {
            AV10Filename = "";
            AV11ErrorMessage = AV9ExcelDocument.ErrDescription;
            AV9ExcelDocument.Close();
            returnInSub = true;
            if (true) return;
         }
      }

      public override void cleanup( )
      {
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         AV9ExcelDocument = new ExcelDocumentI();
         scmdbuf = "";
         lV15resultado = "";
         A62resultado = "";
         P000C2_A25Proyectoid = new int[1] ;
         P000C2_A15Paisid = new int[1] ;
         P000C2_A62resultado = new String[] {""} ;
         P000C2_A118Paisnombre = new String[] {""} ;
         P000C2_A63objetivo = new String[] {""} ;
         P000C2_A74Proyectonombre = new String[] {""} ;
         P000C2_A4Indicadorid = new int[1] ;
         A118Paisnombre = "";
         A63objetivo = "";
         A74Proyectonombre = "";
         pr_datastore1 = new DataStoreProvider(context, new GeneXus.Programs.exportwwindicador__datastore1(),
            new Object[][] {
                new Object[] {
               P000C2_A25Proyectoid, P000C2_A15Paisid, P000C2_A62resultado, P000C2_A118Paisnombre, P000C2_A63objetivo, P000C2_A74Proyectonombre, P000C2_A4Indicadorid
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private int AV14Random ;
      private int AV12CellRow ;
      private int AV13FirstColumn ;
      private int A25Proyectoid ;
      private int A15Paisid ;
      private int A4Indicadorid ;
      private String scmdbuf ;
      private bool returnInSub ;
      private String AV15resultado ;
      private String AV11ErrorMessage ;
      private String AV10Filename ;
      private String lV15resultado ;
      private String A62resultado ;
      private String A118Paisnombre ;
      private String A63objetivo ;
      private String A74Proyectonombre ;
      private IGxDataStore dsDataStore1 ;
      private IGxDataStore dsDefault ;
      private IDataStoreProvider pr_datastore1 ;
      private int[] P000C2_A25Proyectoid ;
      private int[] P000C2_A15Paisid ;
      private String[] P000C2_A62resultado ;
      private String[] P000C2_A118Paisnombre ;
      private String[] P000C2_A63objetivo ;
      private String[] P000C2_A74Proyectonombre ;
      private int[] P000C2_A4Indicadorid ;
      private String aP1_Filename ;
      private String aP2_ErrorMessage ;
      private ExcelDocumentI AV9ExcelDocument ;
   }

   public class exportwwindicador__datastore1 : DataStoreHelperBase, IDataStoreHelper
   {
      protected Object[] conditional_P000C2( IGxContext context ,
                                             String AV15resultado ,
                                             String A62resultado )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int1 ;
         GXv_int1 = new short [1] ;
         Object[] GXv_Object2 ;
         GXv_Object2 = new Object [2] ;
         scmdbuf = "SELECT T1.[idProyecto] AS Proyectoid, T1.[idPais] AS Paisid, T1.[resultado], T3.[nombre] AS Paisnombre, T1.[objetivo], T2.[nombre] AS Proyectonombre, T1.[id] AS Indicadorid FROM ((dbo.[Indicador] T1 WITH (NOLOCK) INNER JOIN dbo.[Proyecto] T2 WITH (NOLOCK) ON T2.[id] = T1.[idProyecto]) INNER JOIN dbo.[Pais] T3 WITH (NOLOCK) ON T3.[id] = T1.[idPais])";
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV15resultado)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[resultado] like @lV15resultado)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[resultado] like @lV15resultado)";
            }
         }
         else
         {
            GXv_int1[0] = 1;
         }
         if ( StringUtil.StrCmp("", sWhereString) != 0 )
         {
            scmdbuf = scmdbuf + " WHERE" + sWhereString;
         }
         scmdbuf = scmdbuf + " ORDER BY T1.[resultado]";
         GXv_Object2[0] = scmdbuf;
         GXv_Object2[1] = GXv_int1;
         return GXv_Object2 ;
      }

      public override Object [] getDynamicStatement( int cursor ,
                                                     IGxContext context ,
                                                     Object [] dynConstraints )
      {
         switch ( cursor )
         {
               case 0 :
                     return conditional_P000C2(context, (String)dynConstraints[0] , (String)dynConstraints[1] );
         }
         return base.getDynamicStatement(cursor, context, dynConstraints);
      }

      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmP000C2 ;
          prmP000C2 = new Object[] {
          new Object[] {"@lV15resultado",SqlDbType.VarChar,400,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("P000C2", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP000C2,100,0,false,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((String[]) buf[2])[0] = rslt.getVarchar(3) ;
                ((String[]) buf[3])[0] = rslt.getVarchar(4) ;
                ((String[]) buf[4])[0] = rslt.getVarchar(5) ;
                ((String[]) buf[5])[0] = rslt.getVarchar(6) ;
                ((int[]) buf[6])[0] = rslt.getInt(7) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       short sIdx ;
       switch ( cursor )
       {
             case 0 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[1]);
                }
                return;
       }
    }

    public String getDataStoreName( )
    {
       return "DATASTORE1";
    }

 }

}
