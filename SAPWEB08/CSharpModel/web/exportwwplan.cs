/*
               File: ExportWWPlan
        Description: Export WWPlan
             Author: GeneXus C# Generator version 16_0_0-127771
       Generated on: 1/5/2019 14:46:46.24
       Program type: Callable routine
          Main DBMS: SQL Server
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Office;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class exportwwplan : GXProcedure
   {
      public exportwwplan( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsDataStore1 = context.GetDataStore("DataStore1");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("Carmine");
      }

      public exportwwplan( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsDataStore1 = context.GetDataStore("DataStore1");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( String aP0_nombreEvento ,
                           out String aP1_Filename ,
                           out String aP2_ErrorMessage )
      {
         this.AV15nombreEvento = aP0_nombreEvento;
         this.AV10Filename = "" ;
         this.AV11ErrorMessage = "" ;
         initialize();
         executePrivate();
         aP1_Filename=this.AV10Filename;
         aP2_ErrorMessage=this.AV11ErrorMessage;
      }

      public String executeUdp( String aP0_nombreEvento ,
                                out String aP1_Filename )
      {
         this.AV15nombreEvento = aP0_nombreEvento;
         this.AV10Filename = "" ;
         this.AV11ErrorMessage = "" ;
         initialize();
         executePrivate();
         aP1_Filename=this.AV10Filename;
         aP2_ErrorMessage=this.AV11ErrorMessage;
         return AV11ErrorMessage ;
      }

      public void executeSubmit( String aP0_nombreEvento ,
                                 out String aP1_Filename ,
                                 out String aP2_ErrorMessage )
      {
         exportwwplan objexportwwplan;
         objexportwwplan = new exportwwplan();
         objexportwwplan.AV15nombreEvento = aP0_nombreEvento;
         objexportwwplan.AV10Filename = "" ;
         objexportwwplan.AV11ErrorMessage = "" ;
         objexportwwplan.context.SetSubmitInitialConfig(context);
         objexportwwplan.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objexportwwplan);
         aP1_Filename=this.AV10Filename;
         aP2_ErrorMessage=this.AV11ErrorMessage;
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((exportwwplan)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Design", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         AV14Random = (int)(NumberUtil.Random( )*10000);
         AV10Filename = "ExportWWPlan-" + StringUtil.Trim( StringUtil.Str( (decimal)(AV14Random), 8, 0)) + ".xlsx";
         AV9ExcelDocument.Open(AV10Filename);
         /* Execute user subroutine: 'CHECKSTATUS' */
         S111 ();
         if ( returnInSub )
         {
            this.cleanup();
            if (true) return;
         }
         AV9ExcelDocument.Clear();
         AV12CellRow = 1;
         AV13FirstColumn = 1;
         AV9ExcelDocument.get_Cells(AV12CellRow, AV13FirstColumn+0, 1, 1).Bold = 1;
         AV9ExcelDocument.get_Cells(AV12CellRow, AV13FirstColumn+0, 1, 1).Text = "Evento";
         AV9ExcelDocument.get_Cells(AV12CellRow, AV13FirstColumn+1, 1, 1).Bold = 1;
         AV9ExcelDocument.get_Cells(AV12CellRow, AV13FirstColumn+1, 1, 1).Text = "Comunidad";
         AV9ExcelDocument.get_Cells(AV12CellRow, AV13FirstColumn+2, 1, 1).Bold = 1;
         AV9ExcelDocument.get_Cells(AV12CellRow, AV13FirstColumn+2, 1, 1).Text = "Fecha de Inicio";
         AV9ExcelDocument.get_Cells(AV12CellRow, AV13FirstColumn+3, 1, 1).Bold = 1;
         AV9ExcelDocument.get_Cells(AV12CellRow, AV13FirstColumn+3, 1, 1).Text = "Fecha de Finalización";
         AV9ExcelDocument.get_Cells(AV12CellRow, AV13FirstColumn+4, 1, 1).Bold = 1;
         AV9ExcelDocument.get_Cells(AV12CellRow, AV13FirstColumn+4, 1, 1).Text = "Estado";
         AV9ExcelDocument.get_Cells(AV12CellRow, AV13FirstColumn+5, 1, 1).Bold = 1;
         AV9ExcelDocument.get_Cells(AV12CellRow, AV13FirstColumn+5, 1, 1).Text = "Benf. Esperados";
         pr_datastore1.dynParam(0, new Object[]{ new Object[]{
                                              AV15nombreEvento ,
                                              A30nombreEvento } ,
                                              new int[]{
                                              TypeConstants.STRING, TypeConstants.STRING
                                              }
         } ) ;
         lV15nombreEvento = StringUtil.Concat( StringUtil.RTrim( AV15nombreEvento), "%", "");
         /* Using cursor P000D2 */
         pr_datastore1.execute(0, new Object[] {lV15nombreEvento});
         while ( (pr_datastore1.getStatus(0) != 101) )
         {
            A19Comunidadid = P000D2_A19Comunidadid[0];
            A12Estadoid = P000D2_A12Estadoid[0];
            A30nombreEvento = P000D2_A30nombreEvento[0];
            A130Comunidadnombre = P000D2_A130Comunidadnombre[0];
            A31inicio = P000D2_A31inicio[0];
            A32fin = P000D2_A32fin[0];
            A61Estadonombre = P000D2_A61Estadonombre[0];
            A39cantBeneficiarios = P000D2_A39cantBeneficiarios[0];
            n39cantBeneficiarios = P000D2_n39cantBeneficiarios[0];
            A13Planid = P000D2_A13Planid[0];
            A130Comunidadnombre = P000D2_A130Comunidadnombre[0];
            A61Estadonombre = P000D2_A61Estadonombre[0];
            AV12CellRow = (int)(AV12CellRow+1);
            AV9ExcelDocument.get_Cells(AV12CellRow, AV13FirstColumn+0, 1, 1).Text = A30nombreEvento;
            AV9ExcelDocument.get_Cells(AV12CellRow, AV13FirstColumn+1, 1, 1).Text = A130Comunidadnombre;
            GXt_dtime1 = DateTimeUtil.ResetTime( A31inicio ) ;
            AV9ExcelDocument.SetDateFormat(context, 8, 5, 1, 2, "/", ":", " ");
            AV9ExcelDocument.get_Cells(AV12CellRow, AV13FirstColumn+2, 1, 1).Date = GXt_dtime1;
            GXt_dtime1 = DateTimeUtil.ResetTime( A32fin ) ;
            AV9ExcelDocument.SetDateFormat(context, 8, 5, 1, 2, "/", ":", " ");
            AV9ExcelDocument.get_Cells(AV12CellRow, AV13FirstColumn+3, 1, 1).Date = GXt_dtime1;
            AV9ExcelDocument.get_Cells(AV12CellRow, AV13FirstColumn+4, 1, 1).Text = A61Estadonombre;
            AV9ExcelDocument.get_Cells(AV12CellRow, AV13FirstColumn+5, 1, 1).Number = A39cantBeneficiarios;
            pr_datastore1.readNext(0);
         }
         pr_datastore1.close(0);
         AV9ExcelDocument.Save();
         /* Execute user subroutine: 'CHECKSTATUS' */
         S111 ();
         if ( returnInSub )
         {
            this.cleanup();
            if (true) return;
         }
         AV9ExcelDocument.Close();
         this.cleanup();
      }

      protected void S111( )
      {
         /* 'CHECKSTATUS' Routine */
         if ( AV9ExcelDocument.ErrCode != 0 )
         {
            AV10Filename = "";
            AV11ErrorMessage = AV9ExcelDocument.ErrDescription;
            AV9ExcelDocument.Close();
            returnInSub = true;
            if (true) return;
         }
      }

      public override void cleanup( )
      {
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         AV9ExcelDocument = new ExcelDocumentI();
         scmdbuf = "";
         lV15nombreEvento = "";
         A30nombreEvento = "";
         P000D2_A19Comunidadid = new int[1] ;
         P000D2_A12Estadoid = new int[1] ;
         P000D2_A30nombreEvento = new String[] {""} ;
         P000D2_A130Comunidadnombre = new String[] {""} ;
         P000D2_A31inicio = new DateTime[] {DateTime.MinValue} ;
         P000D2_A32fin = new DateTime[] {DateTime.MinValue} ;
         P000D2_A61Estadonombre = new String[] {""} ;
         P000D2_A39cantBeneficiarios = new int[1] ;
         P000D2_n39cantBeneficiarios = new bool[] {false} ;
         P000D2_A13Planid = new int[1] ;
         A130Comunidadnombre = "";
         A31inicio = DateTime.MinValue;
         A32fin = DateTime.MinValue;
         A61Estadonombre = "";
         GXt_dtime1 = (DateTime)(DateTime.MinValue);
         pr_datastore1 = new DataStoreProvider(context, new GeneXus.Programs.exportwwplan__datastore1(),
            new Object[][] {
                new Object[] {
               P000D2_A19Comunidadid, P000D2_A12Estadoid, P000D2_A30nombreEvento, P000D2_A130Comunidadnombre, P000D2_A31inicio, P000D2_A32fin, P000D2_A61Estadonombre, P000D2_A39cantBeneficiarios, P000D2_n39cantBeneficiarios, P000D2_A13Planid
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private int AV14Random ;
      private int AV12CellRow ;
      private int AV13FirstColumn ;
      private int A19Comunidadid ;
      private int A12Estadoid ;
      private int A39cantBeneficiarios ;
      private int A13Planid ;
      private String scmdbuf ;
      private DateTime GXt_dtime1 ;
      private DateTime A31inicio ;
      private DateTime A32fin ;
      private bool returnInSub ;
      private bool n39cantBeneficiarios ;
      private String AV15nombreEvento ;
      private String AV11ErrorMessage ;
      private String AV10Filename ;
      private String lV15nombreEvento ;
      private String A30nombreEvento ;
      private String A130Comunidadnombre ;
      private String A61Estadonombre ;
      private IGxDataStore dsDataStore1 ;
      private IGxDataStore dsDefault ;
      private IDataStoreProvider pr_datastore1 ;
      private int[] P000D2_A19Comunidadid ;
      private int[] P000D2_A12Estadoid ;
      private String[] P000D2_A30nombreEvento ;
      private String[] P000D2_A130Comunidadnombre ;
      private DateTime[] P000D2_A31inicio ;
      private DateTime[] P000D2_A32fin ;
      private String[] P000D2_A61Estadonombre ;
      private int[] P000D2_A39cantBeneficiarios ;
      private bool[] P000D2_n39cantBeneficiarios ;
      private int[] P000D2_A13Planid ;
      private String aP1_Filename ;
      private String aP2_ErrorMessage ;
      private ExcelDocumentI AV9ExcelDocument ;
   }

   public class exportwwplan__datastore1 : DataStoreHelperBase, IDataStoreHelper
   {
      protected Object[] conditional_P000D2( IGxContext context ,
                                             String AV15nombreEvento ,
                                             String A30nombreEvento )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int2 ;
         GXv_int2 = new short [1] ;
         Object[] GXv_Object3 ;
         GXv_Object3 = new Object [2] ;
         scmdbuf = "SELECT T1.[idLugar] AS Comunidadid, T1.[idEstado] AS Estadoid, T1.[nombreEvento], T2.[nombre] AS Comunidadnombre, T1.[inicio], T1.[fin], T3.[nombre] AS Estadonombre, T1.[cantBeneficiarios], T1.[id] AS Planid FROM ((dbo.[Plan] T1 WITH (NOLOCK) INNER JOIN dbo.[Comunidad] T2 WITH (NOLOCK) ON T2.[id] = T1.[idLugar]) INNER JOIN dbo.[Estado] T3 WITH (NOLOCK) ON T3.[id] = T1.[idEstado])";
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV15nombreEvento)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[nombreEvento] like @lV15nombreEvento)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[nombreEvento] like @lV15nombreEvento)";
            }
         }
         else
         {
            GXv_int2[0] = 1;
         }
         if ( StringUtil.StrCmp("", sWhereString) != 0 )
         {
            scmdbuf = scmdbuf + " WHERE" + sWhereString;
         }
         scmdbuf = scmdbuf + " ORDER BY T1.[nombreEvento]";
         GXv_Object3[0] = scmdbuf;
         GXv_Object3[1] = GXv_int2;
         return GXv_Object3 ;
      }

      public override Object [] getDynamicStatement( int cursor ,
                                                     IGxContext context ,
                                                     Object [] dynConstraints )
      {
         switch ( cursor )
         {
               case 0 :
                     return conditional_P000D2(context, (String)dynConstraints[0] , (String)dynConstraints[1] );
         }
         return base.getDynamicStatement(cursor, context, dynConstraints);
      }

      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmP000D2 ;
          prmP000D2 = new Object[] {
          new Object[] {"@lV15nombreEvento",SqlDbType.VarChar,50,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("P000D2", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP000D2,100,0,false,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((String[]) buf[2])[0] = rslt.getVarchar(3) ;
                ((String[]) buf[3])[0] = rslt.getVarchar(4) ;
                ((DateTime[]) buf[4])[0] = rslt.getGXDate(5) ;
                ((DateTime[]) buf[5])[0] = rslt.getGXDate(6) ;
                ((String[]) buf[6])[0] = rslt.getVarchar(7) ;
                ((int[]) buf[7])[0] = rslt.getInt(8) ;
                ((bool[]) buf[8])[0] = rslt.wasNull(8);
                ((int[]) buf[9])[0] = rslt.getInt(9) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       short sIdx ;
       switch ( cursor )
       {
             case 0 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[1]);
                }
                return;
       }
    }

    public String getDataStoreName( )
    {
       return "DATASTORE1";
    }

 }

}
