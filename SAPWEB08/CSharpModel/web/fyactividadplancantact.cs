/*
               File: FYActividadPlanCantAct
        Description: FYActividad Plan Cant Act
             Author: GeneXus C# Generator version 16_0_0-127771
       Generated on: 1/3/2019 18:1:56.17
       Program type: Callable routine
          Main DBMS: SQL Server
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Xml.Serialization;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class fyactividadplancantact : GXDataArea, System.Web.SessionState.IRequiresSessionState
   {
      protected void INITENV( )
      {
         if ( GxWebError != 0 )
         {
            return  ;
         }
      }

      protected void INITTRN( )
      {
         initialize_properties( ) ;
         entryPointCalled = false;
         gxfirstwebparm = GetNextPar( );
         gxfirstwebparm_bkp = gxfirstwebparm;
         gxfirstwebparm = DecryptAjaxCall( gxfirstwebparm);
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         if ( StringUtil.StrCmp(gxfirstwebparm, "dyncall") == 0 )
         {
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            dyncall( GetNextPar( )) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxExecAct_"+"gxLoad_2") == 0 )
         {
            A23ActividadParaPlanid = (int)(NumberUtil.Val( GetNextPar( ), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A23ActividadParaPlanid", StringUtil.LTrim( StringUtil.Str( (decimal)(A23ActividadParaPlanid), 9, 0)));
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxLoad_2( A23ActividadParaPlanid) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxEvt") == 0 )
         {
            setAjaxEventMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxfirstwebparm = GetNextPar( );
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxfullajaxEvt") == 0 )
         {
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxfirstwebparm = GetNextPar( );
         }
         else
         {
            if ( ! IsValidAjaxCall( false) )
            {
               GxWebError = 1;
               return  ;
            }
            gxfirstwebparm = gxfirstwebparm_bkp;
         }
         if ( context.isSpaRequest( ) )
         {
            enableJsOutput();
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.GetCookie( "GX_SESSION_ID"))) )
         {
            gxcookieaux = context.SetCookie( "GX_SESSION_ID", Encrypt64( Crypto.GetEncryptionKey( ), Crypto.GetServerKey( )), "", (DateTime)(DateTime.MinValue), "", 0);
         }
         GXKey = Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
         toggleJsOutput = isJsOutputEnabled( );
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         init_web_controls( ) ;
         if ( toggleJsOutput )
         {
            if ( context.isSpaRequest( ) )
            {
               enableJsOutput();
            }
         }
         if ( ! context.isSpaRequest( ) )
         {
            Form.Meta.addItem("generator", "GeneXus C# 16_0_0-127771", 0) ;
            Form.Meta.addItem("description", "FYActividad Plan Cant Act", 0) ;
         }
         context.wjLoc = "";
         context.nUserReturn = 0;
         context.wbHandled = 0;
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
         }
         GX_FocusControl = edtFYActividadPlanCantActid_Internalname;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         wbErr = false;
         context.SetDefaultTheme("Carmine");
         if ( ! context.IsLocalStorageSupported( ) )
         {
            context.PushCurrentUrl();
         }
      }

      public fyactividadplancantact( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsDataStore1 = context.GetDataStore("DataStore1");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("Carmine");
      }

      public fyactividadplancantact( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsDataStore1 = context.GetDataStore("DataStore1");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( )
      {
         executePrivate();
      }

      void executePrivate( )
      {
         isStatic = false;
         webExecute();
      }

      protected override void createObjects( )
      {
      }

      public override void webExecute( )
      {
         if ( initialized == 0 )
         {
            createObjects();
            initialize();
         }
         INITENV( ) ;
         INITTRN( ) ;
         if ( ( GxWebError == 0 ) && ! isAjaxCallMode( ) )
         {
            MasterPageObj = (GXMasterPage) ClassLoader.GetInstance("rwdmasterpage", "GeneXus.Programs.rwdmasterpage", new Object[] {new GxContext( context.handle, context.DataStores, context.HttpContext)});
            MasterPageObj.setDataArea(this,false);
            ValidateSpaRequest();
            MasterPageObj.webExecute();
            if ( ( GxWebError == 0 ) && context.isAjaxRequest( ) )
            {
               enableOutput();
               if ( ! context.isAjaxRequest( ) )
               {
                  context.GX_webresponse.AppendHeader("Cache-Control", "no-store");
               }
               if ( ! context.WillRedirect( ) )
               {
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               }
               else
               {
                  if ( context.isAjaxRequest( ) )
                  {
                     disableOutput();
                  }
                  RenderHtmlHeaders( ) ;
                  context.Redirect( context.wjLoc );
                  context.DispatchAjaxCommands();
               }
            }
         }
         this.cleanup();
      }

      protected void fix_multi_value_controls( )
      {
      }

      protected void Draw( )
      {
         if ( context.isAjaxRequest( ) )
         {
            disableOutput();
         }
         if ( ! GxWebStd.gx_redirect( context) )
         {
            disable_std_buttons( ) ;
            enableDisable( ) ;
            set_caption( ) ;
            /* Form start */
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "Section", "left", "top", " "+"data-gx-base-lib=\"bootstrapv3\""+" "+"data-abstract-form"+" ", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, divMaintable_Internalname, 1, 0, "px", 0, "px", "WWAdvancedContainer", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12 col-sm-8 col-sm-offset-2", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, divTitlecontainer_Internalname, 1, 0, "px", 0, "px", "TableTop", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12", "left", "top", "", "", "div");
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTitle_Internalname, "FYActividad Plan Cant Act", "", "", lblTitle_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "Title", 0, "", 1, 1, 0, "HLP_FYActividadPlanCantAct.htm");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12", "left", "top", "", "", "div");
            ClassString = "ErrorViewer";
            StyleString = "";
            GxWebStd.gx_msg_list( context, "", context.GX_msglist.DisplayMode, StyleString, ClassString, "", "false");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12 col-sm-8 col-sm-offset-2", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, divFormcontainer_Internalname, 1, 0, "px", 0, "px", "FormContainer", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, divToolbarcell_Internalname, 1, 0, "px", 0, "px", "col-xs-12 col-sm-9 col-sm-offset-3 ToolbarCellClass", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "gx-action-group ActionGroup", "left", "top", " "+"data-gx-actiongroup-type=\"toolbar\""+" ", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "btn-group", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "gx-button", "left", "top", "", "", "div");
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 21,'',false,'',0)\"";
            ClassString = "BtnFirst";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtn_first_Internalname, "", "", bttBtn_first_Jsonclick, 5, "", "", StyleString, ClassString, bttBtn_first_Visible, 1, "standard", "'"+""+"'"+",false,"+"'"+"EFIRST."+"'", TempTags, "", context.GetButtonType( ), "HLP_FYActividadPlanCantAct.htm");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "gx-button", "left", "top", "", "", "div");
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 23,'',false,'',0)\"";
            ClassString = "BtnPrevious";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtn_previous_Internalname, "", "", bttBtn_previous_Jsonclick, 5, "", "", StyleString, ClassString, bttBtn_previous_Visible, 1, "standard", "'"+""+"'"+",false,"+"'"+"EPREVIOUS."+"'", TempTags, "", context.GetButtonType( ), "HLP_FYActividadPlanCantAct.htm");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "gx-button", "left", "top", "", "", "div");
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 25,'',false,'',0)\"";
            ClassString = "BtnNext";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtn_next_Internalname, "", "", bttBtn_next_Jsonclick, 5, "", "", StyleString, ClassString, bttBtn_next_Visible, 1, "standard", "'"+""+"'"+",false,"+"'"+"ENEXT."+"'", TempTags, "", context.GetButtonType( ), "HLP_FYActividadPlanCantAct.htm");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "gx-button", "left", "top", "", "", "div");
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 27,'',false,'',0)\"";
            ClassString = "BtnLast";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtn_last_Internalname, "", "", bttBtn_last_Jsonclick, 5, "", "", StyleString, ClassString, bttBtn_last_Visible, 1, "standard", "'"+""+"'"+",false,"+"'"+"ELAST."+"'", TempTags, "", context.GetButtonType( ), "HLP_FYActividadPlanCantAct.htm");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "gx-button", "left", "top", "", "", "div");
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 29,'',false,'',0)\"";
            ClassString = "BtnSelect";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtn_select_Internalname, "", "Select", bttBtn_select_Jsonclick, 4, "Select", "", StyleString, ClassString, bttBtn_select_Visible, 1, "standard", "'"+""+"'"+",false,"+"'"+"ESELECT."+"'", TempTags, "gx.popup.openPrompt('"+"gx00n0.aspx"+"',["+"{Ctrl:gx.dom.el('"+"FYACTIVIDADPLANCANTACTID"+"'), id:'"+"FYACTIVIDADPLANCANTACTID"+"'"+",IOType:'out',isKey:true,isLastKey:true}"+"],"+"null"+","+"'', false"+","+"true"+");"+"return false;", 2, "HLP_FYActividadPlanCantAct.htm");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12 FormCellAdvanced", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "form-group gx-form-group", "left", "top", ""+" data-gx-for=\""+edtFYActividadPlanCantActid_Internalname+"\"", "", "div");
            /* Attribute/Variable Label */
            GxWebStd.gx_label_element( context, edtFYActividadPlanCantActid_Internalname, "id", "col-sm-3 AttributeLabel", 1, true);
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-sm-9 gx-attribute", "left", "top", "", "", "div");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 34,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtFYActividadPlanCantActid_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A24FYActividadPlanCantActid), 9, 0, ".", "")), ((edtFYActividadPlanCantActid_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( (decimal)(A24FYActividadPlanCantActid), "ZZZZZZZZ9")) : context.localUtil.Format( (decimal)(A24FYActividadPlanCantActid), "ZZZZZZZZ9")), TempTags+" onchange=\""+"gx.num.valid_integer( this,',');"+";gx.evt.onchange(this, event)\" "+" onblur=\""+";gx.evt.onblur(this,34);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtFYActividadPlanCantActid_Jsonclick, 0, "Attribute", "", "", "", "", 1, edtFYActividadPlanCantActid_Enabled, 0, "number", "1", 9, "chr", 1, "row", 9, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_FYActividadPlanCantAct.htm");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12 FormCell", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "form-group gx-form-group", "left", "top", ""+" data-gx-for=\""+edtActividadParaPlanid_Internalname+"\"", "", "div");
            /* Attribute/Variable Label */
            GxWebStd.gx_label_element( context, edtActividadParaPlanid_Internalname, "Actividad Para Planid", "col-sm-3 AttributeLabel", 1, true);
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-sm-9 gx-attribute", "left", "top", "", "", "div");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 39,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtActividadParaPlanid_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A23ActividadParaPlanid), 9, 0, ".", "")), ((edtActividadParaPlanid_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( (decimal)(A23ActividadParaPlanid), "ZZZZZZZZ9")) : context.localUtil.Format( (decimal)(A23ActividadParaPlanid), "ZZZZZZZZ9")), TempTags+" onchange=\""+"gx.num.valid_integer( this,',');"+";gx.evt.onchange(this, event)\" "+" onblur=\""+";gx.evt.onblur(this,39);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtActividadParaPlanid_Jsonclick, 0, "Attribute", "", "", "", "", 1, edtActividadParaPlanid_Enabled, 0, "number", "1", 9, "chr", 1, "row", 9, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_FYActividadPlanCantAct.htm");
            /* Static images/pictures */
            ClassString = "gx-prompt Image";
            StyleString = "";
            sImgUrl = (String)(context.GetImagePath( "prompt.gif", "", context.GetTheme( )));
            GxWebStd.gx_bitmap( context, imgprompt_23_Internalname, sImgUrl, imgprompt_23_Link, "", "", context.GetTheme( ), imgprompt_23_Visible, 1, "", "", 0, 0, 0, "", 0, "", 0, 0, 0, "", "", StyleString, ClassString, "", "", "", "", "", "", "", 1, false, false, context.GetImageSrcSet( sImgUrl), "HLP_FYActividadPlanCantAct.htm");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12 FormCell", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "form-group gx-form-group", "left", "top", ""+" data-gx-for=\""+edtFYActividadPlanCantActFY_Internalname+"\"", "", "div");
            /* Attribute/Variable Label */
            GxWebStd.gx_label_element( context, edtFYActividadPlanCantActFY_Internalname, "FY", "col-sm-3 AttributeLabel", 1, true);
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-sm-9 gx-attribute", "left", "top", "", "", "div");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 44,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtFYActividadPlanCantActFY_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A104FYActividadPlanCantActFY), 9, 0, ".", "")), ((edtFYActividadPlanCantActFY_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( (decimal)(A104FYActividadPlanCantActFY), "ZZZZZZZZ9")) : context.localUtil.Format( (decimal)(A104FYActividadPlanCantActFY), "ZZZZZZZZ9")), TempTags+" onchange=\""+"gx.num.valid_integer( this,',');"+";gx.evt.onchange(this, event)\" "+" onblur=\""+";gx.evt.onblur(this,44);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtFYActividadPlanCantActFY_Jsonclick, 0, "Attribute", "", "", "", "", 1, edtFYActividadPlanCantActFY_Enabled, 0, "number", "1", 9, "chr", 1, "row", 9, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_FYActividadPlanCantAct.htm");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12 FormCell", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "form-group gx-form-group", "left", "top", ""+" data-gx-for=\""+edtmes1_Internalname+"\"", "", "div");
            /* Attribute/Variable Label */
            GxWebStd.gx_label_element( context, edtmes1_Internalname, "mes1", "col-sm-3 AttributeLabel", 1, true);
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-sm-9 gx-attribute", "left", "top", "", "", "div");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 49,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtmes1_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A105mes1), 9, 0, ".", "")), ((edtmes1_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( (decimal)(A105mes1), "ZZZZZZZZ9")) : context.localUtil.Format( (decimal)(A105mes1), "ZZZZZZZZ9")), TempTags+" onchange=\""+"gx.num.valid_integer( this,',');"+";gx.evt.onchange(this, event)\" "+" onblur=\""+";gx.evt.onblur(this,49);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtmes1_Jsonclick, 0, "Attribute", "", "", "", "", 1, edtmes1_Enabled, 0, "number", "1", 9, "chr", 1, "row", 9, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_FYActividadPlanCantAct.htm");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12 FormCell", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "form-group gx-form-group", "left", "top", ""+" data-gx-for=\""+edtmes2_Internalname+"\"", "", "div");
            /* Attribute/Variable Label */
            GxWebStd.gx_label_element( context, edtmes2_Internalname, "mes2", "col-sm-3 AttributeLabel", 1, true);
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-sm-9 gx-attribute", "left", "top", "", "", "div");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 54,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtmes2_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A106mes2), 9, 0, ".", "")), ((edtmes2_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( (decimal)(A106mes2), "ZZZZZZZZ9")) : context.localUtil.Format( (decimal)(A106mes2), "ZZZZZZZZ9")), TempTags+" onchange=\""+"gx.num.valid_integer( this,',');"+";gx.evt.onchange(this, event)\" "+" onblur=\""+";gx.evt.onblur(this,54);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtmes2_Jsonclick, 0, "Attribute", "", "", "", "", 1, edtmes2_Enabled, 0, "number", "1", 9, "chr", 1, "row", 9, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_FYActividadPlanCantAct.htm");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12 FormCell", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "form-group gx-form-group", "left", "top", ""+" data-gx-for=\""+edtmes3_Internalname+"\"", "", "div");
            /* Attribute/Variable Label */
            GxWebStd.gx_label_element( context, edtmes3_Internalname, "mes3", "col-sm-3 AttributeLabel", 1, true);
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-sm-9 gx-attribute", "left", "top", "", "", "div");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 59,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtmes3_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A107mes3), 9, 0, ".", "")), ((edtmes3_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( (decimal)(A107mes3), "ZZZZZZZZ9")) : context.localUtil.Format( (decimal)(A107mes3), "ZZZZZZZZ9")), TempTags+" onchange=\""+"gx.num.valid_integer( this,',');"+";gx.evt.onchange(this, event)\" "+" onblur=\""+";gx.evt.onblur(this,59);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtmes3_Jsonclick, 0, "Attribute", "", "", "", "", 1, edtmes3_Enabled, 0, "number", "1", 9, "chr", 1, "row", 9, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_FYActividadPlanCantAct.htm");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12 FormCell", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "form-group gx-form-group", "left", "top", ""+" data-gx-for=\""+edtmes4_Internalname+"\"", "", "div");
            /* Attribute/Variable Label */
            GxWebStd.gx_label_element( context, edtmes4_Internalname, "mes4", "col-sm-3 AttributeLabel", 1, true);
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-sm-9 gx-attribute", "left", "top", "", "", "div");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 64,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtmes4_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A108mes4), 9, 0, ".", "")), ((edtmes4_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( (decimal)(A108mes4), "ZZZZZZZZ9")) : context.localUtil.Format( (decimal)(A108mes4), "ZZZZZZZZ9")), TempTags+" onchange=\""+"gx.num.valid_integer( this,',');"+";gx.evt.onchange(this, event)\" "+" onblur=\""+";gx.evt.onblur(this,64);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtmes4_Jsonclick, 0, "Attribute", "", "", "", "", 1, edtmes4_Enabled, 0, "number", "1", 9, "chr", 1, "row", 9, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_FYActividadPlanCantAct.htm");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12 FormCell", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "form-group gx-form-group", "left", "top", ""+" data-gx-for=\""+edtmes5_Internalname+"\"", "", "div");
            /* Attribute/Variable Label */
            GxWebStd.gx_label_element( context, edtmes5_Internalname, "mes5", "col-sm-3 AttributeLabel", 1, true);
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-sm-9 gx-attribute", "left", "top", "", "", "div");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 69,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtmes5_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A109mes5), 9, 0, ".", "")), ((edtmes5_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( (decimal)(A109mes5), "ZZZZZZZZ9")) : context.localUtil.Format( (decimal)(A109mes5), "ZZZZZZZZ9")), TempTags+" onchange=\""+"gx.num.valid_integer( this,',');"+";gx.evt.onchange(this, event)\" "+" onblur=\""+";gx.evt.onblur(this,69);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtmes5_Jsonclick, 0, "Attribute", "", "", "", "", 1, edtmes5_Enabled, 0, "number", "1", 9, "chr", 1, "row", 9, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_FYActividadPlanCantAct.htm");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12 FormCell", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "form-group gx-form-group", "left", "top", ""+" data-gx-for=\""+edtmes6_Internalname+"\"", "", "div");
            /* Attribute/Variable Label */
            GxWebStd.gx_label_element( context, edtmes6_Internalname, "mes6", "col-sm-3 AttributeLabel", 1, true);
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-sm-9 gx-attribute", "left", "top", "", "", "div");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 74,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtmes6_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A110mes6), 9, 0, ".", "")), ((edtmes6_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( (decimal)(A110mes6), "ZZZZZZZZ9")) : context.localUtil.Format( (decimal)(A110mes6), "ZZZZZZZZ9")), TempTags+" onchange=\""+"gx.num.valid_integer( this,',');"+";gx.evt.onchange(this, event)\" "+" onblur=\""+";gx.evt.onblur(this,74);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtmes6_Jsonclick, 0, "Attribute", "", "", "", "", 1, edtmes6_Enabled, 0, "number", "1", 9, "chr", 1, "row", 9, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_FYActividadPlanCantAct.htm");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12 FormCell", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "form-group gx-form-group", "left", "top", ""+" data-gx-for=\""+edtmes7_Internalname+"\"", "", "div");
            /* Attribute/Variable Label */
            GxWebStd.gx_label_element( context, edtmes7_Internalname, "mes7", "col-sm-3 AttributeLabel", 1, true);
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-sm-9 gx-attribute", "left", "top", "", "", "div");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 79,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtmes7_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A111mes7), 9, 0, ".", "")), ((edtmes7_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( (decimal)(A111mes7), "ZZZZZZZZ9")) : context.localUtil.Format( (decimal)(A111mes7), "ZZZZZZZZ9")), TempTags+" onchange=\""+"gx.num.valid_integer( this,',');"+";gx.evt.onchange(this, event)\" "+" onblur=\""+";gx.evt.onblur(this,79);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtmes7_Jsonclick, 0, "Attribute", "", "", "", "", 1, edtmes7_Enabled, 0, "number", "1", 9, "chr", 1, "row", 9, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_FYActividadPlanCantAct.htm");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12 FormCell", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "form-group gx-form-group", "left", "top", ""+" data-gx-for=\""+edtmes8_Internalname+"\"", "", "div");
            /* Attribute/Variable Label */
            GxWebStd.gx_label_element( context, edtmes8_Internalname, "mes8", "col-sm-3 AttributeLabel", 1, true);
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-sm-9 gx-attribute", "left", "top", "", "", "div");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 84,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtmes8_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A112mes8), 9, 0, ".", "")), ((edtmes8_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( (decimal)(A112mes8), "ZZZZZZZZ9")) : context.localUtil.Format( (decimal)(A112mes8), "ZZZZZZZZ9")), TempTags+" onchange=\""+"gx.num.valid_integer( this,',');"+";gx.evt.onchange(this, event)\" "+" onblur=\""+";gx.evt.onblur(this,84);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtmes8_Jsonclick, 0, "Attribute", "", "", "", "", 1, edtmes8_Enabled, 0, "number", "1", 9, "chr", 1, "row", 9, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_FYActividadPlanCantAct.htm");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12 FormCell", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "form-group gx-form-group", "left", "top", ""+" data-gx-for=\""+edtmes9_Internalname+"\"", "", "div");
            /* Attribute/Variable Label */
            GxWebStd.gx_label_element( context, edtmes9_Internalname, "mes9", "col-sm-3 AttributeLabel", 1, true);
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-sm-9 gx-attribute", "left", "top", "", "", "div");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 89,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtmes9_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A113mes9), 9, 0, ".", "")), ((edtmes9_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( (decimal)(A113mes9), "ZZZZZZZZ9")) : context.localUtil.Format( (decimal)(A113mes9), "ZZZZZZZZ9")), TempTags+" onchange=\""+"gx.num.valid_integer( this,',');"+";gx.evt.onchange(this, event)\" "+" onblur=\""+";gx.evt.onblur(this,89);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtmes9_Jsonclick, 0, "Attribute", "", "", "", "", 1, edtmes9_Enabled, 0, "number", "1", 9, "chr", 1, "row", 9, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_FYActividadPlanCantAct.htm");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12 FormCell", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "form-group gx-form-group", "left", "top", ""+" data-gx-for=\""+edtmes10_Internalname+"\"", "", "div");
            /* Attribute/Variable Label */
            GxWebStd.gx_label_element( context, edtmes10_Internalname, "mes10", "col-sm-3 AttributeLabel", 1, true);
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-sm-9 gx-attribute", "left", "top", "", "", "div");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 94,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtmes10_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A114mes10), 9, 0, ".", "")), ((edtmes10_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( (decimal)(A114mes10), "ZZZZZZZZ9")) : context.localUtil.Format( (decimal)(A114mes10), "ZZZZZZZZ9")), TempTags+" onchange=\""+"gx.num.valid_integer( this,',');"+";gx.evt.onchange(this, event)\" "+" onblur=\""+";gx.evt.onblur(this,94);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtmes10_Jsonclick, 0, "Attribute", "", "", "", "", 1, edtmes10_Enabled, 0, "number", "1", 9, "chr", 1, "row", 9, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_FYActividadPlanCantAct.htm");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12 FormCell", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "form-group gx-form-group", "left", "top", ""+" data-gx-for=\""+edtmes11_Internalname+"\"", "", "div");
            /* Attribute/Variable Label */
            GxWebStd.gx_label_element( context, edtmes11_Internalname, "mes11", "col-sm-3 AttributeLabel", 1, true);
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-sm-9 gx-attribute", "left", "top", "", "", "div");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 99,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtmes11_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A115mes11), 9, 0, ".", "")), ((edtmes11_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( (decimal)(A115mes11), "ZZZZZZZZ9")) : context.localUtil.Format( (decimal)(A115mes11), "ZZZZZZZZ9")), TempTags+" onchange=\""+"gx.num.valid_integer( this,',');"+";gx.evt.onchange(this, event)\" "+" onblur=\""+";gx.evt.onblur(this,99);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtmes11_Jsonclick, 0, "Attribute", "", "", "", "", 1, edtmes11_Enabled, 0, "number", "1", 9, "chr", 1, "row", 9, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_FYActividadPlanCantAct.htm");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12 FormCell", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "form-group gx-form-group", "left", "top", ""+" data-gx-for=\""+edtmes12_Internalname+"\"", "", "div");
            /* Attribute/Variable Label */
            GxWebStd.gx_label_element( context, edtmes12_Internalname, "mes12", "col-sm-3 AttributeLabel", 1, true);
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-sm-9 gx-attribute", "left", "top", "", "", "div");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 104,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtmes12_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A116mes12), 9, 0, ".", "")), ((edtmes12_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( (decimal)(A116mes12), "ZZZZZZZZ9")) : context.localUtil.Format( (decimal)(A116mes12), "ZZZZZZZZ9")), TempTags+" onchange=\""+"gx.num.valid_integer( this,',');"+";gx.evt.onchange(this, event)\" "+" onblur=\""+";gx.evt.onblur(this,104);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtmes12_Jsonclick, 0, "Attribute", "", "", "", "", 1, edtmes12_Enabled, 0, "number", "1", 9, "chr", 1, "row", 9, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_FYActividadPlanCantAct.htm");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12", "Center", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "gx-action-group Confirm", "left", "top", " "+"data-gx-actiongroup-type=\"toolbar\""+" ", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "gx-button", "left", "top", "", "", "div");
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 109,'',false,'',0)\"";
            ClassString = "BtnEnter";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtn_enter_Internalname, "", "Confirm", bttBtn_enter_Jsonclick, 5, "Confirm", "", StyleString, ClassString, bttBtn_enter_Visible, bttBtn_enter_Enabled, "standard", "'"+""+"'"+",false,"+"'"+"EENTER."+"'", TempTags, "", context.GetButtonType( ), "HLP_FYActividadPlanCantAct.htm");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "gx-button", "left", "top", "", "", "div");
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 111,'',false,'',0)\"";
            ClassString = "BtnCancel";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtn_cancel_Internalname, "", "Cancel", bttBtn_cancel_Jsonclick, 1, "Cancel", "", StyleString, ClassString, bttBtn_cancel_Visible, 1, "standard", "'"+""+"'"+",false,"+"'"+"ECANCEL."+"'", TempTags, "", context.GetButtonType( ), "HLP_FYActividadPlanCantAct.htm");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "gx-button", "left", "top", "", "", "div");
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 113,'',false,'',0)\"";
            ClassString = "BtnDelete";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtn_delete_Internalname, "", "Delete", bttBtn_delete_Jsonclick, 5, "Delete", "", StyleString, ClassString, bttBtn_delete_Visible, bttBtn_delete_Enabled, "standard", "'"+""+"'"+",false,"+"'"+"EDELETE."+"'", TempTags, "", context.GetButtonType( ), "HLP_FYActividadPlanCantAct.htm");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "Center", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            fix_multi_value_controls( ) ;
         }
         /* Execute Exit event if defined. */
      }

      protected void UserMain( )
      {
         standaloneStartup( ) ;
      }

      protected void UserMainFullajax( )
      {
         INITENV( ) ;
         INITTRN( ) ;
         UserMain( ) ;
         Draw( ) ;
         SendCloseFormHiddens( ) ;
      }

      protected void standaloneStartup( )
      {
         standaloneStartupServer( ) ;
         disable_std_buttons( ) ;
         enableDisable( ) ;
         Process( ) ;
      }

      protected void standaloneStartupServer( )
      {
         /* Execute Start event if defined. */
         context.wbGlbDoneStart = 0;
         context.wbGlbDoneStart = 1;
         assign_properties_default( ) ;
         if ( AnyError == 0 )
         {
            if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
            {
               /* Read saved SDTs. */
               /* Read variables values. */
               if ( ( ( context.localUtil.CToN( cgiGet( edtFYActividadPlanCantActid_Internalname), ".", ",") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtFYActividadPlanCantActid_Internalname), ".", ",") > Convert.ToDecimal( 999999999 )) ) )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "FYACTIVIDADPLANCANTACTID");
                  AnyError = 1;
                  GX_FocusControl = edtFYActividadPlanCantActid_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  wbErr = true;
                  A24FYActividadPlanCantActid = 0;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A24FYActividadPlanCantActid", StringUtil.LTrim( StringUtil.Str( (decimal)(A24FYActividadPlanCantActid), 9, 0)));
               }
               else
               {
                  A24FYActividadPlanCantActid = (int)(context.localUtil.CToN( cgiGet( edtFYActividadPlanCantActid_Internalname), ".", ","));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A24FYActividadPlanCantActid", StringUtil.LTrim( StringUtil.Str( (decimal)(A24FYActividadPlanCantActid), 9, 0)));
               }
               if ( ( ( context.localUtil.CToN( cgiGet( edtActividadParaPlanid_Internalname), ".", ",") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtActividadParaPlanid_Internalname), ".", ",") > Convert.ToDecimal( 999999999 )) ) )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "ACTIVIDADPARAPLANID");
                  AnyError = 1;
                  GX_FocusControl = edtActividadParaPlanid_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  wbErr = true;
                  A23ActividadParaPlanid = 0;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A23ActividadParaPlanid", StringUtil.LTrim( StringUtil.Str( (decimal)(A23ActividadParaPlanid), 9, 0)));
               }
               else
               {
                  A23ActividadParaPlanid = (int)(context.localUtil.CToN( cgiGet( edtActividadParaPlanid_Internalname), ".", ","));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A23ActividadParaPlanid", StringUtil.LTrim( StringUtil.Str( (decimal)(A23ActividadParaPlanid), 9, 0)));
               }
               if ( ( ( context.localUtil.CToN( cgiGet( edtFYActividadPlanCantActFY_Internalname), ".", ",") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtFYActividadPlanCantActFY_Internalname), ".", ",") > Convert.ToDecimal( 999999999 )) ) )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "FYACTIVIDADPLANCANTACTFY");
                  AnyError = 1;
                  GX_FocusControl = edtFYActividadPlanCantActFY_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  wbErr = true;
                  A104FYActividadPlanCantActFY = 0;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A104FYActividadPlanCantActFY", StringUtil.LTrim( StringUtil.Str( (decimal)(A104FYActividadPlanCantActFY), 9, 0)));
               }
               else
               {
                  A104FYActividadPlanCantActFY = (int)(context.localUtil.CToN( cgiGet( edtFYActividadPlanCantActFY_Internalname), ".", ","));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A104FYActividadPlanCantActFY", StringUtil.LTrim( StringUtil.Str( (decimal)(A104FYActividadPlanCantActFY), 9, 0)));
               }
               if ( ( ( context.localUtil.CToN( cgiGet( edtmes1_Internalname), ".", ",") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtmes1_Internalname), ".", ",") > Convert.ToDecimal( 999999999 )) ) )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "MES1");
                  AnyError = 1;
                  GX_FocusControl = edtmes1_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  wbErr = true;
                  A105mes1 = 0;
                  n105mes1 = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A105mes1", StringUtil.LTrim( StringUtil.Str( (decimal)(A105mes1), 9, 0)));
               }
               else
               {
                  A105mes1 = (int)(context.localUtil.CToN( cgiGet( edtmes1_Internalname), ".", ","));
                  n105mes1 = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A105mes1", StringUtil.LTrim( StringUtil.Str( (decimal)(A105mes1), 9, 0)));
               }
               n105mes1 = ((0==A105mes1) ? true : false);
               if ( ( ( context.localUtil.CToN( cgiGet( edtmes2_Internalname), ".", ",") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtmes2_Internalname), ".", ",") > Convert.ToDecimal( 999999999 )) ) )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "MES2");
                  AnyError = 1;
                  GX_FocusControl = edtmes2_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  wbErr = true;
                  A106mes2 = 0;
                  n106mes2 = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A106mes2", StringUtil.LTrim( StringUtil.Str( (decimal)(A106mes2), 9, 0)));
               }
               else
               {
                  A106mes2 = (int)(context.localUtil.CToN( cgiGet( edtmes2_Internalname), ".", ","));
                  n106mes2 = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A106mes2", StringUtil.LTrim( StringUtil.Str( (decimal)(A106mes2), 9, 0)));
               }
               n106mes2 = ((0==A106mes2) ? true : false);
               if ( ( ( context.localUtil.CToN( cgiGet( edtmes3_Internalname), ".", ",") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtmes3_Internalname), ".", ",") > Convert.ToDecimal( 999999999 )) ) )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "MES3");
                  AnyError = 1;
                  GX_FocusControl = edtmes3_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  wbErr = true;
                  A107mes3 = 0;
                  n107mes3 = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A107mes3", StringUtil.LTrim( StringUtil.Str( (decimal)(A107mes3), 9, 0)));
               }
               else
               {
                  A107mes3 = (int)(context.localUtil.CToN( cgiGet( edtmes3_Internalname), ".", ","));
                  n107mes3 = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A107mes3", StringUtil.LTrim( StringUtil.Str( (decimal)(A107mes3), 9, 0)));
               }
               n107mes3 = ((0==A107mes3) ? true : false);
               if ( ( ( context.localUtil.CToN( cgiGet( edtmes4_Internalname), ".", ",") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtmes4_Internalname), ".", ",") > Convert.ToDecimal( 999999999 )) ) )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "MES4");
                  AnyError = 1;
                  GX_FocusControl = edtmes4_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  wbErr = true;
                  A108mes4 = 0;
                  n108mes4 = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A108mes4", StringUtil.LTrim( StringUtil.Str( (decimal)(A108mes4), 9, 0)));
               }
               else
               {
                  A108mes4 = (int)(context.localUtil.CToN( cgiGet( edtmes4_Internalname), ".", ","));
                  n108mes4 = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A108mes4", StringUtil.LTrim( StringUtil.Str( (decimal)(A108mes4), 9, 0)));
               }
               n108mes4 = ((0==A108mes4) ? true : false);
               if ( ( ( context.localUtil.CToN( cgiGet( edtmes5_Internalname), ".", ",") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtmes5_Internalname), ".", ",") > Convert.ToDecimal( 999999999 )) ) )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "MES5");
                  AnyError = 1;
                  GX_FocusControl = edtmes5_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  wbErr = true;
                  A109mes5 = 0;
                  n109mes5 = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A109mes5", StringUtil.LTrim( StringUtil.Str( (decimal)(A109mes5), 9, 0)));
               }
               else
               {
                  A109mes5 = (int)(context.localUtil.CToN( cgiGet( edtmes5_Internalname), ".", ","));
                  n109mes5 = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A109mes5", StringUtil.LTrim( StringUtil.Str( (decimal)(A109mes5), 9, 0)));
               }
               n109mes5 = ((0==A109mes5) ? true : false);
               if ( ( ( context.localUtil.CToN( cgiGet( edtmes6_Internalname), ".", ",") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtmes6_Internalname), ".", ",") > Convert.ToDecimal( 999999999 )) ) )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "MES6");
                  AnyError = 1;
                  GX_FocusControl = edtmes6_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  wbErr = true;
                  A110mes6 = 0;
                  n110mes6 = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A110mes6", StringUtil.LTrim( StringUtil.Str( (decimal)(A110mes6), 9, 0)));
               }
               else
               {
                  A110mes6 = (int)(context.localUtil.CToN( cgiGet( edtmes6_Internalname), ".", ","));
                  n110mes6 = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A110mes6", StringUtil.LTrim( StringUtil.Str( (decimal)(A110mes6), 9, 0)));
               }
               n110mes6 = ((0==A110mes6) ? true : false);
               if ( ( ( context.localUtil.CToN( cgiGet( edtmes7_Internalname), ".", ",") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtmes7_Internalname), ".", ",") > Convert.ToDecimal( 999999999 )) ) )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "MES7");
                  AnyError = 1;
                  GX_FocusControl = edtmes7_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  wbErr = true;
                  A111mes7 = 0;
                  n111mes7 = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A111mes7", StringUtil.LTrim( StringUtil.Str( (decimal)(A111mes7), 9, 0)));
               }
               else
               {
                  A111mes7 = (int)(context.localUtil.CToN( cgiGet( edtmes7_Internalname), ".", ","));
                  n111mes7 = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A111mes7", StringUtil.LTrim( StringUtil.Str( (decimal)(A111mes7), 9, 0)));
               }
               n111mes7 = ((0==A111mes7) ? true : false);
               if ( ( ( context.localUtil.CToN( cgiGet( edtmes8_Internalname), ".", ",") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtmes8_Internalname), ".", ",") > Convert.ToDecimal( 999999999 )) ) )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "MES8");
                  AnyError = 1;
                  GX_FocusControl = edtmes8_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  wbErr = true;
                  A112mes8 = 0;
                  n112mes8 = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A112mes8", StringUtil.LTrim( StringUtil.Str( (decimal)(A112mes8), 9, 0)));
               }
               else
               {
                  A112mes8 = (int)(context.localUtil.CToN( cgiGet( edtmes8_Internalname), ".", ","));
                  n112mes8 = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A112mes8", StringUtil.LTrim( StringUtil.Str( (decimal)(A112mes8), 9, 0)));
               }
               n112mes8 = ((0==A112mes8) ? true : false);
               if ( ( ( context.localUtil.CToN( cgiGet( edtmes9_Internalname), ".", ",") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtmes9_Internalname), ".", ",") > Convert.ToDecimal( 999999999 )) ) )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "MES9");
                  AnyError = 1;
                  GX_FocusControl = edtmes9_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  wbErr = true;
                  A113mes9 = 0;
                  n113mes9 = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A113mes9", StringUtil.LTrim( StringUtil.Str( (decimal)(A113mes9), 9, 0)));
               }
               else
               {
                  A113mes9 = (int)(context.localUtil.CToN( cgiGet( edtmes9_Internalname), ".", ","));
                  n113mes9 = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A113mes9", StringUtil.LTrim( StringUtil.Str( (decimal)(A113mes9), 9, 0)));
               }
               n113mes9 = ((0==A113mes9) ? true : false);
               if ( ( ( context.localUtil.CToN( cgiGet( edtmes10_Internalname), ".", ",") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtmes10_Internalname), ".", ",") > Convert.ToDecimal( 999999999 )) ) )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "MES10");
                  AnyError = 1;
                  GX_FocusControl = edtmes10_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  wbErr = true;
                  A114mes10 = 0;
                  n114mes10 = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A114mes10", StringUtil.LTrim( StringUtil.Str( (decimal)(A114mes10), 9, 0)));
               }
               else
               {
                  A114mes10 = (int)(context.localUtil.CToN( cgiGet( edtmes10_Internalname), ".", ","));
                  n114mes10 = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A114mes10", StringUtil.LTrim( StringUtil.Str( (decimal)(A114mes10), 9, 0)));
               }
               n114mes10 = ((0==A114mes10) ? true : false);
               if ( ( ( context.localUtil.CToN( cgiGet( edtmes11_Internalname), ".", ",") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtmes11_Internalname), ".", ",") > Convert.ToDecimal( 999999999 )) ) )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "MES11");
                  AnyError = 1;
                  GX_FocusControl = edtmes11_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  wbErr = true;
                  A115mes11 = 0;
                  n115mes11 = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A115mes11", StringUtil.LTrim( StringUtil.Str( (decimal)(A115mes11), 9, 0)));
               }
               else
               {
                  A115mes11 = (int)(context.localUtil.CToN( cgiGet( edtmes11_Internalname), ".", ","));
                  n115mes11 = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A115mes11", StringUtil.LTrim( StringUtil.Str( (decimal)(A115mes11), 9, 0)));
               }
               n115mes11 = ((0==A115mes11) ? true : false);
               if ( ( ( context.localUtil.CToN( cgiGet( edtmes12_Internalname), ".", ",") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtmes12_Internalname), ".", ",") > Convert.ToDecimal( 999999999 )) ) )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "MES12");
                  AnyError = 1;
                  GX_FocusControl = edtmes12_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  wbErr = true;
                  A116mes12 = 0;
                  n116mes12 = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A116mes12", StringUtil.LTrim( StringUtil.Str( (decimal)(A116mes12), 9, 0)));
               }
               else
               {
                  A116mes12 = (int)(context.localUtil.CToN( cgiGet( edtmes12_Internalname), ".", ","));
                  n116mes12 = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A116mes12", StringUtil.LTrim( StringUtil.Str( (decimal)(A116mes12), 9, 0)));
               }
               n116mes12 = ((0==A116mes12) ? true : false);
               /* Read saved values. */
               Z24FYActividadPlanCantActid = (int)(context.localUtil.CToN( cgiGet( "Z24FYActividadPlanCantActid"), ".", ","));
               Z104FYActividadPlanCantActFY = (int)(context.localUtil.CToN( cgiGet( "Z104FYActividadPlanCantActFY"), ".", ","));
               Z105mes1 = (int)(context.localUtil.CToN( cgiGet( "Z105mes1"), ".", ","));
               n105mes1 = ((0==A105mes1) ? true : false);
               Z106mes2 = (int)(context.localUtil.CToN( cgiGet( "Z106mes2"), ".", ","));
               n106mes2 = ((0==A106mes2) ? true : false);
               Z107mes3 = (int)(context.localUtil.CToN( cgiGet( "Z107mes3"), ".", ","));
               n107mes3 = ((0==A107mes3) ? true : false);
               Z108mes4 = (int)(context.localUtil.CToN( cgiGet( "Z108mes4"), ".", ","));
               n108mes4 = ((0==A108mes4) ? true : false);
               Z109mes5 = (int)(context.localUtil.CToN( cgiGet( "Z109mes5"), ".", ","));
               n109mes5 = ((0==A109mes5) ? true : false);
               Z110mes6 = (int)(context.localUtil.CToN( cgiGet( "Z110mes6"), ".", ","));
               n110mes6 = ((0==A110mes6) ? true : false);
               Z111mes7 = (int)(context.localUtil.CToN( cgiGet( "Z111mes7"), ".", ","));
               n111mes7 = ((0==A111mes7) ? true : false);
               Z112mes8 = (int)(context.localUtil.CToN( cgiGet( "Z112mes8"), ".", ","));
               n112mes8 = ((0==A112mes8) ? true : false);
               Z113mes9 = (int)(context.localUtil.CToN( cgiGet( "Z113mes9"), ".", ","));
               n113mes9 = ((0==A113mes9) ? true : false);
               Z114mes10 = (int)(context.localUtil.CToN( cgiGet( "Z114mes10"), ".", ","));
               n114mes10 = ((0==A114mes10) ? true : false);
               Z115mes11 = (int)(context.localUtil.CToN( cgiGet( "Z115mes11"), ".", ","));
               n115mes11 = ((0==A115mes11) ? true : false);
               Z116mes12 = (int)(context.localUtil.CToN( cgiGet( "Z116mes12"), ".", ","));
               n116mes12 = ((0==A116mes12) ? true : false);
               Z23ActividadParaPlanid = (int)(context.localUtil.CToN( cgiGet( "Z23ActividadParaPlanid"), ".", ","));
               IsConfirmed = (short)(context.localUtil.CToN( cgiGet( "IsConfirmed"), ".", ","));
               IsModified = (short)(context.localUtil.CToN( cgiGet( "IsModified"), ".", ","));
               Gx_mode = cgiGet( "Mode");
               Gx_mode = cgiGet( "vMODE");
               /* Read subfile selected row values. */
               /* Read hidden variables. */
               GXKey = Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
               standaloneNotModal( ) ;
            }
            else
            {
               standaloneNotModal( ) ;
               if ( StringUtil.StrCmp(gxfirstwebparm, "viewer") == 0 )
               {
                  Gx_mode = "DSP";
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                  A24FYActividadPlanCantActid = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A24FYActividadPlanCantActid", StringUtil.LTrim( StringUtil.Str( (decimal)(A24FYActividadPlanCantActid), 9, 0)));
                  getEqualNoModal( ) ;
                  Gx_mode = "DSP";
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                  disable_std_buttons_dsp( ) ;
                  standaloneModal( ) ;
               }
               else
               {
                  Gx_mode = "INS";
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                  standaloneModal( ) ;
               }
            }
         }
      }

      protected void Process( )
      {
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            /* Read Transaction buttons. */
            sEvt = cgiGet( "_EventName");
            EvtGridId = cgiGet( "_EventGridId");
            EvtRowId = cgiGet( "_EventRowId");
            if ( StringUtil.Len( sEvt) > 0 )
            {
               sEvtType = StringUtil.Left( sEvt, 1);
               sEvt = StringUtil.Right( sEvt, (short)(StringUtil.Len( sEvt)-1));
               if ( StringUtil.StrCmp(sEvtType, "M") != 0 )
               {
                  if ( StringUtil.StrCmp(sEvtType, "E") == 0 )
                  {
                     sEvtType = StringUtil.Right( sEvt, 1);
                     if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                     {
                        sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                        if ( StringUtil.StrCmp(sEvt, "ENTER") == 0 )
                        {
                           context.wbHandled = 1;
                           btn_enter( ) ;
                           /* No code required for Cancel button. It is implemented as the Reset button. */
                        }
                        else if ( StringUtil.StrCmp(sEvt, "FIRST") == 0 )
                        {
                           context.wbHandled = 1;
                           btn_first( ) ;
                        }
                        else if ( StringUtil.StrCmp(sEvt, "PREVIOUS") == 0 )
                        {
                           context.wbHandled = 1;
                           btn_previous( ) ;
                        }
                        else if ( StringUtil.StrCmp(sEvt, "NEXT") == 0 )
                        {
                           context.wbHandled = 1;
                           btn_next( ) ;
                        }
                        else if ( StringUtil.StrCmp(sEvt, "LAST") == 0 )
                        {
                           context.wbHandled = 1;
                           btn_last( ) ;
                        }
                        else if ( StringUtil.StrCmp(sEvt, "SELECT") == 0 )
                        {
                           context.wbHandled = 1;
                           btn_select( ) ;
                        }
                        else if ( StringUtil.StrCmp(sEvt, "DELETE") == 0 )
                        {
                           context.wbHandled = 1;
                           btn_delete( ) ;
                        }
                        else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                        {
                           context.wbHandled = 1;
                           AfterKeyLoadScreen( ) ;
                        }
                     }
                     else
                     {
                     }
                  }
                  context.wbHandled = 1;
               }
            }
         }
      }

      protected void AfterTrn( )
      {
         if ( trnEnded == 1 )
         {
            trnEnded = 0;
            standaloneNotModal( ) ;
            standaloneModal( ) ;
            if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  )
            {
               /* Clear variables for new insertion. */
               InitAll0N23( ) ;
               standaloneNotModal( ) ;
               standaloneModal( ) ;
            }
         }
      }

      public override String ToString( )
      {
         return "" ;
      }

      public GxContentInfo GetContentInfo( )
      {
         return (GxContentInfo)(null) ;
      }

      protected void disable_std_buttons( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )
         {
            bttBtn_delete_Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_delete_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_delete_Enabled), 5, 0)), true);
         }
      }

      protected void disable_std_buttons_dsp( )
      {
         bttBtn_delete_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_delete_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_delete_Visible), 5, 0)), true);
         bttBtn_first_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_first_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_first_Visible), 5, 0)), true);
         bttBtn_previous_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_previous_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_previous_Visible), 5, 0)), true);
         bttBtn_next_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_next_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_next_Visible), 5, 0)), true);
         bttBtn_last_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_last_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_last_Visible), 5, 0)), true);
         bttBtn_select_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_select_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_select_Visible), 5, 0)), true);
         bttBtn_delete_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_delete_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_delete_Visible), 5, 0)), true);
         if ( StringUtil.StrCmp(Gx_mode, "DSP") == 0 )
         {
            bttBtn_enter_Visible = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_enter_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_enter_Visible), 5, 0)), true);
         }
         DisableAttributes0N23( ) ;
      }

      protected void set_caption( )
      {
         if ( ( IsConfirmed == 1 ) && ( AnyError == 0 ) )
         {
            if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_confdelete", ""), 0, "", true);
            }
            else
            {
               GX_msglist.addItem(context.GetMessage( "GXM_mustconfirm", ""), 0, "", true);
            }
         }
      }

      protected void ResetCaption0N0( )
      {
      }

      protected void ZM0N23( short GX_JID )
      {
         if ( ( GX_JID == 1 ) || ( GX_JID == 0 ) )
         {
            if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
            {
               Z104FYActividadPlanCantActFY = T000N3_A104FYActividadPlanCantActFY[0];
               Z105mes1 = T000N3_A105mes1[0];
               Z106mes2 = T000N3_A106mes2[0];
               Z107mes3 = T000N3_A107mes3[0];
               Z108mes4 = T000N3_A108mes4[0];
               Z109mes5 = T000N3_A109mes5[0];
               Z110mes6 = T000N3_A110mes6[0];
               Z111mes7 = T000N3_A111mes7[0];
               Z112mes8 = T000N3_A112mes8[0];
               Z113mes9 = T000N3_A113mes9[0];
               Z114mes10 = T000N3_A114mes10[0];
               Z115mes11 = T000N3_A115mes11[0];
               Z116mes12 = T000N3_A116mes12[0];
               Z23ActividadParaPlanid = T000N3_A23ActividadParaPlanid[0];
            }
            else
            {
               Z104FYActividadPlanCantActFY = A104FYActividadPlanCantActFY;
               Z105mes1 = A105mes1;
               Z106mes2 = A106mes2;
               Z107mes3 = A107mes3;
               Z108mes4 = A108mes4;
               Z109mes5 = A109mes5;
               Z110mes6 = A110mes6;
               Z111mes7 = A111mes7;
               Z112mes8 = A112mes8;
               Z113mes9 = A113mes9;
               Z114mes10 = A114mes10;
               Z115mes11 = A115mes11;
               Z116mes12 = A116mes12;
               Z23ActividadParaPlanid = A23ActividadParaPlanid;
            }
         }
         if ( GX_JID == -1 )
         {
            Z24FYActividadPlanCantActid = A24FYActividadPlanCantActid;
            Z104FYActividadPlanCantActFY = A104FYActividadPlanCantActFY;
            Z105mes1 = A105mes1;
            Z106mes2 = A106mes2;
            Z107mes3 = A107mes3;
            Z108mes4 = A108mes4;
            Z109mes5 = A109mes5;
            Z110mes6 = A110mes6;
            Z111mes7 = A111mes7;
            Z112mes8 = A112mes8;
            Z113mes9 = A113mes9;
            Z114mes10 = A114mes10;
            Z115mes11 = A115mes11;
            Z116mes12 = A116mes12;
            Z23ActividadParaPlanid = A23ActividadParaPlanid;
         }
      }

      protected void standaloneNotModal( )
      {
         imgprompt_23_Link = ((StringUtil.StrCmp(Gx_mode, "DSP")==0) ? "" : "javascript:"+"gx.popup.openPrompt('"+"gx00e0.aspx"+"',["+"{Ctrl:gx.dom.el('"+"ACTIVIDADPARAPLANID"+"'), id:'"+"ACTIVIDADPARAPLANID"+"'"+",IOType:'out'}"+"],"+"null"+","+"'', false"+","+"false"+");");
      }

      protected void standaloneModal( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )
         {
            bttBtn_delete_Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_delete_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_delete_Enabled), 5, 0)), true);
         }
         else
         {
            bttBtn_delete_Enabled = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_delete_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_delete_Enabled), 5, 0)), true);
         }
         if ( StringUtil.StrCmp(Gx_mode, "DSP") == 0 )
         {
            bttBtn_enter_Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_enter_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_enter_Enabled), 5, 0)), true);
         }
         else
         {
            bttBtn_enter_Enabled = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_enter_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_enter_Enabled), 5, 0)), true);
         }
      }

      protected void Load0N23( )
      {
         /* Using cursor T000N5 */
         pr_datastore1.execute(3, new Object[] {A24FYActividadPlanCantActid});
         if ( (pr_datastore1.getStatus(3) != 101) )
         {
            RcdFound23 = 1;
            A104FYActividadPlanCantActFY = T000N5_A104FYActividadPlanCantActFY[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A104FYActividadPlanCantActFY", StringUtil.LTrim( StringUtil.Str( (decimal)(A104FYActividadPlanCantActFY), 9, 0)));
            A105mes1 = T000N5_A105mes1[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A105mes1", StringUtil.LTrim( StringUtil.Str( (decimal)(A105mes1), 9, 0)));
            n105mes1 = T000N5_n105mes1[0];
            A106mes2 = T000N5_A106mes2[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A106mes2", StringUtil.LTrim( StringUtil.Str( (decimal)(A106mes2), 9, 0)));
            n106mes2 = T000N5_n106mes2[0];
            A107mes3 = T000N5_A107mes3[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A107mes3", StringUtil.LTrim( StringUtil.Str( (decimal)(A107mes3), 9, 0)));
            n107mes3 = T000N5_n107mes3[0];
            A108mes4 = T000N5_A108mes4[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A108mes4", StringUtil.LTrim( StringUtil.Str( (decimal)(A108mes4), 9, 0)));
            n108mes4 = T000N5_n108mes4[0];
            A109mes5 = T000N5_A109mes5[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A109mes5", StringUtil.LTrim( StringUtil.Str( (decimal)(A109mes5), 9, 0)));
            n109mes5 = T000N5_n109mes5[0];
            A110mes6 = T000N5_A110mes6[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A110mes6", StringUtil.LTrim( StringUtil.Str( (decimal)(A110mes6), 9, 0)));
            n110mes6 = T000N5_n110mes6[0];
            A111mes7 = T000N5_A111mes7[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A111mes7", StringUtil.LTrim( StringUtil.Str( (decimal)(A111mes7), 9, 0)));
            n111mes7 = T000N5_n111mes7[0];
            A112mes8 = T000N5_A112mes8[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A112mes8", StringUtil.LTrim( StringUtil.Str( (decimal)(A112mes8), 9, 0)));
            n112mes8 = T000N5_n112mes8[0];
            A113mes9 = T000N5_A113mes9[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A113mes9", StringUtil.LTrim( StringUtil.Str( (decimal)(A113mes9), 9, 0)));
            n113mes9 = T000N5_n113mes9[0];
            A114mes10 = T000N5_A114mes10[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A114mes10", StringUtil.LTrim( StringUtil.Str( (decimal)(A114mes10), 9, 0)));
            n114mes10 = T000N5_n114mes10[0];
            A115mes11 = T000N5_A115mes11[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A115mes11", StringUtil.LTrim( StringUtil.Str( (decimal)(A115mes11), 9, 0)));
            n115mes11 = T000N5_n115mes11[0];
            A116mes12 = T000N5_A116mes12[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A116mes12", StringUtil.LTrim( StringUtil.Str( (decimal)(A116mes12), 9, 0)));
            n116mes12 = T000N5_n116mes12[0];
            A23ActividadParaPlanid = T000N5_A23ActividadParaPlanid[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A23ActividadParaPlanid", StringUtil.LTrim( StringUtil.Str( (decimal)(A23ActividadParaPlanid), 9, 0)));
            ZM0N23( -1) ;
         }
         pr_datastore1.close(3);
         OnLoadActions0N23( ) ;
      }

      protected void OnLoadActions0N23( )
      {
      }

      protected void CheckExtendedTable0N23( )
      {
         Gx_BScreen = 1;
         standaloneModal( ) ;
         /* Using cursor T000N4 */
         pr_datastore1.execute(2, new Object[] {A23ActividadParaPlanid});
         if ( (pr_datastore1.getStatus(2) == 101) )
         {
            GX_msglist.addItem("No matching 'Actividad Para Plan'.", "ForeignKeyNotFound", 1, "ACTIVIDADPARAPLANID");
            AnyError = 1;
            GX_FocusControl = edtActividadParaPlanid_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         pr_datastore1.close(2);
      }

      protected void CloseExtendedTableCursors0N23( )
      {
         pr_datastore1.close(2);
      }

      protected void enableDisable( )
      {
      }

      protected void gxLoad_2( int A23ActividadParaPlanid )
      {
         /* Using cursor T000N6 */
         pr_datastore1.execute(4, new Object[] {A23ActividadParaPlanid});
         if ( (pr_datastore1.getStatus(4) == 101) )
         {
            GX_msglist.addItem("No matching 'Actividad Para Plan'.", "ForeignKeyNotFound", 1, "ACTIVIDADPARAPLANID");
            AnyError = 1;
            GX_FocusControl = edtActividadParaPlanid_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         GxWebStd.set_html_headers( context, 0, "", "");
         context.GX_webresponse.AddString("[[");
         context.GX_webresponse.AddString("");
         context.GX_webresponse.AddString("]");
         if ( (pr_datastore1.getStatus(4) == 101) )
         {
            context.GX_webresponse.AddString(",");
            context.GX_webresponse.AddString("101");
         }
         context.GX_webresponse.AddString("]");
         pr_datastore1.close(4);
      }

      protected void GetKey0N23( )
      {
         /* Using cursor T000N7 */
         pr_datastore1.execute(5, new Object[] {A24FYActividadPlanCantActid});
         if ( (pr_datastore1.getStatus(5) != 101) )
         {
            RcdFound23 = 1;
         }
         else
         {
            RcdFound23 = 0;
         }
         pr_datastore1.close(5);
      }

      protected void getByPrimaryKey( )
      {
         /* Using cursor T000N3 */
         pr_datastore1.execute(1, new Object[] {A24FYActividadPlanCantActid});
         if ( (pr_datastore1.getStatus(1) != 101) )
         {
            ZM0N23( 1) ;
            RcdFound23 = 1;
            A24FYActividadPlanCantActid = T000N3_A24FYActividadPlanCantActid[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A24FYActividadPlanCantActid", StringUtil.LTrim( StringUtil.Str( (decimal)(A24FYActividadPlanCantActid), 9, 0)));
            A104FYActividadPlanCantActFY = T000N3_A104FYActividadPlanCantActFY[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A104FYActividadPlanCantActFY", StringUtil.LTrim( StringUtil.Str( (decimal)(A104FYActividadPlanCantActFY), 9, 0)));
            A105mes1 = T000N3_A105mes1[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A105mes1", StringUtil.LTrim( StringUtil.Str( (decimal)(A105mes1), 9, 0)));
            n105mes1 = T000N3_n105mes1[0];
            A106mes2 = T000N3_A106mes2[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A106mes2", StringUtil.LTrim( StringUtil.Str( (decimal)(A106mes2), 9, 0)));
            n106mes2 = T000N3_n106mes2[0];
            A107mes3 = T000N3_A107mes3[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A107mes3", StringUtil.LTrim( StringUtil.Str( (decimal)(A107mes3), 9, 0)));
            n107mes3 = T000N3_n107mes3[0];
            A108mes4 = T000N3_A108mes4[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A108mes4", StringUtil.LTrim( StringUtil.Str( (decimal)(A108mes4), 9, 0)));
            n108mes4 = T000N3_n108mes4[0];
            A109mes5 = T000N3_A109mes5[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A109mes5", StringUtil.LTrim( StringUtil.Str( (decimal)(A109mes5), 9, 0)));
            n109mes5 = T000N3_n109mes5[0];
            A110mes6 = T000N3_A110mes6[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A110mes6", StringUtil.LTrim( StringUtil.Str( (decimal)(A110mes6), 9, 0)));
            n110mes6 = T000N3_n110mes6[0];
            A111mes7 = T000N3_A111mes7[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A111mes7", StringUtil.LTrim( StringUtil.Str( (decimal)(A111mes7), 9, 0)));
            n111mes7 = T000N3_n111mes7[0];
            A112mes8 = T000N3_A112mes8[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A112mes8", StringUtil.LTrim( StringUtil.Str( (decimal)(A112mes8), 9, 0)));
            n112mes8 = T000N3_n112mes8[0];
            A113mes9 = T000N3_A113mes9[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A113mes9", StringUtil.LTrim( StringUtil.Str( (decimal)(A113mes9), 9, 0)));
            n113mes9 = T000N3_n113mes9[0];
            A114mes10 = T000N3_A114mes10[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A114mes10", StringUtil.LTrim( StringUtil.Str( (decimal)(A114mes10), 9, 0)));
            n114mes10 = T000N3_n114mes10[0];
            A115mes11 = T000N3_A115mes11[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A115mes11", StringUtil.LTrim( StringUtil.Str( (decimal)(A115mes11), 9, 0)));
            n115mes11 = T000N3_n115mes11[0];
            A116mes12 = T000N3_A116mes12[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A116mes12", StringUtil.LTrim( StringUtil.Str( (decimal)(A116mes12), 9, 0)));
            n116mes12 = T000N3_n116mes12[0];
            A23ActividadParaPlanid = T000N3_A23ActividadParaPlanid[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A23ActividadParaPlanid", StringUtil.LTrim( StringUtil.Str( (decimal)(A23ActividadParaPlanid), 9, 0)));
            Z24FYActividadPlanCantActid = A24FYActividadPlanCantActid;
            sMode23 = Gx_mode;
            Gx_mode = "DSP";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            standaloneModal( ) ;
            Load0N23( ) ;
            if ( AnyError == 1 )
            {
               RcdFound23 = 0;
               InitializeNonKey0N23( ) ;
            }
            Gx_mode = sMode23;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         }
         else
         {
            RcdFound23 = 0;
            InitializeNonKey0N23( ) ;
            sMode23 = Gx_mode;
            Gx_mode = "DSP";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            standaloneModal( ) ;
            Gx_mode = sMode23;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         }
         pr_datastore1.close(1);
      }

      protected void getEqualNoModal( )
      {
         GetKey0N23( ) ;
         if ( RcdFound23 == 0 )
         {
            Gx_mode = "INS";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         }
         else
         {
            Gx_mode = "UPD";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         }
         getByPrimaryKey( ) ;
      }

      protected void move_next( )
      {
         RcdFound23 = 0;
         /* Using cursor T000N8 */
         pr_datastore1.execute(6, new Object[] {A24FYActividadPlanCantActid});
         if ( (pr_datastore1.getStatus(6) != 101) )
         {
            while ( (pr_datastore1.getStatus(6) != 101) && ( ( T000N8_A24FYActividadPlanCantActid[0] < A24FYActividadPlanCantActid ) ) )
            {
               pr_datastore1.readNext(6);
            }
            if ( (pr_datastore1.getStatus(6) != 101) && ( ( T000N8_A24FYActividadPlanCantActid[0] > A24FYActividadPlanCantActid ) ) )
            {
               A24FYActividadPlanCantActid = T000N8_A24FYActividadPlanCantActid[0];
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A24FYActividadPlanCantActid", StringUtil.LTrim( StringUtil.Str( (decimal)(A24FYActividadPlanCantActid), 9, 0)));
               RcdFound23 = 1;
            }
         }
         pr_datastore1.close(6);
      }

      protected void move_previous( )
      {
         RcdFound23 = 0;
         /* Using cursor T000N9 */
         pr_datastore1.execute(7, new Object[] {A24FYActividadPlanCantActid});
         if ( (pr_datastore1.getStatus(7) != 101) )
         {
            while ( (pr_datastore1.getStatus(7) != 101) && ( ( T000N9_A24FYActividadPlanCantActid[0] > A24FYActividadPlanCantActid ) ) )
            {
               pr_datastore1.readNext(7);
            }
            if ( (pr_datastore1.getStatus(7) != 101) && ( ( T000N9_A24FYActividadPlanCantActid[0] < A24FYActividadPlanCantActid ) ) )
            {
               A24FYActividadPlanCantActid = T000N9_A24FYActividadPlanCantActid[0];
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A24FYActividadPlanCantActid", StringUtil.LTrim( StringUtil.Str( (decimal)(A24FYActividadPlanCantActid), 9, 0)));
               RcdFound23 = 1;
            }
         }
         pr_datastore1.close(7);
      }

      protected void btn_enter( )
      {
         nKeyPressed = 1;
         GetKey0N23( ) ;
         if ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )
         {
            /* Insert record */
            GX_FocusControl = edtFYActividadPlanCantActid_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            Insert0N23( ) ;
            if ( AnyError == 1 )
            {
               GX_FocusControl = "";
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            }
         }
         else
         {
            if ( RcdFound23 == 1 )
            {
               if ( A24FYActividadPlanCantActid != Z24FYActividadPlanCantActid )
               {
                  A24FYActividadPlanCantActid = Z24FYActividadPlanCantActid;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A24FYActividadPlanCantActid", StringUtil.LTrim( StringUtil.Str( (decimal)(A24FYActividadPlanCantActid), 9, 0)));
                  GX_msglist.addItem(context.GetMessage( "GXM_getbeforeupd", ""), "CandidateKeyNotFound", 1, "FYACTIVIDADPLANCANTACTID");
                  AnyError = 1;
                  GX_FocusControl = edtFYActividadPlanCantActid_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               }
               else if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
               {
                  delete( ) ;
                  AfterTrn( ) ;
                  GX_FocusControl = edtFYActividadPlanCantActid_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               }
               else
               {
                  Gx_mode = "UPD";
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                  /* Update record */
                  Update0N23( ) ;
                  GX_FocusControl = edtFYActividadPlanCantActid_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               }
            }
            else
            {
               if ( A24FYActividadPlanCantActid != Z24FYActividadPlanCantActid )
               {
                  Gx_mode = "INS";
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                  /* Insert record */
                  GX_FocusControl = edtFYActividadPlanCantActid_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  Insert0N23( ) ;
                  if ( AnyError == 1 )
                  {
                     GX_FocusControl = "";
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  }
               }
               else
               {
                  if ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 )
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_recdeleted", ""), 1, "FYACTIVIDADPLANCANTACTID");
                     AnyError = 1;
                     GX_FocusControl = edtFYActividadPlanCantActid_Internalname;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  }
                  else
                  {
                     Gx_mode = "INS";
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                     /* Insert record */
                     GX_FocusControl = edtFYActividadPlanCantActid_Internalname;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                     Insert0N23( ) ;
                     if ( AnyError == 1 )
                     {
                        GX_FocusControl = "";
                        context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                     }
                  }
               }
            }
         }
         AfterTrn( ) ;
      }

      protected void btn_delete( )
      {
         if ( A24FYActividadPlanCantActid != Z24FYActividadPlanCantActid )
         {
            A24FYActividadPlanCantActid = Z24FYActividadPlanCantActid;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A24FYActividadPlanCantActid", StringUtil.LTrim( StringUtil.Str( (decimal)(A24FYActividadPlanCantActid), 9, 0)));
            GX_msglist.addItem(context.GetMessage( "GXM_getbeforedlt", ""), 1, "FYACTIVIDADPLANCANTACTID");
            AnyError = 1;
            GX_FocusControl = edtFYActividadPlanCantActid_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         else
         {
            delete( ) ;
            AfterTrn( ) ;
            GX_FocusControl = edtFYActividadPlanCantActid_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         if ( AnyError != 0 )
         {
            Gx_mode = "UPD";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         }
         else
         {
            getByPrimaryKey( ) ;
         }
         CloseOpenCursors();
      }

      protected void btn_get( )
      {
         nKeyPressed = 2;
         IsConfirmed = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "IsConfirmed", StringUtil.LTrim( StringUtil.Str( (decimal)(IsConfirmed), 4, 0)));
         getEqualNoModal( ) ;
         if ( RcdFound23 == 0 )
         {
            GX_msglist.addItem(context.GetMessage( "GXM_keynfound", ""), "PrimaryKeyNotFound", 1, "FYACTIVIDADPLANCANTACTID");
            AnyError = 1;
            GX_FocusControl = edtFYActividadPlanCantActid_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         GX_FocusControl = edtActividadParaPlanid_Internalname;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         standaloneNotModal( ) ;
         standaloneModal( ) ;
      }

      protected void btn_first( )
      {
         nKeyPressed = 2;
         IsConfirmed = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "IsConfirmed", StringUtil.LTrim( StringUtil.Str( (decimal)(IsConfirmed), 4, 0)));
         ScanStart0N23( ) ;
         if ( RcdFound23 == 0 )
         {
            GX_msglist.addItem(context.GetMessage( "GXM_norectobrow", ""), 0, "", true);
         }
         else
         {
            Gx_mode = "UPD";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         }
         GX_FocusControl = edtActividadParaPlanid_Internalname;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         ScanEnd0N23( ) ;
         getByPrimaryKey( ) ;
         standaloneNotModal( ) ;
         standaloneModal( ) ;
      }

      protected void btn_previous( )
      {
         nKeyPressed = 2;
         IsConfirmed = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "IsConfirmed", StringUtil.LTrim( StringUtil.Str( (decimal)(IsConfirmed), 4, 0)));
         move_previous( ) ;
         if ( RcdFound23 == 0 )
         {
            GX_msglist.addItem(context.GetMessage( "GXM_norectobrow", ""), 0, "", true);
         }
         else
         {
            Gx_mode = "UPD";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         }
         GX_FocusControl = edtActividadParaPlanid_Internalname;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         getByPrimaryKey( ) ;
         standaloneNotModal( ) ;
         standaloneModal( ) ;
      }

      protected void btn_next( )
      {
         nKeyPressed = 2;
         IsConfirmed = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "IsConfirmed", StringUtil.LTrim( StringUtil.Str( (decimal)(IsConfirmed), 4, 0)));
         move_next( ) ;
         if ( RcdFound23 == 0 )
         {
            GX_msglist.addItem(context.GetMessage( "GXM_norectobrow", ""), 0, "", true);
         }
         else
         {
            Gx_mode = "UPD";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         }
         GX_FocusControl = edtActividadParaPlanid_Internalname;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         getByPrimaryKey( ) ;
         standaloneNotModal( ) ;
         standaloneModal( ) ;
      }

      protected void btn_last( )
      {
         nKeyPressed = 2;
         IsConfirmed = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "IsConfirmed", StringUtil.LTrim( StringUtil.Str( (decimal)(IsConfirmed), 4, 0)));
         ScanStart0N23( ) ;
         if ( RcdFound23 == 0 )
         {
            GX_msglist.addItem(context.GetMessage( "GXM_norectobrow", ""), 0, "", true);
         }
         else
         {
            while ( RcdFound23 != 0 )
            {
               ScanNext0N23( ) ;
            }
            Gx_mode = "UPD";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         }
         GX_FocusControl = edtActividadParaPlanid_Internalname;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         ScanEnd0N23( ) ;
         getByPrimaryKey( ) ;
         standaloneNotModal( ) ;
         standaloneModal( ) ;
      }

      protected void btn_select( )
      {
         getEqualNoModal( ) ;
      }

      protected void CheckOptimisticConcurrency0N23( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
         {
            /* Using cursor T000N2 */
            pr_datastore1.execute(0, new Object[] {A24FYActividadPlanCantActid});
            if ( (pr_datastore1.getStatus(0) == 103) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_lock", new   object[]  {"FYACTIVIDADPLANCANTACT"}), "RecordIsLocked", 1, "");
               AnyError = 1;
               return  ;
            }
            Gx_longc = false;
            if ( (pr_datastore1.getStatus(0) == 101) || ( Z104FYActividadPlanCantActFY != T000N2_A104FYActividadPlanCantActFY[0] ) || ( Z105mes1 != T000N2_A105mes1[0] ) || ( Z106mes2 != T000N2_A106mes2[0] ) || ( Z107mes3 != T000N2_A107mes3[0] ) || ( Z108mes4 != T000N2_A108mes4[0] ) )
            {
               Gx_longc = true;
            }
            if ( Gx_longc || ( Z109mes5 != T000N2_A109mes5[0] ) || ( Z110mes6 != T000N2_A110mes6[0] ) || ( Z111mes7 != T000N2_A111mes7[0] ) || ( Z112mes8 != T000N2_A112mes8[0] ) || ( Z113mes9 != T000N2_A113mes9[0] ) )
            {
               Gx_longc = true;
            }
            if ( Gx_longc || ( Z114mes10 != T000N2_A114mes10[0] ) || ( Z115mes11 != T000N2_A115mes11[0] ) || ( Z116mes12 != T000N2_A116mes12[0] ) || ( Z23ActividadParaPlanid != T000N2_A23ActividadParaPlanid[0] ) )
            {
               if ( Z104FYActividadPlanCantActFY != T000N2_A104FYActividadPlanCantActFY[0] )
               {
                  GXUtil.WriteLog("fyactividadplancantact:[seudo value changed for attri]"+"FYActividadPlanCantActFY");
                  GXUtil.WriteLogRaw("Old: ",Z104FYActividadPlanCantActFY);
                  GXUtil.WriteLogRaw("Current: ",T000N2_A104FYActividadPlanCantActFY[0]);
               }
               if ( Z105mes1 != T000N2_A105mes1[0] )
               {
                  GXUtil.WriteLog("fyactividadplancantact:[seudo value changed for attri]"+"mes1");
                  GXUtil.WriteLogRaw("Old: ",Z105mes1);
                  GXUtil.WriteLogRaw("Current: ",T000N2_A105mes1[0]);
               }
               if ( Z106mes2 != T000N2_A106mes2[0] )
               {
                  GXUtil.WriteLog("fyactividadplancantact:[seudo value changed for attri]"+"mes2");
                  GXUtil.WriteLogRaw("Old: ",Z106mes2);
                  GXUtil.WriteLogRaw("Current: ",T000N2_A106mes2[0]);
               }
               if ( Z107mes3 != T000N2_A107mes3[0] )
               {
                  GXUtil.WriteLog("fyactividadplancantact:[seudo value changed for attri]"+"mes3");
                  GXUtil.WriteLogRaw("Old: ",Z107mes3);
                  GXUtil.WriteLogRaw("Current: ",T000N2_A107mes3[0]);
               }
               if ( Z108mes4 != T000N2_A108mes4[0] )
               {
                  GXUtil.WriteLog("fyactividadplancantact:[seudo value changed for attri]"+"mes4");
                  GXUtil.WriteLogRaw("Old: ",Z108mes4);
                  GXUtil.WriteLogRaw("Current: ",T000N2_A108mes4[0]);
               }
               if ( Z109mes5 != T000N2_A109mes5[0] )
               {
                  GXUtil.WriteLog("fyactividadplancantact:[seudo value changed for attri]"+"mes5");
                  GXUtil.WriteLogRaw("Old: ",Z109mes5);
                  GXUtil.WriteLogRaw("Current: ",T000N2_A109mes5[0]);
               }
               if ( Z110mes6 != T000N2_A110mes6[0] )
               {
                  GXUtil.WriteLog("fyactividadplancantact:[seudo value changed for attri]"+"mes6");
                  GXUtil.WriteLogRaw("Old: ",Z110mes6);
                  GXUtil.WriteLogRaw("Current: ",T000N2_A110mes6[0]);
               }
               if ( Z111mes7 != T000N2_A111mes7[0] )
               {
                  GXUtil.WriteLog("fyactividadplancantact:[seudo value changed for attri]"+"mes7");
                  GXUtil.WriteLogRaw("Old: ",Z111mes7);
                  GXUtil.WriteLogRaw("Current: ",T000N2_A111mes7[0]);
               }
               if ( Z112mes8 != T000N2_A112mes8[0] )
               {
                  GXUtil.WriteLog("fyactividadplancantact:[seudo value changed for attri]"+"mes8");
                  GXUtil.WriteLogRaw("Old: ",Z112mes8);
                  GXUtil.WriteLogRaw("Current: ",T000N2_A112mes8[0]);
               }
               if ( Z113mes9 != T000N2_A113mes9[0] )
               {
                  GXUtil.WriteLog("fyactividadplancantact:[seudo value changed for attri]"+"mes9");
                  GXUtil.WriteLogRaw("Old: ",Z113mes9);
                  GXUtil.WriteLogRaw("Current: ",T000N2_A113mes9[0]);
               }
               if ( Z114mes10 != T000N2_A114mes10[0] )
               {
                  GXUtil.WriteLog("fyactividadplancantact:[seudo value changed for attri]"+"mes10");
                  GXUtil.WriteLogRaw("Old: ",Z114mes10);
                  GXUtil.WriteLogRaw("Current: ",T000N2_A114mes10[0]);
               }
               if ( Z115mes11 != T000N2_A115mes11[0] )
               {
                  GXUtil.WriteLog("fyactividadplancantact:[seudo value changed for attri]"+"mes11");
                  GXUtil.WriteLogRaw("Old: ",Z115mes11);
                  GXUtil.WriteLogRaw("Current: ",T000N2_A115mes11[0]);
               }
               if ( Z116mes12 != T000N2_A116mes12[0] )
               {
                  GXUtil.WriteLog("fyactividadplancantact:[seudo value changed for attri]"+"mes12");
                  GXUtil.WriteLogRaw("Old: ",Z116mes12);
                  GXUtil.WriteLogRaw("Current: ",T000N2_A116mes12[0]);
               }
               if ( Z23ActividadParaPlanid != T000N2_A23ActividadParaPlanid[0] )
               {
                  GXUtil.WriteLog("fyactividadplancantact:[seudo value changed for attri]"+"ActividadParaPlanid");
                  GXUtil.WriteLogRaw("Old: ",Z23ActividadParaPlanid);
                  GXUtil.WriteLogRaw("Current: ",T000N2_A23ActividadParaPlanid[0]);
               }
               GX_msglist.addItem(context.GetMessage( "GXM_waschg", new   object[]  {"FYACTIVIDADPLANCANTACT"}), "RecordWasChanged", 1, "");
               AnyError = 1;
               return  ;
            }
         }
      }

      protected void Insert0N23( )
      {
         BeforeValidate0N23( ) ;
         if ( AnyError == 0 )
         {
            CheckExtendedTable0N23( ) ;
         }
         if ( AnyError == 0 )
         {
            ZM0N23( 0) ;
            CheckOptimisticConcurrency0N23( ) ;
            if ( AnyError == 0 )
            {
               AfterConfirm0N23( ) ;
               if ( AnyError == 0 )
               {
                  BeforeInsert0N23( ) ;
                  if ( AnyError == 0 )
                  {
                     /* Using cursor T000N10 */
                     pr_datastore1.execute(8, new Object[] {A104FYActividadPlanCantActFY, n105mes1, A105mes1, n106mes2, A106mes2, n107mes3, A107mes3, n108mes4, A108mes4, n109mes5, A109mes5, n110mes6, A110mes6, n111mes7, A111mes7, n112mes8, A112mes8, n113mes9, A113mes9, n114mes10, A114mes10, n115mes11, A115mes11, n116mes12, A116mes12, A23ActividadParaPlanid});
                     A24FYActividadPlanCantActid = T000N10_A24FYActividadPlanCantActid[0];
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A24FYActividadPlanCantActid", StringUtil.LTrim( StringUtil.Str( (decimal)(A24FYActividadPlanCantActid), 9, 0)));
                     pr_datastore1.close(8);
                     dsDataStore1.SmartCacheProvider.SetUpdated("FYACTIVIDADPLANCANTACT") ;
                     if ( AnyError == 0 )
                     {
                        /* Start of After( Insert) rules */
                        /* End of After( Insert) rules */
                        if ( AnyError == 0 )
                        {
                           /* Save values for previous() function. */
                           GX_msglist.addItem(context.GetMessage( "GXM_sucadded", ""), "SuccessfullyAdded", 0, "", true);
                           ResetCaption0N0( ) ;
                        }
                     }
                  }
                  else
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                     AnyError = 1;
                  }
               }
            }
            else
            {
               Load0N23( ) ;
            }
            EndLevel0N23( ) ;
         }
         CloseExtendedTableCursors0N23( ) ;
      }

      protected void Update0N23( )
      {
         BeforeValidate0N23( ) ;
         if ( AnyError == 0 )
         {
            CheckExtendedTable0N23( ) ;
         }
         if ( AnyError == 0 )
         {
            CheckOptimisticConcurrency0N23( ) ;
            if ( AnyError == 0 )
            {
               AfterConfirm0N23( ) ;
               if ( AnyError == 0 )
               {
                  BeforeUpdate0N23( ) ;
                  if ( AnyError == 0 )
                  {
                     /* Using cursor T000N11 */
                     pr_datastore1.execute(9, new Object[] {A104FYActividadPlanCantActFY, n105mes1, A105mes1, n106mes2, A106mes2, n107mes3, A107mes3, n108mes4, A108mes4, n109mes5, A109mes5, n110mes6, A110mes6, n111mes7, A111mes7, n112mes8, A112mes8, n113mes9, A113mes9, n114mes10, A114mes10, n115mes11, A115mes11, n116mes12, A116mes12, A23ActividadParaPlanid, A24FYActividadPlanCantActid});
                     pr_datastore1.close(9);
                     dsDataStore1.SmartCacheProvider.SetUpdated("FYACTIVIDADPLANCANTACT") ;
                     if ( (pr_datastore1.getStatus(9) == 103) )
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_lock", new   object[]  {"FYACTIVIDADPLANCANTACT"}), "RecordIsLocked", 1, "");
                        AnyError = 1;
                     }
                     DeferredUpdate0N23( ) ;
                     if ( AnyError == 0 )
                     {
                        /* Start of After( update) rules */
                        /* End of After( update) rules */
                        if ( AnyError == 0 )
                        {
                           getByPrimaryKey( ) ;
                           GX_msglist.addItem(context.GetMessage( "GXM_sucupdated", ""), "SuccessfullyUpdated", 0, "", true);
                           ResetCaption0N0( ) ;
                        }
                     }
                     else
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                        AnyError = 1;
                     }
                  }
               }
            }
            EndLevel0N23( ) ;
         }
         CloseExtendedTableCursors0N23( ) ;
      }

      protected void DeferredUpdate0N23( )
      {
      }

      protected void delete( )
      {
         Gx_mode = "DLT";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         BeforeValidate0N23( ) ;
         if ( AnyError == 0 )
         {
            CheckOptimisticConcurrency0N23( ) ;
         }
         if ( AnyError == 0 )
         {
            OnDeleteControls0N23( ) ;
            AfterConfirm0N23( ) ;
            if ( AnyError == 0 )
            {
               BeforeDelete0N23( ) ;
               if ( AnyError == 0 )
               {
                  /* No cascading delete specified. */
                  /* Using cursor T000N12 */
                  pr_datastore1.execute(10, new Object[] {A24FYActividadPlanCantActid});
                  pr_datastore1.close(10);
                  dsDataStore1.SmartCacheProvider.SetUpdated("FYACTIVIDADPLANCANTACT") ;
                  if ( AnyError == 0 )
                  {
                     /* Start of After( delete) rules */
                     /* End of After( delete) rules */
                     if ( AnyError == 0 )
                     {
                        move_next( ) ;
                        if ( RcdFound23 == 0 )
                        {
                           InitAll0N23( ) ;
                           Gx_mode = "INS";
                           context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                        }
                        else
                        {
                           getByPrimaryKey( ) ;
                           Gx_mode = "UPD";
                           context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                        }
                        GX_msglist.addItem(context.GetMessage( "GXM_sucdeleted", ""), "SuccessfullyDeleted", 0, "", true);
                        ResetCaption0N0( ) ;
                     }
                  }
                  else
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                     AnyError = 1;
                  }
               }
            }
         }
         sMode23 = Gx_mode;
         Gx_mode = "DLT";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         EndLevel0N23( ) ;
         Gx_mode = sMode23;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
      }

      protected void OnDeleteControls0N23( )
      {
         standaloneModal( ) ;
         /* No delete mode formulas found. */
      }

      protected void EndLevel0N23( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
         {
            pr_datastore1.close(0);
         }
         if ( AnyError == 0 )
         {
            BeforeComplete0N23( ) ;
         }
         if ( AnyError == 0 )
         {
            pr_datastore1.close(1);
            context.CommitDataStores("fyactividadplancantact",pr_default);
            if ( AnyError == 0 )
            {
               ConfirmValues0N0( ) ;
            }
            /* After transaction rules */
            /* Execute 'After Trn' event if defined. */
            trnEnded = 1;
         }
         else
         {
            pr_datastore1.close(1);
            context.RollbackDataStores("fyactividadplancantact",pr_default);
         }
         IsModified = 0;
         if ( AnyError != 0 )
         {
            context.wjLoc = "";
            context.nUserReturn = 0;
         }
      }

      public void ScanStart0N23( )
      {
         /* Using cursor T000N13 */
         pr_datastore1.execute(11);
         RcdFound23 = 0;
         if ( (pr_datastore1.getStatus(11) != 101) )
         {
            RcdFound23 = 1;
            A24FYActividadPlanCantActid = T000N13_A24FYActividadPlanCantActid[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A24FYActividadPlanCantActid", StringUtil.LTrim( StringUtil.Str( (decimal)(A24FYActividadPlanCantActid), 9, 0)));
         }
         /* Load Subordinate Levels */
      }

      protected void ScanNext0N23( )
      {
         /* Scan next routine */
         pr_datastore1.readNext(11);
         RcdFound23 = 0;
         if ( (pr_datastore1.getStatus(11) != 101) )
         {
            RcdFound23 = 1;
            A24FYActividadPlanCantActid = T000N13_A24FYActividadPlanCantActid[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A24FYActividadPlanCantActid", StringUtil.LTrim( StringUtil.Str( (decimal)(A24FYActividadPlanCantActid), 9, 0)));
         }
      }

      protected void ScanEnd0N23( )
      {
         pr_datastore1.close(11);
      }

      protected void AfterConfirm0N23( )
      {
         /* After Confirm Rules */
      }

      protected void BeforeInsert0N23( )
      {
         /* Before Insert Rules */
      }

      protected void BeforeUpdate0N23( )
      {
         /* Before Update Rules */
      }

      protected void BeforeDelete0N23( )
      {
         /* Before Delete Rules */
      }

      protected void BeforeComplete0N23( )
      {
         /* Before Complete Rules */
      }

      protected void BeforeValidate0N23( )
      {
         /* Before Validate Rules */
      }

      protected void DisableAttributes0N23( )
      {
         edtFYActividadPlanCantActid_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtFYActividadPlanCantActid_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtFYActividadPlanCantActid_Enabled), 5, 0)), true);
         edtActividadParaPlanid_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtActividadParaPlanid_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtActividadParaPlanid_Enabled), 5, 0)), true);
         edtFYActividadPlanCantActFY_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtFYActividadPlanCantActFY_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtFYActividadPlanCantActFY_Enabled), 5, 0)), true);
         edtmes1_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtmes1_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtmes1_Enabled), 5, 0)), true);
         edtmes2_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtmes2_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtmes2_Enabled), 5, 0)), true);
         edtmes3_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtmes3_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtmes3_Enabled), 5, 0)), true);
         edtmes4_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtmes4_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtmes4_Enabled), 5, 0)), true);
         edtmes5_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtmes5_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtmes5_Enabled), 5, 0)), true);
         edtmes6_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtmes6_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtmes6_Enabled), 5, 0)), true);
         edtmes7_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtmes7_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtmes7_Enabled), 5, 0)), true);
         edtmes8_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtmes8_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtmes8_Enabled), 5, 0)), true);
         edtmes9_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtmes9_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtmes9_Enabled), 5, 0)), true);
         edtmes10_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtmes10_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtmes10_Enabled), 5, 0)), true);
         edtmes11_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtmes11_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtmes11_Enabled), 5, 0)), true);
         edtmes12_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtmes12_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtmes12_Enabled), 5, 0)), true);
      }

      protected void send_integrity_lvl_hashes0N23( )
      {
      }

      protected void assign_properties_default( )
      {
      }

      protected void ConfirmValues0N0( )
      {
      }

      public override void RenderHtmlHeaders( )
      {
         GxWebStd.gx_html_headers( context, 0, "", "", Form.Meta, Form.Metaequiv, true);
      }

      public override void RenderHtmlOpenForm( )
      {
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.WriteHtmlText( "<title>") ;
         context.SendWebValue( Form.Caption) ;
         context.WriteHtmlTextNl( "</title>") ;
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         if ( StringUtil.Len( sDynURL) > 0 )
         {
            context.WriteHtmlText( "<BASE href=\""+sDynURL+"\" />") ;
         }
         define_styles( ) ;
         MasterPageObj.master_styles();
         if ( ( ( context.GetBrowserType( ) == 1 ) || ( context.GetBrowserType( ) == 5 ) ) && ( StringUtil.StrCmp(context.GetBrowserVersion( ), "7.0") == 0 ) )
         {
            context.AddJavascriptSource("json2.js", "?"+context.GetBuildNumber( 127771), false);
         }
         context.AddJavascriptSource("jquery.js", "?"+context.GetBuildNumber( 127771), false);
         context.AddJavascriptSource("gxtimezone.js", "?"+context.GetBuildNumber( 127771), false);
         context.AddJavascriptSource("gxgral.js", "?"+context.GetBuildNumber( 127771), false);
         context.AddJavascriptSource("gxcfg.js", "?2019131815887", false);
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.WriteHtmlText( Form.Headerrawhtml) ;
         context.CloseHtmlHeader();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         FormProcess = " data-HasEnter=\"true\" data-Skiponenter=\"false\"";
         context.WriteHtmlText( "<body ") ;
         bodyStyle = "" + "background-color:" + context.BuildHTMLColor( Form.Backcolor) + ";color:" + context.BuildHTMLColor( Form.Textcolor) + ";";
         bodyStyle = bodyStyle + "-moz-opacity:0;opacity:0;";
         if ( ! ( String.IsNullOrEmpty(StringUtil.RTrim( Form.Background)) ) )
         {
            bodyStyle = bodyStyle + " background-image:url(" + context.convertURL( Form.Background) + ")";
         }
         context.WriteHtmlText( " "+"class=\"form-horizontal Form\""+" "+ "style='"+bodyStyle+"'") ;
         context.WriteHtmlText( FormProcess+">") ;
         context.skipLines(1);
         context.WriteHtmlTextNl( "<form id=\"MAINFORM\" autocomplete=\"off\" name=\"MAINFORM\" method=\"post\" tabindex=-1  class=\"form-horizontal Form\" data-gx-class=\"form-horizontal Form\" novalidate action=\""+formatLink("fyactividadplancantact.aspx") +"\">") ;
         GxWebStd.gx_hidden_field( context, "_EventName", "");
         GxWebStd.gx_hidden_field( context, "_EventGridId", "");
         GxWebStd.gx_hidden_field( context, "_EventRowId", "");
         context.WriteHtmlText( "<input type=\"submit\" title=\"submit\" style=\"display:none\" disabled>") ;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, "FORM", "Class", "form-horizontal Form", true);
         toggleJsOutput = isJsOutputEnabled( );
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
      }

      protected void send_integrity_footer_hashes( )
      {
         GXKey = Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
      }

      protected void SendCloseFormHiddens( )
      {
         /* Send hidden variables. */
         /* Send saved values. */
         send_integrity_footer_hashes( ) ;
         GxWebStd.gx_hidden_field( context, "Z24FYActividadPlanCantActid", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z24FYActividadPlanCantActid), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "Z104FYActividadPlanCantActFY", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z104FYActividadPlanCantActFY), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "Z105mes1", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z105mes1), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "Z106mes2", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z106mes2), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "Z107mes3", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z107mes3), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "Z108mes4", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z108mes4), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "Z109mes5", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z109mes5), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "Z110mes6", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z110mes6), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "Z111mes7", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z111mes7), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "Z112mes8", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z112mes8), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "Z113mes9", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z113mes9), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "Z114mes10", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z114mes10), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "Z115mes11", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z115mes11), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "Z116mes12", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z116mes12), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "Z23ActividadParaPlanid", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z23ActividadParaPlanid), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "IsConfirmed", StringUtil.LTrim( StringUtil.NToC( (decimal)(IsConfirmed), 4, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "IsModified", StringUtil.LTrim( StringUtil.NToC( (decimal)(IsModified), 4, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "Mode", StringUtil.RTrim( Gx_mode));
         GxWebStd.gx_hidden_field( context, "vMODE", StringUtil.RTrim( Gx_mode));
      }

      public override void RenderHtmlCloseForm( )
      {
         SendCloseFormHiddens( ) ;
         GxWebStd.gx_hidden_field( context, "GX_FocusControl", GX_FocusControl);
         SendAjaxEncryptionKey();
         SendSecurityToken(sPrefix);
         SendComponentObjects();
         SendServerCommands();
         SendState();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         context.WriteHtmlTextNl( "</form>") ;
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         include_jscripts( ) ;
      }

      public override short ExecuteStartEvent( )
      {
         standaloneStartup( ) ;
         gxajaxcallmode = (short)((isAjaxCallMode( ) ? 1 : 0));
         return gxajaxcallmode ;
      }

      public override void RenderHtmlContent( )
      {
         context.WriteHtmlText( "<div") ;
         GxWebStd.ClassAttribute( context, "gx-ct-body"+" "+(String.IsNullOrEmpty(StringUtil.RTrim( Form.Class)) ? "form-horizontal Form" : Form.Class)+"-fx");
         context.WriteHtmlText( ">") ;
         Draw( ) ;
         context.WriteHtmlText( "</div>") ;
      }

      public override void DispatchEvents( )
      {
         Process( ) ;
      }

      public override bool HasEnterEvent( )
      {
         return true ;
      }

      public override GXWebForm GetForm( )
      {
         return Form ;
      }

      public override String GetSelfLink( )
      {
         return formatLink("fyactividadplancantact.aspx")  ;
      }

      public override String GetPgmname( )
      {
         return "FYActividadPlanCantAct" ;
      }

      public override String GetPgmdesc( )
      {
         return "FYActividad Plan Cant Act" ;
      }

      protected void InitializeNonKey0N23( )
      {
         A23ActividadParaPlanid = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A23ActividadParaPlanid", StringUtil.LTrim( StringUtil.Str( (decimal)(A23ActividadParaPlanid), 9, 0)));
         A104FYActividadPlanCantActFY = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A104FYActividadPlanCantActFY", StringUtil.LTrim( StringUtil.Str( (decimal)(A104FYActividadPlanCantActFY), 9, 0)));
         A105mes1 = 0;
         n105mes1 = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A105mes1", StringUtil.LTrim( StringUtil.Str( (decimal)(A105mes1), 9, 0)));
         n105mes1 = ((0==A105mes1) ? true : false);
         A106mes2 = 0;
         n106mes2 = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A106mes2", StringUtil.LTrim( StringUtil.Str( (decimal)(A106mes2), 9, 0)));
         n106mes2 = ((0==A106mes2) ? true : false);
         A107mes3 = 0;
         n107mes3 = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A107mes3", StringUtil.LTrim( StringUtil.Str( (decimal)(A107mes3), 9, 0)));
         n107mes3 = ((0==A107mes3) ? true : false);
         A108mes4 = 0;
         n108mes4 = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A108mes4", StringUtil.LTrim( StringUtil.Str( (decimal)(A108mes4), 9, 0)));
         n108mes4 = ((0==A108mes4) ? true : false);
         A109mes5 = 0;
         n109mes5 = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A109mes5", StringUtil.LTrim( StringUtil.Str( (decimal)(A109mes5), 9, 0)));
         n109mes5 = ((0==A109mes5) ? true : false);
         A110mes6 = 0;
         n110mes6 = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A110mes6", StringUtil.LTrim( StringUtil.Str( (decimal)(A110mes6), 9, 0)));
         n110mes6 = ((0==A110mes6) ? true : false);
         A111mes7 = 0;
         n111mes7 = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A111mes7", StringUtil.LTrim( StringUtil.Str( (decimal)(A111mes7), 9, 0)));
         n111mes7 = ((0==A111mes7) ? true : false);
         A112mes8 = 0;
         n112mes8 = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A112mes8", StringUtil.LTrim( StringUtil.Str( (decimal)(A112mes8), 9, 0)));
         n112mes8 = ((0==A112mes8) ? true : false);
         A113mes9 = 0;
         n113mes9 = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A113mes9", StringUtil.LTrim( StringUtil.Str( (decimal)(A113mes9), 9, 0)));
         n113mes9 = ((0==A113mes9) ? true : false);
         A114mes10 = 0;
         n114mes10 = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A114mes10", StringUtil.LTrim( StringUtil.Str( (decimal)(A114mes10), 9, 0)));
         n114mes10 = ((0==A114mes10) ? true : false);
         A115mes11 = 0;
         n115mes11 = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A115mes11", StringUtil.LTrim( StringUtil.Str( (decimal)(A115mes11), 9, 0)));
         n115mes11 = ((0==A115mes11) ? true : false);
         A116mes12 = 0;
         n116mes12 = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A116mes12", StringUtil.LTrim( StringUtil.Str( (decimal)(A116mes12), 9, 0)));
         n116mes12 = ((0==A116mes12) ? true : false);
         Z104FYActividadPlanCantActFY = 0;
         Z105mes1 = 0;
         Z106mes2 = 0;
         Z107mes3 = 0;
         Z108mes4 = 0;
         Z109mes5 = 0;
         Z110mes6 = 0;
         Z111mes7 = 0;
         Z112mes8 = 0;
         Z113mes9 = 0;
         Z114mes10 = 0;
         Z115mes11 = 0;
         Z116mes12 = 0;
         Z23ActividadParaPlanid = 0;
      }

      protected void InitAll0N23( )
      {
         A24FYActividadPlanCantActid = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A24FYActividadPlanCantActid", StringUtil.LTrim( StringUtil.Str( (decimal)(A24FYActividadPlanCantActid), 9, 0)));
         InitializeNonKey0N23( ) ;
      }

      protected void StandaloneModalInsert( )
      {
      }

      protected void define_styles( )
      {
         AddThemeStyleSheetFile("", context.GetTheme( )+".css", "?"+GetCacheInvalidationToken( ));
         bool outputEnabled = isOutputEnabled( ) ;
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         idxLst = 1;
         while ( idxLst <= Form.Jscriptsrc.Count )
         {
            context.AddJavascriptSource(StringUtil.RTrim( ((String)Form.Jscriptsrc.Item(idxLst))), "?201913181592", true);
            idxLst = (int)(idxLst+1);
         }
         if ( ! outputEnabled )
         {
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
         }
         /* End function define_styles */
      }

      protected void include_jscripts( )
      {
         context.AddJavascriptSource("messages.eng.js", "?"+GetCacheInvalidationToken( ), false);
         context.AddJavascriptSource("fyactividadplancantact.js", "?201913181592", false);
         /* End function include_jscripts */
      }

      protected void init_default_properties( )
      {
         lblTitle_Internalname = "TITLE";
         divTitlecontainer_Internalname = "TITLECONTAINER";
         bttBtn_first_Internalname = "BTN_FIRST";
         bttBtn_previous_Internalname = "BTN_PREVIOUS";
         bttBtn_next_Internalname = "BTN_NEXT";
         bttBtn_last_Internalname = "BTN_LAST";
         bttBtn_select_Internalname = "BTN_SELECT";
         divToolbarcell_Internalname = "TOOLBARCELL";
         edtFYActividadPlanCantActid_Internalname = "FYACTIVIDADPLANCANTACTID";
         edtActividadParaPlanid_Internalname = "ACTIVIDADPARAPLANID";
         edtFYActividadPlanCantActFY_Internalname = "FYACTIVIDADPLANCANTACTFY";
         edtmes1_Internalname = "MES1";
         edtmes2_Internalname = "MES2";
         edtmes3_Internalname = "MES3";
         edtmes4_Internalname = "MES4";
         edtmes5_Internalname = "MES5";
         edtmes6_Internalname = "MES6";
         edtmes7_Internalname = "MES7";
         edtmes8_Internalname = "MES8";
         edtmes9_Internalname = "MES9";
         edtmes10_Internalname = "MES10";
         edtmes11_Internalname = "MES11";
         edtmes12_Internalname = "MES12";
         divFormcontainer_Internalname = "FORMCONTAINER";
         bttBtn_enter_Internalname = "BTN_ENTER";
         bttBtn_cancel_Internalname = "BTN_CANCEL";
         bttBtn_delete_Internalname = "BTN_DELETE";
         divMaintable_Internalname = "MAINTABLE";
         Form.Internalname = "FORM";
         imgprompt_23_Internalname = "PROMPT_23";
      }

      public override void initialize_properties( )
      {
         context.SetDefaultTheme("Carmine");
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         init_default_properties( ) ;
         Form.Headerrawhtml = "";
         Form.Background = "";
         Form.Textcolor = 0;
         Form.Backcolor = (int)(0xFFFFFF);
         Form.Caption = "FYActividad Plan Cant Act";
         bttBtn_delete_Enabled = 1;
         bttBtn_delete_Visible = 1;
         bttBtn_cancel_Visible = 1;
         bttBtn_enter_Enabled = 1;
         bttBtn_enter_Visible = 1;
         edtmes12_Jsonclick = "";
         edtmes12_Enabled = 1;
         edtmes11_Jsonclick = "";
         edtmes11_Enabled = 1;
         edtmes10_Jsonclick = "";
         edtmes10_Enabled = 1;
         edtmes9_Jsonclick = "";
         edtmes9_Enabled = 1;
         edtmes8_Jsonclick = "";
         edtmes8_Enabled = 1;
         edtmes7_Jsonclick = "";
         edtmes7_Enabled = 1;
         edtmes6_Jsonclick = "";
         edtmes6_Enabled = 1;
         edtmes5_Jsonclick = "";
         edtmes5_Enabled = 1;
         edtmes4_Jsonclick = "";
         edtmes4_Enabled = 1;
         edtmes3_Jsonclick = "";
         edtmes3_Enabled = 1;
         edtmes2_Jsonclick = "";
         edtmes2_Enabled = 1;
         edtmes1_Jsonclick = "";
         edtmes1_Enabled = 1;
         edtFYActividadPlanCantActFY_Jsonclick = "";
         edtFYActividadPlanCantActFY_Enabled = 1;
         imgprompt_23_Visible = 1;
         imgprompt_23_Link = "";
         edtActividadParaPlanid_Jsonclick = "";
         edtActividadParaPlanid_Enabled = 1;
         edtFYActividadPlanCantActid_Jsonclick = "";
         edtFYActividadPlanCantActid_Enabled = 1;
         bttBtn_select_Visible = 1;
         bttBtn_last_Visible = 1;
         bttBtn_next_Visible = 1;
         bttBtn_previous_Visible = 1;
         bttBtn_first_Visible = 1;
         context.GX_msglist.DisplayMode = 1;
         if ( context.isSpaRequest( ) )
         {
            enableJsOutput();
         }
      }

      protected void dynload_actions( )
      {
         /* End function dynload_actions */
      }

      protected void init_web_controls( )
      {
         /* End function init_web_controls */
      }

      protected void AfterKeyLoadScreen( )
      {
         IsConfirmed = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "IsConfirmed", StringUtil.LTrim( StringUtil.Str( (decimal)(IsConfirmed), 4, 0)));
         getEqualNoModal( ) ;
         GX_FocusControl = edtActividadParaPlanid_Internalname;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         standaloneNotModal( ) ;
         standaloneModal( ) ;
         /* End function AfterKeyLoadScreen */
      }

      public void Valid_Fyactividadplancantactid( int GX_Parm1 ,
                                                  int GX_Parm2 ,
                                                  int GX_Parm3 ,
                                                  int GX_Parm4 ,
                                                  int GX_Parm5 ,
                                                  int GX_Parm6 ,
                                                  int GX_Parm7 ,
                                                  int GX_Parm8 ,
                                                  int GX_Parm9 ,
                                                  int GX_Parm10 ,
                                                  int GX_Parm11 ,
                                                  int GX_Parm12 ,
                                                  int GX_Parm13 ,
                                                  int GX_Parm14 ,
                                                  int GX_Parm15 )
      {
         A24FYActividadPlanCantActid = GX_Parm1;
         A104FYActividadPlanCantActFY = GX_Parm2;
         A105mes1 = GX_Parm3;
         n105mes1 = false;
         A106mes2 = GX_Parm4;
         n106mes2 = false;
         A107mes3 = GX_Parm5;
         n107mes3 = false;
         A108mes4 = GX_Parm6;
         n108mes4 = false;
         A109mes5 = GX_Parm7;
         n109mes5 = false;
         A110mes6 = GX_Parm8;
         n110mes6 = false;
         A111mes7 = GX_Parm9;
         n111mes7 = false;
         A112mes8 = GX_Parm10;
         n112mes8 = false;
         A113mes9 = GX_Parm11;
         n113mes9 = false;
         A114mes10 = GX_Parm12;
         n114mes10 = false;
         A115mes11 = GX_Parm13;
         n115mes11 = false;
         A116mes12 = GX_Parm14;
         n116mes12 = false;
         A23ActividadParaPlanid = GX_Parm15;
         context.wbHandled = 1;
         AfterKeyLoadScreen( ) ;
         Draw( ) ;
         send_integrity_footer_hashes( ) ;
         dynload_actions( ) ;
         if ( AnyError == 1 )
         {
         }
         isValidOutput.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(A23ActividadParaPlanid), 9, 0, ".", "")));
         isValidOutput.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(A104FYActividadPlanCantActFY), 9, 0, ".", "")));
         isValidOutput.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(A105mes1), 9, 0, ".", "")));
         isValidOutput.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(A106mes2), 9, 0, ".", "")));
         isValidOutput.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(A107mes3), 9, 0, ".", "")));
         isValidOutput.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(A108mes4), 9, 0, ".", "")));
         isValidOutput.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(A109mes5), 9, 0, ".", "")));
         isValidOutput.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(A110mes6), 9, 0, ".", "")));
         isValidOutput.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(A111mes7), 9, 0, ".", "")));
         isValidOutput.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(A112mes8), 9, 0, ".", "")));
         isValidOutput.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(A113mes9), 9, 0, ".", "")));
         isValidOutput.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(A114mes10), 9, 0, ".", "")));
         isValidOutput.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(A115mes11), 9, 0, ".", "")));
         isValidOutput.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(A116mes12), 9, 0, ".", "")));
         isValidOutput.Add(StringUtil.RTrim( Gx_mode));
         isValidOutput.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(Z24FYActividadPlanCantActid), 9, 0, ".", "")));
         isValidOutput.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(Z23ActividadParaPlanid), 9, 0, ".", "")));
         isValidOutput.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(Z104FYActividadPlanCantActFY), 9, 0, ".", "")));
         isValidOutput.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(Z105mes1), 9, 0, ".", "")));
         isValidOutput.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(Z106mes2), 9, 0, ".", "")));
         isValidOutput.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(Z107mes3), 9, 0, ".", "")));
         isValidOutput.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(Z108mes4), 9, 0, ".", "")));
         isValidOutput.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(Z109mes5), 9, 0, ".", "")));
         isValidOutput.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(Z110mes6), 9, 0, ".", "")));
         isValidOutput.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(Z111mes7), 9, 0, ".", "")));
         isValidOutput.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(Z112mes8), 9, 0, ".", "")));
         isValidOutput.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(Z113mes9), 9, 0, ".", "")));
         isValidOutput.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(Z114mes10), 9, 0, ".", "")));
         isValidOutput.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(Z115mes11), 9, 0, ".", "")));
         isValidOutput.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(Z116mes12), 9, 0, ".", "")));
         isValidOutput.Add(bttBtn_delete_Enabled);
         isValidOutput.Add(bttBtn_enter_Enabled);
         isValidOutput.Add(context.GX_msglist.ToJavascriptSource());
         isValidOutput.Add(context.httpAjaxContext.ajax_rsp_get_hiddens( ));
         context.GX_webresponse.AddString(isValidOutput.ToJSonString());
         wbTemp = context.ResponseContentType( "application/json");
      }

      public void Valid_Actividadparaplanid( int GX_Parm1 )
      {
         A23ActividadParaPlanid = GX_Parm1;
         /* Using cursor T000N14 */
         pr_datastore1.execute(12, new Object[] {A23ActividadParaPlanid});
         if ( (pr_datastore1.getStatus(12) == 101) )
         {
            GX_msglist.addItem("No matching 'Actividad Para Plan'.", "ForeignKeyNotFound", 1, "ACTIVIDADPARAPLANID");
            AnyError = 1;
            GX_FocusControl = edtActividadParaPlanid_Internalname;
         }
         pr_datastore1.close(12);
         dynload_actions( ) ;
         isValidOutput.Add(context.GX_msglist.ToJavascriptSource());
         isValidOutput.Add(context.httpAjaxContext.ajax_rsp_get_hiddens( ));
         context.GX_webresponse.AddString(isValidOutput.ToJSonString());
         wbTemp = context.ResponseContentType( "application/json");
      }

      public override bool SupportAjaxEvent( )
      {
         return true ;
      }

      public override void InitializeDynEvents( )
      {
         setEventMetadata("ENTER","{handler:'UserMainFullajax',iparms:[{postForm:true}]");
         setEventMetadata("ENTER",",oparms:[]}");
         setEventMetadata("REFRESH","{handler:'Refresh',iparms:[]");
         setEventMetadata("REFRESH",",oparms:[]}");
         return  ;
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
         pr_datastore1.close(1);
         pr_datastore1.close(12);
      }

      public override void initialize( )
      {
         sPrefix = "";
         scmdbuf = "";
         gxfirstwebparm = "";
         gxfirstwebparm_bkp = "";
         GXKey = "";
         PreviousTooltip = "";
         PreviousCaption = "";
         Form = new GXWebForm();
         GX_FocusControl = "";
         lblTitle_Jsonclick = "";
         ClassString = "";
         StyleString = "";
         TempTags = "";
         bttBtn_first_Jsonclick = "";
         bttBtn_previous_Jsonclick = "";
         bttBtn_next_Jsonclick = "";
         bttBtn_last_Jsonclick = "";
         bttBtn_select_Jsonclick = "";
         sImgUrl = "";
         bttBtn_enter_Jsonclick = "";
         bttBtn_cancel_Jsonclick = "";
         bttBtn_delete_Jsonclick = "";
         Gx_mode = "";
         sEvt = "";
         EvtGridId = "";
         EvtRowId = "";
         sEvtType = "";
         T000N5_A24FYActividadPlanCantActid = new int[1] ;
         T000N5_A104FYActividadPlanCantActFY = new int[1] ;
         T000N5_A105mes1 = new int[1] ;
         T000N5_n105mes1 = new bool[] {false} ;
         T000N5_A106mes2 = new int[1] ;
         T000N5_n106mes2 = new bool[] {false} ;
         T000N5_A107mes3 = new int[1] ;
         T000N5_n107mes3 = new bool[] {false} ;
         T000N5_A108mes4 = new int[1] ;
         T000N5_n108mes4 = new bool[] {false} ;
         T000N5_A109mes5 = new int[1] ;
         T000N5_n109mes5 = new bool[] {false} ;
         T000N5_A110mes6 = new int[1] ;
         T000N5_n110mes6 = new bool[] {false} ;
         T000N5_A111mes7 = new int[1] ;
         T000N5_n111mes7 = new bool[] {false} ;
         T000N5_A112mes8 = new int[1] ;
         T000N5_n112mes8 = new bool[] {false} ;
         T000N5_A113mes9 = new int[1] ;
         T000N5_n113mes9 = new bool[] {false} ;
         T000N5_A114mes10 = new int[1] ;
         T000N5_n114mes10 = new bool[] {false} ;
         T000N5_A115mes11 = new int[1] ;
         T000N5_n115mes11 = new bool[] {false} ;
         T000N5_A116mes12 = new int[1] ;
         T000N5_n116mes12 = new bool[] {false} ;
         T000N5_A23ActividadParaPlanid = new int[1] ;
         T000N4_A23ActividadParaPlanid = new int[1] ;
         T000N6_A23ActividadParaPlanid = new int[1] ;
         T000N7_A24FYActividadPlanCantActid = new int[1] ;
         T000N3_A24FYActividadPlanCantActid = new int[1] ;
         T000N3_A104FYActividadPlanCantActFY = new int[1] ;
         T000N3_A105mes1 = new int[1] ;
         T000N3_n105mes1 = new bool[] {false} ;
         T000N3_A106mes2 = new int[1] ;
         T000N3_n106mes2 = new bool[] {false} ;
         T000N3_A107mes3 = new int[1] ;
         T000N3_n107mes3 = new bool[] {false} ;
         T000N3_A108mes4 = new int[1] ;
         T000N3_n108mes4 = new bool[] {false} ;
         T000N3_A109mes5 = new int[1] ;
         T000N3_n109mes5 = new bool[] {false} ;
         T000N3_A110mes6 = new int[1] ;
         T000N3_n110mes6 = new bool[] {false} ;
         T000N3_A111mes7 = new int[1] ;
         T000N3_n111mes7 = new bool[] {false} ;
         T000N3_A112mes8 = new int[1] ;
         T000N3_n112mes8 = new bool[] {false} ;
         T000N3_A113mes9 = new int[1] ;
         T000N3_n113mes9 = new bool[] {false} ;
         T000N3_A114mes10 = new int[1] ;
         T000N3_n114mes10 = new bool[] {false} ;
         T000N3_A115mes11 = new int[1] ;
         T000N3_n115mes11 = new bool[] {false} ;
         T000N3_A116mes12 = new int[1] ;
         T000N3_n116mes12 = new bool[] {false} ;
         T000N3_A23ActividadParaPlanid = new int[1] ;
         sMode23 = "";
         T000N8_A24FYActividadPlanCantActid = new int[1] ;
         T000N9_A24FYActividadPlanCantActid = new int[1] ;
         T000N2_A24FYActividadPlanCantActid = new int[1] ;
         T000N2_A104FYActividadPlanCantActFY = new int[1] ;
         T000N2_A105mes1 = new int[1] ;
         T000N2_n105mes1 = new bool[] {false} ;
         T000N2_A106mes2 = new int[1] ;
         T000N2_n106mes2 = new bool[] {false} ;
         T000N2_A107mes3 = new int[1] ;
         T000N2_n107mes3 = new bool[] {false} ;
         T000N2_A108mes4 = new int[1] ;
         T000N2_n108mes4 = new bool[] {false} ;
         T000N2_A109mes5 = new int[1] ;
         T000N2_n109mes5 = new bool[] {false} ;
         T000N2_A110mes6 = new int[1] ;
         T000N2_n110mes6 = new bool[] {false} ;
         T000N2_A111mes7 = new int[1] ;
         T000N2_n111mes7 = new bool[] {false} ;
         T000N2_A112mes8 = new int[1] ;
         T000N2_n112mes8 = new bool[] {false} ;
         T000N2_A113mes9 = new int[1] ;
         T000N2_n113mes9 = new bool[] {false} ;
         T000N2_A114mes10 = new int[1] ;
         T000N2_n114mes10 = new bool[] {false} ;
         T000N2_A115mes11 = new int[1] ;
         T000N2_n115mes11 = new bool[] {false} ;
         T000N2_A116mes12 = new int[1] ;
         T000N2_n116mes12 = new bool[] {false} ;
         T000N2_A23ActividadParaPlanid = new int[1] ;
         T000N10_A24FYActividadPlanCantActid = new int[1] ;
         T000N13_A24FYActividadPlanCantActid = new int[1] ;
         sDynURL = "";
         FormProcess = "";
         bodyStyle = "";
         isValidOutput = new GxUnknownObjectCollection();
         T000N14_A23ActividadParaPlanid = new int[1] ;
         pr_datastore1 = new DataStoreProvider(context, new GeneXus.Programs.fyactividadplancantact__datastore1(),
            new Object[][] {
                new Object[] {
               T000N2_A24FYActividadPlanCantActid, T000N2_A104FYActividadPlanCantActFY, T000N2_A105mes1, T000N2_n105mes1, T000N2_A106mes2, T000N2_n106mes2, T000N2_A107mes3, T000N2_n107mes3, T000N2_A108mes4, T000N2_n108mes4,
               T000N2_A109mes5, T000N2_n109mes5, T000N2_A110mes6, T000N2_n110mes6, T000N2_A111mes7, T000N2_n111mes7, T000N2_A112mes8, T000N2_n112mes8, T000N2_A113mes9, T000N2_n113mes9,
               T000N2_A114mes10, T000N2_n114mes10, T000N2_A115mes11, T000N2_n115mes11, T000N2_A116mes12, T000N2_n116mes12, T000N2_A23ActividadParaPlanid
               }
               , new Object[] {
               T000N3_A24FYActividadPlanCantActid, T000N3_A104FYActividadPlanCantActFY, T000N3_A105mes1, T000N3_n105mes1, T000N3_A106mes2, T000N3_n106mes2, T000N3_A107mes3, T000N3_n107mes3, T000N3_A108mes4, T000N3_n108mes4,
               T000N3_A109mes5, T000N3_n109mes5, T000N3_A110mes6, T000N3_n110mes6, T000N3_A111mes7, T000N3_n111mes7, T000N3_A112mes8, T000N3_n112mes8, T000N3_A113mes9, T000N3_n113mes9,
               T000N3_A114mes10, T000N3_n114mes10, T000N3_A115mes11, T000N3_n115mes11, T000N3_A116mes12, T000N3_n116mes12, T000N3_A23ActividadParaPlanid
               }
               , new Object[] {
               T000N4_A23ActividadParaPlanid
               }
               , new Object[] {
               T000N5_A24FYActividadPlanCantActid, T000N5_A104FYActividadPlanCantActFY, T000N5_A105mes1, T000N5_n105mes1, T000N5_A106mes2, T000N5_n106mes2, T000N5_A107mes3, T000N5_n107mes3, T000N5_A108mes4, T000N5_n108mes4,
               T000N5_A109mes5, T000N5_n109mes5, T000N5_A110mes6, T000N5_n110mes6, T000N5_A111mes7, T000N5_n111mes7, T000N5_A112mes8, T000N5_n112mes8, T000N5_A113mes9, T000N5_n113mes9,
               T000N5_A114mes10, T000N5_n114mes10, T000N5_A115mes11, T000N5_n115mes11, T000N5_A116mes12, T000N5_n116mes12, T000N5_A23ActividadParaPlanid
               }
               , new Object[] {
               T000N6_A23ActividadParaPlanid
               }
               , new Object[] {
               T000N7_A24FYActividadPlanCantActid
               }
               , new Object[] {
               T000N8_A24FYActividadPlanCantActid
               }
               , new Object[] {
               T000N9_A24FYActividadPlanCantActid
               }
               , new Object[] {
               T000N10_A24FYActividadPlanCantActid
               }
               , new Object[] {
               }
               , new Object[] {
               }
               , new Object[] {
               T000N13_A24FYActividadPlanCantActid
               }
               , new Object[] {
               T000N14_A23ActividadParaPlanid
               }
            }
         );
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.fyactividadplancantact__default(),
            new Object[][] {
            }
         );
      }

      private short GxWebError ;
      private short gxcookieaux ;
      private short IsConfirmed ;
      private short IsModified ;
      private short AnyError ;
      private short nKeyPressed ;
      private short initialized ;
      private short GX_JID ;
      private short RcdFound23 ;
      private short Gx_BScreen ;
      private short gxajaxcallmode ;
      private short wbTemp ;
      private int Z24FYActividadPlanCantActid ;
      private int Z104FYActividadPlanCantActFY ;
      private int Z105mes1 ;
      private int Z106mes2 ;
      private int Z107mes3 ;
      private int Z108mes4 ;
      private int Z109mes5 ;
      private int Z110mes6 ;
      private int Z111mes7 ;
      private int Z112mes8 ;
      private int Z113mes9 ;
      private int Z114mes10 ;
      private int Z115mes11 ;
      private int Z116mes12 ;
      private int Z23ActividadParaPlanid ;
      private int A23ActividadParaPlanid ;
      private int trnEnded ;
      private int bttBtn_first_Visible ;
      private int bttBtn_previous_Visible ;
      private int bttBtn_next_Visible ;
      private int bttBtn_last_Visible ;
      private int bttBtn_select_Visible ;
      private int A24FYActividadPlanCantActid ;
      private int edtFYActividadPlanCantActid_Enabled ;
      private int edtActividadParaPlanid_Enabled ;
      private int imgprompt_23_Visible ;
      private int A104FYActividadPlanCantActFY ;
      private int edtFYActividadPlanCantActFY_Enabled ;
      private int A105mes1 ;
      private int edtmes1_Enabled ;
      private int A106mes2 ;
      private int edtmes2_Enabled ;
      private int A107mes3 ;
      private int edtmes3_Enabled ;
      private int A108mes4 ;
      private int edtmes4_Enabled ;
      private int A109mes5 ;
      private int edtmes5_Enabled ;
      private int A110mes6 ;
      private int edtmes6_Enabled ;
      private int A111mes7 ;
      private int edtmes7_Enabled ;
      private int A112mes8 ;
      private int edtmes8_Enabled ;
      private int A113mes9 ;
      private int edtmes9_Enabled ;
      private int A114mes10 ;
      private int edtmes10_Enabled ;
      private int A115mes11 ;
      private int edtmes11_Enabled ;
      private int A116mes12 ;
      private int edtmes12_Enabled ;
      private int bttBtn_enter_Visible ;
      private int bttBtn_enter_Enabled ;
      private int bttBtn_cancel_Visible ;
      private int bttBtn_delete_Visible ;
      private int bttBtn_delete_Enabled ;
      private int idxLst ;
      private String sPrefix ;
      private String scmdbuf ;
      private String gxfirstwebparm ;
      private String gxfirstwebparm_bkp ;
      private String GXKey ;
      private String PreviousTooltip ;
      private String PreviousCaption ;
      private String GX_FocusControl ;
      private String edtFYActividadPlanCantActid_Internalname ;
      private String divMaintable_Internalname ;
      private String divTitlecontainer_Internalname ;
      private String lblTitle_Internalname ;
      private String lblTitle_Jsonclick ;
      private String ClassString ;
      private String StyleString ;
      private String divFormcontainer_Internalname ;
      private String divToolbarcell_Internalname ;
      private String TempTags ;
      private String bttBtn_first_Internalname ;
      private String bttBtn_first_Jsonclick ;
      private String bttBtn_previous_Internalname ;
      private String bttBtn_previous_Jsonclick ;
      private String bttBtn_next_Internalname ;
      private String bttBtn_next_Jsonclick ;
      private String bttBtn_last_Internalname ;
      private String bttBtn_last_Jsonclick ;
      private String bttBtn_select_Internalname ;
      private String bttBtn_select_Jsonclick ;
      private String edtFYActividadPlanCantActid_Jsonclick ;
      private String edtActividadParaPlanid_Internalname ;
      private String edtActividadParaPlanid_Jsonclick ;
      private String sImgUrl ;
      private String imgprompt_23_Internalname ;
      private String imgprompt_23_Link ;
      private String edtFYActividadPlanCantActFY_Internalname ;
      private String edtFYActividadPlanCantActFY_Jsonclick ;
      private String edtmes1_Internalname ;
      private String edtmes1_Jsonclick ;
      private String edtmes2_Internalname ;
      private String edtmes2_Jsonclick ;
      private String edtmes3_Internalname ;
      private String edtmes3_Jsonclick ;
      private String edtmes4_Internalname ;
      private String edtmes4_Jsonclick ;
      private String edtmes5_Internalname ;
      private String edtmes5_Jsonclick ;
      private String edtmes6_Internalname ;
      private String edtmes6_Jsonclick ;
      private String edtmes7_Internalname ;
      private String edtmes7_Jsonclick ;
      private String edtmes8_Internalname ;
      private String edtmes8_Jsonclick ;
      private String edtmes9_Internalname ;
      private String edtmes9_Jsonclick ;
      private String edtmes10_Internalname ;
      private String edtmes10_Jsonclick ;
      private String edtmes11_Internalname ;
      private String edtmes11_Jsonclick ;
      private String edtmes12_Internalname ;
      private String edtmes12_Jsonclick ;
      private String bttBtn_enter_Internalname ;
      private String bttBtn_enter_Jsonclick ;
      private String bttBtn_cancel_Internalname ;
      private String bttBtn_cancel_Jsonclick ;
      private String bttBtn_delete_Internalname ;
      private String bttBtn_delete_Jsonclick ;
      private String Gx_mode ;
      private String sEvt ;
      private String EvtGridId ;
      private String EvtRowId ;
      private String sEvtType ;
      private String sMode23 ;
      private String sDynURL ;
      private String FormProcess ;
      private String bodyStyle ;
      private bool entryPointCalled ;
      private bool toggleJsOutput ;
      private bool wbErr ;
      private bool n105mes1 ;
      private bool n106mes2 ;
      private bool n107mes3 ;
      private bool n108mes4 ;
      private bool n109mes5 ;
      private bool n110mes6 ;
      private bool n111mes7 ;
      private bool n112mes8 ;
      private bool n113mes9 ;
      private bool n114mes10 ;
      private bool n115mes11 ;
      private bool n116mes12 ;
      private bool Gx_longc ;
      private GxUnknownObjectCollection isValidOutput ;
      private IGxDataStore dsDataStore1 ;
      private IGxDataStore dsDefault ;
      private IDataStoreProvider pr_datastore1 ;
      private int[] T000N5_A24FYActividadPlanCantActid ;
      private int[] T000N5_A104FYActividadPlanCantActFY ;
      private int[] T000N5_A105mes1 ;
      private bool[] T000N5_n105mes1 ;
      private int[] T000N5_A106mes2 ;
      private bool[] T000N5_n106mes2 ;
      private int[] T000N5_A107mes3 ;
      private bool[] T000N5_n107mes3 ;
      private int[] T000N5_A108mes4 ;
      private bool[] T000N5_n108mes4 ;
      private int[] T000N5_A109mes5 ;
      private bool[] T000N5_n109mes5 ;
      private int[] T000N5_A110mes6 ;
      private bool[] T000N5_n110mes6 ;
      private int[] T000N5_A111mes7 ;
      private bool[] T000N5_n111mes7 ;
      private int[] T000N5_A112mes8 ;
      private bool[] T000N5_n112mes8 ;
      private int[] T000N5_A113mes9 ;
      private bool[] T000N5_n113mes9 ;
      private int[] T000N5_A114mes10 ;
      private bool[] T000N5_n114mes10 ;
      private int[] T000N5_A115mes11 ;
      private bool[] T000N5_n115mes11 ;
      private int[] T000N5_A116mes12 ;
      private bool[] T000N5_n116mes12 ;
      private int[] T000N5_A23ActividadParaPlanid ;
      private int[] T000N4_A23ActividadParaPlanid ;
      private int[] T000N6_A23ActividadParaPlanid ;
      private int[] T000N7_A24FYActividadPlanCantActid ;
      private int[] T000N3_A24FYActividadPlanCantActid ;
      private int[] T000N3_A104FYActividadPlanCantActFY ;
      private int[] T000N3_A105mes1 ;
      private bool[] T000N3_n105mes1 ;
      private int[] T000N3_A106mes2 ;
      private bool[] T000N3_n106mes2 ;
      private int[] T000N3_A107mes3 ;
      private bool[] T000N3_n107mes3 ;
      private int[] T000N3_A108mes4 ;
      private bool[] T000N3_n108mes4 ;
      private int[] T000N3_A109mes5 ;
      private bool[] T000N3_n109mes5 ;
      private int[] T000N3_A110mes6 ;
      private bool[] T000N3_n110mes6 ;
      private int[] T000N3_A111mes7 ;
      private bool[] T000N3_n111mes7 ;
      private int[] T000N3_A112mes8 ;
      private bool[] T000N3_n112mes8 ;
      private int[] T000N3_A113mes9 ;
      private bool[] T000N3_n113mes9 ;
      private int[] T000N3_A114mes10 ;
      private bool[] T000N3_n114mes10 ;
      private int[] T000N3_A115mes11 ;
      private bool[] T000N3_n115mes11 ;
      private int[] T000N3_A116mes12 ;
      private bool[] T000N3_n116mes12 ;
      private int[] T000N3_A23ActividadParaPlanid ;
      private int[] T000N8_A24FYActividadPlanCantActid ;
      private int[] T000N9_A24FYActividadPlanCantActid ;
      private int[] T000N2_A24FYActividadPlanCantActid ;
      private int[] T000N2_A104FYActividadPlanCantActFY ;
      private int[] T000N2_A105mes1 ;
      private bool[] T000N2_n105mes1 ;
      private int[] T000N2_A106mes2 ;
      private bool[] T000N2_n106mes2 ;
      private int[] T000N2_A107mes3 ;
      private bool[] T000N2_n107mes3 ;
      private int[] T000N2_A108mes4 ;
      private bool[] T000N2_n108mes4 ;
      private int[] T000N2_A109mes5 ;
      private bool[] T000N2_n109mes5 ;
      private int[] T000N2_A110mes6 ;
      private bool[] T000N2_n110mes6 ;
      private int[] T000N2_A111mes7 ;
      private bool[] T000N2_n111mes7 ;
      private int[] T000N2_A112mes8 ;
      private bool[] T000N2_n112mes8 ;
      private int[] T000N2_A113mes9 ;
      private bool[] T000N2_n113mes9 ;
      private int[] T000N2_A114mes10 ;
      private bool[] T000N2_n114mes10 ;
      private int[] T000N2_A115mes11 ;
      private bool[] T000N2_n115mes11 ;
      private int[] T000N2_A116mes12 ;
      private bool[] T000N2_n116mes12 ;
      private int[] T000N2_A23ActividadParaPlanid ;
      private int[] T000N10_A24FYActividadPlanCantActid ;
      private IDataStoreProvider pr_default ;
      private int[] T000N13_A24FYActividadPlanCantActid ;
      private int[] T000N14_A23ActividadParaPlanid ;
      private GXWebForm Form ;
   }

   public class fyactividadplancantact__datastore1 : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
         ,new ForEachCursor(def[2])
         ,new ForEachCursor(def[3])
         ,new ForEachCursor(def[4])
         ,new ForEachCursor(def[5])
         ,new ForEachCursor(def[6])
         ,new ForEachCursor(def[7])
         ,new ForEachCursor(def[8])
         ,new UpdateCursor(def[9])
         ,new UpdateCursor(def[10])
         ,new ForEachCursor(def[11])
         ,new ForEachCursor(def[12])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmT000N5 ;
          prmT000N5 = new Object[] {
          new Object[] {"@FYActividadPlanCantActid",SqlDbType.Int,9,0}
          } ;
          Object[] prmT000N4 ;
          prmT000N4 = new Object[] {
          new Object[] {"@ActividadParaPlanid",SqlDbType.Int,9,0}
          } ;
          Object[] prmT000N6 ;
          prmT000N6 = new Object[] {
          new Object[] {"@ActividadParaPlanid",SqlDbType.Int,9,0}
          } ;
          Object[] prmT000N7 ;
          prmT000N7 = new Object[] {
          new Object[] {"@FYActividadPlanCantActid",SqlDbType.Int,9,0}
          } ;
          Object[] prmT000N3 ;
          prmT000N3 = new Object[] {
          new Object[] {"@FYActividadPlanCantActid",SqlDbType.Int,9,0}
          } ;
          Object[] prmT000N8 ;
          prmT000N8 = new Object[] {
          new Object[] {"@FYActividadPlanCantActid",SqlDbType.Int,9,0}
          } ;
          Object[] prmT000N9 ;
          prmT000N9 = new Object[] {
          new Object[] {"@FYActividadPlanCantActid",SqlDbType.Int,9,0}
          } ;
          Object[] prmT000N2 ;
          prmT000N2 = new Object[] {
          new Object[] {"@FYActividadPlanCantActid",SqlDbType.Int,9,0}
          } ;
          Object[] prmT000N10 ;
          prmT000N10 = new Object[] {
          new Object[] {"@FYActividadPlanCantActFY",SqlDbType.Int,9,0} ,
          new Object[] {"@mes1",SqlDbType.Int,9,0} ,
          new Object[] {"@mes2",SqlDbType.Int,9,0} ,
          new Object[] {"@mes3",SqlDbType.Int,9,0} ,
          new Object[] {"@mes4",SqlDbType.Int,9,0} ,
          new Object[] {"@mes5",SqlDbType.Int,9,0} ,
          new Object[] {"@mes6",SqlDbType.Int,9,0} ,
          new Object[] {"@mes7",SqlDbType.Int,9,0} ,
          new Object[] {"@mes8",SqlDbType.Int,9,0} ,
          new Object[] {"@mes9",SqlDbType.Int,9,0} ,
          new Object[] {"@mes10",SqlDbType.Int,9,0} ,
          new Object[] {"@mes11",SqlDbType.Int,9,0} ,
          new Object[] {"@mes12",SqlDbType.Int,9,0} ,
          new Object[] {"@ActividadParaPlanid",SqlDbType.Int,9,0}
          } ;
          Object[] prmT000N11 ;
          prmT000N11 = new Object[] {
          new Object[] {"@FYActividadPlanCantActFY",SqlDbType.Int,9,0} ,
          new Object[] {"@mes1",SqlDbType.Int,9,0} ,
          new Object[] {"@mes2",SqlDbType.Int,9,0} ,
          new Object[] {"@mes3",SqlDbType.Int,9,0} ,
          new Object[] {"@mes4",SqlDbType.Int,9,0} ,
          new Object[] {"@mes5",SqlDbType.Int,9,0} ,
          new Object[] {"@mes6",SqlDbType.Int,9,0} ,
          new Object[] {"@mes7",SqlDbType.Int,9,0} ,
          new Object[] {"@mes8",SqlDbType.Int,9,0} ,
          new Object[] {"@mes9",SqlDbType.Int,9,0} ,
          new Object[] {"@mes10",SqlDbType.Int,9,0} ,
          new Object[] {"@mes11",SqlDbType.Int,9,0} ,
          new Object[] {"@mes12",SqlDbType.Int,9,0} ,
          new Object[] {"@ActividadParaPlanid",SqlDbType.Int,9,0} ,
          new Object[] {"@FYActividadPlanCantActid",SqlDbType.Int,9,0}
          } ;
          Object[] prmT000N12 ;
          prmT000N12 = new Object[] {
          new Object[] {"@FYActividadPlanCantActid",SqlDbType.Int,9,0}
          } ;
          Object[] prmT000N13 ;
          prmT000N13 = new Object[] {
          } ;
          Object[] prmT000N14 ;
          prmT000N14 = new Object[] {
          new Object[] {"@ActividadParaPlanid",SqlDbType.Int,9,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("T000N2", "SELECT [id] AS FYActividadPlanCantActid, [FY], [mes1], [mes2], [mes3], [mes4], [mes5], [mes6], [mes7], [mes8], [mes9], [mes10], [mes11], [mes12], [idActividadParaPlan] AS ActividadParaPlanid FROM dbo.[FYActividadPlanCantAct] WITH (UPDLOCK) WHERE [id] = @FYActividadPlanCantActid ",true, GxErrorMask.GX_NOMASK, false, this,prmT000N2,1,0,true,false )
             ,new CursorDef("T000N3", "SELECT [id] AS FYActividadPlanCantActid, [FY], [mes1], [mes2], [mes3], [mes4], [mes5], [mes6], [mes7], [mes8], [mes9], [mes10], [mes11], [mes12], [idActividadParaPlan] AS ActividadParaPlanid FROM dbo.[FYActividadPlanCantAct] WITH (NOLOCK) WHERE [id] = @FYActividadPlanCantActid ",true, GxErrorMask.GX_NOMASK, false, this,prmT000N3,1,0,true,false )
             ,new CursorDef("T000N4", "SELECT [id] AS ActividadParaPlanid FROM dbo.[ActividadParaPlan] WITH (NOLOCK) WHERE [id] = @ActividadParaPlanid ",true, GxErrorMask.GX_NOMASK, false, this,prmT000N4,1,0,true,false )
             ,new CursorDef("T000N5", "SELECT TM1.[id] AS FYActividadPlanCantActid, TM1.[FY], TM1.[mes1], TM1.[mes2], TM1.[mes3], TM1.[mes4], TM1.[mes5], TM1.[mes6], TM1.[mes7], TM1.[mes8], TM1.[mes9], TM1.[mes10], TM1.[mes11], TM1.[mes12], TM1.[idActividadParaPlan] AS ActividadParaPlanid FROM dbo.[FYActividadPlanCantAct] TM1 WITH (NOLOCK) WHERE TM1.[id] = @FYActividadPlanCantActid ORDER BY TM1.[id]  OPTION (FAST 100)",true, GxErrorMask.GX_NOMASK, false, this,prmT000N5,100,0,true,false )
             ,new CursorDef("T000N6", "SELECT [id] AS ActividadParaPlanid FROM dbo.[ActividadParaPlan] WITH (NOLOCK) WHERE [id] = @ActividadParaPlanid ",true, GxErrorMask.GX_NOMASK, false, this,prmT000N6,1,0,true,false )
             ,new CursorDef("T000N7", "SELECT [id] AS FYActividadPlanCantActid FROM dbo.[FYActividadPlanCantAct] WITH (NOLOCK) WHERE [id] = @FYActividadPlanCantActid  OPTION (FAST 1)",true, GxErrorMask.GX_NOMASK, false, this,prmT000N7,1,0,true,false )
             ,new CursorDef("T000N8", "SELECT TOP 1 [id] AS FYActividadPlanCantActid FROM dbo.[FYActividadPlanCantAct] WITH (NOLOCK) WHERE ( [id] > @FYActividadPlanCantActid) ORDER BY [id]  OPTION (FAST 1)",true, GxErrorMask.GX_NOMASK, false, this,prmT000N8,1,0,true,true )
             ,new CursorDef("T000N9", "SELECT TOP 1 [id] AS FYActividadPlanCantActid FROM dbo.[FYActividadPlanCantAct] WITH (NOLOCK) WHERE ( [id] < @FYActividadPlanCantActid) ORDER BY [id] DESC  OPTION (FAST 1)",true, GxErrorMask.GX_NOMASK, false, this,prmT000N9,1,0,true,true )
             ,new CursorDef("T000N10", "INSERT INTO dbo.[FYActividadPlanCantAct]([FY], [mes1], [mes2], [mes3], [mes4], [mes5], [mes6], [mes7], [mes8], [mes9], [mes10], [mes11], [mes12], [idActividadParaPlan]) VALUES(@FYActividadPlanCantActFY, @mes1, @mes2, @mes3, @mes4, @mes5, @mes6, @mes7, @mes8, @mes9, @mes10, @mes11, @mes12, @ActividadParaPlanid); SELECT SCOPE_IDENTITY()", GxErrorMask.GX_NOMASK,prmT000N10)
             ,new CursorDef("T000N11", "UPDATE dbo.[FYActividadPlanCantAct] SET [FY]=@FYActividadPlanCantActFY, [mes1]=@mes1, [mes2]=@mes2, [mes3]=@mes3, [mes4]=@mes4, [mes5]=@mes5, [mes6]=@mes6, [mes7]=@mes7, [mes8]=@mes8, [mes9]=@mes9, [mes10]=@mes10, [mes11]=@mes11, [mes12]=@mes12, [idActividadParaPlan]=@ActividadParaPlanid  WHERE [id] = @FYActividadPlanCantActid", GxErrorMask.GX_NOMASK,prmT000N11)
             ,new CursorDef("T000N12", "DELETE FROM dbo.[FYActividadPlanCantAct]  WHERE [id] = @FYActividadPlanCantActid", GxErrorMask.GX_NOMASK,prmT000N12)
             ,new CursorDef("T000N13", "SELECT [id] AS FYActividadPlanCantActid FROM dbo.[FYActividadPlanCantAct] WITH (NOLOCK) ORDER BY [id]  OPTION (FAST 100)",true, GxErrorMask.GX_NOMASK, false, this,prmT000N13,100,0,true,false )
             ,new CursorDef("T000N14", "SELECT [id] AS ActividadParaPlanid FROM dbo.[ActividadParaPlan] WITH (NOLOCK) WHERE [id] = @ActividadParaPlanid ",true, GxErrorMask.GX_NOMASK, false, this,prmT000N14,1,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((int[]) buf[2])[0] = rslt.getInt(3) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(3);
                ((int[]) buf[4])[0] = rslt.getInt(4) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(4);
                ((int[]) buf[6])[0] = rslt.getInt(5) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(5);
                ((int[]) buf[8])[0] = rslt.getInt(6) ;
                ((bool[]) buf[9])[0] = rslt.wasNull(6);
                ((int[]) buf[10])[0] = rslt.getInt(7) ;
                ((bool[]) buf[11])[0] = rslt.wasNull(7);
                ((int[]) buf[12])[0] = rslt.getInt(8) ;
                ((bool[]) buf[13])[0] = rslt.wasNull(8);
                ((int[]) buf[14])[0] = rslt.getInt(9) ;
                ((bool[]) buf[15])[0] = rslt.wasNull(9);
                ((int[]) buf[16])[0] = rslt.getInt(10) ;
                ((bool[]) buf[17])[0] = rslt.wasNull(10);
                ((int[]) buf[18])[0] = rslt.getInt(11) ;
                ((bool[]) buf[19])[0] = rslt.wasNull(11);
                ((int[]) buf[20])[0] = rslt.getInt(12) ;
                ((bool[]) buf[21])[0] = rslt.wasNull(12);
                ((int[]) buf[22])[0] = rslt.getInt(13) ;
                ((bool[]) buf[23])[0] = rslt.wasNull(13);
                ((int[]) buf[24])[0] = rslt.getInt(14) ;
                ((bool[]) buf[25])[0] = rslt.wasNull(14);
                ((int[]) buf[26])[0] = rslt.getInt(15) ;
                return;
             case 1 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((int[]) buf[2])[0] = rslt.getInt(3) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(3);
                ((int[]) buf[4])[0] = rslt.getInt(4) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(4);
                ((int[]) buf[6])[0] = rslt.getInt(5) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(5);
                ((int[]) buf[8])[0] = rslt.getInt(6) ;
                ((bool[]) buf[9])[0] = rslt.wasNull(6);
                ((int[]) buf[10])[0] = rslt.getInt(7) ;
                ((bool[]) buf[11])[0] = rslt.wasNull(7);
                ((int[]) buf[12])[0] = rslt.getInt(8) ;
                ((bool[]) buf[13])[0] = rslt.wasNull(8);
                ((int[]) buf[14])[0] = rslt.getInt(9) ;
                ((bool[]) buf[15])[0] = rslt.wasNull(9);
                ((int[]) buf[16])[0] = rslt.getInt(10) ;
                ((bool[]) buf[17])[0] = rslt.wasNull(10);
                ((int[]) buf[18])[0] = rslt.getInt(11) ;
                ((bool[]) buf[19])[0] = rslt.wasNull(11);
                ((int[]) buf[20])[0] = rslt.getInt(12) ;
                ((bool[]) buf[21])[0] = rslt.wasNull(12);
                ((int[]) buf[22])[0] = rslt.getInt(13) ;
                ((bool[]) buf[23])[0] = rslt.wasNull(13);
                ((int[]) buf[24])[0] = rslt.getInt(14) ;
                ((bool[]) buf[25])[0] = rslt.wasNull(14);
                ((int[]) buf[26])[0] = rslt.getInt(15) ;
                return;
             case 2 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 3 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((int[]) buf[2])[0] = rslt.getInt(3) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(3);
                ((int[]) buf[4])[0] = rslt.getInt(4) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(4);
                ((int[]) buf[6])[0] = rslt.getInt(5) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(5);
                ((int[]) buf[8])[0] = rslt.getInt(6) ;
                ((bool[]) buf[9])[0] = rslt.wasNull(6);
                ((int[]) buf[10])[0] = rslt.getInt(7) ;
                ((bool[]) buf[11])[0] = rslt.wasNull(7);
                ((int[]) buf[12])[0] = rslt.getInt(8) ;
                ((bool[]) buf[13])[0] = rslt.wasNull(8);
                ((int[]) buf[14])[0] = rslt.getInt(9) ;
                ((bool[]) buf[15])[0] = rslt.wasNull(9);
                ((int[]) buf[16])[0] = rslt.getInt(10) ;
                ((bool[]) buf[17])[0] = rslt.wasNull(10);
                ((int[]) buf[18])[0] = rslt.getInt(11) ;
                ((bool[]) buf[19])[0] = rslt.wasNull(11);
                ((int[]) buf[20])[0] = rslt.getInt(12) ;
                ((bool[]) buf[21])[0] = rslt.wasNull(12);
                ((int[]) buf[22])[0] = rslt.getInt(13) ;
                ((bool[]) buf[23])[0] = rslt.wasNull(13);
                ((int[]) buf[24])[0] = rslt.getInt(14) ;
                ((bool[]) buf[25])[0] = rslt.wasNull(14);
                ((int[]) buf[26])[0] = rslt.getInt(15) ;
                return;
             case 4 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 5 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 6 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 7 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 8 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 11 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 12 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 1 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 2 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 3 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 4 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 5 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 6 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 7 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 8 :
                stmt.SetParameter(1, (int)parms[0]);
                if ( (bool)parms[1] )
                {
                   stmt.setNull( 2 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(2, (int)parms[2]);
                }
                if ( (bool)parms[3] )
                {
                   stmt.setNull( 3 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(3, (int)parms[4]);
                }
                if ( (bool)parms[5] )
                {
                   stmt.setNull( 4 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(4, (int)parms[6]);
                }
                if ( (bool)parms[7] )
                {
                   stmt.setNull( 5 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(5, (int)parms[8]);
                }
                if ( (bool)parms[9] )
                {
                   stmt.setNull( 6 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(6, (int)parms[10]);
                }
                if ( (bool)parms[11] )
                {
                   stmt.setNull( 7 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(7, (int)parms[12]);
                }
                if ( (bool)parms[13] )
                {
                   stmt.setNull( 8 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(8, (int)parms[14]);
                }
                if ( (bool)parms[15] )
                {
                   stmt.setNull( 9 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(9, (int)parms[16]);
                }
                if ( (bool)parms[17] )
                {
                   stmt.setNull( 10 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(10, (int)parms[18]);
                }
                if ( (bool)parms[19] )
                {
                   stmt.setNull( 11 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(11, (int)parms[20]);
                }
                if ( (bool)parms[21] )
                {
                   stmt.setNull( 12 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(12, (int)parms[22]);
                }
                if ( (bool)parms[23] )
                {
                   stmt.setNull( 13 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(13, (int)parms[24]);
                }
                stmt.SetParameter(14, (int)parms[25]);
                return;
             case 9 :
                stmt.SetParameter(1, (int)parms[0]);
                if ( (bool)parms[1] )
                {
                   stmt.setNull( 2 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(2, (int)parms[2]);
                }
                if ( (bool)parms[3] )
                {
                   stmt.setNull( 3 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(3, (int)parms[4]);
                }
                if ( (bool)parms[5] )
                {
                   stmt.setNull( 4 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(4, (int)parms[6]);
                }
                if ( (bool)parms[7] )
                {
                   stmt.setNull( 5 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(5, (int)parms[8]);
                }
                if ( (bool)parms[9] )
                {
                   stmt.setNull( 6 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(6, (int)parms[10]);
                }
                if ( (bool)parms[11] )
                {
                   stmt.setNull( 7 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(7, (int)parms[12]);
                }
                if ( (bool)parms[13] )
                {
                   stmt.setNull( 8 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(8, (int)parms[14]);
                }
                if ( (bool)parms[15] )
                {
                   stmt.setNull( 9 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(9, (int)parms[16]);
                }
                if ( (bool)parms[17] )
                {
                   stmt.setNull( 10 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(10, (int)parms[18]);
                }
                if ( (bool)parms[19] )
                {
                   stmt.setNull( 11 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(11, (int)parms[20]);
                }
                if ( (bool)parms[21] )
                {
                   stmt.setNull( 12 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(12, (int)parms[22]);
                }
                if ( (bool)parms[23] )
                {
                   stmt.setNull( 13 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(13, (int)parms[24]);
                }
                stmt.SetParameter(14, (int)parms[25]);
                stmt.SetParameter(15, (int)parms[26]);
                return;
             case 10 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 12 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
       }
    }

    public String getDataStoreName( )
    {
       return "DATASTORE1";
    }

 }

 public class fyactividadplancantact__default : DataStoreHelperBase, IDataStoreHelper
 {
    public ICursor[] getCursors( )
    {
       cursorDefinitions();
       return new Cursor[] {
     };
  }

  private static CursorDef[] def;
  private void cursorDefinitions( )
  {
     if ( def == null )
     {
        def= new CursorDef[] {
        };
     }
  }

  public void getResults( int cursor ,
                          IFieldGetter rslt ,
                          Object[] buf )
  {
     switch ( cursor )
     {
     }
  }

  public void setParameters( int cursor ,
                             IFieldSetter stmt ,
                             Object[] parms )
  {
     switch ( cursor )
     {
     }
  }

}

}
