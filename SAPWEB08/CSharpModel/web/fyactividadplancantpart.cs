/*
               File: FYActividadPlanCantPart
        Description: FYActividad Plan Cant Part
             Author: GeneXus C# Generator version 16_0_0-127771
       Generated on: 1/3/2019 18:1:46.52
       Program type: Callable routine
          Main DBMS: SQL Server
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Xml.Serialization;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class fyactividadplancantpart : GXDataArea, System.Web.SessionState.IRequiresSessionState
   {
      protected void INITENV( )
      {
         if ( GxWebError != 0 )
         {
            return  ;
         }
      }

      protected void INITTRN( )
      {
         initialize_properties( ) ;
         entryPointCalled = false;
         gxfirstwebparm = GetNextPar( );
         gxfirstwebparm_bkp = gxfirstwebparm;
         gxfirstwebparm = DecryptAjaxCall( gxfirstwebparm);
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         if ( StringUtil.StrCmp(gxfirstwebparm, "dyncall") == 0 )
         {
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            dyncall( GetNextPar( )) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxExecAct_"+"gxLoad_2") == 0 )
         {
            A23ActividadParaPlanid = (int)(NumberUtil.Val( GetNextPar( ), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A23ActividadParaPlanid", StringUtil.LTrim( StringUtil.Str( (decimal)(A23ActividadParaPlanid), 9, 0)));
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxLoad_2( A23ActividadParaPlanid) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxEvt") == 0 )
         {
            setAjaxEventMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxfirstwebparm = GetNextPar( );
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxfullajaxEvt") == 0 )
         {
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxfirstwebparm = GetNextPar( );
         }
         else
         {
            if ( ! IsValidAjaxCall( false) )
            {
               GxWebError = 1;
               return  ;
            }
            gxfirstwebparm = gxfirstwebparm_bkp;
         }
         if ( context.isSpaRequest( ) )
         {
            enableJsOutput();
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.GetCookie( "GX_SESSION_ID"))) )
         {
            gxcookieaux = context.SetCookie( "GX_SESSION_ID", Encrypt64( Crypto.GetEncryptionKey( ), Crypto.GetServerKey( )), "", (DateTime)(DateTime.MinValue), "", 0);
         }
         GXKey = Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
         toggleJsOutput = isJsOutputEnabled( );
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         init_web_controls( ) ;
         if ( toggleJsOutput )
         {
            if ( context.isSpaRequest( ) )
            {
               enableJsOutput();
            }
         }
         if ( ! context.isSpaRequest( ) )
         {
            Form.Meta.addItem("generator", "GeneXus C# 16_0_0-127771", 0) ;
            Form.Meta.addItem("description", "FYActividad Plan Cant Part", 0) ;
         }
         context.wjLoc = "";
         context.nUserReturn = 0;
         context.wbHandled = 0;
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
         }
         GX_FocusControl = edtFYActividadPlanCantPartid_Internalname;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         wbErr = false;
         context.SetDefaultTheme("Carmine");
         if ( ! context.IsLocalStorageSupported( ) )
         {
            context.PushCurrentUrl();
         }
      }

      public fyactividadplancantpart( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsDataStore1 = context.GetDataStore("DataStore1");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("Carmine");
      }

      public fyactividadplancantpart( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsDataStore1 = context.GetDataStore("DataStore1");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( )
      {
         executePrivate();
      }

      void executePrivate( )
      {
         isStatic = false;
         webExecute();
      }

      protected override void createObjects( )
      {
      }

      public override void webExecute( )
      {
         if ( initialized == 0 )
         {
            createObjects();
            initialize();
         }
         INITENV( ) ;
         INITTRN( ) ;
         if ( ( GxWebError == 0 ) && ! isAjaxCallMode( ) )
         {
            MasterPageObj = (GXMasterPage) ClassLoader.GetInstance("rwdmasterpage", "GeneXus.Programs.rwdmasterpage", new Object[] {new GxContext( context.handle, context.DataStores, context.HttpContext)});
            MasterPageObj.setDataArea(this,false);
            ValidateSpaRequest();
            MasterPageObj.webExecute();
            if ( ( GxWebError == 0 ) && context.isAjaxRequest( ) )
            {
               enableOutput();
               if ( ! context.isAjaxRequest( ) )
               {
                  context.GX_webresponse.AppendHeader("Cache-Control", "no-store");
               }
               if ( ! context.WillRedirect( ) )
               {
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               }
               else
               {
                  if ( context.isAjaxRequest( ) )
                  {
                     disableOutput();
                  }
                  RenderHtmlHeaders( ) ;
                  context.Redirect( context.wjLoc );
                  context.DispatchAjaxCommands();
               }
            }
         }
         this.cleanup();
      }

      protected void fix_multi_value_controls( )
      {
      }

      protected void Draw( )
      {
         if ( context.isAjaxRequest( ) )
         {
            disableOutput();
         }
         if ( ! GxWebStd.gx_redirect( context) )
         {
            disable_std_buttons( ) ;
            enableDisable( ) ;
            set_caption( ) ;
            /* Form start */
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "Section", "left", "top", " "+"data-gx-base-lib=\"bootstrapv3\""+" "+"data-abstract-form"+" ", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, divMaintable_Internalname, 1, 0, "px", 0, "px", "WWAdvancedContainer", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12 col-sm-8 col-sm-offset-2", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, divTitlecontainer_Internalname, 1, 0, "px", 0, "px", "TableTop", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12", "left", "top", "", "", "div");
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTitle_Internalname, "FYActividad Plan Cant Part", "", "", lblTitle_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "Title", 0, "", 1, 1, 0, "HLP_FYActividadPlanCantPart.htm");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12", "left", "top", "", "", "div");
            ClassString = "ErrorViewer";
            StyleString = "";
            GxWebStd.gx_msg_list( context, "", context.GX_msglist.DisplayMode, StyleString, ClassString, "", "false");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12 col-sm-8 col-sm-offset-2", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, divFormcontainer_Internalname, 1, 0, "px", 0, "px", "FormContainer", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, divToolbarcell_Internalname, 1, 0, "px", 0, "px", "col-xs-12 col-sm-9 col-sm-offset-3 ToolbarCellClass", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "gx-action-group ActionGroup", "left", "top", " "+"data-gx-actiongroup-type=\"toolbar\""+" ", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "btn-group", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "gx-button", "left", "top", "", "", "div");
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 21,'',false,'',0)\"";
            ClassString = "BtnFirst";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtn_first_Internalname, "", "", bttBtn_first_Jsonclick, 5, "", "", StyleString, ClassString, bttBtn_first_Visible, 1, "standard", "'"+""+"'"+",false,"+"'"+"EFIRST."+"'", TempTags, "", context.GetButtonType( ), "HLP_FYActividadPlanCantPart.htm");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "gx-button", "left", "top", "", "", "div");
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 23,'',false,'',0)\"";
            ClassString = "BtnPrevious";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtn_previous_Internalname, "", "", bttBtn_previous_Jsonclick, 5, "", "", StyleString, ClassString, bttBtn_previous_Visible, 1, "standard", "'"+""+"'"+",false,"+"'"+"EPREVIOUS."+"'", TempTags, "", context.GetButtonType( ), "HLP_FYActividadPlanCantPart.htm");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "gx-button", "left", "top", "", "", "div");
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 25,'',false,'',0)\"";
            ClassString = "BtnNext";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtn_next_Internalname, "", "", bttBtn_next_Jsonclick, 5, "", "", StyleString, ClassString, bttBtn_next_Visible, 1, "standard", "'"+""+"'"+",false,"+"'"+"ENEXT."+"'", TempTags, "", context.GetButtonType( ), "HLP_FYActividadPlanCantPart.htm");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "gx-button", "left", "top", "", "", "div");
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 27,'',false,'',0)\"";
            ClassString = "BtnLast";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtn_last_Internalname, "", "", bttBtn_last_Jsonclick, 5, "", "", StyleString, ClassString, bttBtn_last_Visible, 1, "standard", "'"+""+"'"+",false,"+"'"+"ELAST."+"'", TempTags, "", context.GetButtonType( ), "HLP_FYActividadPlanCantPart.htm");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "gx-button", "left", "top", "", "", "div");
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 29,'',false,'',0)\"";
            ClassString = "BtnSelect";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtn_select_Internalname, "", "Select", bttBtn_select_Jsonclick, 4, "Select", "", StyleString, ClassString, bttBtn_select_Visible, 1, "standard", "'"+""+"'"+",false,"+"'"+"ESELECT."+"'", TempTags, "gx.popup.openPrompt('"+"gx00i0.aspx"+"',["+"{Ctrl:gx.dom.el('"+"FYACTIVIDADPLANCANTPARTID"+"'), id:'"+"FYACTIVIDADPLANCANTPARTID"+"'"+",IOType:'out',isKey:true,isLastKey:true}"+"],"+"null"+","+"'', false"+","+"true"+");"+"return false;", 2, "HLP_FYActividadPlanCantPart.htm");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12 FormCellAdvanced", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "form-group gx-form-group", "left", "top", ""+" data-gx-for=\""+edtFYActividadPlanCantPartid_Internalname+"\"", "", "div");
            /* Attribute/Variable Label */
            GxWebStd.gx_label_element( context, edtFYActividadPlanCantPartid_Internalname, "id", "col-sm-3 AttributeLabel", 1, true);
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-sm-9 gx-attribute", "left", "top", "", "", "div");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 34,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtFYActividadPlanCantPartid_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A14FYActividadPlanCantPartid), 9, 0, ".", "")), ((edtFYActividadPlanCantPartid_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( (decimal)(A14FYActividadPlanCantPartid), "ZZZZZZZZ9")) : context.localUtil.Format( (decimal)(A14FYActividadPlanCantPartid), "ZZZZZZZZ9")), TempTags+" onchange=\""+"gx.num.valid_integer( this,',');"+";gx.evt.onchange(this, event)\" "+" onblur=\""+";gx.evt.onblur(this,34);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtFYActividadPlanCantPartid_Jsonclick, 0, "Attribute", "", "", "", "", 1, edtFYActividadPlanCantPartid_Enabled, 0, "number", "1", 9, "chr", 1, "row", 9, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_FYActividadPlanCantPart.htm");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12 FormCell", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "form-group gx-form-group", "left", "top", ""+" data-gx-for=\""+edtActividadParaPlanid_Internalname+"\"", "", "div");
            /* Attribute/Variable Label */
            GxWebStd.gx_label_element( context, edtActividadParaPlanid_Internalname, "Actividad Para Planid", "col-sm-3 AttributeLabel", 1, true);
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-sm-9 gx-attribute", "left", "top", "", "", "div");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 39,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtActividadParaPlanid_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A23ActividadParaPlanid), 9, 0, ".", "")), ((edtActividadParaPlanid_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( (decimal)(A23ActividadParaPlanid), "ZZZZZZZZ9")) : context.localUtil.Format( (decimal)(A23ActividadParaPlanid), "ZZZZZZZZ9")), TempTags+" onchange=\""+"gx.num.valid_integer( this,',');"+";gx.evt.onchange(this, event)\" "+" onblur=\""+";gx.evt.onblur(this,39);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtActividadParaPlanid_Jsonclick, 0, "Attribute", "", "", "", "", 1, edtActividadParaPlanid_Enabled, 0, "number", "1", 9, "chr", 1, "row", 9, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_FYActividadPlanCantPart.htm");
            /* Static images/pictures */
            ClassString = "gx-prompt Image";
            StyleString = "";
            sImgUrl = (String)(context.GetImagePath( "prompt.gif", "", context.GetTheme( )));
            GxWebStd.gx_bitmap( context, imgprompt_23_Internalname, sImgUrl, imgprompt_23_Link, "", "", context.GetTheme( ), imgprompt_23_Visible, 1, "", "", 0, 0, 0, "", 0, "", 0, 0, 0, "", "", StyleString, ClassString, "", "", "", "", "", "", "", 1, false, false, context.GetImageSrcSet( sImgUrl), "HLP_FYActividadPlanCantPart.htm");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12 FormCell", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "form-group gx-form-group", "left", "top", ""+" data-gx-for=\""+edtFYActividadPlanCantPartFY_Internalname+"\"", "", "div");
            /* Attribute/Variable Label */
            GxWebStd.gx_label_element( context, edtFYActividadPlanCantPartFY_Internalname, "FY", "col-sm-3 AttributeLabel", 1, true);
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-sm-9 gx-attribute", "left", "top", "", "", "div");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 44,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtFYActividadPlanCantPartFY_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A79FYActividadPlanCantPartFY), 9, 0, ".", "")), ((edtFYActividadPlanCantPartFY_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( (decimal)(A79FYActividadPlanCantPartFY), "ZZZZZZZZ9")) : context.localUtil.Format( (decimal)(A79FYActividadPlanCantPartFY), "ZZZZZZZZ9")), TempTags+" onchange=\""+"gx.num.valid_integer( this,',');"+";gx.evt.onchange(this, event)\" "+" onblur=\""+";gx.evt.onblur(this,44);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtFYActividadPlanCantPartFY_Jsonclick, 0, "Attribute", "", "", "", "", 1, edtFYActividadPlanCantPartFY_Enabled, 0, "number", "1", 9, "chr", 1, "row", 9, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_FYActividadPlanCantPart.htm");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12 FormCell", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "form-group gx-form-group", "left", "top", ""+" data-gx-for=\""+edtmes1H_Internalname+"\"", "", "div");
            /* Attribute/Variable Label */
            GxWebStd.gx_label_element( context, edtmes1H_Internalname, "mes1H", "col-sm-3 AttributeLabel", 1, true);
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-sm-9 gx-attribute", "left", "top", "", "", "div");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 49,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtmes1H_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A80mes1H), 9, 0, ".", "")), ((edtmes1H_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( (decimal)(A80mes1H), "ZZZZZZZZ9")) : context.localUtil.Format( (decimal)(A80mes1H), "ZZZZZZZZ9")), TempTags+" onchange=\""+"gx.num.valid_integer( this,',');"+";gx.evt.onchange(this, event)\" "+" onblur=\""+";gx.evt.onblur(this,49);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtmes1H_Jsonclick, 0, "Attribute", "", "", "", "", 1, edtmes1H_Enabled, 0, "number", "1", 9, "chr", 1, "row", 9, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_FYActividadPlanCantPart.htm");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12 FormCell", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "form-group gx-form-group", "left", "top", ""+" data-gx-for=\""+edtmes1M_Internalname+"\"", "", "div");
            /* Attribute/Variable Label */
            GxWebStd.gx_label_element( context, edtmes1M_Internalname, "mes1M", "col-sm-3 AttributeLabel", 1, true);
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-sm-9 gx-attribute", "left", "top", "", "", "div");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 54,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtmes1M_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A81mes1M), 9, 0, ".", "")), ((edtmes1M_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( (decimal)(A81mes1M), "ZZZZZZZZ9")) : context.localUtil.Format( (decimal)(A81mes1M), "ZZZZZZZZ9")), TempTags+" onchange=\""+"gx.num.valid_integer( this,',');"+";gx.evt.onchange(this, event)\" "+" onblur=\""+";gx.evt.onblur(this,54);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtmes1M_Jsonclick, 0, "Attribute", "", "", "", "", 1, edtmes1M_Enabled, 0, "number", "1", 9, "chr", 1, "row", 9, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_FYActividadPlanCantPart.htm");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12 FormCell", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "form-group gx-form-group", "left", "top", ""+" data-gx-for=\""+edtmes2H_Internalname+"\"", "", "div");
            /* Attribute/Variable Label */
            GxWebStd.gx_label_element( context, edtmes2H_Internalname, "mes2H", "col-sm-3 AttributeLabel", 1, true);
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-sm-9 gx-attribute", "left", "top", "", "", "div");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 59,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtmes2H_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A82mes2H), 9, 0, ".", "")), ((edtmes2H_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( (decimal)(A82mes2H), "ZZZZZZZZ9")) : context.localUtil.Format( (decimal)(A82mes2H), "ZZZZZZZZ9")), TempTags+" onchange=\""+"gx.num.valid_integer( this,',');"+";gx.evt.onchange(this, event)\" "+" onblur=\""+";gx.evt.onblur(this,59);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtmes2H_Jsonclick, 0, "Attribute", "", "", "", "", 1, edtmes2H_Enabled, 0, "number", "1", 9, "chr", 1, "row", 9, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_FYActividadPlanCantPart.htm");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12 FormCell", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "form-group gx-form-group", "left", "top", ""+" data-gx-for=\""+edtmes2M_Internalname+"\"", "", "div");
            /* Attribute/Variable Label */
            GxWebStd.gx_label_element( context, edtmes2M_Internalname, "mes2M", "col-sm-3 AttributeLabel", 1, true);
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-sm-9 gx-attribute", "left", "top", "", "", "div");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 64,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtmes2M_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A83mes2M), 9, 0, ".", "")), ((edtmes2M_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( (decimal)(A83mes2M), "ZZZZZZZZ9")) : context.localUtil.Format( (decimal)(A83mes2M), "ZZZZZZZZ9")), TempTags+" onchange=\""+"gx.num.valid_integer( this,',');"+";gx.evt.onchange(this, event)\" "+" onblur=\""+";gx.evt.onblur(this,64);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtmes2M_Jsonclick, 0, "Attribute", "", "", "", "", 1, edtmes2M_Enabled, 0, "number", "1", 9, "chr", 1, "row", 9, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_FYActividadPlanCantPart.htm");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12 FormCell", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "form-group gx-form-group", "left", "top", ""+" data-gx-for=\""+edtmes3H_Internalname+"\"", "", "div");
            /* Attribute/Variable Label */
            GxWebStd.gx_label_element( context, edtmes3H_Internalname, "mes3H", "col-sm-3 AttributeLabel", 1, true);
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-sm-9 gx-attribute", "left", "top", "", "", "div");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 69,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtmes3H_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A84mes3H), 9, 0, ".", "")), ((edtmes3H_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( (decimal)(A84mes3H), "ZZZZZZZZ9")) : context.localUtil.Format( (decimal)(A84mes3H), "ZZZZZZZZ9")), TempTags+" onchange=\""+"gx.num.valid_integer( this,',');"+";gx.evt.onchange(this, event)\" "+" onblur=\""+";gx.evt.onblur(this,69);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtmes3H_Jsonclick, 0, "Attribute", "", "", "", "", 1, edtmes3H_Enabled, 0, "number", "1", 9, "chr", 1, "row", 9, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_FYActividadPlanCantPart.htm");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12 FormCell", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "form-group gx-form-group", "left", "top", ""+" data-gx-for=\""+edtmes3M_Internalname+"\"", "", "div");
            /* Attribute/Variable Label */
            GxWebStd.gx_label_element( context, edtmes3M_Internalname, "mes3M", "col-sm-3 AttributeLabel", 1, true);
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-sm-9 gx-attribute", "left", "top", "", "", "div");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 74,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtmes3M_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A85mes3M), 9, 0, ".", "")), ((edtmes3M_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( (decimal)(A85mes3M), "ZZZZZZZZ9")) : context.localUtil.Format( (decimal)(A85mes3M), "ZZZZZZZZ9")), TempTags+" onchange=\""+"gx.num.valid_integer( this,',');"+";gx.evt.onchange(this, event)\" "+" onblur=\""+";gx.evt.onblur(this,74);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtmes3M_Jsonclick, 0, "Attribute", "", "", "", "", 1, edtmes3M_Enabled, 0, "number", "1", 9, "chr", 1, "row", 9, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_FYActividadPlanCantPart.htm");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12 FormCell", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "form-group gx-form-group", "left", "top", ""+" data-gx-for=\""+edtmes4H_Internalname+"\"", "", "div");
            /* Attribute/Variable Label */
            GxWebStd.gx_label_element( context, edtmes4H_Internalname, "mes4H", "col-sm-3 AttributeLabel", 1, true);
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-sm-9 gx-attribute", "left", "top", "", "", "div");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 79,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtmes4H_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A86mes4H), 9, 0, ".", "")), ((edtmes4H_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( (decimal)(A86mes4H), "ZZZZZZZZ9")) : context.localUtil.Format( (decimal)(A86mes4H), "ZZZZZZZZ9")), TempTags+" onchange=\""+"gx.num.valid_integer( this,',');"+";gx.evt.onchange(this, event)\" "+" onblur=\""+";gx.evt.onblur(this,79);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtmes4H_Jsonclick, 0, "Attribute", "", "", "", "", 1, edtmes4H_Enabled, 0, "number", "1", 9, "chr", 1, "row", 9, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_FYActividadPlanCantPart.htm");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12 FormCell", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "form-group gx-form-group", "left", "top", ""+" data-gx-for=\""+edtmes4M_Internalname+"\"", "", "div");
            /* Attribute/Variable Label */
            GxWebStd.gx_label_element( context, edtmes4M_Internalname, "mes4M", "col-sm-3 AttributeLabel", 1, true);
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-sm-9 gx-attribute", "left", "top", "", "", "div");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 84,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtmes4M_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A87mes4M), 9, 0, ".", "")), ((edtmes4M_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( (decimal)(A87mes4M), "ZZZZZZZZ9")) : context.localUtil.Format( (decimal)(A87mes4M), "ZZZZZZZZ9")), TempTags+" onchange=\""+"gx.num.valid_integer( this,',');"+";gx.evt.onchange(this, event)\" "+" onblur=\""+";gx.evt.onblur(this,84);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtmes4M_Jsonclick, 0, "Attribute", "", "", "", "", 1, edtmes4M_Enabled, 0, "number", "1", 9, "chr", 1, "row", 9, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_FYActividadPlanCantPart.htm");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12 FormCell", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "form-group gx-form-group", "left", "top", ""+" data-gx-for=\""+edtmes5H_Internalname+"\"", "", "div");
            /* Attribute/Variable Label */
            GxWebStd.gx_label_element( context, edtmes5H_Internalname, "mes5H", "col-sm-3 AttributeLabel", 1, true);
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-sm-9 gx-attribute", "left", "top", "", "", "div");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 89,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtmes5H_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A88mes5H), 9, 0, ".", "")), ((edtmes5H_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( (decimal)(A88mes5H), "ZZZZZZZZ9")) : context.localUtil.Format( (decimal)(A88mes5H), "ZZZZZZZZ9")), TempTags+" onchange=\""+"gx.num.valid_integer( this,',');"+";gx.evt.onchange(this, event)\" "+" onblur=\""+";gx.evt.onblur(this,89);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtmes5H_Jsonclick, 0, "Attribute", "", "", "", "", 1, edtmes5H_Enabled, 0, "number", "1", 9, "chr", 1, "row", 9, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_FYActividadPlanCantPart.htm");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12 FormCell", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "form-group gx-form-group", "left", "top", ""+" data-gx-for=\""+edtmes5M_Internalname+"\"", "", "div");
            /* Attribute/Variable Label */
            GxWebStd.gx_label_element( context, edtmes5M_Internalname, "mes5M", "col-sm-3 AttributeLabel", 1, true);
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-sm-9 gx-attribute", "left", "top", "", "", "div");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 94,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtmes5M_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A89mes5M), 9, 0, ".", "")), ((edtmes5M_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( (decimal)(A89mes5M), "ZZZZZZZZ9")) : context.localUtil.Format( (decimal)(A89mes5M), "ZZZZZZZZ9")), TempTags+" onchange=\""+"gx.num.valid_integer( this,',');"+";gx.evt.onchange(this, event)\" "+" onblur=\""+";gx.evt.onblur(this,94);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtmes5M_Jsonclick, 0, "Attribute", "", "", "", "", 1, edtmes5M_Enabled, 0, "number", "1", 9, "chr", 1, "row", 9, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_FYActividadPlanCantPart.htm");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12 FormCell", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "form-group gx-form-group", "left", "top", ""+" data-gx-for=\""+edtmes6H_Internalname+"\"", "", "div");
            /* Attribute/Variable Label */
            GxWebStd.gx_label_element( context, edtmes6H_Internalname, "mes6H", "col-sm-3 AttributeLabel", 1, true);
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-sm-9 gx-attribute", "left", "top", "", "", "div");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 99,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtmes6H_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A90mes6H), 9, 0, ".", "")), ((edtmes6H_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( (decimal)(A90mes6H), "ZZZZZZZZ9")) : context.localUtil.Format( (decimal)(A90mes6H), "ZZZZZZZZ9")), TempTags+" onchange=\""+"gx.num.valid_integer( this,',');"+";gx.evt.onchange(this, event)\" "+" onblur=\""+";gx.evt.onblur(this,99);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtmes6H_Jsonclick, 0, "Attribute", "", "", "", "", 1, edtmes6H_Enabled, 0, "number", "1", 9, "chr", 1, "row", 9, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_FYActividadPlanCantPart.htm");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12 FormCell", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "form-group gx-form-group", "left", "top", ""+" data-gx-for=\""+edtmes6M_Internalname+"\"", "", "div");
            /* Attribute/Variable Label */
            GxWebStd.gx_label_element( context, edtmes6M_Internalname, "mes6M", "col-sm-3 AttributeLabel", 1, true);
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-sm-9 gx-attribute", "left", "top", "", "", "div");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 104,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtmes6M_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A91mes6M), 9, 0, ".", "")), ((edtmes6M_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( (decimal)(A91mes6M), "ZZZZZZZZ9")) : context.localUtil.Format( (decimal)(A91mes6M), "ZZZZZZZZ9")), TempTags+" onchange=\""+"gx.num.valid_integer( this,',');"+";gx.evt.onchange(this, event)\" "+" onblur=\""+";gx.evt.onblur(this,104);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtmes6M_Jsonclick, 0, "Attribute", "", "", "", "", 1, edtmes6M_Enabled, 0, "number", "1", 9, "chr", 1, "row", 9, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_FYActividadPlanCantPart.htm");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12 FormCell", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "form-group gx-form-group", "left", "top", ""+" data-gx-for=\""+edtmes7H_Internalname+"\"", "", "div");
            /* Attribute/Variable Label */
            GxWebStd.gx_label_element( context, edtmes7H_Internalname, "mes7H", "col-sm-3 AttributeLabel", 1, true);
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-sm-9 gx-attribute", "left", "top", "", "", "div");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 109,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtmes7H_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A92mes7H), 9, 0, ".", "")), ((edtmes7H_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( (decimal)(A92mes7H), "ZZZZZZZZ9")) : context.localUtil.Format( (decimal)(A92mes7H), "ZZZZZZZZ9")), TempTags+" onchange=\""+"gx.num.valid_integer( this,',');"+";gx.evt.onchange(this, event)\" "+" onblur=\""+";gx.evt.onblur(this,109);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtmes7H_Jsonclick, 0, "Attribute", "", "", "", "", 1, edtmes7H_Enabled, 0, "number", "1", 9, "chr", 1, "row", 9, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_FYActividadPlanCantPart.htm");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12 FormCell", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "form-group gx-form-group", "left", "top", ""+" data-gx-for=\""+edtmes7M_Internalname+"\"", "", "div");
            /* Attribute/Variable Label */
            GxWebStd.gx_label_element( context, edtmes7M_Internalname, "mes7M", "col-sm-3 AttributeLabel", 1, true);
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-sm-9 gx-attribute", "left", "top", "", "", "div");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 114,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtmes7M_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A93mes7M), 9, 0, ".", "")), ((edtmes7M_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( (decimal)(A93mes7M), "ZZZZZZZZ9")) : context.localUtil.Format( (decimal)(A93mes7M), "ZZZZZZZZ9")), TempTags+" onchange=\""+"gx.num.valid_integer( this,',');"+";gx.evt.onchange(this, event)\" "+" onblur=\""+";gx.evt.onblur(this,114);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtmes7M_Jsonclick, 0, "Attribute", "", "", "", "", 1, edtmes7M_Enabled, 0, "number", "1", 9, "chr", 1, "row", 9, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_FYActividadPlanCantPart.htm");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12 FormCell", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "form-group gx-form-group", "left", "top", ""+" data-gx-for=\""+edtmes8H_Internalname+"\"", "", "div");
            /* Attribute/Variable Label */
            GxWebStd.gx_label_element( context, edtmes8H_Internalname, "mes8H", "col-sm-3 AttributeLabel", 1, true);
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-sm-9 gx-attribute", "left", "top", "", "", "div");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 119,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtmes8H_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A94mes8H), 9, 0, ".", "")), ((edtmes8H_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( (decimal)(A94mes8H), "ZZZZZZZZ9")) : context.localUtil.Format( (decimal)(A94mes8H), "ZZZZZZZZ9")), TempTags+" onchange=\""+"gx.num.valid_integer( this,',');"+";gx.evt.onchange(this, event)\" "+" onblur=\""+";gx.evt.onblur(this,119);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtmes8H_Jsonclick, 0, "Attribute", "", "", "", "", 1, edtmes8H_Enabled, 0, "number", "1", 9, "chr", 1, "row", 9, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_FYActividadPlanCantPart.htm");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12 FormCell", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "form-group gx-form-group", "left", "top", ""+" data-gx-for=\""+edtmes8M_Internalname+"\"", "", "div");
            /* Attribute/Variable Label */
            GxWebStd.gx_label_element( context, edtmes8M_Internalname, "mes8M", "col-sm-3 AttributeLabel", 1, true);
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-sm-9 gx-attribute", "left", "top", "", "", "div");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 124,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtmes8M_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A95mes8M), 9, 0, ".", "")), ((edtmes8M_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( (decimal)(A95mes8M), "ZZZZZZZZ9")) : context.localUtil.Format( (decimal)(A95mes8M), "ZZZZZZZZ9")), TempTags+" onchange=\""+"gx.num.valid_integer( this,',');"+";gx.evt.onchange(this, event)\" "+" onblur=\""+";gx.evt.onblur(this,124);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtmes8M_Jsonclick, 0, "Attribute", "", "", "", "", 1, edtmes8M_Enabled, 0, "number", "1", 9, "chr", 1, "row", 9, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_FYActividadPlanCantPart.htm");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12 FormCell", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "form-group gx-form-group", "left", "top", ""+" data-gx-for=\""+edtmes9H_Internalname+"\"", "", "div");
            /* Attribute/Variable Label */
            GxWebStd.gx_label_element( context, edtmes9H_Internalname, "mes9H", "col-sm-3 AttributeLabel", 1, true);
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-sm-9 gx-attribute", "left", "top", "", "", "div");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 129,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtmes9H_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A96mes9H), 9, 0, ".", "")), ((edtmes9H_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( (decimal)(A96mes9H), "ZZZZZZZZ9")) : context.localUtil.Format( (decimal)(A96mes9H), "ZZZZZZZZ9")), TempTags+" onchange=\""+"gx.num.valid_integer( this,',');"+";gx.evt.onchange(this, event)\" "+" onblur=\""+";gx.evt.onblur(this,129);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtmes9H_Jsonclick, 0, "Attribute", "", "", "", "", 1, edtmes9H_Enabled, 0, "number", "1", 9, "chr", 1, "row", 9, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_FYActividadPlanCantPart.htm");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12 FormCell", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "form-group gx-form-group", "left", "top", ""+" data-gx-for=\""+edtmes9M_Internalname+"\"", "", "div");
            /* Attribute/Variable Label */
            GxWebStd.gx_label_element( context, edtmes9M_Internalname, "mes9M", "col-sm-3 AttributeLabel", 1, true);
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-sm-9 gx-attribute", "left", "top", "", "", "div");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 134,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtmes9M_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A97mes9M), 9, 0, ".", "")), ((edtmes9M_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( (decimal)(A97mes9M), "ZZZZZZZZ9")) : context.localUtil.Format( (decimal)(A97mes9M), "ZZZZZZZZ9")), TempTags+" onchange=\""+"gx.num.valid_integer( this,',');"+";gx.evt.onchange(this, event)\" "+" onblur=\""+";gx.evt.onblur(this,134);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtmes9M_Jsonclick, 0, "Attribute", "", "", "", "", 1, edtmes9M_Enabled, 0, "number", "1", 9, "chr", 1, "row", 9, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_FYActividadPlanCantPart.htm");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12 FormCell", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "form-group gx-form-group", "left", "top", ""+" data-gx-for=\""+edtmes10H_Internalname+"\"", "", "div");
            /* Attribute/Variable Label */
            GxWebStd.gx_label_element( context, edtmes10H_Internalname, "mes10H", "col-sm-3 AttributeLabel", 1, true);
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-sm-9 gx-attribute", "left", "top", "", "", "div");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 139,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtmes10H_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A98mes10H), 9, 0, ".", "")), ((edtmes10H_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( (decimal)(A98mes10H), "ZZZZZZZZ9")) : context.localUtil.Format( (decimal)(A98mes10H), "ZZZZZZZZ9")), TempTags+" onchange=\""+"gx.num.valid_integer( this,',');"+";gx.evt.onchange(this, event)\" "+" onblur=\""+";gx.evt.onblur(this,139);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtmes10H_Jsonclick, 0, "Attribute", "", "", "", "", 1, edtmes10H_Enabled, 0, "number", "1", 9, "chr", 1, "row", 9, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_FYActividadPlanCantPart.htm");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12 FormCell", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "form-group gx-form-group", "left", "top", ""+" data-gx-for=\""+edtmes10M_Internalname+"\"", "", "div");
            /* Attribute/Variable Label */
            GxWebStd.gx_label_element( context, edtmes10M_Internalname, "mes10M", "col-sm-3 AttributeLabel", 1, true);
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-sm-9 gx-attribute", "left", "top", "", "", "div");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 144,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtmes10M_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A99mes10M), 9, 0, ".", "")), ((edtmes10M_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( (decimal)(A99mes10M), "ZZZZZZZZ9")) : context.localUtil.Format( (decimal)(A99mes10M), "ZZZZZZZZ9")), TempTags+" onchange=\""+"gx.num.valid_integer( this,',');"+";gx.evt.onchange(this, event)\" "+" onblur=\""+";gx.evt.onblur(this,144);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtmes10M_Jsonclick, 0, "Attribute", "", "", "", "", 1, edtmes10M_Enabled, 0, "number", "1", 9, "chr", 1, "row", 9, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_FYActividadPlanCantPart.htm");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12 FormCell", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "form-group gx-form-group", "left", "top", ""+" data-gx-for=\""+edtmes11H_Internalname+"\"", "", "div");
            /* Attribute/Variable Label */
            GxWebStd.gx_label_element( context, edtmes11H_Internalname, "mes11H", "col-sm-3 AttributeLabel", 1, true);
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-sm-9 gx-attribute", "left", "top", "", "", "div");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 149,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtmes11H_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A100mes11H), 9, 0, ".", "")), ((edtmes11H_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( (decimal)(A100mes11H), "ZZZZZZZZ9")) : context.localUtil.Format( (decimal)(A100mes11H), "ZZZZZZZZ9")), TempTags+" onchange=\""+"gx.num.valid_integer( this,',');"+";gx.evt.onchange(this, event)\" "+" onblur=\""+";gx.evt.onblur(this,149);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtmes11H_Jsonclick, 0, "Attribute", "", "", "", "", 1, edtmes11H_Enabled, 0, "number", "1", 9, "chr", 1, "row", 9, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_FYActividadPlanCantPart.htm");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12 FormCell", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "form-group gx-form-group", "left", "top", ""+" data-gx-for=\""+edtmes11M_Internalname+"\"", "", "div");
            /* Attribute/Variable Label */
            GxWebStd.gx_label_element( context, edtmes11M_Internalname, "mes11M", "col-sm-3 AttributeLabel", 1, true);
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-sm-9 gx-attribute", "left", "top", "", "", "div");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 154,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtmes11M_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A101mes11M), 9, 0, ".", "")), ((edtmes11M_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( (decimal)(A101mes11M), "ZZZZZZZZ9")) : context.localUtil.Format( (decimal)(A101mes11M), "ZZZZZZZZ9")), TempTags+" onchange=\""+"gx.num.valid_integer( this,',');"+";gx.evt.onchange(this, event)\" "+" onblur=\""+";gx.evt.onblur(this,154);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtmes11M_Jsonclick, 0, "Attribute", "", "", "", "", 1, edtmes11M_Enabled, 0, "number", "1", 9, "chr", 1, "row", 9, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_FYActividadPlanCantPart.htm");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12 FormCell", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "form-group gx-form-group", "left", "top", ""+" data-gx-for=\""+edtmes12H_Internalname+"\"", "", "div");
            /* Attribute/Variable Label */
            GxWebStd.gx_label_element( context, edtmes12H_Internalname, "mes12H", "col-sm-3 AttributeLabel", 1, true);
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-sm-9 gx-attribute", "left", "top", "", "", "div");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 159,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtmes12H_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A102mes12H), 9, 0, ".", "")), ((edtmes12H_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( (decimal)(A102mes12H), "ZZZZZZZZ9")) : context.localUtil.Format( (decimal)(A102mes12H), "ZZZZZZZZ9")), TempTags+" onchange=\""+"gx.num.valid_integer( this,',');"+";gx.evt.onchange(this, event)\" "+" onblur=\""+";gx.evt.onblur(this,159);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtmes12H_Jsonclick, 0, "Attribute", "", "", "", "", 1, edtmes12H_Enabled, 0, "number", "1", 9, "chr", 1, "row", 9, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_FYActividadPlanCantPart.htm");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12 FormCell", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "form-group gx-form-group", "left", "top", ""+" data-gx-for=\""+edtmes12M_Internalname+"\"", "", "div");
            /* Attribute/Variable Label */
            GxWebStd.gx_label_element( context, edtmes12M_Internalname, "mes12M", "col-sm-3 AttributeLabel", 1, true);
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-sm-9 gx-attribute", "left", "top", "", "", "div");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 164,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtmes12M_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A103mes12M), 9, 0, ".", "")), ((edtmes12M_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( (decimal)(A103mes12M), "ZZZZZZZZ9")) : context.localUtil.Format( (decimal)(A103mes12M), "ZZZZZZZZ9")), TempTags+" onchange=\""+"gx.num.valid_integer( this,',');"+";gx.evt.onchange(this, event)\" "+" onblur=\""+";gx.evt.onblur(this,164);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtmes12M_Jsonclick, 0, "Attribute", "", "", "", "", 1, edtmes12M_Enabled, 0, "number", "1", 9, "chr", 1, "row", 9, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_FYActividadPlanCantPart.htm");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12", "Center", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "gx-action-group Confirm", "left", "top", " "+"data-gx-actiongroup-type=\"toolbar\""+" ", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "gx-button", "left", "top", "", "", "div");
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 169,'',false,'',0)\"";
            ClassString = "BtnEnter";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtn_enter_Internalname, "", "Confirm", bttBtn_enter_Jsonclick, 5, "Confirm", "", StyleString, ClassString, bttBtn_enter_Visible, bttBtn_enter_Enabled, "standard", "'"+""+"'"+",false,"+"'"+"EENTER."+"'", TempTags, "", context.GetButtonType( ), "HLP_FYActividadPlanCantPart.htm");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "gx-button", "left", "top", "", "", "div");
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 171,'',false,'',0)\"";
            ClassString = "BtnCancel";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtn_cancel_Internalname, "", "Cancel", bttBtn_cancel_Jsonclick, 1, "Cancel", "", StyleString, ClassString, bttBtn_cancel_Visible, 1, "standard", "'"+""+"'"+",false,"+"'"+"ECANCEL."+"'", TempTags, "", context.GetButtonType( ), "HLP_FYActividadPlanCantPart.htm");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "gx-button", "left", "top", "", "", "div");
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 173,'',false,'',0)\"";
            ClassString = "BtnDelete";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtn_delete_Internalname, "", "Delete", bttBtn_delete_Jsonclick, 5, "Delete", "", StyleString, ClassString, bttBtn_delete_Visible, bttBtn_delete_Enabled, "standard", "'"+""+"'"+",false,"+"'"+"EDELETE."+"'", TempTags, "", context.GetButtonType( ), "HLP_FYActividadPlanCantPart.htm");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "Center", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            fix_multi_value_controls( ) ;
         }
         /* Execute Exit event if defined. */
      }

      protected void UserMain( )
      {
         standaloneStartup( ) ;
      }

      protected void UserMainFullajax( )
      {
         INITENV( ) ;
         INITTRN( ) ;
         UserMain( ) ;
         Draw( ) ;
         SendCloseFormHiddens( ) ;
      }

      protected void standaloneStartup( )
      {
         standaloneStartupServer( ) ;
         disable_std_buttons( ) ;
         enableDisable( ) ;
         Process( ) ;
      }

      protected void standaloneStartupServer( )
      {
         /* Execute Start event if defined. */
         context.wbGlbDoneStart = 0;
         context.wbGlbDoneStart = 1;
         assign_properties_default( ) ;
         if ( AnyError == 0 )
         {
            if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
            {
               /* Read saved SDTs. */
               /* Read variables values. */
               if ( ( ( context.localUtil.CToN( cgiGet( edtFYActividadPlanCantPartid_Internalname), ".", ",") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtFYActividadPlanCantPartid_Internalname), ".", ",") > Convert.ToDecimal( 999999999 )) ) )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "FYACTIVIDADPLANCANTPARTID");
                  AnyError = 1;
                  GX_FocusControl = edtFYActividadPlanCantPartid_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  wbErr = true;
                  A14FYActividadPlanCantPartid = 0;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A14FYActividadPlanCantPartid", StringUtil.LTrim( StringUtil.Str( (decimal)(A14FYActividadPlanCantPartid), 9, 0)));
               }
               else
               {
                  A14FYActividadPlanCantPartid = (int)(context.localUtil.CToN( cgiGet( edtFYActividadPlanCantPartid_Internalname), ".", ","));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A14FYActividadPlanCantPartid", StringUtil.LTrim( StringUtil.Str( (decimal)(A14FYActividadPlanCantPartid), 9, 0)));
               }
               if ( ( ( context.localUtil.CToN( cgiGet( edtActividadParaPlanid_Internalname), ".", ",") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtActividadParaPlanid_Internalname), ".", ",") > Convert.ToDecimal( 999999999 )) ) )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "ACTIVIDADPARAPLANID");
                  AnyError = 1;
                  GX_FocusControl = edtActividadParaPlanid_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  wbErr = true;
                  A23ActividadParaPlanid = 0;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A23ActividadParaPlanid", StringUtil.LTrim( StringUtil.Str( (decimal)(A23ActividadParaPlanid), 9, 0)));
               }
               else
               {
                  A23ActividadParaPlanid = (int)(context.localUtil.CToN( cgiGet( edtActividadParaPlanid_Internalname), ".", ","));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A23ActividadParaPlanid", StringUtil.LTrim( StringUtil.Str( (decimal)(A23ActividadParaPlanid), 9, 0)));
               }
               if ( ( ( context.localUtil.CToN( cgiGet( edtFYActividadPlanCantPartFY_Internalname), ".", ",") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtFYActividadPlanCantPartFY_Internalname), ".", ",") > Convert.ToDecimal( 999999999 )) ) )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "FYACTIVIDADPLANCANTPARTFY");
                  AnyError = 1;
                  GX_FocusControl = edtFYActividadPlanCantPartFY_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  wbErr = true;
                  A79FYActividadPlanCantPartFY = 0;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A79FYActividadPlanCantPartFY", StringUtil.LTrim( StringUtil.Str( (decimal)(A79FYActividadPlanCantPartFY), 9, 0)));
               }
               else
               {
                  A79FYActividadPlanCantPartFY = (int)(context.localUtil.CToN( cgiGet( edtFYActividadPlanCantPartFY_Internalname), ".", ","));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A79FYActividadPlanCantPartFY", StringUtil.LTrim( StringUtil.Str( (decimal)(A79FYActividadPlanCantPartFY), 9, 0)));
               }
               if ( ( ( context.localUtil.CToN( cgiGet( edtmes1H_Internalname), ".", ",") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtmes1H_Internalname), ".", ",") > Convert.ToDecimal( 999999999 )) ) )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "MES1H");
                  AnyError = 1;
                  GX_FocusControl = edtmes1H_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  wbErr = true;
                  A80mes1H = 0;
                  n80mes1H = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A80mes1H", StringUtil.LTrim( StringUtil.Str( (decimal)(A80mes1H), 9, 0)));
               }
               else
               {
                  A80mes1H = (int)(context.localUtil.CToN( cgiGet( edtmes1H_Internalname), ".", ","));
                  n80mes1H = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A80mes1H", StringUtil.LTrim( StringUtil.Str( (decimal)(A80mes1H), 9, 0)));
               }
               n80mes1H = ((0==A80mes1H) ? true : false);
               if ( ( ( context.localUtil.CToN( cgiGet( edtmes1M_Internalname), ".", ",") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtmes1M_Internalname), ".", ",") > Convert.ToDecimal( 999999999 )) ) )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "MES1M");
                  AnyError = 1;
                  GX_FocusControl = edtmes1M_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  wbErr = true;
                  A81mes1M = 0;
                  n81mes1M = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A81mes1M", StringUtil.LTrim( StringUtil.Str( (decimal)(A81mes1M), 9, 0)));
               }
               else
               {
                  A81mes1M = (int)(context.localUtil.CToN( cgiGet( edtmes1M_Internalname), ".", ","));
                  n81mes1M = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A81mes1M", StringUtil.LTrim( StringUtil.Str( (decimal)(A81mes1M), 9, 0)));
               }
               n81mes1M = ((0==A81mes1M) ? true : false);
               if ( ( ( context.localUtil.CToN( cgiGet( edtmes2H_Internalname), ".", ",") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtmes2H_Internalname), ".", ",") > Convert.ToDecimal( 999999999 )) ) )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "MES2H");
                  AnyError = 1;
                  GX_FocusControl = edtmes2H_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  wbErr = true;
                  A82mes2H = 0;
                  n82mes2H = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A82mes2H", StringUtil.LTrim( StringUtil.Str( (decimal)(A82mes2H), 9, 0)));
               }
               else
               {
                  A82mes2H = (int)(context.localUtil.CToN( cgiGet( edtmes2H_Internalname), ".", ","));
                  n82mes2H = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A82mes2H", StringUtil.LTrim( StringUtil.Str( (decimal)(A82mes2H), 9, 0)));
               }
               n82mes2H = ((0==A82mes2H) ? true : false);
               if ( ( ( context.localUtil.CToN( cgiGet( edtmes2M_Internalname), ".", ",") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtmes2M_Internalname), ".", ",") > Convert.ToDecimal( 999999999 )) ) )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "MES2M");
                  AnyError = 1;
                  GX_FocusControl = edtmes2M_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  wbErr = true;
                  A83mes2M = 0;
                  n83mes2M = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A83mes2M", StringUtil.LTrim( StringUtil.Str( (decimal)(A83mes2M), 9, 0)));
               }
               else
               {
                  A83mes2M = (int)(context.localUtil.CToN( cgiGet( edtmes2M_Internalname), ".", ","));
                  n83mes2M = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A83mes2M", StringUtil.LTrim( StringUtil.Str( (decimal)(A83mes2M), 9, 0)));
               }
               n83mes2M = ((0==A83mes2M) ? true : false);
               if ( ( ( context.localUtil.CToN( cgiGet( edtmes3H_Internalname), ".", ",") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtmes3H_Internalname), ".", ",") > Convert.ToDecimal( 999999999 )) ) )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "MES3H");
                  AnyError = 1;
                  GX_FocusControl = edtmes3H_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  wbErr = true;
                  A84mes3H = 0;
                  n84mes3H = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A84mes3H", StringUtil.LTrim( StringUtil.Str( (decimal)(A84mes3H), 9, 0)));
               }
               else
               {
                  A84mes3H = (int)(context.localUtil.CToN( cgiGet( edtmes3H_Internalname), ".", ","));
                  n84mes3H = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A84mes3H", StringUtil.LTrim( StringUtil.Str( (decimal)(A84mes3H), 9, 0)));
               }
               n84mes3H = ((0==A84mes3H) ? true : false);
               if ( ( ( context.localUtil.CToN( cgiGet( edtmes3M_Internalname), ".", ",") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtmes3M_Internalname), ".", ",") > Convert.ToDecimal( 999999999 )) ) )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "MES3M");
                  AnyError = 1;
                  GX_FocusControl = edtmes3M_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  wbErr = true;
                  A85mes3M = 0;
                  n85mes3M = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A85mes3M", StringUtil.LTrim( StringUtil.Str( (decimal)(A85mes3M), 9, 0)));
               }
               else
               {
                  A85mes3M = (int)(context.localUtil.CToN( cgiGet( edtmes3M_Internalname), ".", ","));
                  n85mes3M = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A85mes3M", StringUtil.LTrim( StringUtil.Str( (decimal)(A85mes3M), 9, 0)));
               }
               n85mes3M = ((0==A85mes3M) ? true : false);
               if ( ( ( context.localUtil.CToN( cgiGet( edtmes4H_Internalname), ".", ",") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtmes4H_Internalname), ".", ",") > Convert.ToDecimal( 999999999 )) ) )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "MES4H");
                  AnyError = 1;
                  GX_FocusControl = edtmes4H_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  wbErr = true;
                  A86mes4H = 0;
                  n86mes4H = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A86mes4H", StringUtil.LTrim( StringUtil.Str( (decimal)(A86mes4H), 9, 0)));
               }
               else
               {
                  A86mes4H = (int)(context.localUtil.CToN( cgiGet( edtmes4H_Internalname), ".", ","));
                  n86mes4H = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A86mes4H", StringUtil.LTrim( StringUtil.Str( (decimal)(A86mes4H), 9, 0)));
               }
               n86mes4H = ((0==A86mes4H) ? true : false);
               if ( ( ( context.localUtil.CToN( cgiGet( edtmes4M_Internalname), ".", ",") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtmes4M_Internalname), ".", ",") > Convert.ToDecimal( 999999999 )) ) )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "MES4M");
                  AnyError = 1;
                  GX_FocusControl = edtmes4M_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  wbErr = true;
                  A87mes4M = 0;
                  n87mes4M = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A87mes4M", StringUtil.LTrim( StringUtil.Str( (decimal)(A87mes4M), 9, 0)));
               }
               else
               {
                  A87mes4M = (int)(context.localUtil.CToN( cgiGet( edtmes4M_Internalname), ".", ","));
                  n87mes4M = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A87mes4M", StringUtil.LTrim( StringUtil.Str( (decimal)(A87mes4M), 9, 0)));
               }
               n87mes4M = ((0==A87mes4M) ? true : false);
               if ( ( ( context.localUtil.CToN( cgiGet( edtmes5H_Internalname), ".", ",") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtmes5H_Internalname), ".", ",") > Convert.ToDecimal( 999999999 )) ) )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "MES5H");
                  AnyError = 1;
                  GX_FocusControl = edtmes5H_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  wbErr = true;
                  A88mes5H = 0;
                  n88mes5H = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A88mes5H", StringUtil.LTrim( StringUtil.Str( (decimal)(A88mes5H), 9, 0)));
               }
               else
               {
                  A88mes5H = (int)(context.localUtil.CToN( cgiGet( edtmes5H_Internalname), ".", ","));
                  n88mes5H = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A88mes5H", StringUtil.LTrim( StringUtil.Str( (decimal)(A88mes5H), 9, 0)));
               }
               n88mes5H = ((0==A88mes5H) ? true : false);
               if ( ( ( context.localUtil.CToN( cgiGet( edtmes5M_Internalname), ".", ",") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtmes5M_Internalname), ".", ",") > Convert.ToDecimal( 999999999 )) ) )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "MES5M");
                  AnyError = 1;
                  GX_FocusControl = edtmes5M_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  wbErr = true;
                  A89mes5M = 0;
                  n89mes5M = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A89mes5M", StringUtil.LTrim( StringUtil.Str( (decimal)(A89mes5M), 9, 0)));
               }
               else
               {
                  A89mes5M = (int)(context.localUtil.CToN( cgiGet( edtmes5M_Internalname), ".", ","));
                  n89mes5M = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A89mes5M", StringUtil.LTrim( StringUtil.Str( (decimal)(A89mes5M), 9, 0)));
               }
               n89mes5M = ((0==A89mes5M) ? true : false);
               if ( ( ( context.localUtil.CToN( cgiGet( edtmes6H_Internalname), ".", ",") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtmes6H_Internalname), ".", ",") > Convert.ToDecimal( 999999999 )) ) )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "MES6H");
                  AnyError = 1;
                  GX_FocusControl = edtmes6H_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  wbErr = true;
                  A90mes6H = 0;
                  n90mes6H = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A90mes6H", StringUtil.LTrim( StringUtil.Str( (decimal)(A90mes6H), 9, 0)));
               }
               else
               {
                  A90mes6H = (int)(context.localUtil.CToN( cgiGet( edtmes6H_Internalname), ".", ","));
                  n90mes6H = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A90mes6H", StringUtil.LTrim( StringUtil.Str( (decimal)(A90mes6H), 9, 0)));
               }
               n90mes6H = ((0==A90mes6H) ? true : false);
               if ( ( ( context.localUtil.CToN( cgiGet( edtmes6M_Internalname), ".", ",") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtmes6M_Internalname), ".", ",") > Convert.ToDecimal( 999999999 )) ) )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "MES6M");
                  AnyError = 1;
                  GX_FocusControl = edtmes6M_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  wbErr = true;
                  A91mes6M = 0;
                  n91mes6M = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A91mes6M", StringUtil.LTrim( StringUtil.Str( (decimal)(A91mes6M), 9, 0)));
               }
               else
               {
                  A91mes6M = (int)(context.localUtil.CToN( cgiGet( edtmes6M_Internalname), ".", ","));
                  n91mes6M = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A91mes6M", StringUtil.LTrim( StringUtil.Str( (decimal)(A91mes6M), 9, 0)));
               }
               n91mes6M = ((0==A91mes6M) ? true : false);
               if ( ( ( context.localUtil.CToN( cgiGet( edtmes7H_Internalname), ".", ",") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtmes7H_Internalname), ".", ",") > Convert.ToDecimal( 999999999 )) ) )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "MES7H");
                  AnyError = 1;
                  GX_FocusControl = edtmes7H_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  wbErr = true;
                  A92mes7H = 0;
                  n92mes7H = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A92mes7H", StringUtil.LTrim( StringUtil.Str( (decimal)(A92mes7H), 9, 0)));
               }
               else
               {
                  A92mes7H = (int)(context.localUtil.CToN( cgiGet( edtmes7H_Internalname), ".", ","));
                  n92mes7H = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A92mes7H", StringUtil.LTrim( StringUtil.Str( (decimal)(A92mes7H), 9, 0)));
               }
               n92mes7H = ((0==A92mes7H) ? true : false);
               if ( ( ( context.localUtil.CToN( cgiGet( edtmes7M_Internalname), ".", ",") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtmes7M_Internalname), ".", ",") > Convert.ToDecimal( 999999999 )) ) )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "MES7M");
                  AnyError = 1;
                  GX_FocusControl = edtmes7M_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  wbErr = true;
                  A93mes7M = 0;
                  n93mes7M = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A93mes7M", StringUtil.LTrim( StringUtil.Str( (decimal)(A93mes7M), 9, 0)));
               }
               else
               {
                  A93mes7M = (int)(context.localUtil.CToN( cgiGet( edtmes7M_Internalname), ".", ","));
                  n93mes7M = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A93mes7M", StringUtil.LTrim( StringUtil.Str( (decimal)(A93mes7M), 9, 0)));
               }
               n93mes7M = ((0==A93mes7M) ? true : false);
               if ( ( ( context.localUtil.CToN( cgiGet( edtmes8H_Internalname), ".", ",") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtmes8H_Internalname), ".", ",") > Convert.ToDecimal( 999999999 )) ) )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "MES8H");
                  AnyError = 1;
                  GX_FocusControl = edtmes8H_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  wbErr = true;
                  A94mes8H = 0;
                  n94mes8H = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A94mes8H", StringUtil.LTrim( StringUtil.Str( (decimal)(A94mes8H), 9, 0)));
               }
               else
               {
                  A94mes8H = (int)(context.localUtil.CToN( cgiGet( edtmes8H_Internalname), ".", ","));
                  n94mes8H = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A94mes8H", StringUtil.LTrim( StringUtil.Str( (decimal)(A94mes8H), 9, 0)));
               }
               n94mes8H = ((0==A94mes8H) ? true : false);
               if ( ( ( context.localUtil.CToN( cgiGet( edtmes8M_Internalname), ".", ",") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtmes8M_Internalname), ".", ",") > Convert.ToDecimal( 999999999 )) ) )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "MES8M");
                  AnyError = 1;
                  GX_FocusControl = edtmes8M_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  wbErr = true;
                  A95mes8M = 0;
                  n95mes8M = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A95mes8M", StringUtil.LTrim( StringUtil.Str( (decimal)(A95mes8M), 9, 0)));
               }
               else
               {
                  A95mes8M = (int)(context.localUtil.CToN( cgiGet( edtmes8M_Internalname), ".", ","));
                  n95mes8M = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A95mes8M", StringUtil.LTrim( StringUtil.Str( (decimal)(A95mes8M), 9, 0)));
               }
               n95mes8M = ((0==A95mes8M) ? true : false);
               if ( ( ( context.localUtil.CToN( cgiGet( edtmes9H_Internalname), ".", ",") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtmes9H_Internalname), ".", ",") > Convert.ToDecimal( 999999999 )) ) )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "MES9H");
                  AnyError = 1;
                  GX_FocusControl = edtmes9H_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  wbErr = true;
                  A96mes9H = 0;
                  n96mes9H = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A96mes9H", StringUtil.LTrim( StringUtil.Str( (decimal)(A96mes9H), 9, 0)));
               }
               else
               {
                  A96mes9H = (int)(context.localUtil.CToN( cgiGet( edtmes9H_Internalname), ".", ","));
                  n96mes9H = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A96mes9H", StringUtil.LTrim( StringUtil.Str( (decimal)(A96mes9H), 9, 0)));
               }
               n96mes9H = ((0==A96mes9H) ? true : false);
               if ( ( ( context.localUtil.CToN( cgiGet( edtmes9M_Internalname), ".", ",") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtmes9M_Internalname), ".", ",") > Convert.ToDecimal( 999999999 )) ) )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "MES9M");
                  AnyError = 1;
                  GX_FocusControl = edtmes9M_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  wbErr = true;
                  A97mes9M = 0;
                  n97mes9M = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A97mes9M", StringUtil.LTrim( StringUtil.Str( (decimal)(A97mes9M), 9, 0)));
               }
               else
               {
                  A97mes9M = (int)(context.localUtil.CToN( cgiGet( edtmes9M_Internalname), ".", ","));
                  n97mes9M = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A97mes9M", StringUtil.LTrim( StringUtil.Str( (decimal)(A97mes9M), 9, 0)));
               }
               n97mes9M = ((0==A97mes9M) ? true : false);
               if ( ( ( context.localUtil.CToN( cgiGet( edtmes10H_Internalname), ".", ",") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtmes10H_Internalname), ".", ",") > Convert.ToDecimal( 999999999 )) ) )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "MES10H");
                  AnyError = 1;
                  GX_FocusControl = edtmes10H_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  wbErr = true;
                  A98mes10H = 0;
                  n98mes10H = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A98mes10H", StringUtil.LTrim( StringUtil.Str( (decimal)(A98mes10H), 9, 0)));
               }
               else
               {
                  A98mes10H = (int)(context.localUtil.CToN( cgiGet( edtmes10H_Internalname), ".", ","));
                  n98mes10H = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A98mes10H", StringUtil.LTrim( StringUtil.Str( (decimal)(A98mes10H), 9, 0)));
               }
               n98mes10H = ((0==A98mes10H) ? true : false);
               if ( ( ( context.localUtil.CToN( cgiGet( edtmes10M_Internalname), ".", ",") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtmes10M_Internalname), ".", ",") > Convert.ToDecimal( 999999999 )) ) )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "MES10M");
                  AnyError = 1;
                  GX_FocusControl = edtmes10M_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  wbErr = true;
                  A99mes10M = 0;
                  n99mes10M = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A99mes10M", StringUtil.LTrim( StringUtil.Str( (decimal)(A99mes10M), 9, 0)));
               }
               else
               {
                  A99mes10M = (int)(context.localUtil.CToN( cgiGet( edtmes10M_Internalname), ".", ","));
                  n99mes10M = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A99mes10M", StringUtil.LTrim( StringUtil.Str( (decimal)(A99mes10M), 9, 0)));
               }
               n99mes10M = ((0==A99mes10M) ? true : false);
               if ( ( ( context.localUtil.CToN( cgiGet( edtmes11H_Internalname), ".", ",") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtmes11H_Internalname), ".", ",") > Convert.ToDecimal( 999999999 )) ) )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "MES11H");
                  AnyError = 1;
                  GX_FocusControl = edtmes11H_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  wbErr = true;
                  A100mes11H = 0;
                  n100mes11H = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A100mes11H", StringUtil.LTrim( StringUtil.Str( (decimal)(A100mes11H), 9, 0)));
               }
               else
               {
                  A100mes11H = (int)(context.localUtil.CToN( cgiGet( edtmes11H_Internalname), ".", ","));
                  n100mes11H = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A100mes11H", StringUtil.LTrim( StringUtil.Str( (decimal)(A100mes11H), 9, 0)));
               }
               n100mes11H = ((0==A100mes11H) ? true : false);
               if ( ( ( context.localUtil.CToN( cgiGet( edtmes11M_Internalname), ".", ",") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtmes11M_Internalname), ".", ",") > Convert.ToDecimal( 999999999 )) ) )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "MES11M");
                  AnyError = 1;
                  GX_FocusControl = edtmes11M_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  wbErr = true;
                  A101mes11M = 0;
                  n101mes11M = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A101mes11M", StringUtil.LTrim( StringUtil.Str( (decimal)(A101mes11M), 9, 0)));
               }
               else
               {
                  A101mes11M = (int)(context.localUtil.CToN( cgiGet( edtmes11M_Internalname), ".", ","));
                  n101mes11M = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A101mes11M", StringUtil.LTrim( StringUtil.Str( (decimal)(A101mes11M), 9, 0)));
               }
               n101mes11M = ((0==A101mes11M) ? true : false);
               if ( ( ( context.localUtil.CToN( cgiGet( edtmes12H_Internalname), ".", ",") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtmes12H_Internalname), ".", ",") > Convert.ToDecimal( 999999999 )) ) )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "MES12H");
                  AnyError = 1;
                  GX_FocusControl = edtmes12H_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  wbErr = true;
                  A102mes12H = 0;
                  n102mes12H = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A102mes12H", StringUtil.LTrim( StringUtil.Str( (decimal)(A102mes12H), 9, 0)));
               }
               else
               {
                  A102mes12H = (int)(context.localUtil.CToN( cgiGet( edtmes12H_Internalname), ".", ","));
                  n102mes12H = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A102mes12H", StringUtil.LTrim( StringUtil.Str( (decimal)(A102mes12H), 9, 0)));
               }
               n102mes12H = ((0==A102mes12H) ? true : false);
               if ( ( ( context.localUtil.CToN( cgiGet( edtmes12M_Internalname), ".", ",") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtmes12M_Internalname), ".", ",") > Convert.ToDecimal( 999999999 )) ) )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "MES12M");
                  AnyError = 1;
                  GX_FocusControl = edtmes12M_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  wbErr = true;
                  A103mes12M = 0;
                  n103mes12M = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A103mes12M", StringUtil.LTrim( StringUtil.Str( (decimal)(A103mes12M), 9, 0)));
               }
               else
               {
                  A103mes12M = (int)(context.localUtil.CToN( cgiGet( edtmes12M_Internalname), ".", ","));
                  n103mes12M = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A103mes12M", StringUtil.LTrim( StringUtil.Str( (decimal)(A103mes12M), 9, 0)));
               }
               n103mes12M = ((0==A103mes12M) ? true : false);
               /* Read saved values. */
               Z14FYActividadPlanCantPartid = (int)(context.localUtil.CToN( cgiGet( "Z14FYActividadPlanCantPartid"), ".", ","));
               Z79FYActividadPlanCantPartFY = (int)(context.localUtil.CToN( cgiGet( "Z79FYActividadPlanCantPartFY"), ".", ","));
               Z80mes1H = (int)(context.localUtil.CToN( cgiGet( "Z80mes1H"), ".", ","));
               n80mes1H = ((0==A80mes1H) ? true : false);
               Z81mes1M = (int)(context.localUtil.CToN( cgiGet( "Z81mes1M"), ".", ","));
               n81mes1M = ((0==A81mes1M) ? true : false);
               Z82mes2H = (int)(context.localUtil.CToN( cgiGet( "Z82mes2H"), ".", ","));
               n82mes2H = ((0==A82mes2H) ? true : false);
               Z83mes2M = (int)(context.localUtil.CToN( cgiGet( "Z83mes2M"), ".", ","));
               n83mes2M = ((0==A83mes2M) ? true : false);
               Z84mes3H = (int)(context.localUtil.CToN( cgiGet( "Z84mes3H"), ".", ","));
               n84mes3H = ((0==A84mes3H) ? true : false);
               Z85mes3M = (int)(context.localUtil.CToN( cgiGet( "Z85mes3M"), ".", ","));
               n85mes3M = ((0==A85mes3M) ? true : false);
               Z86mes4H = (int)(context.localUtil.CToN( cgiGet( "Z86mes4H"), ".", ","));
               n86mes4H = ((0==A86mes4H) ? true : false);
               Z87mes4M = (int)(context.localUtil.CToN( cgiGet( "Z87mes4M"), ".", ","));
               n87mes4M = ((0==A87mes4M) ? true : false);
               Z88mes5H = (int)(context.localUtil.CToN( cgiGet( "Z88mes5H"), ".", ","));
               n88mes5H = ((0==A88mes5H) ? true : false);
               Z89mes5M = (int)(context.localUtil.CToN( cgiGet( "Z89mes5M"), ".", ","));
               n89mes5M = ((0==A89mes5M) ? true : false);
               Z90mes6H = (int)(context.localUtil.CToN( cgiGet( "Z90mes6H"), ".", ","));
               n90mes6H = ((0==A90mes6H) ? true : false);
               Z91mes6M = (int)(context.localUtil.CToN( cgiGet( "Z91mes6M"), ".", ","));
               n91mes6M = ((0==A91mes6M) ? true : false);
               Z92mes7H = (int)(context.localUtil.CToN( cgiGet( "Z92mes7H"), ".", ","));
               n92mes7H = ((0==A92mes7H) ? true : false);
               Z93mes7M = (int)(context.localUtil.CToN( cgiGet( "Z93mes7M"), ".", ","));
               n93mes7M = ((0==A93mes7M) ? true : false);
               Z94mes8H = (int)(context.localUtil.CToN( cgiGet( "Z94mes8H"), ".", ","));
               n94mes8H = ((0==A94mes8H) ? true : false);
               Z95mes8M = (int)(context.localUtil.CToN( cgiGet( "Z95mes8M"), ".", ","));
               n95mes8M = ((0==A95mes8M) ? true : false);
               Z96mes9H = (int)(context.localUtil.CToN( cgiGet( "Z96mes9H"), ".", ","));
               n96mes9H = ((0==A96mes9H) ? true : false);
               Z97mes9M = (int)(context.localUtil.CToN( cgiGet( "Z97mes9M"), ".", ","));
               n97mes9M = ((0==A97mes9M) ? true : false);
               Z98mes10H = (int)(context.localUtil.CToN( cgiGet( "Z98mes10H"), ".", ","));
               n98mes10H = ((0==A98mes10H) ? true : false);
               Z99mes10M = (int)(context.localUtil.CToN( cgiGet( "Z99mes10M"), ".", ","));
               n99mes10M = ((0==A99mes10M) ? true : false);
               Z100mes11H = (int)(context.localUtil.CToN( cgiGet( "Z100mes11H"), ".", ","));
               n100mes11H = ((0==A100mes11H) ? true : false);
               Z101mes11M = (int)(context.localUtil.CToN( cgiGet( "Z101mes11M"), ".", ","));
               n101mes11M = ((0==A101mes11M) ? true : false);
               Z102mes12H = (int)(context.localUtil.CToN( cgiGet( "Z102mes12H"), ".", ","));
               n102mes12H = ((0==A102mes12H) ? true : false);
               Z103mes12M = (int)(context.localUtil.CToN( cgiGet( "Z103mes12M"), ".", ","));
               n103mes12M = ((0==A103mes12M) ? true : false);
               Z23ActividadParaPlanid = (int)(context.localUtil.CToN( cgiGet( "Z23ActividadParaPlanid"), ".", ","));
               IsConfirmed = (short)(context.localUtil.CToN( cgiGet( "IsConfirmed"), ".", ","));
               IsModified = (short)(context.localUtil.CToN( cgiGet( "IsModified"), ".", ","));
               Gx_mode = cgiGet( "Mode");
               Gx_mode = cgiGet( "vMODE");
               /* Read subfile selected row values. */
               /* Read hidden variables. */
               GXKey = Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
               standaloneNotModal( ) ;
            }
            else
            {
               standaloneNotModal( ) ;
               if ( StringUtil.StrCmp(gxfirstwebparm, "viewer") == 0 )
               {
                  Gx_mode = "DSP";
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                  A14FYActividadPlanCantPartid = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A14FYActividadPlanCantPartid", StringUtil.LTrim( StringUtil.Str( (decimal)(A14FYActividadPlanCantPartid), 9, 0)));
                  getEqualNoModal( ) ;
                  Gx_mode = "DSP";
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                  disable_std_buttons_dsp( ) ;
                  standaloneModal( ) ;
               }
               else
               {
                  Gx_mode = "INS";
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                  standaloneModal( ) ;
               }
            }
         }
      }

      protected void Process( )
      {
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            /* Read Transaction buttons. */
            sEvt = cgiGet( "_EventName");
            EvtGridId = cgiGet( "_EventGridId");
            EvtRowId = cgiGet( "_EventRowId");
            if ( StringUtil.Len( sEvt) > 0 )
            {
               sEvtType = StringUtil.Left( sEvt, 1);
               sEvt = StringUtil.Right( sEvt, (short)(StringUtil.Len( sEvt)-1));
               if ( StringUtil.StrCmp(sEvtType, "M") != 0 )
               {
                  if ( StringUtil.StrCmp(sEvtType, "E") == 0 )
                  {
                     sEvtType = StringUtil.Right( sEvt, 1);
                     if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                     {
                        sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                        if ( StringUtil.StrCmp(sEvt, "ENTER") == 0 )
                        {
                           context.wbHandled = 1;
                           btn_enter( ) ;
                           /* No code required for Cancel button. It is implemented as the Reset button. */
                        }
                        else if ( StringUtil.StrCmp(sEvt, "FIRST") == 0 )
                        {
                           context.wbHandled = 1;
                           btn_first( ) ;
                        }
                        else if ( StringUtil.StrCmp(sEvt, "PREVIOUS") == 0 )
                        {
                           context.wbHandled = 1;
                           btn_previous( ) ;
                        }
                        else if ( StringUtil.StrCmp(sEvt, "NEXT") == 0 )
                        {
                           context.wbHandled = 1;
                           btn_next( ) ;
                        }
                        else if ( StringUtil.StrCmp(sEvt, "LAST") == 0 )
                        {
                           context.wbHandled = 1;
                           btn_last( ) ;
                        }
                        else if ( StringUtil.StrCmp(sEvt, "SELECT") == 0 )
                        {
                           context.wbHandled = 1;
                           btn_select( ) ;
                        }
                        else if ( StringUtil.StrCmp(sEvt, "DELETE") == 0 )
                        {
                           context.wbHandled = 1;
                           btn_delete( ) ;
                        }
                        else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                        {
                           context.wbHandled = 1;
                           AfterKeyLoadScreen( ) ;
                        }
                     }
                     else
                     {
                     }
                  }
                  context.wbHandled = 1;
               }
            }
         }
      }

      protected void AfterTrn( )
      {
         if ( trnEnded == 1 )
         {
            trnEnded = 0;
            standaloneNotModal( ) ;
            standaloneModal( ) ;
            if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  )
            {
               /* Clear variables for new insertion. */
               InitAll0I18( ) ;
               standaloneNotModal( ) ;
               standaloneModal( ) ;
            }
         }
      }

      public override String ToString( )
      {
         return "" ;
      }

      public GxContentInfo GetContentInfo( )
      {
         return (GxContentInfo)(null) ;
      }

      protected void disable_std_buttons( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )
         {
            bttBtn_delete_Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_delete_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_delete_Enabled), 5, 0)), true);
         }
      }

      protected void disable_std_buttons_dsp( )
      {
         bttBtn_delete_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_delete_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_delete_Visible), 5, 0)), true);
         bttBtn_first_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_first_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_first_Visible), 5, 0)), true);
         bttBtn_previous_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_previous_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_previous_Visible), 5, 0)), true);
         bttBtn_next_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_next_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_next_Visible), 5, 0)), true);
         bttBtn_last_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_last_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_last_Visible), 5, 0)), true);
         bttBtn_select_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_select_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_select_Visible), 5, 0)), true);
         bttBtn_delete_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_delete_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_delete_Visible), 5, 0)), true);
         if ( StringUtil.StrCmp(Gx_mode, "DSP") == 0 )
         {
            bttBtn_enter_Visible = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_enter_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_enter_Visible), 5, 0)), true);
         }
         DisableAttributes0I18( ) ;
      }

      protected void set_caption( )
      {
         if ( ( IsConfirmed == 1 ) && ( AnyError == 0 ) )
         {
            if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_confdelete", ""), 0, "", true);
            }
            else
            {
               GX_msglist.addItem(context.GetMessage( "GXM_mustconfirm", ""), 0, "", true);
            }
         }
      }

      protected void ResetCaption0I0( )
      {
      }

      protected void ZM0I18( short GX_JID )
      {
         if ( ( GX_JID == 1 ) || ( GX_JID == 0 ) )
         {
            if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
            {
               Z79FYActividadPlanCantPartFY = T000I3_A79FYActividadPlanCantPartFY[0];
               Z80mes1H = T000I3_A80mes1H[0];
               Z81mes1M = T000I3_A81mes1M[0];
               Z82mes2H = T000I3_A82mes2H[0];
               Z83mes2M = T000I3_A83mes2M[0];
               Z84mes3H = T000I3_A84mes3H[0];
               Z85mes3M = T000I3_A85mes3M[0];
               Z86mes4H = T000I3_A86mes4H[0];
               Z87mes4M = T000I3_A87mes4M[0];
               Z88mes5H = T000I3_A88mes5H[0];
               Z89mes5M = T000I3_A89mes5M[0];
               Z90mes6H = T000I3_A90mes6H[0];
               Z91mes6M = T000I3_A91mes6M[0];
               Z92mes7H = T000I3_A92mes7H[0];
               Z93mes7M = T000I3_A93mes7M[0];
               Z94mes8H = T000I3_A94mes8H[0];
               Z95mes8M = T000I3_A95mes8M[0];
               Z96mes9H = T000I3_A96mes9H[0];
               Z97mes9M = T000I3_A97mes9M[0];
               Z98mes10H = T000I3_A98mes10H[0];
               Z99mes10M = T000I3_A99mes10M[0];
               Z100mes11H = T000I3_A100mes11H[0];
               Z101mes11M = T000I3_A101mes11M[0];
               Z102mes12H = T000I3_A102mes12H[0];
               Z103mes12M = T000I3_A103mes12M[0];
               Z23ActividadParaPlanid = T000I3_A23ActividadParaPlanid[0];
            }
            else
            {
               Z79FYActividadPlanCantPartFY = A79FYActividadPlanCantPartFY;
               Z80mes1H = A80mes1H;
               Z81mes1M = A81mes1M;
               Z82mes2H = A82mes2H;
               Z83mes2M = A83mes2M;
               Z84mes3H = A84mes3H;
               Z85mes3M = A85mes3M;
               Z86mes4H = A86mes4H;
               Z87mes4M = A87mes4M;
               Z88mes5H = A88mes5H;
               Z89mes5M = A89mes5M;
               Z90mes6H = A90mes6H;
               Z91mes6M = A91mes6M;
               Z92mes7H = A92mes7H;
               Z93mes7M = A93mes7M;
               Z94mes8H = A94mes8H;
               Z95mes8M = A95mes8M;
               Z96mes9H = A96mes9H;
               Z97mes9M = A97mes9M;
               Z98mes10H = A98mes10H;
               Z99mes10M = A99mes10M;
               Z100mes11H = A100mes11H;
               Z101mes11M = A101mes11M;
               Z102mes12H = A102mes12H;
               Z103mes12M = A103mes12M;
               Z23ActividadParaPlanid = A23ActividadParaPlanid;
            }
         }
         if ( GX_JID == -1 )
         {
            Z14FYActividadPlanCantPartid = A14FYActividadPlanCantPartid;
            Z79FYActividadPlanCantPartFY = A79FYActividadPlanCantPartFY;
            Z80mes1H = A80mes1H;
            Z81mes1M = A81mes1M;
            Z82mes2H = A82mes2H;
            Z83mes2M = A83mes2M;
            Z84mes3H = A84mes3H;
            Z85mes3M = A85mes3M;
            Z86mes4H = A86mes4H;
            Z87mes4M = A87mes4M;
            Z88mes5H = A88mes5H;
            Z89mes5M = A89mes5M;
            Z90mes6H = A90mes6H;
            Z91mes6M = A91mes6M;
            Z92mes7H = A92mes7H;
            Z93mes7M = A93mes7M;
            Z94mes8H = A94mes8H;
            Z95mes8M = A95mes8M;
            Z96mes9H = A96mes9H;
            Z97mes9M = A97mes9M;
            Z98mes10H = A98mes10H;
            Z99mes10M = A99mes10M;
            Z100mes11H = A100mes11H;
            Z101mes11M = A101mes11M;
            Z102mes12H = A102mes12H;
            Z103mes12M = A103mes12M;
            Z23ActividadParaPlanid = A23ActividadParaPlanid;
         }
      }

      protected void standaloneNotModal( )
      {
         imgprompt_23_Link = ((StringUtil.StrCmp(Gx_mode, "DSP")==0) ? "" : "javascript:"+"gx.popup.openPrompt('"+"gx00e0.aspx"+"',["+"{Ctrl:gx.dom.el('"+"ACTIVIDADPARAPLANID"+"'), id:'"+"ACTIVIDADPARAPLANID"+"'"+",IOType:'out'}"+"],"+"null"+","+"'', false"+","+"false"+");");
      }

      protected void standaloneModal( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )
         {
            bttBtn_delete_Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_delete_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_delete_Enabled), 5, 0)), true);
         }
         else
         {
            bttBtn_delete_Enabled = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_delete_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_delete_Enabled), 5, 0)), true);
         }
         if ( StringUtil.StrCmp(Gx_mode, "DSP") == 0 )
         {
            bttBtn_enter_Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_enter_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_enter_Enabled), 5, 0)), true);
         }
         else
         {
            bttBtn_enter_Enabled = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_enter_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_enter_Enabled), 5, 0)), true);
         }
      }

      protected void Load0I18( )
      {
         /* Using cursor T000I5 */
         pr_datastore1.execute(3, new Object[] {A14FYActividadPlanCantPartid});
         if ( (pr_datastore1.getStatus(3) != 101) )
         {
            RcdFound18 = 1;
            A79FYActividadPlanCantPartFY = T000I5_A79FYActividadPlanCantPartFY[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A79FYActividadPlanCantPartFY", StringUtil.LTrim( StringUtil.Str( (decimal)(A79FYActividadPlanCantPartFY), 9, 0)));
            A80mes1H = T000I5_A80mes1H[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A80mes1H", StringUtil.LTrim( StringUtil.Str( (decimal)(A80mes1H), 9, 0)));
            n80mes1H = T000I5_n80mes1H[0];
            A81mes1M = T000I5_A81mes1M[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A81mes1M", StringUtil.LTrim( StringUtil.Str( (decimal)(A81mes1M), 9, 0)));
            n81mes1M = T000I5_n81mes1M[0];
            A82mes2H = T000I5_A82mes2H[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A82mes2H", StringUtil.LTrim( StringUtil.Str( (decimal)(A82mes2H), 9, 0)));
            n82mes2H = T000I5_n82mes2H[0];
            A83mes2M = T000I5_A83mes2M[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A83mes2M", StringUtil.LTrim( StringUtil.Str( (decimal)(A83mes2M), 9, 0)));
            n83mes2M = T000I5_n83mes2M[0];
            A84mes3H = T000I5_A84mes3H[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A84mes3H", StringUtil.LTrim( StringUtil.Str( (decimal)(A84mes3H), 9, 0)));
            n84mes3H = T000I5_n84mes3H[0];
            A85mes3M = T000I5_A85mes3M[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A85mes3M", StringUtil.LTrim( StringUtil.Str( (decimal)(A85mes3M), 9, 0)));
            n85mes3M = T000I5_n85mes3M[0];
            A86mes4H = T000I5_A86mes4H[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A86mes4H", StringUtil.LTrim( StringUtil.Str( (decimal)(A86mes4H), 9, 0)));
            n86mes4H = T000I5_n86mes4H[0];
            A87mes4M = T000I5_A87mes4M[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A87mes4M", StringUtil.LTrim( StringUtil.Str( (decimal)(A87mes4M), 9, 0)));
            n87mes4M = T000I5_n87mes4M[0];
            A88mes5H = T000I5_A88mes5H[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A88mes5H", StringUtil.LTrim( StringUtil.Str( (decimal)(A88mes5H), 9, 0)));
            n88mes5H = T000I5_n88mes5H[0];
            A89mes5M = T000I5_A89mes5M[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A89mes5M", StringUtil.LTrim( StringUtil.Str( (decimal)(A89mes5M), 9, 0)));
            n89mes5M = T000I5_n89mes5M[0];
            A90mes6H = T000I5_A90mes6H[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A90mes6H", StringUtil.LTrim( StringUtil.Str( (decimal)(A90mes6H), 9, 0)));
            n90mes6H = T000I5_n90mes6H[0];
            A91mes6M = T000I5_A91mes6M[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A91mes6M", StringUtil.LTrim( StringUtil.Str( (decimal)(A91mes6M), 9, 0)));
            n91mes6M = T000I5_n91mes6M[0];
            A92mes7H = T000I5_A92mes7H[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A92mes7H", StringUtil.LTrim( StringUtil.Str( (decimal)(A92mes7H), 9, 0)));
            n92mes7H = T000I5_n92mes7H[0];
            A93mes7M = T000I5_A93mes7M[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A93mes7M", StringUtil.LTrim( StringUtil.Str( (decimal)(A93mes7M), 9, 0)));
            n93mes7M = T000I5_n93mes7M[0];
            A94mes8H = T000I5_A94mes8H[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A94mes8H", StringUtil.LTrim( StringUtil.Str( (decimal)(A94mes8H), 9, 0)));
            n94mes8H = T000I5_n94mes8H[0];
            A95mes8M = T000I5_A95mes8M[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A95mes8M", StringUtil.LTrim( StringUtil.Str( (decimal)(A95mes8M), 9, 0)));
            n95mes8M = T000I5_n95mes8M[0];
            A96mes9H = T000I5_A96mes9H[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A96mes9H", StringUtil.LTrim( StringUtil.Str( (decimal)(A96mes9H), 9, 0)));
            n96mes9H = T000I5_n96mes9H[0];
            A97mes9M = T000I5_A97mes9M[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A97mes9M", StringUtil.LTrim( StringUtil.Str( (decimal)(A97mes9M), 9, 0)));
            n97mes9M = T000I5_n97mes9M[0];
            A98mes10H = T000I5_A98mes10H[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A98mes10H", StringUtil.LTrim( StringUtil.Str( (decimal)(A98mes10H), 9, 0)));
            n98mes10H = T000I5_n98mes10H[0];
            A99mes10M = T000I5_A99mes10M[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A99mes10M", StringUtil.LTrim( StringUtil.Str( (decimal)(A99mes10M), 9, 0)));
            n99mes10M = T000I5_n99mes10M[0];
            A100mes11H = T000I5_A100mes11H[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A100mes11H", StringUtil.LTrim( StringUtil.Str( (decimal)(A100mes11H), 9, 0)));
            n100mes11H = T000I5_n100mes11H[0];
            A101mes11M = T000I5_A101mes11M[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A101mes11M", StringUtil.LTrim( StringUtil.Str( (decimal)(A101mes11M), 9, 0)));
            n101mes11M = T000I5_n101mes11M[0];
            A102mes12H = T000I5_A102mes12H[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A102mes12H", StringUtil.LTrim( StringUtil.Str( (decimal)(A102mes12H), 9, 0)));
            n102mes12H = T000I5_n102mes12H[0];
            A103mes12M = T000I5_A103mes12M[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A103mes12M", StringUtil.LTrim( StringUtil.Str( (decimal)(A103mes12M), 9, 0)));
            n103mes12M = T000I5_n103mes12M[0];
            A23ActividadParaPlanid = T000I5_A23ActividadParaPlanid[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A23ActividadParaPlanid", StringUtil.LTrim( StringUtil.Str( (decimal)(A23ActividadParaPlanid), 9, 0)));
            ZM0I18( -1) ;
         }
         pr_datastore1.close(3);
         OnLoadActions0I18( ) ;
      }

      protected void OnLoadActions0I18( )
      {
      }

      protected void CheckExtendedTable0I18( )
      {
         Gx_BScreen = 1;
         standaloneModal( ) ;
         /* Using cursor T000I4 */
         pr_datastore1.execute(2, new Object[] {A23ActividadParaPlanid});
         if ( (pr_datastore1.getStatus(2) == 101) )
         {
            GX_msglist.addItem("No matching 'Actividad Para Plan'.", "ForeignKeyNotFound", 1, "ACTIVIDADPARAPLANID");
            AnyError = 1;
            GX_FocusControl = edtActividadParaPlanid_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         pr_datastore1.close(2);
      }

      protected void CloseExtendedTableCursors0I18( )
      {
         pr_datastore1.close(2);
      }

      protected void enableDisable( )
      {
      }

      protected void gxLoad_2( int A23ActividadParaPlanid )
      {
         /* Using cursor T000I6 */
         pr_datastore1.execute(4, new Object[] {A23ActividadParaPlanid});
         if ( (pr_datastore1.getStatus(4) == 101) )
         {
            GX_msglist.addItem("No matching 'Actividad Para Plan'.", "ForeignKeyNotFound", 1, "ACTIVIDADPARAPLANID");
            AnyError = 1;
            GX_FocusControl = edtActividadParaPlanid_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         GxWebStd.set_html_headers( context, 0, "", "");
         context.GX_webresponse.AddString("[[");
         context.GX_webresponse.AddString("");
         context.GX_webresponse.AddString("]");
         if ( (pr_datastore1.getStatus(4) == 101) )
         {
            context.GX_webresponse.AddString(",");
            context.GX_webresponse.AddString("101");
         }
         context.GX_webresponse.AddString("]");
         pr_datastore1.close(4);
      }

      protected void GetKey0I18( )
      {
         /* Using cursor T000I7 */
         pr_datastore1.execute(5, new Object[] {A14FYActividadPlanCantPartid});
         if ( (pr_datastore1.getStatus(5) != 101) )
         {
            RcdFound18 = 1;
         }
         else
         {
            RcdFound18 = 0;
         }
         pr_datastore1.close(5);
      }

      protected void getByPrimaryKey( )
      {
         /* Using cursor T000I3 */
         pr_datastore1.execute(1, new Object[] {A14FYActividadPlanCantPartid});
         if ( (pr_datastore1.getStatus(1) != 101) )
         {
            ZM0I18( 1) ;
            RcdFound18 = 1;
            A14FYActividadPlanCantPartid = T000I3_A14FYActividadPlanCantPartid[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A14FYActividadPlanCantPartid", StringUtil.LTrim( StringUtil.Str( (decimal)(A14FYActividadPlanCantPartid), 9, 0)));
            A79FYActividadPlanCantPartFY = T000I3_A79FYActividadPlanCantPartFY[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A79FYActividadPlanCantPartFY", StringUtil.LTrim( StringUtil.Str( (decimal)(A79FYActividadPlanCantPartFY), 9, 0)));
            A80mes1H = T000I3_A80mes1H[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A80mes1H", StringUtil.LTrim( StringUtil.Str( (decimal)(A80mes1H), 9, 0)));
            n80mes1H = T000I3_n80mes1H[0];
            A81mes1M = T000I3_A81mes1M[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A81mes1M", StringUtil.LTrim( StringUtil.Str( (decimal)(A81mes1M), 9, 0)));
            n81mes1M = T000I3_n81mes1M[0];
            A82mes2H = T000I3_A82mes2H[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A82mes2H", StringUtil.LTrim( StringUtil.Str( (decimal)(A82mes2H), 9, 0)));
            n82mes2H = T000I3_n82mes2H[0];
            A83mes2M = T000I3_A83mes2M[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A83mes2M", StringUtil.LTrim( StringUtil.Str( (decimal)(A83mes2M), 9, 0)));
            n83mes2M = T000I3_n83mes2M[0];
            A84mes3H = T000I3_A84mes3H[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A84mes3H", StringUtil.LTrim( StringUtil.Str( (decimal)(A84mes3H), 9, 0)));
            n84mes3H = T000I3_n84mes3H[0];
            A85mes3M = T000I3_A85mes3M[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A85mes3M", StringUtil.LTrim( StringUtil.Str( (decimal)(A85mes3M), 9, 0)));
            n85mes3M = T000I3_n85mes3M[0];
            A86mes4H = T000I3_A86mes4H[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A86mes4H", StringUtil.LTrim( StringUtil.Str( (decimal)(A86mes4H), 9, 0)));
            n86mes4H = T000I3_n86mes4H[0];
            A87mes4M = T000I3_A87mes4M[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A87mes4M", StringUtil.LTrim( StringUtil.Str( (decimal)(A87mes4M), 9, 0)));
            n87mes4M = T000I3_n87mes4M[0];
            A88mes5H = T000I3_A88mes5H[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A88mes5H", StringUtil.LTrim( StringUtil.Str( (decimal)(A88mes5H), 9, 0)));
            n88mes5H = T000I3_n88mes5H[0];
            A89mes5M = T000I3_A89mes5M[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A89mes5M", StringUtil.LTrim( StringUtil.Str( (decimal)(A89mes5M), 9, 0)));
            n89mes5M = T000I3_n89mes5M[0];
            A90mes6H = T000I3_A90mes6H[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A90mes6H", StringUtil.LTrim( StringUtil.Str( (decimal)(A90mes6H), 9, 0)));
            n90mes6H = T000I3_n90mes6H[0];
            A91mes6M = T000I3_A91mes6M[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A91mes6M", StringUtil.LTrim( StringUtil.Str( (decimal)(A91mes6M), 9, 0)));
            n91mes6M = T000I3_n91mes6M[0];
            A92mes7H = T000I3_A92mes7H[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A92mes7H", StringUtil.LTrim( StringUtil.Str( (decimal)(A92mes7H), 9, 0)));
            n92mes7H = T000I3_n92mes7H[0];
            A93mes7M = T000I3_A93mes7M[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A93mes7M", StringUtil.LTrim( StringUtil.Str( (decimal)(A93mes7M), 9, 0)));
            n93mes7M = T000I3_n93mes7M[0];
            A94mes8H = T000I3_A94mes8H[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A94mes8H", StringUtil.LTrim( StringUtil.Str( (decimal)(A94mes8H), 9, 0)));
            n94mes8H = T000I3_n94mes8H[0];
            A95mes8M = T000I3_A95mes8M[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A95mes8M", StringUtil.LTrim( StringUtil.Str( (decimal)(A95mes8M), 9, 0)));
            n95mes8M = T000I3_n95mes8M[0];
            A96mes9H = T000I3_A96mes9H[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A96mes9H", StringUtil.LTrim( StringUtil.Str( (decimal)(A96mes9H), 9, 0)));
            n96mes9H = T000I3_n96mes9H[0];
            A97mes9M = T000I3_A97mes9M[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A97mes9M", StringUtil.LTrim( StringUtil.Str( (decimal)(A97mes9M), 9, 0)));
            n97mes9M = T000I3_n97mes9M[0];
            A98mes10H = T000I3_A98mes10H[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A98mes10H", StringUtil.LTrim( StringUtil.Str( (decimal)(A98mes10H), 9, 0)));
            n98mes10H = T000I3_n98mes10H[0];
            A99mes10M = T000I3_A99mes10M[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A99mes10M", StringUtil.LTrim( StringUtil.Str( (decimal)(A99mes10M), 9, 0)));
            n99mes10M = T000I3_n99mes10M[0];
            A100mes11H = T000I3_A100mes11H[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A100mes11H", StringUtil.LTrim( StringUtil.Str( (decimal)(A100mes11H), 9, 0)));
            n100mes11H = T000I3_n100mes11H[0];
            A101mes11M = T000I3_A101mes11M[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A101mes11M", StringUtil.LTrim( StringUtil.Str( (decimal)(A101mes11M), 9, 0)));
            n101mes11M = T000I3_n101mes11M[0];
            A102mes12H = T000I3_A102mes12H[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A102mes12H", StringUtil.LTrim( StringUtil.Str( (decimal)(A102mes12H), 9, 0)));
            n102mes12H = T000I3_n102mes12H[0];
            A103mes12M = T000I3_A103mes12M[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A103mes12M", StringUtil.LTrim( StringUtil.Str( (decimal)(A103mes12M), 9, 0)));
            n103mes12M = T000I3_n103mes12M[0];
            A23ActividadParaPlanid = T000I3_A23ActividadParaPlanid[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A23ActividadParaPlanid", StringUtil.LTrim( StringUtil.Str( (decimal)(A23ActividadParaPlanid), 9, 0)));
            Z14FYActividadPlanCantPartid = A14FYActividadPlanCantPartid;
            sMode18 = Gx_mode;
            Gx_mode = "DSP";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            standaloneModal( ) ;
            Load0I18( ) ;
            if ( AnyError == 1 )
            {
               RcdFound18 = 0;
               InitializeNonKey0I18( ) ;
            }
            Gx_mode = sMode18;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         }
         else
         {
            RcdFound18 = 0;
            InitializeNonKey0I18( ) ;
            sMode18 = Gx_mode;
            Gx_mode = "DSP";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            standaloneModal( ) ;
            Gx_mode = sMode18;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         }
         pr_datastore1.close(1);
      }

      protected void getEqualNoModal( )
      {
         GetKey0I18( ) ;
         if ( RcdFound18 == 0 )
         {
            Gx_mode = "INS";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         }
         else
         {
            Gx_mode = "UPD";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         }
         getByPrimaryKey( ) ;
      }

      protected void move_next( )
      {
         RcdFound18 = 0;
         /* Using cursor T000I8 */
         pr_datastore1.execute(6, new Object[] {A14FYActividadPlanCantPartid});
         if ( (pr_datastore1.getStatus(6) != 101) )
         {
            while ( (pr_datastore1.getStatus(6) != 101) && ( ( T000I8_A14FYActividadPlanCantPartid[0] < A14FYActividadPlanCantPartid ) ) )
            {
               pr_datastore1.readNext(6);
            }
            if ( (pr_datastore1.getStatus(6) != 101) && ( ( T000I8_A14FYActividadPlanCantPartid[0] > A14FYActividadPlanCantPartid ) ) )
            {
               A14FYActividadPlanCantPartid = T000I8_A14FYActividadPlanCantPartid[0];
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A14FYActividadPlanCantPartid", StringUtil.LTrim( StringUtil.Str( (decimal)(A14FYActividadPlanCantPartid), 9, 0)));
               RcdFound18 = 1;
            }
         }
         pr_datastore1.close(6);
      }

      protected void move_previous( )
      {
         RcdFound18 = 0;
         /* Using cursor T000I9 */
         pr_datastore1.execute(7, new Object[] {A14FYActividadPlanCantPartid});
         if ( (pr_datastore1.getStatus(7) != 101) )
         {
            while ( (pr_datastore1.getStatus(7) != 101) && ( ( T000I9_A14FYActividadPlanCantPartid[0] > A14FYActividadPlanCantPartid ) ) )
            {
               pr_datastore1.readNext(7);
            }
            if ( (pr_datastore1.getStatus(7) != 101) && ( ( T000I9_A14FYActividadPlanCantPartid[0] < A14FYActividadPlanCantPartid ) ) )
            {
               A14FYActividadPlanCantPartid = T000I9_A14FYActividadPlanCantPartid[0];
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A14FYActividadPlanCantPartid", StringUtil.LTrim( StringUtil.Str( (decimal)(A14FYActividadPlanCantPartid), 9, 0)));
               RcdFound18 = 1;
            }
         }
         pr_datastore1.close(7);
      }

      protected void btn_enter( )
      {
         nKeyPressed = 1;
         GetKey0I18( ) ;
         if ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )
         {
            /* Insert record */
            GX_FocusControl = edtFYActividadPlanCantPartid_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            Insert0I18( ) ;
            if ( AnyError == 1 )
            {
               GX_FocusControl = "";
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            }
         }
         else
         {
            if ( RcdFound18 == 1 )
            {
               if ( A14FYActividadPlanCantPartid != Z14FYActividadPlanCantPartid )
               {
                  A14FYActividadPlanCantPartid = Z14FYActividadPlanCantPartid;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A14FYActividadPlanCantPartid", StringUtil.LTrim( StringUtil.Str( (decimal)(A14FYActividadPlanCantPartid), 9, 0)));
                  GX_msglist.addItem(context.GetMessage( "GXM_getbeforeupd", ""), "CandidateKeyNotFound", 1, "FYACTIVIDADPLANCANTPARTID");
                  AnyError = 1;
                  GX_FocusControl = edtFYActividadPlanCantPartid_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               }
               else if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
               {
                  delete( ) ;
                  AfterTrn( ) ;
                  GX_FocusControl = edtFYActividadPlanCantPartid_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               }
               else
               {
                  Gx_mode = "UPD";
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                  /* Update record */
                  Update0I18( ) ;
                  GX_FocusControl = edtFYActividadPlanCantPartid_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               }
            }
            else
            {
               if ( A14FYActividadPlanCantPartid != Z14FYActividadPlanCantPartid )
               {
                  Gx_mode = "INS";
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                  /* Insert record */
                  GX_FocusControl = edtFYActividadPlanCantPartid_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  Insert0I18( ) ;
                  if ( AnyError == 1 )
                  {
                     GX_FocusControl = "";
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  }
               }
               else
               {
                  if ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 )
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_recdeleted", ""), 1, "FYACTIVIDADPLANCANTPARTID");
                     AnyError = 1;
                     GX_FocusControl = edtFYActividadPlanCantPartid_Internalname;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  }
                  else
                  {
                     Gx_mode = "INS";
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                     /* Insert record */
                     GX_FocusControl = edtFYActividadPlanCantPartid_Internalname;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                     Insert0I18( ) ;
                     if ( AnyError == 1 )
                     {
                        GX_FocusControl = "";
                        context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                     }
                  }
               }
            }
         }
         AfterTrn( ) ;
      }

      protected void btn_delete( )
      {
         if ( A14FYActividadPlanCantPartid != Z14FYActividadPlanCantPartid )
         {
            A14FYActividadPlanCantPartid = Z14FYActividadPlanCantPartid;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A14FYActividadPlanCantPartid", StringUtil.LTrim( StringUtil.Str( (decimal)(A14FYActividadPlanCantPartid), 9, 0)));
            GX_msglist.addItem(context.GetMessage( "GXM_getbeforedlt", ""), 1, "FYACTIVIDADPLANCANTPARTID");
            AnyError = 1;
            GX_FocusControl = edtFYActividadPlanCantPartid_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         else
         {
            delete( ) ;
            AfterTrn( ) ;
            GX_FocusControl = edtFYActividadPlanCantPartid_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         if ( AnyError != 0 )
         {
            Gx_mode = "UPD";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         }
         else
         {
            getByPrimaryKey( ) ;
         }
         CloseOpenCursors();
      }

      protected void btn_get( )
      {
         nKeyPressed = 2;
         IsConfirmed = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "IsConfirmed", StringUtil.LTrim( StringUtil.Str( (decimal)(IsConfirmed), 4, 0)));
         getEqualNoModal( ) ;
         if ( RcdFound18 == 0 )
         {
            GX_msglist.addItem(context.GetMessage( "GXM_keynfound", ""), "PrimaryKeyNotFound", 1, "FYACTIVIDADPLANCANTPARTID");
            AnyError = 1;
            GX_FocusControl = edtFYActividadPlanCantPartid_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         GX_FocusControl = edtActividadParaPlanid_Internalname;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         standaloneNotModal( ) ;
         standaloneModal( ) ;
      }

      protected void btn_first( )
      {
         nKeyPressed = 2;
         IsConfirmed = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "IsConfirmed", StringUtil.LTrim( StringUtil.Str( (decimal)(IsConfirmed), 4, 0)));
         ScanStart0I18( ) ;
         if ( RcdFound18 == 0 )
         {
            GX_msglist.addItem(context.GetMessage( "GXM_norectobrow", ""), 0, "", true);
         }
         else
         {
            Gx_mode = "UPD";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         }
         GX_FocusControl = edtActividadParaPlanid_Internalname;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         ScanEnd0I18( ) ;
         getByPrimaryKey( ) ;
         standaloneNotModal( ) ;
         standaloneModal( ) ;
      }

      protected void btn_previous( )
      {
         nKeyPressed = 2;
         IsConfirmed = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "IsConfirmed", StringUtil.LTrim( StringUtil.Str( (decimal)(IsConfirmed), 4, 0)));
         move_previous( ) ;
         if ( RcdFound18 == 0 )
         {
            GX_msglist.addItem(context.GetMessage( "GXM_norectobrow", ""), 0, "", true);
         }
         else
         {
            Gx_mode = "UPD";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         }
         GX_FocusControl = edtActividadParaPlanid_Internalname;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         getByPrimaryKey( ) ;
         standaloneNotModal( ) ;
         standaloneModal( ) ;
      }

      protected void btn_next( )
      {
         nKeyPressed = 2;
         IsConfirmed = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "IsConfirmed", StringUtil.LTrim( StringUtil.Str( (decimal)(IsConfirmed), 4, 0)));
         move_next( ) ;
         if ( RcdFound18 == 0 )
         {
            GX_msglist.addItem(context.GetMessage( "GXM_norectobrow", ""), 0, "", true);
         }
         else
         {
            Gx_mode = "UPD";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         }
         GX_FocusControl = edtActividadParaPlanid_Internalname;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         getByPrimaryKey( ) ;
         standaloneNotModal( ) ;
         standaloneModal( ) ;
      }

      protected void btn_last( )
      {
         nKeyPressed = 2;
         IsConfirmed = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "IsConfirmed", StringUtil.LTrim( StringUtil.Str( (decimal)(IsConfirmed), 4, 0)));
         ScanStart0I18( ) ;
         if ( RcdFound18 == 0 )
         {
            GX_msglist.addItem(context.GetMessage( "GXM_norectobrow", ""), 0, "", true);
         }
         else
         {
            while ( RcdFound18 != 0 )
            {
               ScanNext0I18( ) ;
            }
            Gx_mode = "UPD";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         }
         GX_FocusControl = edtActividadParaPlanid_Internalname;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         ScanEnd0I18( ) ;
         getByPrimaryKey( ) ;
         standaloneNotModal( ) ;
         standaloneModal( ) ;
      }

      protected void btn_select( )
      {
         getEqualNoModal( ) ;
      }

      protected void CheckOptimisticConcurrency0I18( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
         {
            /* Using cursor T000I2 */
            pr_datastore1.execute(0, new Object[] {A14FYActividadPlanCantPartid});
            if ( (pr_datastore1.getStatus(0) == 103) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_lock", new   object[]  {"FYACTIVIDADPLANCANTPART"}), "RecordIsLocked", 1, "");
               AnyError = 1;
               return  ;
            }
            Gx_longc = false;
            if ( (pr_datastore1.getStatus(0) == 101) || ( Z79FYActividadPlanCantPartFY != T000I2_A79FYActividadPlanCantPartFY[0] ) || ( Z80mes1H != T000I2_A80mes1H[0] ) || ( Z81mes1M != T000I2_A81mes1M[0] ) || ( Z82mes2H != T000I2_A82mes2H[0] ) || ( Z83mes2M != T000I2_A83mes2M[0] ) )
            {
               Gx_longc = true;
            }
            if ( Gx_longc || ( Z84mes3H != T000I2_A84mes3H[0] ) || ( Z85mes3M != T000I2_A85mes3M[0] ) || ( Z86mes4H != T000I2_A86mes4H[0] ) || ( Z87mes4M != T000I2_A87mes4M[0] ) || ( Z88mes5H != T000I2_A88mes5H[0] ) )
            {
               Gx_longc = true;
            }
            if ( Gx_longc || ( Z89mes5M != T000I2_A89mes5M[0] ) || ( Z90mes6H != T000I2_A90mes6H[0] ) || ( Z91mes6M != T000I2_A91mes6M[0] ) || ( Z92mes7H != T000I2_A92mes7H[0] ) || ( Z93mes7M != T000I2_A93mes7M[0] ) )
            {
               Gx_longc = true;
            }
            if ( Gx_longc || ( Z94mes8H != T000I2_A94mes8H[0] ) || ( Z95mes8M != T000I2_A95mes8M[0] ) || ( Z96mes9H != T000I2_A96mes9H[0] ) || ( Z97mes9M != T000I2_A97mes9M[0] ) || ( Z98mes10H != T000I2_A98mes10H[0] ) )
            {
               Gx_longc = true;
            }
            if ( Gx_longc || ( Z99mes10M != T000I2_A99mes10M[0] ) || ( Z100mes11H != T000I2_A100mes11H[0] ) || ( Z101mes11M != T000I2_A101mes11M[0] ) || ( Z102mes12H != T000I2_A102mes12H[0] ) || ( Z103mes12M != T000I2_A103mes12M[0] ) )
            {
               Gx_longc = true;
            }
            if ( Gx_longc || ( Z23ActividadParaPlanid != T000I2_A23ActividadParaPlanid[0] ) )
            {
               if ( Z79FYActividadPlanCantPartFY != T000I2_A79FYActividadPlanCantPartFY[0] )
               {
                  GXUtil.WriteLog("fyactividadplancantpart:[seudo value changed for attri]"+"FYActividadPlanCantPartFY");
                  GXUtil.WriteLogRaw("Old: ",Z79FYActividadPlanCantPartFY);
                  GXUtil.WriteLogRaw("Current: ",T000I2_A79FYActividadPlanCantPartFY[0]);
               }
               if ( Z80mes1H != T000I2_A80mes1H[0] )
               {
                  GXUtil.WriteLog("fyactividadplancantpart:[seudo value changed for attri]"+"mes1H");
                  GXUtil.WriteLogRaw("Old: ",Z80mes1H);
                  GXUtil.WriteLogRaw("Current: ",T000I2_A80mes1H[0]);
               }
               if ( Z81mes1M != T000I2_A81mes1M[0] )
               {
                  GXUtil.WriteLog("fyactividadplancantpart:[seudo value changed for attri]"+"mes1M");
                  GXUtil.WriteLogRaw("Old: ",Z81mes1M);
                  GXUtil.WriteLogRaw("Current: ",T000I2_A81mes1M[0]);
               }
               if ( Z82mes2H != T000I2_A82mes2H[0] )
               {
                  GXUtil.WriteLog("fyactividadplancantpart:[seudo value changed for attri]"+"mes2H");
                  GXUtil.WriteLogRaw("Old: ",Z82mes2H);
                  GXUtil.WriteLogRaw("Current: ",T000I2_A82mes2H[0]);
               }
               if ( Z83mes2M != T000I2_A83mes2M[0] )
               {
                  GXUtil.WriteLog("fyactividadplancantpart:[seudo value changed for attri]"+"mes2M");
                  GXUtil.WriteLogRaw("Old: ",Z83mes2M);
                  GXUtil.WriteLogRaw("Current: ",T000I2_A83mes2M[0]);
               }
               if ( Z84mes3H != T000I2_A84mes3H[0] )
               {
                  GXUtil.WriteLog("fyactividadplancantpart:[seudo value changed for attri]"+"mes3H");
                  GXUtil.WriteLogRaw("Old: ",Z84mes3H);
                  GXUtil.WriteLogRaw("Current: ",T000I2_A84mes3H[0]);
               }
               if ( Z85mes3M != T000I2_A85mes3M[0] )
               {
                  GXUtil.WriteLog("fyactividadplancantpart:[seudo value changed for attri]"+"mes3M");
                  GXUtil.WriteLogRaw("Old: ",Z85mes3M);
                  GXUtil.WriteLogRaw("Current: ",T000I2_A85mes3M[0]);
               }
               if ( Z86mes4H != T000I2_A86mes4H[0] )
               {
                  GXUtil.WriteLog("fyactividadplancantpart:[seudo value changed for attri]"+"mes4H");
                  GXUtil.WriteLogRaw("Old: ",Z86mes4H);
                  GXUtil.WriteLogRaw("Current: ",T000I2_A86mes4H[0]);
               }
               if ( Z87mes4M != T000I2_A87mes4M[0] )
               {
                  GXUtil.WriteLog("fyactividadplancantpart:[seudo value changed for attri]"+"mes4M");
                  GXUtil.WriteLogRaw("Old: ",Z87mes4M);
                  GXUtil.WriteLogRaw("Current: ",T000I2_A87mes4M[0]);
               }
               if ( Z88mes5H != T000I2_A88mes5H[0] )
               {
                  GXUtil.WriteLog("fyactividadplancantpart:[seudo value changed for attri]"+"mes5H");
                  GXUtil.WriteLogRaw("Old: ",Z88mes5H);
                  GXUtil.WriteLogRaw("Current: ",T000I2_A88mes5H[0]);
               }
               if ( Z89mes5M != T000I2_A89mes5M[0] )
               {
                  GXUtil.WriteLog("fyactividadplancantpart:[seudo value changed for attri]"+"mes5M");
                  GXUtil.WriteLogRaw("Old: ",Z89mes5M);
                  GXUtil.WriteLogRaw("Current: ",T000I2_A89mes5M[0]);
               }
               if ( Z90mes6H != T000I2_A90mes6H[0] )
               {
                  GXUtil.WriteLog("fyactividadplancantpart:[seudo value changed for attri]"+"mes6H");
                  GXUtil.WriteLogRaw("Old: ",Z90mes6H);
                  GXUtil.WriteLogRaw("Current: ",T000I2_A90mes6H[0]);
               }
               if ( Z91mes6M != T000I2_A91mes6M[0] )
               {
                  GXUtil.WriteLog("fyactividadplancantpart:[seudo value changed for attri]"+"mes6M");
                  GXUtil.WriteLogRaw("Old: ",Z91mes6M);
                  GXUtil.WriteLogRaw("Current: ",T000I2_A91mes6M[0]);
               }
               if ( Z92mes7H != T000I2_A92mes7H[0] )
               {
                  GXUtil.WriteLog("fyactividadplancantpart:[seudo value changed for attri]"+"mes7H");
                  GXUtil.WriteLogRaw("Old: ",Z92mes7H);
                  GXUtil.WriteLogRaw("Current: ",T000I2_A92mes7H[0]);
               }
               if ( Z93mes7M != T000I2_A93mes7M[0] )
               {
                  GXUtil.WriteLog("fyactividadplancantpart:[seudo value changed for attri]"+"mes7M");
                  GXUtil.WriteLogRaw("Old: ",Z93mes7M);
                  GXUtil.WriteLogRaw("Current: ",T000I2_A93mes7M[0]);
               }
               if ( Z94mes8H != T000I2_A94mes8H[0] )
               {
                  GXUtil.WriteLog("fyactividadplancantpart:[seudo value changed for attri]"+"mes8H");
                  GXUtil.WriteLogRaw("Old: ",Z94mes8H);
                  GXUtil.WriteLogRaw("Current: ",T000I2_A94mes8H[0]);
               }
               if ( Z95mes8M != T000I2_A95mes8M[0] )
               {
                  GXUtil.WriteLog("fyactividadplancantpart:[seudo value changed for attri]"+"mes8M");
                  GXUtil.WriteLogRaw("Old: ",Z95mes8M);
                  GXUtil.WriteLogRaw("Current: ",T000I2_A95mes8M[0]);
               }
               if ( Z96mes9H != T000I2_A96mes9H[0] )
               {
                  GXUtil.WriteLog("fyactividadplancantpart:[seudo value changed for attri]"+"mes9H");
                  GXUtil.WriteLogRaw("Old: ",Z96mes9H);
                  GXUtil.WriteLogRaw("Current: ",T000I2_A96mes9H[0]);
               }
               if ( Z97mes9M != T000I2_A97mes9M[0] )
               {
                  GXUtil.WriteLog("fyactividadplancantpart:[seudo value changed for attri]"+"mes9M");
                  GXUtil.WriteLogRaw("Old: ",Z97mes9M);
                  GXUtil.WriteLogRaw("Current: ",T000I2_A97mes9M[0]);
               }
               if ( Z98mes10H != T000I2_A98mes10H[0] )
               {
                  GXUtil.WriteLog("fyactividadplancantpart:[seudo value changed for attri]"+"mes10H");
                  GXUtil.WriteLogRaw("Old: ",Z98mes10H);
                  GXUtil.WriteLogRaw("Current: ",T000I2_A98mes10H[0]);
               }
               if ( Z99mes10M != T000I2_A99mes10M[0] )
               {
                  GXUtil.WriteLog("fyactividadplancantpart:[seudo value changed for attri]"+"mes10M");
                  GXUtil.WriteLogRaw("Old: ",Z99mes10M);
                  GXUtil.WriteLogRaw("Current: ",T000I2_A99mes10M[0]);
               }
               if ( Z100mes11H != T000I2_A100mes11H[0] )
               {
                  GXUtil.WriteLog("fyactividadplancantpart:[seudo value changed for attri]"+"mes11H");
                  GXUtil.WriteLogRaw("Old: ",Z100mes11H);
                  GXUtil.WriteLogRaw("Current: ",T000I2_A100mes11H[0]);
               }
               if ( Z101mes11M != T000I2_A101mes11M[0] )
               {
                  GXUtil.WriteLog("fyactividadplancantpart:[seudo value changed for attri]"+"mes11M");
                  GXUtil.WriteLogRaw("Old: ",Z101mes11M);
                  GXUtil.WriteLogRaw("Current: ",T000I2_A101mes11M[0]);
               }
               if ( Z102mes12H != T000I2_A102mes12H[0] )
               {
                  GXUtil.WriteLog("fyactividadplancantpart:[seudo value changed for attri]"+"mes12H");
                  GXUtil.WriteLogRaw("Old: ",Z102mes12H);
                  GXUtil.WriteLogRaw("Current: ",T000I2_A102mes12H[0]);
               }
               if ( Z103mes12M != T000I2_A103mes12M[0] )
               {
                  GXUtil.WriteLog("fyactividadplancantpart:[seudo value changed for attri]"+"mes12M");
                  GXUtil.WriteLogRaw("Old: ",Z103mes12M);
                  GXUtil.WriteLogRaw("Current: ",T000I2_A103mes12M[0]);
               }
               if ( Z23ActividadParaPlanid != T000I2_A23ActividadParaPlanid[0] )
               {
                  GXUtil.WriteLog("fyactividadplancantpart:[seudo value changed for attri]"+"ActividadParaPlanid");
                  GXUtil.WriteLogRaw("Old: ",Z23ActividadParaPlanid);
                  GXUtil.WriteLogRaw("Current: ",T000I2_A23ActividadParaPlanid[0]);
               }
               GX_msglist.addItem(context.GetMessage( "GXM_waschg", new   object[]  {"FYACTIVIDADPLANCANTPART"}), "RecordWasChanged", 1, "");
               AnyError = 1;
               return  ;
            }
         }
      }

      protected void Insert0I18( )
      {
         BeforeValidate0I18( ) ;
         if ( AnyError == 0 )
         {
            CheckExtendedTable0I18( ) ;
         }
         if ( AnyError == 0 )
         {
            ZM0I18( 0) ;
            CheckOptimisticConcurrency0I18( ) ;
            if ( AnyError == 0 )
            {
               AfterConfirm0I18( ) ;
               if ( AnyError == 0 )
               {
                  BeforeInsert0I18( ) ;
                  if ( AnyError == 0 )
                  {
                     /* Using cursor T000I10 */
                     pr_datastore1.execute(8, new Object[] {A79FYActividadPlanCantPartFY, n80mes1H, A80mes1H, n81mes1M, A81mes1M, n82mes2H, A82mes2H, n83mes2M, A83mes2M, n84mes3H, A84mes3H, n85mes3M, A85mes3M, n86mes4H, A86mes4H, n87mes4M, A87mes4M, n88mes5H, A88mes5H, n89mes5M, A89mes5M, n90mes6H, A90mes6H, n91mes6M, A91mes6M, n92mes7H, A92mes7H, n93mes7M, A93mes7M, n94mes8H, A94mes8H, n95mes8M, A95mes8M, n96mes9H, A96mes9H, n97mes9M, A97mes9M, n98mes10H, A98mes10H, n99mes10M, A99mes10M, n100mes11H, A100mes11H, n101mes11M, A101mes11M, n102mes12H, A102mes12H, n103mes12M, A103mes12M, A23ActividadParaPlanid});
                     A14FYActividadPlanCantPartid = T000I10_A14FYActividadPlanCantPartid[0];
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A14FYActividadPlanCantPartid", StringUtil.LTrim( StringUtil.Str( (decimal)(A14FYActividadPlanCantPartid), 9, 0)));
                     pr_datastore1.close(8);
                     dsDataStore1.SmartCacheProvider.SetUpdated("FYACTIVIDADPLANCANTPART") ;
                     if ( AnyError == 0 )
                     {
                        /* Start of After( Insert) rules */
                        /* End of After( Insert) rules */
                        if ( AnyError == 0 )
                        {
                           /* Save values for previous() function. */
                           GX_msglist.addItem(context.GetMessage( "GXM_sucadded", ""), "SuccessfullyAdded", 0, "", true);
                           ResetCaption0I0( ) ;
                        }
                     }
                  }
                  else
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                     AnyError = 1;
                  }
               }
            }
            else
            {
               Load0I18( ) ;
            }
            EndLevel0I18( ) ;
         }
         CloseExtendedTableCursors0I18( ) ;
      }

      protected void Update0I18( )
      {
         BeforeValidate0I18( ) ;
         if ( AnyError == 0 )
         {
            CheckExtendedTable0I18( ) ;
         }
         if ( AnyError == 0 )
         {
            CheckOptimisticConcurrency0I18( ) ;
            if ( AnyError == 0 )
            {
               AfterConfirm0I18( ) ;
               if ( AnyError == 0 )
               {
                  BeforeUpdate0I18( ) ;
                  if ( AnyError == 0 )
                  {
                     /* Using cursor T000I11 */
                     pr_datastore1.execute(9, new Object[] {A79FYActividadPlanCantPartFY, n80mes1H, A80mes1H, n81mes1M, A81mes1M, n82mes2H, A82mes2H, n83mes2M, A83mes2M, n84mes3H, A84mes3H, n85mes3M, A85mes3M, n86mes4H, A86mes4H, n87mes4M, A87mes4M, n88mes5H, A88mes5H, n89mes5M, A89mes5M, n90mes6H, A90mes6H, n91mes6M, A91mes6M, n92mes7H, A92mes7H, n93mes7M, A93mes7M, n94mes8H, A94mes8H, n95mes8M, A95mes8M, n96mes9H, A96mes9H, n97mes9M, A97mes9M, n98mes10H, A98mes10H, n99mes10M, A99mes10M, n100mes11H, A100mes11H, n101mes11M, A101mes11M, n102mes12H, A102mes12H, n103mes12M, A103mes12M, A23ActividadParaPlanid, A14FYActividadPlanCantPartid});
                     pr_datastore1.close(9);
                     dsDataStore1.SmartCacheProvider.SetUpdated("FYACTIVIDADPLANCANTPART") ;
                     if ( (pr_datastore1.getStatus(9) == 103) )
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_lock", new   object[]  {"FYACTIVIDADPLANCANTPART"}), "RecordIsLocked", 1, "");
                        AnyError = 1;
                     }
                     DeferredUpdate0I18( ) ;
                     if ( AnyError == 0 )
                     {
                        /* Start of After( update) rules */
                        /* End of After( update) rules */
                        if ( AnyError == 0 )
                        {
                           getByPrimaryKey( ) ;
                           GX_msglist.addItem(context.GetMessage( "GXM_sucupdated", ""), "SuccessfullyUpdated", 0, "", true);
                           ResetCaption0I0( ) ;
                        }
                     }
                     else
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                        AnyError = 1;
                     }
                  }
               }
            }
            EndLevel0I18( ) ;
         }
         CloseExtendedTableCursors0I18( ) ;
      }

      protected void DeferredUpdate0I18( )
      {
      }

      protected void delete( )
      {
         Gx_mode = "DLT";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         BeforeValidate0I18( ) ;
         if ( AnyError == 0 )
         {
            CheckOptimisticConcurrency0I18( ) ;
         }
         if ( AnyError == 0 )
         {
            OnDeleteControls0I18( ) ;
            AfterConfirm0I18( ) ;
            if ( AnyError == 0 )
            {
               BeforeDelete0I18( ) ;
               if ( AnyError == 0 )
               {
                  /* No cascading delete specified. */
                  /* Using cursor T000I12 */
                  pr_datastore1.execute(10, new Object[] {A14FYActividadPlanCantPartid});
                  pr_datastore1.close(10);
                  dsDataStore1.SmartCacheProvider.SetUpdated("FYACTIVIDADPLANCANTPART") ;
                  if ( AnyError == 0 )
                  {
                     /* Start of After( delete) rules */
                     /* End of After( delete) rules */
                     if ( AnyError == 0 )
                     {
                        move_next( ) ;
                        if ( RcdFound18 == 0 )
                        {
                           InitAll0I18( ) ;
                           Gx_mode = "INS";
                           context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                        }
                        else
                        {
                           getByPrimaryKey( ) ;
                           Gx_mode = "UPD";
                           context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                        }
                        GX_msglist.addItem(context.GetMessage( "GXM_sucdeleted", ""), "SuccessfullyDeleted", 0, "", true);
                        ResetCaption0I0( ) ;
                     }
                  }
                  else
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                     AnyError = 1;
                  }
               }
            }
         }
         sMode18 = Gx_mode;
         Gx_mode = "DLT";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         EndLevel0I18( ) ;
         Gx_mode = sMode18;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
      }

      protected void OnDeleteControls0I18( )
      {
         standaloneModal( ) ;
         /* No delete mode formulas found. */
      }

      protected void EndLevel0I18( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
         {
            pr_datastore1.close(0);
         }
         if ( AnyError == 0 )
         {
            BeforeComplete0I18( ) ;
         }
         if ( AnyError == 0 )
         {
            pr_datastore1.close(1);
            context.CommitDataStores("fyactividadplancantpart",pr_default);
            if ( AnyError == 0 )
            {
               ConfirmValues0I0( ) ;
            }
            /* After transaction rules */
            /* Execute 'After Trn' event if defined. */
            trnEnded = 1;
         }
         else
         {
            pr_datastore1.close(1);
            context.RollbackDataStores("fyactividadplancantpart",pr_default);
         }
         IsModified = 0;
         if ( AnyError != 0 )
         {
            context.wjLoc = "";
            context.nUserReturn = 0;
         }
      }

      public void ScanStart0I18( )
      {
         /* Using cursor T000I13 */
         pr_datastore1.execute(11);
         RcdFound18 = 0;
         if ( (pr_datastore1.getStatus(11) != 101) )
         {
            RcdFound18 = 1;
            A14FYActividadPlanCantPartid = T000I13_A14FYActividadPlanCantPartid[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A14FYActividadPlanCantPartid", StringUtil.LTrim( StringUtil.Str( (decimal)(A14FYActividadPlanCantPartid), 9, 0)));
         }
         /* Load Subordinate Levels */
      }

      protected void ScanNext0I18( )
      {
         /* Scan next routine */
         pr_datastore1.readNext(11);
         RcdFound18 = 0;
         if ( (pr_datastore1.getStatus(11) != 101) )
         {
            RcdFound18 = 1;
            A14FYActividadPlanCantPartid = T000I13_A14FYActividadPlanCantPartid[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A14FYActividadPlanCantPartid", StringUtil.LTrim( StringUtil.Str( (decimal)(A14FYActividadPlanCantPartid), 9, 0)));
         }
      }

      protected void ScanEnd0I18( )
      {
         pr_datastore1.close(11);
      }

      protected void AfterConfirm0I18( )
      {
         /* After Confirm Rules */
      }

      protected void BeforeInsert0I18( )
      {
         /* Before Insert Rules */
      }

      protected void BeforeUpdate0I18( )
      {
         /* Before Update Rules */
      }

      protected void BeforeDelete0I18( )
      {
         /* Before Delete Rules */
      }

      protected void BeforeComplete0I18( )
      {
         /* Before Complete Rules */
      }

      protected void BeforeValidate0I18( )
      {
         /* Before Validate Rules */
      }

      protected void DisableAttributes0I18( )
      {
         edtFYActividadPlanCantPartid_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtFYActividadPlanCantPartid_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtFYActividadPlanCantPartid_Enabled), 5, 0)), true);
         edtActividadParaPlanid_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtActividadParaPlanid_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtActividadParaPlanid_Enabled), 5, 0)), true);
         edtFYActividadPlanCantPartFY_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtFYActividadPlanCantPartFY_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtFYActividadPlanCantPartFY_Enabled), 5, 0)), true);
         edtmes1H_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtmes1H_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtmes1H_Enabled), 5, 0)), true);
         edtmes1M_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtmes1M_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtmes1M_Enabled), 5, 0)), true);
         edtmes2H_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtmes2H_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtmes2H_Enabled), 5, 0)), true);
         edtmes2M_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtmes2M_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtmes2M_Enabled), 5, 0)), true);
         edtmes3H_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtmes3H_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtmes3H_Enabled), 5, 0)), true);
         edtmes3M_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtmes3M_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtmes3M_Enabled), 5, 0)), true);
         edtmes4H_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtmes4H_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtmes4H_Enabled), 5, 0)), true);
         edtmes4M_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtmes4M_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtmes4M_Enabled), 5, 0)), true);
         edtmes5H_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtmes5H_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtmes5H_Enabled), 5, 0)), true);
         edtmes5M_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtmes5M_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtmes5M_Enabled), 5, 0)), true);
         edtmes6H_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtmes6H_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtmes6H_Enabled), 5, 0)), true);
         edtmes6M_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtmes6M_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtmes6M_Enabled), 5, 0)), true);
         edtmes7H_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtmes7H_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtmes7H_Enabled), 5, 0)), true);
         edtmes7M_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtmes7M_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtmes7M_Enabled), 5, 0)), true);
         edtmes8H_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtmes8H_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtmes8H_Enabled), 5, 0)), true);
         edtmes8M_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtmes8M_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtmes8M_Enabled), 5, 0)), true);
         edtmes9H_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtmes9H_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtmes9H_Enabled), 5, 0)), true);
         edtmes9M_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtmes9M_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtmes9M_Enabled), 5, 0)), true);
         edtmes10H_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtmes10H_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtmes10H_Enabled), 5, 0)), true);
         edtmes10M_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtmes10M_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtmes10M_Enabled), 5, 0)), true);
         edtmes11H_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtmes11H_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtmes11H_Enabled), 5, 0)), true);
         edtmes11M_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtmes11M_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtmes11M_Enabled), 5, 0)), true);
         edtmes12H_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtmes12H_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtmes12H_Enabled), 5, 0)), true);
         edtmes12M_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtmes12M_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtmes12M_Enabled), 5, 0)), true);
      }

      protected void send_integrity_lvl_hashes0I18( )
      {
      }

      protected void assign_properties_default( )
      {
      }

      protected void ConfirmValues0I0( )
      {
      }

      public override void RenderHtmlHeaders( )
      {
         GxWebStd.gx_html_headers( context, 0, "", "", Form.Meta, Form.Metaequiv, true);
      }

      public override void RenderHtmlOpenForm( )
      {
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.WriteHtmlText( "<title>") ;
         context.SendWebValue( Form.Caption) ;
         context.WriteHtmlTextNl( "</title>") ;
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         if ( StringUtil.Len( sDynURL) > 0 )
         {
            context.WriteHtmlText( "<BASE href=\""+sDynURL+"\" />") ;
         }
         define_styles( ) ;
         MasterPageObj.master_styles();
         if ( ( ( context.GetBrowserType( ) == 1 ) || ( context.GetBrowserType( ) == 5 ) ) && ( StringUtil.StrCmp(context.GetBrowserVersion( ), "7.0") == 0 ) )
         {
            context.AddJavascriptSource("json2.js", "?"+context.GetBuildNumber( 127771), false);
         }
         context.AddJavascriptSource("jquery.js", "?"+context.GetBuildNumber( 127771), false);
         context.AddJavascriptSource("gxtimezone.js", "?"+context.GetBuildNumber( 127771), false);
         context.AddJavascriptSource("gxgral.js", "?"+context.GetBuildNumber( 127771), false);
         context.AddJavascriptSource("gxcfg.js", "?2019131815219", false);
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.WriteHtmlText( Form.Headerrawhtml) ;
         context.CloseHtmlHeader();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         FormProcess = " data-HasEnter=\"true\" data-Skiponenter=\"false\"";
         context.WriteHtmlText( "<body ") ;
         bodyStyle = "" + "background-color:" + context.BuildHTMLColor( Form.Backcolor) + ";color:" + context.BuildHTMLColor( Form.Textcolor) + ";";
         bodyStyle = bodyStyle + "-moz-opacity:0;opacity:0;";
         if ( ! ( String.IsNullOrEmpty(StringUtil.RTrim( Form.Background)) ) )
         {
            bodyStyle = bodyStyle + " background-image:url(" + context.convertURL( Form.Background) + ")";
         }
         context.WriteHtmlText( " "+"class=\"form-horizontal Form\""+" "+ "style='"+bodyStyle+"'") ;
         context.WriteHtmlText( FormProcess+">") ;
         context.skipLines(1);
         context.WriteHtmlTextNl( "<form id=\"MAINFORM\" autocomplete=\"off\" name=\"MAINFORM\" method=\"post\" tabindex=-1  class=\"form-horizontal Form\" data-gx-class=\"form-horizontal Form\" novalidate action=\""+formatLink("fyactividadplancantpart.aspx") +"\">") ;
         GxWebStd.gx_hidden_field( context, "_EventName", "");
         GxWebStd.gx_hidden_field( context, "_EventGridId", "");
         GxWebStd.gx_hidden_field( context, "_EventRowId", "");
         context.WriteHtmlText( "<input type=\"submit\" title=\"submit\" style=\"display:none\" disabled>") ;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, "FORM", "Class", "form-horizontal Form", true);
         toggleJsOutput = isJsOutputEnabled( );
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
      }

      protected void send_integrity_footer_hashes( )
      {
         GXKey = Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
      }

      protected void SendCloseFormHiddens( )
      {
         /* Send hidden variables. */
         /* Send saved values. */
         send_integrity_footer_hashes( ) ;
         GxWebStd.gx_hidden_field( context, "Z14FYActividadPlanCantPartid", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z14FYActividadPlanCantPartid), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "Z79FYActividadPlanCantPartFY", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z79FYActividadPlanCantPartFY), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "Z80mes1H", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z80mes1H), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "Z81mes1M", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z81mes1M), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "Z82mes2H", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z82mes2H), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "Z83mes2M", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z83mes2M), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "Z84mes3H", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z84mes3H), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "Z85mes3M", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z85mes3M), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "Z86mes4H", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z86mes4H), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "Z87mes4M", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z87mes4M), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "Z88mes5H", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z88mes5H), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "Z89mes5M", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z89mes5M), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "Z90mes6H", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z90mes6H), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "Z91mes6M", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z91mes6M), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "Z92mes7H", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z92mes7H), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "Z93mes7M", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z93mes7M), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "Z94mes8H", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z94mes8H), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "Z95mes8M", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z95mes8M), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "Z96mes9H", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z96mes9H), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "Z97mes9M", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z97mes9M), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "Z98mes10H", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z98mes10H), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "Z99mes10M", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z99mes10M), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "Z100mes11H", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z100mes11H), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "Z101mes11M", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z101mes11M), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "Z102mes12H", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z102mes12H), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "Z103mes12M", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z103mes12M), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "Z23ActividadParaPlanid", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z23ActividadParaPlanid), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "IsConfirmed", StringUtil.LTrim( StringUtil.NToC( (decimal)(IsConfirmed), 4, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "IsModified", StringUtil.LTrim( StringUtil.NToC( (decimal)(IsModified), 4, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "Mode", StringUtil.RTrim( Gx_mode));
         GxWebStd.gx_hidden_field( context, "vMODE", StringUtil.RTrim( Gx_mode));
      }

      public override void RenderHtmlCloseForm( )
      {
         SendCloseFormHiddens( ) ;
         GxWebStd.gx_hidden_field( context, "GX_FocusControl", GX_FocusControl);
         SendAjaxEncryptionKey();
         SendSecurityToken(sPrefix);
         SendComponentObjects();
         SendServerCommands();
         SendState();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         context.WriteHtmlTextNl( "</form>") ;
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         include_jscripts( ) ;
      }

      public override short ExecuteStartEvent( )
      {
         standaloneStartup( ) ;
         gxajaxcallmode = (short)((isAjaxCallMode( ) ? 1 : 0));
         return gxajaxcallmode ;
      }

      public override void RenderHtmlContent( )
      {
         context.WriteHtmlText( "<div") ;
         GxWebStd.ClassAttribute( context, "gx-ct-body"+" "+(String.IsNullOrEmpty(StringUtil.RTrim( Form.Class)) ? "form-horizontal Form" : Form.Class)+"-fx");
         context.WriteHtmlText( ">") ;
         Draw( ) ;
         context.WriteHtmlText( "</div>") ;
      }

      public override void DispatchEvents( )
      {
         Process( ) ;
      }

      public override bool HasEnterEvent( )
      {
         return true ;
      }

      public override GXWebForm GetForm( )
      {
         return Form ;
      }

      public override String GetSelfLink( )
      {
         return formatLink("fyactividadplancantpart.aspx")  ;
      }

      public override String GetPgmname( )
      {
         return "FYActividadPlanCantPart" ;
      }

      public override String GetPgmdesc( )
      {
         return "FYActividad Plan Cant Part" ;
      }

      protected void InitializeNonKey0I18( )
      {
         A23ActividadParaPlanid = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A23ActividadParaPlanid", StringUtil.LTrim( StringUtil.Str( (decimal)(A23ActividadParaPlanid), 9, 0)));
         A79FYActividadPlanCantPartFY = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A79FYActividadPlanCantPartFY", StringUtil.LTrim( StringUtil.Str( (decimal)(A79FYActividadPlanCantPartFY), 9, 0)));
         A80mes1H = 0;
         n80mes1H = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A80mes1H", StringUtil.LTrim( StringUtil.Str( (decimal)(A80mes1H), 9, 0)));
         n80mes1H = ((0==A80mes1H) ? true : false);
         A81mes1M = 0;
         n81mes1M = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A81mes1M", StringUtil.LTrim( StringUtil.Str( (decimal)(A81mes1M), 9, 0)));
         n81mes1M = ((0==A81mes1M) ? true : false);
         A82mes2H = 0;
         n82mes2H = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A82mes2H", StringUtil.LTrim( StringUtil.Str( (decimal)(A82mes2H), 9, 0)));
         n82mes2H = ((0==A82mes2H) ? true : false);
         A83mes2M = 0;
         n83mes2M = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A83mes2M", StringUtil.LTrim( StringUtil.Str( (decimal)(A83mes2M), 9, 0)));
         n83mes2M = ((0==A83mes2M) ? true : false);
         A84mes3H = 0;
         n84mes3H = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A84mes3H", StringUtil.LTrim( StringUtil.Str( (decimal)(A84mes3H), 9, 0)));
         n84mes3H = ((0==A84mes3H) ? true : false);
         A85mes3M = 0;
         n85mes3M = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A85mes3M", StringUtil.LTrim( StringUtil.Str( (decimal)(A85mes3M), 9, 0)));
         n85mes3M = ((0==A85mes3M) ? true : false);
         A86mes4H = 0;
         n86mes4H = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A86mes4H", StringUtil.LTrim( StringUtil.Str( (decimal)(A86mes4H), 9, 0)));
         n86mes4H = ((0==A86mes4H) ? true : false);
         A87mes4M = 0;
         n87mes4M = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A87mes4M", StringUtil.LTrim( StringUtil.Str( (decimal)(A87mes4M), 9, 0)));
         n87mes4M = ((0==A87mes4M) ? true : false);
         A88mes5H = 0;
         n88mes5H = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A88mes5H", StringUtil.LTrim( StringUtil.Str( (decimal)(A88mes5H), 9, 0)));
         n88mes5H = ((0==A88mes5H) ? true : false);
         A89mes5M = 0;
         n89mes5M = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A89mes5M", StringUtil.LTrim( StringUtil.Str( (decimal)(A89mes5M), 9, 0)));
         n89mes5M = ((0==A89mes5M) ? true : false);
         A90mes6H = 0;
         n90mes6H = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A90mes6H", StringUtil.LTrim( StringUtil.Str( (decimal)(A90mes6H), 9, 0)));
         n90mes6H = ((0==A90mes6H) ? true : false);
         A91mes6M = 0;
         n91mes6M = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A91mes6M", StringUtil.LTrim( StringUtil.Str( (decimal)(A91mes6M), 9, 0)));
         n91mes6M = ((0==A91mes6M) ? true : false);
         A92mes7H = 0;
         n92mes7H = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A92mes7H", StringUtil.LTrim( StringUtil.Str( (decimal)(A92mes7H), 9, 0)));
         n92mes7H = ((0==A92mes7H) ? true : false);
         A93mes7M = 0;
         n93mes7M = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A93mes7M", StringUtil.LTrim( StringUtil.Str( (decimal)(A93mes7M), 9, 0)));
         n93mes7M = ((0==A93mes7M) ? true : false);
         A94mes8H = 0;
         n94mes8H = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A94mes8H", StringUtil.LTrim( StringUtil.Str( (decimal)(A94mes8H), 9, 0)));
         n94mes8H = ((0==A94mes8H) ? true : false);
         A95mes8M = 0;
         n95mes8M = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A95mes8M", StringUtil.LTrim( StringUtil.Str( (decimal)(A95mes8M), 9, 0)));
         n95mes8M = ((0==A95mes8M) ? true : false);
         A96mes9H = 0;
         n96mes9H = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A96mes9H", StringUtil.LTrim( StringUtil.Str( (decimal)(A96mes9H), 9, 0)));
         n96mes9H = ((0==A96mes9H) ? true : false);
         A97mes9M = 0;
         n97mes9M = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A97mes9M", StringUtil.LTrim( StringUtil.Str( (decimal)(A97mes9M), 9, 0)));
         n97mes9M = ((0==A97mes9M) ? true : false);
         A98mes10H = 0;
         n98mes10H = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A98mes10H", StringUtil.LTrim( StringUtil.Str( (decimal)(A98mes10H), 9, 0)));
         n98mes10H = ((0==A98mes10H) ? true : false);
         A99mes10M = 0;
         n99mes10M = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A99mes10M", StringUtil.LTrim( StringUtil.Str( (decimal)(A99mes10M), 9, 0)));
         n99mes10M = ((0==A99mes10M) ? true : false);
         A100mes11H = 0;
         n100mes11H = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A100mes11H", StringUtil.LTrim( StringUtil.Str( (decimal)(A100mes11H), 9, 0)));
         n100mes11H = ((0==A100mes11H) ? true : false);
         A101mes11M = 0;
         n101mes11M = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A101mes11M", StringUtil.LTrim( StringUtil.Str( (decimal)(A101mes11M), 9, 0)));
         n101mes11M = ((0==A101mes11M) ? true : false);
         A102mes12H = 0;
         n102mes12H = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A102mes12H", StringUtil.LTrim( StringUtil.Str( (decimal)(A102mes12H), 9, 0)));
         n102mes12H = ((0==A102mes12H) ? true : false);
         A103mes12M = 0;
         n103mes12M = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A103mes12M", StringUtil.LTrim( StringUtil.Str( (decimal)(A103mes12M), 9, 0)));
         n103mes12M = ((0==A103mes12M) ? true : false);
         Z79FYActividadPlanCantPartFY = 0;
         Z80mes1H = 0;
         Z81mes1M = 0;
         Z82mes2H = 0;
         Z83mes2M = 0;
         Z84mes3H = 0;
         Z85mes3M = 0;
         Z86mes4H = 0;
         Z87mes4M = 0;
         Z88mes5H = 0;
         Z89mes5M = 0;
         Z90mes6H = 0;
         Z91mes6M = 0;
         Z92mes7H = 0;
         Z93mes7M = 0;
         Z94mes8H = 0;
         Z95mes8M = 0;
         Z96mes9H = 0;
         Z97mes9M = 0;
         Z98mes10H = 0;
         Z99mes10M = 0;
         Z100mes11H = 0;
         Z101mes11M = 0;
         Z102mes12H = 0;
         Z103mes12M = 0;
         Z23ActividadParaPlanid = 0;
      }

      protected void InitAll0I18( )
      {
         A14FYActividadPlanCantPartid = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A14FYActividadPlanCantPartid", StringUtil.LTrim( StringUtil.Str( (decimal)(A14FYActividadPlanCantPartid), 9, 0)));
         InitializeNonKey0I18( ) ;
      }

      protected void StandaloneModalInsert( )
      {
      }

      protected void define_styles( )
      {
         AddThemeStyleSheetFile("", context.GetTheme( )+".css", "?"+GetCacheInvalidationToken( ));
         bool outputEnabled = isOutputEnabled( ) ;
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         idxLst = 1;
         while ( idxLst <= Form.Jscriptsrc.Count )
         {
            context.AddJavascriptSource(StringUtil.RTrim( ((String)Form.Jscriptsrc.Item(idxLst))), "?2019131815248", true);
            idxLst = (int)(idxLst+1);
         }
         if ( ! outputEnabled )
         {
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
         }
         /* End function define_styles */
      }

      protected void include_jscripts( )
      {
         context.AddJavascriptSource("messages.eng.js", "?"+GetCacheInvalidationToken( ), false);
         context.AddJavascriptSource("fyactividadplancantpart.js", "?2019131815248", false);
         /* End function include_jscripts */
      }

      protected void init_default_properties( )
      {
         lblTitle_Internalname = "TITLE";
         divTitlecontainer_Internalname = "TITLECONTAINER";
         bttBtn_first_Internalname = "BTN_FIRST";
         bttBtn_previous_Internalname = "BTN_PREVIOUS";
         bttBtn_next_Internalname = "BTN_NEXT";
         bttBtn_last_Internalname = "BTN_LAST";
         bttBtn_select_Internalname = "BTN_SELECT";
         divToolbarcell_Internalname = "TOOLBARCELL";
         edtFYActividadPlanCantPartid_Internalname = "FYACTIVIDADPLANCANTPARTID";
         edtActividadParaPlanid_Internalname = "ACTIVIDADPARAPLANID";
         edtFYActividadPlanCantPartFY_Internalname = "FYACTIVIDADPLANCANTPARTFY";
         edtmes1H_Internalname = "MES1H";
         edtmes1M_Internalname = "MES1M";
         edtmes2H_Internalname = "MES2H";
         edtmes2M_Internalname = "MES2M";
         edtmes3H_Internalname = "MES3H";
         edtmes3M_Internalname = "MES3M";
         edtmes4H_Internalname = "MES4H";
         edtmes4M_Internalname = "MES4M";
         edtmes5H_Internalname = "MES5H";
         edtmes5M_Internalname = "MES5M";
         edtmes6H_Internalname = "MES6H";
         edtmes6M_Internalname = "MES6M";
         edtmes7H_Internalname = "MES7H";
         edtmes7M_Internalname = "MES7M";
         edtmes8H_Internalname = "MES8H";
         edtmes8M_Internalname = "MES8M";
         edtmes9H_Internalname = "MES9H";
         edtmes9M_Internalname = "MES9M";
         edtmes10H_Internalname = "MES10H";
         edtmes10M_Internalname = "MES10M";
         edtmes11H_Internalname = "MES11H";
         edtmes11M_Internalname = "MES11M";
         edtmes12H_Internalname = "MES12H";
         edtmes12M_Internalname = "MES12M";
         divFormcontainer_Internalname = "FORMCONTAINER";
         bttBtn_enter_Internalname = "BTN_ENTER";
         bttBtn_cancel_Internalname = "BTN_CANCEL";
         bttBtn_delete_Internalname = "BTN_DELETE";
         divMaintable_Internalname = "MAINTABLE";
         Form.Internalname = "FORM";
         imgprompt_23_Internalname = "PROMPT_23";
      }

      public override void initialize_properties( )
      {
         context.SetDefaultTheme("Carmine");
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         init_default_properties( ) ;
         Form.Headerrawhtml = "";
         Form.Background = "";
         Form.Textcolor = 0;
         Form.Backcolor = (int)(0xFFFFFF);
         Form.Caption = "FYActividad Plan Cant Part";
         bttBtn_delete_Enabled = 1;
         bttBtn_delete_Visible = 1;
         bttBtn_cancel_Visible = 1;
         bttBtn_enter_Enabled = 1;
         bttBtn_enter_Visible = 1;
         edtmes12M_Jsonclick = "";
         edtmes12M_Enabled = 1;
         edtmes12H_Jsonclick = "";
         edtmes12H_Enabled = 1;
         edtmes11M_Jsonclick = "";
         edtmes11M_Enabled = 1;
         edtmes11H_Jsonclick = "";
         edtmes11H_Enabled = 1;
         edtmes10M_Jsonclick = "";
         edtmes10M_Enabled = 1;
         edtmes10H_Jsonclick = "";
         edtmes10H_Enabled = 1;
         edtmes9M_Jsonclick = "";
         edtmes9M_Enabled = 1;
         edtmes9H_Jsonclick = "";
         edtmes9H_Enabled = 1;
         edtmes8M_Jsonclick = "";
         edtmes8M_Enabled = 1;
         edtmes8H_Jsonclick = "";
         edtmes8H_Enabled = 1;
         edtmes7M_Jsonclick = "";
         edtmes7M_Enabled = 1;
         edtmes7H_Jsonclick = "";
         edtmes7H_Enabled = 1;
         edtmes6M_Jsonclick = "";
         edtmes6M_Enabled = 1;
         edtmes6H_Jsonclick = "";
         edtmes6H_Enabled = 1;
         edtmes5M_Jsonclick = "";
         edtmes5M_Enabled = 1;
         edtmes5H_Jsonclick = "";
         edtmes5H_Enabled = 1;
         edtmes4M_Jsonclick = "";
         edtmes4M_Enabled = 1;
         edtmes4H_Jsonclick = "";
         edtmes4H_Enabled = 1;
         edtmes3M_Jsonclick = "";
         edtmes3M_Enabled = 1;
         edtmes3H_Jsonclick = "";
         edtmes3H_Enabled = 1;
         edtmes2M_Jsonclick = "";
         edtmes2M_Enabled = 1;
         edtmes2H_Jsonclick = "";
         edtmes2H_Enabled = 1;
         edtmes1M_Jsonclick = "";
         edtmes1M_Enabled = 1;
         edtmes1H_Jsonclick = "";
         edtmes1H_Enabled = 1;
         edtFYActividadPlanCantPartFY_Jsonclick = "";
         edtFYActividadPlanCantPartFY_Enabled = 1;
         imgprompt_23_Visible = 1;
         imgprompt_23_Link = "";
         edtActividadParaPlanid_Jsonclick = "";
         edtActividadParaPlanid_Enabled = 1;
         edtFYActividadPlanCantPartid_Jsonclick = "";
         edtFYActividadPlanCantPartid_Enabled = 1;
         bttBtn_select_Visible = 1;
         bttBtn_last_Visible = 1;
         bttBtn_next_Visible = 1;
         bttBtn_previous_Visible = 1;
         bttBtn_first_Visible = 1;
         context.GX_msglist.DisplayMode = 1;
         if ( context.isSpaRequest( ) )
         {
            enableJsOutput();
         }
      }

      protected void dynload_actions( )
      {
         /* End function dynload_actions */
      }

      protected void init_web_controls( )
      {
         /* End function init_web_controls */
      }

      protected void AfterKeyLoadScreen( )
      {
         IsConfirmed = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "IsConfirmed", StringUtil.LTrim( StringUtil.Str( (decimal)(IsConfirmed), 4, 0)));
         getEqualNoModal( ) ;
         GX_FocusControl = edtActividadParaPlanid_Internalname;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         standaloneNotModal( ) ;
         standaloneModal( ) ;
         /* End function AfterKeyLoadScreen */
      }

      public void Valid_Fyactividadplancantpartid( int GX_Parm1 ,
                                                   int GX_Parm2 ,
                                                   int GX_Parm3 ,
                                                   int GX_Parm4 ,
                                                   int GX_Parm5 ,
                                                   int GX_Parm6 ,
                                                   int GX_Parm7 ,
                                                   int GX_Parm8 ,
                                                   int GX_Parm9 ,
                                                   int GX_Parm10 ,
                                                   int GX_Parm11 ,
                                                   int GX_Parm12 ,
                                                   int GX_Parm13 ,
                                                   int GX_Parm14 ,
                                                   int GX_Parm15 ,
                                                   int GX_Parm16 ,
                                                   int GX_Parm17 ,
                                                   int GX_Parm18 ,
                                                   int GX_Parm19 ,
                                                   int GX_Parm20 ,
                                                   int GX_Parm21 ,
                                                   int GX_Parm22 ,
                                                   int GX_Parm23 ,
                                                   int GX_Parm24 ,
                                                   int GX_Parm25 ,
                                                   int GX_Parm26 ,
                                                   int GX_Parm27 )
      {
         A14FYActividadPlanCantPartid = GX_Parm1;
         A79FYActividadPlanCantPartFY = GX_Parm2;
         A80mes1H = GX_Parm3;
         n80mes1H = false;
         A81mes1M = GX_Parm4;
         n81mes1M = false;
         A82mes2H = GX_Parm5;
         n82mes2H = false;
         A83mes2M = GX_Parm6;
         n83mes2M = false;
         A84mes3H = GX_Parm7;
         n84mes3H = false;
         A85mes3M = GX_Parm8;
         n85mes3M = false;
         A86mes4H = GX_Parm9;
         n86mes4H = false;
         A87mes4M = GX_Parm10;
         n87mes4M = false;
         A88mes5H = GX_Parm11;
         n88mes5H = false;
         A89mes5M = GX_Parm12;
         n89mes5M = false;
         A90mes6H = GX_Parm13;
         n90mes6H = false;
         A91mes6M = GX_Parm14;
         n91mes6M = false;
         A92mes7H = GX_Parm15;
         n92mes7H = false;
         A93mes7M = GX_Parm16;
         n93mes7M = false;
         A94mes8H = GX_Parm17;
         n94mes8H = false;
         A95mes8M = GX_Parm18;
         n95mes8M = false;
         A96mes9H = GX_Parm19;
         n96mes9H = false;
         A97mes9M = GX_Parm20;
         n97mes9M = false;
         A98mes10H = GX_Parm21;
         n98mes10H = false;
         A99mes10M = GX_Parm22;
         n99mes10M = false;
         A100mes11H = GX_Parm23;
         n100mes11H = false;
         A101mes11M = GX_Parm24;
         n101mes11M = false;
         A102mes12H = GX_Parm25;
         n102mes12H = false;
         A103mes12M = GX_Parm26;
         n103mes12M = false;
         A23ActividadParaPlanid = GX_Parm27;
         context.wbHandled = 1;
         AfterKeyLoadScreen( ) ;
         Draw( ) ;
         send_integrity_footer_hashes( ) ;
         dynload_actions( ) ;
         if ( AnyError == 1 )
         {
         }
         isValidOutput.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(A23ActividadParaPlanid), 9, 0, ".", "")));
         isValidOutput.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(A79FYActividadPlanCantPartFY), 9, 0, ".", "")));
         isValidOutput.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(A80mes1H), 9, 0, ".", "")));
         isValidOutput.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(A81mes1M), 9, 0, ".", "")));
         isValidOutput.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(A82mes2H), 9, 0, ".", "")));
         isValidOutput.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(A83mes2M), 9, 0, ".", "")));
         isValidOutput.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(A84mes3H), 9, 0, ".", "")));
         isValidOutput.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(A85mes3M), 9, 0, ".", "")));
         isValidOutput.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(A86mes4H), 9, 0, ".", "")));
         isValidOutput.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(A87mes4M), 9, 0, ".", "")));
         isValidOutput.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(A88mes5H), 9, 0, ".", "")));
         isValidOutput.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(A89mes5M), 9, 0, ".", "")));
         isValidOutput.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(A90mes6H), 9, 0, ".", "")));
         isValidOutput.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(A91mes6M), 9, 0, ".", "")));
         isValidOutput.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(A92mes7H), 9, 0, ".", "")));
         isValidOutput.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(A93mes7M), 9, 0, ".", "")));
         isValidOutput.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(A94mes8H), 9, 0, ".", "")));
         isValidOutput.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(A95mes8M), 9, 0, ".", "")));
         isValidOutput.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(A96mes9H), 9, 0, ".", "")));
         isValidOutput.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(A97mes9M), 9, 0, ".", "")));
         isValidOutput.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(A98mes10H), 9, 0, ".", "")));
         isValidOutput.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(A99mes10M), 9, 0, ".", "")));
         isValidOutput.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(A100mes11H), 9, 0, ".", "")));
         isValidOutput.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(A101mes11M), 9, 0, ".", "")));
         isValidOutput.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(A102mes12H), 9, 0, ".", "")));
         isValidOutput.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(A103mes12M), 9, 0, ".", "")));
         isValidOutput.Add(StringUtil.RTrim( Gx_mode));
         isValidOutput.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(Z14FYActividadPlanCantPartid), 9, 0, ".", "")));
         isValidOutput.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(Z23ActividadParaPlanid), 9, 0, ".", "")));
         isValidOutput.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(Z79FYActividadPlanCantPartFY), 9, 0, ".", "")));
         isValidOutput.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(Z80mes1H), 9, 0, ".", "")));
         isValidOutput.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(Z81mes1M), 9, 0, ".", "")));
         isValidOutput.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(Z82mes2H), 9, 0, ".", "")));
         isValidOutput.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(Z83mes2M), 9, 0, ".", "")));
         isValidOutput.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(Z84mes3H), 9, 0, ".", "")));
         isValidOutput.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(Z85mes3M), 9, 0, ".", "")));
         isValidOutput.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(Z86mes4H), 9, 0, ".", "")));
         isValidOutput.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(Z87mes4M), 9, 0, ".", "")));
         isValidOutput.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(Z88mes5H), 9, 0, ".", "")));
         isValidOutput.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(Z89mes5M), 9, 0, ".", "")));
         isValidOutput.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(Z90mes6H), 9, 0, ".", "")));
         isValidOutput.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(Z91mes6M), 9, 0, ".", "")));
         isValidOutput.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(Z92mes7H), 9, 0, ".", "")));
         isValidOutput.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(Z93mes7M), 9, 0, ".", "")));
         isValidOutput.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(Z94mes8H), 9, 0, ".", "")));
         isValidOutput.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(Z95mes8M), 9, 0, ".", "")));
         isValidOutput.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(Z96mes9H), 9, 0, ".", "")));
         isValidOutput.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(Z97mes9M), 9, 0, ".", "")));
         isValidOutput.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(Z98mes10H), 9, 0, ".", "")));
         isValidOutput.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(Z99mes10M), 9, 0, ".", "")));
         isValidOutput.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(Z100mes11H), 9, 0, ".", "")));
         isValidOutput.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(Z101mes11M), 9, 0, ".", "")));
         isValidOutput.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(Z102mes12H), 9, 0, ".", "")));
         isValidOutput.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(Z103mes12M), 9, 0, ".", "")));
         isValidOutput.Add(bttBtn_delete_Enabled);
         isValidOutput.Add(bttBtn_enter_Enabled);
         isValidOutput.Add(context.GX_msglist.ToJavascriptSource());
         isValidOutput.Add(context.httpAjaxContext.ajax_rsp_get_hiddens( ));
         context.GX_webresponse.AddString(isValidOutput.ToJSonString());
         wbTemp = context.ResponseContentType( "application/json");
      }

      public void Valid_Actividadparaplanid( int GX_Parm1 )
      {
         A23ActividadParaPlanid = GX_Parm1;
         /* Using cursor T000I14 */
         pr_datastore1.execute(12, new Object[] {A23ActividadParaPlanid});
         if ( (pr_datastore1.getStatus(12) == 101) )
         {
            GX_msglist.addItem("No matching 'Actividad Para Plan'.", "ForeignKeyNotFound", 1, "ACTIVIDADPARAPLANID");
            AnyError = 1;
            GX_FocusControl = edtActividadParaPlanid_Internalname;
         }
         pr_datastore1.close(12);
         dynload_actions( ) ;
         isValidOutput.Add(context.GX_msglist.ToJavascriptSource());
         isValidOutput.Add(context.httpAjaxContext.ajax_rsp_get_hiddens( ));
         context.GX_webresponse.AddString(isValidOutput.ToJSonString());
         wbTemp = context.ResponseContentType( "application/json");
      }

      public override bool SupportAjaxEvent( )
      {
         return true ;
      }

      public override void InitializeDynEvents( )
      {
         setEventMetadata("ENTER","{handler:'UserMainFullajax',iparms:[{postForm:true}]");
         setEventMetadata("ENTER",",oparms:[]}");
         setEventMetadata("REFRESH","{handler:'Refresh',iparms:[]");
         setEventMetadata("REFRESH",",oparms:[]}");
         return  ;
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
         pr_datastore1.close(1);
         pr_datastore1.close(12);
      }

      public override void initialize( )
      {
         sPrefix = "";
         scmdbuf = "";
         gxfirstwebparm = "";
         gxfirstwebparm_bkp = "";
         GXKey = "";
         PreviousTooltip = "";
         PreviousCaption = "";
         Form = new GXWebForm();
         GX_FocusControl = "";
         lblTitle_Jsonclick = "";
         ClassString = "";
         StyleString = "";
         TempTags = "";
         bttBtn_first_Jsonclick = "";
         bttBtn_previous_Jsonclick = "";
         bttBtn_next_Jsonclick = "";
         bttBtn_last_Jsonclick = "";
         bttBtn_select_Jsonclick = "";
         sImgUrl = "";
         bttBtn_enter_Jsonclick = "";
         bttBtn_cancel_Jsonclick = "";
         bttBtn_delete_Jsonclick = "";
         Gx_mode = "";
         sEvt = "";
         EvtGridId = "";
         EvtRowId = "";
         sEvtType = "";
         T000I5_A14FYActividadPlanCantPartid = new int[1] ;
         T000I5_A79FYActividadPlanCantPartFY = new int[1] ;
         T000I5_A80mes1H = new int[1] ;
         T000I5_n80mes1H = new bool[] {false} ;
         T000I5_A81mes1M = new int[1] ;
         T000I5_n81mes1M = new bool[] {false} ;
         T000I5_A82mes2H = new int[1] ;
         T000I5_n82mes2H = new bool[] {false} ;
         T000I5_A83mes2M = new int[1] ;
         T000I5_n83mes2M = new bool[] {false} ;
         T000I5_A84mes3H = new int[1] ;
         T000I5_n84mes3H = new bool[] {false} ;
         T000I5_A85mes3M = new int[1] ;
         T000I5_n85mes3M = new bool[] {false} ;
         T000I5_A86mes4H = new int[1] ;
         T000I5_n86mes4H = new bool[] {false} ;
         T000I5_A87mes4M = new int[1] ;
         T000I5_n87mes4M = new bool[] {false} ;
         T000I5_A88mes5H = new int[1] ;
         T000I5_n88mes5H = new bool[] {false} ;
         T000I5_A89mes5M = new int[1] ;
         T000I5_n89mes5M = new bool[] {false} ;
         T000I5_A90mes6H = new int[1] ;
         T000I5_n90mes6H = new bool[] {false} ;
         T000I5_A91mes6M = new int[1] ;
         T000I5_n91mes6M = new bool[] {false} ;
         T000I5_A92mes7H = new int[1] ;
         T000I5_n92mes7H = new bool[] {false} ;
         T000I5_A93mes7M = new int[1] ;
         T000I5_n93mes7M = new bool[] {false} ;
         T000I5_A94mes8H = new int[1] ;
         T000I5_n94mes8H = new bool[] {false} ;
         T000I5_A95mes8M = new int[1] ;
         T000I5_n95mes8M = new bool[] {false} ;
         T000I5_A96mes9H = new int[1] ;
         T000I5_n96mes9H = new bool[] {false} ;
         T000I5_A97mes9M = new int[1] ;
         T000I5_n97mes9M = new bool[] {false} ;
         T000I5_A98mes10H = new int[1] ;
         T000I5_n98mes10H = new bool[] {false} ;
         T000I5_A99mes10M = new int[1] ;
         T000I5_n99mes10M = new bool[] {false} ;
         T000I5_A100mes11H = new int[1] ;
         T000I5_n100mes11H = new bool[] {false} ;
         T000I5_A101mes11M = new int[1] ;
         T000I5_n101mes11M = new bool[] {false} ;
         T000I5_A102mes12H = new int[1] ;
         T000I5_n102mes12H = new bool[] {false} ;
         T000I5_A103mes12M = new int[1] ;
         T000I5_n103mes12M = new bool[] {false} ;
         T000I5_A23ActividadParaPlanid = new int[1] ;
         T000I4_A23ActividadParaPlanid = new int[1] ;
         T000I6_A23ActividadParaPlanid = new int[1] ;
         T000I7_A14FYActividadPlanCantPartid = new int[1] ;
         T000I3_A14FYActividadPlanCantPartid = new int[1] ;
         T000I3_A79FYActividadPlanCantPartFY = new int[1] ;
         T000I3_A80mes1H = new int[1] ;
         T000I3_n80mes1H = new bool[] {false} ;
         T000I3_A81mes1M = new int[1] ;
         T000I3_n81mes1M = new bool[] {false} ;
         T000I3_A82mes2H = new int[1] ;
         T000I3_n82mes2H = new bool[] {false} ;
         T000I3_A83mes2M = new int[1] ;
         T000I3_n83mes2M = new bool[] {false} ;
         T000I3_A84mes3H = new int[1] ;
         T000I3_n84mes3H = new bool[] {false} ;
         T000I3_A85mes3M = new int[1] ;
         T000I3_n85mes3M = new bool[] {false} ;
         T000I3_A86mes4H = new int[1] ;
         T000I3_n86mes4H = new bool[] {false} ;
         T000I3_A87mes4M = new int[1] ;
         T000I3_n87mes4M = new bool[] {false} ;
         T000I3_A88mes5H = new int[1] ;
         T000I3_n88mes5H = new bool[] {false} ;
         T000I3_A89mes5M = new int[1] ;
         T000I3_n89mes5M = new bool[] {false} ;
         T000I3_A90mes6H = new int[1] ;
         T000I3_n90mes6H = new bool[] {false} ;
         T000I3_A91mes6M = new int[1] ;
         T000I3_n91mes6M = new bool[] {false} ;
         T000I3_A92mes7H = new int[1] ;
         T000I3_n92mes7H = new bool[] {false} ;
         T000I3_A93mes7M = new int[1] ;
         T000I3_n93mes7M = new bool[] {false} ;
         T000I3_A94mes8H = new int[1] ;
         T000I3_n94mes8H = new bool[] {false} ;
         T000I3_A95mes8M = new int[1] ;
         T000I3_n95mes8M = new bool[] {false} ;
         T000I3_A96mes9H = new int[1] ;
         T000I3_n96mes9H = new bool[] {false} ;
         T000I3_A97mes9M = new int[1] ;
         T000I3_n97mes9M = new bool[] {false} ;
         T000I3_A98mes10H = new int[1] ;
         T000I3_n98mes10H = new bool[] {false} ;
         T000I3_A99mes10M = new int[1] ;
         T000I3_n99mes10M = new bool[] {false} ;
         T000I3_A100mes11H = new int[1] ;
         T000I3_n100mes11H = new bool[] {false} ;
         T000I3_A101mes11M = new int[1] ;
         T000I3_n101mes11M = new bool[] {false} ;
         T000I3_A102mes12H = new int[1] ;
         T000I3_n102mes12H = new bool[] {false} ;
         T000I3_A103mes12M = new int[1] ;
         T000I3_n103mes12M = new bool[] {false} ;
         T000I3_A23ActividadParaPlanid = new int[1] ;
         sMode18 = "";
         T000I8_A14FYActividadPlanCantPartid = new int[1] ;
         T000I9_A14FYActividadPlanCantPartid = new int[1] ;
         T000I2_A14FYActividadPlanCantPartid = new int[1] ;
         T000I2_A79FYActividadPlanCantPartFY = new int[1] ;
         T000I2_A80mes1H = new int[1] ;
         T000I2_n80mes1H = new bool[] {false} ;
         T000I2_A81mes1M = new int[1] ;
         T000I2_n81mes1M = new bool[] {false} ;
         T000I2_A82mes2H = new int[1] ;
         T000I2_n82mes2H = new bool[] {false} ;
         T000I2_A83mes2M = new int[1] ;
         T000I2_n83mes2M = new bool[] {false} ;
         T000I2_A84mes3H = new int[1] ;
         T000I2_n84mes3H = new bool[] {false} ;
         T000I2_A85mes3M = new int[1] ;
         T000I2_n85mes3M = new bool[] {false} ;
         T000I2_A86mes4H = new int[1] ;
         T000I2_n86mes4H = new bool[] {false} ;
         T000I2_A87mes4M = new int[1] ;
         T000I2_n87mes4M = new bool[] {false} ;
         T000I2_A88mes5H = new int[1] ;
         T000I2_n88mes5H = new bool[] {false} ;
         T000I2_A89mes5M = new int[1] ;
         T000I2_n89mes5M = new bool[] {false} ;
         T000I2_A90mes6H = new int[1] ;
         T000I2_n90mes6H = new bool[] {false} ;
         T000I2_A91mes6M = new int[1] ;
         T000I2_n91mes6M = new bool[] {false} ;
         T000I2_A92mes7H = new int[1] ;
         T000I2_n92mes7H = new bool[] {false} ;
         T000I2_A93mes7M = new int[1] ;
         T000I2_n93mes7M = new bool[] {false} ;
         T000I2_A94mes8H = new int[1] ;
         T000I2_n94mes8H = new bool[] {false} ;
         T000I2_A95mes8M = new int[1] ;
         T000I2_n95mes8M = new bool[] {false} ;
         T000I2_A96mes9H = new int[1] ;
         T000I2_n96mes9H = new bool[] {false} ;
         T000I2_A97mes9M = new int[1] ;
         T000I2_n97mes9M = new bool[] {false} ;
         T000I2_A98mes10H = new int[1] ;
         T000I2_n98mes10H = new bool[] {false} ;
         T000I2_A99mes10M = new int[1] ;
         T000I2_n99mes10M = new bool[] {false} ;
         T000I2_A100mes11H = new int[1] ;
         T000I2_n100mes11H = new bool[] {false} ;
         T000I2_A101mes11M = new int[1] ;
         T000I2_n101mes11M = new bool[] {false} ;
         T000I2_A102mes12H = new int[1] ;
         T000I2_n102mes12H = new bool[] {false} ;
         T000I2_A103mes12M = new int[1] ;
         T000I2_n103mes12M = new bool[] {false} ;
         T000I2_A23ActividadParaPlanid = new int[1] ;
         T000I10_A14FYActividadPlanCantPartid = new int[1] ;
         T000I13_A14FYActividadPlanCantPartid = new int[1] ;
         sDynURL = "";
         FormProcess = "";
         bodyStyle = "";
         isValidOutput = new GxUnknownObjectCollection();
         T000I14_A23ActividadParaPlanid = new int[1] ;
         pr_datastore1 = new DataStoreProvider(context, new GeneXus.Programs.fyactividadplancantpart__datastore1(),
            new Object[][] {
                new Object[] {
               T000I2_A14FYActividadPlanCantPartid, T000I2_A79FYActividadPlanCantPartFY, T000I2_A80mes1H, T000I2_n80mes1H, T000I2_A81mes1M, T000I2_n81mes1M, T000I2_A82mes2H, T000I2_n82mes2H, T000I2_A83mes2M, T000I2_n83mes2M,
               T000I2_A84mes3H, T000I2_n84mes3H, T000I2_A85mes3M, T000I2_n85mes3M, T000I2_A86mes4H, T000I2_n86mes4H, T000I2_A87mes4M, T000I2_n87mes4M, T000I2_A88mes5H, T000I2_n88mes5H,
               T000I2_A89mes5M, T000I2_n89mes5M, T000I2_A90mes6H, T000I2_n90mes6H, T000I2_A91mes6M, T000I2_n91mes6M, T000I2_A92mes7H, T000I2_n92mes7H, T000I2_A93mes7M, T000I2_n93mes7M,
               T000I2_A94mes8H, T000I2_n94mes8H, T000I2_A95mes8M, T000I2_n95mes8M, T000I2_A96mes9H, T000I2_n96mes9H, T000I2_A97mes9M, T000I2_n97mes9M, T000I2_A98mes10H, T000I2_n98mes10H,
               T000I2_A99mes10M, T000I2_n99mes10M, T000I2_A100mes11H, T000I2_n100mes11H, T000I2_A101mes11M, T000I2_n101mes11M, T000I2_A102mes12H, T000I2_n102mes12H, T000I2_A103mes12M, T000I2_n103mes12M,
               T000I2_A23ActividadParaPlanid
               }
               , new Object[] {
               T000I3_A14FYActividadPlanCantPartid, T000I3_A79FYActividadPlanCantPartFY, T000I3_A80mes1H, T000I3_n80mes1H, T000I3_A81mes1M, T000I3_n81mes1M, T000I3_A82mes2H, T000I3_n82mes2H, T000I3_A83mes2M, T000I3_n83mes2M,
               T000I3_A84mes3H, T000I3_n84mes3H, T000I3_A85mes3M, T000I3_n85mes3M, T000I3_A86mes4H, T000I3_n86mes4H, T000I3_A87mes4M, T000I3_n87mes4M, T000I3_A88mes5H, T000I3_n88mes5H,
               T000I3_A89mes5M, T000I3_n89mes5M, T000I3_A90mes6H, T000I3_n90mes6H, T000I3_A91mes6M, T000I3_n91mes6M, T000I3_A92mes7H, T000I3_n92mes7H, T000I3_A93mes7M, T000I3_n93mes7M,
               T000I3_A94mes8H, T000I3_n94mes8H, T000I3_A95mes8M, T000I3_n95mes8M, T000I3_A96mes9H, T000I3_n96mes9H, T000I3_A97mes9M, T000I3_n97mes9M, T000I3_A98mes10H, T000I3_n98mes10H,
               T000I3_A99mes10M, T000I3_n99mes10M, T000I3_A100mes11H, T000I3_n100mes11H, T000I3_A101mes11M, T000I3_n101mes11M, T000I3_A102mes12H, T000I3_n102mes12H, T000I3_A103mes12M, T000I3_n103mes12M,
               T000I3_A23ActividadParaPlanid
               }
               , new Object[] {
               T000I4_A23ActividadParaPlanid
               }
               , new Object[] {
               T000I5_A14FYActividadPlanCantPartid, T000I5_A79FYActividadPlanCantPartFY, T000I5_A80mes1H, T000I5_n80mes1H, T000I5_A81mes1M, T000I5_n81mes1M, T000I5_A82mes2H, T000I5_n82mes2H, T000I5_A83mes2M, T000I5_n83mes2M,
               T000I5_A84mes3H, T000I5_n84mes3H, T000I5_A85mes3M, T000I5_n85mes3M, T000I5_A86mes4H, T000I5_n86mes4H, T000I5_A87mes4M, T000I5_n87mes4M, T000I5_A88mes5H, T000I5_n88mes5H,
               T000I5_A89mes5M, T000I5_n89mes5M, T000I5_A90mes6H, T000I5_n90mes6H, T000I5_A91mes6M, T000I5_n91mes6M, T000I5_A92mes7H, T000I5_n92mes7H, T000I5_A93mes7M, T000I5_n93mes7M,
               T000I5_A94mes8H, T000I5_n94mes8H, T000I5_A95mes8M, T000I5_n95mes8M, T000I5_A96mes9H, T000I5_n96mes9H, T000I5_A97mes9M, T000I5_n97mes9M, T000I5_A98mes10H, T000I5_n98mes10H,
               T000I5_A99mes10M, T000I5_n99mes10M, T000I5_A100mes11H, T000I5_n100mes11H, T000I5_A101mes11M, T000I5_n101mes11M, T000I5_A102mes12H, T000I5_n102mes12H, T000I5_A103mes12M, T000I5_n103mes12M,
               T000I5_A23ActividadParaPlanid
               }
               , new Object[] {
               T000I6_A23ActividadParaPlanid
               }
               , new Object[] {
               T000I7_A14FYActividadPlanCantPartid
               }
               , new Object[] {
               T000I8_A14FYActividadPlanCantPartid
               }
               , new Object[] {
               T000I9_A14FYActividadPlanCantPartid
               }
               , new Object[] {
               T000I10_A14FYActividadPlanCantPartid
               }
               , new Object[] {
               }
               , new Object[] {
               }
               , new Object[] {
               T000I13_A14FYActividadPlanCantPartid
               }
               , new Object[] {
               T000I14_A23ActividadParaPlanid
               }
            }
         );
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.fyactividadplancantpart__default(),
            new Object[][] {
            }
         );
      }

      private short GxWebError ;
      private short gxcookieaux ;
      private short IsConfirmed ;
      private short IsModified ;
      private short AnyError ;
      private short nKeyPressed ;
      private short initialized ;
      private short GX_JID ;
      private short RcdFound18 ;
      private short Gx_BScreen ;
      private short gxajaxcallmode ;
      private short wbTemp ;
      private int Z14FYActividadPlanCantPartid ;
      private int Z79FYActividadPlanCantPartFY ;
      private int Z80mes1H ;
      private int Z81mes1M ;
      private int Z82mes2H ;
      private int Z83mes2M ;
      private int Z84mes3H ;
      private int Z85mes3M ;
      private int Z86mes4H ;
      private int Z87mes4M ;
      private int Z88mes5H ;
      private int Z89mes5M ;
      private int Z90mes6H ;
      private int Z91mes6M ;
      private int Z92mes7H ;
      private int Z93mes7M ;
      private int Z94mes8H ;
      private int Z95mes8M ;
      private int Z96mes9H ;
      private int Z97mes9M ;
      private int Z98mes10H ;
      private int Z99mes10M ;
      private int Z100mes11H ;
      private int Z101mes11M ;
      private int Z102mes12H ;
      private int Z103mes12M ;
      private int Z23ActividadParaPlanid ;
      private int A23ActividadParaPlanid ;
      private int trnEnded ;
      private int bttBtn_first_Visible ;
      private int bttBtn_previous_Visible ;
      private int bttBtn_next_Visible ;
      private int bttBtn_last_Visible ;
      private int bttBtn_select_Visible ;
      private int A14FYActividadPlanCantPartid ;
      private int edtFYActividadPlanCantPartid_Enabled ;
      private int edtActividadParaPlanid_Enabled ;
      private int imgprompt_23_Visible ;
      private int A79FYActividadPlanCantPartFY ;
      private int edtFYActividadPlanCantPartFY_Enabled ;
      private int A80mes1H ;
      private int edtmes1H_Enabled ;
      private int A81mes1M ;
      private int edtmes1M_Enabled ;
      private int A82mes2H ;
      private int edtmes2H_Enabled ;
      private int A83mes2M ;
      private int edtmes2M_Enabled ;
      private int A84mes3H ;
      private int edtmes3H_Enabled ;
      private int A85mes3M ;
      private int edtmes3M_Enabled ;
      private int A86mes4H ;
      private int edtmes4H_Enabled ;
      private int A87mes4M ;
      private int edtmes4M_Enabled ;
      private int A88mes5H ;
      private int edtmes5H_Enabled ;
      private int A89mes5M ;
      private int edtmes5M_Enabled ;
      private int A90mes6H ;
      private int edtmes6H_Enabled ;
      private int A91mes6M ;
      private int edtmes6M_Enabled ;
      private int A92mes7H ;
      private int edtmes7H_Enabled ;
      private int A93mes7M ;
      private int edtmes7M_Enabled ;
      private int A94mes8H ;
      private int edtmes8H_Enabled ;
      private int A95mes8M ;
      private int edtmes8M_Enabled ;
      private int A96mes9H ;
      private int edtmes9H_Enabled ;
      private int A97mes9M ;
      private int edtmes9M_Enabled ;
      private int A98mes10H ;
      private int edtmes10H_Enabled ;
      private int A99mes10M ;
      private int edtmes10M_Enabled ;
      private int A100mes11H ;
      private int edtmes11H_Enabled ;
      private int A101mes11M ;
      private int edtmes11M_Enabled ;
      private int A102mes12H ;
      private int edtmes12H_Enabled ;
      private int A103mes12M ;
      private int edtmes12M_Enabled ;
      private int bttBtn_enter_Visible ;
      private int bttBtn_enter_Enabled ;
      private int bttBtn_cancel_Visible ;
      private int bttBtn_delete_Visible ;
      private int bttBtn_delete_Enabled ;
      private int idxLst ;
      private String sPrefix ;
      private String scmdbuf ;
      private String gxfirstwebparm ;
      private String gxfirstwebparm_bkp ;
      private String GXKey ;
      private String PreviousTooltip ;
      private String PreviousCaption ;
      private String GX_FocusControl ;
      private String edtFYActividadPlanCantPartid_Internalname ;
      private String divMaintable_Internalname ;
      private String divTitlecontainer_Internalname ;
      private String lblTitle_Internalname ;
      private String lblTitle_Jsonclick ;
      private String ClassString ;
      private String StyleString ;
      private String divFormcontainer_Internalname ;
      private String divToolbarcell_Internalname ;
      private String TempTags ;
      private String bttBtn_first_Internalname ;
      private String bttBtn_first_Jsonclick ;
      private String bttBtn_previous_Internalname ;
      private String bttBtn_previous_Jsonclick ;
      private String bttBtn_next_Internalname ;
      private String bttBtn_next_Jsonclick ;
      private String bttBtn_last_Internalname ;
      private String bttBtn_last_Jsonclick ;
      private String bttBtn_select_Internalname ;
      private String bttBtn_select_Jsonclick ;
      private String edtFYActividadPlanCantPartid_Jsonclick ;
      private String edtActividadParaPlanid_Internalname ;
      private String edtActividadParaPlanid_Jsonclick ;
      private String sImgUrl ;
      private String imgprompt_23_Internalname ;
      private String imgprompt_23_Link ;
      private String edtFYActividadPlanCantPartFY_Internalname ;
      private String edtFYActividadPlanCantPartFY_Jsonclick ;
      private String edtmes1H_Internalname ;
      private String edtmes1H_Jsonclick ;
      private String edtmes1M_Internalname ;
      private String edtmes1M_Jsonclick ;
      private String edtmes2H_Internalname ;
      private String edtmes2H_Jsonclick ;
      private String edtmes2M_Internalname ;
      private String edtmes2M_Jsonclick ;
      private String edtmes3H_Internalname ;
      private String edtmes3H_Jsonclick ;
      private String edtmes3M_Internalname ;
      private String edtmes3M_Jsonclick ;
      private String edtmes4H_Internalname ;
      private String edtmes4H_Jsonclick ;
      private String edtmes4M_Internalname ;
      private String edtmes4M_Jsonclick ;
      private String edtmes5H_Internalname ;
      private String edtmes5H_Jsonclick ;
      private String edtmes5M_Internalname ;
      private String edtmes5M_Jsonclick ;
      private String edtmes6H_Internalname ;
      private String edtmes6H_Jsonclick ;
      private String edtmes6M_Internalname ;
      private String edtmes6M_Jsonclick ;
      private String edtmes7H_Internalname ;
      private String edtmes7H_Jsonclick ;
      private String edtmes7M_Internalname ;
      private String edtmes7M_Jsonclick ;
      private String edtmes8H_Internalname ;
      private String edtmes8H_Jsonclick ;
      private String edtmes8M_Internalname ;
      private String edtmes8M_Jsonclick ;
      private String edtmes9H_Internalname ;
      private String edtmes9H_Jsonclick ;
      private String edtmes9M_Internalname ;
      private String edtmes9M_Jsonclick ;
      private String edtmes10H_Internalname ;
      private String edtmes10H_Jsonclick ;
      private String edtmes10M_Internalname ;
      private String edtmes10M_Jsonclick ;
      private String edtmes11H_Internalname ;
      private String edtmes11H_Jsonclick ;
      private String edtmes11M_Internalname ;
      private String edtmes11M_Jsonclick ;
      private String edtmes12H_Internalname ;
      private String edtmes12H_Jsonclick ;
      private String edtmes12M_Internalname ;
      private String edtmes12M_Jsonclick ;
      private String bttBtn_enter_Internalname ;
      private String bttBtn_enter_Jsonclick ;
      private String bttBtn_cancel_Internalname ;
      private String bttBtn_cancel_Jsonclick ;
      private String bttBtn_delete_Internalname ;
      private String bttBtn_delete_Jsonclick ;
      private String Gx_mode ;
      private String sEvt ;
      private String EvtGridId ;
      private String EvtRowId ;
      private String sEvtType ;
      private String sMode18 ;
      private String sDynURL ;
      private String FormProcess ;
      private String bodyStyle ;
      private bool entryPointCalled ;
      private bool toggleJsOutput ;
      private bool wbErr ;
      private bool n80mes1H ;
      private bool n81mes1M ;
      private bool n82mes2H ;
      private bool n83mes2M ;
      private bool n84mes3H ;
      private bool n85mes3M ;
      private bool n86mes4H ;
      private bool n87mes4M ;
      private bool n88mes5H ;
      private bool n89mes5M ;
      private bool n90mes6H ;
      private bool n91mes6M ;
      private bool n92mes7H ;
      private bool n93mes7M ;
      private bool n94mes8H ;
      private bool n95mes8M ;
      private bool n96mes9H ;
      private bool n97mes9M ;
      private bool n98mes10H ;
      private bool n99mes10M ;
      private bool n100mes11H ;
      private bool n101mes11M ;
      private bool n102mes12H ;
      private bool n103mes12M ;
      private bool Gx_longc ;
      private GxUnknownObjectCollection isValidOutput ;
      private IGxDataStore dsDataStore1 ;
      private IGxDataStore dsDefault ;
      private IDataStoreProvider pr_datastore1 ;
      private int[] T000I5_A14FYActividadPlanCantPartid ;
      private int[] T000I5_A79FYActividadPlanCantPartFY ;
      private int[] T000I5_A80mes1H ;
      private bool[] T000I5_n80mes1H ;
      private int[] T000I5_A81mes1M ;
      private bool[] T000I5_n81mes1M ;
      private int[] T000I5_A82mes2H ;
      private bool[] T000I5_n82mes2H ;
      private int[] T000I5_A83mes2M ;
      private bool[] T000I5_n83mes2M ;
      private int[] T000I5_A84mes3H ;
      private bool[] T000I5_n84mes3H ;
      private int[] T000I5_A85mes3M ;
      private bool[] T000I5_n85mes3M ;
      private int[] T000I5_A86mes4H ;
      private bool[] T000I5_n86mes4H ;
      private int[] T000I5_A87mes4M ;
      private bool[] T000I5_n87mes4M ;
      private int[] T000I5_A88mes5H ;
      private bool[] T000I5_n88mes5H ;
      private int[] T000I5_A89mes5M ;
      private bool[] T000I5_n89mes5M ;
      private int[] T000I5_A90mes6H ;
      private bool[] T000I5_n90mes6H ;
      private int[] T000I5_A91mes6M ;
      private bool[] T000I5_n91mes6M ;
      private int[] T000I5_A92mes7H ;
      private bool[] T000I5_n92mes7H ;
      private int[] T000I5_A93mes7M ;
      private bool[] T000I5_n93mes7M ;
      private int[] T000I5_A94mes8H ;
      private bool[] T000I5_n94mes8H ;
      private int[] T000I5_A95mes8M ;
      private bool[] T000I5_n95mes8M ;
      private int[] T000I5_A96mes9H ;
      private bool[] T000I5_n96mes9H ;
      private int[] T000I5_A97mes9M ;
      private bool[] T000I5_n97mes9M ;
      private int[] T000I5_A98mes10H ;
      private bool[] T000I5_n98mes10H ;
      private int[] T000I5_A99mes10M ;
      private bool[] T000I5_n99mes10M ;
      private int[] T000I5_A100mes11H ;
      private bool[] T000I5_n100mes11H ;
      private int[] T000I5_A101mes11M ;
      private bool[] T000I5_n101mes11M ;
      private int[] T000I5_A102mes12H ;
      private bool[] T000I5_n102mes12H ;
      private int[] T000I5_A103mes12M ;
      private bool[] T000I5_n103mes12M ;
      private int[] T000I5_A23ActividadParaPlanid ;
      private int[] T000I4_A23ActividadParaPlanid ;
      private int[] T000I6_A23ActividadParaPlanid ;
      private int[] T000I7_A14FYActividadPlanCantPartid ;
      private int[] T000I3_A14FYActividadPlanCantPartid ;
      private int[] T000I3_A79FYActividadPlanCantPartFY ;
      private int[] T000I3_A80mes1H ;
      private bool[] T000I3_n80mes1H ;
      private int[] T000I3_A81mes1M ;
      private bool[] T000I3_n81mes1M ;
      private int[] T000I3_A82mes2H ;
      private bool[] T000I3_n82mes2H ;
      private int[] T000I3_A83mes2M ;
      private bool[] T000I3_n83mes2M ;
      private int[] T000I3_A84mes3H ;
      private bool[] T000I3_n84mes3H ;
      private int[] T000I3_A85mes3M ;
      private bool[] T000I3_n85mes3M ;
      private int[] T000I3_A86mes4H ;
      private bool[] T000I3_n86mes4H ;
      private int[] T000I3_A87mes4M ;
      private bool[] T000I3_n87mes4M ;
      private int[] T000I3_A88mes5H ;
      private bool[] T000I3_n88mes5H ;
      private int[] T000I3_A89mes5M ;
      private bool[] T000I3_n89mes5M ;
      private int[] T000I3_A90mes6H ;
      private bool[] T000I3_n90mes6H ;
      private int[] T000I3_A91mes6M ;
      private bool[] T000I3_n91mes6M ;
      private int[] T000I3_A92mes7H ;
      private bool[] T000I3_n92mes7H ;
      private int[] T000I3_A93mes7M ;
      private bool[] T000I3_n93mes7M ;
      private int[] T000I3_A94mes8H ;
      private bool[] T000I3_n94mes8H ;
      private int[] T000I3_A95mes8M ;
      private bool[] T000I3_n95mes8M ;
      private int[] T000I3_A96mes9H ;
      private bool[] T000I3_n96mes9H ;
      private int[] T000I3_A97mes9M ;
      private bool[] T000I3_n97mes9M ;
      private int[] T000I3_A98mes10H ;
      private bool[] T000I3_n98mes10H ;
      private int[] T000I3_A99mes10M ;
      private bool[] T000I3_n99mes10M ;
      private int[] T000I3_A100mes11H ;
      private bool[] T000I3_n100mes11H ;
      private int[] T000I3_A101mes11M ;
      private bool[] T000I3_n101mes11M ;
      private int[] T000I3_A102mes12H ;
      private bool[] T000I3_n102mes12H ;
      private int[] T000I3_A103mes12M ;
      private bool[] T000I3_n103mes12M ;
      private int[] T000I3_A23ActividadParaPlanid ;
      private int[] T000I8_A14FYActividadPlanCantPartid ;
      private int[] T000I9_A14FYActividadPlanCantPartid ;
      private int[] T000I2_A14FYActividadPlanCantPartid ;
      private int[] T000I2_A79FYActividadPlanCantPartFY ;
      private int[] T000I2_A80mes1H ;
      private bool[] T000I2_n80mes1H ;
      private int[] T000I2_A81mes1M ;
      private bool[] T000I2_n81mes1M ;
      private int[] T000I2_A82mes2H ;
      private bool[] T000I2_n82mes2H ;
      private int[] T000I2_A83mes2M ;
      private bool[] T000I2_n83mes2M ;
      private int[] T000I2_A84mes3H ;
      private bool[] T000I2_n84mes3H ;
      private int[] T000I2_A85mes3M ;
      private bool[] T000I2_n85mes3M ;
      private int[] T000I2_A86mes4H ;
      private bool[] T000I2_n86mes4H ;
      private int[] T000I2_A87mes4M ;
      private bool[] T000I2_n87mes4M ;
      private int[] T000I2_A88mes5H ;
      private bool[] T000I2_n88mes5H ;
      private int[] T000I2_A89mes5M ;
      private bool[] T000I2_n89mes5M ;
      private int[] T000I2_A90mes6H ;
      private bool[] T000I2_n90mes6H ;
      private int[] T000I2_A91mes6M ;
      private bool[] T000I2_n91mes6M ;
      private int[] T000I2_A92mes7H ;
      private bool[] T000I2_n92mes7H ;
      private int[] T000I2_A93mes7M ;
      private bool[] T000I2_n93mes7M ;
      private int[] T000I2_A94mes8H ;
      private bool[] T000I2_n94mes8H ;
      private int[] T000I2_A95mes8M ;
      private bool[] T000I2_n95mes8M ;
      private int[] T000I2_A96mes9H ;
      private bool[] T000I2_n96mes9H ;
      private int[] T000I2_A97mes9M ;
      private bool[] T000I2_n97mes9M ;
      private int[] T000I2_A98mes10H ;
      private bool[] T000I2_n98mes10H ;
      private int[] T000I2_A99mes10M ;
      private bool[] T000I2_n99mes10M ;
      private int[] T000I2_A100mes11H ;
      private bool[] T000I2_n100mes11H ;
      private int[] T000I2_A101mes11M ;
      private bool[] T000I2_n101mes11M ;
      private int[] T000I2_A102mes12H ;
      private bool[] T000I2_n102mes12H ;
      private int[] T000I2_A103mes12M ;
      private bool[] T000I2_n103mes12M ;
      private int[] T000I2_A23ActividadParaPlanid ;
      private int[] T000I10_A14FYActividadPlanCantPartid ;
      private IDataStoreProvider pr_default ;
      private int[] T000I13_A14FYActividadPlanCantPartid ;
      private int[] T000I14_A23ActividadParaPlanid ;
      private GXWebForm Form ;
   }

   public class fyactividadplancantpart__datastore1 : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
         ,new ForEachCursor(def[2])
         ,new ForEachCursor(def[3])
         ,new ForEachCursor(def[4])
         ,new ForEachCursor(def[5])
         ,new ForEachCursor(def[6])
         ,new ForEachCursor(def[7])
         ,new ForEachCursor(def[8])
         ,new UpdateCursor(def[9])
         ,new UpdateCursor(def[10])
         ,new ForEachCursor(def[11])
         ,new ForEachCursor(def[12])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmT000I5 ;
          prmT000I5 = new Object[] {
          new Object[] {"@FYActividadPlanCantPartid",SqlDbType.Int,9,0}
          } ;
          Object[] prmT000I4 ;
          prmT000I4 = new Object[] {
          new Object[] {"@ActividadParaPlanid",SqlDbType.Int,9,0}
          } ;
          Object[] prmT000I6 ;
          prmT000I6 = new Object[] {
          new Object[] {"@ActividadParaPlanid",SqlDbType.Int,9,0}
          } ;
          Object[] prmT000I7 ;
          prmT000I7 = new Object[] {
          new Object[] {"@FYActividadPlanCantPartid",SqlDbType.Int,9,0}
          } ;
          Object[] prmT000I3 ;
          prmT000I3 = new Object[] {
          new Object[] {"@FYActividadPlanCantPartid",SqlDbType.Int,9,0}
          } ;
          Object[] prmT000I8 ;
          prmT000I8 = new Object[] {
          new Object[] {"@FYActividadPlanCantPartid",SqlDbType.Int,9,0}
          } ;
          Object[] prmT000I9 ;
          prmT000I9 = new Object[] {
          new Object[] {"@FYActividadPlanCantPartid",SqlDbType.Int,9,0}
          } ;
          Object[] prmT000I2 ;
          prmT000I2 = new Object[] {
          new Object[] {"@FYActividadPlanCantPartid",SqlDbType.Int,9,0}
          } ;
          Object[] prmT000I10 ;
          prmT000I10 = new Object[] {
          new Object[] {"@FYActividadPlanCantPartFY",SqlDbType.Int,9,0} ,
          new Object[] {"@mes1H",SqlDbType.Int,9,0} ,
          new Object[] {"@mes1M",SqlDbType.Int,9,0} ,
          new Object[] {"@mes2H",SqlDbType.Int,9,0} ,
          new Object[] {"@mes2M",SqlDbType.Int,9,0} ,
          new Object[] {"@mes3H",SqlDbType.Int,9,0} ,
          new Object[] {"@mes3M",SqlDbType.Int,9,0} ,
          new Object[] {"@mes4H",SqlDbType.Int,9,0} ,
          new Object[] {"@mes4M",SqlDbType.Int,9,0} ,
          new Object[] {"@mes5H",SqlDbType.Int,9,0} ,
          new Object[] {"@mes5M",SqlDbType.Int,9,0} ,
          new Object[] {"@mes6H",SqlDbType.Int,9,0} ,
          new Object[] {"@mes6M",SqlDbType.Int,9,0} ,
          new Object[] {"@mes7H",SqlDbType.Int,9,0} ,
          new Object[] {"@mes7M",SqlDbType.Int,9,0} ,
          new Object[] {"@mes8H",SqlDbType.Int,9,0} ,
          new Object[] {"@mes8M",SqlDbType.Int,9,0} ,
          new Object[] {"@mes9H",SqlDbType.Int,9,0} ,
          new Object[] {"@mes9M",SqlDbType.Int,9,0} ,
          new Object[] {"@mes10H",SqlDbType.Int,9,0} ,
          new Object[] {"@mes10M",SqlDbType.Int,9,0} ,
          new Object[] {"@mes11H",SqlDbType.Int,9,0} ,
          new Object[] {"@mes11M",SqlDbType.Int,9,0} ,
          new Object[] {"@mes12H",SqlDbType.Int,9,0} ,
          new Object[] {"@mes12M",SqlDbType.Int,9,0} ,
          new Object[] {"@ActividadParaPlanid",SqlDbType.Int,9,0}
          } ;
          Object[] prmT000I11 ;
          prmT000I11 = new Object[] {
          new Object[] {"@FYActividadPlanCantPartFY",SqlDbType.Int,9,0} ,
          new Object[] {"@mes1H",SqlDbType.Int,9,0} ,
          new Object[] {"@mes1M",SqlDbType.Int,9,0} ,
          new Object[] {"@mes2H",SqlDbType.Int,9,0} ,
          new Object[] {"@mes2M",SqlDbType.Int,9,0} ,
          new Object[] {"@mes3H",SqlDbType.Int,9,0} ,
          new Object[] {"@mes3M",SqlDbType.Int,9,0} ,
          new Object[] {"@mes4H",SqlDbType.Int,9,0} ,
          new Object[] {"@mes4M",SqlDbType.Int,9,0} ,
          new Object[] {"@mes5H",SqlDbType.Int,9,0} ,
          new Object[] {"@mes5M",SqlDbType.Int,9,0} ,
          new Object[] {"@mes6H",SqlDbType.Int,9,0} ,
          new Object[] {"@mes6M",SqlDbType.Int,9,0} ,
          new Object[] {"@mes7H",SqlDbType.Int,9,0} ,
          new Object[] {"@mes7M",SqlDbType.Int,9,0} ,
          new Object[] {"@mes8H",SqlDbType.Int,9,0} ,
          new Object[] {"@mes8M",SqlDbType.Int,9,0} ,
          new Object[] {"@mes9H",SqlDbType.Int,9,0} ,
          new Object[] {"@mes9M",SqlDbType.Int,9,0} ,
          new Object[] {"@mes10H",SqlDbType.Int,9,0} ,
          new Object[] {"@mes10M",SqlDbType.Int,9,0} ,
          new Object[] {"@mes11H",SqlDbType.Int,9,0} ,
          new Object[] {"@mes11M",SqlDbType.Int,9,0} ,
          new Object[] {"@mes12H",SqlDbType.Int,9,0} ,
          new Object[] {"@mes12M",SqlDbType.Int,9,0} ,
          new Object[] {"@ActividadParaPlanid",SqlDbType.Int,9,0} ,
          new Object[] {"@FYActividadPlanCantPartid",SqlDbType.Int,9,0}
          } ;
          Object[] prmT000I12 ;
          prmT000I12 = new Object[] {
          new Object[] {"@FYActividadPlanCantPartid",SqlDbType.Int,9,0}
          } ;
          Object[] prmT000I13 ;
          prmT000I13 = new Object[] {
          } ;
          Object[] prmT000I14 ;
          prmT000I14 = new Object[] {
          new Object[] {"@ActividadParaPlanid",SqlDbType.Int,9,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("T000I2", "SELECT [id] AS FYActividadPlanCantPartid, [FY], [mes1H], [mes1M], [mes2H], [mes2M], [mes3H], [mes3M], [mes4H], [mes4M], [mes5H], [mes5M], [mes6H], [mes6M], [mes7H], [mes7M], [mes8H], [mes8M], [mes9H], [mes9M], [mes10H], [mes10M], [mes11H], [mes11M], [mes12H], [mes12M], [idActividadParaPlan] AS ActividadParaPlanid FROM dbo.[FYActividadPlanCantPart] WITH (UPDLOCK) WHERE [id] = @FYActividadPlanCantPartid ",true, GxErrorMask.GX_NOMASK, false, this,prmT000I2,1,0,true,false )
             ,new CursorDef("T000I3", "SELECT [id] AS FYActividadPlanCantPartid, [FY], [mes1H], [mes1M], [mes2H], [mes2M], [mes3H], [mes3M], [mes4H], [mes4M], [mes5H], [mes5M], [mes6H], [mes6M], [mes7H], [mes7M], [mes8H], [mes8M], [mes9H], [mes9M], [mes10H], [mes10M], [mes11H], [mes11M], [mes12H], [mes12M], [idActividadParaPlan] AS ActividadParaPlanid FROM dbo.[FYActividadPlanCantPart] WITH (NOLOCK) WHERE [id] = @FYActividadPlanCantPartid ",true, GxErrorMask.GX_NOMASK, false, this,prmT000I3,1,0,true,false )
             ,new CursorDef("T000I4", "SELECT [id] AS ActividadParaPlanid FROM dbo.[ActividadParaPlan] WITH (NOLOCK) WHERE [id] = @ActividadParaPlanid ",true, GxErrorMask.GX_NOMASK, false, this,prmT000I4,1,0,true,false )
             ,new CursorDef("T000I5", "SELECT TM1.[id] AS FYActividadPlanCantPartid, TM1.[FY], TM1.[mes1H], TM1.[mes1M], TM1.[mes2H], TM1.[mes2M], TM1.[mes3H], TM1.[mes3M], TM1.[mes4H], TM1.[mes4M], TM1.[mes5H], TM1.[mes5M], TM1.[mes6H], TM1.[mes6M], TM1.[mes7H], TM1.[mes7M], TM1.[mes8H], TM1.[mes8M], TM1.[mes9H], TM1.[mes9M], TM1.[mes10H], TM1.[mes10M], TM1.[mes11H], TM1.[mes11M], TM1.[mes12H], TM1.[mes12M], TM1.[idActividadParaPlan] AS ActividadParaPlanid FROM dbo.[FYActividadPlanCantPart] TM1 WITH (NOLOCK) WHERE TM1.[id] = @FYActividadPlanCantPartid ORDER BY TM1.[id]  OPTION (FAST 100)",true, GxErrorMask.GX_NOMASK, false, this,prmT000I5,100,0,true,false )
             ,new CursorDef("T000I6", "SELECT [id] AS ActividadParaPlanid FROM dbo.[ActividadParaPlan] WITH (NOLOCK) WHERE [id] = @ActividadParaPlanid ",true, GxErrorMask.GX_NOMASK, false, this,prmT000I6,1,0,true,false )
             ,new CursorDef("T000I7", "SELECT [id] AS FYActividadPlanCantPartid FROM dbo.[FYActividadPlanCantPart] WITH (NOLOCK) WHERE [id] = @FYActividadPlanCantPartid  OPTION (FAST 1)",true, GxErrorMask.GX_NOMASK, false, this,prmT000I7,1,0,true,false )
             ,new CursorDef("T000I8", "SELECT TOP 1 [id] AS FYActividadPlanCantPartid FROM dbo.[FYActividadPlanCantPart] WITH (NOLOCK) WHERE ( [id] > @FYActividadPlanCantPartid) ORDER BY [id]  OPTION (FAST 1)",true, GxErrorMask.GX_NOMASK, false, this,prmT000I8,1,0,true,true )
             ,new CursorDef("T000I9", "SELECT TOP 1 [id] AS FYActividadPlanCantPartid FROM dbo.[FYActividadPlanCantPart] WITH (NOLOCK) WHERE ( [id] < @FYActividadPlanCantPartid) ORDER BY [id] DESC  OPTION (FAST 1)",true, GxErrorMask.GX_NOMASK, false, this,prmT000I9,1,0,true,true )
             ,new CursorDef("T000I10", "INSERT INTO dbo.[FYActividadPlanCantPart]([FY], [mes1H], [mes1M], [mes2H], [mes2M], [mes3H], [mes3M], [mes4H], [mes4M], [mes5H], [mes5M], [mes6H], [mes6M], [mes7H], [mes7M], [mes8H], [mes8M], [mes9H], [mes9M], [mes10H], [mes10M], [mes11H], [mes11M], [mes12H], [mes12M], [idActividadParaPlan]) VALUES(@FYActividadPlanCantPartFY, @mes1H, @mes1M, @mes2H, @mes2M, @mes3H, @mes3M, @mes4H, @mes4M, @mes5H, @mes5M, @mes6H, @mes6M, @mes7H, @mes7M, @mes8H, @mes8M, @mes9H, @mes9M, @mes10H, @mes10M, @mes11H, @mes11M, @mes12H, @mes12M, @ActividadParaPlanid); SELECT SCOPE_IDENTITY()", GxErrorMask.GX_NOMASK,prmT000I10)
             ,new CursorDef("T000I11", "UPDATE dbo.[FYActividadPlanCantPart] SET [FY]=@FYActividadPlanCantPartFY, [mes1H]=@mes1H, [mes1M]=@mes1M, [mes2H]=@mes2H, [mes2M]=@mes2M, [mes3H]=@mes3H, [mes3M]=@mes3M, [mes4H]=@mes4H, [mes4M]=@mes4M, [mes5H]=@mes5H, [mes5M]=@mes5M, [mes6H]=@mes6H, [mes6M]=@mes6M, [mes7H]=@mes7H, [mes7M]=@mes7M, [mes8H]=@mes8H, [mes8M]=@mes8M, [mes9H]=@mes9H, [mes9M]=@mes9M, [mes10H]=@mes10H, [mes10M]=@mes10M, [mes11H]=@mes11H, [mes11M]=@mes11M, [mes12H]=@mes12H, [mes12M]=@mes12M, [idActividadParaPlan]=@ActividadParaPlanid  WHERE [id] = @FYActividadPlanCantPartid", GxErrorMask.GX_NOMASK,prmT000I11)
             ,new CursorDef("T000I12", "DELETE FROM dbo.[FYActividadPlanCantPart]  WHERE [id] = @FYActividadPlanCantPartid", GxErrorMask.GX_NOMASK,prmT000I12)
             ,new CursorDef("T000I13", "SELECT [id] AS FYActividadPlanCantPartid FROM dbo.[FYActividadPlanCantPart] WITH (NOLOCK) ORDER BY [id]  OPTION (FAST 100)",true, GxErrorMask.GX_NOMASK, false, this,prmT000I13,100,0,true,false )
             ,new CursorDef("T000I14", "SELECT [id] AS ActividadParaPlanid FROM dbo.[ActividadParaPlan] WITH (NOLOCK) WHERE [id] = @ActividadParaPlanid ",true, GxErrorMask.GX_NOMASK, false, this,prmT000I14,1,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((int[]) buf[2])[0] = rslt.getInt(3) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(3);
                ((int[]) buf[4])[0] = rslt.getInt(4) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(4);
                ((int[]) buf[6])[0] = rslt.getInt(5) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(5);
                ((int[]) buf[8])[0] = rslt.getInt(6) ;
                ((bool[]) buf[9])[0] = rslt.wasNull(6);
                ((int[]) buf[10])[0] = rslt.getInt(7) ;
                ((bool[]) buf[11])[0] = rslt.wasNull(7);
                ((int[]) buf[12])[0] = rslt.getInt(8) ;
                ((bool[]) buf[13])[0] = rslt.wasNull(8);
                ((int[]) buf[14])[0] = rslt.getInt(9) ;
                ((bool[]) buf[15])[0] = rslt.wasNull(9);
                ((int[]) buf[16])[0] = rslt.getInt(10) ;
                ((bool[]) buf[17])[0] = rslt.wasNull(10);
                ((int[]) buf[18])[0] = rslt.getInt(11) ;
                ((bool[]) buf[19])[0] = rslt.wasNull(11);
                ((int[]) buf[20])[0] = rslt.getInt(12) ;
                ((bool[]) buf[21])[0] = rslt.wasNull(12);
                ((int[]) buf[22])[0] = rslt.getInt(13) ;
                ((bool[]) buf[23])[0] = rslt.wasNull(13);
                ((int[]) buf[24])[0] = rslt.getInt(14) ;
                ((bool[]) buf[25])[0] = rslt.wasNull(14);
                ((int[]) buf[26])[0] = rslt.getInt(15) ;
                ((bool[]) buf[27])[0] = rslt.wasNull(15);
                ((int[]) buf[28])[0] = rslt.getInt(16) ;
                ((bool[]) buf[29])[0] = rslt.wasNull(16);
                ((int[]) buf[30])[0] = rslt.getInt(17) ;
                ((bool[]) buf[31])[0] = rslt.wasNull(17);
                ((int[]) buf[32])[0] = rslt.getInt(18) ;
                ((bool[]) buf[33])[0] = rslt.wasNull(18);
                ((int[]) buf[34])[0] = rslt.getInt(19) ;
                ((bool[]) buf[35])[0] = rslt.wasNull(19);
                ((int[]) buf[36])[0] = rslt.getInt(20) ;
                ((bool[]) buf[37])[0] = rslt.wasNull(20);
                ((int[]) buf[38])[0] = rslt.getInt(21) ;
                ((bool[]) buf[39])[0] = rslt.wasNull(21);
                ((int[]) buf[40])[0] = rslt.getInt(22) ;
                ((bool[]) buf[41])[0] = rslt.wasNull(22);
                ((int[]) buf[42])[0] = rslt.getInt(23) ;
                ((bool[]) buf[43])[0] = rslt.wasNull(23);
                ((int[]) buf[44])[0] = rslt.getInt(24) ;
                ((bool[]) buf[45])[0] = rslt.wasNull(24);
                ((int[]) buf[46])[0] = rslt.getInt(25) ;
                ((bool[]) buf[47])[0] = rslt.wasNull(25);
                ((int[]) buf[48])[0] = rslt.getInt(26) ;
                ((bool[]) buf[49])[0] = rslt.wasNull(26);
                ((int[]) buf[50])[0] = rslt.getInt(27) ;
                return;
             case 1 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((int[]) buf[2])[0] = rslt.getInt(3) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(3);
                ((int[]) buf[4])[0] = rslt.getInt(4) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(4);
                ((int[]) buf[6])[0] = rslt.getInt(5) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(5);
                ((int[]) buf[8])[0] = rslt.getInt(6) ;
                ((bool[]) buf[9])[0] = rslt.wasNull(6);
                ((int[]) buf[10])[0] = rslt.getInt(7) ;
                ((bool[]) buf[11])[0] = rslt.wasNull(7);
                ((int[]) buf[12])[0] = rslt.getInt(8) ;
                ((bool[]) buf[13])[0] = rslt.wasNull(8);
                ((int[]) buf[14])[0] = rslt.getInt(9) ;
                ((bool[]) buf[15])[0] = rslt.wasNull(9);
                ((int[]) buf[16])[0] = rslt.getInt(10) ;
                ((bool[]) buf[17])[0] = rslt.wasNull(10);
                ((int[]) buf[18])[0] = rslt.getInt(11) ;
                ((bool[]) buf[19])[0] = rslt.wasNull(11);
                ((int[]) buf[20])[0] = rslt.getInt(12) ;
                ((bool[]) buf[21])[0] = rslt.wasNull(12);
                ((int[]) buf[22])[0] = rslt.getInt(13) ;
                ((bool[]) buf[23])[0] = rslt.wasNull(13);
                ((int[]) buf[24])[0] = rslt.getInt(14) ;
                ((bool[]) buf[25])[0] = rslt.wasNull(14);
                ((int[]) buf[26])[0] = rslt.getInt(15) ;
                ((bool[]) buf[27])[0] = rslt.wasNull(15);
                ((int[]) buf[28])[0] = rslt.getInt(16) ;
                ((bool[]) buf[29])[0] = rslt.wasNull(16);
                ((int[]) buf[30])[0] = rslt.getInt(17) ;
                ((bool[]) buf[31])[0] = rslt.wasNull(17);
                ((int[]) buf[32])[0] = rslt.getInt(18) ;
                ((bool[]) buf[33])[0] = rslt.wasNull(18);
                ((int[]) buf[34])[0] = rslt.getInt(19) ;
                ((bool[]) buf[35])[0] = rslt.wasNull(19);
                ((int[]) buf[36])[0] = rslt.getInt(20) ;
                ((bool[]) buf[37])[0] = rslt.wasNull(20);
                ((int[]) buf[38])[0] = rslt.getInt(21) ;
                ((bool[]) buf[39])[0] = rslt.wasNull(21);
                ((int[]) buf[40])[0] = rslt.getInt(22) ;
                ((bool[]) buf[41])[0] = rslt.wasNull(22);
                ((int[]) buf[42])[0] = rslt.getInt(23) ;
                ((bool[]) buf[43])[0] = rslt.wasNull(23);
                ((int[]) buf[44])[0] = rslt.getInt(24) ;
                ((bool[]) buf[45])[0] = rslt.wasNull(24);
                ((int[]) buf[46])[0] = rslt.getInt(25) ;
                ((bool[]) buf[47])[0] = rslt.wasNull(25);
                ((int[]) buf[48])[0] = rslt.getInt(26) ;
                ((bool[]) buf[49])[0] = rslt.wasNull(26);
                ((int[]) buf[50])[0] = rslt.getInt(27) ;
                return;
             case 2 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 3 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((int[]) buf[2])[0] = rslt.getInt(3) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(3);
                ((int[]) buf[4])[0] = rslt.getInt(4) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(4);
                ((int[]) buf[6])[0] = rslt.getInt(5) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(5);
                ((int[]) buf[8])[0] = rslt.getInt(6) ;
                ((bool[]) buf[9])[0] = rslt.wasNull(6);
                ((int[]) buf[10])[0] = rslt.getInt(7) ;
                ((bool[]) buf[11])[0] = rslt.wasNull(7);
                ((int[]) buf[12])[0] = rslt.getInt(8) ;
                ((bool[]) buf[13])[0] = rslt.wasNull(8);
                ((int[]) buf[14])[0] = rslt.getInt(9) ;
                ((bool[]) buf[15])[0] = rslt.wasNull(9);
                ((int[]) buf[16])[0] = rslt.getInt(10) ;
                ((bool[]) buf[17])[0] = rslt.wasNull(10);
                ((int[]) buf[18])[0] = rslt.getInt(11) ;
                ((bool[]) buf[19])[0] = rslt.wasNull(11);
                ((int[]) buf[20])[0] = rslt.getInt(12) ;
                ((bool[]) buf[21])[0] = rslt.wasNull(12);
                ((int[]) buf[22])[0] = rslt.getInt(13) ;
                ((bool[]) buf[23])[0] = rslt.wasNull(13);
                ((int[]) buf[24])[0] = rslt.getInt(14) ;
                ((bool[]) buf[25])[0] = rslt.wasNull(14);
                ((int[]) buf[26])[0] = rslt.getInt(15) ;
                ((bool[]) buf[27])[0] = rslt.wasNull(15);
                ((int[]) buf[28])[0] = rslt.getInt(16) ;
                ((bool[]) buf[29])[0] = rslt.wasNull(16);
                ((int[]) buf[30])[0] = rslt.getInt(17) ;
                ((bool[]) buf[31])[0] = rslt.wasNull(17);
                ((int[]) buf[32])[0] = rslt.getInt(18) ;
                ((bool[]) buf[33])[0] = rslt.wasNull(18);
                ((int[]) buf[34])[0] = rslt.getInt(19) ;
                ((bool[]) buf[35])[0] = rslt.wasNull(19);
                ((int[]) buf[36])[0] = rslt.getInt(20) ;
                ((bool[]) buf[37])[0] = rslt.wasNull(20);
                ((int[]) buf[38])[0] = rslt.getInt(21) ;
                ((bool[]) buf[39])[0] = rslt.wasNull(21);
                ((int[]) buf[40])[0] = rslt.getInt(22) ;
                ((bool[]) buf[41])[0] = rslt.wasNull(22);
                ((int[]) buf[42])[0] = rslt.getInt(23) ;
                ((bool[]) buf[43])[0] = rslt.wasNull(23);
                ((int[]) buf[44])[0] = rslt.getInt(24) ;
                ((bool[]) buf[45])[0] = rslt.wasNull(24);
                ((int[]) buf[46])[0] = rslt.getInt(25) ;
                ((bool[]) buf[47])[0] = rslt.wasNull(25);
                ((int[]) buf[48])[0] = rslt.getInt(26) ;
                ((bool[]) buf[49])[0] = rslt.wasNull(26);
                ((int[]) buf[50])[0] = rslt.getInt(27) ;
                return;
             case 4 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 5 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 6 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 7 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 8 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 11 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 12 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 1 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 2 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 3 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 4 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 5 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 6 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 7 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 8 :
                stmt.SetParameter(1, (int)parms[0]);
                if ( (bool)parms[1] )
                {
                   stmt.setNull( 2 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(2, (int)parms[2]);
                }
                if ( (bool)parms[3] )
                {
                   stmt.setNull( 3 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(3, (int)parms[4]);
                }
                if ( (bool)parms[5] )
                {
                   stmt.setNull( 4 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(4, (int)parms[6]);
                }
                if ( (bool)parms[7] )
                {
                   stmt.setNull( 5 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(5, (int)parms[8]);
                }
                if ( (bool)parms[9] )
                {
                   stmt.setNull( 6 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(6, (int)parms[10]);
                }
                if ( (bool)parms[11] )
                {
                   stmt.setNull( 7 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(7, (int)parms[12]);
                }
                if ( (bool)parms[13] )
                {
                   stmt.setNull( 8 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(8, (int)parms[14]);
                }
                if ( (bool)parms[15] )
                {
                   stmt.setNull( 9 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(9, (int)parms[16]);
                }
                if ( (bool)parms[17] )
                {
                   stmt.setNull( 10 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(10, (int)parms[18]);
                }
                if ( (bool)parms[19] )
                {
                   stmt.setNull( 11 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(11, (int)parms[20]);
                }
                if ( (bool)parms[21] )
                {
                   stmt.setNull( 12 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(12, (int)parms[22]);
                }
                if ( (bool)parms[23] )
                {
                   stmt.setNull( 13 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(13, (int)parms[24]);
                }
                if ( (bool)parms[25] )
                {
                   stmt.setNull( 14 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(14, (int)parms[26]);
                }
                if ( (bool)parms[27] )
                {
                   stmt.setNull( 15 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(15, (int)parms[28]);
                }
                if ( (bool)parms[29] )
                {
                   stmt.setNull( 16 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(16, (int)parms[30]);
                }
                if ( (bool)parms[31] )
                {
                   stmt.setNull( 17 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(17, (int)parms[32]);
                }
                if ( (bool)parms[33] )
                {
                   stmt.setNull( 18 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(18, (int)parms[34]);
                }
                if ( (bool)parms[35] )
                {
                   stmt.setNull( 19 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(19, (int)parms[36]);
                }
                if ( (bool)parms[37] )
                {
                   stmt.setNull( 20 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(20, (int)parms[38]);
                }
                if ( (bool)parms[39] )
                {
                   stmt.setNull( 21 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(21, (int)parms[40]);
                }
                if ( (bool)parms[41] )
                {
                   stmt.setNull( 22 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(22, (int)parms[42]);
                }
                if ( (bool)parms[43] )
                {
                   stmt.setNull( 23 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(23, (int)parms[44]);
                }
                if ( (bool)parms[45] )
                {
                   stmt.setNull( 24 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(24, (int)parms[46]);
                }
                if ( (bool)parms[47] )
                {
                   stmt.setNull( 25 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(25, (int)parms[48]);
                }
                stmt.SetParameter(26, (int)parms[49]);
                return;
             case 9 :
                stmt.SetParameter(1, (int)parms[0]);
                if ( (bool)parms[1] )
                {
                   stmt.setNull( 2 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(2, (int)parms[2]);
                }
                if ( (bool)parms[3] )
                {
                   stmt.setNull( 3 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(3, (int)parms[4]);
                }
                if ( (bool)parms[5] )
                {
                   stmt.setNull( 4 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(4, (int)parms[6]);
                }
                if ( (bool)parms[7] )
                {
                   stmt.setNull( 5 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(5, (int)parms[8]);
                }
                if ( (bool)parms[9] )
                {
                   stmt.setNull( 6 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(6, (int)parms[10]);
                }
                if ( (bool)parms[11] )
                {
                   stmt.setNull( 7 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(7, (int)parms[12]);
                }
                if ( (bool)parms[13] )
                {
                   stmt.setNull( 8 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(8, (int)parms[14]);
                }
                if ( (bool)parms[15] )
                {
                   stmt.setNull( 9 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(9, (int)parms[16]);
                }
                if ( (bool)parms[17] )
                {
                   stmt.setNull( 10 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(10, (int)parms[18]);
                }
                if ( (bool)parms[19] )
                {
                   stmt.setNull( 11 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(11, (int)parms[20]);
                }
                if ( (bool)parms[21] )
                {
                   stmt.setNull( 12 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(12, (int)parms[22]);
                }
                if ( (bool)parms[23] )
                {
                   stmt.setNull( 13 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(13, (int)parms[24]);
                }
                if ( (bool)parms[25] )
                {
                   stmt.setNull( 14 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(14, (int)parms[26]);
                }
                if ( (bool)parms[27] )
                {
                   stmt.setNull( 15 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(15, (int)parms[28]);
                }
                if ( (bool)parms[29] )
                {
                   stmt.setNull( 16 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(16, (int)parms[30]);
                }
                if ( (bool)parms[31] )
                {
                   stmt.setNull( 17 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(17, (int)parms[32]);
                }
                if ( (bool)parms[33] )
                {
                   stmt.setNull( 18 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(18, (int)parms[34]);
                }
                if ( (bool)parms[35] )
                {
                   stmt.setNull( 19 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(19, (int)parms[36]);
                }
                if ( (bool)parms[37] )
                {
                   stmt.setNull( 20 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(20, (int)parms[38]);
                }
                if ( (bool)parms[39] )
                {
                   stmt.setNull( 21 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(21, (int)parms[40]);
                }
                if ( (bool)parms[41] )
                {
                   stmt.setNull( 22 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(22, (int)parms[42]);
                }
                if ( (bool)parms[43] )
                {
                   stmt.setNull( 23 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(23, (int)parms[44]);
                }
                if ( (bool)parms[45] )
                {
                   stmt.setNull( 24 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(24, (int)parms[46]);
                }
                if ( (bool)parms[47] )
                {
                   stmt.setNull( 25 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(25, (int)parms[48]);
                }
                stmt.SetParameter(26, (int)parms[49]);
                stmt.SetParameter(27, (int)parms[50]);
                return;
             case 10 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 12 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
       }
    }

    public String getDataStoreName( )
    {
       return "DATASTORE1";
    }

 }

 public class fyactividadplancantpart__default : DataStoreHelperBase, IDataStoreHelper
 {
    public ICursor[] getCursors( )
    {
       cursorDefinitions();
       return new Cursor[] {
     };
  }

  private static CursorDef[] def;
  private void cursorDefinitions( )
  {
     if ( def == null )
     {
        def= new CursorDef[] {
        };
     }
  }

  public void getResults( int cursor ,
                          IFieldGetter rslt ,
                          Object[] buf )
  {
     switch ( cursor )
     {
     }
  }

  public void setParameters( int cursor ,
                             IFieldSetter stmt ,
                             Object[] parms )
  {
     switch ( cursor )
     {
     }
  }

}

}
