/*
               File: Gx00I0
        Description: Selection List FYActividad Plan Cant Part
             Author: GeneXus C# Generator version 16_0_0-127771
       Generated on: 1/3/2019 18:1:50.66
       Program type: Callable routine
          Main DBMS: SQL Server
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Xml.Serialization;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class gx00i0 : GXDataArea, System.Web.SessionState.IRequiresSessionState
   {
      public gx00i0( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsDataStore1 = context.GetDataStore("DataStore1");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("Carmine");
      }

      public gx00i0( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsDataStore1 = context.GetDataStore("DataStore1");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( out int aP0_pFYActividadPlanCantPartid )
      {
         this.AV13pFYActividadPlanCantPartid = 0 ;
         executePrivate();
         aP0_pFYActividadPlanCantPartid=this.AV13pFYActividadPlanCantPartid;
      }

      void executePrivate( )
      {
         isStatic = false;
         webExecute();
      }

      protected override void createObjects( )
      {
      }

      protected void INITWEB( )
      {
         initialize_properties( ) ;
         if ( nGotPars == 0 )
         {
            entryPointCalled = false;
            gxfirstwebparm = GetNextPar( );
            gxfirstwebparm_bkp = gxfirstwebparm;
            gxfirstwebparm = DecryptAjaxCall( gxfirstwebparm);
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
            if ( StringUtil.StrCmp(gxfirstwebparm, "dyncall") == 0 )
            {
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               dyncall( GetNextPar( )) ;
               return  ;
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxEvt") == 0 )
            {
               setAjaxEventMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = GetNextPar( );
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxfullajaxEvt") == 0 )
            {
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = GetNextPar( );
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxNewRow_"+"Grid1") == 0 )
            {
               nRC_GXsfl_84 = (short)(NumberUtil.Val( GetNextPar( ), "."));
               nGXsfl_84_idx = (short)(NumberUtil.Val( GetNextPar( ), "."));
               sGXsfl_84_idx = GetNextPar( );
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxnrGrid1_newrow( ) ;
               return  ;
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxGridRefresh_"+"Grid1") == 0 )
            {
               subGrid1_Rows = (int)(NumberUtil.Val( GetNextPar( ), "."));
               AV6cFYActividadPlanCantPartid = (int)(NumberUtil.Val( GetNextPar( ), "."));
               AV7cActividadParaPlanid = (int)(NumberUtil.Val( GetNextPar( ), "."));
               AV8cFYActividadPlanCantPartFY = (int)(NumberUtil.Val( GetNextPar( ), "."));
               AV9cmes1H = (int)(NumberUtil.Val( GetNextPar( ), "."));
               AV10cmes1M = (int)(NumberUtil.Val( GetNextPar( ), "."));
               AV11cmes2H = (int)(NumberUtil.Val( GetNextPar( ), "."));
               AV12cmes2M = (int)(NumberUtil.Val( GetNextPar( ), "."));
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxgrGrid1_refresh( subGrid1_Rows, AV6cFYActividadPlanCantPartid, AV7cActividadParaPlanid, AV8cFYActividadPlanCantPartFY, AV9cmes1H, AV10cmes1M, AV11cmes2H, AV12cmes2M) ;
               GXKey = Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
               GXKey = Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
               context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               return  ;
            }
            else
            {
               if ( ! IsValidAjaxCall( false) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = gxfirstwebparm_bkp;
            }
            if ( ! entryPointCalled && ! ( isAjaxCallMode( ) || isFullAjaxMode( ) ) )
            {
               AV13pFYActividadPlanCantPartid = (int)(NumberUtil.Val( gxfirstwebparm, "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13pFYActividadPlanCantPartid", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13pFYActividadPlanCantPartid), 9, 0)));
            }
            if ( context.isSpaRequest( ) )
            {
               enableJsOutput();
            }
         }
         if ( ! context.IsLocalStorageSupported( ) )
         {
            context.PushCurrentUrl();
         }
      }

      public override void webExecute( )
      {
         if ( initialized == 0 )
         {
            createObjects();
            initialize();
         }
         INITWEB( ) ;
         if ( ! isAjaxCallMode( ) )
         {
            MasterPageObj = (GXMasterPage) ClassLoader.GetInstance("rwdpromptmasterpage", "GeneXus.Programs.rwdpromptmasterpage", new Object[] {new GxContext( context.handle, context.DataStores, context.HttpContext)});
            MasterPageObj.setDataArea(this,true);
            ValidateSpaRequest();
            MasterPageObj.webExecute();
            if ( ( GxWebError == 0 ) && context.isAjaxRequest( ) )
            {
               enableOutput();
               if ( ! context.isAjaxRequest( ) )
               {
                  context.GX_webresponse.AppendHeader("Cache-Control", "no-store");
               }
               if ( ! context.WillRedirect( ) )
               {
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               }
               else
               {
                  if ( context.isAjaxRequest( ) )
                  {
                     disableOutput();
                  }
                  RenderHtmlHeaders( ) ;
                  context.Redirect( context.wjLoc );
                  context.DispatchAjaxCommands();
               }
            }
         }
         this.cleanup();
      }

      public override short ExecuteStartEvent( )
      {
         PA0S2( ) ;
         gxajaxcallmode = (short)((isAjaxCallMode( ) ? 1 : 0));
         if ( ( gxajaxcallmode == 0 ) && ( GxWebError == 0 ) )
         {
            START0S2( ) ;
         }
         return gxajaxcallmode ;
      }

      public override void RenderHtmlHeaders( )
      {
         GxWebStd.gx_html_headers( context, 0, "", "", Form.Meta, Form.Metaequiv, true);
      }

      public override void RenderHtmlOpenForm( )
      {
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.WriteHtmlText( "<title>") ;
         context.SendWebValue( Form.Caption) ;
         context.WriteHtmlTextNl( "</title>") ;
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         if ( StringUtil.Len( sDynURL) > 0 )
         {
            context.WriteHtmlText( "<BASE href=\""+sDynURL+"\" />") ;
         }
         define_styles( ) ;
         if ( nGXWrapped != 1 )
         {
            MasterPageObj.master_styles();
         }
         if ( ( ( context.GetBrowserType( ) == 1 ) || ( context.GetBrowserType( ) == 5 ) ) && ( StringUtil.StrCmp(context.GetBrowserVersion( ), "7.0") == 0 ) )
         {
            context.AddJavascriptSource("json2.js", "?"+context.GetBuildNumber( 127771), false);
         }
         context.AddJavascriptSource("jquery.js", "?"+context.GetBuildNumber( 127771), false);
         context.AddJavascriptSource("gxtimezone.js", "?"+context.GetBuildNumber( 127771), false);
         context.AddJavascriptSource("gxgral.js", "?"+context.GetBuildNumber( 127771), false);
         context.AddJavascriptSource("gxcfg.js", "?2019131815079", false);
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.WriteHtmlText( Form.Headerrawhtml) ;
         context.CloseHtmlHeader();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         FormProcess = " data-HasEnter=\"true\" data-Skiponenter=\"false\"";
         context.WriteHtmlText( "<body ") ;
         bodyStyle = "" + "background-color:" + context.BuildHTMLColor( Form.Backcolor) + ";color:" + context.BuildHTMLColor( Form.Textcolor) + ";";
         if ( nGXWrapped == 0 )
         {
            bodyStyle = bodyStyle + "-moz-opacity:0;opacity:0;";
         }
         if ( ! ( String.IsNullOrEmpty(StringUtil.RTrim( Form.Background)) ) )
         {
            bodyStyle = bodyStyle + " background-image:url(" + context.convertURL( Form.Background) + ")";
         }
         context.WriteHtmlText( " "+"class=\"form-horizontal Form\""+" "+ "style='"+bodyStyle+"'") ;
         context.WriteHtmlText( FormProcess+">") ;
         context.skipLines(1);
         context.WriteHtmlTextNl( "<form id=\"MAINFORM\" autocomplete=\"off\" name=\"MAINFORM\" method=\"post\" tabindex=-1  class=\"form-horizontal Form\" data-gx-class=\"form-horizontal Form\" novalidate action=\""+formatLink("gx00i0.aspx") + "?" + UrlEncode("" +AV13pFYActividadPlanCantPartid)+"\">") ;
         GxWebStd.gx_hidden_field( context, "_EventName", "");
         GxWebStd.gx_hidden_field( context, "_EventGridId", "");
         GxWebStd.gx_hidden_field( context, "_EventRowId", "");
         context.WriteHtmlText( "<input type=\"submit\" title=\"submit\" style=\"display:none\" disabled>") ;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, "FORM", "Class", "form-horizontal Form", true);
         toggleJsOutput = isJsOutputEnabled( );
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
      }

      protected void send_integrity_footer_hashes( )
      {
         GXKey = Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
      }

      protected void SendCloseFormHiddens( )
      {
         /* Send hidden variables. */
         GxWebStd.gx_hidden_field( context, "GXH_vCFYACTIVIDADPLANCANTPARTID", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV6cFYActividadPlanCantPartid), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vCACTIVIDADPARAPLANID", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV7cActividadParaPlanid), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vCFYACTIVIDADPLANCANTPARTFY", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV8cFYActividadPlanCantPartFY), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vCMES1H", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV9cmes1H), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vCMES1M", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV10cmes1M), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vCMES2H", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV11cmes2H), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vCMES2M", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV12cmes2M), 9, 0, ".", "")));
         /* Send saved values. */
         send_integrity_footer_hashes( ) ;
         GxWebStd.gx_hidden_field( context, "nRC_GXsfl_84", StringUtil.LTrim( StringUtil.NToC( (decimal)(nRC_GXsfl_84), 4, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "vPFYACTIVIDADPLANCANTPARTID", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV13pFYActividadPlanCantPartid), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "GRID1_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID1_nFirstRecordOnPage), 15, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "GRID1_nEOF", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID1_nEOF), 1, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "ADVANCEDCONTAINER_Class", StringUtil.RTrim( divAdvancedcontainer_Class));
         GxWebStd.gx_hidden_field( context, "BTNTOGGLE_Class", StringUtil.RTrim( bttBtntoggle_Class));
         GxWebStd.gx_hidden_field( context, "FYACTIVIDADPLANCANTPARTIDFILTERCONTAINER_Class", StringUtil.RTrim( divFyactividadplancantpartidfiltercontainer_Class));
         GxWebStd.gx_hidden_field( context, "ACTIVIDADPARAPLANIDFILTERCONTAINER_Class", StringUtil.RTrim( divActividadparaplanidfiltercontainer_Class));
         GxWebStd.gx_hidden_field( context, "FYACTIVIDADPLANCANTPARTFYFILTERCONTAINER_Class", StringUtil.RTrim( divFyactividadplancantpartfyfiltercontainer_Class));
         GxWebStd.gx_hidden_field( context, "MES1HFILTERCONTAINER_Class", StringUtil.RTrim( divMes1hfiltercontainer_Class));
         GxWebStd.gx_hidden_field( context, "MES1MFILTERCONTAINER_Class", StringUtil.RTrim( divMes1mfiltercontainer_Class));
         GxWebStd.gx_hidden_field( context, "MES2HFILTERCONTAINER_Class", StringUtil.RTrim( divMes2hfiltercontainer_Class));
         GxWebStd.gx_hidden_field( context, "MES2MFILTERCONTAINER_Class", StringUtil.RTrim( divMes2mfiltercontainer_Class));
      }

      public override void RenderHtmlCloseForm( )
      {
         SendCloseFormHiddens( ) ;
         GxWebStd.gx_hidden_field( context, "GX_FocusControl", "notset");
         SendAjaxEncryptionKey();
         SendSecurityToken((String)(sPrefix));
         SendComponentObjects();
         SendServerCommands();
         SendState();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         context.WriteHtmlTextNl( "</form>") ;
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         include_jscripts( ) ;
      }

      public override void RenderHtmlContent( )
      {
         gxajaxcallmode = (short)((isAjaxCallMode( ) ? 1 : 0));
         if ( ( gxajaxcallmode == 0 ) && ( GxWebError == 0 ) )
         {
            context.WriteHtmlText( "<div") ;
            GxWebStd.ClassAttribute( context, "gx-ct-body"+" "+(String.IsNullOrEmpty(StringUtil.RTrim( Form.Class)) ? "form-horizontal Form" : Form.Class)+"-fx");
            context.WriteHtmlText( ">") ;
            WE0S2( ) ;
            context.WriteHtmlText( "</div>") ;
         }
      }

      public override void DispatchEvents( )
      {
         EVT0S2( ) ;
      }

      public override bool HasEnterEvent( )
      {
         return true ;
      }

      public override GXWebForm GetForm( )
      {
         return Form ;
      }

      public override String GetSelfLink( )
      {
         return formatLink("gx00i0.aspx") + "?" + UrlEncode("" +AV13pFYActividadPlanCantPartid) ;
      }

      public override String GetPgmname( )
      {
         return "Gx00I0" ;
      }

      public override String GetPgmdesc( )
      {
         return "Selection List FYActividad Plan Cant Part" ;
      }

      protected void WB0S0( )
      {
         if ( context.isAjaxRequest( ) )
         {
            disableOutput();
         }
         if ( ! wbLoad )
         {
            if ( nGXWrapped == 1 )
            {
               RenderHtmlHeaders( ) ;
               RenderHtmlOpenForm( ) ;
            }
            GxWebStd.gx_msg_list( context, "", context.GX_msglist.DisplayMode, "", "", "", "false");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "Section", "left", "top", " "+"data-gx-base-lib=\"bootstrapv3\""+" "+"data-abstract-form"+" ", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, divMain_Internalname, 1, 0, "px", 0, "px", "ContainerFluid PromptContainer", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12 col-sm-3 PromptAdvancedBarCell", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, divAdvancedcontainer_Internalname, 1, 0, "px", 0, "px", divAdvancedcontainer_Class, "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, divFyactividadplancantpartidfiltercontainer_Internalname, 1, 0, "px", 0, "px", divFyactividadplancantpartidfiltercontainer_Class, "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12", "left", "top", "", "", "div");
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblLblfyactividadplancantpartidfilter_Internalname, "FYActividad Plan Cant Partid", "", "", lblLblfyactividadplancantpartidfilter_Jsonclick, "'"+""+"'"+",false,"+"'"+"e110s1_client"+"'", "", "WWAdvancedLabel WWFilterLabel", 7, "", 1, 1, 1, "HLP_Gx00I0.htm");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12 WWFiltersCell", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", " gx-attribute", "left", "top", "", "", "div");
            /* Attribute/Variable Label */
            GxWebStd.gx_label_element( context, edtavCfyactividadplancantpartid_Internalname, "id", "col-sm-3 AttributeLabel", 0, true);
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 16,'',false,'" + sGXsfl_84_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavCfyactividadplancantpartid_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV6cFYActividadPlanCantPartid), 9, 0, ".", "")), ((edtavCfyactividadplancantpartid_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( (decimal)(AV6cFYActividadPlanCantPartid), "ZZZZZZZZ9")) : context.localUtil.Format( (decimal)(AV6cFYActividadPlanCantPartid), "ZZZZZZZZ9")), TempTags+" onchange=\""+"gx.num.valid_integer( this,',');"+";gx.evt.onchange(this, event)\" "+" onblur=\""+";gx.evt.onblur(this,16);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavCfyactividadplancantpartid_Jsonclick, 0, "Attribute", "", "", "", "", edtavCfyactividadplancantpartid_Visible, edtavCfyactividadplancantpartid_Enabled, 0, "number", "1", 9, "chr", 1, "row", 9, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_Gx00I0.htm");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, divActividadparaplanidfiltercontainer_Internalname, 1, 0, "px", 0, "px", divActividadparaplanidfiltercontainer_Class, "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12", "left", "top", "", "", "div");
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblLblactividadparaplanidfilter_Internalname, "Actividad Para Planid", "", "", lblLblactividadparaplanidfilter_Jsonclick, "'"+""+"'"+",false,"+"'"+"e120s1_client"+"'", "", "WWAdvancedLabel WWFilterLabel", 7, "", 1, 1, 1, "HLP_Gx00I0.htm");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12 WWFiltersCell", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", " gx-attribute", "left", "top", "", "", "div");
            /* Attribute/Variable Label */
            GxWebStd.gx_label_element( context, edtavCactividadparaplanid_Internalname, "id", "col-sm-3 AttributeLabel", 0, true);
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 26,'',false,'" + sGXsfl_84_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavCactividadparaplanid_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV7cActividadParaPlanid), 9, 0, ".", "")), ((edtavCactividadparaplanid_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( (decimal)(AV7cActividadParaPlanid), "ZZZZZZZZ9")) : context.localUtil.Format( (decimal)(AV7cActividadParaPlanid), "ZZZZZZZZ9")), TempTags+" onchange=\""+"gx.num.valid_integer( this,',');"+";gx.evt.onchange(this, event)\" "+" onblur=\""+";gx.evt.onblur(this,26);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavCactividadparaplanid_Jsonclick, 0, "Attribute", "", "", "", "", edtavCactividadparaplanid_Visible, edtavCactividadparaplanid_Enabled, 0, "number", "1", 9, "chr", 1, "row", 9, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_Gx00I0.htm");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, divFyactividadplancantpartfyfiltercontainer_Internalname, 1, 0, "px", 0, "px", divFyactividadplancantpartfyfiltercontainer_Class, "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12", "left", "top", "", "", "div");
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblLblfyactividadplancantpartfyfilter_Internalname, "FYActividad Plan Cant Part FY", "", "", lblLblfyactividadplancantpartfyfilter_Jsonclick, "'"+""+"'"+",false,"+"'"+"e130s1_client"+"'", "", "WWAdvancedLabel WWFilterLabel", 7, "", 1, 1, 1, "HLP_Gx00I0.htm");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12 WWFiltersCell", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", " gx-attribute", "left", "top", "", "", "div");
            /* Attribute/Variable Label */
            GxWebStd.gx_label_element( context, edtavCfyactividadplancantpartfy_Internalname, "FY", "col-sm-3 AttributeLabel", 0, true);
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 36,'',false,'" + sGXsfl_84_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavCfyactividadplancantpartfy_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV8cFYActividadPlanCantPartFY), 9, 0, ".", "")), ((edtavCfyactividadplancantpartfy_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( (decimal)(AV8cFYActividadPlanCantPartFY), "ZZZZZZZZ9")) : context.localUtil.Format( (decimal)(AV8cFYActividadPlanCantPartFY), "ZZZZZZZZ9")), TempTags+" onchange=\""+"gx.num.valid_integer( this,',');"+";gx.evt.onchange(this, event)\" "+" onblur=\""+";gx.evt.onblur(this,36);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavCfyactividadplancantpartfy_Jsonclick, 0, "Attribute", "", "", "", "", edtavCfyactividadplancantpartfy_Visible, edtavCfyactividadplancantpartfy_Enabled, 0, "number", "1", 9, "chr", 1, "row", 9, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_Gx00I0.htm");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, divMes1hfiltercontainer_Internalname, 1, 0, "px", 0, "px", divMes1hfiltercontainer_Class, "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12", "left", "top", "", "", "div");
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblLblmes1hfilter_Internalname, "mes1H", "", "", lblLblmes1hfilter_Jsonclick, "'"+""+"'"+",false,"+"'"+"e140s1_client"+"'", "", "WWAdvancedLabel WWFilterLabel", 7, "", 1, 1, 1, "HLP_Gx00I0.htm");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12 WWFiltersCell", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", " gx-attribute", "left", "top", "", "", "div");
            /* Attribute/Variable Label */
            GxWebStd.gx_label_element( context, edtavCmes1h_Internalname, "mes1H", "col-sm-3 AttributeLabel", 0, true);
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 46,'',false,'" + sGXsfl_84_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavCmes1h_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV9cmes1H), 9, 0, ".", "")), ((edtavCmes1h_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( (decimal)(AV9cmes1H), "ZZZZZZZZ9")) : context.localUtil.Format( (decimal)(AV9cmes1H), "ZZZZZZZZ9")), TempTags+" onchange=\""+"gx.num.valid_integer( this,',');"+";gx.evt.onchange(this, event)\" "+" onblur=\""+";gx.evt.onblur(this,46);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavCmes1h_Jsonclick, 0, "Attribute", "", "", "", "", edtavCmes1h_Visible, edtavCmes1h_Enabled, 0, "number", "1", 9, "chr", 1, "row", 9, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_Gx00I0.htm");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, divMes1mfiltercontainer_Internalname, 1, 0, "px", 0, "px", divMes1mfiltercontainer_Class, "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12", "left", "top", "", "", "div");
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblLblmes1mfilter_Internalname, "mes1M", "", "", lblLblmes1mfilter_Jsonclick, "'"+""+"'"+",false,"+"'"+"e150s1_client"+"'", "", "WWAdvancedLabel WWFilterLabel", 7, "", 1, 1, 1, "HLP_Gx00I0.htm");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12 WWFiltersCell", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", " gx-attribute", "left", "top", "", "", "div");
            /* Attribute/Variable Label */
            GxWebStd.gx_label_element( context, edtavCmes1m_Internalname, "mes1M", "col-sm-3 AttributeLabel", 0, true);
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 56,'',false,'" + sGXsfl_84_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavCmes1m_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV10cmes1M), 9, 0, ".", "")), ((edtavCmes1m_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( (decimal)(AV10cmes1M), "ZZZZZZZZ9")) : context.localUtil.Format( (decimal)(AV10cmes1M), "ZZZZZZZZ9")), TempTags+" onchange=\""+"gx.num.valid_integer( this,',');"+";gx.evt.onchange(this, event)\" "+" onblur=\""+";gx.evt.onblur(this,56);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavCmes1m_Jsonclick, 0, "Attribute", "", "", "", "", edtavCmes1m_Visible, edtavCmes1m_Enabled, 0, "number", "1", 9, "chr", 1, "row", 9, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_Gx00I0.htm");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, divMes2hfiltercontainer_Internalname, 1, 0, "px", 0, "px", divMes2hfiltercontainer_Class, "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12", "left", "top", "", "", "div");
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblLblmes2hfilter_Internalname, "mes2H", "", "", lblLblmes2hfilter_Jsonclick, "'"+""+"'"+",false,"+"'"+"e160s1_client"+"'", "", "WWAdvancedLabel WWFilterLabel", 7, "", 1, 1, 1, "HLP_Gx00I0.htm");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12 WWFiltersCell", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", " gx-attribute", "left", "top", "", "", "div");
            /* Attribute/Variable Label */
            GxWebStd.gx_label_element( context, edtavCmes2h_Internalname, "mes2H", "col-sm-3 AttributeLabel", 0, true);
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 66,'',false,'" + sGXsfl_84_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavCmes2h_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV11cmes2H), 9, 0, ".", "")), ((edtavCmes2h_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( (decimal)(AV11cmes2H), "ZZZZZZZZ9")) : context.localUtil.Format( (decimal)(AV11cmes2H), "ZZZZZZZZ9")), TempTags+" onchange=\""+"gx.num.valid_integer( this,',');"+";gx.evt.onchange(this, event)\" "+" onblur=\""+";gx.evt.onblur(this,66);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavCmes2h_Jsonclick, 0, "Attribute", "", "", "", "", edtavCmes2h_Visible, edtavCmes2h_Enabled, 0, "number", "1", 9, "chr", 1, "row", 9, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_Gx00I0.htm");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, divMes2mfiltercontainer_Internalname, 1, 0, "px", 0, "px", divMes2mfiltercontainer_Class, "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12", "left", "top", "", "", "div");
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblLblmes2mfilter_Internalname, "mes2M", "", "", lblLblmes2mfilter_Jsonclick, "'"+""+"'"+",false,"+"'"+"e170s1_client"+"'", "", "WWAdvancedLabel WWFilterLabel", 7, "", 1, 1, 1, "HLP_Gx00I0.htm");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12 WWFiltersCell", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", " gx-attribute", "left", "top", "", "", "div");
            /* Attribute/Variable Label */
            GxWebStd.gx_label_element( context, edtavCmes2m_Internalname, "mes2M", "col-sm-3 AttributeLabel", 0, true);
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 76,'',false,'" + sGXsfl_84_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavCmes2m_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV12cmes2M), 9, 0, ".", "")), ((edtavCmes2m_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( (decimal)(AV12cmes2M), "ZZZZZZZZ9")) : context.localUtil.Format( (decimal)(AV12cmes2M), "ZZZZZZZZ9")), TempTags+" onchange=\""+"gx.num.valid_integer( this,',');"+";gx.evt.onchange(this, event)\" "+" onblur=\""+";gx.evt.onblur(this,76);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavCmes2m_Jsonclick, 0, "Attribute", "", "", "", "", edtavCmes2m_Visible, edtavCmes2m_Enabled, 0, "number", "1", 9, "chr", 1, "row", 9, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_Gx00I0.htm");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12 col-sm-9 WWGridCell", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, divGridtable_Internalname, 1, 0, "px", 0, "px", "Table", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12 hidden-sm hidden-md hidden-lg ToggleCell", "left", "top", "", "", "div");
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 81,'',false,'',0)\"";
            ClassString = bttBtntoggle_Class;
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtntoggle_Internalname, "gx.evt.setGridEvt("+StringUtil.Str( (decimal)(84), 2, 0)+","+"null"+");", "|||", bttBtntoggle_Jsonclick, 7, "|||", "", StyleString, ClassString, 1, 1, "standard", "'"+""+"'"+",false,"+"'"+"e180s1_client"+"'", TempTags, "", 2, "HLP_Gx00I0.htm");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12", "left", "top", "", "", "div");
            /*  Grid Control  */
            Grid1Container.SetWrapped(nGXWrapped);
            if ( Grid1Container.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<div id=\""+"Grid1Container"+"DivS\" data-gxgridid=\"84\">") ;
               sStyleString = "";
               GxWebStd.gx_table_start( context, subGrid1_Internalname, subGrid1_Internalname, "", "PromptGrid", 0, "", "", 1, 2, sStyleString, "", "", 0);
               /* Subfile titles */
               context.WriteHtmlText( "<tr") ;
               context.WriteHtmlTextNl( ">") ;
               if ( subGrid1_Backcolorstyle == 0 )
               {
                  subGrid1_Titlebackstyle = 0;
                  if ( StringUtil.Len( subGrid1_Class) > 0 )
                  {
                     subGrid1_Linesclass = subGrid1_Class+"Title";
                  }
               }
               else
               {
                  subGrid1_Titlebackstyle = 1;
                  if ( subGrid1_Backcolorstyle == 1 )
                  {
                     subGrid1_Titlebackcolor = subGrid1_Allbackcolor;
                     if ( StringUtil.Len( subGrid1_Class) > 0 )
                     {
                        subGrid1_Linesclass = subGrid1_Class+"UniformTitle";
                     }
                  }
                  else
                  {
                     if ( StringUtil.Len( subGrid1_Class) > 0 )
                     {
                        subGrid1_Linesclass = subGrid1_Class+"Title";
                     }
                  }
               }
               context.WriteHtmlText( "<th align=\""+""+"\" "+" nowrap=\"nowrap\" "+" class=\""+"SelectionAttribute"+"\" "+" style=\""+""+""+"\" "+">") ;
               context.SendWebValue( "") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+"Attribute"+"\" "+" style=\""+""+""+"\" "+">") ;
               context.SendWebValue( "id") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+"Attribute"+"\" "+" style=\""+""+""+"\" "+">") ;
               context.SendWebValue( "Actividad Para Planid") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+"DescriptionAttribute"+"\" "+" style=\""+""+""+"\" "+">") ;
               context.SendWebValue( "FY") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+"Attribute"+"\" "+" style=\""+""+""+"\" "+">") ;
               context.SendWebValue( "mes1H") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+"Attribute"+"\" "+" style=\""+""+""+"\" "+">") ;
               context.SendWebValue( "mes1M") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+"Attribute"+"\" "+" style=\""+""+""+"\" "+">") ;
               context.SendWebValue( "mes2H") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+"Attribute"+"\" "+" style=\""+""+""+"\" "+">") ;
               context.SendWebValue( "mes2M") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+"Attribute"+"\" "+" style=\""+""+""+"\" "+">") ;
               context.SendWebValue( "mes3H") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlTextNl( "</tr>") ;
               Grid1Container.AddObjectProperty("GridName", "Grid1");
            }
            else
            {
               if ( isAjaxCallMode( ) )
               {
                  Grid1Container = new GXWebGrid( context);
               }
               else
               {
                  Grid1Container.Clear();
               }
               Grid1Container.SetWrapped(nGXWrapped);
               Grid1Container.AddObjectProperty("GridName", "Grid1");
               Grid1Container.AddObjectProperty("Header", subGrid1_Header);
               Grid1Container.AddObjectProperty("Class", "PromptGrid");
               Grid1Container.AddObjectProperty("Cellpadding", StringUtil.LTrim( StringUtil.NToC( (decimal)(1), 4, 0, ".", "")));
               Grid1Container.AddObjectProperty("Cellspacing", StringUtil.LTrim( StringUtil.NToC( (decimal)(2), 4, 0, ".", "")));
               Grid1Container.AddObjectProperty("Backcolorstyle", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid1_Backcolorstyle), 1, 0, ".", "")));
               Grid1Container.AddObjectProperty("CmpContext", "");
               Grid1Container.AddObjectProperty("InMasterPage", "false");
               Grid1Column = GXWebColumn.GetNew(isAjaxCallMode( ));
               Grid1Column.AddObjectProperty("Value", context.convertURL( AV5LinkSelection));
               Grid1Column.AddObjectProperty("Link", StringUtil.RTrim( edtavLinkselection_Link));
               Grid1Container.AddColumnProperties(Grid1Column);
               Grid1Column = GXWebColumn.GetNew(isAjaxCallMode( ));
               Grid1Column.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(A14FYActividadPlanCantPartid), 9, 0, ".", "")));
               Grid1Container.AddColumnProperties(Grid1Column);
               Grid1Column = GXWebColumn.GetNew(isAjaxCallMode( ));
               Grid1Column.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(A23ActividadParaPlanid), 9, 0, ".", "")));
               Grid1Container.AddColumnProperties(Grid1Column);
               Grid1Column = GXWebColumn.GetNew(isAjaxCallMode( ));
               Grid1Column.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(A79FYActividadPlanCantPartFY), 9, 0, ".", "")));
               Grid1Column.AddObjectProperty("Link", StringUtil.RTrim( edtFYActividadPlanCantPartFY_Link));
               Grid1Container.AddColumnProperties(Grid1Column);
               Grid1Column = GXWebColumn.GetNew(isAjaxCallMode( ));
               Grid1Column.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(A80mes1H), 9, 0, ".", "")));
               Grid1Container.AddColumnProperties(Grid1Column);
               Grid1Column = GXWebColumn.GetNew(isAjaxCallMode( ));
               Grid1Column.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(A81mes1M), 9, 0, ".", "")));
               Grid1Container.AddColumnProperties(Grid1Column);
               Grid1Column = GXWebColumn.GetNew(isAjaxCallMode( ));
               Grid1Column.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(A82mes2H), 9, 0, ".", "")));
               Grid1Container.AddColumnProperties(Grid1Column);
               Grid1Column = GXWebColumn.GetNew(isAjaxCallMode( ));
               Grid1Column.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(A83mes2M), 9, 0, ".", "")));
               Grid1Container.AddColumnProperties(Grid1Column);
               Grid1Column = GXWebColumn.GetNew(isAjaxCallMode( ));
               Grid1Column.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(A84mes3H), 9, 0, ".", "")));
               Grid1Container.AddColumnProperties(Grid1Column);
               Grid1Container.AddObjectProperty("Selectedindex", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid1_Selectedindex), 4, 0, ".", "")));
               Grid1Container.AddObjectProperty("Allowselection", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid1_Allowselection), 1, 0, ".", "")));
               Grid1Container.AddObjectProperty("Selectioncolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid1_Selectioncolor), 9, 0, ".", "")));
               Grid1Container.AddObjectProperty("Allowhover", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid1_Allowhovering), 1, 0, ".", "")));
               Grid1Container.AddObjectProperty("Hovercolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid1_Hoveringcolor), 9, 0, ".", "")));
               Grid1Container.AddObjectProperty("Allowcollapsing", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid1_Allowcollapsing), 1, 0, ".", "")));
               Grid1Container.AddObjectProperty("Collapsed", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid1_Collapsed), 1, 0, ".", "")));
            }
         }
         if ( wbEnd == 84 )
         {
            wbEnd = 0;
            nRC_GXsfl_84 = (short)(nGXsfl_84_idx-1);
            if ( Grid1Container.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "</table>") ;
               context.WriteHtmlText( "</div>") ;
            }
            else
            {
               Grid1Container.AddObjectProperty("GRID1_nEOF", GRID1_nEOF);
               Grid1Container.AddObjectProperty("GRID1_nFirstRecordOnPage", GRID1_nFirstRecordOnPage);
               sStyleString = "";
               context.WriteHtmlText( "<div id=\""+"Grid1Container"+"Div\" "+sStyleString+">"+"</div>") ;
               context.httpAjaxContext.ajax_rsp_assign_grid("_"+"Grid1", Grid1Container);
               if ( ! context.isAjaxRequest( ) && ! context.isSpaRequest( ) )
               {
                  GxWebStd.gx_hidden_field( context, "Grid1ContainerData", Grid1Container.ToJavascriptSource());
               }
               if ( context.isAjaxRequest( ) || context.isSpaRequest( ) )
               {
                  GxWebStd.gx_hidden_field( context, "Grid1ContainerData"+"V", Grid1Container.GridValuesHidden());
               }
               else
               {
                  context.WriteHtmlText( "<input type=\"hidden\" "+"name=\""+"Grid1ContainerData"+"V"+"\" value='"+Grid1Container.GridValuesHidden()+"'/>") ;
               }
            }
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12", "left", "top", "", "", "div");
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 96,'',false,'',0)\"";
            ClassString = "BtnCancel";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtn_cancel_Internalname, "gx.evt.setGridEvt("+StringUtil.Str( (decimal)(84), 2, 0)+","+"null"+");", "Cancel", bttBtn_cancel_Jsonclick, 1, "Cancel", "", StyleString, ClassString, 1, 1, "standard", "'"+""+"'"+",false,"+"'"+"ECANCEL."+"'", TempTags, "", context.GetButtonType( ), "HLP_Gx00I0.htm");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
         }
         if ( wbEnd == 84 )
         {
            wbEnd = 0;
            if ( isFullAjaxMode( ) )
            {
               if ( Grid1Container.GetWrapped() == 1 )
               {
                  context.WriteHtmlText( "</table>") ;
                  context.WriteHtmlText( "</div>") ;
               }
               else
               {
                  Grid1Container.AddObjectProperty("GRID1_nEOF", GRID1_nEOF);
                  Grid1Container.AddObjectProperty("GRID1_nFirstRecordOnPage", GRID1_nFirstRecordOnPage);
                  sStyleString = "";
                  context.WriteHtmlText( "<div id=\""+"Grid1Container"+"Div\" "+sStyleString+">"+"</div>") ;
                  context.httpAjaxContext.ajax_rsp_assign_grid("_"+"Grid1", Grid1Container);
                  if ( ! context.isAjaxRequest( ) && ! context.isSpaRequest( ) )
                  {
                     GxWebStd.gx_hidden_field( context, "Grid1ContainerData", Grid1Container.ToJavascriptSource());
                  }
                  if ( context.isAjaxRequest( ) || context.isSpaRequest( ) )
                  {
                     GxWebStd.gx_hidden_field( context, "Grid1ContainerData"+"V", Grid1Container.GridValuesHidden());
                  }
                  else
                  {
                     context.WriteHtmlText( "<input type=\"hidden\" "+"name=\""+"Grid1ContainerData"+"V"+"\" value='"+Grid1Container.GridValuesHidden()+"'/>") ;
                  }
               }
            }
         }
         wbLoad = true;
      }

      protected void START0S2( )
      {
         wbLoad = false;
         wbEnd = 0;
         wbStart = 0;
         if ( ! context.isSpaRequest( ) )
         {
            Form.Meta.addItem("generator", "GeneXus C# 16_0_0-127771", 0) ;
            Form.Meta.addItem("description", "Selection List FYActividad Plan Cant Part", 0) ;
         }
         context.wjLoc = "";
         context.nUserReturn = 0;
         context.wbHandled = 0;
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
         }
         wbErr = false;
         STRUP0S0( ) ;
      }

      protected void WS0S2( )
      {
         START0S2( ) ;
         EVT0S2( ) ;
      }

      protected void EVT0S2( )
      {
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            if ( ! context.WillRedirect( ) && ( context.nUserReturn != 1 ) && ! wbErr )
            {
               /* Read Web Panel buttons. */
               sEvt = cgiGet( "_EventName");
               EvtGridId = cgiGet( "_EventGridId");
               EvtRowId = cgiGet( "_EventRowId");
               if ( StringUtil.Len( sEvt) > 0 )
               {
                  sEvtType = StringUtil.Left( sEvt, 1);
                  sEvt = StringUtil.Right( sEvt, (short)(StringUtil.Len( sEvt)-1));
                  if ( StringUtil.StrCmp(sEvtType, "M") != 0 )
                  {
                     if ( StringUtil.StrCmp(sEvtType, "E") == 0 )
                     {
                        sEvtType = StringUtil.Right( sEvt, 1);
                        if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                        {
                           sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                           if ( StringUtil.StrCmp(sEvt, "RFR") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* No code required for Cancel button. It is implemented as the Reset button. */
                           }
                           else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                           }
                           else if ( StringUtil.StrCmp(sEvt, "GRID1PAGING") == 0 )
                           {
                              context.wbHandled = 1;
                              sEvt = cgiGet( "GRID1PAGING");
                              if ( StringUtil.StrCmp(sEvt, "FIRST") == 0 )
                              {
                                 subgrid1_firstpage( ) ;
                              }
                              else if ( StringUtil.StrCmp(sEvt, "PREV") == 0 )
                              {
                                 subgrid1_previouspage( ) ;
                              }
                              else if ( StringUtil.StrCmp(sEvt, "NEXT") == 0 )
                              {
                                 subgrid1_nextpage( ) ;
                              }
                              else if ( StringUtil.StrCmp(sEvt, "LAST") == 0 )
                              {
                                 subgrid1_lastpage( ) ;
                              }
                              dynload_actions( ) ;
                           }
                        }
                        else
                        {
                           sEvtType = StringUtil.Right( sEvt, 4);
                           sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-4));
                           if ( ( StringUtil.StrCmp(StringUtil.Left( sEvt, 5), "START") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 4), "LOAD") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 5), "ENTER") == 0 ) )
                           {
                              nGXsfl_84_idx = (short)(NumberUtil.Val( sEvtType, "."));
                              sGXsfl_84_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_84_idx), 4, 0)), 4, "0");
                              SubsflControlProps_842( ) ;
                              AV5LinkSelection = cgiGet( edtavLinkselection_Internalname);
                              context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavLinkselection_Internalname, "Bitmap", (String.IsNullOrEmpty(StringUtil.RTrim( AV5LinkSelection)) ? AV17Linkselection_GXI : context.convertURL( context.PathToRelativeUrl( AV5LinkSelection))), !bGXsfl_84_Refreshing);
                              context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavLinkselection_Internalname, "SrcSet", context.GetImageSrcSet( AV5LinkSelection), true);
                              A14FYActividadPlanCantPartid = (int)(context.localUtil.CToN( cgiGet( edtFYActividadPlanCantPartid_Internalname), ".", ","));
                              A23ActividadParaPlanid = (int)(context.localUtil.CToN( cgiGet( edtActividadParaPlanid_Internalname), ".", ","));
                              A79FYActividadPlanCantPartFY = (int)(context.localUtil.CToN( cgiGet( edtFYActividadPlanCantPartFY_Internalname), ".", ","));
                              A80mes1H = (int)(context.localUtil.CToN( cgiGet( edtmes1H_Internalname), ".", ","));
                              n80mes1H = false;
                              A81mes1M = (int)(context.localUtil.CToN( cgiGet( edtmes1M_Internalname), ".", ","));
                              n81mes1M = false;
                              A82mes2H = (int)(context.localUtil.CToN( cgiGet( edtmes2H_Internalname), ".", ","));
                              n82mes2H = false;
                              A83mes2M = (int)(context.localUtil.CToN( cgiGet( edtmes2M_Internalname), ".", ","));
                              n83mes2M = false;
                              A84mes3H = (int)(context.localUtil.CToN( cgiGet( edtmes3H_Internalname), ".", ","));
                              n84mes3H = false;
                              sEvtType = StringUtil.Right( sEvt, 1);
                              if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                              {
                                 sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                                 if ( StringUtil.StrCmp(sEvt, "START") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                    /* Execute user event: Start */
                                    E190S2 ();
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "LOAD") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                    /* Execute user event: Load */
                                    E200S2 ();
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "ENTER") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    if ( ! wbErr )
                                    {
                                       Rfr0gs = false;
                                       /* Set Refresh If Cfyactividadplancantpartid Changed */
                                       if ( ( context.localUtil.CToN( cgiGet( "GXH_vCFYACTIVIDADPLANCANTPARTID"), ".", ",") != Convert.ToDecimal( AV6cFYActividadPlanCantPartid )) )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Cactividadparaplanid Changed */
                                       if ( ( context.localUtil.CToN( cgiGet( "GXH_vCACTIVIDADPARAPLANID"), ".", ",") != Convert.ToDecimal( AV7cActividadParaPlanid )) )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Cfyactividadplancantpartfy Changed */
                                       if ( ( context.localUtil.CToN( cgiGet( "GXH_vCFYACTIVIDADPLANCANTPARTFY"), ".", ",") != Convert.ToDecimal( AV8cFYActividadPlanCantPartFY )) )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Cmes1h Changed */
                                       if ( ( context.localUtil.CToN( cgiGet( "GXH_vCMES1H"), ".", ",") != Convert.ToDecimal( AV9cmes1H )) )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Cmes1m Changed */
                                       if ( ( context.localUtil.CToN( cgiGet( "GXH_vCMES1M"), ".", ",") != Convert.ToDecimal( AV10cmes1M )) )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Cmes2h Changed */
                                       if ( ( context.localUtil.CToN( cgiGet( "GXH_vCMES2H"), ".", ",") != Convert.ToDecimal( AV11cmes2H )) )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Cmes2m Changed */
                                       if ( ( context.localUtil.CToN( cgiGet( "GXH_vCMES2M"), ".", ",") != Convert.ToDecimal( AV12cmes2M )) )
                                       {
                                          Rfr0gs = true;
                                       }
                                       if ( ! Rfr0gs )
                                       {
                                          /* Execute user event: Enter */
                                          E210S2 ();
                                       }
                                       dynload_actions( ) ;
                                    }
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                 }
                              }
                              else
                              {
                              }
                           }
                        }
                     }
                     context.wbHandled = 1;
                  }
               }
            }
         }
      }

      protected void WE0S2( )
      {
         if ( ! GxWebStd.gx_redirect( context) )
         {
            Rfr0gs = true;
            Refresh( ) ;
            if ( ! GxWebStd.gx_redirect( context) )
            {
               if ( nGXWrapped == 1 )
               {
                  RenderHtmlCloseForm( ) ;
               }
            }
         }
      }

      protected void PA0S2( )
      {
         if ( nDonePA == 0 )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.GetCookie( "GX_SESSION_ID"))) )
            {
               gxcookieaux = context.SetCookie( "GX_SESSION_ID", Encrypt64( Crypto.GetEncryptionKey( ), Crypto.GetServerKey( )), "", (DateTime)(DateTime.MinValue), "", 0);
            }
            GXKey = Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            toggleJsOutput = isJsOutputEnabled( );
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
            init_web_controls( ) ;
            if ( toggleJsOutput )
            {
               if ( context.isSpaRequest( ) )
               {
                  enableJsOutput();
               }
            }
            nDonePA = 1;
         }
      }

      protected void dynload_actions( )
      {
         /* End function dynload_actions */
      }

      protected void gxnrGrid1_newrow( )
      {
         GxWebStd.set_html_headers( context, 0, "", "");
         SubsflControlProps_842( ) ;
         while ( nGXsfl_84_idx <= nRC_GXsfl_84 )
         {
            sendrow_842( ) ;
            nGXsfl_84_idx = (short)(((subGrid1_Islastpage==1)&&(nGXsfl_84_idx+1>subGrid1_Recordsperpage( )) ? 1 : nGXsfl_84_idx+1));
            sGXsfl_84_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_84_idx), 4, 0)), 4, "0");
            SubsflControlProps_842( ) ;
         }
         context.GX_webresponse.AddString(context.httpAjaxContext.getJSONContainerResponse( Grid1Container));
         /* End function gxnrGrid1_newrow */
      }

      protected void gxgrGrid1_refresh( int subGrid1_Rows ,
                                        int AV6cFYActividadPlanCantPartid ,
                                        int AV7cActividadParaPlanid ,
                                        int AV8cFYActividadPlanCantPartFY ,
                                        int AV9cmes1H ,
                                        int AV10cmes1M ,
                                        int AV11cmes2H ,
                                        int AV12cmes2M )
      {
         initialize_formulas( ) ;
         GxWebStd.set_html_headers( context, 0, "", "");
         GxWebStd.gx_hidden_field( context, "GRID1_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid1_Rows), 6, 0, ".", "")));
         GRID1_nCurrentRecord = 0;
         RF0S2( ) ;
         /* End function gxgrGrid1_refresh */
      }

      protected void send_integrity_hashes( )
      {
         GxWebStd.gx_hidden_field( context, "gxhash_FYACTIVIDADPLANCANTPARTID", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(A14FYActividadPlanCantPartid), "ZZZZZZZZ9"), context));
         GxWebStd.gx_hidden_field( context, "FYACTIVIDADPLANCANTPARTID", StringUtil.LTrim( StringUtil.NToC( (decimal)(A14FYActividadPlanCantPartid), 9, 0, ".", "")));
      }

      protected void clear_multi_value_controls( )
      {
         if ( context.isAjaxRequest( ) )
         {
            dynload_actions( ) ;
         }
      }

      protected void fix_multi_value_controls( )
      {
      }

      public void Refresh( )
      {
         send_integrity_hashes( ) ;
         RF0S2( ) ;
         if ( isFullAjaxMode( ) )
         {
            send_integrity_footer_hashes( ) ;
         }
      }

      protected void initialize_formulas( )
      {
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      protected void RF0S2( )
      {
         initialize_formulas( ) ;
         clear_multi_value_controls( ) ;
         if ( isAjaxCallMode( ) )
         {
            Grid1Container.ClearRows();
         }
         wbStart = 84;
         nGXsfl_84_idx = 1;
         sGXsfl_84_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_84_idx), 4, 0)), 4, "0");
         SubsflControlProps_842( ) ;
         bGXsfl_84_Refreshing = true;
         Grid1Container.AddObjectProperty("GridName", "Grid1");
         Grid1Container.AddObjectProperty("CmpContext", "");
         Grid1Container.AddObjectProperty("InMasterPage", "false");
         Grid1Container.AddObjectProperty("Class", "PromptGrid");
         Grid1Container.AddObjectProperty("Cellpadding", StringUtil.LTrim( StringUtil.NToC( (decimal)(1), 4, 0, ".", "")));
         Grid1Container.AddObjectProperty("Cellspacing", StringUtil.LTrim( StringUtil.NToC( (decimal)(2), 4, 0, ".", "")));
         Grid1Container.AddObjectProperty("Backcolorstyle", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid1_Backcolorstyle), 1, 0, ".", "")));
         Grid1Container.PageSize = subGrid1_Recordsperpage( );
         gxdyncontrolsrefreshing = true;
         fix_multi_value_controls( ) ;
         gxdyncontrolsrefreshing = false;
         if ( ! context.WillRedirect( ) && ( context.nUserReturn != 1 ) )
         {
            SubsflControlProps_842( ) ;
            GXPagingFrom2 = (int)(((10==0) ? 0 : GRID1_nFirstRecordOnPage));
            GXPagingTo2 = ((10==0) ? 10000 : subGrid1_Recordsperpage( )+1);
            pr_datastore1.dynParam(0, new Object[]{ new Object[]{
                                                 AV7cActividadParaPlanid ,
                                                 AV8cFYActividadPlanCantPartFY ,
                                                 AV9cmes1H ,
                                                 AV10cmes1M ,
                                                 AV11cmes2H ,
                                                 AV12cmes2M ,
                                                 A23ActividadParaPlanid ,
                                                 A79FYActividadPlanCantPartFY ,
                                                 A80mes1H ,
                                                 A81mes1M ,
                                                 A82mes2H ,
                                                 A83mes2M ,
                                                 AV6cFYActividadPlanCantPartid } ,
                                                 new int[]{
                                                 TypeConstants.INT, TypeConstants.INT, TypeConstants.INT, TypeConstants.INT, TypeConstants.INT, TypeConstants.INT, TypeConstants.INT, TypeConstants.INT, TypeConstants.INT, TypeConstants.BOOLEAN,
                                                 TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.INT
                                                 }
            } ) ;
            /* Using cursor H000S2 */
            pr_datastore1.execute(0, new Object[] {AV6cFYActividadPlanCantPartid, AV7cActividadParaPlanid, AV8cFYActividadPlanCantPartFY, AV9cmes1H, AV10cmes1M, AV11cmes2H, AV12cmes2M, GXPagingFrom2, GXPagingTo2, GXPagingTo2});
            nGXsfl_84_idx = 1;
            sGXsfl_84_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_84_idx), 4, 0)), 4, "0");
            SubsflControlProps_842( ) ;
            while ( ( (pr_datastore1.getStatus(0) != 101) ) && ( ( ( 10 == 0 ) || ( GRID1_nCurrentRecord < subGrid1_Recordsperpage( ) ) ) ) )
            {
               A84mes3H = H000S2_A84mes3H[0];
               n84mes3H = H000S2_n84mes3H[0];
               A83mes2M = H000S2_A83mes2M[0];
               n83mes2M = H000S2_n83mes2M[0];
               A82mes2H = H000S2_A82mes2H[0];
               n82mes2H = H000S2_n82mes2H[0];
               A81mes1M = H000S2_A81mes1M[0];
               n81mes1M = H000S2_n81mes1M[0];
               A80mes1H = H000S2_A80mes1H[0];
               n80mes1H = H000S2_n80mes1H[0];
               A79FYActividadPlanCantPartFY = H000S2_A79FYActividadPlanCantPartFY[0];
               A23ActividadParaPlanid = H000S2_A23ActividadParaPlanid[0];
               A14FYActividadPlanCantPartid = H000S2_A14FYActividadPlanCantPartid[0];
               /* Execute user event: Load */
               E200S2 ();
               pr_datastore1.readNext(0);
            }
            GRID1_nEOF = (short)(((pr_datastore1.getStatus(0) == 101) ? 1 : 0));
            GxWebStd.gx_hidden_field( context, "GRID1_nEOF", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID1_nEOF), 1, 0, ".", "")));
            pr_datastore1.close(0);
            wbEnd = 84;
            WB0S0( ) ;
         }
         bGXsfl_84_Refreshing = true;
      }

      protected void send_integrity_lvl_hashes0S2( )
      {
         GxWebStd.gx_hidden_field( context, "gxhash_FYACTIVIDADPLANCANTPARTID"+"_"+sGXsfl_84_idx, GetSecureSignedToken( sGXsfl_84_idx, context.localUtil.Format( (decimal)(A14FYActividadPlanCantPartid), "ZZZZZZZZ9"), context));
      }

      protected int subGrid1_Pagecount( )
      {
         GRID1_nRecordCount = subGrid1_Recordcount( );
         if ( ((int)((GRID1_nRecordCount) % (subGrid1_Recordsperpage( )))) == 0 )
         {
            return (int)(NumberUtil.Int( (long)(GRID1_nRecordCount/ (decimal)(subGrid1_Recordsperpage( ))))) ;
         }
         return (int)(NumberUtil.Int( (long)(GRID1_nRecordCount/ (decimal)(subGrid1_Recordsperpage( ))))+1) ;
      }

      protected int subGrid1_Recordcount( )
      {
         pr_datastore1.dynParam(1, new Object[]{ new Object[]{
                                              AV7cActividadParaPlanid ,
                                              AV8cFYActividadPlanCantPartFY ,
                                              AV9cmes1H ,
                                              AV10cmes1M ,
                                              AV11cmes2H ,
                                              AV12cmes2M ,
                                              A23ActividadParaPlanid ,
                                              A79FYActividadPlanCantPartFY ,
                                              A80mes1H ,
                                              A81mes1M ,
                                              A82mes2H ,
                                              A83mes2M ,
                                              AV6cFYActividadPlanCantPartid } ,
                                              new int[]{
                                              TypeConstants.INT, TypeConstants.INT, TypeConstants.INT, TypeConstants.INT, TypeConstants.INT, TypeConstants.INT, TypeConstants.INT, TypeConstants.INT, TypeConstants.INT, TypeConstants.BOOLEAN,
                                              TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.INT
                                              }
         } ) ;
         /* Using cursor H000S3 */
         pr_datastore1.execute(1, new Object[] {AV6cFYActividadPlanCantPartid, AV7cActividadParaPlanid, AV8cFYActividadPlanCantPartFY, AV9cmes1H, AV10cmes1M, AV11cmes2H, AV12cmes2M});
         GRID1_nRecordCount = H000S3_AGRID1_nRecordCount[0];
         pr_datastore1.close(1);
         return (int)(GRID1_nRecordCount) ;
      }

      protected int subGrid1_Recordsperpage( )
      {
         return (int)(10*1) ;
      }

      protected int subGrid1_Currentpage( )
      {
         return (int)(NumberUtil.Int( (long)(GRID1_nFirstRecordOnPage/ (decimal)(subGrid1_Recordsperpage( ))))+1) ;
      }

      protected short subgrid1_firstpage( )
      {
         GRID1_nFirstRecordOnPage = 0;
         GxWebStd.gx_hidden_field( context, "GRID1_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID1_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid1_refresh( subGrid1_Rows, AV6cFYActividadPlanCantPartid, AV7cActividadParaPlanid, AV8cFYActividadPlanCantPartFY, AV9cmes1H, AV10cmes1M, AV11cmes2H, AV12cmes2M) ;
         }
         send_integrity_footer_hashes( ) ;
         return 0 ;
      }

      protected short subgrid1_nextpage( )
      {
         GRID1_nRecordCount = subGrid1_Recordcount( );
         if ( ( GRID1_nRecordCount >= subGrid1_Recordsperpage( ) ) && ( GRID1_nEOF == 0 ) )
         {
            GRID1_nFirstRecordOnPage = (long)(GRID1_nFirstRecordOnPage+subGrid1_Recordsperpage( ));
         }
         else
         {
            return 2 ;
         }
         GxWebStd.gx_hidden_field( context, "GRID1_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID1_nFirstRecordOnPage), 15, 0, ".", "")));
         Grid1Container.AddObjectProperty("GRID1_nFirstRecordOnPage", GRID1_nFirstRecordOnPage);
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid1_refresh( subGrid1_Rows, AV6cFYActividadPlanCantPartid, AV7cActividadParaPlanid, AV8cFYActividadPlanCantPartFY, AV9cmes1H, AV10cmes1M, AV11cmes2H, AV12cmes2M) ;
         }
         send_integrity_footer_hashes( ) ;
         return (short)(((GRID1_nEOF==0) ? 0 : 2)) ;
      }

      protected short subgrid1_previouspage( )
      {
         if ( GRID1_nFirstRecordOnPage >= subGrid1_Recordsperpage( ) )
         {
            GRID1_nFirstRecordOnPage = (long)(GRID1_nFirstRecordOnPage-subGrid1_Recordsperpage( ));
         }
         else
         {
            return 2 ;
         }
         GxWebStd.gx_hidden_field( context, "GRID1_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID1_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid1_refresh( subGrid1_Rows, AV6cFYActividadPlanCantPartid, AV7cActividadParaPlanid, AV8cFYActividadPlanCantPartFY, AV9cmes1H, AV10cmes1M, AV11cmes2H, AV12cmes2M) ;
         }
         send_integrity_footer_hashes( ) ;
         return 0 ;
      }

      protected short subgrid1_lastpage( )
      {
         GRID1_nRecordCount = subGrid1_Recordcount( );
         if ( GRID1_nRecordCount > subGrid1_Recordsperpage( ) )
         {
            if ( ((int)((GRID1_nRecordCount) % (subGrid1_Recordsperpage( )))) == 0 )
            {
               GRID1_nFirstRecordOnPage = (long)(GRID1_nRecordCount-subGrid1_Recordsperpage( ));
            }
            else
            {
               GRID1_nFirstRecordOnPage = (long)(GRID1_nRecordCount-((int)((GRID1_nRecordCount) % (subGrid1_Recordsperpage( )))));
            }
         }
         else
         {
            GRID1_nFirstRecordOnPage = 0;
         }
         GxWebStd.gx_hidden_field( context, "GRID1_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID1_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid1_refresh( subGrid1_Rows, AV6cFYActividadPlanCantPartid, AV7cActividadParaPlanid, AV8cFYActividadPlanCantPartFY, AV9cmes1H, AV10cmes1M, AV11cmes2H, AV12cmes2M) ;
         }
         send_integrity_footer_hashes( ) ;
         return 0 ;
      }

      protected int subgrid1_gotopage( int nPageNo )
      {
         if ( nPageNo > 0 )
         {
            GRID1_nFirstRecordOnPage = (long)(subGrid1_Recordsperpage( )*(nPageNo-1));
         }
         else
         {
            GRID1_nFirstRecordOnPage = 0;
         }
         GxWebStd.gx_hidden_field( context, "GRID1_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID1_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid1_refresh( subGrid1_Rows, AV6cFYActividadPlanCantPartid, AV7cActividadParaPlanid, AV8cFYActividadPlanCantPartFY, AV9cmes1H, AV10cmes1M, AV11cmes2H, AV12cmes2M) ;
         }
         send_integrity_footer_hashes( ) ;
         return (int)(0) ;
      }

      protected void STRUP0S0( )
      {
         /* Before Start, stand alone formulas. */
         context.Gx_err = 0;
         /* Execute Start event if defined. */
         context.wbGlbDoneStart = 0;
         /* Execute user event: Start */
         E190S2 ();
         context.wbGlbDoneStart = 1;
         /* After Start, stand alone formulas. */
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            /* Read saved SDTs. */
            /* Read variables values. */
            if ( ( ( context.localUtil.CToN( cgiGet( edtavCfyactividadplancantpartid_Internalname), ".", ",") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavCfyactividadplancantpartid_Internalname), ".", ",") > Convert.ToDecimal( 999999999 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vCFYACTIVIDADPLANCANTPARTID");
               GX_FocusControl = edtavCfyactividadplancantpartid_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV6cFYActividadPlanCantPartid = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV6cFYActividadPlanCantPartid", StringUtil.LTrim( StringUtil.Str( (decimal)(AV6cFYActividadPlanCantPartid), 9, 0)));
            }
            else
            {
               AV6cFYActividadPlanCantPartid = (int)(context.localUtil.CToN( cgiGet( edtavCfyactividadplancantpartid_Internalname), ".", ","));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV6cFYActividadPlanCantPartid", StringUtil.LTrim( StringUtil.Str( (decimal)(AV6cFYActividadPlanCantPartid), 9, 0)));
            }
            if ( ( ( context.localUtil.CToN( cgiGet( edtavCactividadparaplanid_Internalname), ".", ",") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavCactividadparaplanid_Internalname), ".", ",") > Convert.ToDecimal( 999999999 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vCACTIVIDADPARAPLANID");
               GX_FocusControl = edtavCactividadparaplanid_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV7cActividadParaPlanid = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV7cActividadParaPlanid", StringUtil.LTrim( StringUtil.Str( (decimal)(AV7cActividadParaPlanid), 9, 0)));
            }
            else
            {
               AV7cActividadParaPlanid = (int)(context.localUtil.CToN( cgiGet( edtavCactividadparaplanid_Internalname), ".", ","));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV7cActividadParaPlanid", StringUtil.LTrim( StringUtil.Str( (decimal)(AV7cActividadParaPlanid), 9, 0)));
            }
            if ( ( ( context.localUtil.CToN( cgiGet( edtavCfyactividadplancantpartfy_Internalname), ".", ",") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavCfyactividadplancantpartfy_Internalname), ".", ",") > Convert.ToDecimal( 999999999 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vCFYACTIVIDADPLANCANTPARTFY");
               GX_FocusControl = edtavCfyactividadplancantpartfy_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV8cFYActividadPlanCantPartFY = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV8cFYActividadPlanCantPartFY", StringUtil.LTrim( StringUtil.Str( (decimal)(AV8cFYActividadPlanCantPartFY), 9, 0)));
            }
            else
            {
               AV8cFYActividadPlanCantPartFY = (int)(context.localUtil.CToN( cgiGet( edtavCfyactividadplancantpartfy_Internalname), ".", ","));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV8cFYActividadPlanCantPartFY", StringUtil.LTrim( StringUtil.Str( (decimal)(AV8cFYActividadPlanCantPartFY), 9, 0)));
            }
            if ( ( ( context.localUtil.CToN( cgiGet( edtavCmes1h_Internalname), ".", ",") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavCmes1h_Internalname), ".", ",") > Convert.ToDecimal( 999999999 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vCMES1H");
               GX_FocusControl = edtavCmes1h_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV9cmes1H = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV9cmes1H", StringUtil.LTrim( StringUtil.Str( (decimal)(AV9cmes1H), 9, 0)));
            }
            else
            {
               AV9cmes1H = (int)(context.localUtil.CToN( cgiGet( edtavCmes1h_Internalname), ".", ","));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV9cmes1H", StringUtil.LTrim( StringUtil.Str( (decimal)(AV9cmes1H), 9, 0)));
            }
            if ( ( ( context.localUtil.CToN( cgiGet( edtavCmes1m_Internalname), ".", ",") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavCmes1m_Internalname), ".", ",") > Convert.ToDecimal( 999999999 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vCMES1M");
               GX_FocusControl = edtavCmes1m_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV10cmes1M = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV10cmes1M", StringUtil.LTrim( StringUtil.Str( (decimal)(AV10cmes1M), 9, 0)));
            }
            else
            {
               AV10cmes1M = (int)(context.localUtil.CToN( cgiGet( edtavCmes1m_Internalname), ".", ","));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV10cmes1M", StringUtil.LTrim( StringUtil.Str( (decimal)(AV10cmes1M), 9, 0)));
            }
            if ( ( ( context.localUtil.CToN( cgiGet( edtavCmes2h_Internalname), ".", ",") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavCmes2h_Internalname), ".", ",") > Convert.ToDecimal( 999999999 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vCMES2H");
               GX_FocusControl = edtavCmes2h_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV11cmes2H = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV11cmes2H", StringUtil.LTrim( StringUtil.Str( (decimal)(AV11cmes2H), 9, 0)));
            }
            else
            {
               AV11cmes2H = (int)(context.localUtil.CToN( cgiGet( edtavCmes2h_Internalname), ".", ","));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV11cmes2H", StringUtil.LTrim( StringUtil.Str( (decimal)(AV11cmes2H), 9, 0)));
            }
            if ( ( ( context.localUtil.CToN( cgiGet( edtavCmes2m_Internalname), ".", ",") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavCmes2m_Internalname), ".", ",") > Convert.ToDecimal( 999999999 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vCMES2M");
               GX_FocusControl = edtavCmes2m_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV12cmes2M = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV12cmes2M", StringUtil.LTrim( StringUtil.Str( (decimal)(AV12cmes2M), 9, 0)));
            }
            else
            {
               AV12cmes2M = (int)(context.localUtil.CToN( cgiGet( edtavCmes2m_Internalname), ".", ","));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV12cmes2M", StringUtil.LTrim( StringUtil.Str( (decimal)(AV12cmes2M), 9, 0)));
            }
            /* Read saved values. */
            nRC_GXsfl_84 = (short)(context.localUtil.CToN( cgiGet( "nRC_GXsfl_84"), ".", ","));
            GRID1_nFirstRecordOnPage = (long)(context.localUtil.CToN( cgiGet( "GRID1_nFirstRecordOnPage"), ".", ","));
            GRID1_nEOF = (short)(context.localUtil.CToN( cgiGet( "GRID1_nEOF"), ".", ","));
            /* Read subfile selected row values. */
            /* Read hidden variables. */
            GXKey = Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            /* Check if conditions changed and reset current page numbers */
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vCFYACTIVIDADPLANCANTPARTID"), ".", ",") != Convert.ToDecimal( AV6cFYActividadPlanCantPartid )) )
            {
               GRID1_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vCACTIVIDADPARAPLANID"), ".", ",") != Convert.ToDecimal( AV7cActividadParaPlanid )) )
            {
               GRID1_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vCFYACTIVIDADPLANCANTPARTFY"), ".", ",") != Convert.ToDecimal( AV8cFYActividadPlanCantPartFY )) )
            {
               GRID1_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vCMES1H"), ".", ",") != Convert.ToDecimal( AV9cmes1H )) )
            {
               GRID1_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vCMES1M"), ".", ",") != Convert.ToDecimal( AV10cmes1M )) )
            {
               GRID1_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vCMES2H"), ".", ",") != Convert.ToDecimal( AV11cmes2H )) )
            {
               GRID1_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vCMES2M"), ".", ",") != Convert.ToDecimal( AV12cmes2M )) )
            {
               GRID1_nFirstRecordOnPage = 0;
            }
         }
         else
         {
            dynload_actions( ) ;
         }
      }

      protected void GXStart( )
      {
         /* Execute user event: Start */
         E190S2 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E190S2( )
      {
         /* Start Routine */
         Form.Caption = StringUtil.Format( "Selection List %1", "FYActividad Plan Cant Part", "", "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, "FORM", "Caption", Form.Caption, true);
         AV14ADVANCED_LABEL_TEMPLATE = "%1 <strong>%2</strong>";
      }

      private void E200S2( )
      {
         /* Load Routine */
         AV5LinkSelection = context.GetImagePath( "3914535b-0c03-44c5-9538-906a99cdd2bc", "", context.GetTheme( ));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavLinkselection_Internalname, AV5LinkSelection);
         AV17Linkselection_GXI = GXDbFile.PathToUrl( context.GetImagePath( "3914535b-0c03-44c5-9538-906a99cdd2bc", "", context.GetTheme( )));
         sendrow_842( ) ;
         GRID1_nCurrentRecord = (long)(GRID1_nCurrentRecord+1);
         if ( isFullAjaxMode( ) && ! bGXsfl_84_Refreshing )
         {
            context.DoAjaxLoad(84, Grid1Row);
         }
      }

      public void GXEnter( )
      {
         /* Execute user event: Enter */
         E210S2 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E210S2( )
      {
         /* Enter Routine */
         AV13pFYActividadPlanCantPartid = A14FYActividadPlanCantPartid;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13pFYActividadPlanCantPartid", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13pFYActividadPlanCantPartid), 9, 0)));
         context.setWebReturnParms(new Object[] {(int)AV13pFYActividadPlanCantPartid});
         context.setWebReturnParmsMetadata(new Object[] {"AV13pFYActividadPlanCantPartid"});
         context.wjLocDisableFrm = 1;
         context.nUserReturn = 1;
         returnInSub = true;
         if (true) return;
         /*  Sending Event outputs  */
      }

      public override void setparameters( Object[] obj )
      {
         createObjects();
         initialize();
         AV13pFYActividadPlanCantPartid = Convert.ToInt32(getParm(obj,0));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13pFYActividadPlanCantPartid", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13pFYActividadPlanCantPartid), 9, 0)));
      }

      public override String getresponse( String sGXDynURL )
      {
         initialize_properties( ) ;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         sDynURL = sGXDynURL;
         nGotPars = (short)(1);
         nGXWrapped = (short)(1);
         context.SetWrapped(true);
         PA0S2( ) ;
         WS0S2( ) ;
         WE0S2( ) ;
         this.cleanup();
         context.SetWrapped(false);
         context.GX_msglist = BackMsgLst;
         return "";
      }

      public void responsestatic( String sGXDynURL )
      {
      }

      protected void define_styles( )
      {
         AddThemeStyleSheetFile("", context.GetTheme( )+".css", "?"+GetCacheInvalidationToken( ));
         bool outputEnabled = isOutputEnabled( ) ;
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         idxLst = 1;
         while ( idxLst <= Form.Jscriptsrc.Count )
         {
            context.AddJavascriptSource(StringUtil.RTrim( ((String)Form.Jscriptsrc.Item(idxLst))), "?201913181534", true);
            idxLst = (int)(idxLst+1);
         }
         if ( ! outputEnabled )
         {
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
         }
         /* End function define_styles */
      }

      protected void include_jscripts( )
      {
         context.AddJavascriptSource("messages.eng.js", "?"+GetCacheInvalidationToken( ), false);
         context.AddJavascriptSource("gx00i0.js", "?201913181534", false);
         /* End function include_jscripts */
      }

      protected void SubsflControlProps_842( )
      {
         edtavLinkselection_Internalname = "vLINKSELECTION_"+sGXsfl_84_idx;
         edtFYActividadPlanCantPartid_Internalname = "FYACTIVIDADPLANCANTPARTID_"+sGXsfl_84_idx;
         edtActividadParaPlanid_Internalname = "ACTIVIDADPARAPLANID_"+sGXsfl_84_idx;
         edtFYActividadPlanCantPartFY_Internalname = "FYACTIVIDADPLANCANTPARTFY_"+sGXsfl_84_idx;
         edtmes1H_Internalname = "MES1H_"+sGXsfl_84_idx;
         edtmes1M_Internalname = "MES1M_"+sGXsfl_84_idx;
         edtmes2H_Internalname = "MES2H_"+sGXsfl_84_idx;
         edtmes2M_Internalname = "MES2M_"+sGXsfl_84_idx;
         edtmes3H_Internalname = "MES3H_"+sGXsfl_84_idx;
      }

      protected void SubsflControlProps_fel_842( )
      {
         edtavLinkselection_Internalname = "vLINKSELECTION_"+sGXsfl_84_fel_idx;
         edtFYActividadPlanCantPartid_Internalname = "FYACTIVIDADPLANCANTPARTID_"+sGXsfl_84_fel_idx;
         edtActividadParaPlanid_Internalname = "ACTIVIDADPARAPLANID_"+sGXsfl_84_fel_idx;
         edtFYActividadPlanCantPartFY_Internalname = "FYACTIVIDADPLANCANTPARTFY_"+sGXsfl_84_fel_idx;
         edtmes1H_Internalname = "MES1H_"+sGXsfl_84_fel_idx;
         edtmes1M_Internalname = "MES1M_"+sGXsfl_84_fel_idx;
         edtmes2H_Internalname = "MES2H_"+sGXsfl_84_fel_idx;
         edtmes2M_Internalname = "MES2M_"+sGXsfl_84_fel_idx;
         edtmes3H_Internalname = "MES3H_"+sGXsfl_84_fel_idx;
      }

      protected void sendrow_842( )
      {
         SubsflControlProps_842( ) ;
         WB0S0( ) ;
         if ( ( 10 * 1 == 0 ) || ( nGXsfl_84_idx <= subGrid1_Recordsperpage( ) * 1 ) )
         {
            Grid1Row = GXWebRow.GetNew(context,Grid1Container);
            if ( subGrid1_Backcolorstyle == 0 )
            {
               /* None style subfile background logic. */
               subGrid1_Backstyle = 0;
               if ( StringUtil.StrCmp(subGrid1_Class, "") != 0 )
               {
                  subGrid1_Linesclass = subGrid1_Class+"Odd";
               }
            }
            else if ( subGrid1_Backcolorstyle == 1 )
            {
               /* Uniform style subfile background logic. */
               subGrid1_Backstyle = 0;
               subGrid1_Backcolor = subGrid1_Allbackcolor;
               if ( StringUtil.StrCmp(subGrid1_Class, "") != 0 )
               {
                  subGrid1_Linesclass = subGrid1_Class+"Uniform";
               }
            }
            else if ( subGrid1_Backcolorstyle == 2 )
            {
               /* Header style subfile background logic. */
               subGrid1_Backstyle = 1;
               if ( StringUtil.StrCmp(subGrid1_Class, "") != 0 )
               {
                  subGrid1_Linesclass = subGrid1_Class+"Odd";
               }
               subGrid1_Backcolor = (int)(0x0);
            }
            else if ( subGrid1_Backcolorstyle == 3 )
            {
               /* Report style subfile background logic. */
               subGrid1_Backstyle = 1;
               if ( ((int)(((nGXsfl_84_idx-1)/ (decimal)(1)) % (2))) == 0 )
               {
                  subGrid1_Backcolor = (int)(0x0);
                  if ( StringUtil.StrCmp(subGrid1_Class, "") != 0 )
                  {
                     subGrid1_Linesclass = subGrid1_Class+"Odd";
                  }
               }
               else
               {
                  subGrid1_Backcolor = (int)(0x0);
                  if ( StringUtil.StrCmp(subGrid1_Class, "") != 0 )
                  {
                     subGrid1_Linesclass = subGrid1_Class+"Even";
                  }
               }
            }
            if ( Grid1Container.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<tr ") ;
               context.WriteHtmlText( " class=\""+"PromptGrid"+"\" style=\""+""+"\"") ;
               context.WriteHtmlText( " gxrow=\""+sGXsfl_84_idx+"\">") ;
            }
            /* Subfile cell */
            if ( Grid1Container.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+""+"\""+" style=\""+""+"\">") ;
            }
            /* Static Bitmap Variable */
            edtavLinkselection_Link = "javascript:gx.popup.gxReturn(["+"'"+GXUtil.EncodeJSConstant( StringUtil.LTrim( StringUtil.NToC( (decimal)(A14FYActividadPlanCantPartid), 9, 0, ".", "")))+"'"+"]);";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavLinkselection_Internalname, "Link", edtavLinkselection_Link, !bGXsfl_84_Refreshing);
            ClassString = "SelectionAttribute";
            StyleString = "";
            AV5LinkSelection_IsBlob = (bool)((String.IsNullOrEmpty(StringUtil.RTrim( AV5LinkSelection))&&String.IsNullOrEmpty(StringUtil.RTrim( AV17Linkselection_GXI)))||!String.IsNullOrEmpty(StringUtil.RTrim( AV5LinkSelection)));
            sImgUrl = (String.IsNullOrEmpty(StringUtil.RTrim( AV5LinkSelection)) ? AV17Linkselection_GXI : context.PathToRelativeUrl( AV5LinkSelection));
            Grid1Row.AddColumnProperties("bitmap", 1, isAjaxCallMode( ), new Object[] {(String)edtavLinkselection_Internalname,(String)sImgUrl,(String)edtavLinkselection_Link,(String)"",(String)"",context.GetTheme( ),(short)-1,(short)1,(String)"",(String)"",(short)1,(short)-1,(short)0,(String)"px",(short)0,(String)"px",(short)0,(short)0,(short)0,(String)"",(String)"",(String)StyleString,(String)ClassString,(String)"WWActionColumn",(String)"",(String)"",(String)"",(String)"",(String)"",(String)"",(short)1,(bool)AV5LinkSelection_IsBlob,(bool)false,context.GetImageSrcSet( sImgUrl)});
            /* Subfile cell */
            if ( Grid1Container.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "Attribute";
            Grid1Row.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtFYActividadPlanCantPartid_Internalname,StringUtil.LTrim( StringUtil.NToC( (decimal)(A14FYActividadPlanCantPartid), 9, 0, ".", "")),context.localUtil.Format( (decimal)(A14FYActividadPlanCantPartid), "ZZZZZZZZ9"),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtFYActividadPlanCantPartid_Jsonclick,(short)0,(String)"Attribute",(String)"",(String)ROClassString,(String)"WWColumn",(String)"",(short)-1,(short)0,(short)0,(String)"number",(String)"1",(short)0,(String)"px",(short)17,(String)"px",(short)9,(short)0,(short)0,(short)84,(short)1,(short)-1,(short)0,(bool)true,(String)"",(String)"right",(bool)false});
            /* Subfile cell */
            if ( Grid1Container.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "Attribute";
            Grid1Row.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtActividadParaPlanid_Internalname,StringUtil.LTrim( StringUtil.NToC( (decimal)(A23ActividadParaPlanid), 9, 0, ".", "")),context.localUtil.Format( (decimal)(A23ActividadParaPlanid), "ZZZZZZZZ9"),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtActividadParaPlanid_Jsonclick,(short)0,(String)"Attribute",(String)"",(String)ROClassString,(String)"WWColumn OptionalColumn",(String)"",(short)-1,(short)0,(short)0,(String)"number",(String)"1",(short)0,(String)"px",(short)17,(String)"px",(short)9,(short)0,(short)0,(short)84,(short)1,(short)-1,(short)0,(bool)true,(String)"",(String)"right",(bool)false});
            /* Subfile cell */
            if ( Grid1Container.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "DescriptionAttribute";
            edtFYActividadPlanCantPartFY_Link = "javascript:gx.popup.gxReturn(["+"'"+GXUtil.EncodeJSConstant( StringUtil.LTrim( StringUtil.NToC( (decimal)(A14FYActividadPlanCantPartid), 9, 0, ".", "")))+"'"+"]);";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtFYActividadPlanCantPartFY_Internalname, "Link", edtFYActividadPlanCantPartFY_Link, !bGXsfl_84_Refreshing);
            Grid1Row.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtFYActividadPlanCantPartFY_Internalname,StringUtil.LTrim( StringUtil.NToC( (decimal)(A79FYActividadPlanCantPartFY), 9, 0, ".", "")),context.localUtil.Format( (decimal)(A79FYActividadPlanCantPartFY), "ZZZZZZZZ9"),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)edtFYActividadPlanCantPartFY_Link,(String)"",(String)"",(String)"",(String)edtFYActividadPlanCantPartFY_Jsonclick,(short)0,(String)"DescriptionAttribute",(String)"",(String)ROClassString,(String)"WWColumn",(String)"",(short)-1,(short)0,(short)0,(String)"number",(String)"1",(short)0,(String)"px",(short)17,(String)"px",(short)9,(short)0,(short)0,(short)84,(short)1,(short)-1,(short)0,(bool)true,(String)"",(String)"right",(bool)false});
            /* Subfile cell */
            if ( Grid1Container.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "Attribute";
            Grid1Row.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtmes1H_Internalname,StringUtil.LTrim( StringUtil.NToC( (decimal)(A80mes1H), 9, 0, ".", "")),context.localUtil.Format( (decimal)(A80mes1H), "ZZZZZZZZ9"),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtmes1H_Jsonclick,(short)0,(String)"Attribute",(String)"",(String)ROClassString,(String)"WWColumn OptionalColumn",(String)"",(short)-1,(short)0,(short)0,(String)"number",(String)"1",(short)0,(String)"px",(short)17,(String)"px",(short)9,(short)0,(short)0,(short)84,(short)1,(short)-1,(short)0,(bool)true,(String)"",(String)"right",(bool)false});
            /* Subfile cell */
            if ( Grid1Container.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "Attribute";
            Grid1Row.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtmes1M_Internalname,StringUtil.LTrim( StringUtil.NToC( (decimal)(A81mes1M), 9, 0, ".", "")),context.localUtil.Format( (decimal)(A81mes1M), "ZZZZZZZZ9"),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtmes1M_Jsonclick,(short)0,(String)"Attribute",(String)"",(String)ROClassString,(String)"WWColumn OptionalColumn",(String)"",(short)-1,(short)0,(short)0,(String)"number",(String)"1",(short)0,(String)"px",(short)17,(String)"px",(short)9,(short)0,(short)0,(short)84,(short)1,(short)-1,(short)0,(bool)true,(String)"",(String)"right",(bool)false});
            /* Subfile cell */
            if ( Grid1Container.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "Attribute";
            Grid1Row.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtmes2H_Internalname,StringUtil.LTrim( StringUtil.NToC( (decimal)(A82mes2H), 9, 0, ".", "")),context.localUtil.Format( (decimal)(A82mes2H), "ZZZZZZZZ9"),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtmes2H_Jsonclick,(short)0,(String)"Attribute",(String)"",(String)ROClassString,(String)"WWColumn OptionalColumn",(String)"",(short)-1,(short)0,(short)0,(String)"number",(String)"1",(short)0,(String)"px",(short)17,(String)"px",(short)9,(short)0,(short)0,(short)84,(short)1,(short)-1,(short)0,(bool)true,(String)"",(String)"right",(bool)false});
            /* Subfile cell */
            if ( Grid1Container.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "Attribute";
            Grid1Row.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtmes2M_Internalname,StringUtil.LTrim( StringUtil.NToC( (decimal)(A83mes2M), 9, 0, ".", "")),context.localUtil.Format( (decimal)(A83mes2M), "ZZZZZZZZ9"),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtmes2M_Jsonclick,(short)0,(String)"Attribute",(String)"",(String)ROClassString,(String)"WWColumn OptionalColumn",(String)"",(short)-1,(short)0,(short)0,(String)"number",(String)"1",(short)0,(String)"px",(short)17,(String)"px",(short)9,(short)0,(short)0,(short)84,(short)1,(short)-1,(short)0,(bool)true,(String)"",(String)"right",(bool)false});
            /* Subfile cell */
            if ( Grid1Container.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "Attribute";
            Grid1Row.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtmes3H_Internalname,StringUtil.LTrim( StringUtil.NToC( (decimal)(A84mes3H), 9, 0, ".", "")),context.localUtil.Format( (decimal)(A84mes3H), "ZZZZZZZZ9"),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtmes3H_Jsonclick,(short)0,(String)"Attribute",(String)"",(String)ROClassString,(String)"WWColumn OptionalColumn",(String)"",(short)-1,(short)0,(short)0,(String)"number",(String)"1",(short)0,(String)"px",(short)17,(String)"px",(short)9,(short)0,(short)0,(short)84,(short)1,(short)-1,(short)0,(bool)true,(String)"",(String)"right",(bool)false});
            send_integrity_lvl_hashes0S2( ) ;
            Grid1Container.AddRow(Grid1Row);
            nGXsfl_84_idx = (short)(((subGrid1_Islastpage==1)&&(nGXsfl_84_idx+1>subGrid1_Recordsperpage( )) ? 1 : nGXsfl_84_idx+1));
            sGXsfl_84_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_84_idx), 4, 0)), 4, "0");
            SubsflControlProps_842( ) ;
         }
         /* End function sendrow_842 */
      }

      protected void init_web_controls( )
      {
         /* End function init_web_controls */
      }

      protected void init_default_properties( )
      {
         lblLblfyactividadplancantpartidfilter_Internalname = "LBLFYACTIVIDADPLANCANTPARTIDFILTER";
         edtavCfyactividadplancantpartid_Internalname = "vCFYACTIVIDADPLANCANTPARTID";
         divFyactividadplancantpartidfiltercontainer_Internalname = "FYACTIVIDADPLANCANTPARTIDFILTERCONTAINER";
         lblLblactividadparaplanidfilter_Internalname = "LBLACTIVIDADPARAPLANIDFILTER";
         edtavCactividadparaplanid_Internalname = "vCACTIVIDADPARAPLANID";
         divActividadparaplanidfiltercontainer_Internalname = "ACTIVIDADPARAPLANIDFILTERCONTAINER";
         lblLblfyactividadplancantpartfyfilter_Internalname = "LBLFYACTIVIDADPLANCANTPARTFYFILTER";
         edtavCfyactividadplancantpartfy_Internalname = "vCFYACTIVIDADPLANCANTPARTFY";
         divFyactividadplancantpartfyfiltercontainer_Internalname = "FYACTIVIDADPLANCANTPARTFYFILTERCONTAINER";
         lblLblmes1hfilter_Internalname = "LBLMES1HFILTER";
         edtavCmes1h_Internalname = "vCMES1H";
         divMes1hfiltercontainer_Internalname = "MES1HFILTERCONTAINER";
         lblLblmes1mfilter_Internalname = "LBLMES1MFILTER";
         edtavCmes1m_Internalname = "vCMES1M";
         divMes1mfiltercontainer_Internalname = "MES1MFILTERCONTAINER";
         lblLblmes2hfilter_Internalname = "LBLMES2HFILTER";
         edtavCmes2h_Internalname = "vCMES2H";
         divMes2hfiltercontainer_Internalname = "MES2HFILTERCONTAINER";
         lblLblmes2mfilter_Internalname = "LBLMES2MFILTER";
         edtavCmes2m_Internalname = "vCMES2M";
         divMes2mfiltercontainer_Internalname = "MES2MFILTERCONTAINER";
         divAdvancedcontainer_Internalname = "ADVANCEDCONTAINER";
         bttBtntoggle_Internalname = "BTNTOGGLE";
         edtavLinkselection_Internalname = "vLINKSELECTION";
         edtFYActividadPlanCantPartid_Internalname = "FYACTIVIDADPLANCANTPARTID";
         edtActividadParaPlanid_Internalname = "ACTIVIDADPARAPLANID";
         edtFYActividadPlanCantPartFY_Internalname = "FYACTIVIDADPLANCANTPARTFY";
         edtmes1H_Internalname = "MES1H";
         edtmes1M_Internalname = "MES1M";
         edtmes2H_Internalname = "MES2H";
         edtmes2M_Internalname = "MES2M";
         edtmes3H_Internalname = "MES3H";
         bttBtn_cancel_Internalname = "BTN_CANCEL";
         divGridtable_Internalname = "GRIDTABLE";
         divMain_Internalname = "MAIN";
         Form.Internalname = "FORM";
         subGrid1_Internalname = "GRID1";
      }

      public override void initialize_properties( )
      {
         context.SetDefaultTheme("Carmine");
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         init_default_properties( ) ;
         edtmes3H_Jsonclick = "";
         edtmes2M_Jsonclick = "";
         edtmes2H_Jsonclick = "";
         edtmes1M_Jsonclick = "";
         edtmes1H_Jsonclick = "";
         edtFYActividadPlanCantPartFY_Jsonclick = "";
         edtActividadParaPlanid_Jsonclick = "";
         edtFYActividadPlanCantPartid_Jsonclick = "";
         subGrid1_Allowcollapsing = 0;
         subGrid1_Allowselection = 0;
         edtFYActividadPlanCantPartFY_Link = "";
         edtavLinkselection_Link = "";
         subGrid1_Header = "";
         subGrid1_Class = "PromptGrid";
         subGrid1_Backcolorstyle = 0;
         edtavCmes2m_Jsonclick = "";
         edtavCmes2m_Enabled = 1;
         edtavCmes2m_Visible = 1;
         edtavCmes2h_Jsonclick = "";
         edtavCmes2h_Enabled = 1;
         edtavCmes2h_Visible = 1;
         edtavCmes1m_Jsonclick = "";
         edtavCmes1m_Enabled = 1;
         edtavCmes1m_Visible = 1;
         edtavCmes1h_Jsonclick = "";
         edtavCmes1h_Enabled = 1;
         edtavCmes1h_Visible = 1;
         edtavCfyactividadplancantpartfy_Jsonclick = "";
         edtavCfyactividadplancantpartfy_Enabled = 1;
         edtavCfyactividadplancantpartfy_Visible = 1;
         edtavCactividadparaplanid_Jsonclick = "";
         edtavCactividadparaplanid_Enabled = 1;
         edtavCactividadparaplanid_Visible = 1;
         edtavCfyactividadplancantpartid_Jsonclick = "";
         edtavCfyactividadplancantpartid_Enabled = 1;
         edtavCfyactividadplancantpartid_Visible = 1;
         divMes2mfiltercontainer_Class = "AdvancedContainerItem";
         divMes2hfiltercontainer_Class = "AdvancedContainerItem";
         divMes1mfiltercontainer_Class = "AdvancedContainerItem";
         divMes1hfiltercontainer_Class = "AdvancedContainerItem";
         divFyactividadplancantpartfyfiltercontainer_Class = "AdvancedContainerItem";
         divActividadparaplanidfiltercontainer_Class = "AdvancedContainerItem";
         divFyactividadplancantpartidfiltercontainer_Class = "AdvancedContainerItem";
         bttBtntoggle_Class = "BtnToggle";
         divAdvancedcontainer_Class = "AdvancedContainerVisible";
         Form.Headerrawhtml = "";
         Form.Background = "";
         Form.Textcolor = 0;
         Form.Backcolor = (int)(0xFFFFFF);
         Form.Caption = "Selection List FYActividad Plan Cant Part";
         subGrid1_Rows = 10;
         if ( context.isSpaRequest( ) )
         {
            enableJsOutput();
         }
      }

      public override bool SupportAjaxEvent( )
      {
         return true ;
      }

      public override void InitializeDynEvents( )
      {
         setEventMetadata("REFRESH","{handler:'Refresh',iparms:[{av:'GRID1_nFirstRecordOnPage'},{av:'GRID1_nEOF'},{av:'subGrid1_Rows',ctrl:'GRID1',prop:'Rows'},{av:'AV6cFYActividadPlanCantPartid',fld:'vCFYACTIVIDADPLANCANTPARTID',pic:'ZZZZZZZZ9'},{av:'AV7cActividadParaPlanid',fld:'vCACTIVIDADPARAPLANID',pic:'ZZZZZZZZ9'},{av:'AV8cFYActividadPlanCantPartFY',fld:'vCFYACTIVIDADPLANCANTPARTFY',pic:'ZZZZZZZZ9'},{av:'AV9cmes1H',fld:'vCMES1H',pic:'ZZZZZZZZ9'},{av:'AV10cmes1M',fld:'vCMES1M',pic:'ZZZZZZZZ9'},{av:'AV11cmes2H',fld:'vCMES2H',pic:'ZZZZZZZZ9'},{av:'AV12cmes2M',fld:'vCMES2M',pic:'ZZZZZZZZ9'}]");
         setEventMetadata("REFRESH",",oparms:[]}");
         setEventMetadata("'TOGGLE'","{handler:'E180S1',iparms:[{av:'divAdvancedcontainer_Class',ctrl:'ADVANCEDCONTAINER',prop:'Class'},{ctrl:'BTNTOGGLE',prop:'Class'}]");
         setEventMetadata("'TOGGLE'",",oparms:[{av:'divAdvancedcontainer_Class',ctrl:'ADVANCEDCONTAINER',prop:'Class'},{ctrl:'BTNTOGGLE',prop:'Class'}]}");
         setEventMetadata("LBLFYACTIVIDADPLANCANTPARTIDFILTER.CLICK","{handler:'E110S1',iparms:[{av:'divFyactividadplancantpartidfiltercontainer_Class',ctrl:'FYACTIVIDADPLANCANTPARTIDFILTERCONTAINER',prop:'Class'}]");
         setEventMetadata("LBLFYACTIVIDADPLANCANTPARTIDFILTER.CLICK",",oparms:[{av:'divFyactividadplancantpartidfiltercontainer_Class',ctrl:'FYACTIVIDADPLANCANTPARTIDFILTERCONTAINER',prop:'Class'},{av:'edtavCfyactividadplancantpartid_Visible',ctrl:'vCFYACTIVIDADPLANCANTPARTID',prop:'Visible'}]}");
         setEventMetadata("LBLACTIVIDADPARAPLANIDFILTER.CLICK","{handler:'E120S1',iparms:[{av:'divActividadparaplanidfiltercontainer_Class',ctrl:'ACTIVIDADPARAPLANIDFILTERCONTAINER',prop:'Class'}]");
         setEventMetadata("LBLACTIVIDADPARAPLANIDFILTER.CLICK",",oparms:[{av:'divActividadparaplanidfiltercontainer_Class',ctrl:'ACTIVIDADPARAPLANIDFILTERCONTAINER',prop:'Class'},{av:'edtavCactividadparaplanid_Visible',ctrl:'vCACTIVIDADPARAPLANID',prop:'Visible'}]}");
         setEventMetadata("LBLFYACTIVIDADPLANCANTPARTFYFILTER.CLICK","{handler:'E130S1',iparms:[{av:'divFyactividadplancantpartfyfiltercontainer_Class',ctrl:'FYACTIVIDADPLANCANTPARTFYFILTERCONTAINER',prop:'Class'}]");
         setEventMetadata("LBLFYACTIVIDADPLANCANTPARTFYFILTER.CLICK",",oparms:[{av:'divFyactividadplancantpartfyfiltercontainer_Class',ctrl:'FYACTIVIDADPLANCANTPARTFYFILTERCONTAINER',prop:'Class'},{av:'edtavCfyactividadplancantpartfy_Visible',ctrl:'vCFYACTIVIDADPLANCANTPARTFY',prop:'Visible'}]}");
         setEventMetadata("LBLMES1HFILTER.CLICK","{handler:'E140S1',iparms:[{av:'divMes1hfiltercontainer_Class',ctrl:'MES1HFILTERCONTAINER',prop:'Class'}]");
         setEventMetadata("LBLMES1HFILTER.CLICK",",oparms:[{av:'divMes1hfiltercontainer_Class',ctrl:'MES1HFILTERCONTAINER',prop:'Class'},{av:'edtavCmes1h_Visible',ctrl:'vCMES1H',prop:'Visible'}]}");
         setEventMetadata("LBLMES1MFILTER.CLICK","{handler:'E150S1',iparms:[{av:'divMes1mfiltercontainer_Class',ctrl:'MES1MFILTERCONTAINER',prop:'Class'}]");
         setEventMetadata("LBLMES1MFILTER.CLICK",",oparms:[{av:'divMes1mfiltercontainer_Class',ctrl:'MES1MFILTERCONTAINER',prop:'Class'},{av:'edtavCmes1m_Visible',ctrl:'vCMES1M',prop:'Visible'}]}");
         setEventMetadata("LBLMES2HFILTER.CLICK","{handler:'E160S1',iparms:[{av:'divMes2hfiltercontainer_Class',ctrl:'MES2HFILTERCONTAINER',prop:'Class'}]");
         setEventMetadata("LBLMES2HFILTER.CLICK",",oparms:[{av:'divMes2hfiltercontainer_Class',ctrl:'MES2HFILTERCONTAINER',prop:'Class'},{av:'edtavCmes2h_Visible',ctrl:'vCMES2H',prop:'Visible'}]}");
         setEventMetadata("LBLMES2MFILTER.CLICK","{handler:'E170S1',iparms:[{av:'divMes2mfiltercontainer_Class',ctrl:'MES2MFILTERCONTAINER',prop:'Class'}]");
         setEventMetadata("LBLMES2MFILTER.CLICK",",oparms:[{av:'divMes2mfiltercontainer_Class',ctrl:'MES2MFILTERCONTAINER',prop:'Class'},{av:'edtavCmes2m_Visible',ctrl:'vCMES2M',prop:'Visible'}]}");
         setEventMetadata("ENTER","{handler:'E210S2',iparms:[{av:'A14FYActividadPlanCantPartid',fld:'FYACTIVIDADPLANCANTPARTID',pic:'ZZZZZZZZ9',hsh:true}]");
         setEventMetadata("ENTER",",oparms:[{av:'AV13pFYActividadPlanCantPartid',fld:'vPFYACTIVIDADPLANCANTPARTID',pic:'ZZZZZZZZ9'}]}");
         setEventMetadata("GRID1_FIRSTPAGE","{handler:'subgrid1_firstpage',iparms:[{av:'GRID1_nFirstRecordOnPage'},{av:'GRID1_nEOF'},{av:'subGrid1_Rows',ctrl:'GRID1',prop:'Rows'},{av:'AV6cFYActividadPlanCantPartid',fld:'vCFYACTIVIDADPLANCANTPARTID',pic:'ZZZZZZZZ9'},{av:'AV7cActividadParaPlanid',fld:'vCACTIVIDADPARAPLANID',pic:'ZZZZZZZZ9'},{av:'AV8cFYActividadPlanCantPartFY',fld:'vCFYACTIVIDADPLANCANTPARTFY',pic:'ZZZZZZZZ9'},{av:'AV9cmes1H',fld:'vCMES1H',pic:'ZZZZZZZZ9'},{av:'AV10cmes1M',fld:'vCMES1M',pic:'ZZZZZZZZ9'},{av:'AV11cmes2H',fld:'vCMES2H',pic:'ZZZZZZZZ9'},{av:'AV12cmes2M',fld:'vCMES2M',pic:'ZZZZZZZZ9'}]");
         setEventMetadata("GRID1_FIRSTPAGE",",oparms:[]}");
         setEventMetadata("GRID1_PREVPAGE","{handler:'subgrid1_previouspage',iparms:[{av:'GRID1_nFirstRecordOnPage'},{av:'GRID1_nEOF'},{av:'subGrid1_Rows',ctrl:'GRID1',prop:'Rows'},{av:'AV6cFYActividadPlanCantPartid',fld:'vCFYACTIVIDADPLANCANTPARTID',pic:'ZZZZZZZZ9'},{av:'AV7cActividadParaPlanid',fld:'vCACTIVIDADPARAPLANID',pic:'ZZZZZZZZ9'},{av:'AV8cFYActividadPlanCantPartFY',fld:'vCFYACTIVIDADPLANCANTPARTFY',pic:'ZZZZZZZZ9'},{av:'AV9cmes1H',fld:'vCMES1H',pic:'ZZZZZZZZ9'},{av:'AV10cmes1M',fld:'vCMES1M',pic:'ZZZZZZZZ9'},{av:'AV11cmes2H',fld:'vCMES2H',pic:'ZZZZZZZZ9'},{av:'AV12cmes2M',fld:'vCMES2M',pic:'ZZZZZZZZ9'}]");
         setEventMetadata("GRID1_PREVPAGE",",oparms:[]}");
         setEventMetadata("GRID1_NEXTPAGE","{handler:'subgrid1_nextpage',iparms:[{av:'GRID1_nFirstRecordOnPage'},{av:'GRID1_nEOF'},{av:'subGrid1_Rows',ctrl:'GRID1',prop:'Rows'},{av:'AV6cFYActividadPlanCantPartid',fld:'vCFYACTIVIDADPLANCANTPARTID',pic:'ZZZZZZZZ9'},{av:'AV7cActividadParaPlanid',fld:'vCACTIVIDADPARAPLANID',pic:'ZZZZZZZZ9'},{av:'AV8cFYActividadPlanCantPartFY',fld:'vCFYACTIVIDADPLANCANTPARTFY',pic:'ZZZZZZZZ9'},{av:'AV9cmes1H',fld:'vCMES1H',pic:'ZZZZZZZZ9'},{av:'AV10cmes1M',fld:'vCMES1M',pic:'ZZZZZZZZ9'},{av:'AV11cmes2H',fld:'vCMES2H',pic:'ZZZZZZZZ9'},{av:'AV12cmes2M',fld:'vCMES2M',pic:'ZZZZZZZZ9'}]");
         setEventMetadata("GRID1_NEXTPAGE",",oparms:[]}");
         setEventMetadata("GRID1_LASTPAGE","{handler:'subgrid1_lastpage',iparms:[{av:'GRID1_nFirstRecordOnPage'},{av:'GRID1_nEOF'},{av:'subGrid1_Rows',ctrl:'GRID1',prop:'Rows'},{av:'AV6cFYActividadPlanCantPartid',fld:'vCFYACTIVIDADPLANCANTPARTID',pic:'ZZZZZZZZ9'},{av:'AV7cActividadParaPlanid',fld:'vCACTIVIDADPARAPLANID',pic:'ZZZZZZZZ9'},{av:'AV8cFYActividadPlanCantPartFY',fld:'vCFYACTIVIDADPLANCANTPARTFY',pic:'ZZZZZZZZ9'},{av:'AV9cmes1H',fld:'vCMES1H',pic:'ZZZZZZZZ9'},{av:'AV10cmes1M',fld:'vCMES1M',pic:'ZZZZZZZZ9'},{av:'AV11cmes2H',fld:'vCMES2H',pic:'ZZZZZZZZ9'},{av:'AV12cmes2M',fld:'vCMES2M',pic:'ZZZZZZZZ9'}]");
         setEventMetadata("GRID1_LASTPAGE",",oparms:[]}");
         return  ;
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         gxfirstwebparm = "";
         gxfirstwebparm_bkp = "";
         GXKey = "";
         sDynURL = "";
         FormProcess = "";
         bodyStyle = "";
         GX_FocusControl = "";
         Form = new GXWebForm();
         sPrefix = "";
         lblLblfyactividadplancantpartidfilter_Jsonclick = "";
         TempTags = "";
         lblLblactividadparaplanidfilter_Jsonclick = "";
         lblLblfyactividadplancantpartfyfilter_Jsonclick = "";
         lblLblmes1hfilter_Jsonclick = "";
         lblLblmes1mfilter_Jsonclick = "";
         lblLblmes2hfilter_Jsonclick = "";
         lblLblmes2mfilter_Jsonclick = "";
         ClassString = "";
         StyleString = "";
         bttBtntoggle_Jsonclick = "";
         Grid1Container = new GXWebGrid( context);
         sStyleString = "";
         subGrid1_Linesclass = "";
         Grid1Column = new GXWebColumn();
         AV5LinkSelection = "";
         bttBtn_cancel_Jsonclick = "";
         sEvt = "";
         EvtGridId = "";
         EvtRowId = "";
         sEvtType = "";
         AV17Linkselection_GXI = "";
         scmdbuf = "";
         H000S2_A84mes3H = new int[1] ;
         H000S2_n84mes3H = new bool[] {false} ;
         H000S2_A83mes2M = new int[1] ;
         H000S2_n83mes2M = new bool[] {false} ;
         H000S2_A82mes2H = new int[1] ;
         H000S2_n82mes2H = new bool[] {false} ;
         H000S2_A81mes1M = new int[1] ;
         H000S2_n81mes1M = new bool[] {false} ;
         H000S2_A80mes1H = new int[1] ;
         H000S2_n80mes1H = new bool[] {false} ;
         H000S2_A79FYActividadPlanCantPartFY = new int[1] ;
         H000S2_A23ActividadParaPlanid = new int[1] ;
         H000S2_A14FYActividadPlanCantPartid = new int[1] ;
         H000S3_AGRID1_nRecordCount = new long[1] ;
         AV14ADVANCED_LABEL_TEMPLATE = "";
         Grid1Row = new GXWebRow();
         BackMsgLst = new msglist();
         LclMsgLst = new msglist();
         sImgUrl = "";
         ROClassString = "";
         pr_datastore1 = new DataStoreProvider(context, new GeneXus.Programs.gx00i0__datastore1(),
            new Object[][] {
                new Object[] {
               H000S2_A84mes3H, H000S2_n84mes3H, H000S2_A83mes2M, H000S2_n83mes2M, H000S2_A82mes2H, H000S2_n82mes2H, H000S2_A81mes1M, H000S2_n81mes1M, H000S2_A80mes1H, H000S2_n80mes1H,
               H000S2_A79FYActividadPlanCantPartFY, H000S2_A23ActividadParaPlanid, H000S2_A14FYActividadPlanCantPartid
               }
               , new Object[] {
               H000S3_AGRID1_nRecordCount
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private short nGotPars ;
      private short GxWebError ;
      private short nRC_GXsfl_84 ;
      private short nGXsfl_84_idx=1 ;
      private short GRID1_nEOF ;
      private short initialized ;
      private short gxajaxcallmode ;
      private short wbEnd ;
      private short wbStart ;
      private short subGrid1_Backcolorstyle ;
      private short subGrid1_Titlebackstyle ;
      private short subGrid1_Allowselection ;
      private short subGrid1_Allowhovering ;
      private short subGrid1_Allowcollapsing ;
      private short subGrid1_Collapsed ;
      private short nDonePA ;
      private short gxcookieaux ;
      private short nGXWrapped ;
      private short subGrid1_Backstyle ;
      private int subGrid1_Rows ;
      private int AV6cFYActividadPlanCantPartid ;
      private int AV7cActividadParaPlanid ;
      private int AV8cFYActividadPlanCantPartFY ;
      private int AV9cmes1H ;
      private int AV10cmes1M ;
      private int AV11cmes2H ;
      private int AV12cmes2M ;
      private int AV13pFYActividadPlanCantPartid ;
      private int edtavCfyactividadplancantpartid_Enabled ;
      private int edtavCfyactividadplancantpartid_Visible ;
      private int edtavCactividadparaplanid_Enabled ;
      private int edtavCactividadparaplanid_Visible ;
      private int edtavCfyactividadplancantpartfy_Enabled ;
      private int edtavCfyactividadplancantpartfy_Visible ;
      private int edtavCmes1h_Enabled ;
      private int edtavCmes1h_Visible ;
      private int edtavCmes1m_Enabled ;
      private int edtavCmes1m_Visible ;
      private int edtavCmes2h_Enabled ;
      private int edtavCmes2h_Visible ;
      private int edtavCmes2m_Enabled ;
      private int edtavCmes2m_Visible ;
      private int subGrid1_Titlebackcolor ;
      private int subGrid1_Allbackcolor ;
      private int A14FYActividadPlanCantPartid ;
      private int A23ActividadParaPlanid ;
      private int A79FYActividadPlanCantPartFY ;
      private int A80mes1H ;
      private int A81mes1M ;
      private int A82mes2H ;
      private int A83mes2M ;
      private int A84mes3H ;
      private int subGrid1_Selectedindex ;
      private int subGrid1_Selectioncolor ;
      private int subGrid1_Hoveringcolor ;
      private int subGrid1_Islastpage ;
      private int GXPagingFrom2 ;
      private int GXPagingTo2 ;
      private int idxLst ;
      private int subGrid1_Backcolor ;
      private long GRID1_nFirstRecordOnPage ;
      private long GRID1_nCurrentRecord ;
      private long GRID1_nRecordCount ;
      private String divAdvancedcontainer_Class ;
      private String bttBtntoggle_Class ;
      private String divFyactividadplancantpartidfiltercontainer_Class ;
      private String divActividadparaplanidfiltercontainer_Class ;
      private String divFyactividadplancantpartfyfiltercontainer_Class ;
      private String divMes1hfiltercontainer_Class ;
      private String divMes1mfiltercontainer_Class ;
      private String divMes2hfiltercontainer_Class ;
      private String divMes2mfiltercontainer_Class ;
      private String gxfirstwebparm ;
      private String gxfirstwebparm_bkp ;
      private String sGXsfl_84_idx="0001" ;
      private String GXKey ;
      private String sDynURL ;
      private String FormProcess ;
      private String bodyStyle ;
      private String GX_FocusControl ;
      private String sPrefix ;
      private String divMain_Internalname ;
      private String divAdvancedcontainer_Internalname ;
      private String divFyactividadplancantpartidfiltercontainer_Internalname ;
      private String lblLblfyactividadplancantpartidfilter_Internalname ;
      private String lblLblfyactividadplancantpartidfilter_Jsonclick ;
      private String edtavCfyactividadplancantpartid_Internalname ;
      private String TempTags ;
      private String edtavCfyactividadplancantpartid_Jsonclick ;
      private String divActividadparaplanidfiltercontainer_Internalname ;
      private String lblLblactividadparaplanidfilter_Internalname ;
      private String lblLblactividadparaplanidfilter_Jsonclick ;
      private String edtavCactividadparaplanid_Internalname ;
      private String edtavCactividadparaplanid_Jsonclick ;
      private String divFyactividadplancantpartfyfiltercontainer_Internalname ;
      private String lblLblfyactividadplancantpartfyfilter_Internalname ;
      private String lblLblfyactividadplancantpartfyfilter_Jsonclick ;
      private String edtavCfyactividadplancantpartfy_Internalname ;
      private String edtavCfyactividadplancantpartfy_Jsonclick ;
      private String divMes1hfiltercontainer_Internalname ;
      private String lblLblmes1hfilter_Internalname ;
      private String lblLblmes1hfilter_Jsonclick ;
      private String edtavCmes1h_Internalname ;
      private String edtavCmes1h_Jsonclick ;
      private String divMes1mfiltercontainer_Internalname ;
      private String lblLblmes1mfilter_Internalname ;
      private String lblLblmes1mfilter_Jsonclick ;
      private String edtavCmes1m_Internalname ;
      private String edtavCmes1m_Jsonclick ;
      private String divMes2hfiltercontainer_Internalname ;
      private String lblLblmes2hfilter_Internalname ;
      private String lblLblmes2hfilter_Jsonclick ;
      private String edtavCmes2h_Internalname ;
      private String edtavCmes2h_Jsonclick ;
      private String divMes2mfiltercontainer_Internalname ;
      private String lblLblmes2mfilter_Internalname ;
      private String lblLblmes2mfilter_Jsonclick ;
      private String edtavCmes2m_Internalname ;
      private String edtavCmes2m_Jsonclick ;
      private String divGridtable_Internalname ;
      private String ClassString ;
      private String StyleString ;
      private String bttBtntoggle_Internalname ;
      private String bttBtntoggle_Jsonclick ;
      private String sStyleString ;
      private String subGrid1_Internalname ;
      private String subGrid1_Class ;
      private String subGrid1_Linesclass ;
      private String subGrid1_Header ;
      private String edtavLinkselection_Link ;
      private String edtFYActividadPlanCantPartFY_Link ;
      private String bttBtn_cancel_Internalname ;
      private String bttBtn_cancel_Jsonclick ;
      private String sEvt ;
      private String EvtGridId ;
      private String EvtRowId ;
      private String sEvtType ;
      private String edtavLinkselection_Internalname ;
      private String edtFYActividadPlanCantPartid_Internalname ;
      private String edtActividadParaPlanid_Internalname ;
      private String edtFYActividadPlanCantPartFY_Internalname ;
      private String edtmes1H_Internalname ;
      private String edtmes1M_Internalname ;
      private String edtmes2H_Internalname ;
      private String edtmes2M_Internalname ;
      private String edtmes3H_Internalname ;
      private String scmdbuf ;
      private String AV14ADVANCED_LABEL_TEMPLATE ;
      private String sGXsfl_84_fel_idx="0001" ;
      private String sImgUrl ;
      private String ROClassString ;
      private String edtFYActividadPlanCantPartid_Jsonclick ;
      private String edtActividadParaPlanid_Jsonclick ;
      private String edtFYActividadPlanCantPartFY_Jsonclick ;
      private String edtmes1H_Jsonclick ;
      private String edtmes1M_Jsonclick ;
      private String edtmes2H_Jsonclick ;
      private String edtmes2M_Jsonclick ;
      private String edtmes3H_Jsonclick ;
      private bool entryPointCalled ;
      private bool toggleJsOutput ;
      private bool wbLoad ;
      private bool Rfr0gs ;
      private bool wbErr ;
      private bool bGXsfl_84_Refreshing=false ;
      private bool n80mes1H ;
      private bool n81mes1M ;
      private bool n82mes2H ;
      private bool n83mes2M ;
      private bool n84mes3H ;
      private bool gxdyncontrolsrefreshing ;
      private bool returnInSub ;
      private bool AV5LinkSelection_IsBlob ;
      private String AV17Linkselection_GXI ;
      private String AV5LinkSelection ;
      private GXWebGrid Grid1Container ;
      private GXWebRow Grid1Row ;
      private GXWebColumn Grid1Column ;
      private IGxDataStore dsDataStore1 ;
      private IGxDataStore dsDefault ;
      private IDataStoreProvider pr_datastore1 ;
      private int[] H000S2_A84mes3H ;
      private bool[] H000S2_n84mes3H ;
      private int[] H000S2_A83mes2M ;
      private bool[] H000S2_n83mes2M ;
      private int[] H000S2_A82mes2H ;
      private bool[] H000S2_n82mes2H ;
      private int[] H000S2_A81mes1M ;
      private bool[] H000S2_n81mes1M ;
      private int[] H000S2_A80mes1H ;
      private bool[] H000S2_n80mes1H ;
      private int[] H000S2_A79FYActividadPlanCantPartFY ;
      private int[] H000S2_A23ActividadParaPlanid ;
      private int[] H000S2_A14FYActividadPlanCantPartid ;
      private long[] H000S3_AGRID1_nRecordCount ;
      private msglist BackMsgLst ;
      private msglist LclMsgLst ;
      private int aP0_pFYActividadPlanCantPartid ;
      private GXWebForm Form ;
   }

   public class gx00i0__datastore1 : DataStoreHelperBase, IDataStoreHelper
   {
      protected Object[] conditional_H000S2( IGxContext context ,
                                             int AV7cActividadParaPlanid ,
                                             int AV8cFYActividadPlanCantPartFY ,
                                             int AV9cmes1H ,
                                             int AV10cmes1M ,
                                             int AV11cmes2H ,
                                             int AV12cmes2M ,
                                             int A23ActividadParaPlanid ,
                                             int A79FYActividadPlanCantPartFY ,
                                             int A80mes1H ,
                                             int A81mes1M ,
                                             int A82mes2H ,
                                             int A83mes2M ,
                                             int AV6cFYActividadPlanCantPartid )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int1 ;
         GXv_int1 = new short [10] ;
         Object[] GXv_Object2 ;
         GXv_Object2 = new Object [2] ;
         String sSelectString ;
         String sFromString ;
         String sOrderString ;
         sSelectString = " [mes3H], [mes2M], [mes2H], [mes1M], [mes1H], [FY], [idActividadParaPlan], [id]";
         sFromString = " FROM dbo.[FYActividadPlanCantPart] WITH (NOLOCK)";
         sOrderString = "";
         sWhereString = sWhereString + " WHERE ([id] >= @AV6cFYActividadPlanCantPartid)";
         if ( ! (0==AV7cActividadParaPlanid) )
         {
            sWhereString = sWhereString + " and ([idActividadParaPlan] >= @AV7cActividadParaPlanid)";
         }
         else
         {
            GXv_int1[1] = 1;
         }
         if ( ! (0==AV8cFYActividadPlanCantPartFY) )
         {
            sWhereString = sWhereString + " and ([FY] >= @AV8cFYActividadPlanCantPartFY)";
         }
         else
         {
            GXv_int1[2] = 1;
         }
         if ( ! (0==AV9cmes1H) )
         {
            sWhereString = sWhereString + " and ([mes1H] >= @AV9cmes1H)";
         }
         else
         {
            GXv_int1[3] = 1;
         }
         if ( ! (0==AV10cmes1M) )
         {
            sWhereString = sWhereString + " and ([mes1M] >= @AV10cmes1M)";
         }
         else
         {
            GXv_int1[4] = 1;
         }
         if ( ! (0==AV11cmes2H) )
         {
            sWhereString = sWhereString + " and ([mes2H] >= @AV11cmes2H)";
         }
         else
         {
            GXv_int1[5] = 1;
         }
         if ( ! (0==AV12cmes2M) )
         {
            sWhereString = sWhereString + " and ([mes2M] >= @AV12cmes2M)";
         }
         else
         {
            GXv_int1[6] = 1;
         }
         sOrderString = sOrderString + " ORDER BY [id]";
         scmdbuf = "SELECT " + sSelectString + sFromString + sWhereString + "" + sOrderString + " OFFSET " + "@GXPagingFrom2" + " ROWS FETCH NEXT CAST((SELECT CASE WHEN " + "@GXPagingTo2" + " > 0 THEN " + "@GXPagingTo2" + " ELSE 1e9 END) AS INTEGER) ROWS ONLY";
         GXv_Object2[0] = scmdbuf;
         GXv_Object2[1] = GXv_int1;
         return GXv_Object2 ;
      }

      protected Object[] conditional_H000S3( IGxContext context ,
                                             int AV7cActividadParaPlanid ,
                                             int AV8cFYActividadPlanCantPartFY ,
                                             int AV9cmes1H ,
                                             int AV10cmes1M ,
                                             int AV11cmes2H ,
                                             int AV12cmes2M ,
                                             int A23ActividadParaPlanid ,
                                             int A79FYActividadPlanCantPartFY ,
                                             int A80mes1H ,
                                             int A81mes1M ,
                                             int A82mes2H ,
                                             int A83mes2M ,
                                             int AV6cFYActividadPlanCantPartid )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int3 ;
         GXv_int3 = new short [7] ;
         Object[] GXv_Object4 ;
         GXv_Object4 = new Object [2] ;
         scmdbuf = "SELECT COUNT(*) FROM dbo.[FYActividadPlanCantPart] WITH (NOLOCK)";
         scmdbuf = scmdbuf + " WHERE ([id] >= @AV6cFYActividadPlanCantPartid)";
         if ( ! (0==AV7cActividadParaPlanid) )
         {
            sWhereString = sWhereString + " and ([idActividadParaPlan] >= @AV7cActividadParaPlanid)";
         }
         else
         {
            GXv_int3[1] = 1;
         }
         if ( ! (0==AV8cFYActividadPlanCantPartFY) )
         {
            sWhereString = sWhereString + " and ([FY] >= @AV8cFYActividadPlanCantPartFY)";
         }
         else
         {
            GXv_int3[2] = 1;
         }
         if ( ! (0==AV9cmes1H) )
         {
            sWhereString = sWhereString + " and ([mes1H] >= @AV9cmes1H)";
         }
         else
         {
            GXv_int3[3] = 1;
         }
         if ( ! (0==AV10cmes1M) )
         {
            sWhereString = sWhereString + " and ([mes1M] >= @AV10cmes1M)";
         }
         else
         {
            GXv_int3[4] = 1;
         }
         if ( ! (0==AV11cmes2H) )
         {
            sWhereString = sWhereString + " and ([mes2H] >= @AV11cmes2H)";
         }
         else
         {
            GXv_int3[5] = 1;
         }
         if ( ! (0==AV12cmes2M) )
         {
            sWhereString = sWhereString + " and ([mes2M] >= @AV12cmes2M)";
         }
         else
         {
            GXv_int3[6] = 1;
         }
         scmdbuf = scmdbuf + sWhereString;
         scmdbuf = scmdbuf + "";
         GXv_Object4[0] = scmdbuf;
         GXv_Object4[1] = GXv_int3;
         return GXv_Object4 ;
      }

      public override Object [] getDynamicStatement( int cursor ,
                                                     IGxContext context ,
                                                     Object [] dynConstraints )
      {
         switch ( cursor )
         {
               case 0 :
                     return conditional_H000S2(context, (int)dynConstraints[0] , (int)dynConstraints[1] , (int)dynConstraints[2] , (int)dynConstraints[3] , (int)dynConstraints[4] , (int)dynConstraints[5] , (int)dynConstraints[6] , (int)dynConstraints[7] , (int)dynConstraints[8] , (int)dynConstraints[9] , (int)dynConstraints[10] , (int)dynConstraints[11] , (int)dynConstraints[12] );
               case 1 :
                     return conditional_H000S3(context, (int)dynConstraints[0] , (int)dynConstraints[1] , (int)dynConstraints[2] , (int)dynConstraints[3] , (int)dynConstraints[4] , (int)dynConstraints[5] , (int)dynConstraints[6] , (int)dynConstraints[7] , (int)dynConstraints[8] , (int)dynConstraints[9] , (int)dynConstraints[10] , (int)dynConstraints[11] , (int)dynConstraints[12] );
         }
         return base.getDynamicStatement(cursor, context, dynConstraints);
      }

      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmH000S2 ;
          prmH000S2 = new Object[] {
          new Object[] {"@AV6cFYActividadPlanCantPartid",SqlDbType.Int,9,0} ,
          new Object[] {"@AV7cActividadParaPlanid",SqlDbType.Int,9,0} ,
          new Object[] {"@AV8cFYActividadPlanCantPartFY",SqlDbType.Int,9,0} ,
          new Object[] {"@AV9cmes1H",SqlDbType.Int,9,0} ,
          new Object[] {"@AV10cmes1M",SqlDbType.Int,9,0} ,
          new Object[] {"@AV11cmes2H",SqlDbType.Int,9,0} ,
          new Object[] {"@AV12cmes2M",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingFrom2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingTo2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingTo2",SqlDbType.Int,9,0}
          } ;
          Object[] prmH000S3 ;
          prmH000S3 = new Object[] {
          new Object[] {"@AV6cFYActividadPlanCantPartid",SqlDbType.Int,9,0} ,
          new Object[] {"@AV7cActividadParaPlanid",SqlDbType.Int,9,0} ,
          new Object[] {"@AV8cFYActividadPlanCantPartFY",SqlDbType.Int,9,0} ,
          new Object[] {"@AV9cmes1H",SqlDbType.Int,9,0} ,
          new Object[] {"@AV10cmes1M",SqlDbType.Int,9,0} ,
          new Object[] {"@AV11cmes2H",SqlDbType.Int,9,0} ,
          new Object[] {"@AV12cmes2M",SqlDbType.Int,9,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("H000S2", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH000S2,11,0,false,false )
             ,new CursorDef("H000S3", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH000S3,1,0,false,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((int[]) buf[2])[0] = rslt.getInt(2) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                ((int[]) buf[4])[0] = rslt.getInt(3) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(3);
                ((int[]) buf[6])[0] = rslt.getInt(4) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(4);
                ((int[]) buf[8])[0] = rslt.getInt(5) ;
                ((bool[]) buf[9])[0] = rslt.wasNull(5);
                ((int[]) buf[10])[0] = rslt.getInt(6) ;
                ((int[]) buf[11])[0] = rslt.getInt(7) ;
                ((int[]) buf[12])[0] = rslt.getInt(8) ;
                return;
             case 1 :
                ((long[]) buf[0])[0] = rslt.getLong(1) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       short sIdx ;
       switch ( cursor )
       {
             case 0 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[10]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[11]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[12]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[13]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[14]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[15]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[16]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[17]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[18]);
                }
                if ( (short)parms[9] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[19]);
                }
                return;
             case 1 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[7]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[8]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[9]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[10]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[11]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[12]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[13]);
                }
                return;
       }
    }

    public String getDataStoreName( )
    {
       return "DATASTORE1";
    }

 }

}
