/*
               File: Gx00N0
        Description: Selection List FYActividad Plan Cant Act
             Author: GeneXus C# Generator version 16_0_0-127771
       Generated on: 1/3/2019 18:2:0.26
       Program type: Callable routine
          Main DBMS: SQL Server
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Xml.Serialization;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class gx00n0 : GXDataArea, System.Web.SessionState.IRequiresSessionState
   {
      public gx00n0( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsDataStore1 = context.GetDataStore("DataStore1");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("Carmine");
      }

      public gx00n0( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsDataStore1 = context.GetDataStore("DataStore1");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( out int aP0_pFYActividadPlanCantActid )
      {
         this.AV13pFYActividadPlanCantActid = 0 ;
         executePrivate();
         aP0_pFYActividadPlanCantActid=this.AV13pFYActividadPlanCantActid;
      }

      void executePrivate( )
      {
         isStatic = false;
         webExecute();
      }

      protected override void createObjects( )
      {
      }

      protected void INITWEB( )
      {
         initialize_properties( ) ;
         if ( nGotPars == 0 )
         {
            entryPointCalled = false;
            gxfirstwebparm = GetNextPar( );
            gxfirstwebparm_bkp = gxfirstwebparm;
            gxfirstwebparm = DecryptAjaxCall( gxfirstwebparm);
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
            if ( StringUtil.StrCmp(gxfirstwebparm, "dyncall") == 0 )
            {
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               dyncall( GetNextPar( )) ;
               return  ;
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxEvt") == 0 )
            {
               setAjaxEventMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = GetNextPar( );
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxfullajaxEvt") == 0 )
            {
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = GetNextPar( );
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxNewRow_"+"Grid1") == 0 )
            {
               nRC_GXsfl_84 = (short)(NumberUtil.Val( GetNextPar( ), "."));
               nGXsfl_84_idx = (short)(NumberUtil.Val( GetNextPar( ), "."));
               sGXsfl_84_idx = GetNextPar( );
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxnrGrid1_newrow( ) ;
               return  ;
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxGridRefresh_"+"Grid1") == 0 )
            {
               subGrid1_Rows = (int)(NumberUtil.Val( GetNextPar( ), "."));
               AV6cFYActividadPlanCantActid = (int)(NumberUtil.Val( GetNextPar( ), "."));
               AV7cActividadParaPlanid = (int)(NumberUtil.Val( GetNextPar( ), "."));
               AV8cFYActividadPlanCantActFY = (int)(NumberUtil.Val( GetNextPar( ), "."));
               AV9cmes1 = (int)(NumberUtil.Val( GetNextPar( ), "."));
               AV10cmes2 = (int)(NumberUtil.Val( GetNextPar( ), "."));
               AV11cmes3 = (int)(NumberUtil.Val( GetNextPar( ), "."));
               AV12cmes4 = (int)(NumberUtil.Val( GetNextPar( ), "."));
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxgrGrid1_refresh( subGrid1_Rows, AV6cFYActividadPlanCantActid, AV7cActividadParaPlanid, AV8cFYActividadPlanCantActFY, AV9cmes1, AV10cmes2, AV11cmes3, AV12cmes4) ;
               GXKey = Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
               GXKey = Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
               context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               return  ;
            }
            else
            {
               if ( ! IsValidAjaxCall( false) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = gxfirstwebparm_bkp;
            }
            if ( ! entryPointCalled && ! ( isAjaxCallMode( ) || isFullAjaxMode( ) ) )
            {
               AV13pFYActividadPlanCantActid = (int)(NumberUtil.Val( gxfirstwebparm, "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13pFYActividadPlanCantActid", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13pFYActividadPlanCantActid), 9, 0)));
            }
            if ( context.isSpaRequest( ) )
            {
               enableJsOutput();
            }
         }
         if ( ! context.IsLocalStorageSupported( ) )
         {
            context.PushCurrentUrl();
         }
      }

      public override void webExecute( )
      {
         if ( initialized == 0 )
         {
            createObjects();
            initialize();
         }
         INITWEB( ) ;
         if ( ! isAjaxCallMode( ) )
         {
            MasterPageObj = (GXMasterPage) ClassLoader.GetInstance("rwdpromptmasterpage", "GeneXus.Programs.rwdpromptmasterpage", new Object[] {new GxContext( context.handle, context.DataStores, context.HttpContext)});
            MasterPageObj.setDataArea(this,true);
            ValidateSpaRequest();
            MasterPageObj.webExecute();
            if ( ( GxWebError == 0 ) && context.isAjaxRequest( ) )
            {
               enableOutput();
               if ( ! context.isAjaxRequest( ) )
               {
                  context.GX_webresponse.AppendHeader("Cache-Control", "no-store");
               }
               if ( ! context.WillRedirect( ) )
               {
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               }
               else
               {
                  if ( context.isAjaxRequest( ) )
                  {
                     disableOutput();
                  }
                  RenderHtmlHeaders( ) ;
                  context.Redirect( context.wjLoc );
                  context.DispatchAjaxCommands();
               }
            }
         }
         this.cleanup();
      }

      public override short ExecuteStartEvent( )
      {
         PA0U2( ) ;
         gxajaxcallmode = (short)((isAjaxCallMode( ) ? 1 : 0));
         if ( ( gxajaxcallmode == 0 ) && ( GxWebError == 0 ) )
         {
            START0U2( ) ;
         }
         return gxajaxcallmode ;
      }

      public override void RenderHtmlHeaders( )
      {
         GxWebStd.gx_html_headers( context, 0, "", "", Form.Meta, Form.Metaequiv, true);
      }

      public override void RenderHtmlOpenForm( )
      {
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.WriteHtmlText( "<title>") ;
         context.SendWebValue( Form.Caption) ;
         context.WriteHtmlTextNl( "</title>") ;
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         if ( StringUtil.Len( sDynURL) > 0 )
         {
            context.WriteHtmlText( "<BASE href=\""+sDynURL+"\" />") ;
         }
         define_styles( ) ;
         if ( nGXWrapped != 1 )
         {
            MasterPageObj.master_styles();
         }
         if ( ( ( context.GetBrowserType( ) == 1 ) || ( context.GetBrowserType( ) == 5 ) ) && ( StringUtil.StrCmp(context.GetBrowserVersion( ), "7.0") == 0 ) )
         {
            context.AddJavascriptSource("json2.js", "?"+context.GetBuildNumber( 127771), false);
         }
         context.AddJavascriptSource("jquery.js", "?"+context.GetBuildNumber( 127771), false);
         context.AddJavascriptSource("gxtimezone.js", "?"+context.GetBuildNumber( 127771), false);
         context.AddJavascriptSource("gxgral.js", "?"+context.GetBuildNumber( 127771), false);
         context.AddJavascriptSource("gxcfg.js", "?201913182037", false);
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.WriteHtmlText( Form.Headerrawhtml) ;
         context.CloseHtmlHeader();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         FormProcess = " data-HasEnter=\"true\" data-Skiponenter=\"false\"";
         context.WriteHtmlText( "<body ") ;
         bodyStyle = "" + "background-color:" + context.BuildHTMLColor( Form.Backcolor) + ";color:" + context.BuildHTMLColor( Form.Textcolor) + ";";
         if ( nGXWrapped == 0 )
         {
            bodyStyle = bodyStyle + "-moz-opacity:0;opacity:0;";
         }
         if ( ! ( String.IsNullOrEmpty(StringUtil.RTrim( Form.Background)) ) )
         {
            bodyStyle = bodyStyle + " background-image:url(" + context.convertURL( Form.Background) + ")";
         }
         context.WriteHtmlText( " "+"class=\"form-horizontal Form\""+" "+ "style='"+bodyStyle+"'") ;
         context.WriteHtmlText( FormProcess+">") ;
         context.skipLines(1);
         context.WriteHtmlTextNl( "<form id=\"MAINFORM\" autocomplete=\"off\" name=\"MAINFORM\" method=\"post\" tabindex=-1  class=\"form-horizontal Form\" data-gx-class=\"form-horizontal Form\" novalidate action=\""+formatLink("gx00n0.aspx") + "?" + UrlEncode("" +AV13pFYActividadPlanCantActid)+"\">") ;
         GxWebStd.gx_hidden_field( context, "_EventName", "");
         GxWebStd.gx_hidden_field( context, "_EventGridId", "");
         GxWebStd.gx_hidden_field( context, "_EventRowId", "");
         context.WriteHtmlText( "<input type=\"submit\" title=\"submit\" style=\"display:none\" disabled>") ;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, "FORM", "Class", "form-horizontal Form", true);
         toggleJsOutput = isJsOutputEnabled( );
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
      }

      protected void send_integrity_footer_hashes( )
      {
         GXKey = Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
      }

      protected void SendCloseFormHiddens( )
      {
         /* Send hidden variables. */
         GxWebStd.gx_hidden_field( context, "GXH_vCFYACTIVIDADPLANCANTACTID", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV6cFYActividadPlanCantActid), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vCACTIVIDADPARAPLANID", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV7cActividadParaPlanid), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vCFYACTIVIDADPLANCANTACTFY", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV8cFYActividadPlanCantActFY), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vCMES1", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV9cmes1), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vCMES2", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV10cmes2), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vCMES3", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV11cmes3), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vCMES4", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV12cmes4), 9, 0, ".", "")));
         /* Send saved values. */
         send_integrity_footer_hashes( ) ;
         GxWebStd.gx_hidden_field( context, "nRC_GXsfl_84", StringUtil.LTrim( StringUtil.NToC( (decimal)(nRC_GXsfl_84), 4, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "vPFYACTIVIDADPLANCANTACTID", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV13pFYActividadPlanCantActid), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "GRID1_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID1_nFirstRecordOnPage), 15, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "GRID1_nEOF", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID1_nEOF), 1, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "ADVANCEDCONTAINER_Class", StringUtil.RTrim( divAdvancedcontainer_Class));
         GxWebStd.gx_hidden_field( context, "BTNTOGGLE_Class", StringUtil.RTrim( bttBtntoggle_Class));
         GxWebStd.gx_hidden_field( context, "FYACTIVIDADPLANCANTACTIDFILTERCONTAINER_Class", StringUtil.RTrim( divFyactividadplancantactidfiltercontainer_Class));
         GxWebStd.gx_hidden_field( context, "ACTIVIDADPARAPLANIDFILTERCONTAINER_Class", StringUtil.RTrim( divActividadparaplanidfiltercontainer_Class));
         GxWebStd.gx_hidden_field( context, "FYACTIVIDADPLANCANTACTFYFILTERCONTAINER_Class", StringUtil.RTrim( divFyactividadplancantactfyfiltercontainer_Class));
         GxWebStd.gx_hidden_field( context, "MES1FILTERCONTAINER_Class", StringUtil.RTrim( divMes1filtercontainer_Class));
         GxWebStd.gx_hidden_field( context, "MES2FILTERCONTAINER_Class", StringUtil.RTrim( divMes2filtercontainer_Class));
         GxWebStd.gx_hidden_field( context, "MES3FILTERCONTAINER_Class", StringUtil.RTrim( divMes3filtercontainer_Class));
         GxWebStd.gx_hidden_field( context, "MES4FILTERCONTAINER_Class", StringUtil.RTrim( divMes4filtercontainer_Class));
      }

      public override void RenderHtmlCloseForm( )
      {
         SendCloseFormHiddens( ) ;
         GxWebStd.gx_hidden_field( context, "GX_FocusControl", "notset");
         SendAjaxEncryptionKey();
         SendSecurityToken((String)(sPrefix));
         SendComponentObjects();
         SendServerCommands();
         SendState();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         context.WriteHtmlTextNl( "</form>") ;
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         include_jscripts( ) ;
      }

      public override void RenderHtmlContent( )
      {
         gxajaxcallmode = (short)((isAjaxCallMode( ) ? 1 : 0));
         if ( ( gxajaxcallmode == 0 ) && ( GxWebError == 0 ) )
         {
            context.WriteHtmlText( "<div") ;
            GxWebStd.ClassAttribute( context, "gx-ct-body"+" "+(String.IsNullOrEmpty(StringUtil.RTrim( Form.Class)) ? "form-horizontal Form" : Form.Class)+"-fx");
            context.WriteHtmlText( ">") ;
            WE0U2( ) ;
            context.WriteHtmlText( "</div>") ;
         }
      }

      public override void DispatchEvents( )
      {
         EVT0U2( ) ;
      }

      public override bool HasEnterEvent( )
      {
         return true ;
      }

      public override GXWebForm GetForm( )
      {
         return Form ;
      }

      public override String GetSelfLink( )
      {
         return formatLink("gx00n0.aspx") + "?" + UrlEncode("" +AV13pFYActividadPlanCantActid) ;
      }

      public override String GetPgmname( )
      {
         return "Gx00N0" ;
      }

      public override String GetPgmdesc( )
      {
         return "Selection List FYActividad Plan Cant Act" ;
      }

      protected void WB0U0( )
      {
         if ( context.isAjaxRequest( ) )
         {
            disableOutput();
         }
         if ( ! wbLoad )
         {
            if ( nGXWrapped == 1 )
            {
               RenderHtmlHeaders( ) ;
               RenderHtmlOpenForm( ) ;
            }
            GxWebStd.gx_msg_list( context, "", context.GX_msglist.DisplayMode, "", "", "", "false");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "Section", "left", "top", " "+"data-gx-base-lib=\"bootstrapv3\""+" "+"data-abstract-form"+" ", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, divMain_Internalname, 1, 0, "px", 0, "px", "ContainerFluid PromptContainer", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12 col-sm-3 PromptAdvancedBarCell", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, divAdvancedcontainer_Internalname, 1, 0, "px", 0, "px", divAdvancedcontainer_Class, "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, divFyactividadplancantactidfiltercontainer_Internalname, 1, 0, "px", 0, "px", divFyactividadplancantactidfiltercontainer_Class, "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12", "left", "top", "", "", "div");
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblLblfyactividadplancantactidfilter_Internalname, "FYActividad Plan Cant Actid", "", "", lblLblfyactividadplancantactidfilter_Jsonclick, "'"+""+"'"+",false,"+"'"+"e110u1_client"+"'", "", "WWAdvancedLabel WWFilterLabel", 7, "", 1, 1, 1, "HLP_Gx00N0.htm");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12 WWFiltersCell", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", " gx-attribute", "left", "top", "", "", "div");
            /* Attribute/Variable Label */
            GxWebStd.gx_label_element( context, edtavCfyactividadplancantactid_Internalname, "id", "col-sm-3 AttributeLabel", 0, true);
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 16,'',false,'" + sGXsfl_84_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavCfyactividadplancantactid_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV6cFYActividadPlanCantActid), 9, 0, ".", "")), ((edtavCfyactividadplancantactid_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( (decimal)(AV6cFYActividadPlanCantActid), "ZZZZZZZZ9")) : context.localUtil.Format( (decimal)(AV6cFYActividadPlanCantActid), "ZZZZZZZZ9")), TempTags+" onchange=\""+"gx.num.valid_integer( this,',');"+";gx.evt.onchange(this, event)\" "+" onblur=\""+";gx.evt.onblur(this,16);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavCfyactividadplancantactid_Jsonclick, 0, "Attribute", "", "", "", "", edtavCfyactividadplancantactid_Visible, edtavCfyactividadplancantactid_Enabled, 0, "number", "1", 9, "chr", 1, "row", 9, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_Gx00N0.htm");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, divActividadparaplanidfiltercontainer_Internalname, 1, 0, "px", 0, "px", divActividadparaplanidfiltercontainer_Class, "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12", "left", "top", "", "", "div");
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblLblactividadparaplanidfilter_Internalname, "Actividad Para Planid", "", "", lblLblactividadparaplanidfilter_Jsonclick, "'"+""+"'"+",false,"+"'"+"e120u1_client"+"'", "", "WWAdvancedLabel WWFilterLabel", 7, "", 1, 1, 1, "HLP_Gx00N0.htm");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12 WWFiltersCell", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", " gx-attribute", "left", "top", "", "", "div");
            /* Attribute/Variable Label */
            GxWebStd.gx_label_element( context, edtavCactividadparaplanid_Internalname, "id", "col-sm-3 AttributeLabel", 0, true);
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 26,'',false,'" + sGXsfl_84_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavCactividadparaplanid_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV7cActividadParaPlanid), 9, 0, ".", "")), ((edtavCactividadparaplanid_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( (decimal)(AV7cActividadParaPlanid), "ZZZZZZZZ9")) : context.localUtil.Format( (decimal)(AV7cActividadParaPlanid), "ZZZZZZZZ9")), TempTags+" onchange=\""+"gx.num.valid_integer( this,',');"+";gx.evt.onchange(this, event)\" "+" onblur=\""+";gx.evt.onblur(this,26);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavCactividadparaplanid_Jsonclick, 0, "Attribute", "", "", "", "", edtavCactividadparaplanid_Visible, edtavCactividadparaplanid_Enabled, 0, "number", "1", 9, "chr", 1, "row", 9, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_Gx00N0.htm");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, divFyactividadplancantactfyfiltercontainer_Internalname, 1, 0, "px", 0, "px", divFyactividadplancantactfyfiltercontainer_Class, "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12", "left", "top", "", "", "div");
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblLblfyactividadplancantactfyfilter_Internalname, "FYActividad Plan Cant Act FY", "", "", lblLblfyactividadplancantactfyfilter_Jsonclick, "'"+""+"'"+",false,"+"'"+"e130u1_client"+"'", "", "WWAdvancedLabel WWFilterLabel", 7, "", 1, 1, 1, "HLP_Gx00N0.htm");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12 WWFiltersCell", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", " gx-attribute", "left", "top", "", "", "div");
            /* Attribute/Variable Label */
            GxWebStd.gx_label_element( context, edtavCfyactividadplancantactfy_Internalname, "FY", "col-sm-3 AttributeLabel", 0, true);
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 36,'',false,'" + sGXsfl_84_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavCfyactividadplancantactfy_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV8cFYActividadPlanCantActFY), 9, 0, ".", "")), ((edtavCfyactividadplancantactfy_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( (decimal)(AV8cFYActividadPlanCantActFY), "ZZZZZZZZ9")) : context.localUtil.Format( (decimal)(AV8cFYActividadPlanCantActFY), "ZZZZZZZZ9")), TempTags+" onchange=\""+"gx.num.valid_integer( this,',');"+";gx.evt.onchange(this, event)\" "+" onblur=\""+";gx.evt.onblur(this,36);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavCfyactividadplancantactfy_Jsonclick, 0, "Attribute", "", "", "", "", edtavCfyactividadplancantactfy_Visible, edtavCfyactividadplancantactfy_Enabled, 0, "number", "1", 9, "chr", 1, "row", 9, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_Gx00N0.htm");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, divMes1filtercontainer_Internalname, 1, 0, "px", 0, "px", divMes1filtercontainer_Class, "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12", "left", "top", "", "", "div");
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblLblmes1filter_Internalname, "mes1", "", "", lblLblmes1filter_Jsonclick, "'"+""+"'"+",false,"+"'"+"e140u1_client"+"'", "", "WWAdvancedLabel WWFilterLabel", 7, "", 1, 1, 1, "HLP_Gx00N0.htm");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12 WWFiltersCell", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", " gx-attribute", "left", "top", "", "", "div");
            /* Attribute/Variable Label */
            GxWebStd.gx_label_element( context, edtavCmes1_Internalname, "mes1", "col-sm-3 AttributeLabel", 0, true);
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 46,'',false,'" + sGXsfl_84_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavCmes1_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV9cmes1), 9, 0, ".", "")), ((edtavCmes1_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( (decimal)(AV9cmes1), "ZZZZZZZZ9")) : context.localUtil.Format( (decimal)(AV9cmes1), "ZZZZZZZZ9")), TempTags+" onchange=\""+"gx.num.valid_integer( this,',');"+";gx.evt.onchange(this, event)\" "+" onblur=\""+";gx.evt.onblur(this,46);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavCmes1_Jsonclick, 0, "Attribute", "", "", "", "", edtavCmes1_Visible, edtavCmes1_Enabled, 0, "number", "1", 9, "chr", 1, "row", 9, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_Gx00N0.htm");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, divMes2filtercontainer_Internalname, 1, 0, "px", 0, "px", divMes2filtercontainer_Class, "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12", "left", "top", "", "", "div");
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblLblmes2filter_Internalname, "mes2", "", "", lblLblmes2filter_Jsonclick, "'"+""+"'"+",false,"+"'"+"e150u1_client"+"'", "", "WWAdvancedLabel WWFilterLabel", 7, "", 1, 1, 1, "HLP_Gx00N0.htm");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12 WWFiltersCell", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", " gx-attribute", "left", "top", "", "", "div");
            /* Attribute/Variable Label */
            GxWebStd.gx_label_element( context, edtavCmes2_Internalname, "mes2", "col-sm-3 AttributeLabel", 0, true);
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 56,'',false,'" + sGXsfl_84_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavCmes2_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV10cmes2), 9, 0, ".", "")), ((edtavCmes2_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( (decimal)(AV10cmes2), "ZZZZZZZZ9")) : context.localUtil.Format( (decimal)(AV10cmes2), "ZZZZZZZZ9")), TempTags+" onchange=\""+"gx.num.valid_integer( this,',');"+";gx.evt.onchange(this, event)\" "+" onblur=\""+";gx.evt.onblur(this,56);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavCmes2_Jsonclick, 0, "Attribute", "", "", "", "", edtavCmes2_Visible, edtavCmes2_Enabled, 0, "number", "1", 9, "chr", 1, "row", 9, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_Gx00N0.htm");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, divMes3filtercontainer_Internalname, 1, 0, "px", 0, "px", divMes3filtercontainer_Class, "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12", "left", "top", "", "", "div");
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblLblmes3filter_Internalname, "mes3", "", "", lblLblmes3filter_Jsonclick, "'"+""+"'"+",false,"+"'"+"e160u1_client"+"'", "", "WWAdvancedLabel WWFilterLabel", 7, "", 1, 1, 1, "HLP_Gx00N0.htm");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12 WWFiltersCell", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", " gx-attribute", "left", "top", "", "", "div");
            /* Attribute/Variable Label */
            GxWebStd.gx_label_element( context, edtavCmes3_Internalname, "mes3", "col-sm-3 AttributeLabel", 0, true);
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 66,'',false,'" + sGXsfl_84_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavCmes3_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV11cmes3), 9, 0, ".", "")), ((edtavCmes3_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( (decimal)(AV11cmes3), "ZZZZZZZZ9")) : context.localUtil.Format( (decimal)(AV11cmes3), "ZZZZZZZZ9")), TempTags+" onchange=\""+"gx.num.valid_integer( this,',');"+";gx.evt.onchange(this, event)\" "+" onblur=\""+";gx.evt.onblur(this,66);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavCmes3_Jsonclick, 0, "Attribute", "", "", "", "", edtavCmes3_Visible, edtavCmes3_Enabled, 0, "number", "1", 9, "chr", 1, "row", 9, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_Gx00N0.htm");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, divMes4filtercontainer_Internalname, 1, 0, "px", 0, "px", divMes4filtercontainer_Class, "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12", "left", "top", "", "", "div");
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblLblmes4filter_Internalname, "mes4", "", "", lblLblmes4filter_Jsonclick, "'"+""+"'"+",false,"+"'"+"e170u1_client"+"'", "", "WWAdvancedLabel WWFilterLabel", 7, "", 1, 1, 1, "HLP_Gx00N0.htm");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12 WWFiltersCell", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", " gx-attribute", "left", "top", "", "", "div");
            /* Attribute/Variable Label */
            GxWebStd.gx_label_element( context, edtavCmes4_Internalname, "mes4", "col-sm-3 AttributeLabel", 0, true);
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 76,'',false,'" + sGXsfl_84_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavCmes4_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV12cmes4), 9, 0, ".", "")), ((edtavCmes4_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( (decimal)(AV12cmes4), "ZZZZZZZZ9")) : context.localUtil.Format( (decimal)(AV12cmes4), "ZZZZZZZZ9")), TempTags+" onchange=\""+"gx.num.valid_integer( this,',');"+";gx.evt.onchange(this, event)\" "+" onblur=\""+";gx.evt.onblur(this,76);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavCmes4_Jsonclick, 0, "Attribute", "", "", "", "", edtavCmes4_Visible, edtavCmes4_Enabled, 0, "number", "1", 9, "chr", 1, "row", 9, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_Gx00N0.htm");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12 col-sm-9 WWGridCell", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, divGridtable_Internalname, 1, 0, "px", 0, "px", "Table", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12 hidden-sm hidden-md hidden-lg ToggleCell", "left", "top", "", "", "div");
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 81,'',false,'',0)\"";
            ClassString = bttBtntoggle_Class;
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtntoggle_Internalname, "gx.evt.setGridEvt("+StringUtil.Str( (decimal)(84), 2, 0)+","+"null"+");", "|||", bttBtntoggle_Jsonclick, 7, "|||", "", StyleString, ClassString, 1, 1, "standard", "'"+""+"'"+",false,"+"'"+"e180u1_client"+"'", TempTags, "", 2, "HLP_Gx00N0.htm");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12", "left", "top", "", "", "div");
            /*  Grid Control  */
            Grid1Container.SetWrapped(nGXWrapped);
            if ( Grid1Container.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<div id=\""+"Grid1Container"+"DivS\" data-gxgridid=\"84\">") ;
               sStyleString = "";
               GxWebStd.gx_table_start( context, subGrid1_Internalname, subGrid1_Internalname, "", "PromptGrid", 0, "", "", 1, 2, sStyleString, "", "", 0);
               /* Subfile titles */
               context.WriteHtmlText( "<tr") ;
               context.WriteHtmlTextNl( ">") ;
               if ( subGrid1_Backcolorstyle == 0 )
               {
                  subGrid1_Titlebackstyle = 0;
                  if ( StringUtil.Len( subGrid1_Class) > 0 )
                  {
                     subGrid1_Linesclass = subGrid1_Class+"Title";
                  }
               }
               else
               {
                  subGrid1_Titlebackstyle = 1;
                  if ( subGrid1_Backcolorstyle == 1 )
                  {
                     subGrid1_Titlebackcolor = subGrid1_Allbackcolor;
                     if ( StringUtil.Len( subGrid1_Class) > 0 )
                     {
                        subGrid1_Linesclass = subGrid1_Class+"UniformTitle";
                     }
                  }
                  else
                  {
                     if ( StringUtil.Len( subGrid1_Class) > 0 )
                     {
                        subGrid1_Linesclass = subGrid1_Class+"Title";
                     }
                  }
               }
               context.WriteHtmlText( "<th align=\""+""+"\" "+" nowrap=\"nowrap\" "+" class=\""+"SelectionAttribute"+"\" "+" style=\""+""+""+"\" "+">") ;
               context.SendWebValue( "") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+"Attribute"+"\" "+" style=\""+""+""+"\" "+">") ;
               context.SendWebValue( "id") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+"Attribute"+"\" "+" style=\""+""+""+"\" "+">") ;
               context.SendWebValue( "Actividad Para Planid") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+"DescriptionAttribute"+"\" "+" style=\""+""+""+"\" "+">") ;
               context.SendWebValue( "FY") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+"Attribute"+"\" "+" style=\""+""+""+"\" "+">") ;
               context.SendWebValue( "mes1") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+"Attribute"+"\" "+" style=\""+""+""+"\" "+">") ;
               context.SendWebValue( "mes2") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+"Attribute"+"\" "+" style=\""+""+""+"\" "+">") ;
               context.SendWebValue( "mes3") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+"Attribute"+"\" "+" style=\""+""+""+"\" "+">") ;
               context.SendWebValue( "mes4") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+"Attribute"+"\" "+" style=\""+""+""+"\" "+">") ;
               context.SendWebValue( "mes5") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlTextNl( "</tr>") ;
               Grid1Container.AddObjectProperty("GridName", "Grid1");
            }
            else
            {
               if ( isAjaxCallMode( ) )
               {
                  Grid1Container = new GXWebGrid( context);
               }
               else
               {
                  Grid1Container.Clear();
               }
               Grid1Container.SetWrapped(nGXWrapped);
               Grid1Container.AddObjectProperty("GridName", "Grid1");
               Grid1Container.AddObjectProperty("Header", subGrid1_Header);
               Grid1Container.AddObjectProperty("Class", "PromptGrid");
               Grid1Container.AddObjectProperty("Cellpadding", StringUtil.LTrim( StringUtil.NToC( (decimal)(1), 4, 0, ".", "")));
               Grid1Container.AddObjectProperty("Cellspacing", StringUtil.LTrim( StringUtil.NToC( (decimal)(2), 4, 0, ".", "")));
               Grid1Container.AddObjectProperty("Backcolorstyle", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid1_Backcolorstyle), 1, 0, ".", "")));
               Grid1Container.AddObjectProperty("CmpContext", "");
               Grid1Container.AddObjectProperty("InMasterPage", "false");
               Grid1Column = GXWebColumn.GetNew(isAjaxCallMode( ));
               Grid1Column.AddObjectProperty("Value", context.convertURL( AV5LinkSelection));
               Grid1Column.AddObjectProperty("Link", StringUtil.RTrim( edtavLinkselection_Link));
               Grid1Container.AddColumnProperties(Grid1Column);
               Grid1Column = GXWebColumn.GetNew(isAjaxCallMode( ));
               Grid1Column.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(A24FYActividadPlanCantActid), 9, 0, ".", "")));
               Grid1Container.AddColumnProperties(Grid1Column);
               Grid1Column = GXWebColumn.GetNew(isAjaxCallMode( ));
               Grid1Column.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(A23ActividadParaPlanid), 9, 0, ".", "")));
               Grid1Container.AddColumnProperties(Grid1Column);
               Grid1Column = GXWebColumn.GetNew(isAjaxCallMode( ));
               Grid1Column.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(A104FYActividadPlanCantActFY), 9, 0, ".", "")));
               Grid1Column.AddObjectProperty("Link", StringUtil.RTrim( edtFYActividadPlanCantActFY_Link));
               Grid1Container.AddColumnProperties(Grid1Column);
               Grid1Column = GXWebColumn.GetNew(isAjaxCallMode( ));
               Grid1Column.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(A105mes1), 9, 0, ".", "")));
               Grid1Container.AddColumnProperties(Grid1Column);
               Grid1Column = GXWebColumn.GetNew(isAjaxCallMode( ));
               Grid1Column.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(A106mes2), 9, 0, ".", "")));
               Grid1Container.AddColumnProperties(Grid1Column);
               Grid1Column = GXWebColumn.GetNew(isAjaxCallMode( ));
               Grid1Column.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(A107mes3), 9, 0, ".", "")));
               Grid1Container.AddColumnProperties(Grid1Column);
               Grid1Column = GXWebColumn.GetNew(isAjaxCallMode( ));
               Grid1Column.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(A108mes4), 9, 0, ".", "")));
               Grid1Container.AddColumnProperties(Grid1Column);
               Grid1Column = GXWebColumn.GetNew(isAjaxCallMode( ));
               Grid1Column.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(A109mes5), 9, 0, ".", "")));
               Grid1Container.AddColumnProperties(Grid1Column);
               Grid1Container.AddObjectProperty("Selectedindex", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid1_Selectedindex), 4, 0, ".", "")));
               Grid1Container.AddObjectProperty("Allowselection", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid1_Allowselection), 1, 0, ".", "")));
               Grid1Container.AddObjectProperty("Selectioncolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid1_Selectioncolor), 9, 0, ".", "")));
               Grid1Container.AddObjectProperty("Allowhover", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid1_Allowhovering), 1, 0, ".", "")));
               Grid1Container.AddObjectProperty("Hovercolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid1_Hoveringcolor), 9, 0, ".", "")));
               Grid1Container.AddObjectProperty("Allowcollapsing", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid1_Allowcollapsing), 1, 0, ".", "")));
               Grid1Container.AddObjectProperty("Collapsed", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid1_Collapsed), 1, 0, ".", "")));
            }
         }
         if ( wbEnd == 84 )
         {
            wbEnd = 0;
            nRC_GXsfl_84 = (short)(nGXsfl_84_idx-1);
            if ( Grid1Container.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "</table>") ;
               context.WriteHtmlText( "</div>") ;
            }
            else
            {
               Grid1Container.AddObjectProperty("GRID1_nEOF", GRID1_nEOF);
               Grid1Container.AddObjectProperty("GRID1_nFirstRecordOnPage", GRID1_nFirstRecordOnPage);
               sStyleString = "";
               context.WriteHtmlText( "<div id=\""+"Grid1Container"+"Div\" "+sStyleString+">"+"</div>") ;
               context.httpAjaxContext.ajax_rsp_assign_grid("_"+"Grid1", Grid1Container);
               if ( ! context.isAjaxRequest( ) && ! context.isSpaRequest( ) )
               {
                  GxWebStd.gx_hidden_field( context, "Grid1ContainerData", Grid1Container.ToJavascriptSource());
               }
               if ( context.isAjaxRequest( ) || context.isSpaRequest( ) )
               {
                  GxWebStd.gx_hidden_field( context, "Grid1ContainerData"+"V", Grid1Container.GridValuesHidden());
               }
               else
               {
                  context.WriteHtmlText( "<input type=\"hidden\" "+"name=\""+"Grid1ContainerData"+"V"+"\" value='"+Grid1Container.GridValuesHidden()+"'/>") ;
               }
            }
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12", "left", "top", "", "", "div");
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 96,'',false,'',0)\"";
            ClassString = "BtnCancel";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtn_cancel_Internalname, "gx.evt.setGridEvt("+StringUtil.Str( (decimal)(84), 2, 0)+","+"null"+");", "Cancel", bttBtn_cancel_Jsonclick, 1, "Cancel", "", StyleString, ClassString, 1, 1, "standard", "'"+""+"'"+",false,"+"'"+"ECANCEL."+"'", TempTags, "", context.GetButtonType( ), "HLP_Gx00N0.htm");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
         }
         if ( wbEnd == 84 )
         {
            wbEnd = 0;
            if ( isFullAjaxMode( ) )
            {
               if ( Grid1Container.GetWrapped() == 1 )
               {
                  context.WriteHtmlText( "</table>") ;
                  context.WriteHtmlText( "</div>") ;
               }
               else
               {
                  Grid1Container.AddObjectProperty("GRID1_nEOF", GRID1_nEOF);
                  Grid1Container.AddObjectProperty("GRID1_nFirstRecordOnPage", GRID1_nFirstRecordOnPage);
                  sStyleString = "";
                  context.WriteHtmlText( "<div id=\""+"Grid1Container"+"Div\" "+sStyleString+">"+"</div>") ;
                  context.httpAjaxContext.ajax_rsp_assign_grid("_"+"Grid1", Grid1Container);
                  if ( ! context.isAjaxRequest( ) && ! context.isSpaRequest( ) )
                  {
                     GxWebStd.gx_hidden_field( context, "Grid1ContainerData", Grid1Container.ToJavascriptSource());
                  }
                  if ( context.isAjaxRequest( ) || context.isSpaRequest( ) )
                  {
                     GxWebStd.gx_hidden_field( context, "Grid1ContainerData"+"V", Grid1Container.GridValuesHidden());
                  }
                  else
                  {
                     context.WriteHtmlText( "<input type=\"hidden\" "+"name=\""+"Grid1ContainerData"+"V"+"\" value='"+Grid1Container.GridValuesHidden()+"'/>") ;
                  }
               }
            }
         }
         wbLoad = true;
      }

      protected void START0U2( )
      {
         wbLoad = false;
         wbEnd = 0;
         wbStart = 0;
         if ( ! context.isSpaRequest( ) )
         {
            Form.Meta.addItem("generator", "GeneXus C# 16_0_0-127771", 0) ;
            Form.Meta.addItem("description", "Selection List FYActividad Plan Cant Act", 0) ;
         }
         context.wjLoc = "";
         context.nUserReturn = 0;
         context.wbHandled = 0;
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
         }
         wbErr = false;
         STRUP0U0( ) ;
      }

      protected void WS0U2( )
      {
         START0U2( ) ;
         EVT0U2( ) ;
      }

      protected void EVT0U2( )
      {
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            if ( ! context.WillRedirect( ) && ( context.nUserReturn != 1 ) && ! wbErr )
            {
               /* Read Web Panel buttons. */
               sEvt = cgiGet( "_EventName");
               EvtGridId = cgiGet( "_EventGridId");
               EvtRowId = cgiGet( "_EventRowId");
               if ( StringUtil.Len( sEvt) > 0 )
               {
                  sEvtType = StringUtil.Left( sEvt, 1);
                  sEvt = StringUtil.Right( sEvt, (short)(StringUtil.Len( sEvt)-1));
                  if ( StringUtil.StrCmp(sEvtType, "M") != 0 )
                  {
                     if ( StringUtil.StrCmp(sEvtType, "E") == 0 )
                     {
                        sEvtType = StringUtil.Right( sEvt, 1);
                        if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                        {
                           sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                           if ( StringUtil.StrCmp(sEvt, "RFR") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* No code required for Cancel button. It is implemented as the Reset button. */
                           }
                           else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                           }
                           else if ( StringUtil.StrCmp(sEvt, "GRID1PAGING") == 0 )
                           {
                              context.wbHandled = 1;
                              sEvt = cgiGet( "GRID1PAGING");
                              if ( StringUtil.StrCmp(sEvt, "FIRST") == 0 )
                              {
                                 subgrid1_firstpage( ) ;
                              }
                              else if ( StringUtil.StrCmp(sEvt, "PREV") == 0 )
                              {
                                 subgrid1_previouspage( ) ;
                              }
                              else if ( StringUtil.StrCmp(sEvt, "NEXT") == 0 )
                              {
                                 subgrid1_nextpage( ) ;
                              }
                              else if ( StringUtil.StrCmp(sEvt, "LAST") == 0 )
                              {
                                 subgrid1_lastpage( ) ;
                              }
                              dynload_actions( ) ;
                           }
                        }
                        else
                        {
                           sEvtType = StringUtil.Right( sEvt, 4);
                           sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-4));
                           if ( ( StringUtil.StrCmp(StringUtil.Left( sEvt, 5), "START") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 4), "LOAD") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 5), "ENTER") == 0 ) )
                           {
                              nGXsfl_84_idx = (short)(NumberUtil.Val( sEvtType, "."));
                              sGXsfl_84_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_84_idx), 4, 0)), 4, "0");
                              SubsflControlProps_842( ) ;
                              AV5LinkSelection = cgiGet( edtavLinkselection_Internalname);
                              context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavLinkselection_Internalname, "Bitmap", (String.IsNullOrEmpty(StringUtil.RTrim( AV5LinkSelection)) ? AV17Linkselection_GXI : context.convertURL( context.PathToRelativeUrl( AV5LinkSelection))), !bGXsfl_84_Refreshing);
                              context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavLinkselection_Internalname, "SrcSet", context.GetImageSrcSet( AV5LinkSelection), true);
                              A24FYActividadPlanCantActid = (int)(context.localUtil.CToN( cgiGet( edtFYActividadPlanCantActid_Internalname), ".", ","));
                              A23ActividadParaPlanid = (int)(context.localUtil.CToN( cgiGet( edtActividadParaPlanid_Internalname), ".", ","));
                              A104FYActividadPlanCantActFY = (int)(context.localUtil.CToN( cgiGet( edtFYActividadPlanCantActFY_Internalname), ".", ","));
                              A105mes1 = (int)(context.localUtil.CToN( cgiGet( edtmes1_Internalname), ".", ","));
                              n105mes1 = false;
                              A106mes2 = (int)(context.localUtil.CToN( cgiGet( edtmes2_Internalname), ".", ","));
                              n106mes2 = false;
                              A107mes3 = (int)(context.localUtil.CToN( cgiGet( edtmes3_Internalname), ".", ","));
                              n107mes3 = false;
                              A108mes4 = (int)(context.localUtil.CToN( cgiGet( edtmes4_Internalname), ".", ","));
                              n108mes4 = false;
                              A109mes5 = (int)(context.localUtil.CToN( cgiGet( edtmes5_Internalname), ".", ","));
                              n109mes5 = false;
                              sEvtType = StringUtil.Right( sEvt, 1);
                              if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                              {
                                 sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                                 if ( StringUtil.StrCmp(sEvt, "START") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                    /* Execute user event: Start */
                                    E190U2 ();
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "LOAD") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                    /* Execute user event: Load */
                                    E200U2 ();
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "ENTER") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    if ( ! wbErr )
                                    {
                                       Rfr0gs = false;
                                       /* Set Refresh If Cfyactividadplancantactid Changed */
                                       if ( ( context.localUtil.CToN( cgiGet( "GXH_vCFYACTIVIDADPLANCANTACTID"), ".", ",") != Convert.ToDecimal( AV6cFYActividadPlanCantActid )) )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Cactividadparaplanid Changed */
                                       if ( ( context.localUtil.CToN( cgiGet( "GXH_vCACTIVIDADPARAPLANID"), ".", ",") != Convert.ToDecimal( AV7cActividadParaPlanid )) )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Cfyactividadplancantactfy Changed */
                                       if ( ( context.localUtil.CToN( cgiGet( "GXH_vCFYACTIVIDADPLANCANTACTFY"), ".", ",") != Convert.ToDecimal( AV8cFYActividadPlanCantActFY )) )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Cmes1 Changed */
                                       if ( ( context.localUtil.CToN( cgiGet( "GXH_vCMES1"), ".", ",") != Convert.ToDecimal( AV9cmes1 )) )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Cmes2 Changed */
                                       if ( ( context.localUtil.CToN( cgiGet( "GXH_vCMES2"), ".", ",") != Convert.ToDecimal( AV10cmes2 )) )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Cmes3 Changed */
                                       if ( ( context.localUtil.CToN( cgiGet( "GXH_vCMES3"), ".", ",") != Convert.ToDecimal( AV11cmes3 )) )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Cmes4 Changed */
                                       if ( ( context.localUtil.CToN( cgiGet( "GXH_vCMES4"), ".", ",") != Convert.ToDecimal( AV12cmes4 )) )
                                       {
                                          Rfr0gs = true;
                                       }
                                       if ( ! Rfr0gs )
                                       {
                                          /* Execute user event: Enter */
                                          E210U2 ();
                                       }
                                       dynload_actions( ) ;
                                    }
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                 }
                              }
                              else
                              {
                              }
                           }
                        }
                     }
                     context.wbHandled = 1;
                  }
               }
            }
         }
      }

      protected void WE0U2( )
      {
         if ( ! GxWebStd.gx_redirect( context) )
         {
            Rfr0gs = true;
            Refresh( ) ;
            if ( ! GxWebStd.gx_redirect( context) )
            {
               if ( nGXWrapped == 1 )
               {
                  RenderHtmlCloseForm( ) ;
               }
            }
         }
      }

      protected void PA0U2( )
      {
         if ( nDonePA == 0 )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.GetCookie( "GX_SESSION_ID"))) )
            {
               gxcookieaux = context.SetCookie( "GX_SESSION_ID", Encrypt64( Crypto.GetEncryptionKey( ), Crypto.GetServerKey( )), "", (DateTime)(DateTime.MinValue), "", 0);
            }
            GXKey = Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            toggleJsOutput = isJsOutputEnabled( );
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
            init_web_controls( ) ;
            if ( toggleJsOutput )
            {
               if ( context.isSpaRequest( ) )
               {
                  enableJsOutput();
               }
            }
            nDonePA = 1;
         }
      }

      protected void dynload_actions( )
      {
         /* End function dynload_actions */
      }

      protected void gxnrGrid1_newrow( )
      {
         GxWebStd.set_html_headers( context, 0, "", "");
         SubsflControlProps_842( ) ;
         while ( nGXsfl_84_idx <= nRC_GXsfl_84 )
         {
            sendrow_842( ) ;
            nGXsfl_84_idx = (short)(((subGrid1_Islastpage==1)&&(nGXsfl_84_idx+1>subGrid1_Recordsperpage( )) ? 1 : nGXsfl_84_idx+1));
            sGXsfl_84_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_84_idx), 4, 0)), 4, "0");
            SubsflControlProps_842( ) ;
         }
         context.GX_webresponse.AddString(context.httpAjaxContext.getJSONContainerResponse( Grid1Container));
         /* End function gxnrGrid1_newrow */
      }

      protected void gxgrGrid1_refresh( int subGrid1_Rows ,
                                        int AV6cFYActividadPlanCantActid ,
                                        int AV7cActividadParaPlanid ,
                                        int AV8cFYActividadPlanCantActFY ,
                                        int AV9cmes1 ,
                                        int AV10cmes2 ,
                                        int AV11cmes3 ,
                                        int AV12cmes4 )
      {
         initialize_formulas( ) ;
         GxWebStd.set_html_headers( context, 0, "", "");
         GxWebStd.gx_hidden_field( context, "GRID1_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid1_Rows), 6, 0, ".", "")));
         GRID1_nCurrentRecord = 0;
         RF0U2( ) ;
         /* End function gxgrGrid1_refresh */
      }

      protected void send_integrity_hashes( )
      {
         GxWebStd.gx_hidden_field( context, "gxhash_FYACTIVIDADPLANCANTACTID", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(A24FYActividadPlanCantActid), "ZZZZZZZZ9"), context));
         GxWebStd.gx_hidden_field( context, "FYACTIVIDADPLANCANTACTID", StringUtil.LTrim( StringUtil.NToC( (decimal)(A24FYActividadPlanCantActid), 9, 0, ".", "")));
      }

      protected void clear_multi_value_controls( )
      {
         if ( context.isAjaxRequest( ) )
         {
            dynload_actions( ) ;
         }
      }

      protected void fix_multi_value_controls( )
      {
      }

      public void Refresh( )
      {
         send_integrity_hashes( ) ;
         RF0U2( ) ;
         if ( isFullAjaxMode( ) )
         {
            send_integrity_footer_hashes( ) ;
         }
      }

      protected void initialize_formulas( )
      {
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      protected void RF0U2( )
      {
         initialize_formulas( ) ;
         clear_multi_value_controls( ) ;
         if ( isAjaxCallMode( ) )
         {
            Grid1Container.ClearRows();
         }
         wbStart = 84;
         nGXsfl_84_idx = 1;
         sGXsfl_84_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_84_idx), 4, 0)), 4, "0");
         SubsflControlProps_842( ) ;
         bGXsfl_84_Refreshing = true;
         Grid1Container.AddObjectProperty("GridName", "Grid1");
         Grid1Container.AddObjectProperty("CmpContext", "");
         Grid1Container.AddObjectProperty("InMasterPage", "false");
         Grid1Container.AddObjectProperty("Class", "PromptGrid");
         Grid1Container.AddObjectProperty("Cellpadding", StringUtil.LTrim( StringUtil.NToC( (decimal)(1), 4, 0, ".", "")));
         Grid1Container.AddObjectProperty("Cellspacing", StringUtil.LTrim( StringUtil.NToC( (decimal)(2), 4, 0, ".", "")));
         Grid1Container.AddObjectProperty("Backcolorstyle", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid1_Backcolorstyle), 1, 0, ".", "")));
         Grid1Container.PageSize = subGrid1_Recordsperpage( );
         gxdyncontrolsrefreshing = true;
         fix_multi_value_controls( ) ;
         gxdyncontrolsrefreshing = false;
         if ( ! context.WillRedirect( ) && ( context.nUserReturn != 1 ) )
         {
            SubsflControlProps_842( ) ;
            GXPagingFrom2 = (int)(((10==0) ? 0 : GRID1_nFirstRecordOnPage));
            GXPagingTo2 = ((10==0) ? 10000 : subGrid1_Recordsperpage( )+1);
            pr_datastore1.dynParam(0, new Object[]{ new Object[]{
                                                 AV7cActividadParaPlanid ,
                                                 AV8cFYActividadPlanCantActFY ,
                                                 AV9cmes1 ,
                                                 AV10cmes2 ,
                                                 AV11cmes3 ,
                                                 AV12cmes4 ,
                                                 A23ActividadParaPlanid ,
                                                 A104FYActividadPlanCantActFY ,
                                                 A105mes1 ,
                                                 A106mes2 ,
                                                 A107mes3 ,
                                                 A108mes4 ,
                                                 AV6cFYActividadPlanCantActid } ,
                                                 new int[]{
                                                 TypeConstants.INT, TypeConstants.INT, TypeConstants.INT, TypeConstants.INT, TypeConstants.INT, TypeConstants.INT, TypeConstants.INT, TypeConstants.INT, TypeConstants.INT, TypeConstants.BOOLEAN,
                                                 TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.INT
                                                 }
            } ) ;
            /* Using cursor H000U2 */
            pr_datastore1.execute(0, new Object[] {AV6cFYActividadPlanCantActid, AV7cActividadParaPlanid, AV8cFYActividadPlanCantActFY, AV9cmes1, AV10cmes2, AV11cmes3, AV12cmes4, GXPagingFrom2, GXPagingTo2, GXPagingTo2});
            nGXsfl_84_idx = 1;
            sGXsfl_84_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_84_idx), 4, 0)), 4, "0");
            SubsflControlProps_842( ) ;
            while ( ( (pr_datastore1.getStatus(0) != 101) ) && ( ( ( 10 == 0 ) || ( GRID1_nCurrentRecord < subGrid1_Recordsperpage( ) ) ) ) )
            {
               A109mes5 = H000U2_A109mes5[0];
               n109mes5 = H000U2_n109mes5[0];
               A108mes4 = H000U2_A108mes4[0];
               n108mes4 = H000U2_n108mes4[0];
               A107mes3 = H000U2_A107mes3[0];
               n107mes3 = H000U2_n107mes3[0];
               A106mes2 = H000U2_A106mes2[0];
               n106mes2 = H000U2_n106mes2[0];
               A105mes1 = H000U2_A105mes1[0];
               n105mes1 = H000U2_n105mes1[0];
               A104FYActividadPlanCantActFY = H000U2_A104FYActividadPlanCantActFY[0];
               A23ActividadParaPlanid = H000U2_A23ActividadParaPlanid[0];
               A24FYActividadPlanCantActid = H000U2_A24FYActividadPlanCantActid[0];
               /* Execute user event: Load */
               E200U2 ();
               pr_datastore1.readNext(0);
            }
            GRID1_nEOF = (short)(((pr_datastore1.getStatus(0) == 101) ? 1 : 0));
            GxWebStd.gx_hidden_field( context, "GRID1_nEOF", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID1_nEOF), 1, 0, ".", "")));
            pr_datastore1.close(0);
            wbEnd = 84;
            WB0U0( ) ;
         }
         bGXsfl_84_Refreshing = true;
      }

      protected void send_integrity_lvl_hashes0U2( )
      {
         GxWebStd.gx_hidden_field( context, "gxhash_FYACTIVIDADPLANCANTACTID"+"_"+sGXsfl_84_idx, GetSecureSignedToken( sGXsfl_84_idx, context.localUtil.Format( (decimal)(A24FYActividadPlanCantActid), "ZZZZZZZZ9"), context));
      }

      protected int subGrid1_Pagecount( )
      {
         GRID1_nRecordCount = subGrid1_Recordcount( );
         if ( ((int)((GRID1_nRecordCount) % (subGrid1_Recordsperpage( )))) == 0 )
         {
            return (int)(NumberUtil.Int( (long)(GRID1_nRecordCount/ (decimal)(subGrid1_Recordsperpage( ))))) ;
         }
         return (int)(NumberUtil.Int( (long)(GRID1_nRecordCount/ (decimal)(subGrid1_Recordsperpage( ))))+1) ;
      }

      protected int subGrid1_Recordcount( )
      {
         pr_datastore1.dynParam(1, new Object[]{ new Object[]{
                                              AV7cActividadParaPlanid ,
                                              AV8cFYActividadPlanCantActFY ,
                                              AV9cmes1 ,
                                              AV10cmes2 ,
                                              AV11cmes3 ,
                                              AV12cmes4 ,
                                              A23ActividadParaPlanid ,
                                              A104FYActividadPlanCantActFY ,
                                              A105mes1 ,
                                              A106mes2 ,
                                              A107mes3 ,
                                              A108mes4 ,
                                              AV6cFYActividadPlanCantActid } ,
                                              new int[]{
                                              TypeConstants.INT, TypeConstants.INT, TypeConstants.INT, TypeConstants.INT, TypeConstants.INT, TypeConstants.INT, TypeConstants.INT, TypeConstants.INT, TypeConstants.INT, TypeConstants.BOOLEAN,
                                              TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.INT
                                              }
         } ) ;
         /* Using cursor H000U3 */
         pr_datastore1.execute(1, new Object[] {AV6cFYActividadPlanCantActid, AV7cActividadParaPlanid, AV8cFYActividadPlanCantActFY, AV9cmes1, AV10cmes2, AV11cmes3, AV12cmes4});
         GRID1_nRecordCount = H000U3_AGRID1_nRecordCount[0];
         pr_datastore1.close(1);
         return (int)(GRID1_nRecordCount) ;
      }

      protected int subGrid1_Recordsperpage( )
      {
         return (int)(10*1) ;
      }

      protected int subGrid1_Currentpage( )
      {
         return (int)(NumberUtil.Int( (long)(GRID1_nFirstRecordOnPage/ (decimal)(subGrid1_Recordsperpage( ))))+1) ;
      }

      protected short subgrid1_firstpage( )
      {
         GRID1_nFirstRecordOnPage = 0;
         GxWebStd.gx_hidden_field( context, "GRID1_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID1_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid1_refresh( subGrid1_Rows, AV6cFYActividadPlanCantActid, AV7cActividadParaPlanid, AV8cFYActividadPlanCantActFY, AV9cmes1, AV10cmes2, AV11cmes3, AV12cmes4) ;
         }
         send_integrity_footer_hashes( ) ;
         return 0 ;
      }

      protected short subgrid1_nextpage( )
      {
         GRID1_nRecordCount = subGrid1_Recordcount( );
         if ( ( GRID1_nRecordCount >= subGrid1_Recordsperpage( ) ) && ( GRID1_nEOF == 0 ) )
         {
            GRID1_nFirstRecordOnPage = (long)(GRID1_nFirstRecordOnPage+subGrid1_Recordsperpage( ));
         }
         else
         {
            return 2 ;
         }
         GxWebStd.gx_hidden_field( context, "GRID1_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID1_nFirstRecordOnPage), 15, 0, ".", "")));
         Grid1Container.AddObjectProperty("GRID1_nFirstRecordOnPage", GRID1_nFirstRecordOnPage);
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid1_refresh( subGrid1_Rows, AV6cFYActividadPlanCantActid, AV7cActividadParaPlanid, AV8cFYActividadPlanCantActFY, AV9cmes1, AV10cmes2, AV11cmes3, AV12cmes4) ;
         }
         send_integrity_footer_hashes( ) ;
         return (short)(((GRID1_nEOF==0) ? 0 : 2)) ;
      }

      protected short subgrid1_previouspage( )
      {
         if ( GRID1_nFirstRecordOnPage >= subGrid1_Recordsperpage( ) )
         {
            GRID1_nFirstRecordOnPage = (long)(GRID1_nFirstRecordOnPage-subGrid1_Recordsperpage( ));
         }
         else
         {
            return 2 ;
         }
         GxWebStd.gx_hidden_field( context, "GRID1_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID1_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid1_refresh( subGrid1_Rows, AV6cFYActividadPlanCantActid, AV7cActividadParaPlanid, AV8cFYActividadPlanCantActFY, AV9cmes1, AV10cmes2, AV11cmes3, AV12cmes4) ;
         }
         send_integrity_footer_hashes( ) ;
         return 0 ;
      }

      protected short subgrid1_lastpage( )
      {
         GRID1_nRecordCount = subGrid1_Recordcount( );
         if ( GRID1_nRecordCount > subGrid1_Recordsperpage( ) )
         {
            if ( ((int)((GRID1_nRecordCount) % (subGrid1_Recordsperpage( )))) == 0 )
            {
               GRID1_nFirstRecordOnPage = (long)(GRID1_nRecordCount-subGrid1_Recordsperpage( ));
            }
            else
            {
               GRID1_nFirstRecordOnPage = (long)(GRID1_nRecordCount-((int)((GRID1_nRecordCount) % (subGrid1_Recordsperpage( )))));
            }
         }
         else
         {
            GRID1_nFirstRecordOnPage = 0;
         }
         GxWebStd.gx_hidden_field( context, "GRID1_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID1_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid1_refresh( subGrid1_Rows, AV6cFYActividadPlanCantActid, AV7cActividadParaPlanid, AV8cFYActividadPlanCantActFY, AV9cmes1, AV10cmes2, AV11cmes3, AV12cmes4) ;
         }
         send_integrity_footer_hashes( ) ;
         return 0 ;
      }

      protected int subgrid1_gotopage( int nPageNo )
      {
         if ( nPageNo > 0 )
         {
            GRID1_nFirstRecordOnPage = (long)(subGrid1_Recordsperpage( )*(nPageNo-1));
         }
         else
         {
            GRID1_nFirstRecordOnPage = 0;
         }
         GxWebStd.gx_hidden_field( context, "GRID1_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID1_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid1_refresh( subGrid1_Rows, AV6cFYActividadPlanCantActid, AV7cActividadParaPlanid, AV8cFYActividadPlanCantActFY, AV9cmes1, AV10cmes2, AV11cmes3, AV12cmes4) ;
         }
         send_integrity_footer_hashes( ) ;
         return (int)(0) ;
      }

      protected void STRUP0U0( )
      {
         /* Before Start, stand alone formulas. */
         context.Gx_err = 0;
         /* Execute Start event if defined. */
         context.wbGlbDoneStart = 0;
         /* Execute user event: Start */
         E190U2 ();
         context.wbGlbDoneStart = 1;
         /* After Start, stand alone formulas. */
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            /* Read saved SDTs. */
            /* Read variables values. */
            if ( ( ( context.localUtil.CToN( cgiGet( edtavCfyactividadplancantactid_Internalname), ".", ",") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavCfyactividadplancantactid_Internalname), ".", ",") > Convert.ToDecimal( 999999999 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vCFYACTIVIDADPLANCANTACTID");
               GX_FocusControl = edtavCfyactividadplancantactid_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV6cFYActividadPlanCantActid = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV6cFYActividadPlanCantActid", StringUtil.LTrim( StringUtil.Str( (decimal)(AV6cFYActividadPlanCantActid), 9, 0)));
            }
            else
            {
               AV6cFYActividadPlanCantActid = (int)(context.localUtil.CToN( cgiGet( edtavCfyactividadplancantactid_Internalname), ".", ","));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV6cFYActividadPlanCantActid", StringUtil.LTrim( StringUtil.Str( (decimal)(AV6cFYActividadPlanCantActid), 9, 0)));
            }
            if ( ( ( context.localUtil.CToN( cgiGet( edtavCactividadparaplanid_Internalname), ".", ",") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavCactividadparaplanid_Internalname), ".", ",") > Convert.ToDecimal( 999999999 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vCACTIVIDADPARAPLANID");
               GX_FocusControl = edtavCactividadparaplanid_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV7cActividadParaPlanid = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV7cActividadParaPlanid", StringUtil.LTrim( StringUtil.Str( (decimal)(AV7cActividadParaPlanid), 9, 0)));
            }
            else
            {
               AV7cActividadParaPlanid = (int)(context.localUtil.CToN( cgiGet( edtavCactividadparaplanid_Internalname), ".", ","));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV7cActividadParaPlanid", StringUtil.LTrim( StringUtil.Str( (decimal)(AV7cActividadParaPlanid), 9, 0)));
            }
            if ( ( ( context.localUtil.CToN( cgiGet( edtavCfyactividadplancantactfy_Internalname), ".", ",") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavCfyactividadplancantactfy_Internalname), ".", ",") > Convert.ToDecimal( 999999999 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vCFYACTIVIDADPLANCANTACTFY");
               GX_FocusControl = edtavCfyactividadplancantactfy_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV8cFYActividadPlanCantActFY = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV8cFYActividadPlanCantActFY", StringUtil.LTrim( StringUtil.Str( (decimal)(AV8cFYActividadPlanCantActFY), 9, 0)));
            }
            else
            {
               AV8cFYActividadPlanCantActFY = (int)(context.localUtil.CToN( cgiGet( edtavCfyactividadplancantactfy_Internalname), ".", ","));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV8cFYActividadPlanCantActFY", StringUtil.LTrim( StringUtil.Str( (decimal)(AV8cFYActividadPlanCantActFY), 9, 0)));
            }
            if ( ( ( context.localUtil.CToN( cgiGet( edtavCmes1_Internalname), ".", ",") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavCmes1_Internalname), ".", ",") > Convert.ToDecimal( 999999999 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vCMES1");
               GX_FocusControl = edtavCmes1_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV9cmes1 = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV9cmes1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV9cmes1), 9, 0)));
            }
            else
            {
               AV9cmes1 = (int)(context.localUtil.CToN( cgiGet( edtavCmes1_Internalname), ".", ","));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV9cmes1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV9cmes1), 9, 0)));
            }
            if ( ( ( context.localUtil.CToN( cgiGet( edtavCmes2_Internalname), ".", ",") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavCmes2_Internalname), ".", ",") > Convert.ToDecimal( 999999999 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vCMES2");
               GX_FocusControl = edtavCmes2_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV10cmes2 = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV10cmes2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV10cmes2), 9, 0)));
            }
            else
            {
               AV10cmes2 = (int)(context.localUtil.CToN( cgiGet( edtavCmes2_Internalname), ".", ","));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV10cmes2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV10cmes2), 9, 0)));
            }
            if ( ( ( context.localUtil.CToN( cgiGet( edtavCmes3_Internalname), ".", ",") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavCmes3_Internalname), ".", ",") > Convert.ToDecimal( 999999999 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vCMES3");
               GX_FocusControl = edtavCmes3_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV11cmes3 = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV11cmes3", StringUtil.LTrim( StringUtil.Str( (decimal)(AV11cmes3), 9, 0)));
            }
            else
            {
               AV11cmes3 = (int)(context.localUtil.CToN( cgiGet( edtavCmes3_Internalname), ".", ","));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV11cmes3", StringUtil.LTrim( StringUtil.Str( (decimal)(AV11cmes3), 9, 0)));
            }
            if ( ( ( context.localUtil.CToN( cgiGet( edtavCmes4_Internalname), ".", ",") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavCmes4_Internalname), ".", ",") > Convert.ToDecimal( 999999999 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vCMES4");
               GX_FocusControl = edtavCmes4_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV12cmes4 = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV12cmes4", StringUtil.LTrim( StringUtil.Str( (decimal)(AV12cmes4), 9, 0)));
            }
            else
            {
               AV12cmes4 = (int)(context.localUtil.CToN( cgiGet( edtavCmes4_Internalname), ".", ","));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV12cmes4", StringUtil.LTrim( StringUtil.Str( (decimal)(AV12cmes4), 9, 0)));
            }
            /* Read saved values. */
            nRC_GXsfl_84 = (short)(context.localUtil.CToN( cgiGet( "nRC_GXsfl_84"), ".", ","));
            GRID1_nFirstRecordOnPage = (long)(context.localUtil.CToN( cgiGet( "GRID1_nFirstRecordOnPage"), ".", ","));
            GRID1_nEOF = (short)(context.localUtil.CToN( cgiGet( "GRID1_nEOF"), ".", ","));
            /* Read subfile selected row values. */
            /* Read hidden variables. */
            GXKey = Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            /* Check if conditions changed and reset current page numbers */
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vCFYACTIVIDADPLANCANTACTID"), ".", ",") != Convert.ToDecimal( AV6cFYActividadPlanCantActid )) )
            {
               GRID1_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vCACTIVIDADPARAPLANID"), ".", ",") != Convert.ToDecimal( AV7cActividadParaPlanid )) )
            {
               GRID1_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vCFYACTIVIDADPLANCANTACTFY"), ".", ",") != Convert.ToDecimal( AV8cFYActividadPlanCantActFY )) )
            {
               GRID1_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vCMES1"), ".", ",") != Convert.ToDecimal( AV9cmes1 )) )
            {
               GRID1_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vCMES2"), ".", ",") != Convert.ToDecimal( AV10cmes2 )) )
            {
               GRID1_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vCMES3"), ".", ",") != Convert.ToDecimal( AV11cmes3 )) )
            {
               GRID1_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vCMES4"), ".", ",") != Convert.ToDecimal( AV12cmes4 )) )
            {
               GRID1_nFirstRecordOnPage = 0;
            }
         }
         else
         {
            dynload_actions( ) ;
         }
      }

      protected void GXStart( )
      {
         /* Execute user event: Start */
         E190U2 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E190U2( )
      {
         /* Start Routine */
         Form.Caption = StringUtil.Format( "Selection List %1", "FYActividad Plan Cant Act", "", "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, "FORM", "Caption", Form.Caption, true);
         AV14ADVANCED_LABEL_TEMPLATE = "%1 <strong>%2</strong>";
      }

      private void E200U2( )
      {
         /* Load Routine */
         AV5LinkSelection = context.GetImagePath( "3914535b-0c03-44c5-9538-906a99cdd2bc", "", context.GetTheme( ));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavLinkselection_Internalname, AV5LinkSelection);
         AV17Linkselection_GXI = GXDbFile.PathToUrl( context.GetImagePath( "3914535b-0c03-44c5-9538-906a99cdd2bc", "", context.GetTheme( )));
         sendrow_842( ) ;
         GRID1_nCurrentRecord = (long)(GRID1_nCurrentRecord+1);
         if ( isFullAjaxMode( ) && ! bGXsfl_84_Refreshing )
         {
            context.DoAjaxLoad(84, Grid1Row);
         }
      }

      public void GXEnter( )
      {
         /* Execute user event: Enter */
         E210U2 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E210U2( )
      {
         /* Enter Routine */
         AV13pFYActividadPlanCantActid = A24FYActividadPlanCantActid;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13pFYActividadPlanCantActid", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13pFYActividadPlanCantActid), 9, 0)));
         context.setWebReturnParms(new Object[] {(int)AV13pFYActividadPlanCantActid});
         context.setWebReturnParmsMetadata(new Object[] {"AV13pFYActividadPlanCantActid"});
         context.wjLocDisableFrm = 1;
         context.nUserReturn = 1;
         returnInSub = true;
         if (true) return;
         /*  Sending Event outputs  */
      }

      public override void setparameters( Object[] obj )
      {
         createObjects();
         initialize();
         AV13pFYActividadPlanCantActid = Convert.ToInt32(getParm(obj,0));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13pFYActividadPlanCantActid", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13pFYActividadPlanCantActid), 9, 0)));
      }

      public override String getresponse( String sGXDynURL )
      {
         initialize_properties( ) ;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         sDynURL = sGXDynURL;
         nGotPars = (short)(1);
         nGXWrapped = (short)(1);
         context.SetWrapped(true);
         PA0U2( ) ;
         WS0U2( ) ;
         WE0U2( ) ;
         this.cleanup();
         context.SetWrapped(false);
         context.GX_msglist = BackMsgLst;
         return "";
      }

      public void responsestatic( String sGXDynURL )
      {
      }

      protected void define_styles( )
      {
         AddThemeStyleSheetFile("", context.GetTheme( )+".css", "?"+GetCacheInvalidationToken( ));
         bool outputEnabled = isOutputEnabled( ) ;
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         idxLst = 1;
         while ( idxLst <= Form.Jscriptsrc.Count )
         {
            context.AddJavascriptSource(StringUtil.RTrim( ((String)Form.Jscriptsrc.Item(idxLst))), "?201913182229", true);
            idxLst = (int)(idxLst+1);
         }
         if ( ! outputEnabled )
         {
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
         }
         /* End function define_styles */
      }

      protected void include_jscripts( )
      {
         context.AddJavascriptSource("messages.eng.js", "?"+GetCacheInvalidationToken( ), false);
         context.AddJavascriptSource("gx00n0.js", "?201913182229", false);
         /* End function include_jscripts */
      }

      protected void SubsflControlProps_842( )
      {
         edtavLinkselection_Internalname = "vLINKSELECTION_"+sGXsfl_84_idx;
         edtFYActividadPlanCantActid_Internalname = "FYACTIVIDADPLANCANTACTID_"+sGXsfl_84_idx;
         edtActividadParaPlanid_Internalname = "ACTIVIDADPARAPLANID_"+sGXsfl_84_idx;
         edtFYActividadPlanCantActFY_Internalname = "FYACTIVIDADPLANCANTACTFY_"+sGXsfl_84_idx;
         edtmes1_Internalname = "MES1_"+sGXsfl_84_idx;
         edtmes2_Internalname = "MES2_"+sGXsfl_84_idx;
         edtmes3_Internalname = "MES3_"+sGXsfl_84_idx;
         edtmes4_Internalname = "MES4_"+sGXsfl_84_idx;
         edtmes5_Internalname = "MES5_"+sGXsfl_84_idx;
      }

      protected void SubsflControlProps_fel_842( )
      {
         edtavLinkselection_Internalname = "vLINKSELECTION_"+sGXsfl_84_fel_idx;
         edtFYActividadPlanCantActid_Internalname = "FYACTIVIDADPLANCANTACTID_"+sGXsfl_84_fel_idx;
         edtActividadParaPlanid_Internalname = "ACTIVIDADPARAPLANID_"+sGXsfl_84_fel_idx;
         edtFYActividadPlanCantActFY_Internalname = "FYACTIVIDADPLANCANTACTFY_"+sGXsfl_84_fel_idx;
         edtmes1_Internalname = "MES1_"+sGXsfl_84_fel_idx;
         edtmes2_Internalname = "MES2_"+sGXsfl_84_fel_idx;
         edtmes3_Internalname = "MES3_"+sGXsfl_84_fel_idx;
         edtmes4_Internalname = "MES4_"+sGXsfl_84_fel_idx;
         edtmes5_Internalname = "MES5_"+sGXsfl_84_fel_idx;
      }

      protected void sendrow_842( )
      {
         SubsflControlProps_842( ) ;
         WB0U0( ) ;
         if ( ( 10 * 1 == 0 ) || ( nGXsfl_84_idx <= subGrid1_Recordsperpage( ) * 1 ) )
         {
            Grid1Row = GXWebRow.GetNew(context,Grid1Container);
            if ( subGrid1_Backcolorstyle == 0 )
            {
               /* None style subfile background logic. */
               subGrid1_Backstyle = 0;
               if ( StringUtil.StrCmp(subGrid1_Class, "") != 0 )
               {
                  subGrid1_Linesclass = subGrid1_Class+"Odd";
               }
            }
            else if ( subGrid1_Backcolorstyle == 1 )
            {
               /* Uniform style subfile background logic. */
               subGrid1_Backstyle = 0;
               subGrid1_Backcolor = subGrid1_Allbackcolor;
               if ( StringUtil.StrCmp(subGrid1_Class, "") != 0 )
               {
                  subGrid1_Linesclass = subGrid1_Class+"Uniform";
               }
            }
            else if ( subGrid1_Backcolorstyle == 2 )
            {
               /* Header style subfile background logic. */
               subGrid1_Backstyle = 1;
               if ( StringUtil.StrCmp(subGrid1_Class, "") != 0 )
               {
                  subGrid1_Linesclass = subGrid1_Class+"Odd";
               }
               subGrid1_Backcolor = (int)(0x0);
            }
            else if ( subGrid1_Backcolorstyle == 3 )
            {
               /* Report style subfile background logic. */
               subGrid1_Backstyle = 1;
               if ( ((int)(((nGXsfl_84_idx-1)/ (decimal)(1)) % (2))) == 0 )
               {
                  subGrid1_Backcolor = (int)(0x0);
                  if ( StringUtil.StrCmp(subGrid1_Class, "") != 0 )
                  {
                     subGrid1_Linesclass = subGrid1_Class+"Odd";
                  }
               }
               else
               {
                  subGrid1_Backcolor = (int)(0x0);
                  if ( StringUtil.StrCmp(subGrid1_Class, "") != 0 )
                  {
                     subGrid1_Linesclass = subGrid1_Class+"Even";
                  }
               }
            }
            if ( Grid1Container.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<tr ") ;
               context.WriteHtmlText( " class=\""+"PromptGrid"+"\" style=\""+""+"\"") ;
               context.WriteHtmlText( " gxrow=\""+sGXsfl_84_idx+"\">") ;
            }
            /* Subfile cell */
            if ( Grid1Container.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+""+"\""+" style=\""+""+"\">") ;
            }
            /* Static Bitmap Variable */
            edtavLinkselection_Link = "javascript:gx.popup.gxReturn(["+"'"+GXUtil.EncodeJSConstant( StringUtil.LTrim( StringUtil.NToC( (decimal)(A24FYActividadPlanCantActid), 9, 0, ".", "")))+"'"+"]);";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavLinkselection_Internalname, "Link", edtavLinkselection_Link, !bGXsfl_84_Refreshing);
            ClassString = "SelectionAttribute";
            StyleString = "";
            AV5LinkSelection_IsBlob = (bool)((String.IsNullOrEmpty(StringUtil.RTrim( AV5LinkSelection))&&String.IsNullOrEmpty(StringUtil.RTrim( AV17Linkselection_GXI)))||!String.IsNullOrEmpty(StringUtil.RTrim( AV5LinkSelection)));
            sImgUrl = (String.IsNullOrEmpty(StringUtil.RTrim( AV5LinkSelection)) ? AV17Linkselection_GXI : context.PathToRelativeUrl( AV5LinkSelection));
            Grid1Row.AddColumnProperties("bitmap", 1, isAjaxCallMode( ), new Object[] {(String)edtavLinkselection_Internalname,(String)sImgUrl,(String)edtavLinkselection_Link,(String)"",(String)"",context.GetTheme( ),(short)-1,(short)1,(String)"",(String)"",(short)1,(short)-1,(short)0,(String)"px",(short)0,(String)"px",(short)0,(short)0,(short)0,(String)"",(String)"",(String)StyleString,(String)ClassString,(String)"WWActionColumn",(String)"",(String)"",(String)"",(String)"",(String)"",(String)"",(short)1,(bool)AV5LinkSelection_IsBlob,(bool)false,context.GetImageSrcSet( sImgUrl)});
            /* Subfile cell */
            if ( Grid1Container.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "Attribute";
            Grid1Row.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtFYActividadPlanCantActid_Internalname,StringUtil.LTrim( StringUtil.NToC( (decimal)(A24FYActividadPlanCantActid), 9, 0, ".", "")),context.localUtil.Format( (decimal)(A24FYActividadPlanCantActid), "ZZZZZZZZ9"),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtFYActividadPlanCantActid_Jsonclick,(short)0,(String)"Attribute",(String)"",(String)ROClassString,(String)"WWColumn",(String)"",(short)-1,(short)0,(short)0,(String)"number",(String)"1",(short)0,(String)"px",(short)17,(String)"px",(short)9,(short)0,(short)0,(short)84,(short)1,(short)-1,(short)0,(bool)true,(String)"",(String)"right",(bool)false});
            /* Subfile cell */
            if ( Grid1Container.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "Attribute";
            Grid1Row.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtActividadParaPlanid_Internalname,StringUtil.LTrim( StringUtil.NToC( (decimal)(A23ActividadParaPlanid), 9, 0, ".", "")),context.localUtil.Format( (decimal)(A23ActividadParaPlanid), "ZZZZZZZZ9"),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtActividadParaPlanid_Jsonclick,(short)0,(String)"Attribute",(String)"",(String)ROClassString,(String)"WWColumn OptionalColumn",(String)"",(short)-1,(short)0,(short)0,(String)"number",(String)"1",(short)0,(String)"px",(short)17,(String)"px",(short)9,(short)0,(short)0,(short)84,(short)1,(short)-1,(short)0,(bool)true,(String)"",(String)"right",(bool)false});
            /* Subfile cell */
            if ( Grid1Container.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "DescriptionAttribute";
            edtFYActividadPlanCantActFY_Link = "javascript:gx.popup.gxReturn(["+"'"+GXUtil.EncodeJSConstant( StringUtil.LTrim( StringUtil.NToC( (decimal)(A24FYActividadPlanCantActid), 9, 0, ".", "")))+"'"+"]);";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtFYActividadPlanCantActFY_Internalname, "Link", edtFYActividadPlanCantActFY_Link, !bGXsfl_84_Refreshing);
            Grid1Row.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtFYActividadPlanCantActFY_Internalname,StringUtil.LTrim( StringUtil.NToC( (decimal)(A104FYActividadPlanCantActFY), 9, 0, ".", "")),context.localUtil.Format( (decimal)(A104FYActividadPlanCantActFY), "ZZZZZZZZ9"),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)edtFYActividadPlanCantActFY_Link,(String)"",(String)"",(String)"",(String)edtFYActividadPlanCantActFY_Jsonclick,(short)0,(String)"DescriptionAttribute",(String)"",(String)ROClassString,(String)"WWColumn",(String)"",(short)-1,(short)0,(short)0,(String)"number",(String)"1",(short)0,(String)"px",(short)17,(String)"px",(short)9,(short)0,(short)0,(short)84,(short)1,(short)-1,(short)0,(bool)true,(String)"",(String)"right",(bool)false});
            /* Subfile cell */
            if ( Grid1Container.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "Attribute";
            Grid1Row.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtmes1_Internalname,StringUtil.LTrim( StringUtil.NToC( (decimal)(A105mes1), 9, 0, ".", "")),context.localUtil.Format( (decimal)(A105mes1), "ZZZZZZZZ9"),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtmes1_Jsonclick,(short)0,(String)"Attribute",(String)"",(String)ROClassString,(String)"WWColumn OptionalColumn",(String)"",(short)-1,(short)0,(short)0,(String)"number",(String)"1",(short)0,(String)"px",(short)17,(String)"px",(short)9,(short)0,(short)0,(short)84,(short)1,(short)-1,(short)0,(bool)true,(String)"",(String)"right",(bool)false});
            /* Subfile cell */
            if ( Grid1Container.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "Attribute";
            Grid1Row.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtmes2_Internalname,StringUtil.LTrim( StringUtil.NToC( (decimal)(A106mes2), 9, 0, ".", "")),context.localUtil.Format( (decimal)(A106mes2), "ZZZZZZZZ9"),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtmes2_Jsonclick,(short)0,(String)"Attribute",(String)"",(String)ROClassString,(String)"WWColumn OptionalColumn",(String)"",(short)-1,(short)0,(short)0,(String)"number",(String)"1",(short)0,(String)"px",(short)17,(String)"px",(short)9,(short)0,(short)0,(short)84,(short)1,(short)-1,(short)0,(bool)true,(String)"",(String)"right",(bool)false});
            /* Subfile cell */
            if ( Grid1Container.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "Attribute";
            Grid1Row.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtmes3_Internalname,StringUtil.LTrim( StringUtil.NToC( (decimal)(A107mes3), 9, 0, ".", "")),context.localUtil.Format( (decimal)(A107mes3), "ZZZZZZZZ9"),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtmes3_Jsonclick,(short)0,(String)"Attribute",(String)"",(String)ROClassString,(String)"WWColumn OptionalColumn",(String)"",(short)-1,(short)0,(short)0,(String)"number",(String)"1",(short)0,(String)"px",(short)17,(String)"px",(short)9,(short)0,(short)0,(short)84,(short)1,(short)-1,(short)0,(bool)true,(String)"",(String)"right",(bool)false});
            /* Subfile cell */
            if ( Grid1Container.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "Attribute";
            Grid1Row.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtmes4_Internalname,StringUtil.LTrim( StringUtil.NToC( (decimal)(A108mes4), 9, 0, ".", "")),context.localUtil.Format( (decimal)(A108mes4), "ZZZZZZZZ9"),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtmes4_Jsonclick,(short)0,(String)"Attribute",(String)"",(String)ROClassString,(String)"WWColumn OptionalColumn",(String)"",(short)-1,(short)0,(short)0,(String)"number",(String)"1",(short)0,(String)"px",(short)17,(String)"px",(short)9,(short)0,(short)0,(short)84,(short)1,(short)-1,(short)0,(bool)true,(String)"",(String)"right",(bool)false});
            /* Subfile cell */
            if ( Grid1Container.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "Attribute";
            Grid1Row.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtmes5_Internalname,StringUtil.LTrim( StringUtil.NToC( (decimal)(A109mes5), 9, 0, ".", "")),context.localUtil.Format( (decimal)(A109mes5), "ZZZZZZZZ9"),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtmes5_Jsonclick,(short)0,(String)"Attribute",(String)"",(String)ROClassString,(String)"WWColumn OptionalColumn",(String)"",(short)-1,(short)0,(short)0,(String)"number",(String)"1",(short)0,(String)"px",(short)17,(String)"px",(short)9,(short)0,(short)0,(short)84,(short)1,(short)-1,(short)0,(bool)true,(String)"",(String)"right",(bool)false});
            send_integrity_lvl_hashes0U2( ) ;
            Grid1Container.AddRow(Grid1Row);
            nGXsfl_84_idx = (short)(((subGrid1_Islastpage==1)&&(nGXsfl_84_idx+1>subGrid1_Recordsperpage( )) ? 1 : nGXsfl_84_idx+1));
            sGXsfl_84_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_84_idx), 4, 0)), 4, "0");
            SubsflControlProps_842( ) ;
         }
         /* End function sendrow_842 */
      }

      protected void init_web_controls( )
      {
         /* End function init_web_controls */
      }

      protected void init_default_properties( )
      {
         lblLblfyactividadplancantactidfilter_Internalname = "LBLFYACTIVIDADPLANCANTACTIDFILTER";
         edtavCfyactividadplancantactid_Internalname = "vCFYACTIVIDADPLANCANTACTID";
         divFyactividadplancantactidfiltercontainer_Internalname = "FYACTIVIDADPLANCANTACTIDFILTERCONTAINER";
         lblLblactividadparaplanidfilter_Internalname = "LBLACTIVIDADPARAPLANIDFILTER";
         edtavCactividadparaplanid_Internalname = "vCACTIVIDADPARAPLANID";
         divActividadparaplanidfiltercontainer_Internalname = "ACTIVIDADPARAPLANIDFILTERCONTAINER";
         lblLblfyactividadplancantactfyfilter_Internalname = "LBLFYACTIVIDADPLANCANTACTFYFILTER";
         edtavCfyactividadplancantactfy_Internalname = "vCFYACTIVIDADPLANCANTACTFY";
         divFyactividadplancantactfyfiltercontainer_Internalname = "FYACTIVIDADPLANCANTACTFYFILTERCONTAINER";
         lblLblmes1filter_Internalname = "LBLMES1FILTER";
         edtavCmes1_Internalname = "vCMES1";
         divMes1filtercontainer_Internalname = "MES1FILTERCONTAINER";
         lblLblmes2filter_Internalname = "LBLMES2FILTER";
         edtavCmes2_Internalname = "vCMES2";
         divMes2filtercontainer_Internalname = "MES2FILTERCONTAINER";
         lblLblmes3filter_Internalname = "LBLMES3FILTER";
         edtavCmes3_Internalname = "vCMES3";
         divMes3filtercontainer_Internalname = "MES3FILTERCONTAINER";
         lblLblmes4filter_Internalname = "LBLMES4FILTER";
         edtavCmes4_Internalname = "vCMES4";
         divMes4filtercontainer_Internalname = "MES4FILTERCONTAINER";
         divAdvancedcontainer_Internalname = "ADVANCEDCONTAINER";
         bttBtntoggle_Internalname = "BTNTOGGLE";
         edtavLinkselection_Internalname = "vLINKSELECTION";
         edtFYActividadPlanCantActid_Internalname = "FYACTIVIDADPLANCANTACTID";
         edtActividadParaPlanid_Internalname = "ACTIVIDADPARAPLANID";
         edtFYActividadPlanCantActFY_Internalname = "FYACTIVIDADPLANCANTACTFY";
         edtmes1_Internalname = "MES1";
         edtmes2_Internalname = "MES2";
         edtmes3_Internalname = "MES3";
         edtmes4_Internalname = "MES4";
         edtmes5_Internalname = "MES5";
         bttBtn_cancel_Internalname = "BTN_CANCEL";
         divGridtable_Internalname = "GRIDTABLE";
         divMain_Internalname = "MAIN";
         Form.Internalname = "FORM";
         subGrid1_Internalname = "GRID1";
      }

      public override void initialize_properties( )
      {
         context.SetDefaultTheme("Carmine");
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         init_default_properties( ) ;
         edtmes5_Jsonclick = "";
         edtmes4_Jsonclick = "";
         edtmes3_Jsonclick = "";
         edtmes2_Jsonclick = "";
         edtmes1_Jsonclick = "";
         edtFYActividadPlanCantActFY_Jsonclick = "";
         edtActividadParaPlanid_Jsonclick = "";
         edtFYActividadPlanCantActid_Jsonclick = "";
         subGrid1_Allowcollapsing = 0;
         subGrid1_Allowselection = 0;
         edtFYActividadPlanCantActFY_Link = "";
         edtavLinkselection_Link = "";
         subGrid1_Header = "";
         subGrid1_Class = "PromptGrid";
         subGrid1_Backcolorstyle = 0;
         edtavCmes4_Jsonclick = "";
         edtavCmes4_Enabled = 1;
         edtavCmes4_Visible = 1;
         edtavCmes3_Jsonclick = "";
         edtavCmes3_Enabled = 1;
         edtavCmes3_Visible = 1;
         edtavCmes2_Jsonclick = "";
         edtavCmes2_Enabled = 1;
         edtavCmes2_Visible = 1;
         edtavCmes1_Jsonclick = "";
         edtavCmes1_Enabled = 1;
         edtavCmes1_Visible = 1;
         edtavCfyactividadplancantactfy_Jsonclick = "";
         edtavCfyactividadplancantactfy_Enabled = 1;
         edtavCfyactividadplancantactfy_Visible = 1;
         edtavCactividadparaplanid_Jsonclick = "";
         edtavCactividadparaplanid_Enabled = 1;
         edtavCactividadparaplanid_Visible = 1;
         edtavCfyactividadplancantactid_Jsonclick = "";
         edtavCfyactividadplancantactid_Enabled = 1;
         edtavCfyactividadplancantactid_Visible = 1;
         divMes4filtercontainer_Class = "AdvancedContainerItem";
         divMes3filtercontainer_Class = "AdvancedContainerItem";
         divMes2filtercontainer_Class = "AdvancedContainerItem";
         divMes1filtercontainer_Class = "AdvancedContainerItem";
         divFyactividadplancantactfyfiltercontainer_Class = "AdvancedContainerItem";
         divActividadparaplanidfiltercontainer_Class = "AdvancedContainerItem";
         divFyactividadplancantactidfiltercontainer_Class = "AdvancedContainerItem";
         bttBtntoggle_Class = "BtnToggle";
         divAdvancedcontainer_Class = "AdvancedContainerVisible";
         Form.Headerrawhtml = "";
         Form.Background = "";
         Form.Textcolor = 0;
         Form.Backcolor = (int)(0xFFFFFF);
         Form.Caption = "Selection List FYActividad Plan Cant Act";
         subGrid1_Rows = 10;
         if ( context.isSpaRequest( ) )
         {
            enableJsOutput();
         }
      }

      public override bool SupportAjaxEvent( )
      {
         return true ;
      }

      public override void InitializeDynEvents( )
      {
         setEventMetadata("REFRESH","{handler:'Refresh',iparms:[{av:'GRID1_nFirstRecordOnPage'},{av:'GRID1_nEOF'},{av:'subGrid1_Rows',ctrl:'GRID1',prop:'Rows'},{av:'AV6cFYActividadPlanCantActid',fld:'vCFYACTIVIDADPLANCANTACTID',pic:'ZZZZZZZZ9'},{av:'AV7cActividadParaPlanid',fld:'vCACTIVIDADPARAPLANID',pic:'ZZZZZZZZ9'},{av:'AV8cFYActividadPlanCantActFY',fld:'vCFYACTIVIDADPLANCANTACTFY',pic:'ZZZZZZZZ9'},{av:'AV9cmes1',fld:'vCMES1',pic:'ZZZZZZZZ9'},{av:'AV10cmes2',fld:'vCMES2',pic:'ZZZZZZZZ9'},{av:'AV11cmes3',fld:'vCMES3',pic:'ZZZZZZZZ9'},{av:'AV12cmes4',fld:'vCMES4',pic:'ZZZZZZZZ9'}]");
         setEventMetadata("REFRESH",",oparms:[]}");
         setEventMetadata("'TOGGLE'","{handler:'E180U1',iparms:[{av:'divAdvancedcontainer_Class',ctrl:'ADVANCEDCONTAINER',prop:'Class'},{ctrl:'BTNTOGGLE',prop:'Class'}]");
         setEventMetadata("'TOGGLE'",",oparms:[{av:'divAdvancedcontainer_Class',ctrl:'ADVANCEDCONTAINER',prop:'Class'},{ctrl:'BTNTOGGLE',prop:'Class'}]}");
         setEventMetadata("LBLFYACTIVIDADPLANCANTACTIDFILTER.CLICK","{handler:'E110U1',iparms:[{av:'divFyactividadplancantactidfiltercontainer_Class',ctrl:'FYACTIVIDADPLANCANTACTIDFILTERCONTAINER',prop:'Class'}]");
         setEventMetadata("LBLFYACTIVIDADPLANCANTACTIDFILTER.CLICK",",oparms:[{av:'divFyactividadplancantactidfiltercontainer_Class',ctrl:'FYACTIVIDADPLANCANTACTIDFILTERCONTAINER',prop:'Class'},{av:'edtavCfyactividadplancantactid_Visible',ctrl:'vCFYACTIVIDADPLANCANTACTID',prop:'Visible'}]}");
         setEventMetadata("LBLACTIVIDADPARAPLANIDFILTER.CLICK","{handler:'E120U1',iparms:[{av:'divActividadparaplanidfiltercontainer_Class',ctrl:'ACTIVIDADPARAPLANIDFILTERCONTAINER',prop:'Class'}]");
         setEventMetadata("LBLACTIVIDADPARAPLANIDFILTER.CLICK",",oparms:[{av:'divActividadparaplanidfiltercontainer_Class',ctrl:'ACTIVIDADPARAPLANIDFILTERCONTAINER',prop:'Class'},{av:'edtavCactividadparaplanid_Visible',ctrl:'vCACTIVIDADPARAPLANID',prop:'Visible'}]}");
         setEventMetadata("LBLFYACTIVIDADPLANCANTACTFYFILTER.CLICK","{handler:'E130U1',iparms:[{av:'divFyactividadplancantactfyfiltercontainer_Class',ctrl:'FYACTIVIDADPLANCANTACTFYFILTERCONTAINER',prop:'Class'}]");
         setEventMetadata("LBLFYACTIVIDADPLANCANTACTFYFILTER.CLICK",",oparms:[{av:'divFyactividadplancantactfyfiltercontainer_Class',ctrl:'FYACTIVIDADPLANCANTACTFYFILTERCONTAINER',prop:'Class'},{av:'edtavCfyactividadplancantactfy_Visible',ctrl:'vCFYACTIVIDADPLANCANTACTFY',prop:'Visible'}]}");
         setEventMetadata("LBLMES1FILTER.CLICK","{handler:'E140U1',iparms:[{av:'divMes1filtercontainer_Class',ctrl:'MES1FILTERCONTAINER',prop:'Class'}]");
         setEventMetadata("LBLMES1FILTER.CLICK",",oparms:[{av:'divMes1filtercontainer_Class',ctrl:'MES1FILTERCONTAINER',prop:'Class'},{av:'edtavCmes1_Visible',ctrl:'vCMES1',prop:'Visible'}]}");
         setEventMetadata("LBLMES2FILTER.CLICK","{handler:'E150U1',iparms:[{av:'divMes2filtercontainer_Class',ctrl:'MES2FILTERCONTAINER',prop:'Class'}]");
         setEventMetadata("LBLMES2FILTER.CLICK",",oparms:[{av:'divMes2filtercontainer_Class',ctrl:'MES2FILTERCONTAINER',prop:'Class'},{av:'edtavCmes2_Visible',ctrl:'vCMES2',prop:'Visible'}]}");
         setEventMetadata("LBLMES3FILTER.CLICK","{handler:'E160U1',iparms:[{av:'divMes3filtercontainer_Class',ctrl:'MES3FILTERCONTAINER',prop:'Class'}]");
         setEventMetadata("LBLMES3FILTER.CLICK",",oparms:[{av:'divMes3filtercontainer_Class',ctrl:'MES3FILTERCONTAINER',prop:'Class'},{av:'edtavCmes3_Visible',ctrl:'vCMES3',prop:'Visible'}]}");
         setEventMetadata("LBLMES4FILTER.CLICK","{handler:'E170U1',iparms:[{av:'divMes4filtercontainer_Class',ctrl:'MES4FILTERCONTAINER',prop:'Class'}]");
         setEventMetadata("LBLMES4FILTER.CLICK",",oparms:[{av:'divMes4filtercontainer_Class',ctrl:'MES4FILTERCONTAINER',prop:'Class'},{av:'edtavCmes4_Visible',ctrl:'vCMES4',prop:'Visible'}]}");
         setEventMetadata("ENTER","{handler:'E210U2',iparms:[{av:'A24FYActividadPlanCantActid',fld:'FYACTIVIDADPLANCANTACTID',pic:'ZZZZZZZZ9',hsh:true}]");
         setEventMetadata("ENTER",",oparms:[{av:'AV13pFYActividadPlanCantActid',fld:'vPFYACTIVIDADPLANCANTACTID',pic:'ZZZZZZZZ9'}]}");
         setEventMetadata("GRID1_FIRSTPAGE","{handler:'subgrid1_firstpage',iparms:[{av:'GRID1_nFirstRecordOnPage'},{av:'GRID1_nEOF'},{av:'subGrid1_Rows',ctrl:'GRID1',prop:'Rows'},{av:'AV6cFYActividadPlanCantActid',fld:'vCFYACTIVIDADPLANCANTACTID',pic:'ZZZZZZZZ9'},{av:'AV7cActividadParaPlanid',fld:'vCACTIVIDADPARAPLANID',pic:'ZZZZZZZZ9'},{av:'AV8cFYActividadPlanCantActFY',fld:'vCFYACTIVIDADPLANCANTACTFY',pic:'ZZZZZZZZ9'},{av:'AV9cmes1',fld:'vCMES1',pic:'ZZZZZZZZ9'},{av:'AV10cmes2',fld:'vCMES2',pic:'ZZZZZZZZ9'},{av:'AV11cmes3',fld:'vCMES3',pic:'ZZZZZZZZ9'},{av:'AV12cmes4',fld:'vCMES4',pic:'ZZZZZZZZ9'}]");
         setEventMetadata("GRID1_FIRSTPAGE",",oparms:[]}");
         setEventMetadata("GRID1_PREVPAGE","{handler:'subgrid1_previouspage',iparms:[{av:'GRID1_nFirstRecordOnPage'},{av:'GRID1_nEOF'},{av:'subGrid1_Rows',ctrl:'GRID1',prop:'Rows'},{av:'AV6cFYActividadPlanCantActid',fld:'vCFYACTIVIDADPLANCANTACTID',pic:'ZZZZZZZZ9'},{av:'AV7cActividadParaPlanid',fld:'vCACTIVIDADPARAPLANID',pic:'ZZZZZZZZ9'},{av:'AV8cFYActividadPlanCantActFY',fld:'vCFYACTIVIDADPLANCANTACTFY',pic:'ZZZZZZZZ9'},{av:'AV9cmes1',fld:'vCMES1',pic:'ZZZZZZZZ9'},{av:'AV10cmes2',fld:'vCMES2',pic:'ZZZZZZZZ9'},{av:'AV11cmes3',fld:'vCMES3',pic:'ZZZZZZZZ9'},{av:'AV12cmes4',fld:'vCMES4',pic:'ZZZZZZZZ9'}]");
         setEventMetadata("GRID1_PREVPAGE",",oparms:[]}");
         setEventMetadata("GRID1_NEXTPAGE","{handler:'subgrid1_nextpage',iparms:[{av:'GRID1_nFirstRecordOnPage'},{av:'GRID1_nEOF'},{av:'subGrid1_Rows',ctrl:'GRID1',prop:'Rows'},{av:'AV6cFYActividadPlanCantActid',fld:'vCFYACTIVIDADPLANCANTACTID',pic:'ZZZZZZZZ9'},{av:'AV7cActividadParaPlanid',fld:'vCACTIVIDADPARAPLANID',pic:'ZZZZZZZZ9'},{av:'AV8cFYActividadPlanCantActFY',fld:'vCFYACTIVIDADPLANCANTACTFY',pic:'ZZZZZZZZ9'},{av:'AV9cmes1',fld:'vCMES1',pic:'ZZZZZZZZ9'},{av:'AV10cmes2',fld:'vCMES2',pic:'ZZZZZZZZ9'},{av:'AV11cmes3',fld:'vCMES3',pic:'ZZZZZZZZ9'},{av:'AV12cmes4',fld:'vCMES4',pic:'ZZZZZZZZ9'}]");
         setEventMetadata("GRID1_NEXTPAGE",",oparms:[]}");
         setEventMetadata("GRID1_LASTPAGE","{handler:'subgrid1_lastpage',iparms:[{av:'GRID1_nFirstRecordOnPage'},{av:'GRID1_nEOF'},{av:'subGrid1_Rows',ctrl:'GRID1',prop:'Rows'},{av:'AV6cFYActividadPlanCantActid',fld:'vCFYACTIVIDADPLANCANTACTID',pic:'ZZZZZZZZ9'},{av:'AV7cActividadParaPlanid',fld:'vCACTIVIDADPARAPLANID',pic:'ZZZZZZZZ9'},{av:'AV8cFYActividadPlanCantActFY',fld:'vCFYACTIVIDADPLANCANTACTFY',pic:'ZZZZZZZZ9'},{av:'AV9cmes1',fld:'vCMES1',pic:'ZZZZZZZZ9'},{av:'AV10cmes2',fld:'vCMES2',pic:'ZZZZZZZZ9'},{av:'AV11cmes3',fld:'vCMES3',pic:'ZZZZZZZZ9'},{av:'AV12cmes4',fld:'vCMES4',pic:'ZZZZZZZZ9'}]");
         setEventMetadata("GRID1_LASTPAGE",",oparms:[]}");
         return  ;
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         gxfirstwebparm = "";
         gxfirstwebparm_bkp = "";
         GXKey = "";
         sDynURL = "";
         FormProcess = "";
         bodyStyle = "";
         GX_FocusControl = "";
         Form = new GXWebForm();
         sPrefix = "";
         lblLblfyactividadplancantactidfilter_Jsonclick = "";
         TempTags = "";
         lblLblactividadparaplanidfilter_Jsonclick = "";
         lblLblfyactividadplancantactfyfilter_Jsonclick = "";
         lblLblmes1filter_Jsonclick = "";
         lblLblmes2filter_Jsonclick = "";
         lblLblmes3filter_Jsonclick = "";
         lblLblmes4filter_Jsonclick = "";
         ClassString = "";
         StyleString = "";
         bttBtntoggle_Jsonclick = "";
         Grid1Container = new GXWebGrid( context);
         sStyleString = "";
         subGrid1_Linesclass = "";
         Grid1Column = new GXWebColumn();
         AV5LinkSelection = "";
         bttBtn_cancel_Jsonclick = "";
         sEvt = "";
         EvtGridId = "";
         EvtRowId = "";
         sEvtType = "";
         AV17Linkselection_GXI = "";
         scmdbuf = "";
         H000U2_A109mes5 = new int[1] ;
         H000U2_n109mes5 = new bool[] {false} ;
         H000U2_A108mes4 = new int[1] ;
         H000U2_n108mes4 = new bool[] {false} ;
         H000U2_A107mes3 = new int[1] ;
         H000U2_n107mes3 = new bool[] {false} ;
         H000U2_A106mes2 = new int[1] ;
         H000U2_n106mes2 = new bool[] {false} ;
         H000U2_A105mes1 = new int[1] ;
         H000U2_n105mes1 = new bool[] {false} ;
         H000U2_A104FYActividadPlanCantActFY = new int[1] ;
         H000U2_A23ActividadParaPlanid = new int[1] ;
         H000U2_A24FYActividadPlanCantActid = new int[1] ;
         H000U3_AGRID1_nRecordCount = new long[1] ;
         AV14ADVANCED_LABEL_TEMPLATE = "";
         Grid1Row = new GXWebRow();
         BackMsgLst = new msglist();
         LclMsgLst = new msglist();
         sImgUrl = "";
         ROClassString = "";
         pr_datastore1 = new DataStoreProvider(context, new GeneXus.Programs.gx00n0__datastore1(),
            new Object[][] {
                new Object[] {
               H000U2_A109mes5, H000U2_n109mes5, H000U2_A108mes4, H000U2_n108mes4, H000U2_A107mes3, H000U2_n107mes3, H000U2_A106mes2, H000U2_n106mes2, H000U2_A105mes1, H000U2_n105mes1,
               H000U2_A104FYActividadPlanCantActFY, H000U2_A23ActividadParaPlanid, H000U2_A24FYActividadPlanCantActid
               }
               , new Object[] {
               H000U3_AGRID1_nRecordCount
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private short nGotPars ;
      private short GxWebError ;
      private short nRC_GXsfl_84 ;
      private short nGXsfl_84_idx=1 ;
      private short GRID1_nEOF ;
      private short initialized ;
      private short gxajaxcallmode ;
      private short wbEnd ;
      private short wbStart ;
      private short subGrid1_Backcolorstyle ;
      private short subGrid1_Titlebackstyle ;
      private short subGrid1_Allowselection ;
      private short subGrid1_Allowhovering ;
      private short subGrid1_Allowcollapsing ;
      private short subGrid1_Collapsed ;
      private short nDonePA ;
      private short gxcookieaux ;
      private short nGXWrapped ;
      private short subGrid1_Backstyle ;
      private int subGrid1_Rows ;
      private int AV6cFYActividadPlanCantActid ;
      private int AV7cActividadParaPlanid ;
      private int AV8cFYActividadPlanCantActFY ;
      private int AV9cmes1 ;
      private int AV10cmes2 ;
      private int AV11cmes3 ;
      private int AV12cmes4 ;
      private int AV13pFYActividadPlanCantActid ;
      private int edtavCfyactividadplancantactid_Enabled ;
      private int edtavCfyactividadplancantactid_Visible ;
      private int edtavCactividadparaplanid_Enabled ;
      private int edtavCactividadparaplanid_Visible ;
      private int edtavCfyactividadplancantactfy_Enabled ;
      private int edtavCfyactividadplancantactfy_Visible ;
      private int edtavCmes1_Enabled ;
      private int edtavCmes1_Visible ;
      private int edtavCmes2_Enabled ;
      private int edtavCmes2_Visible ;
      private int edtavCmes3_Enabled ;
      private int edtavCmes3_Visible ;
      private int edtavCmes4_Enabled ;
      private int edtavCmes4_Visible ;
      private int subGrid1_Titlebackcolor ;
      private int subGrid1_Allbackcolor ;
      private int A24FYActividadPlanCantActid ;
      private int A23ActividadParaPlanid ;
      private int A104FYActividadPlanCantActFY ;
      private int A105mes1 ;
      private int A106mes2 ;
      private int A107mes3 ;
      private int A108mes4 ;
      private int A109mes5 ;
      private int subGrid1_Selectedindex ;
      private int subGrid1_Selectioncolor ;
      private int subGrid1_Hoveringcolor ;
      private int subGrid1_Islastpage ;
      private int GXPagingFrom2 ;
      private int GXPagingTo2 ;
      private int idxLst ;
      private int subGrid1_Backcolor ;
      private long GRID1_nFirstRecordOnPage ;
      private long GRID1_nCurrentRecord ;
      private long GRID1_nRecordCount ;
      private String divAdvancedcontainer_Class ;
      private String bttBtntoggle_Class ;
      private String divFyactividadplancantactidfiltercontainer_Class ;
      private String divActividadparaplanidfiltercontainer_Class ;
      private String divFyactividadplancantactfyfiltercontainer_Class ;
      private String divMes1filtercontainer_Class ;
      private String divMes2filtercontainer_Class ;
      private String divMes3filtercontainer_Class ;
      private String divMes4filtercontainer_Class ;
      private String gxfirstwebparm ;
      private String gxfirstwebparm_bkp ;
      private String sGXsfl_84_idx="0001" ;
      private String GXKey ;
      private String sDynURL ;
      private String FormProcess ;
      private String bodyStyle ;
      private String GX_FocusControl ;
      private String sPrefix ;
      private String divMain_Internalname ;
      private String divAdvancedcontainer_Internalname ;
      private String divFyactividadplancantactidfiltercontainer_Internalname ;
      private String lblLblfyactividadplancantactidfilter_Internalname ;
      private String lblLblfyactividadplancantactidfilter_Jsonclick ;
      private String edtavCfyactividadplancantactid_Internalname ;
      private String TempTags ;
      private String edtavCfyactividadplancantactid_Jsonclick ;
      private String divActividadparaplanidfiltercontainer_Internalname ;
      private String lblLblactividadparaplanidfilter_Internalname ;
      private String lblLblactividadparaplanidfilter_Jsonclick ;
      private String edtavCactividadparaplanid_Internalname ;
      private String edtavCactividadparaplanid_Jsonclick ;
      private String divFyactividadplancantactfyfiltercontainer_Internalname ;
      private String lblLblfyactividadplancantactfyfilter_Internalname ;
      private String lblLblfyactividadplancantactfyfilter_Jsonclick ;
      private String edtavCfyactividadplancantactfy_Internalname ;
      private String edtavCfyactividadplancantactfy_Jsonclick ;
      private String divMes1filtercontainer_Internalname ;
      private String lblLblmes1filter_Internalname ;
      private String lblLblmes1filter_Jsonclick ;
      private String edtavCmes1_Internalname ;
      private String edtavCmes1_Jsonclick ;
      private String divMes2filtercontainer_Internalname ;
      private String lblLblmes2filter_Internalname ;
      private String lblLblmes2filter_Jsonclick ;
      private String edtavCmes2_Internalname ;
      private String edtavCmes2_Jsonclick ;
      private String divMes3filtercontainer_Internalname ;
      private String lblLblmes3filter_Internalname ;
      private String lblLblmes3filter_Jsonclick ;
      private String edtavCmes3_Internalname ;
      private String edtavCmes3_Jsonclick ;
      private String divMes4filtercontainer_Internalname ;
      private String lblLblmes4filter_Internalname ;
      private String lblLblmes4filter_Jsonclick ;
      private String edtavCmes4_Internalname ;
      private String edtavCmes4_Jsonclick ;
      private String divGridtable_Internalname ;
      private String ClassString ;
      private String StyleString ;
      private String bttBtntoggle_Internalname ;
      private String bttBtntoggle_Jsonclick ;
      private String sStyleString ;
      private String subGrid1_Internalname ;
      private String subGrid1_Class ;
      private String subGrid1_Linesclass ;
      private String subGrid1_Header ;
      private String edtavLinkselection_Link ;
      private String edtFYActividadPlanCantActFY_Link ;
      private String bttBtn_cancel_Internalname ;
      private String bttBtn_cancel_Jsonclick ;
      private String sEvt ;
      private String EvtGridId ;
      private String EvtRowId ;
      private String sEvtType ;
      private String edtavLinkselection_Internalname ;
      private String edtFYActividadPlanCantActid_Internalname ;
      private String edtActividadParaPlanid_Internalname ;
      private String edtFYActividadPlanCantActFY_Internalname ;
      private String edtmes1_Internalname ;
      private String edtmes2_Internalname ;
      private String edtmes3_Internalname ;
      private String edtmes4_Internalname ;
      private String edtmes5_Internalname ;
      private String scmdbuf ;
      private String AV14ADVANCED_LABEL_TEMPLATE ;
      private String sGXsfl_84_fel_idx="0001" ;
      private String sImgUrl ;
      private String ROClassString ;
      private String edtFYActividadPlanCantActid_Jsonclick ;
      private String edtActividadParaPlanid_Jsonclick ;
      private String edtFYActividadPlanCantActFY_Jsonclick ;
      private String edtmes1_Jsonclick ;
      private String edtmes2_Jsonclick ;
      private String edtmes3_Jsonclick ;
      private String edtmes4_Jsonclick ;
      private String edtmes5_Jsonclick ;
      private bool entryPointCalled ;
      private bool toggleJsOutput ;
      private bool wbLoad ;
      private bool Rfr0gs ;
      private bool wbErr ;
      private bool bGXsfl_84_Refreshing=false ;
      private bool n105mes1 ;
      private bool n106mes2 ;
      private bool n107mes3 ;
      private bool n108mes4 ;
      private bool n109mes5 ;
      private bool gxdyncontrolsrefreshing ;
      private bool returnInSub ;
      private bool AV5LinkSelection_IsBlob ;
      private String AV17Linkselection_GXI ;
      private String AV5LinkSelection ;
      private GXWebGrid Grid1Container ;
      private GXWebRow Grid1Row ;
      private GXWebColumn Grid1Column ;
      private IGxDataStore dsDataStore1 ;
      private IGxDataStore dsDefault ;
      private IDataStoreProvider pr_datastore1 ;
      private int[] H000U2_A109mes5 ;
      private bool[] H000U2_n109mes5 ;
      private int[] H000U2_A108mes4 ;
      private bool[] H000U2_n108mes4 ;
      private int[] H000U2_A107mes3 ;
      private bool[] H000U2_n107mes3 ;
      private int[] H000U2_A106mes2 ;
      private bool[] H000U2_n106mes2 ;
      private int[] H000U2_A105mes1 ;
      private bool[] H000U2_n105mes1 ;
      private int[] H000U2_A104FYActividadPlanCantActFY ;
      private int[] H000U2_A23ActividadParaPlanid ;
      private int[] H000U2_A24FYActividadPlanCantActid ;
      private long[] H000U3_AGRID1_nRecordCount ;
      private msglist BackMsgLst ;
      private msglist LclMsgLst ;
      private int aP0_pFYActividadPlanCantActid ;
      private GXWebForm Form ;
   }

   public class gx00n0__datastore1 : DataStoreHelperBase, IDataStoreHelper
   {
      protected Object[] conditional_H000U2( IGxContext context ,
                                             int AV7cActividadParaPlanid ,
                                             int AV8cFYActividadPlanCantActFY ,
                                             int AV9cmes1 ,
                                             int AV10cmes2 ,
                                             int AV11cmes3 ,
                                             int AV12cmes4 ,
                                             int A23ActividadParaPlanid ,
                                             int A104FYActividadPlanCantActFY ,
                                             int A105mes1 ,
                                             int A106mes2 ,
                                             int A107mes3 ,
                                             int A108mes4 ,
                                             int AV6cFYActividadPlanCantActid )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int1 ;
         GXv_int1 = new short [10] ;
         Object[] GXv_Object2 ;
         GXv_Object2 = new Object [2] ;
         String sSelectString ;
         String sFromString ;
         String sOrderString ;
         sSelectString = " [mes5], [mes4], [mes3], [mes2], [mes1], [FY], [idActividadParaPlan], [id]";
         sFromString = " FROM dbo.[FYActividadPlanCantAct] WITH (NOLOCK)";
         sOrderString = "";
         sWhereString = sWhereString + " WHERE ([id] >= @AV6cFYActividadPlanCantActid)";
         if ( ! (0==AV7cActividadParaPlanid) )
         {
            sWhereString = sWhereString + " and ([idActividadParaPlan] >= @AV7cActividadParaPlanid)";
         }
         else
         {
            GXv_int1[1] = 1;
         }
         if ( ! (0==AV8cFYActividadPlanCantActFY) )
         {
            sWhereString = sWhereString + " and ([FY] >= @AV8cFYActividadPlanCantActFY)";
         }
         else
         {
            GXv_int1[2] = 1;
         }
         if ( ! (0==AV9cmes1) )
         {
            sWhereString = sWhereString + " and ([mes1] >= @AV9cmes1)";
         }
         else
         {
            GXv_int1[3] = 1;
         }
         if ( ! (0==AV10cmes2) )
         {
            sWhereString = sWhereString + " and ([mes2] >= @AV10cmes2)";
         }
         else
         {
            GXv_int1[4] = 1;
         }
         if ( ! (0==AV11cmes3) )
         {
            sWhereString = sWhereString + " and ([mes3] >= @AV11cmes3)";
         }
         else
         {
            GXv_int1[5] = 1;
         }
         if ( ! (0==AV12cmes4) )
         {
            sWhereString = sWhereString + " and ([mes4] >= @AV12cmes4)";
         }
         else
         {
            GXv_int1[6] = 1;
         }
         sOrderString = sOrderString + " ORDER BY [id]";
         scmdbuf = "SELECT " + sSelectString + sFromString + sWhereString + "" + sOrderString + " OFFSET " + "@GXPagingFrom2" + " ROWS FETCH NEXT CAST((SELECT CASE WHEN " + "@GXPagingTo2" + " > 0 THEN " + "@GXPagingTo2" + " ELSE 1e9 END) AS INTEGER) ROWS ONLY";
         GXv_Object2[0] = scmdbuf;
         GXv_Object2[1] = GXv_int1;
         return GXv_Object2 ;
      }

      protected Object[] conditional_H000U3( IGxContext context ,
                                             int AV7cActividadParaPlanid ,
                                             int AV8cFYActividadPlanCantActFY ,
                                             int AV9cmes1 ,
                                             int AV10cmes2 ,
                                             int AV11cmes3 ,
                                             int AV12cmes4 ,
                                             int A23ActividadParaPlanid ,
                                             int A104FYActividadPlanCantActFY ,
                                             int A105mes1 ,
                                             int A106mes2 ,
                                             int A107mes3 ,
                                             int A108mes4 ,
                                             int AV6cFYActividadPlanCantActid )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int3 ;
         GXv_int3 = new short [7] ;
         Object[] GXv_Object4 ;
         GXv_Object4 = new Object [2] ;
         scmdbuf = "SELECT COUNT(*) FROM dbo.[FYActividadPlanCantAct] WITH (NOLOCK)";
         scmdbuf = scmdbuf + " WHERE ([id] >= @AV6cFYActividadPlanCantActid)";
         if ( ! (0==AV7cActividadParaPlanid) )
         {
            sWhereString = sWhereString + " and ([idActividadParaPlan] >= @AV7cActividadParaPlanid)";
         }
         else
         {
            GXv_int3[1] = 1;
         }
         if ( ! (0==AV8cFYActividadPlanCantActFY) )
         {
            sWhereString = sWhereString + " and ([FY] >= @AV8cFYActividadPlanCantActFY)";
         }
         else
         {
            GXv_int3[2] = 1;
         }
         if ( ! (0==AV9cmes1) )
         {
            sWhereString = sWhereString + " and ([mes1] >= @AV9cmes1)";
         }
         else
         {
            GXv_int3[3] = 1;
         }
         if ( ! (0==AV10cmes2) )
         {
            sWhereString = sWhereString + " and ([mes2] >= @AV10cmes2)";
         }
         else
         {
            GXv_int3[4] = 1;
         }
         if ( ! (0==AV11cmes3) )
         {
            sWhereString = sWhereString + " and ([mes3] >= @AV11cmes3)";
         }
         else
         {
            GXv_int3[5] = 1;
         }
         if ( ! (0==AV12cmes4) )
         {
            sWhereString = sWhereString + " and ([mes4] >= @AV12cmes4)";
         }
         else
         {
            GXv_int3[6] = 1;
         }
         scmdbuf = scmdbuf + sWhereString;
         scmdbuf = scmdbuf + "";
         GXv_Object4[0] = scmdbuf;
         GXv_Object4[1] = GXv_int3;
         return GXv_Object4 ;
      }

      public override Object [] getDynamicStatement( int cursor ,
                                                     IGxContext context ,
                                                     Object [] dynConstraints )
      {
         switch ( cursor )
         {
               case 0 :
                     return conditional_H000U2(context, (int)dynConstraints[0] , (int)dynConstraints[1] , (int)dynConstraints[2] , (int)dynConstraints[3] , (int)dynConstraints[4] , (int)dynConstraints[5] , (int)dynConstraints[6] , (int)dynConstraints[7] , (int)dynConstraints[8] , (int)dynConstraints[9] , (int)dynConstraints[10] , (int)dynConstraints[11] , (int)dynConstraints[12] );
               case 1 :
                     return conditional_H000U3(context, (int)dynConstraints[0] , (int)dynConstraints[1] , (int)dynConstraints[2] , (int)dynConstraints[3] , (int)dynConstraints[4] , (int)dynConstraints[5] , (int)dynConstraints[6] , (int)dynConstraints[7] , (int)dynConstraints[8] , (int)dynConstraints[9] , (int)dynConstraints[10] , (int)dynConstraints[11] , (int)dynConstraints[12] );
         }
         return base.getDynamicStatement(cursor, context, dynConstraints);
      }

      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmH000U2 ;
          prmH000U2 = new Object[] {
          new Object[] {"@AV6cFYActividadPlanCantActid",SqlDbType.Int,9,0} ,
          new Object[] {"@AV7cActividadParaPlanid",SqlDbType.Int,9,0} ,
          new Object[] {"@AV8cFYActividadPlanCantActFY",SqlDbType.Int,9,0} ,
          new Object[] {"@AV9cmes1",SqlDbType.Int,9,0} ,
          new Object[] {"@AV10cmes2",SqlDbType.Int,9,0} ,
          new Object[] {"@AV11cmes3",SqlDbType.Int,9,0} ,
          new Object[] {"@AV12cmes4",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingFrom2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingTo2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingTo2",SqlDbType.Int,9,0}
          } ;
          Object[] prmH000U3 ;
          prmH000U3 = new Object[] {
          new Object[] {"@AV6cFYActividadPlanCantActid",SqlDbType.Int,9,0} ,
          new Object[] {"@AV7cActividadParaPlanid",SqlDbType.Int,9,0} ,
          new Object[] {"@AV8cFYActividadPlanCantActFY",SqlDbType.Int,9,0} ,
          new Object[] {"@AV9cmes1",SqlDbType.Int,9,0} ,
          new Object[] {"@AV10cmes2",SqlDbType.Int,9,0} ,
          new Object[] {"@AV11cmes3",SqlDbType.Int,9,0} ,
          new Object[] {"@AV12cmes4",SqlDbType.Int,9,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("H000U2", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH000U2,11,0,false,false )
             ,new CursorDef("H000U3", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH000U3,1,0,false,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((int[]) buf[2])[0] = rslt.getInt(2) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                ((int[]) buf[4])[0] = rslt.getInt(3) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(3);
                ((int[]) buf[6])[0] = rslt.getInt(4) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(4);
                ((int[]) buf[8])[0] = rslt.getInt(5) ;
                ((bool[]) buf[9])[0] = rslt.wasNull(5);
                ((int[]) buf[10])[0] = rslt.getInt(6) ;
                ((int[]) buf[11])[0] = rslt.getInt(7) ;
                ((int[]) buf[12])[0] = rslt.getInt(8) ;
                return;
             case 1 :
                ((long[]) buf[0])[0] = rslt.getLong(1) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       short sIdx ;
       switch ( cursor )
       {
             case 0 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[10]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[11]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[12]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[13]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[14]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[15]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[16]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[17]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[18]);
                }
                if ( (short)parms[9] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[19]);
                }
                return;
             case 1 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[7]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[8]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[9]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[10]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[11]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[12]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[13]);
                }
                return;
       }
    }

    public String getDataStoreName( )
    {
       return "DATASTORE1";
    }

 }

}
