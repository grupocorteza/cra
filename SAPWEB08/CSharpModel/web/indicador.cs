/*
               File: Indicador
        Description: Indicador
             Author: GeneXus C# Generator version 16_0_0-127771
       Generated on: 1/5/2019 14:2:39.85
       Program type: Callable routine
          Main DBMS: SQL Server
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Xml.Serialization;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class indicador : GXDataArea, System.Web.SessionState.IRequiresSessionState
   {
      protected void INITENV( )
      {
         if ( GxWebError != 0 )
         {
            return  ;
         }
      }

      protected void INITTRN( )
      {
         initialize_properties( ) ;
         entryPointCalled = false;
         gxfirstwebparm = GetNextPar( );
         gxfirstwebparm_bkp = gxfirstwebparm;
         gxfirstwebparm = DecryptAjaxCall( gxfirstwebparm);
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         if ( StringUtil.StrCmp(gxfirstwebparm, "dyncall") == 0 )
         {
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            dyncall( GetNextPar( )) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxExecAct_"+"gxLoad_11") == 0 )
         {
            A25Proyectoid = (int)(NumberUtil.Val( GetNextPar( ), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A25Proyectoid", StringUtil.LTrim( StringUtil.Str( (decimal)(A25Proyectoid), 9, 0)));
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxLoad_11( A25Proyectoid) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxExecAct_"+"gxLoad_12") == 0 )
         {
            A15Paisid = (int)(NumberUtil.Val( GetNextPar( ), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A15Paisid", StringUtil.LTrim( StringUtil.Str( (decimal)(A15Paisid), 9, 0)));
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxLoad_12( A15Paisid) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxEvt") == 0 )
         {
            setAjaxEventMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxfirstwebparm = GetNextPar( );
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxfullajaxEvt") == 0 )
         {
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxfirstwebparm = GetNextPar( );
         }
         else
         {
            if ( ! IsValidAjaxCall( false) )
            {
               GxWebError = 1;
               return  ;
            }
            gxfirstwebparm = gxfirstwebparm_bkp;
         }
         if ( ! entryPointCalled && ! ( isAjaxCallMode( ) || isFullAjaxMode( ) ) )
         {
            Gx_mode = gxfirstwebparm;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!")), context));
            if ( StringUtil.StrCmp(gxfirstwebparm, "viewer") != 0 )
            {
               AV7Indicadorid = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV7Indicadorid", StringUtil.LTrim( StringUtil.Str( (decimal)(AV7Indicadorid), 9, 0)));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vINDICADORID", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV7Indicadorid), "ZZZZZZZZ9"), context));
            }
         }
         if ( context.isSpaRequest( ) )
         {
            enableJsOutput();
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.GetCookie( "GX_SESSION_ID"))) )
         {
            gxcookieaux = context.SetCookie( "GX_SESSION_ID", Encrypt64( Crypto.GetEncryptionKey( ), Crypto.GetServerKey( )), "", (DateTime)(DateTime.MinValue), "", 0);
         }
         GXKey = Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
         toggleJsOutput = isJsOutputEnabled( );
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         init_web_controls( ) ;
         if ( toggleJsOutput )
         {
            if ( context.isSpaRequest( ) )
            {
               enableJsOutput();
            }
         }
         if ( ! context.isSpaRequest( ) )
         {
            Form.Meta.addItem("generator", "GeneXus C# 16_0_0-127771", 0) ;
            Form.Meta.addItem("description", "Indicador", 0) ;
         }
         context.wjLoc = "";
         context.nUserReturn = 0;
         context.wbHandled = 0;
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
         }
         GX_FocusControl = edtresultado_Internalname;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         wbErr = false;
         context.SetDefaultTheme("Carmine");
         if ( ! context.IsLocalStorageSupported( ) )
         {
            context.PushCurrentUrl();
         }
      }

      public indicador( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsDataStore1 = context.GetDataStore("DataStore1");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("Carmine");
      }

      public indicador( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsDataStore1 = context.GetDataStore("DataStore1");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( String aP0_Gx_mode ,
                           int aP1_Indicadorid )
      {
         this.Gx_mode = aP0_Gx_mode;
         this.AV7Indicadorid = aP1_Indicadorid;
         executePrivate();
      }

      void executePrivate( )
      {
         isStatic = false;
         webExecute();
      }

      protected override void createObjects( )
      {
      }

      public override void webExecute( )
      {
         if ( initialized == 0 )
         {
            createObjects();
            initialize();
         }
         INITENV( ) ;
         INITTRN( ) ;
         if ( ( GxWebError == 0 ) && ! isAjaxCallMode( ) )
         {
            MasterPageObj = (GXMasterPage) ClassLoader.GetInstance("rwdmasterpage", "GeneXus.Programs.rwdmasterpage", new Object[] {new GxContext( context.handle, context.DataStores, context.HttpContext)});
            MasterPageObj.setDataArea(this,false);
            ValidateSpaRequest();
            MasterPageObj.webExecute();
            if ( ( GxWebError == 0 ) && context.isAjaxRequest( ) )
            {
               enableOutput();
               if ( ! context.isAjaxRequest( ) )
               {
                  context.GX_webresponse.AppendHeader("Cache-Control", "no-store");
               }
               if ( ! context.WillRedirect( ) )
               {
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               }
               else
               {
                  if ( context.isAjaxRequest( ) )
                  {
                     disableOutput();
                  }
                  RenderHtmlHeaders( ) ;
                  context.Redirect( context.wjLoc );
                  context.DispatchAjaxCommands();
               }
            }
         }
         this.cleanup();
      }

      protected void fix_multi_value_controls( )
      {
      }

      protected void Draw( )
      {
         if ( context.isAjaxRequest( ) )
         {
            disableOutput();
         }
         if ( ! GxWebStd.gx_redirect( context) )
         {
            disable_std_buttons( ) ;
            enableDisable( ) ;
            set_caption( ) ;
            /* Form start */
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "Section", "left", "top", " "+"data-gx-base-lib=\"bootstrapv3\""+" "+"data-abstract-form"+" ", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, divMaintable_Internalname, 1, 0, "px", 0, "px", "WWAdvancedContainer", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12 col-sm-8 col-sm-offset-2", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, divTitlecontainer_Internalname, 1, 0, "px", 0, "px", "TableTop", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12", "left", "top", "", "", "div");
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTitle_Internalname, "Indicador", "", "", lblTitle_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "Title", 0, "", 1, 1, 0, "HLP_Indicador.htm");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12", "left", "top", "", "", "div");
            ClassString = "ErrorViewer";
            StyleString = "";
            GxWebStd.gx_msg_list( context, "", context.GX_msglist.DisplayMode, StyleString, ClassString, "", "false");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12 col-sm-8 col-sm-offset-2", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, divFormcontainer_Internalname, 1, 0, "px", 0, "px", "FormContainer", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, divToolbarcell_Internalname, 1, 0, "px", 0, "px", "col-xs-12 col-sm-9 col-sm-offset-3 ToolbarCellClass", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "gx-action-group ActionGroup", "left", "top", " "+"data-gx-actiongroup-type=\"toolbar\""+" ", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "btn-group", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "gx-button", "left", "top", "", "", "div");
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 21,'',false,'',0)\"";
            ClassString = "BtnFirst";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtn_first_Internalname, "", "", bttBtn_first_Jsonclick, 5, "", "", StyleString, ClassString, bttBtn_first_Visible, 0, "standard", "'"+""+"'"+",false,"+"'"+"EFIRST."+"'", TempTags, "", context.GetButtonType( ), "HLP_Indicador.htm");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "gx-button", "left", "top", "", "", "div");
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 23,'',false,'',0)\"";
            ClassString = "BtnPrevious";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtn_previous_Internalname, "", "", bttBtn_previous_Jsonclick, 5, "", "", StyleString, ClassString, bttBtn_previous_Visible, 0, "standard", "'"+""+"'"+",false,"+"'"+"EPREVIOUS."+"'", TempTags, "", context.GetButtonType( ), "HLP_Indicador.htm");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "gx-button", "left", "top", "", "", "div");
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 25,'',false,'',0)\"";
            ClassString = "BtnNext";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtn_next_Internalname, "", "", bttBtn_next_Jsonclick, 5, "", "", StyleString, ClassString, bttBtn_next_Visible, 0, "standard", "'"+""+"'"+",false,"+"'"+"ENEXT."+"'", TempTags, "", context.GetButtonType( ), "HLP_Indicador.htm");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "gx-button", "left", "top", "", "", "div");
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 27,'',false,'',0)\"";
            ClassString = "BtnLast";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtn_last_Internalname, "", "", bttBtn_last_Jsonclick, 5, "", "", StyleString, ClassString, bttBtn_last_Visible, 0, "standard", "'"+""+"'"+",false,"+"'"+"ELAST."+"'", TempTags, "", context.GetButtonType( ), "HLP_Indicador.htm");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "gx-button", "left", "top", "", "", "div");
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 29,'',false,'',0)\"";
            ClassString = "BtnSelect";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtn_select_Internalname, "", "Select", bttBtn_select_Jsonclick, 5, "Select", "", StyleString, ClassString, bttBtn_select_Visible, 0, "standard", "'"+""+"'"+",false,"+"'"+"ESELECT."+"'", TempTags, "", 2, "HLP_Indicador.htm");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12 FormCellAdvanced", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "form-group gx-form-group", "left", "top", ""+" data-gx-for=\""+edtresultado_Internalname+"\"", "", "div");
            /* Attribute/Variable Label */
            GxWebStd.gx_label_element( context, edtresultado_Internalname, "resultado", "col-sm-3 AttributeLabel", 1, true);
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-sm-9 gx-attribute", "left", "top", "", "", "div");
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 34,'',false,'',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtresultado_Internalname, A62resultado, "", TempTags+" onchange=\""+""+";gx.evt.onchange(this, event)\" "+" onblur=\""+";gx.evt.onblur(this,34);\"", 0, 1, edtresultado_Enabled, 0, 80, "chr", 5, "row", StyleString, ClassString, "", "", "400", -1, 0, "", "", -1, true, "", "'"+""+"'"+",false,"+"'"+""+"'", 0, "HLP_Indicador.htm");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12 FormCell", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "form-group gx-form-group", "left", "top", ""+" data-gx-for=\""+edtobjetivo_Internalname+"\"", "", "div");
            /* Attribute/Variable Label */
            GxWebStd.gx_label_element( context, edtobjetivo_Internalname, "objetivo", "col-sm-3 AttributeLabel", 1, true);
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-sm-9 gx-attribute", "left", "top", "", "", "div");
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 39,'',false,'',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtobjetivo_Internalname, A63objetivo, "", TempTags+" onchange=\""+""+";gx.evt.onchange(this, event)\" "+" onblur=\""+";gx.evt.onblur(this,39);\"", 0, 1, edtobjetivo_Enabled, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "", "300", -1, 0, "", "", -1, true, "", "'"+""+"'"+",false,"+"'"+""+"'", 0, "HLP_Indicador.htm");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12 FormCell", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "form-group gx-form-group", "left", "top", ""+" data-gx-for=\""+edtobjPlan_Internalname+"\"", "", "div");
            /* Attribute/Variable Label */
            GxWebStd.gx_label_element( context, edtobjPlan_Internalname, "obj Plan", "col-sm-3 AttributeLabel", 1, true);
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-sm-9 gx-attribute", "left", "top", "", "", "div");
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 44,'',false,'',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtobjPlan_Internalname, A64objPlan, "", TempTags+" onchange=\""+""+";gx.evt.onchange(this, event)\" "+" onblur=\""+";gx.evt.onblur(this,44);\"", 0, 1, edtobjPlan_Enabled, 0, 80, "chr", 5, "row", StyleString, ClassString, "", "", "400", -1, 0, "", "", -1, true, "", "'"+""+"'"+",false,"+"'"+""+"'", 0, "HLP_Indicador.htm");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12 FormCell", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "form-group gx-form-group", "left", "top", ""+" data-gx-for=\""+edtProyectoid_Internalname+"\"", "", "div");
            /* Attribute/Variable Label */
            GxWebStd.gx_label_element( context, edtProyectoid_Internalname, "Proyectoid", "col-sm-3 AttributeLabel", 1, true);
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-sm-9 gx-attribute", "left", "top", "", "", "div");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 49,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtProyectoid_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A25Proyectoid), 9, 0, ".", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(A25Proyectoid), "ZZZZZZZZ9")), TempTags+" onchange=\""+"gx.num.valid_integer( this,',');"+";gx.evt.onchange(this, event)\" "+" onblur=\""+";gx.evt.onblur(this,49);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtProyectoid_Jsonclick, 0, "Attribute", "", "", "", "", 1, edtProyectoid_Enabled, 1, "number", "1", 9, "chr", 1, "row", 9, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_Indicador.htm");
            /* Static images/pictures */
            ClassString = "gx-prompt Image";
            StyleString = "";
            sImgUrl = (String)(context.GetImagePath( "prompt.gif", "", context.GetTheme( )));
            GxWebStd.gx_bitmap( context, imgprompt_25_Internalname, sImgUrl, imgprompt_25_Link, "", "", context.GetTheme( ), imgprompt_25_Visible, 1, "", "", 0, 0, 0, "", 0, "", 0, 0, 0, "", "", StyleString, ClassString, "", "", "", "", "", "", "", 1, false, false, context.GetImageSrcSet( sImgUrl), "HLP_Indicador.htm");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12 FormCell", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "form-group gx-form-group", "left", "top", ""+" data-gx-for=\""+edtProyectonombre_Internalname+"\"", "", "div");
            /* Attribute/Variable Label */
            GxWebStd.gx_label_element( context, edtProyectonombre_Internalname, "Proyectonombre", "col-sm-3 AttributeLabel", 1, true);
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-sm-9 gx-attribute", "left", "top", "", "", "div");
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtProyectonombre_Internalname, A74Proyectonombre, StringUtil.RTrim( context.localUtil.Format( A74Proyectonombre, "")), "", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtProyectonombre_Jsonclick, 0, "Attribute", "", "", "", "", 1, edtProyectonombre_Enabled, 0, "text", "", 80, "chr", 1, "row", 100, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_Indicador.htm");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12 FormCell", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "form-group gx-form-group", "left", "top", ""+" data-gx-for=\""+edtPaisid_Internalname+"\"", "", "div");
            /* Attribute/Variable Label */
            GxWebStd.gx_label_element( context, edtPaisid_Internalname, "Pa�s", "col-sm-3 AttributeLabel", 1, true);
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-sm-9 gx-attribute", "left", "top", "", "", "div");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 59,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtPaisid_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A15Paisid), 9, 0, ".", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(A15Paisid), "ZZZZZZZZ9")), TempTags+" onchange=\""+"gx.num.valid_integer( this,',');"+";gx.evt.onchange(this, event)\" "+" onblur=\""+";gx.evt.onblur(this,59);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtPaisid_Jsonclick, 0, "Attribute", "", "", "", "", 1, edtPaisid_Enabled, 1, "number", "1", 9, "chr", 1, "row", 9, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_Indicador.htm");
            /* Static images/pictures */
            ClassString = "gx-prompt Image";
            StyleString = "";
            sImgUrl = (String)(context.GetImagePath( "prompt.gif", "", context.GetTheme( )));
            GxWebStd.gx_bitmap( context, imgprompt_15_Internalname, sImgUrl, imgprompt_15_Link, "", "", context.GetTheme( ), imgprompt_15_Visible, 1, "", "", 0, 0, 0, "", 0, "", 0, 0, 0, "", "", StyleString, ClassString, "", "", "", "", "", "", "", 1, false, false, context.GetImageSrcSet( sImgUrl), "HLP_Indicador.htm");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12 FormCell", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "form-group gx-form-group", "left", "top", ""+" data-gx-for=\""+edtPaisnombre_Internalname+"\"", "", "div");
            /* Attribute/Variable Label */
            GxWebStd.gx_label_element( context, edtPaisnombre_Internalname, "Pa�s", "col-sm-3 AttributeLabel", 1, true);
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-sm-9 gx-attribute", "left", "top", "", "", "div");
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtPaisnombre_Internalname, A118Paisnombre, StringUtil.RTrim( context.localUtil.Format( A118Paisnombre, "")), "", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtPaisnombre_Jsonclick, 0, "Attribute", "", "", "", "", 1, edtPaisnombre_Enabled, 0, "text", "", 40, "chr", 1, "row", 40, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_Indicador.htm");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12 FormCell", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "form-group gx-form-group", "left", "top", ""+" data-gx-for=\""+edtaccion_Internalname+"\"", "", "div");
            /* Attribute/Variable Label */
            GxWebStd.gx_label_element( context, edtaccion_Internalname, "accion", "col-sm-3 AttributeLabel", 1, true);
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-sm-9 gx-attribute", "left", "top", "", "", "div");
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 69,'',false,'',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtaccion_Internalname, A65accion, "", TempTags+" onchange=\""+""+";gx.evt.onchange(this, event)\" "+" onblur=\""+";gx.evt.onblur(this,69);\"", 0, 1, edtaccion_Enabled, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "", "300", -1, 0, "", "", -1, true, "", "'"+""+"'"+",false,"+"'"+""+"'", 0, "HLP_Indicador.htm");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12 FormCell", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "form-group gx-form-group", "left", "top", ""+" data-gx-for=\""+edtaccionPlan_Internalname+"\"", "", "div");
            /* Attribute/Variable Label */
            GxWebStd.gx_label_element( context, edtaccionPlan_Internalname, "accion Plan", "col-sm-3 AttributeLabel", 1, true);
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-sm-9 gx-attribute", "left", "top", "", "", "div");
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 74,'',false,'',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtaccionPlan_Internalname, A66accionPlan, "", TempTags+" onchange=\""+""+";gx.evt.onchange(this, event)\" "+" onblur=\""+";gx.evt.onblur(this,74);\"", 0, 1, edtaccionPlan_Enabled, 0, 80, "chr", 5, "row", StyleString, ClassString, "", "", "400", -1, 0, "", "", -1, true, "", "'"+""+"'"+",false,"+"'"+""+"'", 0, "HLP_Indicador.htm");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12 FormCell", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "form-group gx-form-group", "left", "top", ""+" data-gx-for=\""+edtdonor_Internalname+"\"", "", "div");
            /* Attribute/Variable Label */
            GxWebStd.gx_label_element( context, edtdonor_Internalname, "donor", "col-sm-3 AttributeLabel", 1, true);
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-sm-9 gx-attribute", "left", "top", "", "", "div");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 79,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtdonor_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A67donor), 1, 0, ".", "")), ((edtdonor_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( (decimal)(A67donor), "9")) : context.localUtil.Format( (decimal)(A67donor), "9")), TempTags+" onchange=\""+"gx.num.valid_integer( this,',');"+";gx.evt.onchange(this, event)\" "+" onblur=\""+";gx.evt.onblur(this,79);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtdonor_Jsonclick, 0, "Attribute", "", "", "", "", 1, edtdonor_Enabled, 0, "number", "1", 1, "chr", 1, "row", 1, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_Indicador.htm");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12 FormCell", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "form-group gx-form-group", "left", "top", ""+" data-gx-for=\""+edtmilestone_Internalname+"\"", "", "div");
            /* Attribute/Variable Label */
            GxWebStd.gx_label_element( context, edtmilestone_Internalname, "milestone", "col-sm-3 AttributeLabel", 1, true);
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-sm-9 gx-attribute", "left", "top", "", "", "div");
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 84,'',false,'',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtmilestone_Internalname, A68milestone, "", TempTags+" onchange=\""+""+";gx.evt.onchange(this, event)\" "+" onblur=\""+";gx.evt.onblur(this,84);\"", 0, 1, edtmilestone_Enabled, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "", "300", -1, 0, "", "", -1, true, "", "'"+""+"'"+",false,"+"'"+""+"'", 0, "HLP_Indicador.htm");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12", "Center", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "gx-action-group Confirm", "left", "top", " "+"data-gx-actiongroup-type=\"toolbar\""+" ", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "gx-button", "left", "top", "", "", "div");
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 89,'',false,'',0)\"";
            ClassString = "BtnEnter";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtn_enter_Internalname, "", "Confirm", bttBtn_enter_Jsonclick, 5, "Confirm", "", StyleString, ClassString, bttBtn_enter_Visible, bttBtn_enter_Enabled, "standard", "'"+""+"'"+",false,"+"'"+"EENTER."+"'", TempTags, "", context.GetButtonType( ), "HLP_Indicador.htm");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "gx-button", "left", "top", "", "", "div");
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 91,'',false,'',0)\"";
            ClassString = "BtnCancel";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtn_cancel_Internalname, "", "Cancel", bttBtn_cancel_Jsonclick, 1, "Cancel", "", StyleString, ClassString, bttBtn_cancel_Visible, 1, "standard", "'"+""+"'"+",false,"+"'"+"ECANCEL."+"'", TempTags, "", context.GetButtonType( ), "HLP_Indicador.htm");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "gx-button", "left", "top", "", "", "div");
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 93,'',false,'',0)\"";
            ClassString = "BtnDelete";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtn_delete_Internalname, "", "Delete", bttBtn_delete_Jsonclick, 5, "Delete", "", StyleString, ClassString, bttBtn_delete_Visible, bttBtn_delete_Enabled, "standard", "'"+""+"'"+",false,"+"'"+"EDELETE."+"'", TempTags, "", context.GetButtonType( ), "HLP_Indicador.htm");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "Center", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            fix_multi_value_controls( ) ;
         }
         /* Execute Exit event if defined. */
      }

      protected void UserMain( )
      {
         standaloneStartup( ) ;
      }

      protected void UserMainFullajax( )
      {
         INITENV( ) ;
         INITTRN( ) ;
         UserMain( ) ;
         Draw( ) ;
         SendCloseFormHiddens( ) ;
      }

      protected void standaloneStartup( )
      {
         standaloneStartupServer( ) ;
         disable_std_buttons( ) ;
         enableDisable( ) ;
         Process( ) ;
      }

      protected void standaloneStartupServer( )
      {
         /* Execute Start event if defined. */
         context.wbGlbDoneStart = 0;
         /* Execute user event: Start */
         E11092 ();
         context.wbGlbDoneStart = 1;
         assign_properties_default( ) ;
         if ( AnyError == 0 )
         {
            if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
            {
               /* Read saved SDTs. */
               /* Read variables values. */
               A62resultado = cgiGet( edtresultado_Internalname);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A62resultado", A62resultado);
               A63objetivo = cgiGet( edtobjetivo_Internalname);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A63objetivo", A63objetivo);
               A64objPlan = cgiGet( edtobjPlan_Internalname);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A64objPlan", A64objPlan);
               if ( ( ( context.localUtil.CToN( cgiGet( edtProyectoid_Internalname), ".", ",") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtProyectoid_Internalname), ".", ",") > Convert.ToDecimal( 999999999 )) ) )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "PROYECTOID");
                  AnyError = 1;
                  GX_FocusControl = edtProyectoid_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  wbErr = true;
                  A25Proyectoid = 0;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A25Proyectoid", StringUtil.LTrim( StringUtil.Str( (decimal)(A25Proyectoid), 9, 0)));
               }
               else
               {
                  A25Proyectoid = (int)(context.localUtil.CToN( cgiGet( edtProyectoid_Internalname), ".", ","));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A25Proyectoid", StringUtil.LTrim( StringUtil.Str( (decimal)(A25Proyectoid), 9, 0)));
               }
               A74Proyectonombre = cgiGet( edtProyectonombre_Internalname);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A74Proyectonombre", A74Proyectonombre);
               if ( ( ( context.localUtil.CToN( cgiGet( edtPaisid_Internalname), ".", ",") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtPaisid_Internalname), ".", ",") > Convert.ToDecimal( 999999999 )) ) )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "PAISID");
                  AnyError = 1;
                  GX_FocusControl = edtPaisid_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  wbErr = true;
                  A15Paisid = 0;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A15Paisid", StringUtil.LTrim( StringUtil.Str( (decimal)(A15Paisid), 9, 0)));
               }
               else
               {
                  A15Paisid = (int)(context.localUtil.CToN( cgiGet( edtPaisid_Internalname), ".", ","));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A15Paisid", StringUtil.LTrim( StringUtil.Str( (decimal)(A15Paisid), 9, 0)));
               }
               A118Paisnombre = cgiGet( edtPaisnombre_Internalname);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A118Paisnombre", A118Paisnombre);
               A65accion = cgiGet( edtaccion_Internalname);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A65accion", A65accion);
               A66accionPlan = cgiGet( edtaccionPlan_Internalname);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A66accionPlan", A66accionPlan);
               if ( ( ( context.localUtil.CToN( cgiGet( edtdonor_Internalname), ".", ",") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtdonor_Internalname), ".", ",") > Convert.ToDecimal( 9 )) ) )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "DONOR");
                  AnyError = 1;
                  GX_FocusControl = edtdonor_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  wbErr = true;
                  A67donor = 0;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A67donor", StringUtil.Str( (decimal)(A67donor), 1, 0));
               }
               else
               {
                  A67donor = (short)(context.localUtil.CToN( cgiGet( edtdonor_Internalname), ".", ","));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A67donor", StringUtil.Str( (decimal)(A67donor), 1, 0));
               }
               A68milestone = cgiGet( edtmilestone_Internalname);
               n68milestone = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A68milestone", A68milestone);
               n68milestone = (String.IsNullOrEmpty(StringUtil.RTrim( A68milestone)) ? true : false);
               /* Read saved values. */
               Z4Indicadorid = (int)(context.localUtil.CToN( cgiGet( "Z4Indicadorid"), ".", ","));
               Z62resultado = cgiGet( "Z62resultado");
               Z63objetivo = cgiGet( "Z63objetivo");
               Z64objPlan = cgiGet( "Z64objPlan");
               Z65accion = cgiGet( "Z65accion");
               Z66accionPlan = cgiGet( "Z66accionPlan");
               Z67donor = (short)(context.localUtil.CToN( cgiGet( "Z67donor"), ".", ","));
               Z68milestone = cgiGet( "Z68milestone");
               n68milestone = (String.IsNullOrEmpty(StringUtil.RTrim( A68milestone)) ? true : false);
               Z25Proyectoid = (int)(context.localUtil.CToN( cgiGet( "Z25Proyectoid"), ".", ","));
               Z15Paisid = (int)(context.localUtil.CToN( cgiGet( "Z15Paisid"), ".", ","));
               IsConfirmed = (short)(context.localUtil.CToN( cgiGet( "IsConfirmed"), ".", ","));
               IsModified = (short)(context.localUtil.CToN( cgiGet( "IsModified"), ".", ","));
               Gx_mode = cgiGet( "Mode");
               N25Proyectoid = (int)(context.localUtil.CToN( cgiGet( "N25Proyectoid"), ".", ","));
               N15Paisid = (int)(context.localUtil.CToN( cgiGet( "N15Paisid"), ".", ","));
               AV7Indicadorid = (int)(context.localUtil.CToN( cgiGet( "vINDICADORID"), ".", ","));
               A4Indicadorid = (int)(context.localUtil.CToN( cgiGet( "INDICADORID"), ".", ","));
               AV11Insert_Proyectoid = (int)(context.localUtil.CToN( cgiGet( "vINSERT_PROYECTOID"), ".", ","));
               AV12Insert_Paisid = (int)(context.localUtil.CToN( cgiGet( "vINSERT_PAISID"), ".", ","));
               AV14Pgmname = cgiGet( "vPGMNAME");
               Gx_mode = cgiGet( "vMODE");
               /* Read subfile selected row values. */
               /* Read hidden variables. */
               GXKey = Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
               forbiddenHiddens = "hsh" + "Indicador";
               forbiddenHiddens = forbiddenHiddens + context.localUtil.Format( (decimal)(AV11Insert_Proyectoid), "ZZZZZZZZ9");
               forbiddenHiddens = forbiddenHiddens + context.localUtil.Format( (decimal)(AV12Insert_Paisid), "ZZZZZZZZ9");
               forbiddenHiddens = forbiddenHiddens + StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"));
               forbiddenHiddens = forbiddenHiddens + context.localUtil.Format( (decimal)(A4Indicadorid), "ZZZZZZZZ9");
               hsh = cgiGet( "hsh");
               if ( ! GXUtil.CheckEncryptedHash( forbiddenHiddens, hsh, GXKey) )
               {
                  GXUtil.WriteLog("indicador:[SecurityCheckFailed value for]"+"Insert_Proyectoid:"+context.localUtil.Format( (decimal)(AV11Insert_Proyectoid), "ZZZZZZZZ9"));
                  GXUtil.WriteLog("indicador:[SecurityCheckFailed value for]"+"Insert_Paisid:"+context.localUtil.Format( (decimal)(AV12Insert_Paisid), "ZZZZZZZZ9"));
                  GXUtil.WriteLog("indicador:[SecurityCheckFailed value for]"+"Gx_mode:"+StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!")));
                  GXUtil.WriteLog("indicador:[SecurityCheckFailed value for]"+"Indicadorid:"+context.localUtil.Format( (decimal)(A4Indicadorid), "ZZZZZZZZ9"));
                  GxWebError = 1;
                  context.HttpContext.Response.StatusDescription = 403.ToString();
                  context.HttpContext.Response.StatusCode = 403;
                  context.WriteHtmlText( "<title>403 Forbidden</title>") ;
                  context.WriteHtmlText( "<h1>403 Forbidden</h1>") ;
                  context.WriteHtmlText( "<p /><hr />") ;
                  GXUtil.WriteLog("send_http_error_code " + 403.ToString());
                  AnyError = 1;
                  return  ;
               }
               standaloneNotModal( ) ;
            }
            else
            {
               standaloneNotModal( ) ;
               if ( StringUtil.StrCmp(gxfirstwebparm, "viewer") == 0 )
               {
                  Gx_mode = "DSP";
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!")), context));
                  A4Indicadorid = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A4Indicadorid", StringUtil.LTrim( StringUtil.Str( (decimal)(A4Indicadorid), 9, 0)));
                  getEqualNoModal( ) ;
                  Gx_mode = "DSP";
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!")), context));
                  disable_std_buttons( ) ;
                  standaloneModal( ) ;
               }
               else
               {
                  if ( StringUtil.StrCmp(Gx_mode, "DSP") == 0 )
                  {
                     sMode9 = Gx_mode;
                     Gx_mode = "UPD";
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!")), context));
                     Gx_mode = sMode9;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!")), context));
                  }
                  standaloneModal( ) ;
                  if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
                  {
                     getByPrimaryKey( ) ;
                     if ( RcdFound9 == 1 )
                     {
                        if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
                        {
                           /* Confirm record */
                           CONFIRM_090( ) ;
                           if ( AnyError == 0 )
                           {
                              GX_FocusControl = bttBtn_enter_Internalname;
                              context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                           }
                        }
                     }
                     else
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_noinsert", ""), 1, "");
                        AnyError = 1;
                     }
                  }
               }
            }
         }
      }

      protected void Process( )
      {
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            /* Read Transaction buttons. */
            sEvt = cgiGet( "_EventName");
            EvtGridId = cgiGet( "_EventGridId");
            EvtRowId = cgiGet( "_EventRowId");
            if ( StringUtil.Len( sEvt) > 0 )
            {
               sEvtType = StringUtil.Left( sEvt, 1);
               sEvt = StringUtil.Right( sEvt, (short)(StringUtil.Len( sEvt)-1));
               if ( StringUtil.StrCmp(sEvtType, "M") != 0 )
               {
                  if ( StringUtil.StrCmp(sEvtType, "E") == 0 )
                  {
                     sEvtType = StringUtil.Right( sEvt, 1);
                     if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                     {
                        sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                        if ( StringUtil.StrCmp(sEvt, "START") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: Start */
                           E11092 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "AFTER TRN") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: After Trn */
                           E12092 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "ENTER") == 0 )
                        {
                           context.wbHandled = 1;
                           if ( StringUtil.StrCmp(Gx_mode, "DSP") != 0 )
                           {
                              btn_enter( ) ;
                           }
                           /* No code required for Cancel button. It is implemented as the Reset button. */
                        }
                     }
                     else
                     {
                     }
                  }
                  context.wbHandled = 1;
               }
            }
         }
      }

      protected void AfterTrn( )
      {
         if ( trnEnded == 1 )
         {
            /* Execute user event: After Trn */
            E12092 ();
            trnEnded = 0;
            standaloneNotModal( ) ;
            standaloneModal( ) ;
            if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  )
            {
               /* Clear variables for new insertion. */
               InitAll099( ) ;
               standaloneNotModal( ) ;
               standaloneModal( ) ;
            }
         }
      }

      public override String ToString( )
      {
         return "" ;
      }

      public GxContentInfo GetContentInfo( )
      {
         return (GxContentInfo)(null) ;
      }

      protected void disable_std_buttons( )
      {
         bttBtn_delete_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_delete_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_delete_Visible), 5, 0)), true);
         bttBtn_first_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_first_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_first_Visible), 5, 0)), true);
         bttBtn_previous_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_previous_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_previous_Visible), 5, 0)), true);
         bttBtn_next_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_next_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_next_Visible), 5, 0)), true);
         bttBtn_last_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_last_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_last_Visible), 5, 0)), true);
         bttBtn_select_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_select_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_select_Visible), 5, 0)), true);
         if ( ( StringUtil.StrCmp(Gx_mode, "DSP") == 0 ) || ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 ) )
         {
            bttBtn_delete_Visible = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_delete_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_delete_Visible), 5, 0)), true);
            if ( StringUtil.StrCmp(Gx_mode, "DSP") == 0 )
            {
               bttBtn_enter_Visible = 0;
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_enter_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_enter_Visible), 5, 0)), true);
            }
            DisableAttributes099( ) ;
         }
      }

      protected void set_caption( )
      {
         if ( ( IsConfirmed == 1 ) && ( AnyError == 0 ) )
         {
            if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_confdelete", ""), 0, "", true);
            }
            else
            {
               GX_msglist.addItem(context.GetMessage( "GXM_mustconfirm", ""), 0, "", true);
            }
         }
      }

      protected void CONFIRM_090( )
      {
         BeforeValidate099( ) ;
         if ( AnyError == 0 )
         {
            if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
            {
               OnDeleteControls099( ) ;
            }
            else
            {
               CheckExtendedTable099( ) ;
               CloseExtendedTableCursors099( ) ;
            }
         }
         if ( AnyError == 0 )
         {
            IsConfirmed = 1;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "IsConfirmed", StringUtil.LTrim( StringUtil.Str( (decimal)(IsConfirmed), 4, 0)));
         }
      }

      protected void ResetCaption090( )
      {
      }

      protected void E11092( )
      {
         /* Start Routine */
         if ( ! new isauthorized(context).executeUdp(  AV14Pgmname) )
         {
            CallWebObject(formatLink("notauthorized.aspx") + "?" + UrlEncode(StringUtil.RTrim(AV14Pgmname)));
            context.wjLocDisableFrm = 1;
         }
         AV9TrnContext.FromXml(AV10WebSession.Get("TrnContext"), null, "TransactionContext", "SAPWEB08");
         AV11Insert_Proyectoid = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV11Insert_Proyectoid", StringUtil.LTrim( StringUtil.Str( (decimal)(AV11Insert_Proyectoid), 9, 0)));
         AV12Insert_Paisid = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV12Insert_Paisid", StringUtil.LTrim( StringUtil.Str( (decimal)(AV12Insert_Paisid), 9, 0)));
         if ( ( StringUtil.StrCmp(AV9TrnContext.gxTpr_Transactionname, AV14Pgmname) == 0 ) && ( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) )
         {
            AV15GXV1 = 1;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15GXV1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV15GXV1), 8, 0)));
            while ( AV15GXV1 <= AV9TrnContext.gxTpr_Attributes.Count )
            {
               AV13TrnContextAtt = ((SdtTransactionContext_Attribute)AV9TrnContext.gxTpr_Attributes.Item(AV15GXV1));
               if ( StringUtil.StrCmp(AV13TrnContextAtt.gxTpr_Attributename, "Proyectoid") == 0 )
               {
                  AV11Insert_Proyectoid = (int)(NumberUtil.Val( AV13TrnContextAtt.gxTpr_Attributevalue, "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV11Insert_Proyectoid", StringUtil.LTrim( StringUtil.Str( (decimal)(AV11Insert_Proyectoid), 9, 0)));
               }
               else if ( StringUtil.StrCmp(AV13TrnContextAtt.gxTpr_Attributename, "Paisid") == 0 )
               {
                  AV12Insert_Paisid = (int)(NumberUtil.Val( AV13TrnContextAtt.gxTpr_Attributevalue, "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV12Insert_Paisid", StringUtil.LTrim( StringUtil.Str( (decimal)(AV12Insert_Paisid), 9, 0)));
               }
               AV15GXV1 = (int)(AV15GXV1+1);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15GXV1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV15GXV1), 8, 0)));
            }
         }
      }

      protected void E12092( )
      {
         /* After Trn Routine */
         if ( ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 ) && ! AV9TrnContext.gxTpr_Callerondelete )
         {
            CallWebObject(formatLink("wwindicador.aspx") );
            context.wjLocDisableFrm = 1;
         }
         context.setWebReturnParms(new Object[] {});
         context.setWebReturnParmsMetadata(new Object[] {});
         context.wjLocDisableFrm = 1;
         context.nUserReturn = 1;
         returnInSub = true;
         if (true) return;
      }

      protected void ZM099( short GX_JID )
      {
         if ( ( GX_JID == 10 ) || ( GX_JID == 0 ) )
         {
            if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
            {
               Z62resultado = T00093_A62resultado[0];
               Z63objetivo = T00093_A63objetivo[0];
               Z64objPlan = T00093_A64objPlan[0];
               Z65accion = T00093_A65accion[0];
               Z66accionPlan = T00093_A66accionPlan[0];
               Z67donor = T00093_A67donor[0];
               Z68milestone = T00093_A68milestone[0];
               Z25Proyectoid = T00093_A25Proyectoid[0];
               Z15Paisid = T00093_A15Paisid[0];
            }
            else
            {
               Z62resultado = A62resultado;
               Z63objetivo = A63objetivo;
               Z64objPlan = A64objPlan;
               Z65accion = A65accion;
               Z66accionPlan = A66accionPlan;
               Z67donor = A67donor;
               Z68milestone = A68milestone;
               Z25Proyectoid = A25Proyectoid;
               Z15Paisid = A15Paisid;
            }
         }
         if ( GX_JID == -10 )
         {
            Z4Indicadorid = A4Indicadorid;
            Z62resultado = A62resultado;
            Z63objetivo = A63objetivo;
            Z64objPlan = A64objPlan;
            Z65accion = A65accion;
            Z66accionPlan = A66accionPlan;
            Z67donor = A67donor;
            Z68milestone = A68milestone;
            Z25Proyectoid = A25Proyectoid;
            Z15Paisid = A15Paisid;
            Z74Proyectonombre = A74Proyectonombre;
            Z118Paisnombre = A118Paisnombre;
         }
      }

      protected void standaloneNotModal( )
      {
         imgprompt_25_Link = ((StringUtil.StrCmp(Gx_mode, "DSP")==0) ? "" : "javascript:"+"gx.popup.openPrompt('"+"gx00p0.aspx"+"',["+"{Ctrl:gx.dom.el('"+"PROYECTOID"+"'), id:'"+"PROYECTOID"+"'"+",IOType:'out'}"+"],"+"null"+","+"'', false"+","+"false"+");");
         imgprompt_15_Link = ((StringUtil.StrCmp(Gx_mode, "DSP")==0) ? "" : "javascript:"+"gx.popup.openPrompt('"+"gx00d0.aspx"+"',["+"{Ctrl:gx.dom.el('"+"PAISID"+"'), id:'"+"PAISID"+"'"+",IOType:'out'}"+"],"+"null"+","+"'', false"+","+"false"+");");
         bttBtn_delete_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_delete_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_delete_Enabled), 5, 0)), true);
         if ( ! (0==AV7Indicadorid) )
         {
            A4Indicadorid = AV7Indicadorid;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A4Indicadorid", StringUtil.LTrim( StringUtil.Str( (decimal)(A4Indicadorid), 9, 0)));
         }
         if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) && ! (0==AV11Insert_Proyectoid) )
         {
            edtProyectoid_Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtProyectoid_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtProyectoid_Enabled), 5, 0)), true);
         }
         else
         {
            edtProyectoid_Enabled = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtProyectoid_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtProyectoid_Enabled), 5, 0)), true);
         }
         if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) && ! (0==AV12Insert_Paisid) )
         {
            edtPaisid_Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtPaisid_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtPaisid_Enabled), 5, 0)), true);
         }
         else
         {
            edtPaisid_Enabled = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtPaisid_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtPaisid_Enabled), 5, 0)), true);
         }
      }

      protected void standaloneModal( )
      {
         if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) && ! (0==AV12Insert_Paisid) )
         {
            A15Paisid = AV12Insert_Paisid;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A15Paisid", StringUtil.LTrim( StringUtil.Str( (decimal)(A15Paisid), 9, 0)));
         }
         if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) && ! (0==AV11Insert_Proyectoid) )
         {
            A25Proyectoid = AV11Insert_Proyectoid;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A25Proyectoid", StringUtil.LTrim( StringUtil.Str( (decimal)(A25Proyectoid), 9, 0)));
         }
         if ( StringUtil.StrCmp(Gx_mode, "DSP") == 0 )
         {
            bttBtn_enter_Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_enter_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_enter_Enabled), 5, 0)), true);
         }
         else
         {
            bttBtn_enter_Enabled = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_enter_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_enter_Enabled), 5, 0)), true);
         }
         if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) && ( Gx_BScreen == 0 ) )
         {
            AV14Pgmname = "Indicador";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14Pgmname", AV14Pgmname);
            /* Using cursor T00095 */
            pr_datastore1.execute(3, new Object[] {A15Paisid});
            A118Paisnombre = T00095_A118Paisnombre[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A118Paisnombre", A118Paisnombre);
            pr_datastore1.close(3);
            /* Using cursor T00094 */
            pr_datastore1.execute(2, new Object[] {A25Proyectoid});
            A74Proyectonombre = T00094_A74Proyectonombre[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A74Proyectonombre", A74Proyectonombre);
            pr_datastore1.close(2);
         }
      }

      protected void Load099( )
      {
         /* Using cursor T00096 */
         pr_datastore1.execute(4, new Object[] {A4Indicadorid});
         if ( (pr_datastore1.getStatus(4) != 101) )
         {
            RcdFound9 = 1;
            A62resultado = T00096_A62resultado[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A62resultado", A62resultado);
            A63objetivo = T00096_A63objetivo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A63objetivo", A63objetivo);
            A64objPlan = T00096_A64objPlan[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A64objPlan", A64objPlan);
            A74Proyectonombre = T00096_A74Proyectonombre[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A74Proyectonombre", A74Proyectonombre);
            A118Paisnombre = T00096_A118Paisnombre[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A118Paisnombre", A118Paisnombre);
            A65accion = T00096_A65accion[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A65accion", A65accion);
            A66accionPlan = T00096_A66accionPlan[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A66accionPlan", A66accionPlan);
            A67donor = T00096_A67donor[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A67donor", StringUtil.Str( (decimal)(A67donor), 1, 0));
            A68milestone = T00096_A68milestone[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A68milestone", A68milestone);
            n68milestone = T00096_n68milestone[0];
            A25Proyectoid = T00096_A25Proyectoid[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A25Proyectoid", StringUtil.LTrim( StringUtil.Str( (decimal)(A25Proyectoid), 9, 0)));
            A15Paisid = T00096_A15Paisid[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A15Paisid", StringUtil.LTrim( StringUtil.Str( (decimal)(A15Paisid), 9, 0)));
            ZM099( -10) ;
         }
         pr_datastore1.close(4);
         OnLoadActions099( ) ;
      }

      protected void OnLoadActions099( )
      {
         AV14Pgmname = "Indicador";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14Pgmname", AV14Pgmname);
      }

      protected void CheckExtendedTable099( )
      {
         Gx_BScreen = 1;
         standaloneModal( ) ;
         AV14Pgmname = "Indicador";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14Pgmname", AV14Pgmname);
         /* Using cursor T00094 */
         pr_datastore1.execute(2, new Object[] {A25Proyectoid});
         if ( (pr_datastore1.getStatus(2) == 101) )
         {
            GX_msglist.addItem("No matching 'Proyecto'.", "ForeignKeyNotFound", 1, "PROYECTOID");
            AnyError = 1;
            GX_FocusControl = edtProyectoid_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         A74Proyectonombre = T00094_A74Proyectonombre[0];
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A74Proyectonombre", A74Proyectonombre);
         pr_datastore1.close(2);
         /* Using cursor T00095 */
         pr_datastore1.execute(3, new Object[] {A15Paisid});
         if ( (pr_datastore1.getStatus(3) == 101) )
         {
            GX_msglist.addItem("No matching 'Pais'.", "ForeignKeyNotFound", 1, "PAISID");
            AnyError = 1;
            GX_FocusControl = edtPaisid_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         A118Paisnombre = T00095_A118Paisnombre[0];
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A118Paisnombre", A118Paisnombre);
         pr_datastore1.close(3);
      }

      protected void CloseExtendedTableCursors099( )
      {
         pr_datastore1.close(2);
         pr_datastore1.close(3);
      }

      protected void enableDisable( )
      {
      }

      protected void gxLoad_11( int A25Proyectoid )
      {
         /* Using cursor T00097 */
         pr_datastore1.execute(5, new Object[] {A25Proyectoid});
         if ( (pr_datastore1.getStatus(5) == 101) )
         {
            GX_msglist.addItem("No matching 'Proyecto'.", "ForeignKeyNotFound", 1, "PROYECTOID");
            AnyError = 1;
            GX_FocusControl = edtProyectoid_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         A74Proyectonombre = T00097_A74Proyectonombre[0];
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A74Proyectonombre", A74Proyectonombre);
         GxWebStd.set_html_headers( context, 0, "", "");
         context.GX_webresponse.AddString("[[");
         context.GX_webresponse.AddString("\""+GXUtil.EncodeJSConstant( A74Proyectonombre)+"\"");
         context.GX_webresponse.AddString("]");
         if ( (pr_datastore1.getStatus(5) == 101) )
         {
            context.GX_webresponse.AddString(",");
            context.GX_webresponse.AddString("101");
         }
         context.GX_webresponse.AddString("]");
         pr_datastore1.close(5);
      }

      protected void gxLoad_12( int A15Paisid )
      {
         /* Using cursor T00098 */
         pr_datastore1.execute(6, new Object[] {A15Paisid});
         if ( (pr_datastore1.getStatus(6) == 101) )
         {
            GX_msglist.addItem("No matching 'Pais'.", "ForeignKeyNotFound", 1, "PAISID");
            AnyError = 1;
            GX_FocusControl = edtPaisid_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         A118Paisnombre = T00098_A118Paisnombre[0];
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A118Paisnombre", A118Paisnombre);
         GxWebStd.set_html_headers( context, 0, "", "");
         context.GX_webresponse.AddString("[[");
         context.GX_webresponse.AddString("\""+GXUtil.EncodeJSConstant( A118Paisnombre)+"\"");
         context.GX_webresponse.AddString("]");
         if ( (pr_datastore1.getStatus(6) == 101) )
         {
            context.GX_webresponse.AddString(",");
            context.GX_webresponse.AddString("101");
         }
         context.GX_webresponse.AddString("]");
         pr_datastore1.close(6);
      }

      protected void GetKey099( )
      {
         /* Using cursor T00099 */
         pr_datastore1.execute(7, new Object[] {A4Indicadorid});
         if ( (pr_datastore1.getStatus(7) != 101) )
         {
            RcdFound9 = 1;
         }
         else
         {
            RcdFound9 = 0;
         }
         pr_datastore1.close(7);
      }

      protected void getByPrimaryKey( )
      {
         /* Using cursor T00093 */
         pr_datastore1.execute(1, new Object[] {A4Indicadorid});
         if ( (pr_datastore1.getStatus(1) != 101) )
         {
            ZM099( 10) ;
            RcdFound9 = 1;
            A4Indicadorid = T00093_A4Indicadorid[0];
            A62resultado = T00093_A62resultado[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A62resultado", A62resultado);
            A63objetivo = T00093_A63objetivo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A63objetivo", A63objetivo);
            A64objPlan = T00093_A64objPlan[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A64objPlan", A64objPlan);
            A65accion = T00093_A65accion[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A65accion", A65accion);
            A66accionPlan = T00093_A66accionPlan[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A66accionPlan", A66accionPlan);
            A67donor = T00093_A67donor[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A67donor", StringUtil.Str( (decimal)(A67donor), 1, 0));
            A68milestone = T00093_A68milestone[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A68milestone", A68milestone);
            n68milestone = T00093_n68milestone[0];
            A25Proyectoid = T00093_A25Proyectoid[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A25Proyectoid", StringUtil.LTrim( StringUtil.Str( (decimal)(A25Proyectoid), 9, 0)));
            A15Paisid = T00093_A15Paisid[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A15Paisid", StringUtil.LTrim( StringUtil.Str( (decimal)(A15Paisid), 9, 0)));
            Z4Indicadorid = A4Indicadorid;
            sMode9 = Gx_mode;
            Gx_mode = "DSP";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!")), context));
            Load099( ) ;
            if ( AnyError == 1 )
            {
               RcdFound9 = 0;
               InitializeNonKey099( ) ;
            }
            Gx_mode = sMode9;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!")), context));
         }
         else
         {
            RcdFound9 = 0;
            InitializeNonKey099( ) ;
            sMode9 = Gx_mode;
            Gx_mode = "DSP";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!")), context));
            standaloneModal( ) ;
            Gx_mode = sMode9;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!")), context));
         }
         pr_datastore1.close(1);
      }

      protected void getEqualNoModal( )
      {
         GetKey099( ) ;
         if ( RcdFound9 == 0 )
         {
         }
         else
         {
         }
         getByPrimaryKey( ) ;
      }

      protected void move_next( )
      {
         RcdFound9 = 0;
         /* Using cursor T000910 */
         pr_datastore1.execute(8, new Object[] {A4Indicadorid});
         if ( (pr_datastore1.getStatus(8) != 101) )
         {
            while ( (pr_datastore1.getStatus(8) != 101) && ( ( T000910_A4Indicadorid[0] < A4Indicadorid ) ) )
            {
               pr_datastore1.readNext(8);
            }
            if ( (pr_datastore1.getStatus(8) != 101) && ( ( T000910_A4Indicadorid[0] > A4Indicadorid ) ) )
            {
               A4Indicadorid = T000910_A4Indicadorid[0];
               RcdFound9 = 1;
            }
         }
         pr_datastore1.close(8);
      }

      protected void move_previous( )
      {
         RcdFound9 = 0;
         /* Using cursor T000911 */
         pr_datastore1.execute(9, new Object[] {A4Indicadorid});
         if ( (pr_datastore1.getStatus(9) != 101) )
         {
            while ( (pr_datastore1.getStatus(9) != 101) && ( ( T000911_A4Indicadorid[0] > A4Indicadorid ) ) )
            {
               pr_datastore1.readNext(9);
            }
            if ( (pr_datastore1.getStatus(9) != 101) && ( ( T000911_A4Indicadorid[0] < A4Indicadorid ) ) )
            {
               A4Indicadorid = T000911_A4Indicadorid[0];
               RcdFound9 = 1;
            }
         }
         pr_datastore1.close(9);
      }

      protected void btn_enter( )
      {
         nKeyPressed = 1;
         GetKey099( ) ;
         if ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )
         {
            /* Insert record */
            GX_FocusControl = edtresultado_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            Insert099( ) ;
            if ( AnyError == 1 )
            {
               GX_FocusControl = "";
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            }
         }
         else
         {
            if ( RcdFound9 == 1 )
            {
               if ( A4Indicadorid != Z4Indicadorid )
               {
                  A4Indicadorid = Z4Indicadorid;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A4Indicadorid", StringUtil.LTrim( StringUtil.Str( (decimal)(A4Indicadorid), 9, 0)));
                  GX_msglist.addItem(context.GetMessage( "GXM_getbeforeupd", ""), "CandidateKeyNotFound", 1, "");
                  AnyError = 1;
               }
               else if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
               {
                  delete( ) ;
                  AfterTrn( ) ;
                  GX_FocusControl = edtresultado_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               }
               else
               {
                  /* Update record */
                  Update099( ) ;
                  GX_FocusControl = edtresultado_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               }
            }
            else
            {
               if ( A4Indicadorid != Z4Indicadorid )
               {
                  /* Insert record */
                  GX_FocusControl = edtresultado_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  Insert099( ) ;
                  if ( AnyError == 1 )
                  {
                     GX_FocusControl = "";
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  }
               }
               else
               {
                  if ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 )
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_recdeleted", ""), 1, "");
                     AnyError = 1;
                  }
                  else
                  {
                     /* Insert record */
                     GX_FocusControl = edtresultado_Internalname;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                     Insert099( ) ;
                     if ( AnyError == 1 )
                     {
                        GX_FocusControl = "";
                        context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                     }
                  }
               }
            }
         }
         AfterTrn( ) ;
         if ( ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 ) || ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 ) )
         {
            if ( AnyError == 0 )
            {
               context.nUserReturn = 1;
            }
         }
      }

      protected void btn_delete( )
      {
         if ( A4Indicadorid != Z4Indicadorid )
         {
            A4Indicadorid = Z4Indicadorid;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A4Indicadorid", StringUtil.LTrim( StringUtil.Str( (decimal)(A4Indicadorid), 9, 0)));
            GX_msglist.addItem(context.GetMessage( "GXM_getbeforedlt", ""), 1, "");
            AnyError = 1;
         }
         else
         {
            delete( ) ;
            AfterTrn( ) ;
            GX_FocusControl = edtresultado_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         if ( AnyError != 0 )
         {
         }
      }

      protected void CheckOptimisticConcurrency099( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
         {
            /* Using cursor T00092 */
            pr_datastore1.execute(0, new Object[] {A4Indicadorid});
            if ( (pr_datastore1.getStatus(0) == 103) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_lock", new   object[]  {"INDICADOR"}), "RecordIsLocked", 1, "");
               AnyError = 1;
               return  ;
            }
            Gx_longc = false;
            if ( (pr_datastore1.getStatus(0) == 101) || ( StringUtil.StrCmp(Z62resultado, T00092_A62resultado[0]) != 0 ) || ( StringUtil.StrCmp(Z63objetivo, T00092_A63objetivo[0]) != 0 ) || ( StringUtil.StrCmp(Z64objPlan, T00092_A64objPlan[0]) != 0 ) || ( StringUtil.StrCmp(Z65accion, T00092_A65accion[0]) != 0 ) || ( StringUtil.StrCmp(Z66accionPlan, T00092_A66accionPlan[0]) != 0 ) )
            {
               Gx_longc = true;
            }
            if ( Gx_longc || ( Z67donor != T00092_A67donor[0] ) || ( StringUtil.StrCmp(Z68milestone, T00092_A68milestone[0]) != 0 ) || ( Z25Proyectoid != T00092_A25Proyectoid[0] ) || ( Z15Paisid != T00092_A15Paisid[0] ) )
            {
               if ( StringUtil.StrCmp(Z62resultado, T00092_A62resultado[0]) != 0 )
               {
                  GXUtil.WriteLog("indicador:[seudo value changed for attri]"+"resultado");
                  GXUtil.WriteLogRaw("Old: ",Z62resultado);
                  GXUtil.WriteLogRaw("Current: ",T00092_A62resultado[0]);
               }
               if ( StringUtil.StrCmp(Z63objetivo, T00092_A63objetivo[0]) != 0 )
               {
                  GXUtil.WriteLog("indicador:[seudo value changed for attri]"+"objetivo");
                  GXUtil.WriteLogRaw("Old: ",Z63objetivo);
                  GXUtil.WriteLogRaw("Current: ",T00092_A63objetivo[0]);
               }
               if ( StringUtil.StrCmp(Z64objPlan, T00092_A64objPlan[0]) != 0 )
               {
                  GXUtil.WriteLog("indicador:[seudo value changed for attri]"+"objPlan");
                  GXUtil.WriteLogRaw("Old: ",Z64objPlan);
                  GXUtil.WriteLogRaw("Current: ",T00092_A64objPlan[0]);
               }
               if ( StringUtil.StrCmp(Z65accion, T00092_A65accion[0]) != 0 )
               {
                  GXUtil.WriteLog("indicador:[seudo value changed for attri]"+"accion");
                  GXUtil.WriteLogRaw("Old: ",Z65accion);
                  GXUtil.WriteLogRaw("Current: ",T00092_A65accion[0]);
               }
               if ( StringUtil.StrCmp(Z66accionPlan, T00092_A66accionPlan[0]) != 0 )
               {
                  GXUtil.WriteLog("indicador:[seudo value changed for attri]"+"accionPlan");
                  GXUtil.WriteLogRaw("Old: ",Z66accionPlan);
                  GXUtil.WriteLogRaw("Current: ",T00092_A66accionPlan[0]);
               }
               if ( Z67donor != T00092_A67donor[0] )
               {
                  GXUtil.WriteLog("indicador:[seudo value changed for attri]"+"donor");
                  GXUtil.WriteLogRaw("Old: ",Z67donor);
                  GXUtil.WriteLogRaw("Current: ",T00092_A67donor[0]);
               }
               if ( StringUtil.StrCmp(Z68milestone, T00092_A68milestone[0]) != 0 )
               {
                  GXUtil.WriteLog("indicador:[seudo value changed for attri]"+"milestone");
                  GXUtil.WriteLogRaw("Old: ",Z68milestone);
                  GXUtil.WriteLogRaw("Current: ",T00092_A68milestone[0]);
               }
               if ( Z25Proyectoid != T00092_A25Proyectoid[0] )
               {
                  GXUtil.WriteLog("indicador:[seudo value changed for attri]"+"Proyectoid");
                  GXUtil.WriteLogRaw("Old: ",Z25Proyectoid);
                  GXUtil.WriteLogRaw("Current: ",T00092_A25Proyectoid[0]);
               }
               if ( Z15Paisid != T00092_A15Paisid[0] )
               {
                  GXUtil.WriteLog("indicador:[seudo value changed for attri]"+"Paisid");
                  GXUtil.WriteLogRaw("Old: ",Z15Paisid);
                  GXUtil.WriteLogRaw("Current: ",T00092_A15Paisid[0]);
               }
               GX_msglist.addItem(context.GetMessage( "GXM_waschg", new   object[]  {"INDICADOR"}), "RecordWasChanged", 1, "");
               AnyError = 1;
               return  ;
            }
         }
      }

      protected void Insert099( )
      {
         BeforeValidate099( ) ;
         if ( AnyError == 0 )
         {
            CheckExtendedTable099( ) ;
         }
         if ( AnyError == 0 )
         {
            ZM099( 0) ;
            CheckOptimisticConcurrency099( ) ;
            if ( AnyError == 0 )
            {
               AfterConfirm099( ) ;
               if ( AnyError == 0 )
               {
                  BeforeInsert099( ) ;
                  if ( AnyError == 0 )
                  {
                     /* Using cursor T000912 */
                     pr_datastore1.execute(10, new Object[] {A62resultado, A63objetivo, A64objPlan, A65accion, A66accionPlan, A67donor, n68milestone, A68milestone, A25Proyectoid, A15Paisid});
                     A4Indicadorid = T000912_A4Indicadorid[0];
                     pr_datastore1.close(10);
                     dsDataStore1.SmartCacheProvider.SetUpdated("INDICADOR") ;
                     if ( AnyError == 0 )
                     {
                        /* Start of After( Insert) rules */
                        /* End of After( Insert) rules */
                        if ( AnyError == 0 )
                        {
                           /* Save values for previous() function. */
                           GX_msglist.addItem(context.GetMessage( "GXM_sucadded", ""), "SuccessfullyAdded", 0, "", true);
                           ResetCaption090( ) ;
                        }
                     }
                  }
                  else
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                     AnyError = 1;
                  }
               }
            }
            else
            {
               Load099( ) ;
            }
            EndLevel099( ) ;
         }
         CloseExtendedTableCursors099( ) ;
      }

      protected void Update099( )
      {
         BeforeValidate099( ) ;
         if ( AnyError == 0 )
         {
            CheckExtendedTable099( ) ;
         }
         if ( AnyError == 0 )
         {
            CheckOptimisticConcurrency099( ) ;
            if ( AnyError == 0 )
            {
               AfterConfirm099( ) ;
               if ( AnyError == 0 )
               {
                  BeforeUpdate099( ) ;
                  if ( AnyError == 0 )
                  {
                     /* Using cursor T000913 */
                     pr_datastore1.execute(11, new Object[] {A62resultado, A63objetivo, A64objPlan, A65accion, A66accionPlan, A67donor, n68milestone, A68milestone, A25Proyectoid, A15Paisid, A4Indicadorid});
                     pr_datastore1.close(11);
                     dsDataStore1.SmartCacheProvider.SetUpdated("INDICADOR") ;
                     if ( (pr_datastore1.getStatus(11) == 103) )
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_lock", new   object[]  {"INDICADOR"}), "RecordIsLocked", 1, "");
                        AnyError = 1;
                     }
                     DeferredUpdate099( ) ;
                     if ( AnyError == 0 )
                     {
                        /* Start of After( update) rules */
                        /* End of After( update) rules */
                        if ( AnyError == 0 )
                        {
                           if ( ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 ) || ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 ) )
                           {
                              if ( AnyError == 0 )
                              {
                                 context.nUserReturn = 1;
                              }
                           }
                        }
                     }
                     else
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                        AnyError = 1;
                     }
                  }
               }
            }
            EndLevel099( ) ;
         }
         CloseExtendedTableCursors099( ) ;
      }

      protected void DeferredUpdate099( )
      {
      }

      protected void delete( )
      {
         BeforeValidate099( ) ;
         if ( AnyError == 0 )
         {
            CheckOptimisticConcurrency099( ) ;
         }
         if ( AnyError == 0 )
         {
            OnDeleteControls099( ) ;
            AfterConfirm099( ) ;
            if ( AnyError == 0 )
            {
               BeforeDelete099( ) ;
               if ( AnyError == 0 )
               {
                  /* No cascading delete specified. */
                  /* Using cursor T000914 */
                  pr_datastore1.execute(12, new Object[] {A4Indicadorid});
                  pr_datastore1.close(12);
                  dsDataStore1.SmartCacheProvider.SetUpdated("INDICADOR") ;
                  if ( AnyError == 0 )
                  {
                     /* Start of After( delete) rules */
                     /* End of After( delete) rules */
                     if ( AnyError == 0 )
                     {
                        if ( ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 ) || ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 ) )
                        {
                           if ( AnyError == 0 )
                           {
                              context.nUserReturn = 1;
                           }
                        }
                     }
                  }
                  else
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                     AnyError = 1;
                  }
               }
            }
         }
         sMode9 = Gx_mode;
         Gx_mode = "DLT";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!")), context));
         EndLevel099( ) ;
         Gx_mode = sMode9;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!")), context));
      }

      protected void OnDeleteControls099( )
      {
         standaloneModal( ) ;
         if ( AnyError == 0 )
         {
            /* Delete mode formulas */
            AV14Pgmname = "Indicador";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14Pgmname", AV14Pgmname);
            /* Using cursor T000915 */
            pr_datastore1.execute(13, new Object[] {A25Proyectoid});
            A74Proyectonombre = T000915_A74Proyectonombre[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A74Proyectonombre", A74Proyectonombre);
            pr_datastore1.close(13);
            /* Using cursor T000916 */
            pr_datastore1.execute(14, new Object[] {A15Paisid});
            A118Paisnombre = T000916_A118Paisnombre[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A118Paisnombre", A118Paisnombre);
            pr_datastore1.close(14);
         }
         if ( AnyError == 0 )
         {
            /* Using cursor T000917 */
            pr_datastore1.execute(15, new Object[] {A4Indicadorid});
            if ( (pr_datastore1.getStatus(15) != 101) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_del", new   object[]  {"Actividad"}), "CannotDeleteReferencedRecord", 1, "");
               AnyError = 1;
            }
            pr_datastore1.close(15);
         }
      }

      protected void EndLevel099( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
         {
            pr_datastore1.close(0);
         }
         if ( AnyError == 0 )
         {
            BeforeComplete099( ) ;
         }
         if ( AnyError == 0 )
         {
            pr_datastore1.close(1);
            pr_datastore1.close(13);
            pr_datastore1.close(14);
            context.CommitDataStores("indicador",pr_default);
            if ( AnyError == 0 )
            {
               ConfirmValues090( ) ;
            }
            /* After transaction rules */
            /* Execute 'After Trn' event if defined. */
            trnEnded = 1;
         }
         else
         {
            pr_datastore1.close(1);
            pr_datastore1.close(13);
            pr_datastore1.close(14);
            context.RollbackDataStores("indicador",pr_default);
         }
         IsModified = 0;
         if ( AnyError != 0 )
         {
            context.wjLoc = "";
            context.nUserReturn = 0;
         }
      }

      public void ScanStart099( )
      {
         /* Scan By routine */
         /* Using cursor T000918 */
         pr_datastore1.execute(16);
         RcdFound9 = 0;
         if ( (pr_datastore1.getStatus(16) != 101) )
         {
            RcdFound9 = 1;
            A4Indicadorid = T000918_A4Indicadorid[0];
         }
         /* Load Subordinate Levels */
      }

      protected void ScanNext099( )
      {
         /* Scan next routine */
         pr_datastore1.readNext(16);
         RcdFound9 = 0;
         if ( (pr_datastore1.getStatus(16) != 101) )
         {
            RcdFound9 = 1;
            A4Indicadorid = T000918_A4Indicadorid[0];
         }
      }

      protected void ScanEnd099( )
      {
         pr_datastore1.close(16);
      }

      protected void AfterConfirm099( )
      {
         /* After Confirm Rules */
      }

      protected void BeforeInsert099( )
      {
         /* Before Insert Rules */
      }

      protected void BeforeUpdate099( )
      {
         /* Before Update Rules */
      }

      protected void BeforeDelete099( )
      {
         /* Before Delete Rules */
      }

      protected void BeforeComplete099( )
      {
         /* Before Complete Rules */
      }

      protected void BeforeValidate099( )
      {
         /* Before Validate Rules */
      }

      protected void DisableAttributes099( )
      {
         edtresultado_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtresultado_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtresultado_Enabled), 5, 0)), true);
         edtobjetivo_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtobjetivo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtobjetivo_Enabled), 5, 0)), true);
         edtobjPlan_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtobjPlan_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtobjPlan_Enabled), 5, 0)), true);
         edtProyectoid_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtProyectoid_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtProyectoid_Enabled), 5, 0)), true);
         edtProyectonombre_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtProyectonombre_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtProyectonombre_Enabled), 5, 0)), true);
         edtPaisid_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtPaisid_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtPaisid_Enabled), 5, 0)), true);
         edtPaisnombre_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtPaisnombre_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtPaisnombre_Enabled), 5, 0)), true);
         edtaccion_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtaccion_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtaccion_Enabled), 5, 0)), true);
         edtaccionPlan_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtaccionPlan_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtaccionPlan_Enabled), 5, 0)), true);
         edtdonor_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtdonor_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtdonor_Enabled), 5, 0)), true);
         edtmilestone_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtmilestone_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtmilestone_Enabled), 5, 0)), true);
      }

      protected void send_integrity_lvl_hashes099( )
      {
      }

      protected void assign_properties_default( )
      {
      }

      protected void ConfirmValues090( )
      {
      }

      public override void RenderHtmlHeaders( )
      {
         GxWebStd.gx_html_headers( context, 0, "", "", Form.Meta, Form.Metaequiv, true);
      }

      public override void RenderHtmlOpenForm( )
      {
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.WriteHtmlText( "<title>") ;
         context.SendWebValue( Form.Caption) ;
         context.WriteHtmlTextNl( "</title>") ;
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         if ( StringUtil.Len( sDynURL) > 0 )
         {
            context.WriteHtmlText( "<BASE href=\""+sDynURL+"\" />") ;
         }
         define_styles( ) ;
         MasterPageObj.master_styles();
         if ( ( ( context.GetBrowserType( ) == 1 ) || ( context.GetBrowserType( ) == 5 ) ) && ( StringUtil.StrCmp(context.GetBrowserVersion( ), "7.0") == 0 ) )
         {
            context.AddJavascriptSource("json2.js", "?"+context.GetBuildNumber( 127771), false);
         }
         context.AddJavascriptSource("jquery.js", "?"+context.GetBuildNumber( 127771), false);
         context.AddJavascriptSource("gxtimezone.js", "?"+context.GetBuildNumber( 127771), false);
         context.AddJavascriptSource("gxgral.js", "?"+context.GetBuildNumber( 127771), false);
         context.AddJavascriptSource("gxcfg.js", "?2019151424284", false);
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.WriteHtmlText( Form.Headerrawhtml) ;
         context.CloseHtmlHeader();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         FormProcess = " data-HasEnter=\"true\" data-Skiponenter=\"false\"";
         context.WriteHtmlText( "<body ") ;
         bodyStyle = "" + "background-color:" + context.BuildHTMLColor( Form.Backcolor) + ";color:" + context.BuildHTMLColor( Form.Textcolor) + ";";
         bodyStyle = bodyStyle + "-moz-opacity:0;opacity:0;";
         if ( ! ( String.IsNullOrEmpty(StringUtil.RTrim( Form.Background)) ) )
         {
            bodyStyle = bodyStyle + " background-image:url(" + context.convertURL( Form.Background) + ")";
         }
         context.WriteHtmlText( " "+"class=\"form-horizontal Form\""+" "+ "style='"+bodyStyle+"'") ;
         context.WriteHtmlText( FormProcess+">") ;
         context.skipLines(1);
         context.WriteHtmlTextNl( "<form id=\"MAINFORM\" autocomplete=\"off\" name=\"MAINFORM\" method=\"post\" tabindex=-1  class=\"form-horizontal Form\" data-gx-class=\"form-horizontal Form\" novalidate action=\""+formatLink("indicador.aspx") + "?" + UrlEncode(StringUtil.RTrim(Gx_mode)) + "," + UrlEncode("" +AV7Indicadorid)+"\">") ;
         GxWebStd.gx_hidden_field( context, "_EventName", "");
         GxWebStd.gx_hidden_field( context, "_EventGridId", "");
         GxWebStd.gx_hidden_field( context, "_EventRowId", "");
         context.WriteHtmlText( "<input type=\"submit\" title=\"submit\" style=\"display:none\" disabled>") ;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, "FORM", "Class", "form-horizontal Form", true);
         toggleJsOutput = isJsOutputEnabled( );
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
      }

      protected void send_integrity_footer_hashes( )
      {
         GXKey = Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
         forbiddenHiddens = "hsh" + "Indicador";
         forbiddenHiddens = forbiddenHiddens + context.localUtil.Format( (decimal)(AV11Insert_Proyectoid), "ZZZZZZZZ9");
         forbiddenHiddens = forbiddenHiddens + context.localUtil.Format( (decimal)(AV12Insert_Paisid), "ZZZZZZZZ9");
         forbiddenHiddens = forbiddenHiddens + StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"));
         forbiddenHiddens = forbiddenHiddens + context.localUtil.Format( (decimal)(A4Indicadorid), "ZZZZZZZZ9");
         GxWebStd.gx_hidden_field( context, "hsh", GetEncryptedHash( forbiddenHiddens, GXKey));
         GXUtil.WriteLog("indicador:[SendSecurityCheck value for]"+"Insert_Proyectoid:"+context.localUtil.Format( (decimal)(AV11Insert_Proyectoid), "ZZZZZZZZ9"));
         GXUtil.WriteLog("indicador:[SendSecurityCheck value for]"+"Insert_Paisid:"+context.localUtil.Format( (decimal)(AV12Insert_Paisid), "ZZZZZZZZ9"));
         GXUtil.WriteLog("indicador:[SendSecurityCheck value for]"+"Gx_mode:"+StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!")));
         GXUtil.WriteLog("indicador:[SendSecurityCheck value for]"+"Indicadorid:"+context.localUtil.Format( (decimal)(A4Indicadorid), "ZZZZZZZZ9"));
      }

      protected void SendCloseFormHiddens( )
      {
         /* Send hidden variables. */
         /* Send saved values. */
         send_integrity_footer_hashes( ) ;
         GxWebStd.gx_hidden_field( context, "Z4Indicadorid", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z4Indicadorid), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "Z62resultado", Z62resultado);
         GxWebStd.gx_hidden_field( context, "Z63objetivo", Z63objetivo);
         GxWebStd.gx_hidden_field( context, "Z64objPlan", Z64objPlan);
         GxWebStd.gx_hidden_field( context, "Z65accion", Z65accion);
         GxWebStd.gx_hidden_field( context, "Z66accionPlan", Z66accionPlan);
         GxWebStd.gx_hidden_field( context, "Z67donor", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z67donor), 1, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "Z68milestone", Z68milestone);
         GxWebStd.gx_hidden_field( context, "Z25Proyectoid", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z25Proyectoid), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "Z15Paisid", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z15Paisid), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "IsConfirmed", StringUtil.LTrim( StringUtil.NToC( (decimal)(IsConfirmed), 4, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "IsModified", StringUtil.LTrim( StringUtil.NToC( (decimal)(IsModified), 4, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "Mode", StringUtil.RTrim( Gx_mode));
         GxWebStd.gx_hidden_field( context, "gxhash_Mode", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!")), context));
         GxWebStd.gx_hidden_field( context, "N25Proyectoid", StringUtil.LTrim( StringUtil.NToC( (decimal)(A25Proyectoid), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "N15Paisid", StringUtil.LTrim( StringUtil.NToC( (decimal)(A15Paisid), 9, 0, ".", "")));
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vTRNCONTEXT", AV9TrnContext);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vTRNCONTEXT", AV9TrnContext);
         }
         GxWebStd.gx_hidden_field( context, "vINDICADORID", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV7Indicadorid), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "gxhash_vINDICADORID", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV7Indicadorid), "ZZZZZZZZ9"), context));
         GxWebStd.gx_hidden_field( context, "INDICADORID", StringUtil.LTrim( StringUtil.NToC( (decimal)(A4Indicadorid), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "vINSERT_PROYECTOID", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV11Insert_Proyectoid), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "vINSERT_PAISID", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV12Insert_Paisid), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "vPGMNAME", StringUtil.RTrim( AV14Pgmname));
         GxWebStd.gx_hidden_field( context, "vMODE", StringUtil.RTrim( Gx_mode));
         GxWebStd.gx_hidden_field( context, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!")), context));
      }

      public override void RenderHtmlCloseForm( )
      {
         SendCloseFormHiddens( ) ;
         GxWebStd.gx_hidden_field( context, "GX_FocusControl", GX_FocusControl);
         SendAjaxEncryptionKey();
         SendSecurityToken(sPrefix);
         SendComponentObjects();
         SendServerCommands();
         SendState();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         context.WriteHtmlTextNl( "</form>") ;
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         include_jscripts( ) ;
      }

      public override short ExecuteStartEvent( )
      {
         standaloneStartup( ) ;
         gxajaxcallmode = (short)((isAjaxCallMode( ) ? 1 : 0));
         return gxajaxcallmode ;
      }

      public override void RenderHtmlContent( )
      {
         context.WriteHtmlText( "<div") ;
         GxWebStd.ClassAttribute( context, "gx-ct-body"+" "+(String.IsNullOrEmpty(StringUtil.RTrim( Form.Class)) ? "form-horizontal Form" : Form.Class)+"-fx");
         context.WriteHtmlText( ">") ;
         Draw( ) ;
         context.WriteHtmlText( "</div>") ;
      }

      public override void DispatchEvents( )
      {
         Process( ) ;
      }

      public override bool HasEnterEvent( )
      {
         return true ;
      }

      public override GXWebForm GetForm( )
      {
         return Form ;
      }

      public override String GetSelfLink( )
      {
         return formatLink("indicador.aspx") + "?" + UrlEncode(StringUtil.RTrim(Gx_mode)) + "," + UrlEncode("" +AV7Indicadorid) ;
      }

      public override String GetPgmname( )
      {
         return "Indicador" ;
      }

      public override String GetPgmdesc( )
      {
         return "Indicador" ;
      }

      protected void InitializeNonKey099( )
      {
         A25Proyectoid = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A25Proyectoid", StringUtil.LTrim( StringUtil.Str( (decimal)(A25Proyectoid), 9, 0)));
         A15Paisid = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A15Paisid", StringUtil.LTrim( StringUtil.Str( (decimal)(A15Paisid), 9, 0)));
         A62resultado = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A62resultado", A62resultado);
         A63objetivo = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A63objetivo", A63objetivo);
         A64objPlan = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A64objPlan", A64objPlan);
         A74Proyectonombre = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A74Proyectonombre", A74Proyectonombre);
         A118Paisnombre = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A118Paisnombre", A118Paisnombre);
         A65accion = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A65accion", A65accion);
         A66accionPlan = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A66accionPlan", A66accionPlan);
         A67donor = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A67donor", StringUtil.Str( (decimal)(A67donor), 1, 0));
         A68milestone = "";
         n68milestone = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A68milestone", A68milestone);
         n68milestone = (String.IsNullOrEmpty(StringUtil.RTrim( A68milestone)) ? true : false);
         Z62resultado = "";
         Z63objetivo = "";
         Z64objPlan = "";
         Z65accion = "";
         Z66accionPlan = "";
         Z67donor = 0;
         Z68milestone = "";
         Z25Proyectoid = 0;
         Z15Paisid = 0;
      }

      protected void InitAll099( )
      {
         A4Indicadorid = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A4Indicadorid", StringUtil.LTrim( StringUtil.Str( (decimal)(A4Indicadorid), 9, 0)));
         InitializeNonKey099( ) ;
      }

      protected void StandaloneModalInsert( )
      {
      }

      protected void define_styles( )
      {
         AddThemeStyleSheetFile("", context.GetTheme( )+".css", "?"+GetCacheInvalidationToken( ));
         bool outputEnabled = isOutputEnabled( ) ;
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         idxLst = 1;
         while ( idxLst <= Form.Jscriptsrc.Count )
         {
            context.AddJavascriptSource(StringUtil.RTrim( ((String)Form.Jscriptsrc.Item(idxLst))), "?201915142431", true);
            idxLst = (int)(idxLst+1);
         }
         if ( ! outputEnabled )
         {
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
         }
         /* End function define_styles */
      }

      protected void include_jscripts( )
      {
         context.AddJavascriptSource("messages.eng.js", "?"+GetCacheInvalidationToken( ), false);
         context.AddJavascriptSource("indicador.js", "?201915142431", false);
         /* End function include_jscripts */
      }

      protected void init_default_properties( )
      {
         lblTitle_Internalname = "TITLE";
         divTitlecontainer_Internalname = "TITLECONTAINER";
         bttBtn_first_Internalname = "BTN_FIRST";
         bttBtn_previous_Internalname = "BTN_PREVIOUS";
         bttBtn_next_Internalname = "BTN_NEXT";
         bttBtn_last_Internalname = "BTN_LAST";
         bttBtn_select_Internalname = "BTN_SELECT";
         divToolbarcell_Internalname = "TOOLBARCELL";
         edtresultado_Internalname = "RESULTADO";
         edtobjetivo_Internalname = "OBJETIVO";
         edtobjPlan_Internalname = "OBJPLAN";
         edtProyectoid_Internalname = "PROYECTOID";
         edtProyectonombre_Internalname = "PROYECTONOMBRE";
         edtPaisid_Internalname = "PAISID";
         edtPaisnombre_Internalname = "PAISNOMBRE";
         edtaccion_Internalname = "ACCION";
         edtaccionPlan_Internalname = "ACCIONPLAN";
         edtdonor_Internalname = "DONOR";
         edtmilestone_Internalname = "MILESTONE";
         divFormcontainer_Internalname = "FORMCONTAINER";
         bttBtn_enter_Internalname = "BTN_ENTER";
         bttBtn_cancel_Internalname = "BTN_CANCEL";
         bttBtn_delete_Internalname = "BTN_DELETE";
         divMaintable_Internalname = "MAINTABLE";
         Form.Internalname = "FORM";
         imgprompt_25_Internalname = "PROMPT_25";
         imgprompt_15_Internalname = "PROMPT_15";
      }

      public override void initialize_properties( )
      {
         context.SetDefaultTheme("Carmine");
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         init_default_properties( ) ;
         Form.Headerrawhtml = "";
         Form.Background = "";
         Form.Textcolor = 0;
         Form.Backcolor = (int)(0xFFFFFF);
         Form.Caption = "Indicador";
         bttBtn_delete_Enabled = 0;
         bttBtn_delete_Visible = 1;
         bttBtn_cancel_Visible = 1;
         bttBtn_enter_Enabled = 1;
         bttBtn_enter_Visible = 1;
         edtmilestone_Enabled = 1;
         edtdonor_Jsonclick = "";
         edtdonor_Enabled = 1;
         edtaccionPlan_Enabled = 1;
         edtaccion_Enabled = 1;
         edtPaisnombre_Jsonclick = "";
         edtPaisnombre_Enabled = 0;
         imgprompt_15_Visible = 1;
         imgprompt_15_Link = "";
         edtPaisid_Jsonclick = "";
         edtPaisid_Enabled = 1;
         edtProyectonombre_Jsonclick = "";
         edtProyectonombre_Enabled = 0;
         imgprompt_25_Visible = 1;
         imgprompt_25_Link = "";
         edtProyectoid_Jsonclick = "";
         edtProyectoid_Enabled = 1;
         edtobjPlan_Enabled = 1;
         edtobjetivo_Enabled = 1;
         edtresultado_Enabled = 1;
         bttBtn_select_Visible = 1;
         bttBtn_last_Visible = 1;
         bttBtn_next_Visible = 1;
         bttBtn_previous_Visible = 1;
         bttBtn_first_Visible = 1;
         context.GX_msglist.DisplayMode = 1;
         if ( context.isSpaRequest( ) )
         {
            enableJsOutput();
         }
      }

      protected void dynload_actions( )
      {
         /* End function dynload_actions */
      }

      protected void init_web_controls( )
      {
         /* End function init_web_controls */
      }

      public void Valid_Proyectoid( int GX_Parm1 ,
                                    String GX_Parm2 )
      {
         A25Proyectoid = GX_Parm1;
         A74Proyectonombre = GX_Parm2;
         /* Using cursor T000915 */
         pr_datastore1.execute(13, new Object[] {A25Proyectoid});
         if ( (pr_datastore1.getStatus(13) == 101) )
         {
            GX_msglist.addItem("No matching 'Proyecto'.", "ForeignKeyNotFound", 1, "PROYECTOID");
            AnyError = 1;
            GX_FocusControl = edtProyectoid_Internalname;
         }
         A74Proyectonombre = T000915_A74Proyectonombre[0];
         pr_datastore1.close(13);
         dynload_actions( ) ;
         if ( AnyError == 1 )
         {
            A74Proyectonombre = "";
         }
         isValidOutput.Add(A74Proyectonombre);
         isValidOutput.Add(context.GX_msglist.ToJavascriptSource());
         isValidOutput.Add(context.httpAjaxContext.ajax_rsp_get_hiddens( ));
         context.GX_webresponse.AddString(isValidOutput.ToJSonString());
         wbTemp = context.ResponseContentType( "application/json");
      }

      public void Valid_Paisid( int GX_Parm1 ,
                                String GX_Parm2 )
      {
         A15Paisid = GX_Parm1;
         A118Paisnombre = GX_Parm2;
         /* Using cursor T000916 */
         pr_datastore1.execute(14, new Object[] {A15Paisid});
         if ( (pr_datastore1.getStatus(14) == 101) )
         {
            GX_msglist.addItem("No matching 'Pais'.", "ForeignKeyNotFound", 1, "PAISID");
            AnyError = 1;
            GX_FocusControl = edtPaisid_Internalname;
         }
         A118Paisnombre = T000916_A118Paisnombre[0];
         pr_datastore1.close(14);
         dynload_actions( ) ;
         if ( AnyError == 1 )
         {
            A118Paisnombre = "";
         }
         isValidOutput.Add(A118Paisnombre);
         isValidOutput.Add(context.GX_msglist.ToJavascriptSource());
         isValidOutput.Add(context.httpAjaxContext.ajax_rsp_get_hiddens( ));
         context.GX_webresponse.AddString(isValidOutput.ToJSonString());
         wbTemp = context.ResponseContentType( "application/json");
      }

      public override bool SupportAjaxEvent( )
      {
         return true ;
      }

      public override void InitializeDynEvents( )
      {
         setEventMetadata("ENTER","{handler:'UserMainFullajax',iparms:[{postForm:true},{av:'Gx_mode',fld:'vMODE',pic:'@!',hsh:true},{av:'AV7Indicadorid',fld:'vINDICADORID',pic:'ZZZZZZZZ9',hsh:true}]");
         setEventMetadata("ENTER",",oparms:[]}");
         setEventMetadata("REFRESH","{handler:'Refresh',iparms:[{av:'Gx_mode',fld:'vMODE',pic:'@!',hsh:true},{av:'AV7Indicadorid',fld:'vINDICADORID',pic:'ZZZZZZZZ9',hsh:true},{av:'AV11Insert_Proyectoid',fld:'vINSERT_PROYECTOID',pic:'ZZZZZZZZ9'},{av:'AV12Insert_Paisid',fld:'vINSERT_PAISID',pic:'ZZZZZZZZ9'},{av:'A4Indicadorid',fld:'INDICADORID',pic:'ZZZZZZZZ9'}]");
         setEventMetadata("REFRESH",",oparms:[]}");
         setEventMetadata("AFTER TRN","{handler:'E12092',iparms:[{av:'Gx_mode',fld:'vMODE',pic:'@!',hsh:true},{av:'AV9TrnContext',fld:'vTRNCONTEXT',pic:''}]");
         setEventMetadata("AFTER TRN",",oparms:[]}");
         return  ;
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
         pr_datastore1.close(1);
         pr_datastore1.close(13);
         pr_datastore1.close(14);
      }

      public override void initialize( )
      {
         sPrefix = "";
         wcpOGx_mode = "";
         Z62resultado = "";
         Z63objetivo = "";
         Z64objPlan = "";
         Z65accion = "";
         Z66accionPlan = "";
         Z68milestone = "";
         scmdbuf = "";
         gxfirstwebparm = "";
         gxfirstwebparm_bkp = "";
         GXKey = "";
         PreviousTooltip = "";
         PreviousCaption = "";
         Form = new GXWebForm();
         GX_FocusControl = "";
         lblTitle_Jsonclick = "";
         ClassString = "";
         StyleString = "";
         TempTags = "";
         bttBtn_first_Jsonclick = "";
         bttBtn_previous_Jsonclick = "";
         bttBtn_next_Jsonclick = "";
         bttBtn_last_Jsonclick = "";
         bttBtn_select_Jsonclick = "";
         A62resultado = "";
         A63objetivo = "";
         A64objPlan = "";
         sImgUrl = "";
         A74Proyectonombre = "";
         A118Paisnombre = "";
         A65accion = "";
         A66accionPlan = "";
         A68milestone = "";
         bttBtn_enter_Jsonclick = "";
         bttBtn_cancel_Jsonclick = "";
         bttBtn_delete_Jsonclick = "";
         AV14Pgmname = "";
         forbiddenHiddens = "";
         hsh = "";
         sMode9 = "";
         sEvt = "";
         EvtGridId = "";
         EvtRowId = "";
         sEvtType = "";
         AV9TrnContext = new SdtTransactionContext(context);
         AV10WebSession = context.GetSession();
         AV13TrnContextAtt = new SdtTransactionContext_Attribute(context);
         Z74Proyectonombre = "";
         Z118Paisnombre = "";
         T00095_A118Paisnombre = new String[] {""} ;
         T00094_A74Proyectonombre = new String[] {""} ;
         T00096_A4Indicadorid = new int[1] ;
         T00096_A62resultado = new String[] {""} ;
         T00096_A63objetivo = new String[] {""} ;
         T00096_A64objPlan = new String[] {""} ;
         T00096_A74Proyectonombre = new String[] {""} ;
         T00096_A118Paisnombre = new String[] {""} ;
         T00096_A65accion = new String[] {""} ;
         T00096_A66accionPlan = new String[] {""} ;
         T00096_A67donor = new short[1] ;
         T00096_A68milestone = new String[] {""} ;
         T00096_n68milestone = new bool[] {false} ;
         T00096_A25Proyectoid = new int[1] ;
         T00096_A15Paisid = new int[1] ;
         T00097_A74Proyectonombre = new String[] {""} ;
         T00098_A118Paisnombre = new String[] {""} ;
         T00099_A4Indicadorid = new int[1] ;
         T00093_A4Indicadorid = new int[1] ;
         T00093_A62resultado = new String[] {""} ;
         T00093_A63objetivo = new String[] {""} ;
         T00093_A64objPlan = new String[] {""} ;
         T00093_A65accion = new String[] {""} ;
         T00093_A66accionPlan = new String[] {""} ;
         T00093_A67donor = new short[1] ;
         T00093_A68milestone = new String[] {""} ;
         T00093_n68milestone = new bool[] {false} ;
         T00093_A25Proyectoid = new int[1] ;
         T00093_A15Paisid = new int[1] ;
         T000910_A4Indicadorid = new int[1] ;
         T000911_A4Indicadorid = new int[1] ;
         T00092_A4Indicadorid = new int[1] ;
         T00092_A62resultado = new String[] {""} ;
         T00092_A63objetivo = new String[] {""} ;
         T00092_A64objPlan = new String[] {""} ;
         T00092_A65accion = new String[] {""} ;
         T00092_A66accionPlan = new String[] {""} ;
         T00092_A67donor = new short[1] ;
         T00092_A68milestone = new String[] {""} ;
         T00092_n68milestone = new bool[] {false} ;
         T00092_A25Proyectoid = new int[1] ;
         T00092_A15Paisid = new int[1] ;
         T000912_A4Indicadorid = new int[1] ;
         T000915_A74Proyectonombre = new String[] {""} ;
         T000916_A118Paisnombre = new String[] {""} ;
         T000917_A3Actividadid = new int[1] ;
         T000918_A4Indicadorid = new int[1] ;
         sDynURL = "";
         FormProcess = "";
         bodyStyle = "";
         isValidOutput = new GxUnknownObjectCollection();
         pr_datastore1 = new DataStoreProvider(context, new GeneXus.Programs.indicador__datastore1(),
            new Object[][] {
                new Object[] {
               T00092_A4Indicadorid, T00092_A62resultado, T00092_A63objetivo, T00092_A64objPlan, T00092_A65accion, T00092_A66accionPlan, T00092_A67donor, T00092_A68milestone, T00092_n68milestone, T00092_A25Proyectoid,
               T00092_A15Paisid
               }
               , new Object[] {
               T00093_A4Indicadorid, T00093_A62resultado, T00093_A63objetivo, T00093_A64objPlan, T00093_A65accion, T00093_A66accionPlan, T00093_A67donor, T00093_A68milestone, T00093_n68milestone, T00093_A25Proyectoid,
               T00093_A15Paisid
               }
               , new Object[] {
               T00094_A74Proyectonombre
               }
               , new Object[] {
               T00095_A118Paisnombre
               }
               , new Object[] {
               T00096_A4Indicadorid, T00096_A62resultado, T00096_A63objetivo, T00096_A64objPlan, T00096_A74Proyectonombre, T00096_A118Paisnombre, T00096_A65accion, T00096_A66accionPlan, T00096_A67donor, T00096_A68milestone,
               T00096_n68milestone, T00096_A25Proyectoid, T00096_A15Paisid
               }
               , new Object[] {
               T00097_A74Proyectonombre
               }
               , new Object[] {
               T00098_A118Paisnombre
               }
               , new Object[] {
               T00099_A4Indicadorid
               }
               , new Object[] {
               T000910_A4Indicadorid
               }
               , new Object[] {
               T000911_A4Indicadorid
               }
               , new Object[] {
               T000912_A4Indicadorid
               }
               , new Object[] {
               }
               , new Object[] {
               }
               , new Object[] {
               T000915_A74Proyectonombre
               }
               , new Object[] {
               T000916_A118Paisnombre
               }
               , new Object[] {
               T000917_A3Actividadid
               }
               , new Object[] {
               T000918_A4Indicadorid
               }
            }
         );
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.indicador__default(),
            new Object[][] {
            }
         );
         AV14Pgmname = "Indicador";
      }

      private short Z67donor ;
      private short GxWebError ;
      private short gxcookieaux ;
      private short IsConfirmed ;
      private short IsModified ;
      private short AnyError ;
      private short nKeyPressed ;
      private short initialized ;
      private short A67donor ;
      private short RcdFound9 ;
      private short GX_JID ;
      private short Gx_BScreen ;
      private short gxajaxcallmode ;
      private short wbTemp ;
      private int wcpOAV7Indicadorid ;
      private int Z4Indicadorid ;
      private int Z25Proyectoid ;
      private int Z15Paisid ;
      private int N25Proyectoid ;
      private int N15Paisid ;
      private int A25Proyectoid ;
      private int A15Paisid ;
      private int AV7Indicadorid ;
      private int trnEnded ;
      private int bttBtn_first_Visible ;
      private int bttBtn_previous_Visible ;
      private int bttBtn_next_Visible ;
      private int bttBtn_last_Visible ;
      private int bttBtn_select_Visible ;
      private int edtresultado_Enabled ;
      private int edtobjetivo_Enabled ;
      private int edtobjPlan_Enabled ;
      private int edtProyectoid_Enabled ;
      private int imgprompt_25_Visible ;
      private int edtProyectonombre_Enabled ;
      private int edtPaisid_Enabled ;
      private int imgprompt_15_Visible ;
      private int edtPaisnombre_Enabled ;
      private int edtaccion_Enabled ;
      private int edtaccionPlan_Enabled ;
      private int edtdonor_Enabled ;
      private int edtmilestone_Enabled ;
      private int bttBtn_enter_Visible ;
      private int bttBtn_enter_Enabled ;
      private int bttBtn_cancel_Visible ;
      private int bttBtn_delete_Visible ;
      private int bttBtn_delete_Enabled ;
      private int A4Indicadorid ;
      private int AV11Insert_Proyectoid ;
      private int AV12Insert_Paisid ;
      private int AV15GXV1 ;
      private int idxLst ;
      private String sPrefix ;
      private String wcpOGx_mode ;
      private String scmdbuf ;
      private String gxfirstwebparm ;
      private String gxfirstwebparm_bkp ;
      private String Gx_mode ;
      private String GXKey ;
      private String PreviousTooltip ;
      private String PreviousCaption ;
      private String GX_FocusControl ;
      private String edtresultado_Internalname ;
      private String divMaintable_Internalname ;
      private String divTitlecontainer_Internalname ;
      private String lblTitle_Internalname ;
      private String lblTitle_Jsonclick ;
      private String ClassString ;
      private String StyleString ;
      private String divFormcontainer_Internalname ;
      private String divToolbarcell_Internalname ;
      private String TempTags ;
      private String bttBtn_first_Internalname ;
      private String bttBtn_first_Jsonclick ;
      private String bttBtn_previous_Internalname ;
      private String bttBtn_previous_Jsonclick ;
      private String bttBtn_next_Internalname ;
      private String bttBtn_next_Jsonclick ;
      private String bttBtn_last_Internalname ;
      private String bttBtn_last_Jsonclick ;
      private String bttBtn_select_Internalname ;
      private String bttBtn_select_Jsonclick ;
      private String edtobjetivo_Internalname ;
      private String edtobjPlan_Internalname ;
      private String edtProyectoid_Internalname ;
      private String edtProyectoid_Jsonclick ;
      private String sImgUrl ;
      private String imgprompt_25_Internalname ;
      private String imgprompt_25_Link ;
      private String edtProyectonombre_Internalname ;
      private String edtProyectonombre_Jsonclick ;
      private String edtPaisid_Internalname ;
      private String edtPaisid_Jsonclick ;
      private String imgprompt_15_Internalname ;
      private String imgprompt_15_Link ;
      private String edtPaisnombre_Internalname ;
      private String edtPaisnombre_Jsonclick ;
      private String edtaccion_Internalname ;
      private String edtaccionPlan_Internalname ;
      private String edtdonor_Internalname ;
      private String edtdonor_Jsonclick ;
      private String edtmilestone_Internalname ;
      private String bttBtn_enter_Internalname ;
      private String bttBtn_enter_Jsonclick ;
      private String bttBtn_cancel_Internalname ;
      private String bttBtn_cancel_Jsonclick ;
      private String bttBtn_delete_Internalname ;
      private String bttBtn_delete_Jsonclick ;
      private String AV14Pgmname ;
      private String forbiddenHiddens ;
      private String hsh ;
      private String sMode9 ;
      private String sEvt ;
      private String EvtGridId ;
      private String EvtRowId ;
      private String sEvtType ;
      private String sDynURL ;
      private String FormProcess ;
      private String bodyStyle ;
      private bool entryPointCalled ;
      private bool toggleJsOutput ;
      private bool wbErr ;
      private bool n68milestone ;
      private bool returnInSub ;
      private bool Gx_longc ;
      private String Z62resultado ;
      private String Z63objetivo ;
      private String Z64objPlan ;
      private String Z65accion ;
      private String Z66accionPlan ;
      private String Z68milestone ;
      private String A62resultado ;
      private String A63objetivo ;
      private String A64objPlan ;
      private String A74Proyectonombre ;
      private String A118Paisnombre ;
      private String A65accion ;
      private String A66accionPlan ;
      private String A68milestone ;
      private String Z74Proyectonombre ;
      private String Z118Paisnombre ;
      private IGxSession AV10WebSession ;
      private GxUnknownObjectCollection isValidOutput ;
      private IGxDataStore dsDataStore1 ;
      private IGxDataStore dsDefault ;
      private IDataStoreProvider pr_datastore1 ;
      private String[] T00095_A118Paisnombre ;
      private String[] T00094_A74Proyectonombre ;
      private int[] T00096_A4Indicadorid ;
      private String[] T00096_A62resultado ;
      private String[] T00096_A63objetivo ;
      private String[] T00096_A64objPlan ;
      private String[] T00096_A74Proyectonombre ;
      private String[] T00096_A118Paisnombre ;
      private String[] T00096_A65accion ;
      private String[] T00096_A66accionPlan ;
      private short[] T00096_A67donor ;
      private String[] T00096_A68milestone ;
      private bool[] T00096_n68milestone ;
      private int[] T00096_A25Proyectoid ;
      private int[] T00096_A15Paisid ;
      private String[] T00097_A74Proyectonombre ;
      private String[] T00098_A118Paisnombre ;
      private int[] T00099_A4Indicadorid ;
      private int[] T00093_A4Indicadorid ;
      private String[] T00093_A62resultado ;
      private String[] T00093_A63objetivo ;
      private String[] T00093_A64objPlan ;
      private String[] T00093_A65accion ;
      private String[] T00093_A66accionPlan ;
      private short[] T00093_A67donor ;
      private String[] T00093_A68milestone ;
      private bool[] T00093_n68milestone ;
      private int[] T00093_A25Proyectoid ;
      private int[] T00093_A15Paisid ;
      private int[] T000910_A4Indicadorid ;
      private int[] T000911_A4Indicadorid ;
      private int[] T00092_A4Indicadorid ;
      private String[] T00092_A62resultado ;
      private String[] T00092_A63objetivo ;
      private String[] T00092_A64objPlan ;
      private String[] T00092_A65accion ;
      private String[] T00092_A66accionPlan ;
      private short[] T00092_A67donor ;
      private String[] T00092_A68milestone ;
      private bool[] T00092_n68milestone ;
      private int[] T00092_A25Proyectoid ;
      private int[] T00092_A15Paisid ;
      private int[] T000912_A4Indicadorid ;
      private String[] T000915_A74Proyectonombre ;
      private String[] T000916_A118Paisnombre ;
      private int[] T000917_A3Actividadid ;
      private IDataStoreProvider pr_default ;
      private int[] T000918_A4Indicadorid ;
      private GXWebForm Form ;
      private SdtTransactionContext AV9TrnContext ;
      private SdtTransactionContext_Attribute AV13TrnContextAtt ;
   }

   public class indicador__datastore1 : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
         ,new ForEachCursor(def[2])
         ,new ForEachCursor(def[3])
         ,new ForEachCursor(def[4])
         ,new ForEachCursor(def[5])
         ,new ForEachCursor(def[6])
         ,new ForEachCursor(def[7])
         ,new ForEachCursor(def[8])
         ,new ForEachCursor(def[9])
         ,new ForEachCursor(def[10])
         ,new UpdateCursor(def[11])
         ,new UpdateCursor(def[12])
         ,new ForEachCursor(def[13])
         ,new ForEachCursor(def[14])
         ,new ForEachCursor(def[15])
         ,new ForEachCursor(def[16])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmT00096 ;
          prmT00096 = new Object[] {
          new Object[] {"@Indicadorid",SqlDbType.Int,9,0}
          } ;
          Object[] prmT00094 ;
          prmT00094 = new Object[] {
          new Object[] {"@Proyectoid",SqlDbType.Int,9,0}
          } ;
          Object[] prmT00095 ;
          prmT00095 = new Object[] {
          new Object[] {"@Paisid",SqlDbType.Int,9,0}
          } ;
          Object[] prmT00097 ;
          prmT00097 = new Object[] {
          new Object[] {"@Proyectoid",SqlDbType.Int,9,0}
          } ;
          Object[] prmT00098 ;
          prmT00098 = new Object[] {
          new Object[] {"@Paisid",SqlDbType.Int,9,0}
          } ;
          Object[] prmT00099 ;
          prmT00099 = new Object[] {
          new Object[] {"@Indicadorid",SqlDbType.Int,9,0}
          } ;
          Object[] prmT00093 ;
          prmT00093 = new Object[] {
          new Object[] {"@Indicadorid",SqlDbType.Int,9,0}
          } ;
          Object[] prmT000910 ;
          prmT000910 = new Object[] {
          new Object[] {"@Indicadorid",SqlDbType.Int,9,0}
          } ;
          Object[] prmT000911 ;
          prmT000911 = new Object[] {
          new Object[] {"@Indicadorid",SqlDbType.Int,9,0}
          } ;
          Object[] prmT00092 ;
          prmT00092 = new Object[] {
          new Object[] {"@Indicadorid",SqlDbType.Int,9,0}
          } ;
          Object[] prmT000912 ;
          prmT000912 = new Object[] {
          new Object[] {"@resultado",SqlDbType.VarChar,400,0} ,
          new Object[] {"@objetivo",SqlDbType.VarChar,300,0} ,
          new Object[] {"@objPlan",SqlDbType.VarChar,400,0} ,
          new Object[] {"@accion",SqlDbType.VarChar,300,0} ,
          new Object[] {"@accionPlan",SqlDbType.VarChar,400,0} ,
          new Object[] {"@donor",SqlDbType.SmallInt,1,0} ,
          new Object[] {"@milestone",SqlDbType.VarChar,300,0} ,
          new Object[] {"@Proyectoid",SqlDbType.Int,9,0} ,
          new Object[] {"@Paisid",SqlDbType.Int,9,0}
          } ;
          Object[] prmT000913 ;
          prmT000913 = new Object[] {
          new Object[] {"@resultado",SqlDbType.VarChar,400,0} ,
          new Object[] {"@objetivo",SqlDbType.VarChar,300,0} ,
          new Object[] {"@objPlan",SqlDbType.VarChar,400,0} ,
          new Object[] {"@accion",SqlDbType.VarChar,300,0} ,
          new Object[] {"@accionPlan",SqlDbType.VarChar,400,0} ,
          new Object[] {"@donor",SqlDbType.SmallInt,1,0} ,
          new Object[] {"@milestone",SqlDbType.VarChar,300,0} ,
          new Object[] {"@Proyectoid",SqlDbType.Int,9,0} ,
          new Object[] {"@Paisid",SqlDbType.Int,9,0} ,
          new Object[] {"@Indicadorid",SqlDbType.Int,9,0}
          } ;
          Object[] prmT000914 ;
          prmT000914 = new Object[] {
          new Object[] {"@Indicadorid",SqlDbType.Int,9,0}
          } ;
          Object[] prmT000917 ;
          prmT000917 = new Object[] {
          new Object[] {"@Indicadorid",SqlDbType.Int,9,0}
          } ;
          Object[] prmT000918 ;
          prmT000918 = new Object[] {
          } ;
          Object[] prmT000915 ;
          prmT000915 = new Object[] {
          new Object[] {"@Proyectoid",SqlDbType.Int,9,0}
          } ;
          Object[] prmT000916 ;
          prmT000916 = new Object[] {
          new Object[] {"@Paisid",SqlDbType.Int,9,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("T00092", "SELECT [id] AS Indicadorid, [resultado], [objetivo], [objPlan], [accion], [accionPlan], [donor], [milestone], [idProyecto] AS Proyectoid, [idPais] AS Paisid FROM dbo.[Indicador] WITH (UPDLOCK) WHERE [id] = @Indicadorid ",true, GxErrorMask.GX_NOMASK, false, this,prmT00092,1,0,true,false )
             ,new CursorDef("T00093", "SELECT [id] AS Indicadorid, [resultado], [objetivo], [objPlan], [accion], [accionPlan], [donor], [milestone], [idProyecto] AS Proyectoid, [idPais] AS Paisid FROM dbo.[Indicador] WITH (NOLOCK) WHERE [id] = @Indicadorid ",true, GxErrorMask.GX_NOMASK, false, this,prmT00093,1,0,true,false )
             ,new CursorDef("T00094", "SELECT [nombre] AS Proyectonombre FROM dbo.[Proyecto] WITH (NOLOCK) WHERE [id] = @Proyectoid ",true, GxErrorMask.GX_NOMASK, false, this,prmT00094,1,0,true,false )
             ,new CursorDef("T00095", "SELECT [nombre] AS Paisnombre FROM dbo.[Pais] WITH (NOLOCK) WHERE [id] = @Paisid ",true, GxErrorMask.GX_NOMASK, false, this,prmT00095,1,0,true,false )
             ,new CursorDef("T00096", "SELECT TM1.[id] AS Indicadorid, TM1.[resultado], TM1.[objetivo], TM1.[objPlan], T2.[nombre] AS Proyectonombre, T3.[nombre] AS Paisnombre, TM1.[accion], TM1.[accionPlan], TM1.[donor], TM1.[milestone], TM1.[idProyecto] AS Proyectoid, TM1.[idPais] AS Paisid FROM ((dbo.[Indicador] TM1 WITH (NOLOCK) INNER JOIN dbo.[Proyecto] T2 WITH (NOLOCK) ON T2.[id] = TM1.[idProyecto]) INNER JOIN dbo.[Pais] T3 WITH (NOLOCK) ON T3.[id] = TM1.[idPais]) WHERE TM1.[id] = @Indicadorid ORDER BY TM1.[id]  OPTION (FAST 100)",true, GxErrorMask.GX_NOMASK, false, this,prmT00096,100,0,true,false )
             ,new CursorDef("T00097", "SELECT [nombre] AS Proyectonombre FROM dbo.[Proyecto] WITH (NOLOCK) WHERE [id] = @Proyectoid ",true, GxErrorMask.GX_NOMASK, false, this,prmT00097,1,0,true,false )
             ,new CursorDef("T00098", "SELECT [nombre] AS Paisnombre FROM dbo.[Pais] WITH (NOLOCK) WHERE [id] = @Paisid ",true, GxErrorMask.GX_NOMASK, false, this,prmT00098,1,0,true,false )
             ,new CursorDef("T00099", "SELECT [id] AS Indicadorid FROM dbo.[Indicador] WITH (NOLOCK) WHERE [id] = @Indicadorid  OPTION (FAST 1)",true, GxErrorMask.GX_NOMASK, false, this,prmT00099,1,0,true,false )
             ,new CursorDef("T000910", "SELECT TOP 1 [id] AS Indicadorid FROM dbo.[Indicador] WITH (NOLOCK) WHERE ( [id] > @Indicadorid) ORDER BY [id]  OPTION (FAST 1)",true, GxErrorMask.GX_NOMASK, false, this,prmT000910,1,0,true,true )
             ,new CursorDef("T000911", "SELECT TOP 1 [id] AS Indicadorid FROM dbo.[Indicador] WITH (NOLOCK) WHERE ( [id] < @Indicadorid) ORDER BY [id] DESC  OPTION (FAST 1)",true, GxErrorMask.GX_NOMASK, false, this,prmT000911,1,0,true,true )
             ,new CursorDef("T000912", "INSERT INTO dbo.[Indicador]([resultado], [objetivo], [objPlan], [accion], [accionPlan], [donor], [milestone], [idProyecto], [idPais]) VALUES(@resultado, @objetivo, @objPlan, @accion, @accionPlan, @donor, @milestone, @Proyectoid, @Paisid); SELECT SCOPE_IDENTITY()", GxErrorMask.GX_NOMASK,prmT000912)
             ,new CursorDef("T000913", "UPDATE dbo.[Indicador] SET [resultado]=@resultado, [objetivo]=@objetivo, [objPlan]=@objPlan, [accion]=@accion, [accionPlan]=@accionPlan, [donor]=@donor, [milestone]=@milestone, [idProyecto]=@Proyectoid, [idPais]=@Paisid  WHERE [id] = @Indicadorid", GxErrorMask.GX_NOMASK,prmT000913)
             ,new CursorDef("T000914", "DELETE FROM dbo.[Indicador]  WHERE [id] = @Indicadorid", GxErrorMask.GX_NOMASK,prmT000914)
             ,new CursorDef("T000915", "SELECT [nombre] AS Proyectonombre FROM dbo.[Proyecto] WITH (NOLOCK) WHERE [id] = @Proyectoid ",true, GxErrorMask.GX_NOMASK, false, this,prmT000915,1,0,true,false )
             ,new CursorDef("T000916", "SELECT [nombre] AS Paisnombre FROM dbo.[Pais] WITH (NOLOCK) WHERE [id] = @Paisid ",true, GxErrorMask.GX_NOMASK, false, this,prmT000916,1,0,true,false )
             ,new CursorDef("T000917", "SELECT TOP 1 [id] AS Actividadid FROM dbo.[Actividad] WITH (NOLOCK) WHERE [idIndicador] = @Indicadorid ",true, GxErrorMask.GX_NOMASK, false, this,prmT000917,1,0,true,true )
             ,new CursorDef("T000918", "SELECT [id] AS Indicadorid FROM dbo.[Indicador] WITH (NOLOCK) ORDER BY [id]  OPTION (FAST 100)",true, GxErrorMask.GX_NOMASK, false, this,prmT000918,100,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getVarchar(2) ;
                ((String[]) buf[2])[0] = rslt.getVarchar(3) ;
                ((String[]) buf[3])[0] = rslt.getVarchar(4) ;
                ((String[]) buf[4])[0] = rslt.getVarchar(5) ;
                ((String[]) buf[5])[0] = rslt.getVarchar(6) ;
                ((short[]) buf[6])[0] = rslt.getShort(7) ;
                ((String[]) buf[7])[0] = rslt.getVarchar(8) ;
                ((bool[]) buf[8])[0] = rslt.wasNull(8);
                ((int[]) buf[9])[0] = rslt.getInt(9) ;
                ((int[]) buf[10])[0] = rslt.getInt(10) ;
                return;
             case 1 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getVarchar(2) ;
                ((String[]) buf[2])[0] = rslt.getVarchar(3) ;
                ((String[]) buf[3])[0] = rslt.getVarchar(4) ;
                ((String[]) buf[4])[0] = rslt.getVarchar(5) ;
                ((String[]) buf[5])[0] = rslt.getVarchar(6) ;
                ((short[]) buf[6])[0] = rslt.getShort(7) ;
                ((String[]) buf[7])[0] = rslt.getVarchar(8) ;
                ((bool[]) buf[8])[0] = rslt.wasNull(8);
                ((int[]) buf[9])[0] = rslt.getInt(9) ;
                ((int[]) buf[10])[0] = rslt.getInt(10) ;
                return;
             case 2 :
                ((String[]) buf[0])[0] = rslt.getVarchar(1) ;
                return;
             case 3 :
                ((String[]) buf[0])[0] = rslt.getVarchar(1) ;
                return;
             case 4 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getVarchar(2) ;
                ((String[]) buf[2])[0] = rslt.getVarchar(3) ;
                ((String[]) buf[3])[0] = rslt.getVarchar(4) ;
                ((String[]) buf[4])[0] = rslt.getVarchar(5) ;
                ((String[]) buf[5])[0] = rslt.getVarchar(6) ;
                ((String[]) buf[6])[0] = rslt.getVarchar(7) ;
                ((String[]) buf[7])[0] = rslt.getVarchar(8) ;
                ((short[]) buf[8])[0] = rslt.getShort(9) ;
                ((String[]) buf[9])[0] = rslt.getVarchar(10) ;
                ((bool[]) buf[10])[0] = rslt.wasNull(10);
                ((int[]) buf[11])[0] = rslt.getInt(11) ;
                ((int[]) buf[12])[0] = rslt.getInt(12) ;
                return;
             case 5 :
                ((String[]) buf[0])[0] = rslt.getVarchar(1) ;
                return;
             case 6 :
                ((String[]) buf[0])[0] = rslt.getVarchar(1) ;
                return;
             case 7 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 8 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 9 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 10 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 13 :
                ((String[]) buf[0])[0] = rslt.getVarchar(1) ;
                return;
             case 14 :
                ((String[]) buf[0])[0] = rslt.getVarchar(1) ;
                return;
             case 15 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 16 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 1 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 2 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 3 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 4 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 5 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 6 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 7 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 8 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 9 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 10 :
                stmt.SetParameter(1, (String)parms[0]);
                stmt.SetParameter(2, (String)parms[1]);
                stmt.SetParameter(3, (String)parms[2]);
                stmt.SetParameter(4, (String)parms[3]);
                stmt.SetParameter(5, (String)parms[4]);
                stmt.SetParameter(6, (short)parms[5]);
                if ( (bool)parms[6] )
                {
                   stmt.setNull( 7 , SqlDbType.VarChar );
                }
                else
                {
                   stmt.SetParameter(7, (String)parms[7]);
                }
                stmt.SetParameter(8, (int)parms[8]);
                stmt.SetParameter(9, (int)parms[9]);
                return;
             case 11 :
                stmt.SetParameter(1, (String)parms[0]);
                stmt.SetParameter(2, (String)parms[1]);
                stmt.SetParameter(3, (String)parms[2]);
                stmt.SetParameter(4, (String)parms[3]);
                stmt.SetParameter(5, (String)parms[4]);
                stmt.SetParameter(6, (short)parms[5]);
                if ( (bool)parms[6] )
                {
                   stmt.setNull( 7 , SqlDbType.VarChar );
                }
                else
                {
                   stmt.SetParameter(7, (String)parms[7]);
                }
                stmt.SetParameter(8, (int)parms[8]);
                stmt.SetParameter(9, (int)parms[9]);
                stmt.SetParameter(10, (int)parms[10]);
                return;
             case 12 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 13 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 14 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 15 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
       }
    }

    public String getDataStoreName( )
    {
       return "DATASTORE1";
    }

 }

 public class indicador__default : DataStoreHelperBase, IDataStoreHelper
 {
    public ICursor[] getCursors( )
    {
       cursorDefinitions();
       return new Cursor[] {
     };
  }

  private static CursorDef[] def;
  private void cursorDefinitions( )
  {
     if ( def == null )
     {
        def= new CursorDef[] {
        };
     }
  }

  public void getResults( int cursor ,
                          IFieldGetter rslt ,
                          Object[] buf )
  {
     switch ( cursor )
     {
     }
  }

  public void setParameters( int cursor ,
                             IFieldSetter stmt ,
                             Object[] parms )
  {
     switch ( cursor )
     {
     }
  }

}

}
