/*
               File: Informe_Actividad
        Description: Informe_Actividad
             Author: GeneXus C# Generator version 16_0_0-127771
       Generated on: 1/5/2019 14:56:54.6
       Program type: Callable routine
          Main DBMS: SQL Server
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Xml.Serialization;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class informe_actividad : GXDataArea, System.Web.SessionState.IRequiresSessionState
   {
      protected void INITENV( )
      {
         if ( GxWebError != 0 )
         {
            return  ;
         }
      }

      protected void INITTRN( )
      {
         initialize_properties( ) ;
         entryPointCalled = false;
         gxfirstwebparm = GetNextPar( );
         gxfirstwebparm_bkp = gxfirstwebparm;
         gxfirstwebparm = DecryptAjaxCall( gxfirstwebparm);
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         if ( StringUtil.StrCmp(gxfirstwebparm, "dyncall") == 0 )
         {
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            dyncall( GetNextPar( )) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxExecAct_"+"gxLoad_10") == 0 )
         {
            A13Planid = (int)(NumberUtil.Val( GetNextPar( ), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A13Planid", StringUtil.LTrim( StringUtil.Str( (decimal)(A13Planid), 9, 0)));
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxLoad_10( A13Planid) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxEvt") == 0 )
         {
            setAjaxEventMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxfirstwebparm = GetNextPar( );
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxfullajaxEvt") == 0 )
         {
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxfirstwebparm = GetNextPar( );
         }
         else
         {
            if ( ! IsValidAjaxCall( false) )
            {
               GxWebError = 1;
               return  ;
            }
            gxfirstwebparm = gxfirstwebparm_bkp;
         }
         if ( ! entryPointCalled && ! ( isAjaxCallMode( ) || isFullAjaxMode( ) ) )
         {
            Gx_mode = gxfirstwebparm;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!")), context));
            if ( StringUtil.StrCmp(gxfirstwebparm, "viewer") != 0 )
            {
               AV7Informe_Actividadid = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV7Informe_Actividadid", StringUtil.LTrim( StringUtil.Str( (decimal)(AV7Informe_Actividadid), 9, 0)));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vINFORME_ACTIVIDADID", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV7Informe_Actividadid), "ZZZZZZZZ9"), context));
            }
         }
         if ( context.isSpaRequest( ) )
         {
            enableJsOutput();
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.GetCookie( "GX_SESSION_ID"))) )
         {
            gxcookieaux = context.SetCookie( "GX_SESSION_ID", Encrypt64( Crypto.GetEncryptionKey( ), Crypto.GetServerKey( )), "", (DateTime)(DateTime.MinValue), "", 0);
         }
         GXKey = Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
         toggleJsOutput = isJsOutputEnabled( );
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         init_web_controls( ) ;
         if ( toggleJsOutput )
         {
            if ( context.isSpaRequest( ) )
            {
               enableJsOutput();
            }
         }
         if ( ! context.isSpaRequest( ) )
         {
            Form.Meta.addItem("generator", "GeneXus C# 16_0_0-127771", 0) ;
            Form.Meta.addItem("description", "Informe_Actividad", 0) ;
         }
         context.wjLoc = "";
         context.nUserReturn = 0;
         context.wbHandled = 0;
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
         }
         GX_FocusControl = edtPlanid_Internalname;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         wbErr = false;
         context.SetDefaultTheme("Carmine");
         if ( ! context.IsLocalStorageSupported( ) )
         {
            context.PushCurrentUrl();
         }
      }

      public informe_actividad( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsDataStore1 = context.GetDataStore("DataStore1");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("Carmine");
      }

      public informe_actividad( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsDataStore1 = context.GetDataStore("DataStore1");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( String aP0_Gx_mode ,
                           int aP1_Informe_Actividadid )
      {
         this.Gx_mode = aP0_Gx_mode;
         this.AV7Informe_Actividadid = aP1_Informe_Actividadid;
         executePrivate();
      }

      void executePrivate( )
      {
         isStatic = false;
         webExecute();
      }

      protected override void createObjects( )
      {
      }

      public override void webExecute( )
      {
         if ( initialized == 0 )
         {
            createObjects();
            initialize();
         }
         INITENV( ) ;
         INITTRN( ) ;
         if ( ( GxWebError == 0 ) && ! isAjaxCallMode( ) )
         {
            MasterPageObj = (GXMasterPage) ClassLoader.GetInstance("rwdmasterpage", "GeneXus.Programs.rwdmasterpage", new Object[] {new GxContext( context.handle, context.DataStores, context.HttpContext)});
            MasterPageObj.setDataArea(this,false);
            ValidateSpaRequest();
            MasterPageObj.webExecute();
            if ( ( GxWebError == 0 ) && context.isAjaxRequest( ) )
            {
               enableOutput();
               if ( ! context.isAjaxRequest( ) )
               {
                  context.GX_webresponse.AppendHeader("Cache-Control", "no-store");
               }
               if ( ! context.WillRedirect( ) )
               {
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               }
               else
               {
                  if ( context.isAjaxRequest( ) )
                  {
                     disableOutput();
                  }
                  RenderHtmlHeaders( ) ;
                  context.Redirect( context.wjLoc );
                  context.DispatchAjaxCommands();
               }
            }
         }
         this.cleanup();
      }

      protected void fix_multi_value_controls( )
      {
      }

      protected void Draw( )
      {
         if ( context.isAjaxRequest( ) )
         {
            disableOutput();
         }
         if ( ! GxWebStd.gx_redirect( context) )
         {
            disable_std_buttons( ) ;
            enableDisable( ) ;
            set_caption( ) ;
            /* Form start */
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "Section", "left", "top", " "+"data-gx-base-lib=\"bootstrapv3\""+" "+"data-abstract-form"+" ", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, divMaintable_Internalname, 1, 0, "px", 0, "px", "WWAdvancedContainer", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12 col-sm-8 col-sm-offset-2", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, divTitlecontainer_Internalname, 1, 0, "px", 0, "px", "TableTop", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12", "left", "top", "", "", "div");
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTitle_Internalname, "Informe_Actividad", "", "", lblTitle_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "Title", 0, "", 1, 1, 0, "HLP_Informe_Actividad.htm");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12", "left", "top", "", "", "div");
            ClassString = "ErrorViewer";
            StyleString = "";
            GxWebStd.gx_msg_list( context, "", context.GX_msglist.DisplayMode, StyleString, ClassString, "", "false");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12 col-sm-8 col-sm-offset-2", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, divFormcontainer_Internalname, 1, 0, "px", 0, "px", "FormContainer", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, divToolbarcell_Internalname, 1, 0, "px", 0, "px", "col-xs-12 col-sm-9 col-sm-offset-3 ToolbarCellClass", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "gx-action-group ActionGroup", "left", "top", " "+"data-gx-actiongroup-type=\"toolbar\""+" ", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "btn-group", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "gx-button", "left", "top", "", "", "div");
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 21,'',false,'',0)\"";
            ClassString = "BtnFirst";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtn_first_Internalname, "", "", bttBtn_first_Jsonclick, 5, "", "", StyleString, ClassString, bttBtn_first_Visible, 0, "standard", "'"+""+"'"+",false,"+"'"+"EFIRST."+"'", TempTags, "", context.GetButtonType( ), "HLP_Informe_Actividad.htm");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "gx-button", "left", "top", "", "", "div");
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 23,'',false,'',0)\"";
            ClassString = "BtnPrevious";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtn_previous_Internalname, "", "", bttBtn_previous_Jsonclick, 5, "", "", StyleString, ClassString, bttBtn_previous_Visible, 0, "standard", "'"+""+"'"+",false,"+"'"+"EPREVIOUS."+"'", TempTags, "", context.GetButtonType( ), "HLP_Informe_Actividad.htm");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "gx-button", "left", "top", "", "", "div");
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 25,'',false,'',0)\"";
            ClassString = "BtnNext";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtn_next_Internalname, "", "", bttBtn_next_Jsonclick, 5, "", "", StyleString, ClassString, bttBtn_next_Visible, 0, "standard", "'"+""+"'"+",false,"+"'"+"ENEXT."+"'", TempTags, "", context.GetButtonType( ), "HLP_Informe_Actividad.htm");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "gx-button", "left", "top", "", "", "div");
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 27,'',false,'',0)\"";
            ClassString = "BtnLast";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtn_last_Internalname, "", "", bttBtn_last_Jsonclick, 5, "", "", StyleString, ClassString, bttBtn_last_Visible, 0, "standard", "'"+""+"'"+",false,"+"'"+"ELAST."+"'", TempTags, "", context.GetButtonType( ), "HLP_Informe_Actividad.htm");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "gx-button", "left", "top", "", "", "div");
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 29,'',false,'',0)\"";
            ClassString = "BtnSelect";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtn_select_Internalname, "", "Select", bttBtn_select_Jsonclick, 5, "Select", "", StyleString, ClassString, bttBtn_select_Visible, 0, "standard", "'"+""+"'"+",false,"+"'"+"ESELECT."+"'", TempTags, "", 2, "HLP_Informe_Actividad.htm");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12 FormCellAdvanced", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "form-group gx-form-group", "left", "top", ""+" data-gx-for=\""+edtPlanid_Internalname+"\"", "", "div");
            /* Attribute/Variable Label */
            GxWebStd.gx_label_element( context, edtPlanid_Internalname, "Plan", "col-sm-3 AttributeLabel", 1, true);
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-sm-9 gx-attribute", "left", "top", "", "", "div");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 34,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtPlanid_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A13Planid), 9, 0, ".", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(A13Planid), "ZZZZZZZZ9")), TempTags+" onchange=\""+"gx.num.valid_integer( this,',');"+";gx.evt.onchange(this, event)\" "+" onblur=\""+";gx.evt.onblur(this,34);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtPlanid_Jsonclick, 0, "Attribute", "", "", "", "", 1, edtPlanid_Enabled, 1, "number", "1", 9, "chr", 1, "row", 9, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_Informe_Actividad.htm");
            /* Static images/pictures */
            ClassString = "gx-prompt Image";
            StyleString = "";
            sImgUrl = (String)(context.GetImagePath( "prompt.gif", "", context.GetTheme( )));
            GxWebStd.gx_bitmap( context, imgprompt_13_Internalname, sImgUrl, imgprompt_13_Link, "", "", context.GetTheme( ), imgprompt_13_Visible, 1, "", "", 0, 0, 0, "", 0, "", 0, 0, 0, "", "", StyleString, ClassString, "", "", "", "", "", "", "", 1, false, false, context.GetImageSrcSet( sImgUrl), "HLP_Informe_Actividad.htm");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12 FormCell", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "form-group gx-form-group", "left", "top", ""+" data-gx-for=\""+edtresultados_Internalname+"\"", "", "div");
            /* Attribute/Variable Label */
            GxWebStd.gx_label_element( context, edtresultados_Internalname, "Resultados", "col-sm-3 AttributeLabel", 1, true);
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-sm-9 gx-attribute", "left", "top", "", "", "div");
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 39,'',false,'',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtresultados_Internalname, A41resultados, "", TempTags+" onchange=\""+""+";gx.evt.onchange(this, event)\" "+" onblur=\""+";gx.evt.onblur(this,39);\"", 0, 1, edtresultados_Enabled, 0, 80, "chr", 7, "row", StyleString, ClassString, "", "", "500", -1, 0, "", "", -1, true, "", "'"+""+"'"+",false,"+"'"+""+"'", 0, "HLP_Informe_Actividad.htm");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12 FormCell", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "form-group gx-form-group", "left", "top", ""+" data-gx-for=\""+edtseguimiento_Internalname+"\"", "", "div");
            /* Attribute/Variable Label */
            GxWebStd.gx_label_element( context, edtseguimiento_Internalname, "Seguimiento", "col-sm-3 AttributeLabel", 1, true);
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-sm-9 gx-attribute", "left", "top", "", "", "div");
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 44,'',false,'',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtseguimiento_Internalname, A42seguimiento, "", TempTags+" onchange=\""+""+";gx.evt.onchange(this, event)\" "+" onblur=\""+";gx.evt.onblur(this,44);\"", 0, 1, edtseguimiento_Enabled, 0, 80, "chr", 7, "row", StyleString, ClassString, "", "", "500", -1, 0, "", "", -1, true, "", "'"+""+"'"+",false,"+"'"+""+"'", 0, "HLP_Informe_Actividad.htm");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12 FormCell", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "form-group gx-form-group", "left", "top", ""+" data-gx-for=\""+edtdificultades_Internalname+"\"", "", "div");
            /* Attribute/Variable Label */
            GxWebStd.gx_label_element( context, edtdificultades_Internalname, "Dificultades", "col-sm-3 AttributeLabel", 1, true);
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-sm-9 gx-attribute", "left", "top", "", "", "div");
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 49,'',false,'',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtdificultades_Internalname, A43dificultades, "", TempTags+" onchange=\""+""+";gx.evt.onchange(this, event)\" "+" onblur=\""+";gx.evt.onblur(this,49);\"", 0, 1, edtdificultades_Enabled, 0, 80, "chr", 7, "row", StyleString, ClassString, "", "", "500", -1, 0, "", "", -1, true, "", "'"+""+"'"+",false,"+"'"+""+"'", 0, "HLP_Informe_Actividad.htm");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12 FormCell", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "form-group gx-form-group", "left", "top", ""+" data-gx-for=\""+edtmejorar_Internalname+"\"", "", "div");
            /* Attribute/Variable Label */
            GxWebStd.gx_label_element( context, edtmejorar_Internalname, "Mejorar", "col-sm-3 AttributeLabel", 1, true);
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-sm-9 gx-attribute", "left", "top", "", "", "div");
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 54,'',false,'',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtmejorar_Internalname, A44mejorar, "", TempTags+" onchange=\""+""+";gx.evt.onchange(this, event)\" "+" onblur=\""+";gx.evt.onblur(this,54);\"", 0, 1, edtmejorar_Enabled, 0, 80, "chr", 7, "row", StyleString, ClassString, "", "", "500", -1, 0, "", "", -1, true, "", "'"+""+"'"+",false,"+"'"+""+"'", 0, "HLP_Informe_Actividad.htm");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12 FormCell", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "form-group gx-form-group", "left", "top", ""+" data-gx-for=\""+edtcomentarios_Internalname+"\"", "", "div");
            /* Attribute/Variable Label */
            GxWebStd.gx_label_element( context, edtcomentarios_Internalname, "Comentarios", "col-sm-3 AttributeLabel", 1, true);
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-sm-9 gx-attribute", "left", "top", "", "", "div");
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 59,'',false,'',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtcomentarios_Internalname, A45comentarios, "", TempTags+" onchange=\""+""+";gx.evt.onchange(this, event)\" "+" onblur=\""+";gx.evt.onblur(this,59);\"", 0, 1, edtcomentarios_Enabled, 0, 80, "chr", 7, "row", StyleString, ClassString, "", "", "500", -1, 0, "", "", -1, true, "", "'"+""+"'"+",false,"+"'"+""+"'", 0, "HLP_Informe_Actividad.htm");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12 FormCell", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "form-group gx-form-group", "left", "top", ""+" data-gx-for=\""+edtfecha_Internalname+"\"", "", "div");
            /* Attribute/Variable Label */
            GxWebStd.gx_label_element( context, edtfecha_Internalname, "Fecha/Informe", "col-sm-3 AttributeLabel", 1, true);
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-sm-9 gx-attribute", "left", "top", "", "", "div");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 64,'',false,'',0)\"";
            context.WriteHtmlText( "<div id=\""+edtfecha_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtfecha_Internalname, context.localUtil.Format(A46fecha, "99/99/99"), context.localUtil.Format( A46fecha, "99/99/99"), TempTags+" onchange=\""+"gx.date.valid_date(this, 8,'MDY',0,12,'eng',false,0);"+";gx.evt.onchange(this, event)\" "+" onblur=\""+";gx.evt.onblur(this,64);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtfecha_Jsonclick, 0, "Attribute", "", "", "", "", 1, edtfecha_Enabled, 0, "text", "", 8, "chr", 1, "row", 8, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_Informe_Actividad.htm");
            GxWebStd.gx_bitmap( context, edtfecha_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((1==0)||(edtfecha_Enabled==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", "", 1, false, false, "", "HLP_Informe_Actividad.htm");
            context.WriteHtmlTextNl( "</div>") ;
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12 FormCell", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "form-group gx-form-group", "left", "top", ""+" data-gx-for=\""+edtfechaActividad_Internalname+"\"", "", "div");
            /* Attribute/Variable Label */
            GxWebStd.gx_label_element( context, edtfechaActividad_Internalname, "Fecha Actividad", "col-sm-3 AttributeLabel", 1, true);
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-sm-9 gx-attribute", "left", "top", "", "", "div");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 69,'',false,'',0)\"";
            context.WriteHtmlText( "<div id=\""+edtfechaActividad_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtfechaActividad_Internalname, context.localUtil.Format(A47fechaActividad, "99/99/99"), context.localUtil.Format( A47fechaActividad, "99/99/99"), TempTags+" onchange=\""+"gx.date.valid_date(this, 8,'MDY',0,12,'eng',false,0);"+";gx.evt.onchange(this, event)\" "+" onblur=\""+";gx.evt.onblur(this,69);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtfechaActividad_Jsonclick, 0, "Attribute", "", "", "", "", 1, edtfechaActividad_Enabled, 0, "text", "", 8, "chr", 1, "row", 8, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_Informe_Actividad.htm");
            GxWebStd.gx_bitmap( context, edtfechaActividad_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((1==0)||(edtfechaActividad_Enabled==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", "", 1, false, false, "", "HLP_Informe_Actividad.htm");
            context.WriteHtmlTextNl( "</div>") ;
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12", "Center", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "gx-action-group Confirm", "left", "top", " "+"data-gx-actiongroup-type=\"toolbar\""+" ", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "gx-button", "left", "top", "", "", "div");
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 74,'',false,'',0)\"";
            ClassString = "BtnEnter";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtn_enter_Internalname, "", "Confirm", bttBtn_enter_Jsonclick, 5, "Confirm", "", StyleString, ClassString, bttBtn_enter_Visible, bttBtn_enter_Enabled, "standard", "'"+""+"'"+",false,"+"'"+"EENTER."+"'", TempTags, "", context.GetButtonType( ), "HLP_Informe_Actividad.htm");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "gx-button", "left", "top", "", "", "div");
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 76,'',false,'',0)\"";
            ClassString = "BtnCancel";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtn_cancel_Internalname, "", "Cancel", bttBtn_cancel_Jsonclick, 1, "Cancel", "", StyleString, ClassString, bttBtn_cancel_Visible, 1, "standard", "'"+""+"'"+",false,"+"'"+"ECANCEL."+"'", TempTags, "", context.GetButtonType( ), "HLP_Informe_Actividad.htm");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "gx-button", "left", "top", "", "", "div");
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 78,'',false,'',0)\"";
            ClassString = "BtnDelete";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtn_delete_Internalname, "", "Delete", bttBtn_delete_Jsonclick, 5, "Delete", "", StyleString, ClassString, bttBtn_delete_Visible, bttBtn_delete_Enabled, "standard", "'"+""+"'"+",false,"+"'"+"EDELETE."+"'", TempTags, "", context.GetButtonType( ), "HLP_Informe_Actividad.htm");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "Center", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            fix_multi_value_controls( ) ;
         }
         /* Execute Exit event if defined. */
      }

      protected void UserMain( )
      {
         standaloneStartup( ) ;
      }

      protected void UserMainFullajax( )
      {
         INITENV( ) ;
         INITTRN( ) ;
         UserMain( ) ;
         Draw( ) ;
         SendCloseFormHiddens( ) ;
      }

      protected void standaloneStartup( )
      {
         standaloneStartupServer( ) ;
         disable_std_buttons( ) ;
         enableDisable( ) ;
         Process( ) ;
      }

      protected void standaloneStartupServer( )
      {
         /* Execute Start event if defined. */
         context.wbGlbDoneStart = 0;
         /* Execute user event: Start */
         E11072 ();
         context.wbGlbDoneStart = 1;
         assign_properties_default( ) ;
         if ( AnyError == 0 )
         {
            if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
            {
               /* Read saved SDTs. */
               /* Read variables values. */
               if ( ( ( context.localUtil.CToN( cgiGet( edtPlanid_Internalname), ".", ",") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtPlanid_Internalname), ".", ",") > Convert.ToDecimal( 999999999 )) ) )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "PLANID");
                  AnyError = 1;
                  GX_FocusControl = edtPlanid_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  wbErr = true;
                  A13Planid = 0;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A13Planid", StringUtil.LTrim( StringUtil.Str( (decimal)(A13Planid), 9, 0)));
               }
               else
               {
                  A13Planid = (int)(context.localUtil.CToN( cgiGet( edtPlanid_Internalname), ".", ","));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A13Planid", StringUtil.LTrim( StringUtil.Str( (decimal)(A13Planid), 9, 0)));
               }
               A41resultados = cgiGet( edtresultados_Internalname);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A41resultados", A41resultados);
               A42seguimiento = cgiGet( edtseguimiento_Internalname);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A42seguimiento", A42seguimiento);
               A43dificultades = cgiGet( edtdificultades_Internalname);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A43dificultades", A43dificultades);
               A44mejorar = cgiGet( edtmejorar_Internalname);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A44mejorar", A44mejorar);
               A45comentarios = cgiGet( edtcomentarios_Internalname);
               n45comentarios = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A45comentarios", A45comentarios);
               n45comentarios = (String.IsNullOrEmpty(StringUtil.RTrim( A45comentarios)) ? true : false);
               if ( context.localUtil.VCDate( cgiGet( edtfecha_Internalname), 1) == 0 )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_faildate", new   object[]  {"Fecha del Informe"}), 1, "FECHA");
                  AnyError = 1;
                  GX_FocusControl = edtfecha_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  wbErr = true;
                  A46fecha = DateTime.MinValue;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A46fecha", context.localUtil.Format(A46fecha, "99/99/99"));
               }
               else
               {
                  A46fecha = context.localUtil.CToD( cgiGet( edtfecha_Internalname), 1);
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A46fecha", context.localUtil.Format(A46fecha, "99/99/99"));
               }
               if ( context.localUtil.VCDate( cgiGet( edtfechaActividad_Internalname), 1) == 0 )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_faildate", new   object[]  {"Fecha de la Actividad"}), 1, "FECHAACTIVIDAD");
                  AnyError = 1;
                  GX_FocusControl = edtfechaActividad_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  wbErr = true;
                  A47fechaActividad = DateTime.MinValue;
                  n47fechaActividad = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A47fechaActividad", context.localUtil.Format(A47fechaActividad, "99/99/99"));
               }
               else
               {
                  A47fechaActividad = context.localUtil.CToD( cgiGet( edtfechaActividad_Internalname), 1);
                  n47fechaActividad = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A47fechaActividad", context.localUtil.Format(A47fechaActividad, "99/99/99"));
               }
               n47fechaActividad = ((DateTime.MinValue==A47fechaActividad) ? true : false);
               /* Read saved values. */
               Z11Informe_Actividadid = (int)(context.localUtil.CToN( cgiGet( "Z11Informe_Actividadid"), ".", ","));
               Z41resultados = cgiGet( "Z41resultados");
               Z42seguimiento = cgiGet( "Z42seguimiento");
               Z43dificultades = cgiGet( "Z43dificultades");
               Z44mejorar = cgiGet( "Z44mejorar");
               Z45comentarios = cgiGet( "Z45comentarios");
               n45comentarios = (String.IsNullOrEmpty(StringUtil.RTrim( A45comentarios)) ? true : false);
               Z46fecha = context.localUtil.CToD( cgiGet( "Z46fecha"), 0);
               Z47fechaActividad = context.localUtil.CToD( cgiGet( "Z47fechaActividad"), 0);
               n47fechaActividad = ((DateTime.MinValue==A47fechaActividad) ? true : false);
               Z13Planid = (int)(context.localUtil.CToN( cgiGet( "Z13Planid"), ".", ","));
               IsConfirmed = (short)(context.localUtil.CToN( cgiGet( "IsConfirmed"), ".", ","));
               IsModified = (short)(context.localUtil.CToN( cgiGet( "IsModified"), ".", ","));
               Gx_mode = cgiGet( "Mode");
               N13Planid = (int)(context.localUtil.CToN( cgiGet( "N13Planid"), ".", ","));
               AV7Informe_Actividadid = (int)(context.localUtil.CToN( cgiGet( "vINFORME_ACTIVIDADID"), ".", ","));
               A11Informe_Actividadid = (int)(context.localUtil.CToN( cgiGet( "INFORME_ACTIVIDADID"), ".", ","));
               AV11Insert_Planid = (int)(context.localUtil.CToN( cgiGet( "vINSERT_PLANID"), ".", ","));
               AV13Pgmname = cgiGet( "vPGMNAME");
               Gx_mode = cgiGet( "vMODE");
               /* Read subfile selected row values. */
               /* Read hidden variables. */
               GXKey = Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
               forbiddenHiddens = "hsh" + "Informe_Actividad";
               forbiddenHiddens = forbiddenHiddens + context.localUtil.Format( (decimal)(AV11Insert_Planid), "ZZZZZZZZ9");
               forbiddenHiddens = forbiddenHiddens + StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"));
               forbiddenHiddens = forbiddenHiddens + context.localUtil.Format( (decimal)(A11Informe_Actividadid), "ZZZZZZZZ9");
               hsh = cgiGet( "hsh");
               if ( ! GXUtil.CheckEncryptedHash( forbiddenHiddens, hsh, GXKey) )
               {
                  GXUtil.WriteLog("informe_actividad:[SecurityCheckFailed value for]"+"Insert_Planid:"+context.localUtil.Format( (decimal)(AV11Insert_Planid), "ZZZZZZZZ9"));
                  GXUtil.WriteLog("informe_actividad:[SecurityCheckFailed value for]"+"Gx_mode:"+StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!")));
                  GXUtil.WriteLog("informe_actividad:[SecurityCheckFailed value for]"+"Informe_Actividadid:"+context.localUtil.Format( (decimal)(A11Informe_Actividadid), "ZZZZZZZZ9"));
                  GxWebError = 1;
                  context.HttpContext.Response.StatusDescription = 403.ToString();
                  context.HttpContext.Response.StatusCode = 403;
                  context.WriteHtmlText( "<title>403 Forbidden</title>") ;
                  context.WriteHtmlText( "<h1>403 Forbidden</h1>") ;
                  context.WriteHtmlText( "<p /><hr />") ;
                  GXUtil.WriteLog("send_http_error_code " + 403.ToString());
                  AnyError = 1;
                  return  ;
               }
               standaloneNotModal( ) ;
            }
            else
            {
               standaloneNotModal( ) ;
               if ( StringUtil.StrCmp(gxfirstwebparm, "viewer") == 0 )
               {
                  Gx_mode = "DSP";
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!")), context));
                  A11Informe_Actividadid = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A11Informe_Actividadid", StringUtil.LTrim( StringUtil.Str( (decimal)(A11Informe_Actividadid), 9, 0)));
                  getEqualNoModal( ) ;
                  Gx_mode = "DSP";
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!")), context));
                  disable_std_buttons( ) ;
                  standaloneModal( ) ;
               }
               else
               {
                  if ( StringUtil.StrCmp(Gx_mode, "DSP") == 0 )
                  {
                     sMode7 = Gx_mode;
                     Gx_mode = "UPD";
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!")), context));
                     Gx_mode = sMode7;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!")), context));
                  }
                  standaloneModal( ) ;
                  if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
                  {
                     getByPrimaryKey( ) ;
                     if ( RcdFound7 == 1 )
                     {
                        if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
                        {
                           /* Confirm record */
                           CONFIRM_070( ) ;
                           if ( AnyError == 0 )
                           {
                              GX_FocusControl = bttBtn_enter_Internalname;
                              context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                           }
                        }
                     }
                     else
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_noinsert", ""), 1, "");
                        AnyError = 1;
                     }
                  }
               }
            }
         }
      }

      protected void Process( )
      {
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            /* Read Transaction buttons. */
            sEvt = cgiGet( "_EventName");
            EvtGridId = cgiGet( "_EventGridId");
            EvtRowId = cgiGet( "_EventRowId");
            if ( StringUtil.Len( sEvt) > 0 )
            {
               sEvtType = StringUtil.Left( sEvt, 1);
               sEvt = StringUtil.Right( sEvt, (short)(StringUtil.Len( sEvt)-1));
               if ( StringUtil.StrCmp(sEvtType, "M") != 0 )
               {
                  if ( StringUtil.StrCmp(sEvtType, "E") == 0 )
                  {
                     sEvtType = StringUtil.Right( sEvt, 1);
                     if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                     {
                        sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                        if ( StringUtil.StrCmp(sEvt, "START") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: Start */
                           E11072 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "AFTER TRN") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: After Trn */
                           E12072 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "ENTER") == 0 )
                        {
                           context.wbHandled = 1;
                           if ( StringUtil.StrCmp(Gx_mode, "DSP") != 0 )
                           {
                              btn_enter( ) ;
                           }
                           /* No code required for Cancel button. It is implemented as the Reset button. */
                        }
                     }
                     else
                     {
                     }
                  }
                  context.wbHandled = 1;
               }
            }
         }
      }

      protected void AfterTrn( )
      {
         if ( trnEnded == 1 )
         {
            /* Execute user event: After Trn */
            E12072 ();
            trnEnded = 0;
            standaloneNotModal( ) ;
            standaloneModal( ) ;
            if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  )
            {
               /* Clear variables for new insertion. */
               InitAll077( ) ;
               standaloneNotModal( ) ;
               standaloneModal( ) ;
            }
         }
      }

      public override String ToString( )
      {
         return "" ;
      }

      public GxContentInfo GetContentInfo( )
      {
         return (GxContentInfo)(null) ;
      }

      protected void disable_std_buttons( )
      {
         bttBtn_delete_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_delete_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_delete_Visible), 5, 0)), true);
         bttBtn_first_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_first_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_first_Visible), 5, 0)), true);
         bttBtn_previous_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_previous_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_previous_Visible), 5, 0)), true);
         bttBtn_next_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_next_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_next_Visible), 5, 0)), true);
         bttBtn_last_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_last_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_last_Visible), 5, 0)), true);
         bttBtn_select_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_select_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_select_Visible), 5, 0)), true);
         if ( ( StringUtil.StrCmp(Gx_mode, "DSP") == 0 ) || ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 ) )
         {
            bttBtn_delete_Visible = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_delete_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_delete_Visible), 5, 0)), true);
            if ( StringUtil.StrCmp(Gx_mode, "DSP") == 0 )
            {
               bttBtn_enter_Visible = 0;
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_enter_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_enter_Visible), 5, 0)), true);
            }
            DisableAttributes077( ) ;
         }
      }

      protected void set_caption( )
      {
         if ( ( IsConfirmed == 1 ) && ( AnyError == 0 ) )
         {
            if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_confdelete", ""), 0, "", true);
            }
            else
            {
               GX_msglist.addItem(context.GetMessage( "GXM_mustconfirm", ""), 0, "", true);
            }
         }
      }

      protected void CONFIRM_070( )
      {
         BeforeValidate077( ) ;
         if ( AnyError == 0 )
         {
            if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
            {
               OnDeleteControls077( ) ;
            }
            else
            {
               CheckExtendedTable077( ) ;
               CloseExtendedTableCursors077( ) ;
            }
         }
         if ( AnyError == 0 )
         {
            IsConfirmed = 1;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "IsConfirmed", StringUtil.LTrim( StringUtil.Str( (decimal)(IsConfirmed), 4, 0)));
         }
      }

      protected void ResetCaption070( )
      {
      }

      protected void E11072( )
      {
         /* Start Routine */
         if ( ! new isauthorized(context).executeUdp(  AV13Pgmname) )
         {
            CallWebObject(formatLink("notauthorized.aspx") + "?" + UrlEncode(StringUtil.RTrim(AV13Pgmname)));
            context.wjLocDisableFrm = 1;
         }
         AV9TrnContext.FromXml(AV10WebSession.Get("TrnContext"), null, "TransactionContext", "SAPWEB08");
         AV11Insert_Planid = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV11Insert_Planid", StringUtil.LTrim( StringUtil.Str( (decimal)(AV11Insert_Planid), 9, 0)));
         if ( ( StringUtil.StrCmp(AV9TrnContext.gxTpr_Transactionname, AV13Pgmname) == 0 ) && ( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) )
         {
            AV14GXV1 = 1;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14GXV1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV14GXV1), 8, 0)));
            while ( AV14GXV1 <= AV9TrnContext.gxTpr_Attributes.Count )
            {
               AV12TrnContextAtt = ((SdtTransactionContext_Attribute)AV9TrnContext.gxTpr_Attributes.Item(AV14GXV1));
               if ( StringUtil.StrCmp(AV12TrnContextAtt.gxTpr_Attributename, "Planid") == 0 )
               {
                  AV11Insert_Planid = (int)(NumberUtil.Val( AV12TrnContextAtt.gxTpr_Attributevalue, "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV11Insert_Planid", StringUtil.LTrim( StringUtil.Str( (decimal)(AV11Insert_Planid), 9, 0)));
               }
               AV14GXV1 = (int)(AV14GXV1+1);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14GXV1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV14GXV1), 8, 0)));
            }
         }
      }

      protected void E12072( )
      {
         /* After Trn Routine */
         if ( ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 ) && ! AV9TrnContext.gxTpr_Callerondelete )
         {
            CallWebObject(formatLink("wwinforme_actividad.aspx") );
            context.wjLocDisableFrm = 1;
         }
         context.setWebReturnParms(new Object[] {});
         context.setWebReturnParmsMetadata(new Object[] {});
         context.wjLocDisableFrm = 1;
         context.nUserReturn = 1;
         returnInSub = true;
         if (true) return;
      }

      protected void ZM077( short GX_JID )
      {
         if ( ( GX_JID == 9 ) || ( GX_JID == 0 ) )
         {
            if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
            {
               Z41resultados = T00073_A41resultados[0];
               Z42seguimiento = T00073_A42seguimiento[0];
               Z43dificultades = T00073_A43dificultades[0];
               Z44mejorar = T00073_A44mejorar[0];
               Z45comentarios = T00073_A45comentarios[0];
               Z46fecha = T00073_A46fecha[0];
               Z47fechaActividad = T00073_A47fechaActividad[0];
               Z13Planid = T00073_A13Planid[0];
            }
            else
            {
               Z41resultados = A41resultados;
               Z42seguimiento = A42seguimiento;
               Z43dificultades = A43dificultades;
               Z44mejorar = A44mejorar;
               Z45comentarios = A45comentarios;
               Z46fecha = A46fecha;
               Z47fechaActividad = A47fechaActividad;
               Z13Planid = A13Planid;
            }
         }
         if ( GX_JID == -9 )
         {
            Z11Informe_Actividadid = A11Informe_Actividadid;
            Z41resultados = A41resultados;
            Z42seguimiento = A42seguimiento;
            Z43dificultades = A43dificultades;
            Z44mejorar = A44mejorar;
            Z45comentarios = A45comentarios;
            Z46fecha = A46fecha;
            Z47fechaActividad = A47fechaActividad;
            Z13Planid = A13Planid;
         }
      }

      protected void standaloneNotModal( )
      {
         imgprompt_13_Link = ((StringUtil.StrCmp(Gx_mode, "DSP")==0) ? "" : "javascript:"+"gx.popup.openPrompt('"+"gx00g0.aspx"+"',["+"{Ctrl:gx.dom.el('"+"PLANID"+"'), id:'"+"PLANID"+"'"+",IOType:'out'}"+"],"+"null"+","+"'', false"+","+"false"+");");
         bttBtn_delete_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_delete_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_delete_Enabled), 5, 0)), true);
         if ( ! (0==AV7Informe_Actividadid) )
         {
            A11Informe_Actividadid = AV7Informe_Actividadid;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A11Informe_Actividadid", StringUtil.LTrim( StringUtil.Str( (decimal)(A11Informe_Actividadid), 9, 0)));
         }
         if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) && ! (0==AV11Insert_Planid) )
         {
            edtPlanid_Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtPlanid_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtPlanid_Enabled), 5, 0)), true);
         }
         else
         {
            edtPlanid_Enabled = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtPlanid_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtPlanid_Enabled), 5, 0)), true);
         }
      }

      protected void standaloneModal( )
      {
         if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) && ! (0==AV11Insert_Planid) )
         {
            A13Planid = AV11Insert_Planid;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A13Planid", StringUtil.LTrim( StringUtil.Str( (decimal)(A13Planid), 9, 0)));
         }
         if ( StringUtil.StrCmp(Gx_mode, "DSP") == 0 )
         {
            bttBtn_enter_Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_enter_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_enter_Enabled), 5, 0)), true);
         }
         else
         {
            bttBtn_enter_Enabled = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_enter_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_enter_Enabled), 5, 0)), true);
         }
         if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) && ( Gx_BScreen == 0 ) )
         {
            AV13Pgmname = "Informe_Actividad";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13Pgmname", AV13Pgmname);
         }
      }

      protected void Load077( )
      {
         /* Using cursor T00075 */
         pr_datastore1.execute(3, new Object[] {A11Informe_Actividadid});
         if ( (pr_datastore1.getStatus(3) != 101) )
         {
            RcdFound7 = 1;
            A41resultados = T00075_A41resultados[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A41resultados", A41resultados);
            A42seguimiento = T00075_A42seguimiento[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A42seguimiento", A42seguimiento);
            A43dificultades = T00075_A43dificultades[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A43dificultades", A43dificultades);
            A44mejorar = T00075_A44mejorar[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A44mejorar", A44mejorar);
            A45comentarios = T00075_A45comentarios[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A45comentarios", A45comentarios);
            n45comentarios = T00075_n45comentarios[0];
            A46fecha = T00075_A46fecha[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A46fecha", context.localUtil.Format(A46fecha, "99/99/99"));
            A47fechaActividad = T00075_A47fechaActividad[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A47fechaActividad", context.localUtil.Format(A47fechaActividad, "99/99/99"));
            n47fechaActividad = T00075_n47fechaActividad[0];
            A13Planid = T00075_A13Planid[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A13Planid", StringUtil.LTrim( StringUtil.Str( (decimal)(A13Planid), 9, 0)));
            ZM077( -9) ;
         }
         pr_datastore1.close(3);
         OnLoadActions077( ) ;
      }

      protected void OnLoadActions077( )
      {
         AV13Pgmname = "Informe_Actividad";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13Pgmname", AV13Pgmname);
      }

      protected void CheckExtendedTable077( )
      {
         Gx_BScreen = 1;
         standaloneModal( ) ;
         AV13Pgmname = "Informe_Actividad";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13Pgmname", AV13Pgmname);
         /* Using cursor T00074 */
         pr_datastore1.execute(2, new Object[] {A13Planid});
         if ( (pr_datastore1.getStatus(2) == 101) )
         {
            GX_msglist.addItem("No matching 'Plan'.", "ForeignKeyNotFound", 1, "PLANID");
            AnyError = 1;
            GX_FocusControl = edtPlanid_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         pr_datastore1.close(2);
         if ( ! ( (DateTime.MinValue==A46fecha) || ( A46fecha >= context.localUtil.YMDToD( 1753, 1, 1) ) ) )
         {
            GX_msglist.addItem("Field Fecha del Informe is out of range", "OutOfRange", 1, "FECHA");
            AnyError = 1;
            GX_FocusControl = edtfecha_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         if ( ! ( (DateTime.MinValue==A47fechaActividad) || ( A47fechaActividad >= context.localUtil.YMDToD( 1753, 1, 1) ) ) )
         {
            GX_msglist.addItem("Field Fecha de la Actividad is out of range", "OutOfRange", 1, "FECHAACTIVIDAD");
            AnyError = 1;
            GX_FocusControl = edtfechaActividad_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
      }

      protected void CloseExtendedTableCursors077( )
      {
         pr_datastore1.close(2);
      }

      protected void enableDisable( )
      {
      }

      protected void gxLoad_10( int A13Planid )
      {
         /* Using cursor T00076 */
         pr_datastore1.execute(4, new Object[] {A13Planid});
         if ( (pr_datastore1.getStatus(4) == 101) )
         {
            GX_msglist.addItem("No matching 'Plan'.", "ForeignKeyNotFound", 1, "PLANID");
            AnyError = 1;
            GX_FocusControl = edtPlanid_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         GxWebStd.set_html_headers( context, 0, "", "");
         context.GX_webresponse.AddString("[[");
         context.GX_webresponse.AddString("");
         context.GX_webresponse.AddString("]");
         if ( (pr_datastore1.getStatus(4) == 101) )
         {
            context.GX_webresponse.AddString(",");
            context.GX_webresponse.AddString("101");
         }
         context.GX_webresponse.AddString("]");
         pr_datastore1.close(4);
      }

      protected void GetKey077( )
      {
         /* Using cursor T00077 */
         pr_datastore1.execute(5, new Object[] {A11Informe_Actividadid});
         if ( (pr_datastore1.getStatus(5) != 101) )
         {
            RcdFound7 = 1;
         }
         else
         {
            RcdFound7 = 0;
         }
         pr_datastore1.close(5);
      }

      protected void getByPrimaryKey( )
      {
         /* Using cursor T00073 */
         pr_datastore1.execute(1, new Object[] {A11Informe_Actividadid});
         if ( (pr_datastore1.getStatus(1) != 101) )
         {
            ZM077( 9) ;
            RcdFound7 = 1;
            A11Informe_Actividadid = T00073_A11Informe_Actividadid[0];
            A41resultados = T00073_A41resultados[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A41resultados", A41resultados);
            A42seguimiento = T00073_A42seguimiento[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A42seguimiento", A42seguimiento);
            A43dificultades = T00073_A43dificultades[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A43dificultades", A43dificultades);
            A44mejorar = T00073_A44mejorar[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A44mejorar", A44mejorar);
            A45comentarios = T00073_A45comentarios[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A45comentarios", A45comentarios);
            n45comentarios = T00073_n45comentarios[0];
            A46fecha = T00073_A46fecha[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A46fecha", context.localUtil.Format(A46fecha, "99/99/99"));
            A47fechaActividad = T00073_A47fechaActividad[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A47fechaActividad", context.localUtil.Format(A47fechaActividad, "99/99/99"));
            n47fechaActividad = T00073_n47fechaActividad[0];
            A13Planid = T00073_A13Planid[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A13Planid", StringUtil.LTrim( StringUtil.Str( (decimal)(A13Planid), 9, 0)));
            Z11Informe_Actividadid = A11Informe_Actividadid;
            sMode7 = Gx_mode;
            Gx_mode = "DSP";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!")), context));
            Load077( ) ;
            if ( AnyError == 1 )
            {
               RcdFound7 = 0;
               InitializeNonKey077( ) ;
            }
            Gx_mode = sMode7;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!")), context));
         }
         else
         {
            RcdFound7 = 0;
            InitializeNonKey077( ) ;
            sMode7 = Gx_mode;
            Gx_mode = "DSP";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!")), context));
            standaloneModal( ) ;
            Gx_mode = sMode7;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!")), context));
         }
         pr_datastore1.close(1);
      }

      protected void getEqualNoModal( )
      {
         GetKey077( ) ;
         if ( RcdFound7 == 0 )
         {
         }
         else
         {
         }
         getByPrimaryKey( ) ;
      }

      protected void move_next( )
      {
         RcdFound7 = 0;
         /* Using cursor T00078 */
         pr_datastore1.execute(6, new Object[] {A11Informe_Actividadid});
         if ( (pr_datastore1.getStatus(6) != 101) )
         {
            while ( (pr_datastore1.getStatus(6) != 101) && ( ( T00078_A11Informe_Actividadid[0] < A11Informe_Actividadid ) ) )
            {
               pr_datastore1.readNext(6);
            }
            if ( (pr_datastore1.getStatus(6) != 101) && ( ( T00078_A11Informe_Actividadid[0] > A11Informe_Actividadid ) ) )
            {
               A11Informe_Actividadid = T00078_A11Informe_Actividadid[0];
               RcdFound7 = 1;
            }
         }
         pr_datastore1.close(6);
      }

      protected void move_previous( )
      {
         RcdFound7 = 0;
         /* Using cursor T00079 */
         pr_datastore1.execute(7, new Object[] {A11Informe_Actividadid});
         if ( (pr_datastore1.getStatus(7) != 101) )
         {
            while ( (pr_datastore1.getStatus(7) != 101) && ( ( T00079_A11Informe_Actividadid[0] > A11Informe_Actividadid ) ) )
            {
               pr_datastore1.readNext(7);
            }
            if ( (pr_datastore1.getStatus(7) != 101) && ( ( T00079_A11Informe_Actividadid[0] < A11Informe_Actividadid ) ) )
            {
               A11Informe_Actividadid = T00079_A11Informe_Actividadid[0];
               RcdFound7 = 1;
            }
         }
         pr_datastore1.close(7);
      }

      protected void btn_enter( )
      {
         nKeyPressed = 1;
         GetKey077( ) ;
         if ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )
         {
            /* Insert record */
            GX_FocusControl = edtPlanid_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            Insert077( ) ;
            if ( AnyError == 1 )
            {
               GX_FocusControl = "";
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            }
         }
         else
         {
            if ( RcdFound7 == 1 )
            {
               if ( A11Informe_Actividadid != Z11Informe_Actividadid )
               {
                  A11Informe_Actividadid = Z11Informe_Actividadid;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A11Informe_Actividadid", StringUtil.LTrim( StringUtil.Str( (decimal)(A11Informe_Actividadid), 9, 0)));
                  GX_msglist.addItem(context.GetMessage( "GXM_getbeforeupd", ""), "CandidateKeyNotFound", 1, "");
                  AnyError = 1;
               }
               else if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
               {
                  delete( ) ;
                  AfterTrn( ) ;
                  GX_FocusControl = edtPlanid_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               }
               else
               {
                  /* Update record */
                  Update077( ) ;
                  GX_FocusControl = edtPlanid_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               }
            }
            else
            {
               if ( A11Informe_Actividadid != Z11Informe_Actividadid )
               {
                  /* Insert record */
                  GX_FocusControl = edtPlanid_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  Insert077( ) ;
                  if ( AnyError == 1 )
                  {
                     GX_FocusControl = "";
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  }
               }
               else
               {
                  if ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 )
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_recdeleted", ""), 1, "");
                     AnyError = 1;
                  }
                  else
                  {
                     /* Insert record */
                     GX_FocusControl = edtPlanid_Internalname;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                     Insert077( ) ;
                     if ( AnyError == 1 )
                     {
                        GX_FocusControl = "";
                        context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                     }
                  }
               }
            }
         }
         AfterTrn( ) ;
         if ( ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 ) || ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 ) )
         {
            if ( AnyError == 0 )
            {
               context.nUserReturn = 1;
            }
         }
      }

      protected void btn_delete( )
      {
         if ( A11Informe_Actividadid != Z11Informe_Actividadid )
         {
            A11Informe_Actividadid = Z11Informe_Actividadid;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A11Informe_Actividadid", StringUtil.LTrim( StringUtil.Str( (decimal)(A11Informe_Actividadid), 9, 0)));
            GX_msglist.addItem(context.GetMessage( "GXM_getbeforedlt", ""), 1, "");
            AnyError = 1;
         }
         else
         {
            delete( ) ;
            AfterTrn( ) ;
            GX_FocusControl = edtPlanid_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         if ( AnyError != 0 )
         {
         }
      }

      protected void CheckOptimisticConcurrency077( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
         {
            /* Using cursor T00072 */
            pr_datastore1.execute(0, new Object[] {A11Informe_Actividadid});
            if ( (pr_datastore1.getStatus(0) == 103) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_lock", new   object[]  {"INFORME_ACTIVIDAD"}), "RecordIsLocked", 1, "");
               AnyError = 1;
               return  ;
            }
            Gx_longc = false;
            if ( (pr_datastore1.getStatus(0) == 101) || ( StringUtil.StrCmp(Z41resultados, T00072_A41resultados[0]) != 0 ) || ( StringUtil.StrCmp(Z42seguimiento, T00072_A42seguimiento[0]) != 0 ) || ( StringUtil.StrCmp(Z43dificultades, T00072_A43dificultades[0]) != 0 ) || ( StringUtil.StrCmp(Z44mejorar, T00072_A44mejorar[0]) != 0 ) || ( StringUtil.StrCmp(Z45comentarios, T00072_A45comentarios[0]) != 0 ) )
            {
               Gx_longc = true;
            }
            if ( Gx_longc || ( Z46fecha != T00072_A46fecha[0] ) || ( Z47fechaActividad != T00072_A47fechaActividad[0] ) || ( Z13Planid != T00072_A13Planid[0] ) )
            {
               if ( StringUtil.StrCmp(Z41resultados, T00072_A41resultados[0]) != 0 )
               {
                  GXUtil.WriteLog("informe_actividad:[seudo value changed for attri]"+"resultados");
                  GXUtil.WriteLogRaw("Old: ",Z41resultados);
                  GXUtil.WriteLogRaw("Current: ",T00072_A41resultados[0]);
               }
               if ( StringUtil.StrCmp(Z42seguimiento, T00072_A42seguimiento[0]) != 0 )
               {
                  GXUtil.WriteLog("informe_actividad:[seudo value changed for attri]"+"seguimiento");
                  GXUtil.WriteLogRaw("Old: ",Z42seguimiento);
                  GXUtil.WriteLogRaw("Current: ",T00072_A42seguimiento[0]);
               }
               if ( StringUtil.StrCmp(Z43dificultades, T00072_A43dificultades[0]) != 0 )
               {
                  GXUtil.WriteLog("informe_actividad:[seudo value changed for attri]"+"dificultades");
                  GXUtil.WriteLogRaw("Old: ",Z43dificultades);
                  GXUtil.WriteLogRaw("Current: ",T00072_A43dificultades[0]);
               }
               if ( StringUtil.StrCmp(Z44mejorar, T00072_A44mejorar[0]) != 0 )
               {
                  GXUtil.WriteLog("informe_actividad:[seudo value changed for attri]"+"mejorar");
                  GXUtil.WriteLogRaw("Old: ",Z44mejorar);
                  GXUtil.WriteLogRaw("Current: ",T00072_A44mejorar[0]);
               }
               if ( StringUtil.StrCmp(Z45comentarios, T00072_A45comentarios[0]) != 0 )
               {
                  GXUtil.WriteLog("informe_actividad:[seudo value changed for attri]"+"comentarios");
                  GXUtil.WriteLogRaw("Old: ",Z45comentarios);
                  GXUtil.WriteLogRaw("Current: ",T00072_A45comentarios[0]);
               }
               if ( Z46fecha != T00072_A46fecha[0] )
               {
                  GXUtil.WriteLog("informe_actividad:[seudo value changed for attri]"+"fecha");
                  GXUtil.WriteLogRaw("Old: ",Z46fecha);
                  GXUtil.WriteLogRaw("Current: ",T00072_A46fecha[0]);
               }
               if ( Z47fechaActividad != T00072_A47fechaActividad[0] )
               {
                  GXUtil.WriteLog("informe_actividad:[seudo value changed for attri]"+"fechaActividad");
                  GXUtil.WriteLogRaw("Old: ",Z47fechaActividad);
                  GXUtil.WriteLogRaw("Current: ",T00072_A47fechaActividad[0]);
               }
               if ( Z13Planid != T00072_A13Planid[0] )
               {
                  GXUtil.WriteLog("informe_actividad:[seudo value changed for attri]"+"Planid");
                  GXUtil.WriteLogRaw("Old: ",Z13Planid);
                  GXUtil.WriteLogRaw("Current: ",T00072_A13Planid[0]);
               }
               GX_msglist.addItem(context.GetMessage( "GXM_waschg", new   object[]  {"INFORME_ACTIVIDAD"}), "RecordWasChanged", 1, "");
               AnyError = 1;
               return  ;
            }
         }
      }

      protected void Insert077( )
      {
         BeforeValidate077( ) ;
         if ( AnyError == 0 )
         {
            CheckExtendedTable077( ) ;
         }
         if ( AnyError == 0 )
         {
            ZM077( 0) ;
            CheckOptimisticConcurrency077( ) ;
            if ( AnyError == 0 )
            {
               AfterConfirm077( ) ;
               if ( AnyError == 0 )
               {
                  BeforeInsert077( ) ;
                  if ( AnyError == 0 )
                  {
                     /* Using cursor T000710 */
                     pr_datastore1.execute(8, new Object[] {A41resultados, A42seguimiento, A43dificultades, A44mejorar, n45comentarios, A45comentarios, A46fecha, n47fechaActividad, A47fechaActividad, A13Planid});
                     A11Informe_Actividadid = T000710_A11Informe_Actividadid[0];
                     pr_datastore1.close(8);
                     dsDataStore1.SmartCacheProvider.SetUpdated("INFORME_ACTIVIDAD") ;
                     if ( AnyError == 0 )
                     {
                        /* Start of After( Insert) rules */
                        /* End of After( Insert) rules */
                        if ( AnyError == 0 )
                        {
                           /* Save values for previous() function. */
                           GX_msglist.addItem(context.GetMessage( "GXM_sucadded", ""), "SuccessfullyAdded", 0, "", true);
                           ResetCaption070( ) ;
                        }
                     }
                  }
                  else
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                     AnyError = 1;
                  }
               }
            }
            else
            {
               Load077( ) ;
            }
            EndLevel077( ) ;
         }
         CloseExtendedTableCursors077( ) ;
      }

      protected void Update077( )
      {
         BeforeValidate077( ) ;
         if ( AnyError == 0 )
         {
            CheckExtendedTable077( ) ;
         }
         if ( AnyError == 0 )
         {
            CheckOptimisticConcurrency077( ) ;
            if ( AnyError == 0 )
            {
               AfterConfirm077( ) ;
               if ( AnyError == 0 )
               {
                  BeforeUpdate077( ) ;
                  if ( AnyError == 0 )
                  {
                     /* Using cursor T000711 */
                     pr_datastore1.execute(9, new Object[] {A41resultados, A42seguimiento, A43dificultades, A44mejorar, n45comentarios, A45comentarios, A46fecha, n47fechaActividad, A47fechaActividad, A13Planid, A11Informe_Actividadid});
                     pr_datastore1.close(9);
                     dsDataStore1.SmartCacheProvider.SetUpdated("INFORME_ACTIVIDAD") ;
                     if ( (pr_datastore1.getStatus(9) == 103) )
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_lock", new   object[]  {"INFORME_ACTIVIDAD"}), "RecordIsLocked", 1, "");
                        AnyError = 1;
                     }
                     DeferredUpdate077( ) ;
                     if ( AnyError == 0 )
                     {
                        /* Start of After( update) rules */
                        /* End of After( update) rules */
                        if ( AnyError == 0 )
                        {
                           if ( ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 ) || ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 ) )
                           {
                              if ( AnyError == 0 )
                              {
                                 context.nUserReturn = 1;
                              }
                           }
                        }
                     }
                     else
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                        AnyError = 1;
                     }
                  }
               }
            }
            EndLevel077( ) ;
         }
         CloseExtendedTableCursors077( ) ;
      }

      protected void DeferredUpdate077( )
      {
      }

      protected void delete( )
      {
         BeforeValidate077( ) ;
         if ( AnyError == 0 )
         {
            CheckOptimisticConcurrency077( ) ;
         }
         if ( AnyError == 0 )
         {
            OnDeleteControls077( ) ;
            AfterConfirm077( ) ;
            if ( AnyError == 0 )
            {
               BeforeDelete077( ) ;
               if ( AnyError == 0 )
               {
                  /* No cascading delete specified. */
                  /* Using cursor T000712 */
                  pr_datastore1.execute(10, new Object[] {A11Informe_Actividadid});
                  pr_datastore1.close(10);
                  dsDataStore1.SmartCacheProvider.SetUpdated("INFORME_ACTIVIDAD") ;
                  if ( AnyError == 0 )
                  {
                     /* Start of After( delete) rules */
                     /* End of After( delete) rules */
                     if ( AnyError == 0 )
                     {
                        if ( ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 ) || ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 ) )
                        {
                           if ( AnyError == 0 )
                           {
                              context.nUserReturn = 1;
                           }
                        }
                     }
                  }
                  else
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                     AnyError = 1;
                  }
               }
            }
         }
         sMode7 = Gx_mode;
         Gx_mode = "DLT";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!")), context));
         EndLevel077( ) ;
         Gx_mode = sMode7;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!")), context));
      }

      protected void OnDeleteControls077( )
      {
         standaloneModal( ) ;
         if ( AnyError == 0 )
         {
            /* Delete mode formulas */
            AV13Pgmname = "Informe_Actividad";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13Pgmname", AV13Pgmname);
         }
         if ( AnyError == 0 )
         {
            /* Using cursor T000713 */
            pr_datastore1.execute(11, new Object[] {A11Informe_Actividadid});
            if ( (pr_datastore1.getStatus(11) != 101) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_del", new   object[]  {"Beneficario_Informe_Actividad"}), "CannotDeleteReferencedRecord", 1, "");
               AnyError = 1;
            }
            pr_datastore1.close(11);
            /* Using cursor T000714 */
            pr_datastore1.execute(12, new Object[] {A11Informe_Actividadid});
            if ( (pr_datastore1.getStatus(12) != 101) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_del", new   object[]  {"Foto"}), "CannotDeleteReferencedRecord", 1, "");
               AnyError = 1;
            }
            pr_datastore1.close(12);
            /* Using cursor T000715 */
            pr_datastore1.execute(13, new Object[] {A11Informe_Actividadid});
            if ( (pr_datastore1.getStatus(13) != 101) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_del", new   object[]  {"Contacto_Informe_Actividad"}), "CannotDeleteReferencedRecord", 1, "");
               AnyError = 1;
            }
            pr_datastore1.close(13);
         }
      }

      protected void EndLevel077( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
         {
            pr_datastore1.close(0);
         }
         if ( AnyError == 0 )
         {
            BeforeComplete077( ) ;
         }
         if ( AnyError == 0 )
         {
            pr_datastore1.close(1);
            context.CommitDataStores("informe_actividad",pr_default);
            if ( AnyError == 0 )
            {
               ConfirmValues070( ) ;
            }
            /* After transaction rules */
            /* Execute 'After Trn' event if defined. */
            trnEnded = 1;
         }
         else
         {
            pr_datastore1.close(1);
            context.RollbackDataStores("informe_actividad",pr_default);
         }
         IsModified = 0;
         if ( AnyError != 0 )
         {
            context.wjLoc = "";
            context.nUserReturn = 0;
         }
      }

      public void ScanStart077( )
      {
         /* Scan By routine */
         /* Using cursor T000716 */
         pr_datastore1.execute(14);
         RcdFound7 = 0;
         if ( (pr_datastore1.getStatus(14) != 101) )
         {
            RcdFound7 = 1;
            A11Informe_Actividadid = T000716_A11Informe_Actividadid[0];
         }
         /* Load Subordinate Levels */
      }

      protected void ScanNext077( )
      {
         /* Scan next routine */
         pr_datastore1.readNext(14);
         RcdFound7 = 0;
         if ( (pr_datastore1.getStatus(14) != 101) )
         {
            RcdFound7 = 1;
            A11Informe_Actividadid = T000716_A11Informe_Actividadid[0];
         }
      }

      protected void ScanEnd077( )
      {
         pr_datastore1.close(14);
      }

      protected void AfterConfirm077( )
      {
         /* After Confirm Rules */
      }

      protected void BeforeInsert077( )
      {
         /* Before Insert Rules */
      }

      protected void BeforeUpdate077( )
      {
         /* Before Update Rules */
      }

      protected void BeforeDelete077( )
      {
         /* Before Delete Rules */
      }

      protected void BeforeComplete077( )
      {
         /* Before Complete Rules */
      }

      protected void BeforeValidate077( )
      {
         /* Before Validate Rules */
      }

      protected void DisableAttributes077( )
      {
         edtPlanid_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtPlanid_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtPlanid_Enabled), 5, 0)), true);
         edtresultados_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtresultados_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtresultados_Enabled), 5, 0)), true);
         edtseguimiento_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtseguimiento_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtseguimiento_Enabled), 5, 0)), true);
         edtdificultades_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtdificultades_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtdificultades_Enabled), 5, 0)), true);
         edtmejorar_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtmejorar_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtmejorar_Enabled), 5, 0)), true);
         edtcomentarios_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtcomentarios_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtcomentarios_Enabled), 5, 0)), true);
         edtfecha_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtfecha_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtfecha_Enabled), 5, 0)), true);
         edtfechaActividad_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtfechaActividad_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtfechaActividad_Enabled), 5, 0)), true);
      }

      protected void send_integrity_lvl_hashes077( )
      {
      }

      protected void assign_properties_default( )
      {
      }

      protected void ConfirmValues070( )
      {
      }

      public override void RenderHtmlHeaders( )
      {
         GxWebStd.gx_html_headers( context, 0, "", "", Form.Meta, Form.Metaequiv, true);
      }

      public override void RenderHtmlOpenForm( )
      {
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.WriteHtmlText( "<title>") ;
         context.SendWebValue( Form.Caption) ;
         context.WriteHtmlTextNl( "</title>") ;
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         if ( StringUtil.Len( sDynURL) > 0 )
         {
            context.WriteHtmlText( "<BASE href=\""+sDynURL+"\" />") ;
         }
         define_styles( ) ;
         MasterPageObj.master_styles();
         if ( ( ( context.GetBrowserType( ) == 1 ) || ( context.GetBrowserType( ) == 5 ) ) && ( StringUtil.StrCmp(context.GetBrowserVersion( ), "7.0") == 0 ) )
         {
            context.AddJavascriptSource("json2.js", "?"+context.GetBuildNumber( 127771), false);
         }
         context.AddJavascriptSource("jquery.js", "?"+context.GetBuildNumber( 127771), false);
         context.AddJavascriptSource("gxtimezone.js", "?"+context.GetBuildNumber( 127771), false);
         context.AddJavascriptSource("gxgral.js", "?"+context.GetBuildNumber( 127771), false);
         context.AddJavascriptSource("gxcfg.js", "?20191514565651", false);
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.AddJavascriptSource("calendar.js", "?"+context.GetBuildNumber( 127771), false);
         context.AddJavascriptSource("calendar-setup.js", "?"+context.GetBuildNumber( 127771), false);
         context.AddJavascriptSource("calendar-en.js", "?"+context.GetBuildNumber( 127771), false);
         context.WriteHtmlText( Form.Headerrawhtml) ;
         context.CloseHtmlHeader();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         FormProcess = " data-HasEnter=\"true\" data-Skiponenter=\"false\"";
         context.WriteHtmlText( "<body ") ;
         bodyStyle = "" + "background-color:" + context.BuildHTMLColor( Form.Backcolor) + ";color:" + context.BuildHTMLColor( Form.Textcolor) + ";";
         bodyStyle = bodyStyle + "-moz-opacity:0;opacity:0;";
         if ( ! ( String.IsNullOrEmpty(StringUtil.RTrim( Form.Background)) ) )
         {
            bodyStyle = bodyStyle + " background-image:url(" + context.convertURL( Form.Background) + ")";
         }
         context.WriteHtmlText( " "+"class=\"form-horizontal Form\""+" "+ "style='"+bodyStyle+"'") ;
         context.WriteHtmlText( FormProcess+">") ;
         context.skipLines(1);
         context.WriteHtmlTextNl( "<form id=\"MAINFORM\" autocomplete=\"off\" name=\"MAINFORM\" method=\"post\" tabindex=-1  class=\"form-horizontal Form\" data-gx-class=\"form-horizontal Form\" novalidate action=\""+formatLink("informe_actividad.aspx") + "?" + UrlEncode(StringUtil.RTrim(Gx_mode)) + "," + UrlEncode("" +AV7Informe_Actividadid)+"\">") ;
         GxWebStd.gx_hidden_field( context, "_EventName", "");
         GxWebStd.gx_hidden_field( context, "_EventGridId", "");
         GxWebStd.gx_hidden_field( context, "_EventRowId", "");
         context.WriteHtmlText( "<input type=\"submit\" title=\"submit\" style=\"display:none\" disabled>") ;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, "FORM", "Class", "form-horizontal Form", true);
         toggleJsOutput = isJsOutputEnabled( );
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
      }

      protected void send_integrity_footer_hashes( )
      {
         GXKey = Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
         forbiddenHiddens = "hsh" + "Informe_Actividad";
         forbiddenHiddens = forbiddenHiddens + context.localUtil.Format( (decimal)(AV11Insert_Planid), "ZZZZZZZZ9");
         forbiddenHiddens = forbiddenHiddens + StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"));
         forbiddenHiddens = forbiddenHiddens + context.localUtil.Format( (decimal)(A11Informe_Actividadid), "ZZZZZZZZ9");
         GxWebStd.gx_hidden_field( context, "hsh", GetEncryptedHash( forbiddenHiddens, GXKey));
         GXUtil.WriteLog("informe_actividad:[SendSecurityCheck value for]"+"Insert_Planid:"+context.localUtil.Format( (decimal)(AV11Insert_Planid), "ZZZZZZZZ9"));
         GXUtil.WriteLog("informe_actividad:[SendSecurityCheck value for]"+"Gx_mode:"+StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!")));
         GXUtil.WriteLog("informe_actividad:[SendSecurityCheck value for]"+"Informe_Actividadid:"+context.localUtil.Format( (decimal)(A11Informe_Actividadid), "ZZZZZZZZ9"));
      }

      protected void SendCloseFormHiddens( )
      {
         /* Send hidden variables. */
         /* Send saved values. */
         send_integrity_footer_hashes( ) ;
         GxWebStd.gx_hidden_field( context, "Z11Informe_Actividadid", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z11Informe_Actividadid), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "Z41resultados", Z41resultados);
         GxWebStd.gx_hidden_field( context, "Z42seguimiento", Z42seguimiento);
         GxWebStd.gx_hidden_field( context, "Z43dificultades", Z43dificultades);
         GxWebStd.gx_hidden_field( context, "Z44mejorar", Z44mejorar);
         GxWebStd.gx_hidden_field( context, "Z45comentarios", Z45comentarios);
         GxWebStd.gx_hidden_field( context, "Z46fecha", context.localUtil.DToC( Z46fecha, 0, "/"));
         GxWebStd.gx_hidden_field( context, "Z47fechaActividad", context.localUtil.DToC( Z47fechaActividad, 0, "/"));
         GxWebStd.gx_hidden_field( context, "Z13Planid", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z13Planid), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "IsConfirmed", StringUtil.LTrim( StringUtil.NToC( (decimal)(IsConfirmed), 4, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "IsModified", StringUtil.LTrim( StringUtil.NToC( (decimal)(IsModified), 4, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "Mode", StringUtil.RTrim( Gx_mode));
         GxWebStd.gx_hidden_field( context, "gxhash_Mode", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!")), context));
         GxWebStd.gx_hidden_field( context, "N13Planid", StringUtil.LTrim( StringUtil.NToC( (decimal)(A13Planid), 9, 0, ".", "")));
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vTRNCONTEXT", AV9TrnContext);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vTRNCONTEXT", AV9TrnContext);
         }
         GxWebStd.gx_hidden_field( context, "vINFORME_ACTIVIDADID", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV7Informe_Actividadid), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "gxhash_vINFORME_ACTIVIDADID", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV7Informe_Actividadid), "ZZZZZZZZ9"), context));
         GxWebStd.gx_hidden_field( context, "INFORME_ACTIVIDADID", StringUtil.LTrim( StringUtil.NToC( (decimal)(A11Informe_Actividadid), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "vINSERT_PLANID", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV11Insert_Planid), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "vPGMNAME", StringUtil.RTrim( AV13Pgmname));
         GxWebStd.gx_hidden_field( context, "vMODE", StringUtil.RTrim( Gx_mode));
         GxWebStd.gx_hidden_field( context, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!")), context));
      }

      public override void RenderHtmlCloseForm( )
      {
         SendCloseFormHiddens( ) ;
         GxWebStd.gx_hidden_field( context, "GX_FocusControl", GX_FocusControl);
         SendAjaxEncryptionKey();
         SendSecurityToken(sPrefix);
         SendComponentObjects();
         SendServerCommands();
         SendState();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         context.WriteHtmlTextNl( "</form>") ;
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         include_jscripts( ) ;
      }

      public override short ExecuteStartEvent( )
      {
         standaloneStartup( ) ;
         gxajaxcallmode = (short)((isAjaxCallMode( ) ? 1 : 0));
         return gxajaxcallmode ;
      }

      public override void RenderHtmlContent( )
      {
         context.WriteHtmlText( "<div") ;
         GxWebStd.ClassAttribute( context, "gx-ct-body"+" "+(String.IsNullOrEmpty(StringUtil.RTrim( Form.Class)) ? "form-horizontal Form" : Form.Class)+"-fx");
         context.WriteHtmlText( ">") ;
         Draw( ) ;
         context.WriteHtmlText( "</div>") ;
      }

      public override void DispatchEvents( )
      {
         Process( ) ;
      }

      public override bool HasEnterEvent( )
      {
         return true ;
      }

      public override GXWebForm GetForm( )
      {
         return Form ;
      }

      public override String GetSelfLink( )
      {
         return formatLink("informe_actividad.aspx") + "?" + UrlEncode(StringUtil.RTrim(Gx_mode)) + "," + UrlEncode("" +AV7Informe_Actividadid) ;
      }

      public override String GetPgmname( )
      {
         return "Informe_Actividad" ;
      }

      public override String GetPgmdesc( )
      {
         return "Informe_Actividad" ;
      }

      protected void InitializeNonKey077( )
      {
         A13Planid = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A13Planid", StringUtil.LTrim( StringUtil.Str( (decimal)(A13Planid), 9, 0)));
         A41resultados = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A41resultados", A41resultados);
         A42seguimiento = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A42seguimiento", A42seguimiento);
         A43dificultades = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A43dificultades", A43dificultades);
         A44mejorar = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A44mejorar", A44mejorar);
         A45comentarios = "";
         n45comentarios = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A45comentarios", A45comentarios);
         n45comentarios = (String.IsNullOrEmpty(StringUtil.RTrim( A45comentarios)) ? true : false);
         A46fecha = DateTime.MinValue;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A46fecha", context.localUtil.Format(A46fecha, "99/99/99"));
         A47fechaActividad = DateTime.MinValue;
         n47fechaActividad = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A47fechaActividad", context.localUtil.Format(A47fechaActividad, "99/99/99"));
         n47fechaActividad = ((DateTime.MinValue==A47fechaActividad) ? true : false);
         Z41resultados = "";
         Z42seguimiento = "";
         Z43dificultades = "";
         Z44mejorar = "";
         Z45comentarios = "";
         Z46fecha = DateTime.MinValue;
         Z47fechaActividad = DateTime.MinValue;
         Z13Planid = 0;
      }

      protected void InitAll077( )
      {
         A11Informe_Actividadid = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A11Informe_Actividadid", StringUtil.LTrim( StringUtil.Str( (decimal)(A11Informe_Actividadid), 9, 0)));
         InitializeNonKey077( ) ;
      }

      protected void StandaloneModalInsert( )
      {
      }

      protected void define_styles( )
      {
         AddStyleSheetFile("calendar-system.css", "");
         AddThemeStyleSheetFile("", context.GetTheme( )+".css", "?"+GetCacheInvalidationToken( ));
         bool outputEnabled = isOutputEnabled( ) ;
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         idxLst = 1;
         while ( idxLst <= Form.Jscriptsrc.Count )
         {
            context.AddJavascriptSource(StringUtil.RTrim( ((String)Form.Jscriptsrc.Item(idxLst))), "?20191514565668", true);
            idxLst = (int)(idxLst+1);
         }
         if ( ! outputEnabled )
         {
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
         }
         /* End function define_styles */
      }

      protected void include_jscripts( )
      {
         context.AddJavascriptSource("messages.eng.js", "?"+GetCacheInvalidationToken( ), false);
         context.AddJavascriptSource("informe_actividad.js", "?20191514565669", false);
         /* End function include_jscripts */
      }

      protected void init_default_properties( )
      {
         lblTitle_Internalname = "TITLE";
         divTitlecontainer_Internalname = "TITLECONTAINER";
         bttBtn_first_Internalname = "BTN_FIRST";
         bttBtn_previous_Internalname = "BTN_PREVIOUS";
         bttBtn_next_Internalname = "BTN_NEXT";
         bttBtn_last_Internalname = "BTN_LAST";
         bttBtn_select_Internalname = "BTN_SELECT";
         divToolbarcell_Internalname = "TOOLBARCELL";
         edtPlanid_Internalname = "PLANID";
         edtresultados_Internalname = "RESULTADOS";
         edtseguimiento_Internalname = "SEGUIMIENTO";
         edtdificultades_Internalname = "DIFICULTADES";
         edtmejorar_Internalname = "MEJORAR";
         edtcomentarios_Internalname = "COMENTARIOS";
         edtfecha_Internalname = "FECHA";
         edtfechaActividad_Internalname = "FECHAACTIVIDAD";
         divFormcontainer_Internalname = "FORMCONTAINER";
         bttBtn_enter_Internalname = "BTN_ENTER";
         bttBtn_cancel_Internalname = "BTN_CANCEL";
         bttBtn_delete_Internalname = "BTN_DELETE";
         divMaintable_Internalname = "MAINTABLE";
         Form.Internalname = "FORM";
         imgprompt_13_Internalname = "PROMPT_13";
      }

      public override void initialize_properties( )
      {
         context.SetDefaultTheme("Carmine");
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         init_default_properties( ) ;
         Form.Headerrawhtml = "";
         Form.Background = "";
         Form.Textcolor = 0;
         Form.Backcolor = (int)(0xFFFFFF);
         Form.Caption = "Informe_Actividad";
         bttBtn_delete_Enabled = 0;
         bttBtn_delete_Visible = 1;
         bttBtn_cancel_Visible = 1;
         bttBtn_enter_Enabled = 1;
         bttBtn_enter_Visible = 1;
         edtfechaActividad_Jsonclick = "";
         edtfechaActividad_Enabled = 1;
         edtfecha_Jsonclick = "";
         edtfecha_Enabled = 1;
         edtcomentarios_Enabled = 1;
         edtmejorar_Enabled = 1;
         edtdificultades_Enabled = 1;
         edtseguimiento_Enabled = 1;
         edtresultados_Enabled = 1;
         imgprompt_13_Visible = 1;
         imgprompt_13_Link = "";
         edtPlanid_Jsonclick = "";
         edtPlanid_Enabled = 1;
         bttBtn_select_Visible = 1;
         bttBtn_last_Visible = 1;
         bttBtn_next_Visible = 1;
         bttBtn_previous_Visible = 1;
         bttBtn_first_Visible = 1;
         context.GX_msglist.DisplayMode = 1;
         if ( context.isSpaRequest( ) )
         {
            enableJsOutput();
         }
      }

      protected void dynload_actions( )
      {
         /* End function dynload_actions */
      }

      protected void init_web_controls( )
      {
         /* End function init_web_controls */
      }

      public void Valid_Planid( int GX_Parm1 )
      {
         A13Planid = GX_Parm1;
         /* Using cursor T000717 */
         pr_datastore1.execute(15, new Object[] {A13Planid});
         if ( (pr_datastore1.getStatus(15) == 101) )
         {
            GX_msglist.addItem("No matching 'Plan'.", "ForeignKeyNotFound", 1, "PLANID");
            AnyError = 1;
            GX_FocusControl = edtPlanid_Internalname;
         }
         pr_datastore1.close(15);
         dynload_actions( ) ;
         isValidOutput.Add(context.GX_msglist.ToJavascriptSource());
         isValidOutput.Add(context.httpAjaxContext.ajax_rsp_get_hiddens( ));
         context.GX_webresponse.AddString(isValidOutput.ToJSonString());
         wbTemp = context.ResponseContentType( "application/json");
      }

      public override bool SupportAjaxEvent( )
      {
         return true ;
      }

      public override void InitializeDynEvents( )
      {
         setEventMetadata("ENTER","{handler:'UserMainFullajax',iparms:[{postForm:true},{av:'Gx_mode',fld:'vMODE',pic:'@!',hsh:true},{av:'AV7Informe_Actividadid',fld:'vINFORME_ACTIVIDADID',pic:'ZZZZZZZZ9',hsh:true}]");
         setEventMetadata("ENTER",",oparms:[]}");
         setEventMetadata("REFRESH","{handler:'Refresh',iparms:[{av:'Gx_mode',fld:'vMODE',pic:'@!',hsh:true},{av:'AV7Informe_Actividadid',fld:'vINFORME_ACTIVIDADID',pic:'ZZZZZZZZ9',hsh:true},{av:'AV11Insert_Planid',fld:'vINSERT_PLANID',pic:'ZZZZZZZZ9'},{av:'A11Informe_Actividadid',fld:'INFORME_ACTIVIDADID',pic:'ZZZZZZZZ9'}]");
         setEventMetadata("REFRESH",",oparms:[]}");
         setEventMetadata("AFTER TRN","{handler:'E12072',iparms:[{av:'Gx_mode',fld:'vMODE',pic:'@!',hsh:true},{av:'AV9TrnContext',fld:'vTRNCONTEXT',pic:''}]");
         setEventMetadata("AFTER TRN",",oparms:[]}");
         return  ;
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
         pr_datastore1.close(1);
         pr_datastore1.close(15);
      }

      public override void initialize( )
      {
         sPrefix = "";
         wcpOGx_mode = "";
         Z41resultados = "";
         Z42seguimiento = "";
         Z43dificultades = "";
         Z44mejorar = "";
         Z45comentarios = "";
         Z46fecha = DateTime.MinValue;
         Z47fechaActividad = DateTime.MinValue;
         scmdbuf = "";
         gxfirstwebparm = "";
         gxfirstwebparm_bkp = "";
         GXKey = "";
         PreviousTooltip = "";
         PreviousCaption = "";
         Form = new GXWebForm();
         GX_FocusControl = "";
         lblTitle_Jsonclick = "";
         ClassString = "";
         StyleString = "";
         TempTags = "";
         bttBtn_first_Jsonclick = "";
         bttBtn_previous_Jsonclick = "";
         bttBtn_next_Jsonclick = "";
         bttBtn_last_Jsonclick = "";
         bttBtn_select_Jsonclick = "";
         sImgUrl = "";
         A41resultados = "";
         A42seguimiento = "";
         A43dificultades = "";
         A44mejorar = "";
         A45comentarios = "";
         A46fecha = DateTime.MinValue;
         A47fechaActividad = DateTime.MinValue;
         bttBtn_enter_Jsonclick = "";
         bttBtn_cancel_Jsonclick = "";
         bttBtn_delete_Jsonclick = "";
         AV13Pgmname = "";
         forbiddenHiddens = "";
         hsh = "";
         sMode7 = "";
         sEvt = "";
         EvtGridId = "";
         EvtRowId = "";
         sEvtType = "";
         AV9TrnContext = new SdtTransactionContext(context);
         AV10WebSession = context.GetSession();
         AV12TrnContextAtt = new SdtTransactionContext_Attribute(context);
         T00075_A11Informe_Actividadid = new int[1] ;
         T00075_A41resultados = new String[] {""} ;
         T00075_A42seguimiento = new String[] {""} ;
         T00075_A43dificultades = new String[] {""} ;
         T00075_A44mejorar = new String[] {""} ;
         T00075_A45comentarios = new String[] {""} ;
         T00075_n45comentarios = new bool[] {false} ;
         T00075_A46fecha = new DateTime[] {DateTime.MinValue} ;
         T00075_A47fechaActividad = new DateTime[] {DateTime.MinValue} ;
         T00075_n47fechaActividad = new bool[] {false} ;
         T00075_A13Planid = new int[1] ;
         T00074_A13Planid = new int[1] ;
         T00076_A13Planid = new int[1] ;
         T00077_A11Informe_Actividadid = new int[1] ;
         T00073_A11Informe_Actividadid = new int[1] ;
         T00073_A41resultados = new String[] {""} ;
         T00073_A42seguimiento = new String[] {""} ;
         T00073_A43dificultades = new String[] {""} ;
         T00073_A44mejorar = new String[] {""} ;
         T00073_A45comentarios = new String[] {""} ;
         T00073_n45comentarios = new bool[] {false} ;
         T00073_A46fecha = new DateTime[] {DateTime.MinValue} ;
         T00073_A47fechaActividad = new DateTime[] {DateTime.MinValue} ;
         T00073_n47fechaActividad = new bool[] {false} ;
         T00073_A13Planid = new int[1] ;
         T00078_A11Informe_Actividadid = new int[1] ;
         T00079_A11Informe_Actividadid = new int[1] ;
         T00072_A11Informe_Actividadid = new int[1] ;
         T00072_A41resultados = new String[] {""} ;
         T00072_A42seguimiento = new String[] {""} ;
         T00072_A43dificultades = new String[] {""} ;
         T00072_A44mejorar = new String[] {""} ;
         T00072_A45comentarios = new String[] {""} ;
         T00072_n45comentarios = new bool[] {false} ;
         T00072_A46fecha = new DateTime[] {DateTime.MinValue} ;
         T00072_A47fechaActividad = new DateTime[] {DateTime.MinValue} ;
         T00072_n47fechaActividad = new bool[] {false} ;
         T00072_A13Planid = new int[1] ;
         T000710_A11Informe_Actividadid = new int[1] ;
         T000713_A26Beneficario_Informe_Actividadi = new int[1] ;
         T000714_A1Fotoid = new int[1] ;
         T000715_A10Contacto_Informe_Actividadid = new int[1] ;
         T000716_A11Informe_Actividadid = new int[1] ;
         sDynURL = "";
         FormProcess = "";
         bodyStyle = "";
         T000717_A13Planid = new int[1] ;
         isValidOutput = new GxUnknownObjectCollection();
         pr_datastore1 = new DataStoreProvider(context, new GeneXus.Programs.informe_actividad__datastore1(),
            new Object[][] {
                new Object[] {
               T00072_A11Informe_Actividadid, T00072_A41resultados, T00072_A42seguimiento, T00072_A43dificultades, T00072_A44mejorar, T00072_A45comentarios, T00072_n45comentarios, T00072_A46fecha, T00072_A47fechaActividad, T00072_n47fechaActividad,
               T00072_A13Planid
               }
               , new Object[] {
               T00073_A11Informe_Actividadid, T00073_A41resultados, T00073_A42seguimiento, T00073_A43dificultades, T00073_A44mejorar, T00073_A45comentarios, T00073_n45comentarios, T00073_A46fecha, T00073_A47fechaActividad, T00073_n47fechaActividad,
               T00073_A13Planid
               }
               , new Object[] {
               T00074_A13Planid
               }
               , new Object[] {
               T00075_A11Informe_Actividadid, T00075_A41resultados, T00075_A42seguimiento, T00075_A43dificultades, T00075_A44mejorar, T00075_A45comentarios, T00075_n45comentarios, T00075_A46fecha, T00075_A47fechaActividad, T00075_n47fechaActividad,
               T00075_A13Planid
               }
               , new Object[] {
               T00076_A13Planid
               }
               , new Object[] {
               T00077_A11Informe_Actividadid
               }
               , new Object[] {
               T00078_A11Informe_Actividadid
               }
               , new Object[] {
               T00079_A11Informe_Actividadid
               }
               , new Object[] {
               T000710_A11Informe_Actividadid
               }
               , new Object[] {
               }
               , new Object[] {
               }
               , new Object[] {
               T000713_A26Beneficario_Informe_Actividadi
               }
               , new Object[] {
               T000714_A1Fotoid
               }
               , new Object[] {
               T000715_A10Contacto_Informe_Actividadid
               }
               , new Object[] {
               T000716_A11Informe_Actividadid
               }
               , new Object[] {
               T000717_A13Planid
               }
            }
         );
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.informe_actividad__default(),
            new Object[][] {
            }
         );
         AV13Pgmname = "Informe_Actividad";
      }

      private short GxWebError ;
      private short gxcookieaux ;
      private short IsConfirmed ;
      private short IsModified ;
      private short AnyError ;
      private short nKeyPressed ;
      private short initialized ;
      private short RcdFound7 ;
      private short GX_JID ;
      private short Gx_BScreen ;
      private short gxajaxcallmode ;
      private short wbTemp ;
      private int wcpOAV7Informe_Actividadid ;
      private int Z11Informe_Actividadid ;
      private int Z13Planid ;
      private int N13Planid ;
      private int A13Planid ;
      private int AV7Informe_Actividadid ;
      private int trnEnded ;
      private int bttBtn_first_Visible ;
      private int bttBtn_previous_Visible ;
      private int bttBtn_next_Visible ;
      private int bttBtn_last_Visible ;
      private int bttBtn_select_Visible ;
      private int edtPlanid_Enabled ;
      private int imgprompt_13_Visible ;
      private int edtresultados_Enabled ;
      private int edtseguimiento_Enabled ;
      private int edtdificultades_Enabled ;
      private int edtmejorar_Enabled ;
      private int edtcomentarios_Enabled ;
      private int edtfecha_Enabled ;
      private int edtfechaActividad_Enabled ;
      private int bttBtn_enter_Visible ;
      private int bttBtn_enter_Enabled ;
      private int bttBtn_cancel_Visible ;
      private int bttBtn_delete_Visible ;
      private int bttBtn_delete_Enabled ;
      private int A11Informe_Actividadid ;
      private int AV11Insert_Planid ;
      private int AV14GXV1 ;
      private int idxLst ;
      private String sPrefix ;
      private String wcpOGx_mode ;
      private String scmdbuf ;
      private String gxfirstwebparm ;
      private String gxfirstwebparm_bkp ;
      private String Gx_mode ;
      private String GXKey ;
      private String PreviousTooltip ;
      private String PreviousCaption ;
      private String GX_FocusControl ;
      private String edtPlanid_Internalname ;
      private String divMaintable_Internalname ;
      private String divTitlecontainer_Internalname ;
      private String lblTitle_Internalname ;
      private String lblTitle_Jsonclick ;
      private String ClassString ;
      private String StyleString ;
      private String divFormcontainer_Internalname ;
      private String divToolbarcell_Internalname ;
      private String TempTags ;
      private String bttBtn_first_Internalname ;
      private String bttBtn_first_Jsonclick ;
      private String bttBtn_previous_Internalname ;
      private String bttBtn_previous_Jsonclick ;
      private String bttBtn_next_Internalname ;
      private String bttBtn_next_Jsonclick ;
      private String bttBtn_last_Internalname ;
      private String bttBtn_last_Jsonclick ;
      private String bttBtn_select_Internalname ;
      private String bttBtn_select_Jsonclick ;
      private String edtPlanid_Jsonclick ;
      private String sImgUrl ;
      private String imgprompt_13_Internalname ;
      private String imgprompt_13_Link ;
      private String edtresultados_Internalname ;
      private String edtseguimiento_Internalname ;
      private String edtdificultades_Internalname ;
      private String edtmejorar_Internalname ;
      private String edtcomentarios_Internalname ;
      private String edtfecha_Internalname ;
      private String edtfecha_Jsonclick ;
      private String edtfechaActividad_Internalname ;
      private String edtfechaActividad_Jsonclick ;
      private String bttBtn_enter_Internalname ;
      private String bttBtn_enter_Jsonclick ;
      private String bttBtn_cancel_Internalname ;
      private String bttBtn_cancel_Jsonclick ;
      private String bttBtn_delete_Internalname ;
      private String bttBtn_delete_Jsonclick ;
      private String AV13Pgmname ;
      private String forbiddenHiddens ;
      private String hsh ;
      private String sMode7 ;
      private String sEvt ;
      private String EvtGridId ;
      private String EvtRowId ;
      private String sEvtType ;
      private String sDynURL ;
      private String FormProcess ;
      private String bodyStyle ;
      private DateTime Z46fecha ;
      private DateTime Z47fechaActividad ;
      private DateTime A46fecha ;
      private DateTime A47fechaActividad ;
      private bool entryPointCalled ;
      private bool toggleJsOutput ;
      private bool wbErr ;
      private bool n45comentarios ;
      private bool n47fechaActividad ;
      private bool returnInSub ;
      private bool Gx_longc ;
      private String Z41resultados ;
      private String Z42seguimiento ;
      private String Z43dificultades ;
      private String Z44mejorar ;
      private String Z45comentarios ;
      private String A41resultados ;
      private String A42seguimiento ;
      private String A43dificultades ;
      private String A44mejorar ;
      private String A45comentarios ;
      private IGxSession AV10WebSession ;
      private GxUnknownObjectCollection isValidOutput ;
      private IGxDataStore dsDataStore1 ;
      private IGxDataStore dsDefault ;
      private IDataStoreProvider pr_datastore1 ;
      private int[] T00075_A11Informe_Actividadid ;
      private String[] T00075_A41resultados ;
      private String[] T00075_A42seguimiento ;
      private String[] T00075_A43dificultades ;
      private String[] T00075_A44mejorar ;
      private String[] T00075_A45comentarios ;
      private bool[] T00075_n45comentarios ;
      private DateTime[] T00075_A46fecha ;
      private DateTime[] T00075_A47fechaActividad ;
      private bool[] T00075_n47fechaActividad ;
      private int[] T00075_A13Planid ;
      private int[] T00074_A13Planid ;
      private int[] T00076_A13Planid ;
      private int[] T00077_A11Informe_Actividadid ;
      private int[] T00073_A11Informe_Actividadid ;
      private String[] T00073_A41resultados ;
      private String[] T00073_A42seguimiento ;
      private String[] T00073_A43dificultades ;
      private String[] T00073_A44mejorar ;
      private String[] T00073_A45comentarios ;
      private bool[] T00073_n45comentarios ;
      private DateTime[] T00073_A46fecha ;
      private DateTime[] T00073_A47fechaActividad ;
      private bool[] T00073_n47fechaActividad ;
      private int[] T00073_A13Planid ;
      private int[] T00078_A11Informe_Actividadid ;
      private int[] T00079_A11Informe_Actividadid ;
      private int[] T00072_A11Informe_Actividadid ;
      private String[] T00072_A41resultados ;
      private String[] T00072_A42seguimiento ;
      private String[] T00072_A43dificultades ;
      private String[] T00072_A44mejorar ;
      private String[] T00072_A45comentarios ;
      private bool[] T00072_n45comentarios ;
      private DateTime[] T00072_A46fecha ;
      private DateTime[] T00072_A47fechaActividad ;
      private bool[] T00072_n47fechaActividad ;
      private int[] T00072_A13Planid ;
      private int[] T000710_A11Informe_Actividadid ;
      private int[] T000713_A26Beneficario_Informe_Actividadi ;
      private int[] T000714_A1Fotoid ;
      private int[] T000715_A10Contacto_Informe_Actividadid ;
      private IDataStoreProvider pr_default ;
      private int[] T000716_A11Informe_Actividadid ;
      private int[] T000717_A13Planid ;
      private GXWebForm Form ;
      private SdtTransactionContext AV9TrnContext ;
      private SdtTransactionContext_Attribute AV12TrnContextAtt ;
   }

   public class informe_actividad__datastore1 : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
         ,new ForEachCursor(def[2])
         ,new ForEachCursor(def[3])
         ,new ForEachCursor(def[4])
         ,new ForEachCursor(def[5])
         ,new ForEachCursor(def[6])
         ,new ForEachCursor(def[7])
         ,new ForEachCursor(def[8])
         ,new UpdateCursor(def[9])
         ,new UpdateCursor(def[10])
         ,new ForEachCursor(def[11])
         ,new ForEachCursor(def[12])
         ,new ForEachCursor(def[13])
         ,new ForEachCursor(def[14])
         ,new ForEachCursor(def[15])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmT00075 ;
          prmT00075 = new Object[] {
          new Object[] {"@Informe_Actividadid",SqlDbType.Int,9,0}
          } ;
          Object[] prmT00074 ;
          prmT00074 = new Object[] {
          new Object[] {"@Planid",SqlDbType.Int,9,0}
          } ;
          Object[] prmT00076 ;
          prmT00076 = new Object[] {
          new Object[] {"@Planid",SqlDbType.Int,9,0}
          } ;
          Object[] prmT00077 ;
          prmT00077 = new Object[] {
          new Object[] {"@Informe_Actividadid",SqlDbType.Int,9,0}
          } ;
          Object[] prmT00073 ;
          prmT00073 = new Object[] {
          new Object[] {"@Informe_Actividadid",SqlDbType.Int,9,0}
          } ;
          Object[] prmT00078 ;
          prmT00078 = new Object[] {
          new Object[] {"@Informe_Actividadid",SqlDbType.Int,9,0}
          } ;
          Object[] prmT00079 ;
          prmT00079 = new Object[] {
          new Object[] {"@Informe_Actividadid",SqlDbType.Int,9,0}
          } ;
          Object[] prmT00072 ;
          prmT00072 = new Object[] {
          new Object[] {"@Informe_Actividadid",SqlDbType.Int,9,0}
          } ;
          Object[] prmT000710 ;
          prmT000710 = new Object[] {
          new Object[] {"@resultados",SqlDbType.VarChar,500,0} ,
          new Object[] {"@seguimiento",SqlDbType.VarChar,500,0} ,
          new Object[] {"@dificultades",SqlDbType.VarChar,500,0} ,
          new Object[] {"@mejorar",SqlDbType.VarChar,500,0} ,
          new Object[] {"@comentarios",SqlDbType.VarChar,500,0} ,
          new Object[] {"@fecha",SqlDbType.DateTime,8,0} ,
          new Object[] {"@fechaActividad",SqlDbType.DateTime,8,0} ,
          new Object[] {"@Planid",SqlDbType.Int,9,0}
          } ;
          Object[] prmT000711 ;
          prmT000711 = new Object[] {
          new Object[] {"@resultados",SqlDbType.VarChar,500,0} ,
          new Object[] {"@seguimiento",SqlDbType.VarChar,500,0} ,
          new Object[] {"@dificultades",SqlDbType.VarChar,500,0} ,
          new Object[] {"@mejorar",SqlDbType.VarChar,500,0} ,
          new Object[] {"@comentarios",SqlDbType.VarChar,500,0} ,
          new Object[] {"@fecha",SqlDbType.DateTime,8,0} ,
          new Object[] {"@fechaActividad",SqlDbType.DateTime,8,0} ,
          new Object[] {"@Planid",SqlDbType.Int,9,0} ,
          new Object[] {"@Informe_Actividadid",SqlDbType.Int,9,0}
          } ;
          Object[] prmT000712 ;
          prmT000712 = new Object[] {
          new Object[] {"@Informe_Actividadid",SqlDbType.Int,9,0}
          } ;
          Object[] prmT000713 ;
          prmT000713 = new Object[] {
          new Object[] {"@Informe_Actividadid",SqlDbType.Int,9,0}
          } ;
          Object[] prmT000714 ;
          prmT000714 = new Object[] {
          new Object[] {"@Informe_Actividadid",SqlDbType.Int,9,0}
          } ;
          Object[] prmT000715 ;
          prmT000715 = new Object[] {
          new Object[] {"@Informe_Actividadid",SqlDbType.Int,9,0}
          } ;
          Object[] prmT000716 ;
          prmT000716 = new Object[] {
          } ;
          Object[] prmT000717 ;
          prmT000717 = new Object[] {
          new Object[] {"@Planid",SqlDbType.Int,9,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("T00072", "SELECT [id] AS Informe_Actividadid, [resultados], [seguimiento], [dificultades], [mejorar], [comentarios], [fecha], [fechaActividad], [idPlan] AS Planid FROM dbo.[Informe_Actividad] WITH (UPDLOCK) WHERE [id] = @Informe_Actividadid ",true, GxErrorMask.GX_NOMASK, false, this,prmT00072,1,0,true,false )
             ,new CursorDef("T00073", "SELECT [id] AS Informe_Actividadid, [resultados], [seguimiento], [dificultades], [mejorar], [comentarios], [fecha], [fechaActividad], [idPlan] AS Planid FROM dbo.[Informe_Actividad] WITH (NOLOCK) WHERE [id] = @Informe_Actividadid ",true, GxErrorMask.GX_NOMASK, false, this,prmT00073,1,0,true,false )
             ,new CursorDef("T00074", "SELECT [id] AS Planid FROM dbo.[Plan] WITH (NOLOCK) WHERE [id] = @Planid ",true, GxErrorMask.GX_NOMASK, false, this,prmT00074,1,0,true,false )
             ,new CursorDef("T00075", "SELECT TM1.[id] AS Informe_Actividadid, TM1.[resultados], TM1.[seguimiento], TM1.[dificultades], TM1.[mejorar], TM1.[comentarios], TM1.[fecha], TM1.[fechaActividad], TM1.[idPlan] AS Planid FROM dbo.[Informe_Actividad] TM1 WITH (NOLOCK) WHERE TM1.[id] = @Informe_Actividadid ORDER BY TM1.[id]  OPTION (FAST 100)",true, GxErrorMask.GX_NOMASK, false, this,prmT00075,100,0,true,false )
             ,new CursorDef("T00076", "SELECT [id] AS Planid FROM dbo.[Plan] WITH (NOLOCK) WHERE [id] = @Planid ",true, GxErrorMask.GX_NOMASK, false, this,prmT00076,1,0,true,false )
             ,new CursorDef("T00077", "SELECT [id] AS Informe_Actividadid FROM dbo.[Informe_Actividad] WITH (NOLOCK) WHERE [id] = @Informe_Actividadid  OPTION (FAST 1)",true, GxErrorMask.GX_NOMASK, false, this,prmT00077,1,0,true,false )
             ,new CursorDef("T00078", "SELECT TOP 1 [id] AS Informe_Actividadid FROM dbo.[Informe_Actividad] WITH (NOLOCK) WHERE ( [id] > @Informe_Actividadid) ORDER BY [id]  OPTION (FAST 1)",true, GxErrorMask.GX_NOMASK, false, this,prmT00078,1,0,true,true )
             ,new CursorDef("T00079", "SELECT TOP 1 [id] AS Informe_Actividadid FROM dbo.[Informe_Actividad] WITH (NOLOCK) WHERE ( [id] < @Informe_Actividadid) ORDER BY [id] DESC  OPTION (FAST 1)",true, GxErrorMask.GX_NOMASK, false, this,prmT00079,1,0,true,true )
             ,new CursorDef("T000710", "INSERT INTO dbo.[Informe_Actividad]([resultados], [seguimiento], [dificultades], [mejorar], [comentarios], [fecha], [fechaActividad], [idPlan]) VALUES(@resultados, @seguimiento, @dificultades, @mejorar, @comentarios, @fecha, @fechaActividad, @Planid); SELECT SCOPE_IDENTITY()", GxErrorMask.GX_NOMASK,prmT000710)
             ,new CursorDef("T000711", "UPDATE dbo.[Informe_Actividad] SET [resultados]=@resultados, [seguimiento]=@seguimiento, [dificultades]=@dificultades, [mejorar]=@mejorar, [comentarios]=@comentarios, [fecha]=@fecha, [fechaActividad]=@fechaActividad, [idPlan]=@Planid  WHERE [id] = @Informe_Actividadid", GxErrorMask.GX_NOMASK,prmT000711)
             ,new CursorDef("T000712", "DELETE FROM dbo.[Informe_Actividad]  WHERE [id] = @Informe_Actividadid", GxErrorMask.GX_NOMASK,prmT000712)
             ,new CursorDef("T000713", "SELECT TOP 1 [id] AS Beneficario_Informe_Actividadi FROM dbo.[Beneficario-Informe_Actividad] WITH (NOLOCK) WHERE [idInformeActividad] = @Informe_Actividadid ",true, GxErrorMask.GX_NOMASK, false, this,prmT000713,1,0,true,true )
             ,new CursorDef("T000714", "SELECT TOP 1 [id] AS Fotoid FROM dbo.[Foto] WITH (NOLOCK) WHERE [idInforme_Actividad] = @Informe_Actividadid ",true, GxErrorMask.GX_NOMASK, false, this,prmT000714,1,0,true,true )
             ,new CursorDef("T000715", "SELECT TOP 1 [id] AS Contacto_Informe_Actividadid FROM dbo.[Contacto-Informe_Actividad] WITH (NOLOCK) WHERE [idInformeActividad] = @Informe_Actividadid ",true, GxErrorMask.GX_NOMASK, false, this,prmT000715,1,0,true,true )
             ,new CursorDef("T000716", "SELECT [id] AS Informe_Actividadid FROM dbo.[Informe_Actividad] WITH (NOLOCK) ORDER BY [id]  OPTION (FAST 100)",true, GxErrorMask.GX_NOMASK, false, this,prmT000716,100,0,true,false )
             ,new CursorDef("T000717", "SELECT [id] AS Planid FROM dbo.[Plan] WITH (NOLOCK) WHERE [id] = @Planid ",true, GxErrorMask.GX_NOMASK, false, this,prmT000717,1,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getVarchar(2) ;
                ((String[]) buf[2])[0] = rslt.getVarchar(3) ;
                ((String[]) buf[3])[0] = rslt.getVarchar(4) ;
                ((String[]) buf[4])[0] = rslt.getVarchar(5) ;
                ((String[]) buf[5])[0] = rslt.getVarchar(6) ;
                ((bool[]) buf[6])[0] = rslt.wasNull(6);
                ((DateTime[]) buf[7])[0] = rslt.getGXDate(7) ;
                ((DateTime[]) buf[8])[0] = rslt.getGXDate(8) ;
                ((bool[]) buf[9])[0] = rslt.wasNull(8);
                ((int[]) buf[10])[0] = rslt.getInt(9) ;
                return;
             case 1 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getVarchar(2) ;
                ((String[]) buf[2])[0] = rslt.getVarchar(3) ;
                ((String[]) buf[3])[0] = rslt.getVarchar(4) ;
                ((String[]) buf[4])[0] = rslt.getVarchar(5) ;
                ((String[]) buf[5])[0] = rslt.getVarchar(6) ;
                ((bool[]) buf[6])[0] = rslt.wasNull(6);
                ((DateTime[]) buf[7])[0] = rslt.getGXDate(7) ;
                ((DateTime[]) buf[8])[0] = rslt.getGXDate(8) ;
                ((bool[]) buf[9])[0] = rslt.wasNull(8);
                ((int[]) buf[10])[0] = rslt.getInt(9) ;
                return;
             case 2 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 3 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getVarchar(2) ;
                ((String[]) buf[2])[0] = rslt.getVarchar(3) ;
                ((String[]) buf[3])[0] = rslt.getVarchar(4) ;
                ((String[]) buf[4])[0] = rslt.getVarchar(5) ;
                ((String[]) buf[5])[0] = rslt.getVarchar(6) ;
                ((bool[]) buf[6])[0] = rslt.wasNull(6);
                ((DateTime[]) buf[7])[0] = rslt.getGXDate(7) ;
                ((DateTime[]) buf[8])[0] = rslt.getGXDate(8) ;
                ((bool[]) buf[9])[0] = rslt.wasNull(8);
                ((int[]) buf[10])[0] = rslt.getInt(9) ;
                return;
             case 4 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 5 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 6 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 7 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 8 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 11 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 12 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 13 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 14 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 15 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 1 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 2 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 3 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 4 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 5 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 6 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 7 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 8 :
                stmt.SetParameter(1, (String)parms[0]);
                stmt.SetParameter(2, (String)parms[1]);
                stmt.SetParameter(3, (String)parms[2]);
                stmt.SetParameter(4, (String)parms[3]);
                if ( (bool)parms[4] )
                {
                   stmt.setNull( 5 , SqlDbType.VarChar );
                }
                else
                {
                   stmt.SetParameter(5, (String)parms[5]);
                }
                stmt.SetParameter(6, (DateTime)parms[6]);
                if ( (bool)parms[7] )
                {
                   stmt.setNull( 7 , SqlDbType.DateTime );
                }
                else
                {
                   stmt.SetParameter(7, (DateTime)parms[8]);
                }
                stmt.SetParameter(8, (int)parms[9]);
                return;
             case 9 :
                stmt.SetParameter(1, (String)parms[0]);
                stmt.SetParameter(2, (String)parms[1]);
                stmt.SetParameter(3, (String)parms[2]);
                stmt.SetParameter(4, (String)parms[3]);
                if ( (bool)parms[4] )
                {
                   stmt.setNull( 5 , SqlDbType.VarChar );
                }
                else
                {
                   stmt.SetParameter(5, (String)parms[5]);
                }
                stmt.SetParameter(6, (DateTime)parms[6]);
                if ( (bool)parms[7] )
                {
                   stmt.setNull( 7 , SqlDbType.DateTime );
                }
                else
                {
                   stmt.SetParameter(7, (DateTime)parms[8]);
                }
                stmt.SetParameter(8, (int)parms[9]);
                stmt.SetParameter(9, (int)parms[10]);
                return;
             case 10 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 11 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 12 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 13 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 15 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
       }
    }

    public String getDataStoreName( )
    {
       return "DATASTORE1";
    }

 }

 public class informe_actividad__default : DataStoreHelperBase, IDataStoreHelper
 {
    public ICursor[] getCursors( )
    {
       cursorDefinitions();
       return new Cursor[] {
     };
  }

  private static CursorDef[] def;
  private void cursorDefinitions( )
  {
     if ( def == null )
     {
        def= new CursorDef[] {
        };
     }
  }

  public void getResults( int cursor ,
                          IFieldGetter rslt ,
                          Object[] buf )
  {
     switch ( cursor )
     {
     }
  }

  public void setParameters( int cursor ,
                             IFieldSetter stmt ,
                             Object[] parms )
  {
     switch ( cursor )
     {
     }
  }

}

}
