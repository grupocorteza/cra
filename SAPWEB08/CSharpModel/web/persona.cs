/*
               File: Persona
        Description: Persona
             Author: GeneXus C# Generator version 16_0_0-127771
       Generated on: 1/4/2019 19:2:7.1
       Program type: Callable routine
          Main DBMS: SQL Server
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Xml.Serialization;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class persona : GXDataArea, System.Web.SessionState.IRequiresSessionState
   {
      protected void INITENV( )
      {
         if ( GxWebError != 0 )
         {
            return  ;
         }
      }

      protected void INITTRN( )
      {
         initialize_properties( ) ;
         entryPointCalled = false;
         gxfirstwebparm = GetNextPar( );
         gxfirstwebparm_bkp = gxfirstwebparm;
         gxfirstwebparm = DecryptAjaxCall( gxfirstwebparm);
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         if ( StringUtil.StrCmp(gxfirstwebparm, "dyncall") == 0 )
         {
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            dyncall( GetNextPar( )) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxExecAct_"+"gxLoad_10") == 0 )
         {
            A15Paisid = (int)(NumberUtil.Val( GetNextPar( ), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A15Paisid", StringUtil.LTrim( StringUtil.Str( (decimal)(A15Paisid), 9, 0)));
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxLoad_10( A15Paisid) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxEvt") == 0 )
         {
            setAjaxEventMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxfirstwebparm = GetNextPar( );
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxfullajaxEvt") == 0 )
         {
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxfirstwebparm = GetNextPar( );
         }
         else
         {
            if ( ! IsValidAjaxCall( false) )
            {
               GxWebError = 1;
               return  ;
            }
            gxfirstwebparm = gxfirstwebparm_bkp;
         }
         if ( ! entryPointCalled && ! ( isAjaxCallMode( ) || isFullAjaxMode( ) ) )
         {
            Gx_mode = gxfirstwebparm;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!")), context));
            if ( StringUtil.StrCmp(gxfirstwebparm, "viewer") != 0 )
            {
               AV7Personaid = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV7Personaid", StringUtil.LTrim( StringUtil.Str( (decimal)(AV7Personaid), 9, 0)));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vPERSONAID", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV7Personaid), "ZZZZZZZZ9"), context));
            }
         }
         if ( context.isSpaRequest( ) )
         {
            enableJsOutput();
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.GetCookie( "GX_SESSION_ID"))) )
         {
            gxcookieaux = context.SetCookie( "GX_SESSION_ID", Encrypt64( Crypto.GetEncryptionKey( ), Crypto.GetServerKey( )), "", (DateTime)(DateTime.MinValue), "", 0);
         }
         GXKey = Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
         toggleJsOutput = isJsOutputEnabled( );
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         init_web_controls( ) ;
         if ( toggleJsOutput )
         {
            if ( context.isSpaRequest( ) )
            {
               enableJsOutput();
            }
         }
         if ( ! context.isSpaRequest( ) )
         {
            Form.Meta.addItem("generator", "GeneXus C# 16_0_0-127771", 0) ;
            Form.Meta.addItem("description", "Persona", 0) ;
         }
         context.wjLoc = "";
         context.nUserReturn = 0;
         context.wbHandled = 0;
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
         }
         GX_FocusControl = edtidentificacion_Internalname;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         wbErr = false;
         context.SetDefaultTheme("Carmine");
         if ( ! context.IsLocalStorageSupported( ) )
         {
            context.PushCurrentUrl();
         }
      }

      public persona( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsDataStore1 = context.GetDataStore("DataStore1");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("Carmine");
      }

      public persona( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsDataStore1 = context.GetDataStore("DataStore1");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( String aP0_Gx_mode ,
                           int aP1_Personaid )
      {
         this.Gx_mode = aP0_Gx_mode;
         this.AV7Personaid = aP1_Personaid;
         executePrivate();
      }

      void executePrivate( )
      {
         isStatic = false;
         webExecute();
      }

      protected override void createObjects( )
      {
      }

      public override void webExecute( )
      {
         if ( initialized == 0 )
         {
            createObjects();
            initialize();
         }
         INITENV( ) ;
         INITTRN( ) ;
         if ( ( GxWebError == 0 ) && ! isAjaxCallMode( ) )
         {
            MasterPageObj = (GXMasterPage) ClassLoader.GetInstance("rwdmasterpage", "GeneXus.Programs.rwdmasterpage", new Object[] {new GxContext( context.handle, context.DataStores, context.HttpContext)});
            MasterPageObj.setDataArea(this,false);
            ValidateSpaRequest();
            MasterPageObj.webExecute();
            if ( ( GxWebError == 0 ) && context.isAjaxRequest( ) )
            {
               enableOutput();
               if ( ! context.isAjaxRequest( ) )
               {
                  context.GX_webresponse.AppendHeader("Cache-Control", "no-store");
               }
               if ( ! context.WillRedirect( ) )
               {
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               }
               else
               {
                  if ( context.isAjaxRequest( ) )
                  {
                     disableOutput();
                  }
                  RenderHtmlHeaders( ) ;
                  context.Redirect( context.wjLoc );
                  context.DispatchAjaxCommands();
               }
            }
         }
         this.cleanup();
      }

      protected void fix_multi_value_controls( )
      {
      }

      protected void Draw( )
      {
         if ( context.isAjaxRequest( ) )
         {
            disableOutput();
         }
         if ( ! GxWebStd.gx_redirect( context) )
         {
            disable_std_buttons( ) ;
            enableDisable( ) ;
            set_caption( ) ;
            /* Form start */
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "Section", "left", "top", " "+"data-gx-base-lib=\"bootstrapv3\""+" "+"data-abstract-form"+" ", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, divMaintable_Internalname, 1, 0, "px", 0, "px", "WWAdvancedContainer", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12 col-sm-8 col-sm-offset-2", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, divTitlecontainer_Internalname, 1, 0, "px", 0, "px", "TableTop", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12", "left", "top", "", "", "div");
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTitle_Internalname, "Persona", "", "", lblTitle_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "Title", 0, "", 1, 1, 0, "HLP_Persona.htm");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12", "left", "top", "", "", "div");
            ClassString = "ErrorViewer";
            StyleString = "";
            GxWebStd.gx_msg_list( context, "", context.GX_msglist.DisplayMode, StyleString, ClassString, "", "false");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12 col-sm-8 col-sm-offset-2", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, divFormcontainer_Internalname, 1, 0, "px", 0, "px", "FormContainer", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, divToolbarcell_Internalname, 1, 0, "px", 0, "px", "col-xs-12 col-sm-9 col-sm-offset-3 ToolbarCellClass", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "gx-action-group ActionGroup", "left", "top", " "+"data-gx-actiongroup-type=\"toolbar\""+" ", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "btn-group", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "gx-button", "left", "top", "", "", "div");
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 21,'',false,'',0)\"";
            ClassString = "BtnFirst";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtn_first_Internalname, "", "", bttBtn_first_Jsonclick, 5, "", "", StyleString, ClassString, bttBtn_first_Visible, 0, "standard", "'"+""+"'"+",false,"+"'"+"EFIRST."+"'", TempTags, "", context.GetButtonType( ), "HLP_Persona.htm");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "gx-button", "left", "top", "", "", "div");
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 23,'',false,'',0)\"";
            ClassString = "BtnPrevious";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtn_previous_Internalname, "", "", bttBtn_previous_Jsonclick, 5, "", "", StyleString, ClassString, bttBtn_previous_Visible, 0, "standard", "'"+""+"'"+",false,"+"'"+"EPREVIOUS."+"'", TempTags, "", context.GetButtonType( ), "HLP_Persona.htm");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "gx-button", "left", "top", "", "", "div");
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 25,'',false,'',0)\"";
            ClassString = "BtnNext";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtn_next_Internalname, "", "", bttBtn_next_Jsonclick, 5, "", "", StyleString, ClassString, bttBtn_next_Visible, 0, "standard", "'"+""+"'"+",false,"+"'"+"ENEXT."+"'", TempTags, "", context.GetButtonType( ), "HLP_Persona.htm");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "gx-button", "left", "top", "", "", "div");
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 27,'',false,'',0)\"";
            ClassString = "BtnLast";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtn_last_Internalname, "", "", bttBtn_last_Jsonclick, 5, "", "", StyleString, ClassString, bttBtn_last_Visible, 0, "standard", "'"+""+"'"+",false,"+"'"+"ELAST."+"'", TempTags, "", context.GetButtonType( ), "HLP_Persona.htm");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "gx-button", "left", "top", "", "", "div");
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 29,'',false,'',0)\"";
            ClassString = "BtnSelect";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtn_select_Internalname, "", "Select", bttBtn_select_Jsonclick, 5, "Select", "", StyleString, ClassString, bttBtn_select_Visible, 0, "standard", "'"+""+"'"+",false,"+"'"+"ESELECT."+"'", TempTags, "", 2, "HLP_Persona.htm");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12 FormCellAdvanced", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "form-group gx-form-group", "left", "top", ""+" data-gx-for=\""+edtidentificacion_Internalname+"\"", "", "div");
            /* Attribute/Variable Label */
            GxWebStd.gx_label_element( context, edtidentificacion_Internalname, "identificacion", "col-sm-3 AttributeLabel", 1, true);
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-sm-9 gx-attribute", "left", "top", "", "", "div");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 34,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtidentificacion_Internalname, A9identificacion, StringUtil.RTrim( context.localUtil.Format( A9identificacion, "")), TempTags+" onchange=\""+""+";gx.evt.onchange(this, event)\" "+" onblur=\""+";gx.evt.onblur(this,34);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtidentificacion_Jsonclick, 0, "Attribute", "", "", "", "", 1, edtidentificacion_Enabled, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_Persona.htm");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12 FormCell", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "form-group gx-form-group", "left", "top", ""+" data-gx-for=\""+edtnombre1_Internalname+"\"", "", "div");
            /* Attribute/Variable Label */
            GxWebStd.gx_label_element( context, edtnombre1_Internalname, "Prim.Nombre", "col-sm-3 AttributeLabel", 1, true);
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-sm-9 gx-attribute", "left", "top", "", "", "div");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 39,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtnombre1_Internalname, A54nombre1, StringUtil.RTrim( context.localUtil.Format( A54nombre1, "")), TempTags+" onchange=\""+""+";gx.evt.onchange(this, event)\" "+" onblur=\""+";gx.evt.onblur(this,39);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtnombre1_Jsonclick, 0, "Attribute", "", "", "", "", 1, edtnombre1_Enabled, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_Persona.htm");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12 FormCell", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "form-group gx-form-group", "left", "top", ""+" data-gx-for=\""+edtnombre2_Internalname+"\"", "", "div");
            /* Attribute/Variable Label */
            GxWebStd.gx_label_element( context, edtnombre2_Internalname, "Seg.Nombre", "col-sm-3 AttributeLabel", 1, true);
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-sm-9 gx-attribute", "left", "top", "", "", "div");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 44,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtnombre2_Internalname, A55nombre2, StringUtil.RTrim( context.localUtil.Format( A55nombre2, "")), TempTags+" onchange=\""+""+";gx.evt.onchange(this, event)\" "+" onblur=\""+";gx.evt.onblur(this,44);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtnombre2_Jsonclick, 0, "Attribute", "", "", "", "", 1, edtnombre2_Enabled, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_Persona.htm");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12 FormCell", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "form-group gx-form-group", "left", "top", ""+" data-gx-for=\""+edtPersonaapellidoPaterno_Internalname+"\"", "", "div");
            /* Attribute/Variable Label */
            GxWebStd.gx_label_element( context, edtPersonaapellidoPaterno_Internalname, "apellido Paterno", "col-sm-3 AttributeLabel", 1, true);
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-sm-9 gx-attribute", "left", "top", "", "", "div");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 49,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtPersonaapellidoPaterno_Internalname, A56PersonaapellidoPaterno, StringUtil.RTrim( context.localUtil.Format( A56PersonaapellidoPaterno, "")), TempTags+" onchange=\""+""+";gx.evt.onchange(this, event)\" "+" onblur=\""+";gx.evt.onblur(this,49);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtPersonaapellidoPaterno_Jsonclick, 0, "Attribute", "", "", "", "", 1, edtPersonaapellidoPaterno_Enabled, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_Persona.htm");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12 FormCell", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "form-group gx-form-group", "left", "top", ""+" data-gx-for=\""+edtPersonaapellidoMaterno_Internalname+"\"", "", "div");
            /* Attribute/Variable Label */
            GxWebStd.gx_label_element( context, edtPersonaapellidoMaterno_Internalname, "apellido Materno", "col-sm-3 AttributeLabel", 1, true);
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-sm-9 gx-attribute", "left", "top", "", "", "div");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 54,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtPersonaapellidoMaterno_Internalname, A57PersonaapellidoMaterno, StringUtil.RTrim( context.localUtil.Format( A57PersonaapellidoMaterno, "")), TempTags+" onchange=\""+""+";gx.evt.onchange(this, event)\" "+" onblur=\""+";gx.evt.onblur(this,54);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtPersonaapellidoMaterno_Jsonclick, 0, "Attribute", "", "", "", "", 1, edtPersonaapellidoMaterno_Enabled, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_Persona.htm");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12 FormCell", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "form-group gx-form-group", "left", "top", ""+" data-gx-for=\""+edtnacimiento_Internalname+"\"", "", "div");
            /* Attribute/Variable Label */
            GxWebStd.gx_label_element( context, edtnacimiento_Internalname, "nacimiento", "col-sm-3 AttributeLabel", 1, true);
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-sm-9 gx-attribute", "left", "top", "", "", "div");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 59,'',false,'',0)\"";
            context.WriteHtmlText( "<div id=\""+edtnacimiento_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtnacimiento_Internalname, context.localUtil.Format(A58nacimiento, "99/99/99"), context.localUtil.Format( A58nacimiento, "99/99/99"), TempTags+" onchange=\""+"gx.date.valid_date(this, 8,'MDY',0,12,'eng',false,0);"+";gx.evt.onchange(this, event)\" "+" onblur=\""+";gx.evt.onblur(this,59);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtnacimiento_Jsonclick, 0, "Attribute", "", "", "", "", 1, edtnacimiento_Enabled, 0, "text", "", 8, "chr", 1, "row", 8, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_Persona.htm");
            GxWebStd.gx_bitmap( context, edtnacimiento_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((1==0)||(edtnacimiento_Enabled==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", "", 1, false, false, "", "HLP_Persona.htm");
            context.WriteHtmlTextNl( "</div>") ;
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12 FormCell", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "form-group gx-form-group", "left", "top", ""+" data-gx-for=\""+edtgenero_Internalname+"\"", "", "div");
            /* Attribute/Variable Label */
            GxWebStd.gx_label_element( context, edtgenero_Internalname, "genero", "col-sm-3 AttributeLabel", 1, true);
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-sm-9 gx-attribute", "left", "top", "", "", "div");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 64,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtgenero_Internalname, A59genero, StringUtil.RTrim( context.localUtil.Format( A59genero, "")), TempTags+" onchange=\""+""+";gx.evt.onchange(this, event)\" "+" onblur=\""+";gx.evt.onblur(this,64);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtgenero_Jsonclick, 0, "Attribute", "", "", "", "", 1, edtgenero_Enabled, 0, "text", "", 1, "chr", 1, "row", 1, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_Persona.htm");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12 FormCell", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "form-group gx-form-group", "left", "top", ""+" data-gx-for=\""+edtPersonacorreo_Internalname+"\"", "", "div");
            /* Attribute/Variable Label */
            GxWebStd.gx_label_element( context, edtPersonacorreo_Internalname, "correo", "col-sm-3 AttributeLabel", 1, true);
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-sm-9 gx-attribute", "left", "top", "", "", "div");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 69,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtPersonacorreo_Internalname, A60Personacorreo, StringUtil.RTrim( context.localUtil.Format( A60Personacorreo, "")), TempTags+" onchange=\""+""+";gx.evt.onchange(this, event)\" "+" onblur=\""+";gx.evt.onblur(this,69);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtPersonacorreo_Jsonclick, 0, "Attribute", "", "", "", "", 1, edtPersonacorreo_Enabled, 0, "text", "", 80, "chr", 1, "row", 100, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_Persona.htm");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12 FormCell", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "form-group gx-form-group", "left", "top", ""+" data-gx-for=\""+edtPaisid_Internalname+"\"", "", "div");
            /* Attribute/Variable Label */
            GxWebStd.gx_label_element( context, edtPaisid_Internalname, "Pa�s", "col-sm-3 AttributeLabel", 1, true);
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-sm-9 gx-attribute", "left", "top", "", "", "div");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 74,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtPaisid_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A15Paisid), 9, 0, ".", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(A15Paisid), "ZZZZZZZZ9")), TempTags+" onchange=\""+"gx.num.valid_integer( this,',');"+";gx.evt.onchange(this, event)\" "+" onblur=\""+";gx.evt.onblur(this,74);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtPaisid_Jsonclick, 0, "Attribute", "", "", "", "", 1, edtPaisid_Enabled, 1, "number", "1", 9, "chr", 1, "row", 9, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_Persona.htm");
            /* Static images/pictures */
            ClassString = "gx-prompt Image";
            StyleString = "";
            sImgUrl = (String)(context.GetImagePath( "prompt.gif", "", context.GetTheme( )));
            GxWebStd.gx_bitmap( context, imgprompt_15_Internalname, sImgUrl, imgprompt_15_Link, "", "", context.GetTheme( ), imgprompt_15_Visible, 1, "", "", 0, 0, 0, "", 0, "", 0, 0, 0, "", "", StyleString, ClassString, "", "", "", "", "", "", "", 1, false, false, context.GetImageSrcSet( sImgUrl), "HLP_Persona.htm");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12 FormCell", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "form-group gx-form-group", "left", "top", ""+" data-gx-for=\""+edtPaisnombre_Internalname+"\"", "", "div");
            /* Attribute/Variable Label */
            GxWebStd.gx_label_element( context, edtPaisnombre_Internalname, "Pa�s", "col-sm-3 AttributeLabel", 1, true);
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-sm-9 gx-attribute", "left", "top", "", "", "div");
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtPaisnombre_Internalname, A118Paisnombre, StringUtil.RTrim( context.localUtil.Format( A118Paisnombre, "")), "", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtPaisnombre_Jsonclick, 0, "Attribute", "", "", "", "", 1, edtPaisnombre_Enabled, 0, "text", "", 40, "chr", 1, "row", 40, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_Persona.htm");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12", "Center", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "gx-action-group Confirm", "left", "top", " "+"data-gx-actiongroup-type=\"toolbar\""+" ", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "gx-button", "left", "top", "", "", "div");
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 84,'',false,'',0)\"";
            ClassString = "BtnEnter";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtn_enter_Internalname, "", "Confirm", bttBtn_enter_Jsonclick, 5, "Confirm", "", StyleString, ClassString, bttBtn_enter_Visible, bttBtn_enter_Enabled, "standard", "'"+""+"'"+",false,"+"'"+"EENTER."+"'", TempTags, "", context.GetButtonType( ), "HLP_Persona.htm");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "gx-button", "left", "top", "", "", "div");
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 86,'',false,'',0)\"";
            ClassString = "BtnCancel";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtn_cancel_Internalname, "", "Cancel", bttBtn_cancel_Jsonclick, 1, "Cancel", "", StyleString, ClassString, bttBtn_cancel_Visible, 1, "standard", "'"+""+"'"+",false,"+"'"+"ECANCEL."+"'", TempTags, "", context.GetButtonType( ), "HLP_Persona.htm");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "gx-button", "left", "top", "", "", "div");
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 88,'',false,'',0)\"";
            ClassString = "BtnDelete";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtn_delete_Internalname, "", "Delete", bttBtn_delete_Jsonclick, 5, "Delete", "", StyleString, ClassString, bttBtn_delete_Visible, bttBtn_delete_Enabled, "standard", "'"+""+"'"+",false,"+"'"+"EDELETE."+"'", TempTags, "", context.GetButtonType( ), "HLP_Persona.htm");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "Center", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            fix_multi_value_controls( ) ;
         }
         /* Execute Exit event if defined. */
      }

      protected void UserMain( )
      {
         standaloneStartup( ) ;
      }

      protected void UserMainFullajax( )
      {
         INITENV( ) ;
         INITTRN( ) ;
         UserMain( ) ;
         Draw( ) ;
         SendCloseFormHiddens( ) ;
      }

      protected void standaloneStartup( )
      {
         standaloneStartupServer( ) ;
         disable_std_buttons( ) ;
         enableDisable( ) ;
         Process( ) ;
      }

      protected void standaloneStartupServer( )
      {
         /* Execute Start event if defined. */
         context.wbGlbDoneStart = 0;
         /* Execute user event: Start */
         E11042 ();
         context.wbGlbDoneStart = 1;
         assign_properties_default( ) ;
         if ( AnyError == 0 )
         {
            if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
            {
               /* Read saved SDTs. */
               /* Read variables values. */
               A9identificacion = cgiGet( edtidentificacion_Internalname);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A9identificacion", A9identificacion);
               A54nombre1 = cgiGet( edtnombre1_Internalname);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A54nombre1", A54nombre1);
               A55nombre2 = cgiGet( edtnombre2_Internalname);
               n55nombre2 = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A55nombre2", A55nombre2);
               n55nombre2 = (String.IsNullOrEmpty(StringUtil.RTrim( A55nombre2)) ? true : false);
               A56PersonaapellidoPaterno = cgiGet( edtPersonaapellidoPaterno_Internalname);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A56PersonaapellidoPaterno", A56PersonaapellidoPaterno);
               A57PersonaapellidoMaterno = cgiGet( edtPersonaapellidoMaterno_Internalname);
               n57PersonaapellidoMaterno = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A57PersonaapellidoMaterno", A57PersonaapellidoMaterno);
               n57PersonaapellidoMaterno = (String.IsNullOrEmpty(StringUtil.RTrim( A57PersonaapellidoMaterno)) ? true : false);
               if ( context.localUtil.VCDate( cgiGet( edtnacimiento_Internalname), 1) == 0 )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_faildate", new   object[]  {"nacimiento"}), 1, "NACIMIENTO");
                  AnyError = 1;
                  GX_FocusControl = edtnacimiento_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  wbErr = true;
                  A58nacimiento = DateTime.MinValue;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A58nacimiento", context.localUtil.Format(A58nacimiento, "99/99/99"));
               }
               else
               {
                  A58nacimiento = context.localUtil.CToD( cgiGet( edtnacimiento_Internalname), 1);
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A58nacimiento", context.localUtil.Format(A58nacimiento, "99/99/99"));
               }
               A59genero = cgiGet( edtgenero_Internalname);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A59genero", A59genero);
               A60Personacorreo = cgiGet( edtPersonacorreo_Internalname);
               n60Personacorreo = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A60Personacorreo", A60Personacorreo);
               n60Personacorreo = (String.IsNullOrEmpty(StringUtil.RTrim( A60Personacorreo)) ? true : false);
               if ( ( ( context.localUtil.CToN( cgiGet( edtPaisid_Internalname), ".", ",") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtPaisid_Internalname), ".", ",") > Convert.ToDecimal( 999999999 )) ) )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "PAISID");
                  AnyError = 1;
                  GX_FocusControl = edtPaisid_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  wbErr = true;
                  A15Paisid = 0;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A15Paisid", StringUtil.LTrim( StringUtil.Str( (decimal)(A15Paisid), 9, 0)));
               }
               else
               {
                  A15Paisid = (int)(context.localUtil.CToN( cgiGet( edtPaisid_Internalname), ".", ","));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A15Paisid", StringUtil.LTrim( StringUtil.Str( (decimal)(A15Paisid), 9, 0)));
               }
               A118Paisnombre = cgiGet( edtPaisnombre_Internalname);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A118Paisnombre", A118Paisnombre);
               /* Read saved values. */
               Z8Personaid = (int)(context.localUtil.CToN( cgiGet( "Z8Personaid"), ".", ","));
               Z9identificacion = cgiGet( "Z9identificacion");
               Z54nombre1 = cgiGet( "Z54nombre1");
               Z55nombre2 = cgiGet( "Z55nombre2");
               n55nombre2 = (String.IsNullOrEmpty(StringUtil.RTrim( A55nombre2)) ? true : false);
               Z56PersonaapellidoPaterno = cgiGet( "Z56PersonaapellidoPaterno");
               Z57PersonaapellidoMaterno = cgiGet( "Z57PersonaapellidoMaterno");
               n57PersonaapellidoMaterno = (String.IsNullOrEmpty(StringUtil.RTrim( A57PersonaapellidoMaterno)) ? true : false);
               Z58nacimiento = context.localUtil.CToD( cgiGet( "Z58nacimiento"), 0);
               Z59genero = cgiGet( "Z59genero");
               Z60Personacorreo = cgiGet( "Z60Personacorreo");
               n60Personacorreo = (String.IsNullOrEmpty(StringUtil.RTrim( A60Personacorreo)) ? true : false);
               Z15Paisid = (int)(context.localUtil.CToN( cgiGet( "Z15Paisid"), ".", ","));
               IsConfirmed = (short)(context.localUtil.CToN( cgiGet( "IsConfirmed"), ".", ","));
               IsModified = (short)(context.localUtil.CToN( cgiGet( "IsModified"), ".", ","));
               Gx_mode = cgiGet( "Mode");
               N15Paisid = (int)(context.localUtil.CToN( cgiGet( "N15Paisid"), ".", ","));
               AV7Personaid = (int)(context.localUtil.CToN( cgiGet( "vPERSONAID"), ".", ","));
               A8Personaid = (int)(context.localUtil.CToN( cgiGet( "PERSONAID"), ".", ","));
               AV11Insert_Paisid = (int)(context.localUtil.CToN( cgiGet( "vINSERT_PAISID"), ".", ","));
               AV13Pgmname = cgiGet( "vPGMNAME");
               Gx_mode = cgiGet( "vMODE");
               /* Read subfile selected row values. */
               /* Read hidden variables. */
               GXKey = Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
               forbiddenHiddens = "hsh" + "Persona";
               forbiddenHiddens = forbiddenHiddens + context.localUtil.Format( (decimal)(AV11Insert_Paisid), "ZZZZZZZZ9");
               forbiddenHiddens = forbiddenHiddens + StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"));
               forbiddenHiddens = forbiddenHiddens + context.localUtil.Format( (decimal)(A8Personaid), "ZZZZZZZZ9");
               hsh = cgiGet( "hsh");
               if ( ! GXUtil.CheckEncryptedHash( forbiddenHiddens, hsh, GXKey) )
               {
                  GXUtil.WriteLog("persona:[SecurityCheckFailed value for]"+"Insert_Paisid:"+context.localUtil.Format( (decimal)(AV11Insert_Paisid), "ZZZZZZZZ9"));
                  GXUtil.WriteLog("persona:[SecurityCheckFailed value for]"+"Gx_mode:"+StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!")));
                  GXUtil.WriteLog("persona:[SecurityCheckFailed value for]"+"Personaid:"+context.localUtil.Format( (decimal)(A8Personaid), "ZZZZZZZZ9"));
                  GxWebError = 1;
                  context.HttpContext.Response.StatusDescription = 403.ToString();
                  context.HttpContext.Response.StatusCode = 403;
                  context.WriteHtmlText( "<title>403 Forbidden</title>") ;
                  context.WriteHtmlText( "<h1>403 Forbidden</h1>") ;
                  context.WriteHtmlText( "<p /><hr />") ;
                  GXUtil.WriteLog("send_http_error_code " + 403.ToString());
                  AnyError = 1;
                  return  ;
               }
               standaloneNotModal( ) ;
            }
            else
            {
               standaloneNotModal( ) ;
               if ( StringUtil.StrCmp(gxfirstwebparm, "viewer") == 0 )
               {
                  Gx_mode = "DSP";
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!")), context));
                  A8Personaid = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A8Personaid", StringUtil.LTrim( StringUtil.Str( (decimal)(A8Personaid), 9, 0)));
                  getEqualNoModal( ) ;
                  Gx_mode = "DSP";
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!")), context));
                  disable_std_buttons( ) ;
                  standaloneModal( ) ;
               }
               else
               {
                  if ( StringUtil.StrCmp(Gx_mode, "DSP") == 0 )
                  {
                     sMode4 = Gx_mode;
                     Gx_mode = "UPD";
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!")), context));
                     Gx_mode = sMode4;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!")), context));
                  }
                  standaloneModal( ) ;
                  if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
                  {
                     getByPrimaryKey( ) ;
                     if ( RcdFound4 == 1 )
                     {
                        if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
                        {
                           /* Confirm record */
                           CONFIRM_040( ) ;
                           if ( AnyError == 0 )
                           {
                              GX_FocusControl = bttBtn_enter_Internalname;
                              context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                           }
                        }
                     }
                     else
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_noinsert", ""), 1, "");
                        AnyError = 1;
                     }
                  }
               }
            }
         }
      }

      protected void Process( )
      {
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            /* Read Transaction buttons. */
            sEvt = cgiGet( "_EventName");
            EvtGridId = cgiGet( "_EventGridId");
            EvtRowId = cgiGet( "_EventRowId");
            if ( StringUtil.Len( sEvt) > 0 )
            {
               sEvtType = StringUtil.Left( sEvt, 1);
               sEvt = StringUtil.Right( sEvt, (short)(StringUtil.Len( sEvt)-1));
               if ( StringUtil.StrCmp(sEvtType, "M") != 0 )
               {
                  if ( StringUtil.StrCmp(sEvtType, "E") == 0 )
                  {
                     sEvtType = StringUtil.Right( sEvt, 1);
                     if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                     {
                        sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                        if ( StringUtil.StrCmp(sEvt, "START") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: Start */
                           E11042 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "AFTER TRN") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: After Trn */
                           E12042 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "ENTER") == 0 )
                        {
                           context.wbHandled = 1;
                           if ( StringUtil.StrCmp(Gx_mode, "DSP") != 0 )
                           {
                              btn_enter( ) ;
                           }
                           /* No code required for Cancel button. It is implemented as the Reset button. */
                        }
                     }
                     else
                     {
                     }
                  }
                  context.wbHandled = 1;
               }
            }
         }
      }

      protected void AfterTrn( )
      {
         if ( trnEnded == 1 )
         {
            /* Execute user event: After Trn */
            E12042 ();
            trnEnded = 0;
            standaloneNotModal( ) ;
            standaloneModal( ) ;
            if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  )
            {
               /* Clear variables for new insertion. */
               InitAll044( ) ;
               standaloneNotModal( ) ;
               standaloneModal( ) ;
            }
         }
      }

      public override String ToString( )
      {
         return "" ;
      }

      public GxContentInfo GetContentInfo( )
      {
         return (GxContentInfo)(null) ;
      }

      protected void disable_std_buttons( )
      {
         bttBtn_delete_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_delete_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_delete_Visible), 5, 0)), true);
         bttBtn_first_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_first_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_first_Visible), 5, 0)), true);
         bttBtn_previous_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_previous_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_previous_Visible), 5, 0)), true);
         bttBtn_next_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_next_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_next_Visible), 5, 0)), true);
         bttBtn_last_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_last_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_last_Visible), 5, 0)), true);
         bttBtn_select_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_select_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_select_Visible), 5, 0)), true);
         if ( ( StringUtil.StrCmp(Gx_mode, "DSP") == 0 ) || ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 ) )
         {
            bttBtn_delete_Visible = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_delete_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_delete_Visible), 5, 0)), true);
            if ( StringUtil.StrCmp(Gx_mode, "DSP") == 0 )
            {
               bttBtn_enter_Visible = 0;
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_enter_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_enter_Visible), 5, 0)), true);
            }
            DisableAttributes044( ) ;
         }
      }

      protected void set_caption( )
      {
         if ( ( IsConfirmed == 1 ) && ( AnyError == 0 ) )
         {
            if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_confdelete", ""), 0, "", true);
            }
            else
            {
               GX_msglist.addItem(context.GetMessage( "GXM_mustconfirm", ""), 0, "", true);
            }
         }
      }

      protected void CONFIRM_040( )
      {
         BeforeValidate044( ) ;
         if ( AnyError == 0 )
         {
            if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
            {
               OnDeleteControls044( ) ;
            }
            else
            {
               CheckExtendedTable044( ) ;
               CloseExtendedTableCursors044( ) ;
            }
         }
         if ( AnyError == 0 )
         {
            IsConfirmed = 1;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "IsConfirmed", StringUtil.LTrim( StringUtil.Str( (decimal)(IsConfirmed), 4, 0)));
         }
      }

      protected void ResetCaption040( )
      {
      }

      protected void E11042( )
      {
         /* Start Routine */
         if ( ! new isauthorized(context).executeUdp(  AV13Pgmname) )
         {
            CallWebObject(formatLink("notauthorized.aspx") + "?" + UrlEncode(StringUtil.RTrim(AV13Pgmname)));
            context.wjLocDisableFrm = 1;
         }
         AV9TrnContext.FromXml(AV10WebSession.Get("TrnContext"), null, "TransactionContext", "SAPWEB08");
         AV11Insert_Paisid = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV11Insert_Paisid", StringUtil.LTrim( StringUtil.Str( (decimal)(AV11Insert_Paisid), 9, 0)));
         if ( ( StringUtil.StrCmp(AV9TrnContext.gxTpr_Transactionname, AV13Pgmname) == 0 ) && ( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) )
         {
            AV14GXV1 = 1;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14GXV1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV14GXV1), 8, 0)));
            while ( AV14GXV1 <= AV9TrnContext.gxTpr_Attributes.Count )
            {
               AV12TrnContextAtt = ((SdtTransactionContext_Attribute)AV9TrnContext.gxTpr_Attributes.Item(AV14GXV1));
               if ( StringUtil.StrCmp(AV12TrnContextAtt.gxTpr_Attributename, "Paisid") == 0 )
               {
                  AV11Insert_Paisid = (int)(NumberUtil.Val( AV12TrnContextAtt.gxTpr_Attributevalue, "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV11Insert_Paisid", StringUtil.LTrim( StringUtil.Str( (decimal)(AV11Insert_Paisid), 9, 0)));
               }
               AV14GXV1 = (int)(AV14GXV1+1);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14GXV1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV14GXV1), 8, 0)));
            }
         }
      }

      protected void E12042( )
      {
         /* After Trn Routine */
         if ( ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 ) && ! AV9TrnContext.gxTpr_Callerondelete )
         {
            CallWebObject(formatLink("wwpersona.aspx") );
            context.wjLocDisableFrm = 1;
         }
         context.setWebReturnParms(new Object[] {});
         context.setWebReturnParmsMetadata(new Object[] {});
         context.wjLocDisableFrm = 1;
         context.nUserReturn = 1;
         returnInSub = true;
         if (true) return;
      }

      protected void ZM044( short GX_JID )
      {
         if ( ( GX_JID == 8 ) || ( GX_JID == 0 ) )
         {
            if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
            {
               Z9identificacion = T00043_A9identificacion[0];
               Z54nombre1 = T00043_A54nombre1[0];
               Z55nombre2 = T00043_A55nombre2[0];
               Z56PersonaapellidoPaterno = T00043_A56PersonaapellidoPaterno[0];
               Z57PersonaapellidoMaterno = T00043_A57PersonaapellidoMaterno[0];
               Z58nacimiento = T00043_A58nacimiento[0];
               Z59genero = T00043_A59genero[0];
               Z60Personacorreo = T00043_A60Personacorreo[0];
               Z15Paisid = T00043_A15Paisid[0];
            }
            else
            {
               Z9identificacion = A9identificacion;
               Z54nombre1 = A54nombre1;
               Z55nombre2 = A55nombre2;
               Z56PersonaapellidoPaterno = A56PersonaapellidoPaterno;
               Z57PersonaapellidoMaterno = A57PersonaapellidoMaterno;
               Z58nacimiento = A58nacimiento;
               Z59genero = A59genero;
               Z60Personacorreo = A60Personacorreo;
               Z15Paisid = A15Paisid;
            }
         }
         if ( GX_JID == -8 )
         {
            Z8Personaid = A8Personaid;
            Z9identificacion = A9identificacion;
            Z54nombre1 = A54nombre1;
            Z55nombre2 = A55nombre2;
            Z56PersonaapellidoPaterno = A56PersonaapellidoPaterno;
            Z57PersonaapellidoMaterno = A57PersonaapellidoMaterno;
            Z58nacimiento = A58nacimiento;
            Z59genero = A59genero;
            Z60Personacorreo = A60Personacorreo;
            Z15Paisid = A15Paisid;
            Z118Paisnombre = A118Paisnombre;
         }
      }

      protected void standaloneNotModal( )
      {
         imgprompt_15_Link = ((StringUtil.StrCmp(Gx_mode, "DSP")==0) ? "" : "javascript:"+"gx.popup.openPrompt('"+"gx00d0.aspx"+"',["+"{Ctrl:gx.dom.el('"+"PAISID"+"'), id:'"+"PAISID"+"'"+",IOType:'out'}"+"],"+"null"+","+"'', false"+","+"false"+");");
         bttBtn_delete_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_delete_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_delete_Enabled), 5, 0)), true);
         if ( ! (0==AV7Personaid) )
         {
            A8Personaid = AV7Personaid;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A8Personaid", StringUtil.LTrim( StringUtil.Str( (decimal)(A8Personaid), 9, 0)));
         }
         if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) && ! (0==AV11Insert_Paisid) )
         {
            edtPaisid_Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtPaisid_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtPaisid_Enabled), 5, 0)), true);
         }
         else
         {
            edtPaisid_Enabled = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtPaisid_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtPaisid_Enabled), 5, 0)), true);
         }
      }

      protected void standaloneModal( )
      {
         if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) && ! (0==AV11Insert_Paisid) )
         {
            A15Paisid = AV11Insert_Paisid;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A15Paisid", StringUtil.LTrim( StringUtil.Str( (decimal)(A15Paisid), 9, 0)));
         }
         if ( StringUtil.StrCmp(Gx_mode, "DSP") == 0 )
         {
            bttBtn_enter_Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_enter_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_enter_Enabled), 5, 0)), true);
         }
         else
         {
            bttBtn_enter_Enabled = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_enter_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_enter_Enabled), 5, 0)), true);
         }
         if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) && ( Gx_BScreen == 0 ) )
         {
            AV13Pgmname = "Persona";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13Pgmname", AV13Pgmname);
            /* Using cursor T00044 */
            pr_datastore1.execute(2, new Object[] {A15Paisid});
            A118Paisnombre = T00044_A118Paisnombre[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A118Paisnombre", A118Paisnombre);
            pr_datastore1.close(2);
         }
      }

      protected void Load044( )
      {
         /* Using cursor T00045 */
         pr_datastore1.execute(3, new Object[] {A8Personaid});
         if ( (pr_datastore1.getStatus(3) != 101) )
         {
            RcdFound4 = 1;
            A9identificacion = T00045_A9identificacion[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A9identificacion", A9identificacion);
            A54nombre1 = T00045_A54nombre1[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A54nombre1", A54nombre1);
            A55nombre2 = T00045_A55nombre2[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A55nombre2", A55nombre2);
            n55nombre2 = T00045_n55nombre2[0];
            A56PersonaapellidoPaterno = T00045_A56PersonaapellidoPaterno[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A56PersonaapellidoPaterno", A56PersonaapellidoPaterno);
            A57PersonaapellidoMaterno = T00045_A57PersonaapellidoMaterno[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A57PersonaapellidoMaterno", A57PersonaapellidoMaterno);
            n57PersonaapellidoMaterno = T00045_n57PersonaapellidoMaterno[0];
            A58nacimiento = T00045_A58nacimiento[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A58nacimiento", context.localUtil.Format(A58nacimiento, "99/99/99"));
            A59genero = T00045_A59genero[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A59genero", A59genero);
            A60Personacorreo = T00045_A60Personacorreo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A60Personacorreo", A60Personacorreo);
            n60Personacorreo = T00045_n60Personacorreo[0];
            A118Paisnombre = T00045_A118Paisnombre[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A118Paisnombre", A118Paisnombre);
            A15Paisid = T00045_A15Paisid[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A15Paisid", StringUtil.LTrim( StringUtil.Str( (decimal)(A15Paisid), 9, 0)));
            ZM044( -8) ;
         }
         pr_datastore1.close(3);
         OnLoadActions044( ) ;
      }

      protected void OnLoadActions044( )
      {
         AV13Pgmname = "Persona";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13Pgmname", AV13Pgmname);
      }

      protected void CheckExtendedTable044( )
      {
         Gx_BScreen = 1;
         standaloneModal( ) ;
         AV13Pgmname = "Persona";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13Pgmname", AV13Pgmname);
         if ( ! ( (DateTime.MinValue==A58nacimiento) || ( A58nacimiento >= context.localUtil.YMDToD( 1753, 1, 1) ) ) )
         {
            GX_msglist.addItem("Field nacimiento is out of range", "OutOfRange", 1, "NACIMIENTO");
            AnyError = 1;
            GX_FocusControl = edtnacimiento_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         /* Using cursor T00044 */
         pr_datastore1.execute(2, new Object[] {A15Paisid});
         if ( (pr_datastore1.getStatus(2) == 101) )
         {
            GX_msglist.addItem("No matching 'Pais'.", "ForeignKeyNotFound", 1, "PAISID");
            AnyError = 1;
            GX_FocusControl = edtPaisid_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         A118Paisnombre = T00044_A118Paisnombre[0];
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A118Paisnombre", A118Paisnombre);
         pr_datastore1.close(2);
         /* Using cursor T00046 */
         pr_datastore1.execute(4, new Object[] {A9identificacion, A8Personaid});
         if ( (pr_datastore1.getStatus(4) != 101) )
         {
            GX_msglist.addItem(context.GetMessage( "GXM_1004", new   object[]  {"identificacion"}), 1, "IDENTIFICACION");
            AnyError = 1;
            GX_FocusControl = edtidentificacion_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         pr_datastore1.close(4);
      }

      protected void CloseExtendedTableCursors044( )
      {
         pr_datastore1.close(2);
      }

      protected void enableDisable( )
      {
      }

      protected void gxLoad_10( int A15Paisid )
      {
         /* Using cursor T00047 */
         pr_datastore1.execute(5, new Object[] {A15Paisid});
         if ( (pr_datastore1.getStatus(5) == 101) )
         {
            GX_msglist.addItem("No matching 'Pais'.", "ForeignKeyNotFound", 1, "PAISID");
            AnyError = 1;
            GX_FocusControl = edtPaisid_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         A118Paisnombre = T00047_A118Paisnombre[0];
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A118Paisnombre", A118Paisnombre);
         GxWebStd.set_html_headers( context, 0, "", "");
         context.GX_webresponse.AddString("[[");
         context.GX_webresponse.AddString("\""+GXUtil.EncodeJSConstant( A118Paisnombre)+"\"");
         context.GX_webresponse.AddString("]");
         if ( (pr_datastore1.getStatus(5) == 101) )
         {
            context.GX_webresponse.AddString(",");
            context.GX_webresponse.AddString("101");
         }
         context.GX_webresponse.AddString("]");
         pr_datastore1.close(5);
      }

      protected void GetKey044( )
      {
         /* Using cursor T00048 */
         pr_datastore1.execute(6, new Object[] {A8Personaid});
         if ( (pr_datastore1.getStatus(6) != 101) )
         {
            RcdFound4 = 1;
         }
         else
         {
            RcdFound4 = 0;
         }
         pr_datastore1.close(6);
      }

      protected void getByPrimaryKey( )
      {
         /* Using cursor T00043 */
         pr_datastore1.execute(1, new Object[] {A8Personaid});
         if ( (pr_datastore1.getStatus(1) != 101) )
         {
            ZM044( 8) ;
            RcdFound4 = 1;
            A8Personaid = T00043_A8Personaid[0];
            A9identificacion = T00043_A9identificacion[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A9identificacion", A9identificacion);
            A54nombre1 = T00043_A54nombre1[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A54nombre1", A54nombre1);
            A55nombre2 = T00043_A55nombre2[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A55nombre2", A55nombre2);
            n55nombre2 = T00043_n55nombre2[0];
            A56PersonaapellidoPaterno = T00043_A56PersonaapellidoPaterno[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A56PersonaapellidoPaterno", A56PersonaapellidoPaterno);
            A57PersonaapellidoMaterno = T00043_A57PersonaapellidoMaterno[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A57PersonaapellidoMaterno", A57PersonaapellidoMaterno);
            n57PersonaapellidoMaterno = T00043_n57PersonaapellidoMaterno[0];
            A58nacimiento = T00043_A58nacimiento[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A58nacimiento", context.localUtil.Format(A58nacimiento, "99/99/99"));
            A59genero = T00043_A59genero[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A59genero", A59genero);
            A60Personacorreo = T00043_A60Personacorreo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A60Personacorreo", A60Personacorreo);
            n60Personacorreo = T00043_n60Personacorreo[0];
            A15Paisid = T00043_A15Paisid[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A15Paisid", StringUtil.LTrim( StringUtil.Str( (decimal)(A15Paisid), 9, 0)));
            Z8Personaid = A8Personaid;
            sMode4 = Gx_mode;
            Gx_mode = "DSP";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!")), context));
            Load044( ) ;
            if ( AnyError == 1 )
            {
               RcdFound4 = 0;
               InitializeNonKey044( ) ;
            }
            Gx_mode = sMode4;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!")), context));
         }
         else
         {
            RcdFound4 = 0;
            InitializeNonKey044( ) ;
            sMode4 = Gx_mode;
            Gx_mode = "DSP";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!")), context));
            standaloneModal( ) ;
            Gx_mode = sMode4;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!")), context));
         }
         pr_datastore1.close(1);
      }

      protected void getEqualNoModal( )
      {
         GetKey044( ) ;
         if ( RcdFound4 == 0 )
         {
         }
         else
         {
         }
         getByPrimaryKey( ) ;
      }

      protected void move_next( )
      {
         RcdFound4 = 0;
         /* Using cursor T00049 */
         pr_datastore1.execute(7, new Object[] {A8Personaid});
         if ( (pr_datastore1.getStatus(7) != 101) )
         {
            while ( (pr_datastore1.getStatus(7) != 101) && ( ( T00049_A8Personaid[0] < A8Personaid ) ) )
            {
               pr_datastore1.readNext(7);
            }
            if ( (pr_datastore1.getStatus(7) != 101) && ( ( T00049_A8Personaid[0] > A8Personaid ) ) )
            {
               A8Personaid = T00049_A8Personaid[0];
               RcdFound4 = 1;
            }
         }
         pr_datastore1.close(7);
      }

      protected void move_previous( )
      {
         RcdFound4 = 0;
         /* Using cursor T000410 */
         pr_datastore1.execute(8, new Object[] {A8Personaid});
         if ( (pr_datastore1.getStatus(8) != 101) )
         {
            while ( (pr_datastore1.getStatus(8) != 101) && ( ( T000410_A8Personaid[0] > A8Personaid ) ) )
            {
               pr_datastore1.readNext(8);
            }
            if ( (pr_datastore1.getStatus(8) != 101) && ( ( T000410_A8Personaid[0] < A8Personaid ) ) )
            {
               A8Personaid = T000410_A8Personaid[0];
               RcdFound4 = 1;
            }
         }
         pr_datastore1.close(8);
      }

      protected void btn_enter( )
      {
         nKeyPressed = 1;
         GetKey044( ) ;
         if ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )
         {
            /* Insert record */
            GX_FocusControl = edtidentificacion_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            Insert044( ) ;
            if ( AnyError == 1 )
            {
               GX_FocusControl = "";
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            }
         }
         else
         {
            if ( RcdFound4 == 1 )
            {
               if ( A8Personaid != Z8Personaid )
               {
                  A8Personaid = Z8Personaid;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A8Personaid", StringUtil.LTrim( StringUtil.Str( (decimal)(A8Personaid), 9, 0)));
                  GX_msglist.addItem(context.GetMessage( "GXM_getbeforeupd", ""), "CandidateKeyNotFound", 1, "");
                  AnyError = 1;
               }
               else if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
               {
                  delete( ) ;
                  AfterTrn( ) ;
                  GX_FocusControl = edtidentificacion_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               }
               else
               {
                  /* Update record */
                  Update044( ) ;
                  GX_FocusControl = edtidentificacion_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               }
            }
            else
            {
               if ( A8Personaid != Z8Personaid )
               {
                  /* Insert record */
                  GX_FocusControl = edtidentificacion_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  Insert044( ) ;
                  if ( AnyError == 1 )
                  {
                     GX_FocusControl = "";
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  }
               }
               else
               {
                  if ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 )
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_recdeleted", ""), 1, "");
                     AnyError = 1;
                  }
                  else
                  {
                     /* Insert record */
                     GX_FocusControl = edtidentificacion_Internalname;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                     Insert044( ) ;
                     if ( AnyError == 1 )
                     {
                        GX_FocusControl = "";
                        context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                     }
                  }
               }
            }
         }
         AfterTrn( ) ;
         if ( ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 ) || ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 ) )
         {
            if ( AnyError == 0 )
            {
               context.nUserReturn = 1;
            }
         }
      }

      protected void btn_delete( )
      {
         if ( A8Personaid != Z8Personaid )
         {
            A8Personaid = Z8Personaid;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A8Personaid", StringUtil.LTrim( StringUtil.Str( (decimal)(A8Personaid), 9, 0)));
            GX_msglist.addItem(context.GetMessage( "GXM_getbeforedlt", ""), 1, "");
            AnyError = 1;
         }
         else
         {
            delete( ) ;
            AfterTrn( ) ;
            GX_FocusControl = edtidentificacion_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         if ( AnyError != 0 )
         {
         }
      }

      protected void CheckOptimisticConcurrency044( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
         {
            /* Using cursor T00042 */
            pr_datastore1.execute(0, new Object[] {A8Personaid});
            if ( (pr_datastore1.getStatus(0) == 103) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_lock", new   object[]  {"PERSONA"}), "RecordIsLocked", 1, "");
               AnyError = 1;
               return  ;
            }
            Gx_longc = false;
            if ( (pr_datastore1.getStatus(0) == 101) || ( StringUtil.StrCmp(Z9identificacion, T00042_A9identificacion[0]) != 0 ) || ( StringUtil.StrCmp(Z54nombre1, T00042_A54nombre1[0]) != 0 ) || ( StringUtil.StrCmp(Z55nombre2, T00042_A55nombre2[0]) != 0 ) || ( StringUtil.StrCmp(Z56PersonaapellidoPaterno, T00042_A56PersonaapellidoPaterno[0]) != 0 ) || ( StringUtil.StrCmp(Z57PersonaapellidoMaterno, T00042_A57PersonaapellidoMaterno[0]) != 0 ) )
            {
               Gx_longc = true;
            }
            if ( Gx_longc || ( Z58nacimiento != T00042_A58nacimiento[0] ) || ( StringUtil.StrCmp(Z59genero, T00042_A59genero[0]) != 0 ) || ( StringUtil.StrCmp(Z60Personacorreo, T00042_A60Personacorreo[0]) != 0 ) || ( Z15Paisid != T00042_A15Paisid[0] ) )
            {
               if ( StringUtil.StrCmp(Z9identificacion, T00042_A9identificacion[0]) != 0 )
               {
                  GXUtil.WriteLog("persona:[seudo value changed for attri]"+"identificacion");
                  GXUtil.WriteLogRaw("Old: ",Z9identificacion);
                  GXUtil.WriteLogRaw("Current: ",T00042_A9identificacion[0]);
               }
               if ( StringUtil.StrCmp(Z54nombre1, T00042_A54nombre1[0]) != 0 )
               {
                  GXUtil.WriteLog("persona:[seudo value changed for attri]"+"nombre1");
                  GXUtil.WriteLogRaw("Old: ",Z54nombre1);
                  GXUtil.WriteLogRaw("Current: ",T00042_A54nombre1[0]);
               }
               if ( StringUtil.StrCmp(Z55nombre2, T00042_A55nombre2[0]) != 0 )
               {
                  GXUtil.WriteLog("persona:[seudo value changed for attri]"+"nombre2");
                  GXUtil.WriteLogRaw("Old: ",Z55nombre2);
                  GXUtil.WriteLogRaw("Current: ",T00042_A55nombre2[0]);
               }
               if ( StringUtil.StrCmp(Z56PersonaapellidoPaterno, T00042_A56PersonaapellidoPaterno[0]) != 0 )
               {
                  GXUtil.WriteLog("persona:[seudo value changed for attri]"+"PersonaapellidoPaterno");
                  GXUtil.WriteLogRaw("Old: ",Z56PersonaapellidoPaterno);
                  GXUtil.WriteLogRaw("Current: ",T00042_A56PersonaapellidoPaterno[0]);
               }
               if ( StringUtil.StrCmp(Z57PersonaapellidoMaterno, T00042_A57PersonaapellidoMaterno[0]) != 0 )
               {
                  GXUtil.WriteLog("persona:[seudo value changed for attri]"+"PersonaapellidoMaterno");
                  GXUtil.WriteLogRaw("Old: ",Z57PersonaapellidoMaterno);
                  GXUtil.WriteLogRaw("Current: ",T00042_A57PersonaapellidoMaterno[0]);
               }
               if ( Z58nacimiento != T00042_A58nacimiento[0] )
               {
                  GXUtil.WriteLog("persona:[seudo value changed for attri]"+"nacimiento");
                  GXUtil.WriteLogRaw("Old: ",Z58nacimiento);
                  GXUtil.WriteLogRaw("Current: ",T00042_A58nacimiento[0]);
               }
               if ( StringUtil.StrCmp(Z59genero, T00042_A59genero[0]) != 0 )
               {
                  GXUtil.WriteLog("persona:[seudo value changed for attri]"+"genero");
                  GXUtil.WriteLogRaw("Old: ",Z59genero);
                  GXUtil.WriteLogRaw("Current: ",T00042_A59genero[0]);
               }
               if ( StringUtil.StrCmp(Z60Personacorreo, T00042_A60Personacorreo[0]) != 0 )
               {
                  GXUtil.WriteLog("persona:[seudo value changed for attri]"+"Personacorreo");
                  GXUtil.WriteLogRaw("Old: ",Z60Personacorreo);
                  GXUtil.WriteLogRaw("Current: ",T00042_A60Personacorreo[0]);
               }
               if ( Z15Paisid != T00042_A15Paisid[0] )
               {
                  GXUtil.WriteLog("persona:[seudo value changed for attri]"+"Paisid");
                  GXUtil.WriteLogRaw("Old: ",Z15Paisid);
                  GXUtil.WriteLogRaw("Current: ",T00042_A15Paisid[0]);
               }
               GX_msglist.addItem(context.GetMessage( "GXM_waschg", new   object[]  {"PERSONA"}), "RecordWasChanged", 1, "");
               AnyError = 1;
               return  ;
            }
         }
      }

      protected void Insert044( )
      {
         BeforeValidate044( ) ;
         if ( AnyError == 0 )
         {
            CheckExtendedTable044( ) ;
         }
         if ( AnyError == 0 )
         {
            ZM044( 0) ;
            CheckOptimisticConcurrency044( ) ;
            if ( AnyError == 0 )
            {
               AfterConfirm044( ) ;
               if ( AnyError == 0 )
               {
                  BeforeInsert044( ) ;
                  if ( AnyError == 0 )
                  {
                     /* Using cursor T000411 */
                     pr_datastore1.execute(9, new Object[] {A9identificacion, A54nombre1, n55nombre2, A55nombre2, A56PersonaapellidoPaterno, n57PersonaapellidoMaterno, A57PersonaapellidoMaterno, A58nacimiento, A59genero, n60Personacorreo, A60Personacorreo, A15Paisid});
                     A8Personaid = T000411_A8Personaid[0];
                     pr_datastore1.close(9);
                     dsDataStore1.SmartCacheProvider.SetUpdated("PERSONA") ;
                     if ( AnyError == 0 )
                     {
                        /* Start of After( Insert) rules */
                        /* End of After( Insert) rules */
                        if ( AnyError == 0 )
                        {
                           /* Save values for previous() function. */
                           GX_msglist.addItem(context.GetMessage( "GXM_sucadded", ""), "SuccessfullyAdded", 0, "", true);
                           ResetCaption040( ) ;
                        }
                     }
                  }
                  else
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                     AnyError = 1;
                  }
               }
            }
            else
            {
               Load044( ) ;
            }
            EndLevel044( ) ;
         }
         CloseExtendedTableCursors044( ) ;
      }

      protected void Update044( )
      {
         BeforeValidate044( ) ;
         if ( AnyError == 0 )
         {
            CheckExtendedTable044( ) ;
         }
         if ( AnyError == 0 )
         {
            CheckOptimisticConcurrency044( ) ;
            if ( AnyError == 0 )
            {
               AfterConfirm044( ) ;
               if ( AnyError == 0 )
               {
                  BeforeUpdate044( ) ;
                  if ( AnyError == 0 )
                  {
                     /* Using cursor T000412 */
                     pr_datastore1.execute(10, new Object[] {A9identificacion, A54nombre1, n55nombre2, A55nombre2, A56PersonaapellidoPaterno, n57PersonaapellidoMaterno, A57PersonaapellidoMaterno, A58nacimiento, A59genero, n60Personacorreo, A60Personacorreo, A15Paisid, A8Personaid});
                     pr_datastore1.close(10);
                     dsDataStore1.SmartCacheProvider.SetUpdated("PERSONA") ;
                     if ( (pr_datastore1.getStatus(10) == 103) )
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_lock", new   object[]  {"PERSONA"}), "RecordIsLocked", 1, "");
                        AnyError = 1;
                     }
                     DeferredUpdate044( ) ;
                     if ( AnyError == 0 )
                     {
                        /* Start of After( update) rules */
                        /* End of After( update) rules */
                        if ( AnyError == 0 )
                        {
                           if ( ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 ) || ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 ) )
                           {
                              if ( AnyError == 0 )
                              {
                                 context.nUserReturn = 1;
                              }
                           }
                        }
                     }
                     else
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                        AnyError = 1;
                     }
                  }
               }
            }
            EndLevel044( ) ;
         }
         CloseExtendedTableCursors044( ) ;
      }

      protected void DeferredUpdate044( )
      {
      }

      protected void delete( )
      {
         BeforeValidate044( ) ;
         if ( AnyError == 0 )
         {
            CheckOptimisticConcurrency044( ) ;
         }
         if ( AnyError == 0 )
         {
            OnDeleteControls044( ) ;
            AfterConfirm044( ) ;
            if ( AnyError == 0 )
            {
               BeforeDelete044( ) ;
               if ( AnyError == 0 )
               {
                  /* No cascading delete specified. */
                  /* Using cursor T000413 */
                  pr_datastore1.execute(11, new Object[] {A8Personaid});
                  pr_datastore1.close(11);
                  dsDataStore1.SmartCacheProvider.SetUpdated("PERSONA") ;
                  if ( AnyError == 0 )
                  {
                     /* Start of After( delete) rules */
                     /* End of After( delete) rules */
                     if ( AnyError == 0 )
                     {
                        if ( ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 ) || ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 ) )
                        {
                           if ( AnyError == 0 )
                           {
                              context.nUserReturn = 1;
                           }
                        }
                     }
                  }
                  else
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                     AnyError = 1;
                  }
               }
            }
         }
         sMode4 = Gx_mode;
         Gx_mode = "DLT";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!")), context));
         EndLevel044( ) ;
         Gx_mode = sMode4;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!")), context));
      }

      protected void OnDeleteControls044( )
      {
         standaloneModal( ) ;
         if ( AnyError == 0 )
         {
            /* Delete mode formulas */
            AV13Pgmname = "Persona";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13Pgmname", AV13Pgmname);
            /* Using cursor T000414 */
            pr_datastore1.execute(12, new Object[] {A15Paisid});
            A118Paisnombre = T000414_A118Paisnombre[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A118Paisnombre", A118Paisnombre);
            pr_datastore1.close(12);
         }
         if ( AnyError == 0 )
         {
            /* Using cursor T000415 */
            pr_datastore1.execute(13, new Object[] {A8Personaid});
            if ( (pr_datastore1.getStatus(13) != 101) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_del", new   object[]  {"Responsable"}), "CannotDeleteReferencedRecord", 1, "");
               AnyError = 1;
            }
            pr_datastore1.close(13);
            /* Using cursor T000416 */
            pr_datastore1.execute(14, new Object[] {A8Personaid});
            if ( (pr_datastore1.getStatus(14) != 101) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_del", new   object[]  {"Beneficiario"}), "CannotDeleteReferencedRecord", 1, "");
               AnyError = 1;
            }
            pr_datastore1.close(14);
         }
      }

      protected void EndLevel044( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
         {
            pr_datastore1.close(0);
         }
         if ( AnyError == 0 )
         {
            BeforeComplete044( ) ;
         }
         if ( AnyError == 0 )
         {
            pr_datastore1.close(1);
            pr_datastore1.close(12);
            context.CommitDataStores("persona",pr_default);
            if ( AnyError == 0 )
            {
               ConfirmValues040( ) ;
            }
            /* After transaction rules */
            /* Execute 'After Trn' event if defined. */
            trnEnded = 1;
         }
         else
         {
            pr_datastore1.close(1);
            pr_datastore1.close(12);
            context.RollbackDataStores("persona",pr_default);
         }
         IsModified = 0;
         if ( AnyError != 0 )
         {
            context.wjLoc = "";
            context.nUserReturn = 0;
         }
      }

      public void ScanStart044( )
      {
         /* Scan By routine */
         /* Using cursor T000417 */
         pr_datastore1.execute(15);
         RcdFound4 = 0;
         if ( (pr_datastore1.getStatus(15) != 101) )
         {
            RcdFound4 = 1;
            A8Personaid = T000417_A8Personaid[0];
         }
         /* Load Subordinate Levels */
      }

      protected void ScanNext044( )
      {
         /* Scan next routine */
         pr_datastore1.readNext(15);
         RcdFound4 = 0;
         if ( (pr_datastore1.getStatus(15) != 101) )
         {
            RcdFound4 = 1;
            A8Personaid = T000417_A8Personaid[0];
         }
      }

      protected void ScanEnd044( )
      {
         pr_datastore1.close(15);
      }

      protected void AfterConfirm044( )
      {
         /* After Confirm Rules */
      }

      protected void BeforeInsert044( )
      {
         /* Before Insert Rules */
      }

      protected void BeforeUpdate044( )
      {
         /* Before Update Rules */
      }

      protected void BeforeDelete044( )
      {
         /* Before Delete Rules */
      }

      protected void BeforeComplete044( )
      {
         /* Before Complete Rules */
      }

      protected void BeforeValidate044( )
      {
         /* Before Validate Rules */
      }

      protected void DisableAttributes044( )
      {
         edtidentificacion_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtidentificacion_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtidentificacion_Enabled), 5, 0)), true);
         edtnombre1_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtnombre1_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtnombre1_Enabled), 5, 0)), true);
         edtnombre2_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtnombre2_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtnombre2_Enabled), 5, 0)), true);
         edtPersonaapellidoPaterno_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtPersonaapellidoPaterno_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtPersonaapellidoPaterno_Enabled), 5, 0)), true);
         edtPersonaapellidoMaterno_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtPersonaapellidoMaterno_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtPersonaapellidoMaterno_Enabled), 5, 0)), true);
         edtnacimiento_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtnacimiento_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtnacimiento_Enabled), 5, 0)), true);
         edtgenero_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtgenero_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtgenero_Enabled), 5, 0)), true);
         edtPersonacorreo_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtPersonacorreo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtPersonacorreo_Enabled), 5, 0)), true);
         edtPaisid_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtPaisid_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtPaisid_Enabled), 5, 0)), true);
         edtPaisnombre_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtPaisnombre_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtPaisnombre_Enabled), 5, 0)), true);
      }

      protected void send_integrity_lvl_hashes044( )
      {
      }

      protected void assign_properties_default( )
      {
      }

      protected void ConfirmValues040( )
      {
      }

      public override void RenderHtmlHeaders( )
      {
         GxWebStd.gx_html_headers( context, 0, "", "", Form.Meta, Form.Metaequiv, true);
      }

      public override void RenderHtmlOpenForm( )
      {
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.WriteHtmlText( "<title>") ;
         context.SendWebValue( Form.Caption) ;
         context.WriteHtmlTextNl( "</title>") ;
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         if ( StringUtil.Len( sDynURL) > 0 )
         {
            context.WriteHtmlText( "<BASE href=\""+sDynURL+"\" />") ;
         }
         define_styles( ) ;
         MasterPageObj.master_styles();
         if ( ( ( context.GetBrowserType( ) == 1 ) || ( context.GetBrowserType( ) == 5 ) ) && ( StringUtil.StrCmp(context.GetBrowserVersion( ), "7.0") == 0 ) )
         {
            context.AddJavascriptSource("json2.js", "?"+context.GetBuildNumber( 127771), false);
         }
         context.AddJavascriptSource("jquery.js", "?"+context.GetBuildNumber( 127771), false);
         context.AddJavascriptSource("gxtimezone.js", "?"+context.GetBuildNumber( 127771), false);
         context.AddJavascriptSource("gxgral.js", "?"+context.GetBuildNumber( 127771), false);
         context.AddJavascriptSource("gxcfg.js", "?201914192945", false);
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.AddJavascriptSource("calendar.js", "?"+context.GetBuildNumber( 127771), false);
         context.AddJavascriptSource("calendar-setup.js", "?"+context.GetBuildNumber( 127771), false);
         context.AddJavascriptSource("calendar-en.js", "?"+context.GetBuildNumber( 127771), false);
         context.WriteHtmlText( Form.Headerrawhtml) ;
         context.CloseHtmlHeader();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         FormProcess = " data-HasEnter=\"true\" data-Skiponenter=\"false\"";
         context.WriteHtmlText( "<body ") ;
         bodyStyle = "" + "background-color:" + context.BuildHTMLColor( Form.Backcolor) + ";color:" + context.BuildHTMLColor( Form.Textcolor) + ";";
         bodyStyle = bodyStyle + "-moz-opacity:0;opacity:0;";
         if ( ! ( String.IsNullOrEmpty(StringUtil.RTrim( Form.Background)) ) )
         {
            bodyStyle = bodyStyle + " background-image:url(" + context.convertURL( Form.Background) + ")";
         }
         context.WriteHtmlText( " "+"class=\"form-horizontal Form\""+" "+ "style='"+bodyStyle+"'") ;
         context.WriteHtmlText( FormProcess+">") ;
         context.skipLines(1);
         context.WriteHtmlTextNl( "<form id=\"MAINFORM\" autocomplete=\"off\" name=\"MAINFORM\" method=\"post\" tabindex=-1  class=\"form-horizontal Form\" data-gx-class=\"form-horizontal Form\" novalidate action=\""+formatLink("persona.aspx") + "?" + UrlEncode(StringUtil.RTrim(Gx_mode)) + "," + UrlEncode("" +AV7Personaid)+"\">") ;
         GxWebStd.gx_hidden_field( context, "_EventName", "");
         GxWebStd.gx_hidden_field( context, "_EventGridId", "");
         GxWebStd.gx_hidden_field( context, "_EventRowId", "");
         context.WriteHtmlText( "<input type=\"submit\" title=\"submit\" style=\"display:none\" disabled>") ;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, "FORM", "Class", "form-horizontal Form", true);
         toggleJsOutput = isJsOutputEnabled( );
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
      }

      protected void send_integrity_footer_hashes( )
      {
         GXKey = Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
         forbiddenHiddens = "hsh" + "Persona";
         forbiddenHiddens = forbiddenHiddens + context.localUtil.Format( (decimal)(AV11Insert_Paisid), "ZZZZZZZZ9");
         forbiddenHiddens = forbiddenHiddens + StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"));
         forbiddenHiddens = forbiddenHiddens + context.localUtil.Format( (decimal)(A8Personaid), "ZZZZZZZZ9");
         GxWebStd.gx_hidden_field( context, "hsh", GetEncryptedHash( forbiddenHiddens, GXKey));
         GXUtil.WriteLog("persona:[SendSecurityCheck value for]"+"Insert_Paisid:"+context.localUtil.Format( (decimal)(AV11Insert_Paisid), "ZZZZZZZZ9"));
         GXUtil.WriteLog("persona:[SendSecurityCheck value for]"+"Gx_mode:"+StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!")));
         GXUtil.WriteLog("persona:[SendSecurityCheck value for]"+"Personaid:"+context.localUtil.Format( (decimal)(A8Personaid), "ZZZZZZZZ9"));
      }

      protected void SendCloseFormHiddens( )
      {
         /* Send hidden variables. */
         /* Send saved values. */
         send_integrity_footer_hashes( ) ;
         GxWebStd.gx_hidden_field( context, "Z8Personaid", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z8Personaid), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "Z9identificacion", Z9identificacion);
         GxWebStd.gx_hidden_field( context, "Z54nombre1", Z54nombre1);
         GxWebStd.gx_hidden_field( context, "Z55nombre2", Z55nombre2);
         GxWebStd.gx_hidden_field( context, "Z56PersonaapellidoPaterno", Z56PersonaapellidoPaterno);
         GxWebStd.gx_hidden_field( context, "Z57PersonaapellidoMaterno", Z57PersonaapellidoMaterno);
         GxWebStd.gx_hidden_field( context, "Z58nacimiento", context.localUtil.DToC( Z58nacimiento, 0, "/"));
         GxWebStd.gx_hidden_field( context, "Z59genero", Z59genero);
         GxWebStd.gx_hidden_field( context, "Z60Personacorreo", Z60Personacorreo);
         GxWebStd.gx_hidden_field( context, "Z15Paisid", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z15Paisid), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "IsConfirmed", StringUtil.LTrim( StringUtil.NToC( (decimal)(IsConfirmed), 4, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "IsModified", StringUtil.LTrim( StringUtil.NToC( (decimal)(IsModified), 4, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "Mode", StringUtil.RTrim( Gx_mode));
         GxWebStd.gx_hidden_field( context, "gxhash_Mode", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!")), context));
         GxWebStd.gx_hidden_field( context, "N15Paisid", StringUtil.LTrim( StringUtil.NToC( (decimal)(A15Paisid), 9, 0, ".", "")));
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vTRNCONTEXT", AV9TrnContext);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vTRNCONTEXT", AV9TrnContext);
         }
         GxWebStd.gx_hidden_field( context, "vPERSONAID", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV7Personaid), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "gxhash_vPERSONAID", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV7Personaid), "ZZZZZZZZ9"), context));
         GxWebStd.gx_hidden_field( context, "PERSONAID", StringUtil.LTrim( StringUtil.NToC( (decimal)(A8Personaid), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "vINSERT_PAISID", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV11Insert_Paisid), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "vPGMNAME", StringUtil.RTrim( AV13Pgmname));
         GxWebStd.gx_hidden_field( context, "vMODE", StringUtil.RTrim( Gx_mode));
         GxWebStd.gx_hidden_field( context, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!")), context));
      }

      public override void RenderHtmlCloseForm( )
      {
         SendCloseFormHiddens( ) ;
         GxWebStd.gx_hidden_field( context, "GX_FocusControl", GX_FocusControl);
         SendAjaxEncryptionKey();
         SendSecurityToken(sPrefix);
         SendComponentObjects();
         SendServerCommands();
         SendState();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         context.WriteHtmlTextNl( "</form>") ;
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         include_jscripts( ) ;
      }

      public override short ExecuteStartEvent( )
      {
         standaloneStartup( ) ;
         gxajaxcallmode = (short)((isAjaxCallMode( ) ? 1 : 0));
         return gxajaxcallmode ;
      }

      public override void RenderHtmlContent( )
      {
         context.WriteHtmlText( "<div") ;
         GxWebStd.ClassAttribute( context, "gx-ct-body"+" "+(String.IsNullOrEmpty(StringUtil.RTrim( Form.Class)) ? "form-horizontal Form" : Form.Class)+"-fx");
         context.WriteHtmlText( ">") ;
         Draw( ) ;
         context.WriteHtmlText( "</div>") ;
      }

      public override void DispatchEvents( )
      {
         Process( ) ;
      }

      public override bool HasEnterEvent( )
      {
         return true ;
      }

      public override GXWebForm GetForm( )
      {
         return Form ;
      }

      public override String GetSelfLink( )
      {
         return formatLink("persona.aspx") + "?" + UrlEncode(StringUtil.RTrim(Gx_mode)) + "," + UrlEncode("" +AV7Personaid) ;
      }

      public override String GetPgmname( )
      {
         return "Persona" ;
      }

      public override String GetPgmdesc( )
      {
         return "Persona" ;
      }

      protected void InitializeNonKey044( )
      {
         A15Paisid = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A15Paisid", StringUtil.LTrim( StringUtil.Str( (decimal)(A15Paisid), 9, 0)));
         A9identificacion = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A9identificacion", A9identificacion);
         A54nombre1 = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A54nombre1", A54nombre1);
         A55nombre2 = "";
         n55nombre2 = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A55nombre2", A55nombre2);
         n55nombre2 = (String.IsNullOrEmpty(StringUtil.RTrim( A55nombre2)) ? true : false);
         A56PersonaapellidoPaterno = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A56PersonaapellidoPaterno", A56PersonaapellidoPaterno);
         A57PersonaapellidoMaterno = "";
         n57PersonaapellidoMaterno = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A57PersonaapellidoMaterno", A57PersonaapellidoMaterno);
         n57PersonaapellidoMaterno = (String.IsNullOrEmpty(StringUtil.RTrim( A57PersonaapellidoMaterno)) ? true : false);
         A58nacimiento = DateTime.MinValue;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A58nacimiento", context.localUtil.Format(A58nacimiento, "99/99/99"));
         A59genero = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A59genero", A59genero);
         A60Personacorreo = "";
         n60Personacorreo = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A60Personacorreo", A60Personacorreo);
         n60Personacorreo = (String.IsNullOrEmpty(StringUtil.RTrim( A60Personacorreo)) ? true : false);
         A118Paisnombre = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A118Paisnombre", A118Paisnombre);
         Z9identificacion = "";
         Z54nombre1 = "";
         Z55nombre2 = "";
         Z56PersonaapellidoPaterno = "";
         Z57PersonaapellidoMaterno = "";
         Z58nacimiento = DateTime.MinValue;
         Z59genero = "";
         Z60Personacorreo = "";
         Z15Paisid = 0;
      }

      protected void InitAll044( )
      {
         A8Personaid = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A8Personaid", StringUtil.LTrim( StringUtil.Str( (decimal)(A8Personaid), 9, 0)));
         InitializeNonKey044( ) ;
      }

      protected void StandaloneModalInsert( )
      {
      }

      protected void define_styles( )
      {
         AddStyleSheetFile("calendar-system.css", "");
         AddThemeStyleSheetFile("", context.GetTheme( )+".css", "?"+GetCacheInvalidationToken( ));
         bool outputEnabled = isOutputEnabled( ) ;
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         idxLst = 1;
         while ( idxLst <= Form.Jscriptsrc.Count )
         {
            context.AddJavascriptSource(StringUtil.RTrim( ((String)Form.Jscriptsrc.Item(idxLst))), "?201914192963", true);
            idxLst = (int)(idxLst+1);
         }
         if ( ! outputEnabled )
         {
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
         }
         /* End function define_styles */
      }

      protected void include_jscripts( )
      {
         context.AddJavascriptSource("messages.eng.js", "?"+GetCacheInvalidationToken( ), false);
         context.AddJavascriptSource("persona.js", "?201914192963", false);
         /* End function include_jscripts */
      }

      protected void init_default_properties( )
      {
         lblTitle_Internalname = "TITLE";
         divTitlecontainer_Internalname = "TITLECONTAINER";
         bttBtn_first_Internalname = "BTN_FIRST";
         bttBtn_previous_Internalname = "BTN_PREVIOUS";
         bttBtn_next_Internalname = "BTN_NEXT";
         bttBtn_last_Internalname = "BTN_LAST";
         bttBtn_select_Internalname = "BTN_SELECT";
         divToolbarcell_Internalname = "TOOLBARCELL";
         edtidentificacion_Internalname = "IDENTIFICACION";
         edtnombre1_Internalname = "NOMBRE1";
         edtnombre2_Internalname = "NOMBRE2";
         edtPersonaapellidoPaterno_Internalname = "PERSONAAPELLIDOPATERNO";
         edtPersonaapellidoMaterno_Internalname = "PERSONAAPELLIDOMATERNO";
         edtnacimiento_Internalname = "NACIMIENTO";
         edtgenero_Internalname = "GENERO";
         edtPersonacorreo_Internalname = "PERSONACORREO";
         edtPaisid_Internalname = "PAISID";
         edtPaisnombre_Internalname = "PAISNOMBRE";
         divFormcontainer_Internalname = "FORMCONTAINER";
         bttBtn_enter_Internalname = "BTN_ENTER";
         bttBtn_cancel_Internalname = "BTN_CANCEL";
         bttBtn_delete_Internalname = "BTN_DELETE";
         divMaintable_Internalname = "MAINTABLE";
         Form.Internalname = "FORM";
         imgprompt_15_Internalname = "PROMPT_15";
      }

      public override void initialize_properties( )
      {
         context.SetDefaultTheme("Carmine");
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         init_default_properties( ) ;
         Form.Headerrawhtml = "";
         Form.Background = "";
         Form.Textcolor = 0;
         Form.Backcolor = (int)(0xFFFFFF);
         Form.Caption = "Persona";
         bttBtn_delete_Enabled = 0;
         bttBtn_delete_Visible = 1;
         bttBtn_cancel_Visible = 1;
         bttBtn_enter_Enabled = 1;
         bttBtn_enter_Visible = 1;
         edtPaisnombre_Jsonclick = "";
         edtPaisnombre_Enabled = 0;
         imgprompt_15_Visible = 1;
         imgprompt_15_Link = "";
         edtPaisid_Jsonclick = "";
         edtPaisid_Enabled = 1;
         edtPersonacorreo_Jsonclick = "";
         edtPersonacorreo_Enabled = 1;
         edtgenero_Jsonclick = "";
         edtgenero_Enabled = 1;
         edtnacimiento_Jsonclick = "";
         edtnacimiento_Enabled = 1;
         edtPersonaapellidoMaterno_Jsonclick = "";
         edtPersonaapellidoMaterno_Enabled = 1;
         edtPersonaapellidoPaterno_Jsonclick = "";
         edtPersonaapellidoPaterno_Enabled = 1;
         edtnombre2_Jsonclick = "";
         edtnombre2_Enabled = 1;
         edtnombre1_Jsonclick = "";
         edtnombre1_Enabled = 1;
         edtidentificacion_Jsonclick = "";
         edtidentificacion_Enabled = 1;
         bttBtn_select_Visible = 1;
         bttBtn_last_Visible = 1;
         bttBtn_next_Visible = 1;
         bttBtn_previous_Visible = 1;
         bttBtn_first_Visible = 1;
         context.GX_msglist.DisplayMode = 1;
         if ( context.isSpaRequest( ) )
         {
            enableJsOutput();
         }
      }

      protected void dynload_actions( )
      {
         /* End function dynload_actions */
      }

      protected void init_web_controls( )
      {
         /* End function init_web_controls */
      }

      public void Valid_Identificacion( String GX_Parm1 ,
                                        int GX_Parm2 )
      {
         A9identificacion = GX_Parm1;
         A8Personaid = GX_Parm2;
         /* Using cursor T000418 */
         pr_datastore1.execute(16, new Object[] {A9identificacion, A8Personaid});
         if ( (pr_datastore1.getStatus(16) != 101) )
         {
            GX_msglist.addItem(context.GetMessage( "GXM_1004", new   object[]  {"identificacion"}), 1, "IDENTIFICACION");
            AnyError = 1;
            GX_FocusControl = edtidentificacion_Internalname;
         }
         pr_datastore1.close(16);
         dynload_actions( ) ;
         isValidOutput.Add(context.GX_msglist.ToJavascriptSource());
         isValidOutput.Add(context.httpAjaxContext.ajax_rsp_get_hiddens( ));
         context.GX_webresponse.AddString(isValidOutput.ToJSonString());
         wbTemp = context.ResponseContentType( "application/json");
      }

      public void Valid_Paisid( int GX_Parm1 ,
                                String GX_Parm2 )
      {
         A15Paisid = GX_Parm1;
         A118Paisnombre = GX_Parm2;
         /* Using cursor T000414 */
         pr_datastore1.execute(12, new Object[] {A15Paisid});
         if ( (pr_datastore1.getStatus(12) == 101) )
         {
            GX_msglist.addItem("No matching 'Pais'.", "ForeignKeyNotFound", 1, "PAISID");
            AnyError = 1;
            GX_FocusControl = edtPaisid_Internalname;
         }
         A118Paisnombre = T000414_A118Paisnombre[0];
         pr_datastore1.close(12);
         dynload_actions( ) ;
         if ( AnyError == 1 )
         {
            A118Paisnombre = "";
         }
         isValidOutput.Add(A118Paisnombre);
         isValidOutput.Add(context.GX_msglist.ToJavascriptSource());
         isValidOutput.Add(context.httpAjaxContext.ajax_rsp_get_hiddens( ));
         context.GX_webresponse.AddString(isValidOutput.ToJSonString());
         wbTemp = context.ResponseContentType( "application/json");
      }

      public override bool SupportAjaxEvent( )
      {
         return true ;
      }

      public override void InitializeDynEvents( )
      {
         setEventMetadata("ENTER","{handler:'UserMainFullajax',iparms:[{postForm:true},{av:'Gx_mode',fld:'vMODE',pic:'@!',hsh:true},{av:'AV7Personaid',fld:'vPERSONAID',pic:'ZZZZZZZZ9',hsh:true}]");
         setEventMetadata("ENTER",",oparms:[]}");
         setEventMetadata("REFRESH","{handler:'Refresh',iparms:[{av:'Gx_mode',fld:'vMODE',pic:'@!',hsh:true},{av:'AV7Personaid',fld:'vPERSONAID',pic:'ZZZZZZZZ9',hsh:true},{av:'AV11Insert_Paisid',fld:'vINSERT_PAISID',pic:'ZZZZZZZZ9'},{av:'A8Personaid',fld:'PERSONAID',pic:'ZZZZZZZZ9'}]");
         setEventMetadata("REFRESH",",oparms:[]}");
         setEventMetadata("AFTER TRN","{handler:'E12042',iparms:[{av:'Gx_mode',fld:'vMODE',pic:'@!',hsh:true},{av:'AV9TrnContext',fld:'vTRNCONTEXT',pic:''}]");
         setEventMetadata("AFTER TRN",",oparms:[]}");
         return  ;
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
         pr_datastore1.close(1);
         pr_datastore1.close(12);
      }

      public override void initialize( )
      {
         sPrefix = "";
         wcpOGx_mode = "";
         Z9identificacion = "";
         Z54nombre1 = "";
         Z55nombre2 = "";
         Z56PersonaapellidoPaterno = "";
         Z57PersonaapellidoMaterno = "";
         Z58nacimiento = DateTime.MinValue;
         Z59genero = "";
         Z60Personacorreo = "";
         scmdbuf = "";
         gxfirstwebparm = "";
         gxfirstwebparm_bkp = "";
         GXKey = "";
         PreviousTooltip = "";
         PreviousCaption = "";
         Form = new GXWebForm();
         GX_FocusControl = "";
         lblTitle_Jsonclick = "";
         ClassString = "";
         StyleString = "";
         TempTags = "";
         bttBtn_first_Jsonclick = "";
         bttBtn_previous_Jsonclick = "";
         bttBtn_next_Jsonclick = "";
         bttBtn_last_Jsonclick = "";
         bttBtn_select_Jsonclick = "";
         A9identificacion = "";
         A54nombre1 = "";
         A55nombre2 = "";
         A56PersonaapellidoPaterno = "";
         A57PersonaapellidoMaterno = "";
         A58nacimiento = DateTime.MinValue;
         A59genero = "";
         A60Personacorreo = "";
         sImgUrl = "";
         A118Paisnombre = "";
         bttBtn_enter_Jsonclick = "";
         bttBtn_cancel_Jsonclick = "";
         bttBtn_delete_Jsonclick = "";
         AV13Pgmname = "";
         forbiddenHiddens = "";
         hsh = "";
         sMode4 = "";
         sEvt = "";
         EvtGridId = "";
         EvtRowId = "";
         sEvtType = "";
         AV9TrnContext = new SdtTransactionContext(context);
         AV10WebSession = context.GetSession();
         AV12TrnContextAtt = new SdtTransactionContext_Attribute(context);
         Z118Paisnombre = "";
         T00044_A118Paisnombre = new String[] {""} ;
         T00045_A8Personaid = new int[1] ;
         T00045_A9identificacion = new String[] {""} ;
         T00045_A54nombre1 = new String[] {""} ;
         T00045_A55nombre2 = new String[] {""} ;
         T00045_n55nombre2 = new bool[] {false} ;
         T00045_A56PersonaapellidoPaterno = new String[] {""} ;
         T00045_A57PersonaapellidoMaterno = new String[] {""} ;
         T00045_n57PersonaapellidoMaterno = new bool[] {false} ;
         T00045_A58nacimiento = new DateTime[] {DateTime.MinValue} ;
         T00045_A59genero = new String[] {""} ;
         T00045_A60Personacorreo = new String[] {""} ;
         T00045_n60Personacorreo = new bool[] {false} ;
         T00045_A118Paisnombre = new String[] {""} ;
         T00045_A15Paisid = new int[1] ;
         T00046_A9identificacion = new String[] {""} ;
         T00047_A118Paisnombre = new String[] {""} ;
         T00048_A8Personaid = new int[1] ;
         T00043_A8Personaid = new int[1] ;
         T00043_A9identificacion = new String[] {""} ;
         T00043_A54nombre1 = new String[] {""} ;
         T00043_A55nombre2 = new String[] {""} ;
         T00043_n55nombre2 = new bool[] {false} ;
         T00043_A56PersonaapellidoPaterno = new String[] {""} ;
         T00043_A57PersonaapellidoMaterno = new String[] {""} ;
         T00043_n57PersonaapellidoMaterno = new bool[] {false} ;
         T00043_A58nacimiento = new DateTime[] {DateTime.MinValue} ;
         T00043_A59genero = new String[] {""} ;
         T00043_A60Personacorreo = new String[] {""} ;
         T00043_n60Personacorreo = new bool[] {false} ;
         T00043_A15Paisid = new int[1] ;
         T00049_A8Personaid = new int[1] ;
         T000410_A8Personaid = new int[1] ;
         T00042_A8Personaid = new int[1] ;
         T00042_A9identificacion = new String[] {""} ;
         T00042_A54nombre1 = new String[] {""} ;
         T00042_A55nombre2 = new String[] {""} ;
         T00042_n55nombre2 = new bool[] {false} ;
         T00042_A56PersonaapellidoPaterno = new String[] {""} ;
         T00042_A57PersonaapellidoMaterno = new String[] {""} ;
         T00042_n57PersonaapellidoMaterno = new bool[] {false} ;
         T00042_A58nacimiento = new DateTime[] {DateTime.MinValue} ;
         T00042_A59genero = new String[] {""} ;
         T00042_A60Personacorreo = new String[] {""} ;
         T00042_n60Personacorreo = new bool[] {false} ;
         T00042_A15Paisid = new int[1] ;
         T000411_A8Personaid = new int[1] ;
         T000414_A118Paisnombre = new String[] {""} ;
         T000415_A5Responsableid = new int[1] ;
         T000416_A6Beneficiarioid = new int[1] ;
         T000417_A8Personaid = new int[1] ;
         sDynURL = "";
         FormProcess = "";
         bodyStyle = "";
         T000418_A9identificacion = new String[] {""} ;
         isValidOutput = new GxUnknownObjectCollection();
         pr_datastore1 = new DataStoreProvider(context, new GeneXus.Programs.persona__datastore1(),
            new Object[][] {
                new Object[] {
               T00042_A8Personaid, T00042_A9identificacion, T00042_A54nombre1, T00042_A55nombre2, T00042_n55nombre2, T00042_A56PersonaapellidoPaterno, T00042_A57PersonaapellidoMaterno, T00042_n57PersonaapellidoMaterno, T00042_A58nacimiento, T00042_A59genero,
               T00042_A60Personacorreo, T00042_n60Personacorreo, T00042_A15Paisid
               }
               , new Object[] {
               T00043_A8Personaid, T00043_A9identificacion, T00043_A54nombre1, T00043_A55nombre2, T00043_n55nombre2, T00043_A56PersonaapellidoPaterno, T00043_A57PersonaapellidoMaterno, T00043_n57PersonaapellidoMaterno, T00043_A58nacimiento, T00043_A59genero,
               T00043_A60Personacorreo, T00043_n60Personacorreo, T00043_A15Paisid
               }
               , new Object[] {
               T00044_A118Paisnombre
               }
               , new Object[] {
               T00045_A8Personaid, T00045_A9identificacion, T00045_A54nombre1, T00045_A55nombre2, T00045_n55nombre2, T00045_A56PersonaapellidoPaterno, T00045_A57PersonaapellidoMaterno, T00045_n57PersonaapellidoMaterno, T00045_A58nacimiento, T00045_A59genero,
               T00045_A60Personacorreo, T00045_n60Personacorreo, T00045_A118Paisnombre, T00045_A15Paisid
               }
               , new Object[] {
               T00046_A9identificacion
               }
               , new Object[] {
               T00047_A118Paisnombre
               }
               , new Object[] {
               T00048_A8Personaid
               }
               , new Object[] {
               T00049_A8Personaid
               }
               , new Object[] {
               T000410_A8Personaid
               }
               , new Object[] {
               T000411_A8Personaid
               }
               , new Object[] {
               }
               , new Object[] {
               }
               , new Object[] {
               T000414_A118Paisnombre
               }
               , new Object[] {
               T000415_A5Responsableid
               }
               , new Object[] {
               T000416_A6Beneficiarioid
               }
               , new Object[] {
               T000417_A8Personaid
               }
               , new Object[] {
               T000418_A9identificacion
               }
            }
         );
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.persona__default(),
            new Object[][] {
            }
         );
         AV13Pgmname = "Persona";
      }

      private short GxWebError ;
      private short gxcookieaux ;
      private short IsConfirmed ;
      private short IsModified ;
      private short AnyError ;
      private short nKeyPressed ;
      private short initialized ;
      private short RcdFound4 ;
      private short GX_JID ;
      private short Gx_BScreen ;
      private short gxajaxcallmode ;
      private short wbTemp ;
      private int wcpOAV7Personaid ;
      private int Z8Personaid ;
      private int Z15Paisid ;
      private int N15Paisid ;
      private int A15Paisid ;
      private int AV7Personaid ;
      private int trnEnded ;
      private int bttBtn_first_Visible ;
      private int bttBtn_previous_Visible ;
      private int bttBtn_next_Visible ;
      private int bttBtn_last_Visible ;
      private int bttBtn_select_Visible ;
      private int edtidentificacion_Enabled ;
      private int edtnombre1_Enabled ;
      private int edtnombre2_Enabled ;
      private int edtPersonaapellidoPaterno_Enabled ;
      private int edtPersonaapellidoMaterno_Enabled ;
      private int edtnacimiento_Enabled ;
      private int edtgenero_Enabled ;
      private int edtPersonacorreo_Enabled ;
      private int edtPaisid_Enabled ;
      private int imgprompt_15_Visible ;
      private int edtPaisnombre_Enabled ;
      private int bttBtn_enter_Visible ;
      private int bttBtn_enter_Enabled ;
      private int bttBtn_cancel_Visible ;
      private int bttBtn_delete_Visible ;
      private int bttBtn_delete_Enabled ;
      private int A8Personaid ;
      private int AV11Insert_Paisid ;
      private int AV14GXV1 ;
      private int idxLst ;
      private String sPrefix ;
      private String wcpOGx_mode ;
      private String scmdbuf ;
      private String gxfirstwebparm ;
      private String gxfirstwebparm_bkp ;
      private String Gx_mode ;
      private String GXKey ;
      private String PreviousTooltip ;
      private String PreviousCaption ;
      private String GX_FocusControl ;
      private String edtidentificacion_Internalname ;
      private String divMaintable_Internalname ;
      private String divTitlecontainer_Internalname ;
      private String lblTitle_Internalname ;
      private String lblTitle_Jsonclick ;
      private String ClassString ;
      private String StyleString ;
      private String divFormcontainer_Internalname ;
      private String divToolbarcell_Internalname ;
      private String TempTags ;
      private String bttBtn_first_Internalname ;
      private String bttBtn_first_Jsonclick ;
      private String bttBtn_previous_Internalname ;
      private String bttBtn_previous_Jsonclick ;
      private String bttBtn_next_Internalname ;
      private String bttBtn_next_Jsonclick ;
      private String bttBtn_last_Internalname ;
      private String bttBtn_last_Jsonclick ;
      private String bttBtn_select_Internalname ;
      private String bttBtn_select_Jsonclick ;
      private String edtidentificacion_Jsonclick ;
      private String edtnombre1_Internalname ;
      private String edtnombre1_Jsonclick ;
      private String edtnombre2_Internalname ;
      private String edtnombre2_Jsonclick ;
      private String edtPersonaapellidoPaterno_Internalname ;
      private String edtPersonaapellidoPaterno_Jsonclick ;
      private String edtPersonaapellidoMaterno_Internalname ;
      private String edtPersonaapellidoMaterno_Jsonclick ;
      private String edtnacimiento_Internalname ;
      private String edtnacimiento_Jsonclick ;
      private String edtgenero_Internalname ;
      private String edtgenero_Jsonclick ;
      private String edtPersonacorreo_Internalname ;
      private String edtPersonacorreo_Jsonclick ;
      private String edtPaisid_Internalname ;
      private String edtPaisid_Jsonclick ;
      private String sImgUrl ;
      private String imgprompt_15_Internalname ;
      private String imgprompt_15_Link ;
      private String edtPaisnombre_Internalname ;
      private String edtPaisnombre_Jsonclick ;
      private String bttBtn_enter_Internalname ;
      private String bttBtn_enter_Jsonclick ;
      private String bttBtn_cancel_Internalname ;
      private String bttBtn_cancel_Jsonclick ;
      private String bttBtn_delete_Internalname ;
      private String bttBtn_delete_Jsonclick ;
      private String AV13Pgmname ;
      private String forbiddenHiddens ;
      private String hsh ;
      private String sMode4 ;
      private String sEvt ;
      private String EvtGridId ;
      private String EvtRowId ;
      private String sEvtType ;
      private String sDynURL ;
      private String FormProcess ;
      private String bodyStyle ;
      private DateTime Z58nacimiento ;
      private DateTime A58nacimiento ;
      private bool entryPointCalled ;
      private bool toggleJsOutput ;
      private bool wbErr ;
      private bool n55nombre2 ;
      private bool n57PersonaapellidoMaterno ;
      private bool n60Personacorreo ;
      private bool returnInSub ;
      private bool Gx_longc ;
      private String Z9identificacion ;
      private String Z54nombre1 ;
      private String Z55nombre2 ;
      private String Z56PersonaapellidoPaterno ;
      private String Z57PersonaapellidoMaterno ;
      private String Z59genero ;
      private String Z60Personacorreo ;
      private String A9identificacion ;
      private String A54nombre1 ;
      private String A55nombre2 ;
      private String A56PersonaapellidoPaterno ;
      private String A57PersonaapellidoMaterno ;
      private String A59genero ;
      private String A60Personacorreo ;
      private String A118Paisnombre ;
      private String Z118Paisnombre ;
      private IGxSession AV10WebSession ;
      private GxUnknownObjectCollection isValidOutput ;
      private IGxDataStore dsDataStore1 ;
      private IGxDataStore dsDefault ;
      private IDataStoreProvider pr_datastore1 ;
      private String[] T00044_A118Paisnombre ;
      private int[] T00045_A8Personaid ;
      private String[] T00045_A9identificacion ;
      private String[] T00045_A54nombre1 ;
      private String[] T00045_A55nombre2 ;
      private bool[] T00045_n55nombre2 ;
      private String[] T00045_A56PersonaapellidoPaterno ;
      private String[] T00045_A57PersonaapellidoMaterno ;
      private bool[] T00045_n57PersonaapellidoMaterno ;
      private DateTime[] T00045_A58nacimiento ;
      private String[] T00045_A59genero ;
      private String[] T00045_A60Personacorreo ;
      private bool[] T00045_n60Personacorreo ;
      private String[] T00045_A118Paisnombre ;
      private int[] T00045_A15Paisid ;
      private String[] T00046_A9identificacion ;
      private String[] T00047_A118Paisnombre ;
      private int[] T00048_A8Personaid ;
      private int[] T00043_A8Personaid ;
      private String[] T00043_A9identificacion ;
      private String[] T00043_A54nombre1 ;
      private String[] T00043_A55nombre2 ;
      private bool[] T00043_n55nombre2 ;
      private String[] T00043_A56PersonaapellidoPaterno ;
      private String[] T00043_A57PersonaapellidoMaterno ;
      private bool[] T00043_n57PersonaapellidoMaterno ;
      private DateTime[] T00043_A58nacimiento ;
      private String[] T00043_A59genero ;
      private String[] T00043_A60Personacorreo ;
      private bool[] T00043_n60Personacorreo ;
      private int[] T00043_A15Paisid ;
      private int[] T00049_A8Personaid ;
      private int[] T000410_A8Personaid ;
      private int[] T00042_A8Personaid ;
      private String[] T00042_A9identificacion ;
      private String[] T00042_A54nombre1 ;
      private String[] T00042_A55nombre2 ;
      private bool[] T00042_n55nombre2 ;
      private String[] T00042_A56PersonaapellidoPaterno ;
      private String[] T00042_A57PersonaapellidoMaterno ;
      private bool[] T00042_n57PersonaapellidoMaterno ;
      private DateTime[] T00042_A58nacimiento ;
      private String[] T00042_A59genero ;
      private String[] T00042_A60Personacorreo ;
      private bool[] T00042_n60Personacorreo ;
      private int[] T00042_A15Paisid ;
      private int[] T000411_A8Personaid ;
      private String[] T000414_A118Paisnombre ;
      private int[] T000415_A5Responsableid ;
      private int[] T000416_A6Beneficiarioid ;
      private IDataStoreProvider pr_default ;
      private int[] T000417_A8Personaid ;
      private String[] T000418_A9identificacion ;
      private GXWebForm Form ;
      private SdtTransactionContext AV9TrnContext ;
      private SdtTransactionContext_Attribute AV12TrnContextAtt ;
   }

   public class persona__datastore1 : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
         ,new ForEachCursor(def[2])
         ,new ForEachCursor(def[3])
         ,new ForEachCursor(def[4])
         ,new ForEachCursor(def[5])
         ,new ForEachCursor(def[6])
         ,new ForEachCursor(def[7])
         ,new ForEachCursor(def[8])
         ,new ForEachCursor(def[9])
         ,new UpdateCursor(def[10])
         ,new UpdateCursor(def[11])
         ,new ForEachCursor(def[12])
         ,new ForEachCursor(def[13])
         ,new ForEachCursor(def[14])
         ,new ForEachCursor(def[15])
         ,new ForEachCursor(def[16])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmT00045 ;
          prmT00045 = new Object[] {
          new Object[] {"@Personaid",SqlDbType.Int,9,0}
          } ;
          Object[] prmT00044 ;
          prmT00044 = new Object[] {
          new Object[] {"@Paisid",SqlDbType.Int,9,0}
          } ;
          Object[] prmT00046 ;
          prmT00046 = new Object[] {
          new Object[] {"@identificacion",SqlDbType.VarChar,50,0} ,
          new Object[] {"@Personaid",SqlDbType.Int,9,0}
          } ;
          Object[] prmT00047 ;
          prmT00047 = new Object[] {
          new Object[] {"@Paisid",SqlDbType.Int,9,0}
          } ;
          Object[] prmT00048 ;
          prmT00048 = new Object[] {
          new Object[] {"@Personaid",SqlDbType.Int,9,0}
          } ;
          Object[] prmT00043 ;
          prmT00043 = new Object[] {
          new Object[] {"@Personaid",SqlDbType.Int,9,0}
          } ;
          Object[] prmT00049 ;
          prmT00049 = new Object[] {
          new Object[] {"@Personaid",SqlDbType.Int,9,0}
          } ;
          Object[] prmT000410 ;
          prmT000410 = new Object[] {
          new Object[] {"@Personaid",SqlDbType.Int,9,0}
          } ;
          Object[] prmT00042 ;
          prmT00042 = new Object[] {
          new Object[] {"@Personaid",SqlDbType.Int,9,0}
          } ;
          Object[] prmT000411 ;
          prmT000411 = new Object[] {
          new Object[] {"@identificacion",SqlDbType.VarChar,50,0} ,
          new Object[] {"@nombre1",SqlDbType.VarChar,50,0} ,
          new Object[] {"@nombre2",SqlDbType.VarChar,50,0} ,
          new Object[] {"@PersonaapellidoPaterno",SqlDbType.VarChar,50,0} ,
          new Object[] {"@PersonaapellidoMaterno",SqlDbType.VarChar,50,0} ,
          new Object[] {"@nacimiento",SqlDbType.DateTime,8,0} ,
          new Object[] {"@genero",SqlDbType.VarChar,1,0} ,
          new Object[] {"@Personacorreo",SqlDbType.VarChar,100,0} ,
          new Object[] {"@Paisid",SqlDbType.Int,9,0}
          } ;
          Object[] prmT000412 ;
          prmT000412 = new Object[] {
          new Object[] {"@identificacion",SqlDbType.VarChar,50,0} ,
          new Object[] {"@nombre1",SqlDbType.VarChar,50,0} ,
          new Object[] {"@nombre2",SqlDbType.VarChar,50,0} ,
          new Object[] {"@PersonaapellidoPaterno",SqlDbType.VarChar,50,0} ,
          new Object[] {"@PersonaapellidoMaterno",SqlDbType.VarChar,50,0} ,
          new Object[] {"@nacimiento",SqlDbType.DateTime,8,0} ,
          new Object[] {"@genero",SqlDbType.VarChar,1,0} ,
          new Object[] {"@Personacorreo",SqlDbType.VarChar,100,0} ,
          new Object[] {"@Paisid",SqlDbType.Int,9,0} ,
          new Object[] {"@Personaid",SqlDbType.Int,9,0}
          } ;
          Object[] prmT000413 ;
          prmT000413 = new Object[] {
          new Object[] {"@Personaid",SqlDbType.Int,9,0}
          } ;
          Object[] prmT000415 ;
          prmT000415 = new Object[] {
          new Object[] {"@Personaid",SqlDbType.Int,9,0}
          } ;
          Object[] prmT000416 ;
          prmT000416 = new Object[] {
          new Object[] {"@Personaid",SqlDbType.Int,9,0}
          } ;
          Object[] prmT000417 ;
          prmT000417 = new Object[] {
          } ;
          Object[] prmT000418 ;
          prmT000418 = new Object[] {
          new Object[] {"@identificacion",SqlDbType.VarChar,50,0} ,
          new Object[] {"@Personaid",SqlDbType.Int,9,0}
          } ;
          Object[] prmT000414 ;
          prmT000414 = new Object[] {
          new Object[] {"@Paisid",SqlDbType.Int,9,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("T00042", "SELECT [id] AS Personaid, [identificacion], [nombre1], [nombre2], [apellidoPaterno], [apellidoMaterno], [nacimiento], [genero], [correo], [idPais] AS Paisid FROM dbo.[Persona] WITH (UPDLOCK) WHERE [id] = @Personaid ",true, GxErrorMask.GX_NOMASK, false, this,prmT00042,1,0,true,false )
             ,new CursorDef("T00043", "SELECT [id] AS Personaid, [identificacion], [nombre1], [nombre2], [apellidoPaterno], [apellidoMaterno], [nacimiento], [genero], [correo], [idPais] AS Paisid FROM dbo.[Persona] WITH (NOLOCK) WHERE [id] = @Personaid ",true, GxErrorMask.GX_NOMASK, false, this,prmT00043,1,0,true,false )
             ,new CursorDef("T00044", "SELECT [nombre] FROM dbo.[Pais] WITH (NOLOCK) WHERE [id] = @Paisid ",true, GxErrorMask.GX_NOMASK, false, this,prmT00044,1,0,true,false )
             ,new CursorDef("T00045", "SELECT TM1.[id] AS Personaid, TM1.[identificacion], TM1.[nombre1], TM1.[nombre2], TM1.[apellidoPaterno], TM1.[apellidoMaterno], TM1.[nacimiento], TM1.[genero], TM1.[correo], T2.[nombre], TM1.[idPais] AS Paisid FROM (dbo.[Persona] TM1 WITH (NOLOCK) INNER JOIN dbo.[Pais] T2 WITH (NOLOCK) ON T2.[id] = TM1.[idPais]) WHERE TM1.[id] = @Personaid ORDER BY TM1.[id]  OPTION (FAST 100)",true, GxErrorMask.GX_NOMASK, false, this,prmT00045,100,0,true,false )
             ,new CursorDef("T00046", "SELECT [identificacion] FROM dbo.[Persona] WITH (NOLOCK) WHERE ([identificacion] = @identificacion) AND (Not ( [id] = @Personaid)) ",true, GxErrorMask.GX_NOMASK, false, this,prmT00046,1,0,true,false )
             ,new CursorDef("T00047", "SELECT [nombre] FROM dbo.[Pais] WITH (NOLOCK) WHERE [id] = @Paisid ",true, GxErrorMask.GX_NOMASK, false, this,prmT00047,1,0,true,false )
             ,new CursorDef("T00048", "SELECT [id] AS Personaid FROM dbo.[Persona] WITH (NOLOCK) WHERE [id] = @Personaid  OPTION (FAST 1)",true, GxErrorMask.GX_NOMASK, false, this,prmT00048,1,0,true,false )
             ,new CursorDef("T00049", "SELECT TOP 1 [id] AS Personaid FROM dbo.[Persona] WITH (NOLOCK) WHERE ( [id] > @Personaid) ORDER BY [id]  OPTION (FAST 1)",true, GxErrorMask.GX_NOMASK, false, this,prmT00049,1,0,true,true )
             ,new CursorDef("T000410", "SELECT TOP 1 [id] AS Personaid FROM dbo.[Persona] WITH (NOLOCK) WHERE ( [id] < @Personaid) ORDER BY [id] DESC  OPTION (FAST 1)",true, GxErrorMask.GX_NOMASK, false, this,prmT000410,1,0,true,true )
             ,new CursorDef("T000411", "INSERT INTO dbo.[Persona]([identificacion], [nombre1], [nombre2], [apellidoPaterno], [apellidoMaterno], [nacimiento], [genero], [correo], [idPais]) VALUES(@identificacion, @nombre1, @nombre2, @PersonaapellidoPaterno, @PersonaapellidoMaterno, @nacimiento, @genero, @Personacorreo, @Paisid); SELECT SCOPE_IDENTITY()", GxErrorMask.GX_NOMASK,prmT000411)
             ,new CursorDef("T000412", "UPDATE dbo.[Persona] SET [identificacion]=@identificacion, [nombre1]=@nombre1, [nombre2]=@nombre2, [apellidoPaterno]=@PersonaapellidoPaterno, [apellidoMaterno]=@PersonaapellidoMaterno, [nacimiento]=@nacimiento, [genero]=@genero, [correo]=@Personacorreo, [idPais]=@Paisid  WHERE [id] = @Personaid", GxErrorMask.GX_NOMASK,prmT000412)
             ,new CursorDef("T000413", "DELETE FROM dbo.[Persona]  WHERE [id] = @Personaid", GxErrorMask.GX_NOMASK,prmT000413)
             ,new CursorDef("T000414", "SELECT [nombre] FROM dbo.[Pais] WITH (NOLOCK) WHERE [id] = @Paisid ",true, GxErrorMask.GX_NOMASK, false, this,prmT000414,1,0,true,false )
             ,new CursorDef("T000415", "SELECT TOP 1 [id] AS Responsableid FROM dbo.[Responsable] WITH (NOLOCK) WHERE [idPersona] = @Personaid ",true, GxErrorMask.GX_NOMASK, false, this,prmT000415,1,0,true,true )
             ,new CursorDef("T000416", "SELECT TOP 1 [id] AS Beneficiarioid FROM dbo.[Beneficiario] WITH (NOLOCK) WHERE [idPersona] = @Personaid ",true, GxErrorMask.GX_NOMASK, false, this,prmT000416,1,0,true,true )
             ,new CursorDef("T000417", "SELECT [id] AS Personaid FROM dbo.[Persona] WITH (NOLOCK) ORDER BY [id]  OPTION (FAST 100)",true, GxErrorMask.GX_NOMASK, false, this,prmT000417,100,0,true,false )
             ,new CursorDef("T000418", "SELECT [identificacion] FROM dbo.[Persona] WITH (NOLOCK) WHERE ([identificacion] = @identificacion) AND (Not ( [id] = @Personaid)) ",true, GxErrorMask.GX_NOMASK, false, this,prmT000418,1,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getVarchar(2) ;
                ((String[]) buf[2])[0] = rslt.getVarchar(3) ;
                ((String[]) buf[3])[0] = rslt.getVarchar(4) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(4);
                ((String[]) buf[5])[0] = rslt.getVarchar(5) ;
                ((String[]) buf[6])[0] = rslt.getVarchar(6) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(6);
                ((DateTime[]) buf[8])[0] = rslt.getGXDate(7) ;
                ((String[]) buf[9])[0] = rslt.getVarchar(8) ;
                ((String[]) buf[10])[0] = rslt.getVarchar(9) ;
                ((bool[]) buf[11])[0] = rslt.wasNull(9);
                ((int[]) buf[12])[0] = rslt.getInt(10) ;
                return;
             case 1 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getVarchar(2) ;
                ((String[]) buf[2])[0] = rslt.getVarchar(3) ;
                ((String[]) buf[3])[0] = rslt.getVarchar(4) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(4);
                ((String[]) buf[5])[0] = rslt.getVarchar(5) ;
                ((String[]) buf[6])[0] = rslt.getVarchar(6) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(6);
                ((DateTime[]) buf[8])[0] = rslt.getGXDate(7) ;
                ((String[]) buf[9])[0] = rslt.getVarchar(8) ;
                ((String[]) buf[10])[0] = rslt.getVarchar(9) ;
                ((bool[]) buf[11])[0] = rslt.wasNull(9);
                ((int[]) buf[12])[0] = rslt.getInt(10) ;
                return;
             case 2 :
                ((String[]) buf[0])[0] = rslt.getVarchar(1) ;
                return;
             case 3 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getVarchar(2) ;
                ((String[]) buf[2])[0] = rslt.getVarchar(3) ;
                ((String[]) buf[3])[0] = rslt.getVarchar(4) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(4);
                ((String[]) buf[5])[0] = rslt.getVarchar(5) ;
                ((String[]) buf[6])[0] = rslt.getVarchar(6) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(6);
                ((DateTime[]) buf[8])[0] = rslt.getGXDate(7) ;
                ((String[]) buf[9])[0] = rslt.getVarchar(8) ;
                ((String[]) buf[10])[0] = rslt.getVarchar(9) ;
                ((bool[]) buf[11])[0] = rslt.wasNull(9);
                ((String[]) buf[12])[0] = rslt.getVarchar(10) ;
                ((int[]) buf[13])[0] = rslt.getInt(11) ;
                return;
             case 4 :
                ((String[]) buf[0])[0] = rslt.getVarchar(1) ;
                return;
             case 5 :
                ((String[]) buf[0])[0] = rslt.getVarchar(1) ;
                return;
             case 6 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 7 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 8 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 9 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 12 :
                ((String[]) buf[0])[0] = rslt.getVarchar(1) ;
                return;
             case 13 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 14 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 15 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 16 :
                ((String[]) buf[0])[0] = rslt.getVarchar(1) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 1 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 2 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 3 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 4 :
                stmt.SetParameter(1, (String)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
             case 5 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 6 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 7 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 8 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 9 :
                stmt.SetParameter(1, (String)parms[0]);
                stmt.SetParameter(2, (String)parms[1]);
                if ( (bool)parms[2] )
                {
                   stmt.setNull( 3 , SqlDbType.VarChar );
                }
                else
                {
                   stmt.SetParameter(3, (String)parms[3]);
                }
                stmt.SetParameter(4, (String)parms[4]);
                if ( (bool)parms[5] )
                {
                   stmt.setNull( 5 , SqlDbType.VarChar );
                }
                else
                {
                   stmt.SetParameter(5, (String)parms[6]);
                }
                stmt.SetParameter(6, (DateTime)parms[7]);
                stmt.SetParameter(7, (String)parms[8]);
                if ( (bool)parms[9] )
                {
                   stmt.setNull( 8 , SqlDbType.VarChar );
                }
                else
                {
                   stmt.SetParameter(8, (String)parms[10]);
                }
                stmt.SetParameter(9, (int)parms[11]);
                return;
             case 10 :
                stmt.SetParameter(1, (String)parms[0]);
                stmt.SetParameter(2, (String)parms[1]);
                if ( (bool)parms[2] )
                {
                   stmt.setNull( 3 , SqlDbType.VarChar );
                }
                else
                {
                   stmt.SetParameter(3, (String)parms[3]);
                }
                stmt.SetParameter(4, (String)parms[4]);
                if ( (bool)parms[5] )
                {
                   stmt.setNull( 5 , SqlDbType.VarChar );
                }
                else
                {
                   stmt.SetParameter(5, (String)parms[6]);
                }
                stmt.SetParameter(6, (DateTime)parms[7]);
                stmt.SetParameter(7, (String)parms[8]);
                if ( (bool)parms[9] )
                {
                   stmt.setNull( 8 , SqlDbType.VarChar );
                }
                else
                {
                   stmt.SetParameter(8, (String)parms[10]);
                }
                stmt.SetParameter(9, (int)parms[11]);
                stmt.SetParameter(10, (int)parms[12]);
                return;
             case 11 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 12 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 13 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 14 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 16 :
                stmt.SetParameter(1, (String)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
       }
    }

    public String getDataStoreName( )
    {
       return "DATASTORE1";
    }

 }

 public class persona__default : DataStoreHelperBase, IDataStoreHelper
 {
    public ICursor[] getCursors( )
    {
       cursorDefinitions();
       return new Cursor[] {
     };
  }

  private static CursorDef[] def;
  private void cursorDefinitions( )
  {
     if ( def == null )
     {
        def= new CursorDef[] {
        };
     }
  }

  public void getResults( int cursor ,
                          IFieldGetter rslt ,
                          Object[] buf )
  {
     switch ( cursor )
     {
     }
  }

  public void setParameters( int cursor ,
                             IFieldSetter stmt ,
                             Object[] parms )
  {
     switch ( cursor )
     {
     }
  }

}

}
