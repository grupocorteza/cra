/*
               File: Plan
        Description: Plan
             Author: GeneXus C# Generator version 16_0_0-127771
       Generated on: 1/5/2019 14:46:49.65
       Program type: Callable routine
          Main DBMS: SQL Server
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Xml.Serialization;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class plan : GXDataArea, System.Web.SessionState.IRequiresSessionState
   {
      protected void INITENV( )
      {
         if ( GxWebError != 0 )
         {
            return  ;
         }
      }

      protected void INITTRN( )
      {
         initialize_properties( ) ;
         entryPointCalled = false;
         gxfirstwebparm = GetNextPar( );
         gxfirstwebparm_bkp = gxfirstwebparm;
         gxfirstwebparm = DecryptAjaxCall( gxfirstwebparm);
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         if ( StringUtil.StrCmp(gxfirstwebparm, "dyncall") == 0 )
         {
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            dyncall( GetNextPar( )) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxExecAct_"+"gxLoad_35") == 0 )
         {
            A29PlanidProyecto = (int)(NumberUtil.Val( GetNextPar( ), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A29PlanidProyecto", StringUtil.LTrim( StringUtil.Str( (decimal)(A29PlanidProyecto), 9, 0)));
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxLoad_35( A29PlanidProyecto) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxExecAct_"+"gxLoad_31") == 0 )
         {
            A19Comunidadid = (int)(NumberUtil.Val( GetNextPar( ), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A19Comunidadid", StringUtil.LTrim( StringUtil.Str( (decimal)(A19Comunidadid), 9, 0)));
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxLoad_31( A19Comunidadid) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxExecAct_"+"gxLoad_32") == 0 )
         {
            A22TipoBeneficiarioid = (int)(NumberUtil.Val( GetNextPar( ), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A22TipoBeneficiarioid", StringUtil.LTrim( StringUtil.Str( (decimal)(A22TipoBeneficiarioid), 9, 0)));
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxLoad_32( A22TipoBeneficiarioid) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxExecAct_"+"gxLoad_36") == 0 )
         {
            A33PlanidResponsable = (int)(NumberUtil.Val( GetNextPar( ), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A33PlanidResponsable", StringUtil.LTrim( StringUtil.Str( (decimal)(A33PlanidResponsable), 9, 0)));
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxLoad_36( A33PlanidResponsable) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxExecAct_"+"gxLoad_37") == 0 )
         {
            A34PlanidFinanciero = (int)(NumberUtil.Val( GetNextPar( ), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A34PlanidFinanciero", StringUtil.LTrim( StringUtil.Str( (decimal)(A34PlanidFinanciero), 9, 0)));
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxLoad_37( A34PlanidFinanciero) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxExecAct_"+"gxLoad_38") == 0 )
         {
            A35PlanidTecnico = (int)(NumberUtil.Val( GetNextPar( ), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A35PlanidTecnico", StringUtil.LTrim( StringUtil.Str( (decimal)(A35PlanidTecnico), 9, 0)));
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxLoad_38( A35PlanidTecnico) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxExecAct_"+"gxLoad_33") == 0 )
         {
            A12Estadoid = (int)(NumberUtil.Val( GetNextPar( ), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A12Estadoid", StringUtil.LTrim( StringUtil.Str( (decimal)(A12Estadoid), 9, 0)));
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxLoad_33( A12Estadoid) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxExecAct_"+"gxLoad_34") == 0 )
         {
            A3Actividadid = (int)(NumberUtil.Val( GetNextPar( ), "."));
            n3Actividadid = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A3Actividadid", StringUtil.LTrim( StringUtil.Str( (decimal)(A3Actividadid), 9, 0)));
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxLoad_34( A3Actividadid) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxEvt") == 0 )
         {
            setAjaxEventMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxfirstwebparm = GetNextPar( );
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxfullajaxEvt") == 0 )
         {
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxfirstwebparm = GetNextPar( );
         }
         else
         {
            if ( ! IsValidAjaxCall( false) )
            {
               GxWebError = 1;
               return  ;
            }
            gxfirstwebparm = gxfirstwebparm_bkp;
         }
         if ( ! entryPointCalled && ! ( isAjaxCallMode( ) || isFullAjaxMode( ) ) )
         {
            Gx_mode = gxfirstwebparm;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!")), context));
            if ( StringUtil.StrCmp(gxfirstwebparm, "viewer") != 0 )
            {
               AV7Planid = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV7Planid", StringUtil.LTrim( StringUtil.Str( (decimal)(AV7Planid), 9, 0)));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vPLANID", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV7Planid), "ZZZZZZZZ9"), context));
            }
         }
         if ( context.isSpaRequest( ) )
         {
            enableJsOutput();
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.GetCookie( "GX_SESSION_ID"))) )
         {
            gxcookieaux = context.SetCookie( "GX_SESSION_ID", Encrypt64( Crypto.GetEncryptionKey( ), Crypto.GetServerKey( )), "", (DateTime)(DateTime.MinValue), "", 0);
         }
         GXKey = Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
         toggleJsOutput = isJsOutputEnabled( );
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         init_web_controls( ) ;
         if ( toggleJsOutput )
         {
            if ( context.isSpaRequest( ) )
            {
               enableJsOutput();
            }
         }
         if ( ! context.isSpaRequest( ) )
         {
            Form.Meta.addItem("generator", "GeneXus C# 16_0_0-127771", 0) ;
            Form.Meta.addItem("description", "Plan", 0) ;
         }
         context.wjLoc = "";
         context.nUserReturn = 0;
         context.wbHandled = 0;
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
         }
         GX_FocusControl = edtPlanidProyecto_Internalname;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         wbErr = false;
         context.SetDefaultTheme("Carmine");
         if ( ! context.IsLocalStorageSupported( ) )
         {
            context.PushCurrentUrl();
         }
      }

      public plan( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsDataStore1 = context.GetDataStore("DataStore1");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("Carmine");
      }

      public plan( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsDataStore1 = context.GetDataStore("DataStore1");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( String aP0_Gx_mode ,
                           int aP1_Planid )
      {
         this.Gx_mode = aP0_Gx_mode;
         this.AV7Planid = aP1_Planid;
         executePrivate();
      }

      void executePrivate( )
      {
         isStatic = false;
         webExecute();
      }

      protected override void createObjects( )
      {
      }

      public override void webExecute( )
      {
         if ( initialized == 0 )
         {
            createObjects();
            initialize();
         }
         INITENV( ) ;
         INITTRN( ) ;
         if ( ( GxWebError == 0 ) && ! isAjaxCallMode( ) )
         {
            MasterPageObj = (GXMasterPage) ClassLoader.GetInstance("rwdmasterpage", "GeneXus.Programs.rwdmasterpage", new Object[] {new GxContext( context.handle, context.DataStores, context.HttpContext)});
            MasterPageObj.setDataArea(this,false);
            ValidateSpaRequest();
            MasterPageObj.webExecute();
            if ( ( GxWebError == 0 ) && context.isAjaxRequest( ) )
            {
               enableOutput();
               if ( ! context.isAjaxRequest( ) )
               {
                  context.GX_webresponse.AppendHeader("Cache-Control", "no-store");
               }
               if ( ! context.WillRedirect( ) )
               {
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               }
               else
               {
                  if ( context.isAjaxRequest( ) )
                  {
                     disableOutput();
                  }
                  RenderHtmlHeaders( ) ;
                  context.Redirect( context.wjLoc );
                  context.DispatchAjaxCommands();
               }
            }
         }
         this.cleanup();
      }

      protected void fix_multi_value_controls( )
      {
      }

      protected void Draw( )
      {
         if ( context.isAjaxRequest( ) )
         {
            disableOutput();
         }
         if ( ! GxWebStd.gx_redirect( context) )
         {
            disable_std_buttons( ) ;
            enableDisable( ) ;
            set_caption( ) ;
            /* Form start */
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "Section", "left", "top", " "+"data-gx-base-lib=\"bootstrapv3\""+" "+"data-abstract-form"+" ", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, divMaintable_Internalname, 1, 0, "px", 0, "px", "WWAdvancedContainer", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12 col-sm-8 col-sm-offset-2", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, divTitlecontainer_Internalname, 1, 0, "px", 0, "px", "TableTop", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12", "left", "top", "", "", "div");
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTitle_Internalname, "Plan", "", "", lblTitle_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "Title", 0, "", 1, 1, 0, "HLP_Plan.htm");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12", "left", "top", "", "", "div");
            ClassString = "ErrorViewer";
            StyleString = "";
            GxWebStd.gx_msg_list( context, "", context.GX_msglist.DisplayMode, StyleString, ClassString, "", "false");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12 col-sm-8 col-sm-offset-2", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, divFormcontainer_Internalname, 1, 0, "px", 0, "px", "FormContainer", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, divToolbarcell_Internalname, 1, 0, "px", 0, "px", "col-xs-12 col-sm-9 col-sm-offset-3 ToolbarCellClass", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "gx-action-group ActionGroup", "left", "top", " "+"data-gx-actiongroup-type=\"toolbar\""+" ", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "btn-group", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "gx-button", "left", "top", "", "", "div");
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 21,'',false,'',0)\"";
            ClassString = "BtnFirst";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtn_first_Internalname, "", "", bttBtn_first_Jsonclick, 5, "", "", StyleString, ClassString, bttBtn_first_Visible, 0, "standard", "'"+""+"'"+",false,"+"'"+"EFIRST."+"'", TempTags, "", context.GetButtonType( ), "HLP_Plan.htm");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "gx-button", "left", "top", "", "", "div");
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 23,'',false,'',0)\"";
            ClassString = "BtnPrevious";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtn_previous_Internalname, "", "", bttBtn_previous_Jsonclick, 5, "", "", StyleString, ClassString, bttBtn_previous_Visible, 0, "standard", "'"+""+"'"+",false,"+"'"+"EPREVIOUS."+"'", TempTags, "", context.GetButtonType( ), "HLP_Plan.htm");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "gx-button", "left", "top", "", "", "div");
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 25,'',false,'',0)\"";
            ClassString = "BtnNext";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtn_next_Internalname, "", "", bttBtn_next_Jsonclick, 5, "", "", StyleString, ClassString, bttBtn_next_Visible, 0, "standard", "'"+""+"'"+",false,"+"'"+"ENEXT."+"'", TempTags, "", context.GetButtonType( ), "HLP_Plan.htm");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "gx-button", "left", "top", "", "", "div");
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 27,'',false,'',0)\"";
            ClassString = "BtnLast";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtn_last_Internalname, "", "", bttBtn_last_Jsonclick, 5, "", "", StyleString, ClassString, bttBtn_last_Visible, 0, "standard", "'"+""+"'"+",false,"+"'"+"ELAST."+"'", TempTags, "", context.GetButtonType( ), "HLP_Plan.htm");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "gx-button", "left", "top", "", "", "div");
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 29,'',false,'',0)\"";
            ClassString = "BtnSelect";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtn_select_Internalname, "", "Select", bttBtn_select_Jsonclick, 5, "Select", "", StyleString, ClassString, bttBtn_select_Visible, 0, "standard", "'"+""+"'"+",false,"+"'"+"ESELECT."+"'", TempTags, "", 2, "HLP_Plan.htm");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12 FormCellAdvanced", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "form-group gx-form-group", "left", "top", ""+" data-gx-for=\""+edtPlanidProyecto_Internalname+"\"", "", "div");
            /* Attribute/Variable Label */
            GxWebStd.gx_label_element( context, edtPlanidProyecto_Internalname, "Proyecto", "col-sm-3 AttributeLabel", 1, true);
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-sm-9 gx-attribute", "left", "top", "", "", "div");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 34,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtPlanidProyecto_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A29PlanidProyecto), 9, 0, ".", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(A29PlanidProyecto), "ZZZZZZZZ9")), TempTags+" onchange=\""+"gx.num.valid_integer( this,',');"+";gx.evt.onchange(this, event)\" "+" onblur=\""+";gx.evt.onblur(this,34);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtPlanidProyecto_Jsonclick, 0, "Attribute", "", "", "", "", 1, edtPlanidProyecto_Enabled, 1, "number", "1", 9, "chr", 1, "row", 9, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_Plan.htm");
            /* Static images/pictures */
            ClassString = "gx-prompt Image";
            StyleString = "";
            sImgUrl = (String)(context.GetImagePath( "prompt.gif", "", context.GetTheme( )));
            GxWebStd.gx_bitmap( context, imgprompt_29_Internalname, sImgUrl, imgprompt_29_Link, "", "", context.GetTheme( ), imgprompt_29_Visible, 1, "", "", 0, 0, 0, "", 0, "", 0, 0, 0, "", "", StyleString, ClassString, "", "", "", "", "", "", "", 1, false, false, context.GetImageSrcSet( sImgUrl), "HLP_Plan.htm");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12 FormCell", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "form-group gx-form-group", "left", "top", ""+" data-gx-for=\""+edtnombreEvento_Internalname+"\"", "", "div");
            /* Attribute/Variable Label */
            GxWebStd.gx_label_element( context, edtnombreEvento_Internalname, "Nombre Evento", "col-sm-3 AttributeLabel", 1, true);
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-sm-9 gx-attribute", "left", "top", "", "", "div");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 39,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtnombreEvento_Internalname, A30nombreEvento, StringUtil.RTrim( context.localUtil.Format( A30nombreEvento, "")), TempTags+" onchange=\""+""+";gx.evt.onchange(this, event)\" "+" onblur=\""+";gx.evt.onblur(this,39);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtnombreEvento_Jsonclick, 0, "Attribute", "", "", "", "", 1, edtnombreEvento_Enabled, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_Plan.htm");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12 FormCell", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "form-group gx-form-group", "left", "top", ""+" data-gx-for=\""+edtComunidadid_Internalname+"\"", "", "div");
            /* Attribute/Variable Label */
            GxWebStd.gx_label_element( context, edtComunidadid_Internalname, "Comunidad", "col-sm-3 AttributeLabel", 1, true);
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-sm-9 gx-attribute", "left", "top", "", "", "div");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 44,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtComunidadid_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A19Comunidadid), 9, 0, ".", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(A19Comunidadid), "ZZZZZZZZ9")), TempTags+" onchange=\""+"gx.num.valid_integer( this,',');"+";gx.evt.onchange(this, event)\" "+" onblur=\""+";gx.evt.onblur(this,44);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtComunidadid_Jsonclick, 0, "Attribute", "", "", "", "", 1, edtComunidadid_Enabled, 1, "number", "1", 9, "chr", 1, "row", 9, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_Plan.htm");
            /* Static images/pictures */
            ClassString = "gx-prompt Image";
            StyleString = "";
            sImgUrl = (String)(context.GetImagePath( "prompt.gif", "", context.GetTheme( )));
            GxWebStd.gx_bitmap( context, imgprompt_19_Internalname, sImgUrl, imgprompt_19_Link, "", "", context.GetTheme( ), imgprompt_19_Visible, 1, "", "", 0, 0, 0, "", 0, "", 0, 0, 0, "", "", StyleString, ClassString, "", "", "", "", "", "", "", 1, false, false, context.GetImageSrcSet( sImgUrl), "HLP_Plan.htm");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12 FormCell", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "form-group gx-form-group", "left", "top", ""+" data-gx-for=\""+edtComunidadnombre_Internalname+"\"", "", "div");
            /* Attribute/Variable Label */
            GxWebStd.gx_label_element( context, edtComunidadnombre_Internalname, "Comunidadnombre", "col-sm-3 AttributeLabel", 1, true);
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-sm-9 gx-attribute", "left", "top", "", "", "div");
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtComunidadnombre_Internalname, A130Comunidadnombre, StringUtil.RTrim( context.localUtil.Format( A130Comunidadnombre, "")), "", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtComunidadnombre_Jsonclick, 0, "Attribute", "", "", "", "", 1, edtComunidadnombre_Enabled, 0, "text", "", 60, "chr", 1, "row", 60, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_Plan.htm");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12 FormCell", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "form-group gx-form-group", "left", "top", ""+" data-gx-for=\""+edtinicio_Internalname+"\"", "", "div");
            /* Attribute/Variable Label */
            GxWebStd.gx_label_element( context, edtinicio_Internalname, "Fecha de Inicio", "col-sm-3 AttributeLabel", 1, true);
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-sm-9 gx-attribute", "left", "top", "", "", "div");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 54,'',false,'',0)\"";
            context.WriteHtmlText( "<div id=\""+edtinicio_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtinicio_Internalname, context.localUtil.Format(A31inicio, "99/99/99"), context.localUtil.Format( A31inicio, "99/99/99"), TempTags+" onchange=\""+"gx.date.valid_date(this, 8,'MDY',0,12,'eng',false,0);"+";gx.evt.onchange(this, event)\" "+" onblur=\""+";gx.evt.onblur(this,54);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtinicio_Jsonclick, 0, "Attribute", "", "", "", "", 1, edtinicio_Enabled, 0, "text", "", 8, "chr", 1, "row", 8, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_Plan.htm");
            GxWebStd.gx_bitmap( context, edtinicio_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((1==0)||(edtinicio_Enabled==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", "", 1, false, false, "", "HLP_Plan.htm");
            context.WriteHtmlTextNl( "</div>") ;
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12 FormCell", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "form-group gx-form-group", "left", "top", ""+" data-gx-for=\""+edtfin_Internalname+"\"", "", "div");
            /* Attribute/Variable Label */
            GxWebStd.gx_label_element( context, edtfin_Internalname, "Fecha de Finalización", "col-sm-3 AttributeLabel", 1, true);
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-sm-9 gx-attribute", "left", "top", "", "", "div");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 59,'',false,'',0)\"";
            context.WriteHtmlText( "<div id=\""+edtfin_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtfin_Internalname, context.localUtil.Format(A32fin, "99/99/99"), context.localUtil.Format( A32fin, "99/99/99"), TempTags+" onchange=\""+"gx.date.valid_date(this, 8,'MDY',0,12,'eng',false,0);"+";gx.evt.onchange(this, event)\" "+" onblur=\""+";gx.evt.onblur(this,59);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtfin_Jsonclick, 0, "Attribute", "", "", "", "", 1, edtfin_Enabled, 0, "text", "", 8, "chr", 1, "row", 8, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_Plan.htm");
            GxWebStd.gx_bitmap( context, edtfin_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((1==0)||(edtfin_Enabled==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", "", 1, false, false, "", "HLP_Plan.htm");
            context.WriteHtmlTextNl( "</div>") ;
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12 FormCell", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "form-group gx-form-group", "left", "top", ""+" data-gx-for=\""+edtTipoBeneficiarioid_Internalname+"\"", "", "div");
            /* Attribute/Variable Label */
            GxWebStd.gx_label_element( context, edtTipoBeneficiarioid_Internalname, "Tipo Beneficiario", "col-sm-3 AttributeLabel", 1, true);
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-sm-9 gx-attribute", "left", "top", "", "", "div");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 64,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtTipoBeneficiarioid_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A22TipoBeneficiarioid), 9, 0, ".", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(A22TipoBeneficiarioid), "ZZZZZZZZ9")), TempTags+" onchange=\""+"gx.num.valid_integer( this,',');"+";gx.evt.onchange(this, event)\" "+" onblur=\""+";gx.evt.onblur(this,64);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtTipoBeneficiarioid_Jsonclick, 0, "Attribute", "", "", "", "", 1, edtTipoBeneficiarioid_Enabled, 1, "number", "1", 9, "chr", 1, "row", 9, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_Plan.htm");
            /* Static images/pictures */
            ClassString = "gx-prompt Image";
            StyleString = "";
            sImgUrl = (String)(context.GetImagePath( "prompt.gif", "", context.GetTheme( )));
            GxWebStd.gx_bitmap( context, imgprompt_22_Internalname, sImgUrl, imgprompt_22_Link, "", "", context.GetTheme( ), imgprompt_22_Visible, 1, "", "", 0, 0, 0, "", 0, "", 0, 0, 0, "", "", StyleString, ClassString, "", "", "", "", "", "", "", 1, false, false, context.GetImageSrcSet( sImgUrl), "HLP_Plan.htm");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12 FormCell", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "form-group gx-form-group", "left", "top", ""+" data-gx-for=\""+edtPlanidResponsable_Internalname+"\"", "", "div");
            /* Attribute/Variable Label */
            GxWebStd.gx_label_element( context, edtPlanidResponsable_Internalname, "Responsable", "col-sm-3 AttributeLabel", 1, true);
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-sm-9 gx-attribute", "left", "top", "", "", "div");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 69,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtPlanidResponsable_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A33PlanidResponsable), 9, 0, ".", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(A33PlanidResponsable), "ZZZZZZZZ9")), TempTags+" onchange=\""+"gx.num.valid_integer( this,',');"+";gx.evt.onchange(this, event)\" "+" onblur=\""+";gx.evt.onblur(this,69);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtPlanidResponsable_Jsonclick, 0, "Attribute", "", "", "", "", 1, edtPlanidResponsable_Enabled, 1, "number", "1", 9, "chr", 1, "row", 9, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_Plan.htm");
            /* Static images/pictures */
            ClassString = "gx-prompt Image";
            StyleString = "";
            sImgUrl = (String)(context.GetImagePath( "prompt.gif", "", context.GetTheme( )));
            GxWebStd.gx_bitmap( context, imgprompt_33_Internalname, sImgUrl, imgprompt_33_Link, "", "", context.GetTheme( ), imgprompt_33_Visible, 1, "", "", 0, 0, 0, "", 0, "", 0, 0, 0, "", "", StyleString, ClassString, "", "", "", "", "", "", "", 1, false, false, context.GetImageSrcSet( sImgUrl), "HLP_Plan.htm");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12 FormCell", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "form-group gx-form-group", "left", "top", ""+" data-gx-for=\""+edtPlanidFinanciero_Internalname+"\"", "", "div");
            /* Attribute/Variable Label */
            GxWebStd.gx_label_element( context, edtPlanidFinanciero_Internalname, "Financiero", "col-sm-3 AttributeLabel", 1, true);
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-sm-9 gx-attribute", "left", "top", "", "", "div");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 74,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtPlanidFinanciero_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A34PlanidFinanciero), 9, 0, ".", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(A34PlanidFinanciero), "ZZZZZZZZ9")), TempTags+" onchange=\""+"gx.num.valid_integer( this,',');"+";gx.evt.onchange(this, event)\" "+" onblur=\""+";gx.evt.onblur(this,74);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtPlanidFinanciero_Jsonclick, 0, "Attribute", "", "", "", "", 1, edtPlanidFinanciero_Enabled, 1, "number", "1", 9, "chr", 1, "row", 9, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_Plan.htm");
            /* Static images/pictures */
            ClassString = "gx-prompt Image";
            StyleString = "";
            sImgUrl = (String)(context.GetImagePath( "prompt.gif", "", context.GetTheme( )));
            GxWebStd.gx_bitmap( context, imgprompt_34_Internalname, sImgUrl, imgprompt_34_Link, "", "", context.GetTheme( ), imgprompt_34_Visible, 1, "", "", 0, 0, 0, "", 0, "", 0, 0, 0, "", "", StyleString, ClassString, "", "", "", "", "", "", "", 1, false, false, context.GetImageSrcSet( sImgUrl), "HLP_Plan.htm");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12 FormCell", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "form-group gx-form-group", "left", "top", ""+" data-gx-for=\""+edtPlanidTecnico_Internalname+"\"", "", "div");
            /* Attribute/Variable Label */
            GxWebStd.gx_label_element( context, edtPlanidTecnico_Internalname, "Tecnico", "col-sm-3 AttributeLabel", 1, true);
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-sm-9 gx-attribute", "left", "top", "", "", "div");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 79,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtPlanidTecnico_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A35PlanidTecnico), 9, 0, ".", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(A35PlanidTecnico), "ZZZZZZZZ9")), TempTags+" onchange=\""+"gx.num.valid_integer( this,',');"+";gx.evt.onchange(this, event)\" "+" onblur=\""+";gx.evt.onblur(this,79);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtPlanidTecnico_Jsonclick, 0, "Attribute", "", "", "", "", 1, edtPlanidTecnico_Enabled, 1, "number", "1", 9, "chr", 1, "row", 9, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_Plan.htm");
            /* Static images/pictures */
            ClassString = "gx-prompt Image";
            StyleString = "";
            sImgUrl = (String)(context.GetImagePath( "prompt.gif", "", context.GetTheme( )));
            GxWebStd.gx_bitmap( context, imgprompt_35_Internalname, sImgUrl, imgprompt_35_Link, "", "", context.GetTheme( ), imgprompt_35_Visible, 1, "", "", 0, 0, 0, "", 0, "", 0, 0, 0, "", "", StyleString, ClassString, "", "", "", "", "", "", "", 1, false, false, context.GetImageSrcSet( sImgUrl), "HLP_Plan.htm");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12 FormCell", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "form-group gx-form-group", "left", "top", ""+" data-gx-for=\""+edtobjGeneral_Internalname+"\"", "", "div");
            /* Attribute/Variable Label */
            GxWebStd.gx_label_element( context, edtobjGeneral_Internalname, "Obj General", "col-sm-3 AttributeLabel", 1, true);
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-sm-9 gx-attribute", "left", "top", "", "", "div");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 84,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtobjGeneral_Internalname, A36objGeneral, StringUtil.RTrim( context.localUtil.Format( A36objGeneral, "")), TempTags+" onchange=\""+""+";gx.evt.onchange(this, event)\" "+" onblur=\""+";gx.evt.onblur(this,84);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtobjGeneral_Jsonclick, 0, "Attribute", "", "", "", "", 1, edtobjGeneral_Enabled, 0, "text", "", 80, "chr", 1, "row", 100, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_Plan.htm");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12 FormCell", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "form-group gx-form-group", "left", "top", ""+" data-gx-for=\""+edtobjEspecificos_Internalname+"\"", "", "div");
            /* Attribute/Variable Label */
            GxWebStd.gx_label_element( context, edtobjEspecificos_Internalname, "Obj Especifico", "col-sm-3 AttributeLabel", 1, true);
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-sm-9 gx-attribute", "left", "top", "", "", "div");
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 89,'',false,'',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtobjEspecificos_Internalname, A37objEspecificos, "", TempTags+" onchange=\""+""+";gx.evt.onchange(this, event)\" "+" onblur=\""+";gx.evt.onblur(this,89);\"", 0, 1, edtobjEspecificos_Enabled, 0, 80, "chr", 3, "row", StyleString, ClassString, "", "", "200", -1, 0, "", "", -1, true, "", "'"+""+"'"+",false,"+"'"+""+"'", 0, "HLP_Plan.htm");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12 FormCell", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "form-group gx-form-group", "left", "top", ""+" data-gx-for=\""+edtdescripcionActividad_Internalname+"\"", "", "div");
            /* Attribute/Variable Label */
            GxWebStd.gx_label_element( context, edtdescripcionActividad_Internalname, "Descripcion Actividad", "col-sm-3 AttributeLabel", 1, true);
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-sm-9 gx-attribute", "left", "top", "", "", "div");
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 94,'',false,'',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtdescripcionActividad_Internalname, A38descripcionActividad, "", TempTags+" onchange=\""+""+";gx.evt.onchange(this, event)\" "+" onblur=\""+";gx.evt.onblur(this,94);\"", 0, 1, edtdescripcionActividad_Enabled, 0, 80, "chr", 7, "row", StyleString, ClassString, "", "", "500", -1, 0, "", "", -1, true, "", "'"+""+"'"+",false,"+"'"+""+"'", 0, "HLP_Plan.htm");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12 FormCell", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "form-group gx-form-group", "left", "top", ""+" data-gx-for=\""+edtEstadoid_Internalname+"\"", "", "div");
            /* Attribute/Variable Label */
            GxWebStd.gx_label_element( context, edtEstadoid_Internalname, "Seleccione el Estado", "col-sm-3 AttributeLabel", 1, true);
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-sm-9 gx-attribute", "left", "top", "", "", "div");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 99,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtEstadoid_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A12Estadoid), 9, 0, ".", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(A12Estadoid), "ZZZZZZZZ9")), TempTags+" onchange=\""+"gx.num.valid_integer( this,',');"+";gx.evt.onchange(this, event)\" "+" onblur=\""+";gx.evt.onblur(this,99);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtEstadoid_Jsonclick, 0, "Attribute", "", "", "", "", 1, edtEstadoid_Enabled, 1, "number", "1", 9, "chr", 1, "row", 9, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_Plan.htm");
            /* Static images/pictures */
            ClassString = "gx-prompt Image";
            StyleString = "";
            sImgUrl = (String)(context.GetImagePath( "prompt.gif", "", context.GetTheme( )));
            GxWebStd.gx_bitmap( context, imgprompt_12_Internalname, sImgUrl, imgprompt_12_Link, "", "", context.GetTheme( ), imgprompt_12_Visible, 1, "", "", 0, 0, 0, "", 0, "", 0, 0, 0, "", "", StyleString, ClassString, "", "", "", "", "", "", "", 1, false, false, context.GetImageSrcSet( sImgUrl), "HLP_Plan.htm");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12 FormCell", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "form-group gx-form-group", "left", "top", ""+" data-gx-for=\""+edtEstadonombre_Internalname+"\"", "", "div");
            /* Attribute/Variable Label */
            GxWebStd.gx_label_element( context, edtEstadonombre_Internalname, "Estadonombre", "col-sm-3 AttributeLabel", 1, true);
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-sm-9 gx-attribute", "left", "top", "", "", "div");
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtEstadonombre_Internalname, A61Estadonombre, StringUtil.RTrim( context.localUtil.Format( A61Estadonombre, "")), "", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtEstadonombre_Jsonclick, 0, "Attribute", "", "", "", "", 1, edtEstadonombre_Enabled, 0, "text", "", 20, "chr", 1, "row", 20, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_Plan.htm");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12 FormCell", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "form-group gx-form-group", "left", "top", ""+" data-gx-for=\""+edtcantBeneficiarios_Internalname+"\"", "", "div");
            /* Attribute/Variable Label */
            GxWebStd.gx_label_element( context, edtcantBeneficiarios_Internalname, "cant Beneficiarios", "col-sm-3 AttributeLabel", 1, true);
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-sm-9 gx-attribute", "left", "top", "", "", "div");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 109,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtcantBeneficiarios_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A39cantBeneficiarios), 9, 0, ".", "")), ((edtcantBeneficiarios_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( (decimal)(A39cantBeneficiarios), "ZZZZZZZZ9")) : context.localUtil.Format( (decimal)(A39cantBeneficiarios), "ZZZZZZZZ9")), TempTags+" onchange=\""+"gx.num.valid_integer( this,',');"+";gx.evt.onchange(this, event)\" "+" onblur=\""+";gx.evt.onblur(this,109);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtcantBeneficiarios_Jsonclick, 0, "Attribute", "", "", "", "", 1, edtcantBeneficiarios_Enabled, 0, "number", "1", 9, "chr", 1, "row", 9, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_Plan.htm");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12 FormCell", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "form-group gx-form-group", "left", "top", ""+" data-gx-for=\""+edtActividadid_Internalname+"\"", "", "div");
            /* Attribute/Variable Label */
            GxWebStd.gx_label_element( context, edtActividadid_Internalname, "Actividad", "col-sm-3 AttributeLabel", 1, true);
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-sm-9 gx-attribute", "left", "top", "", "", "div");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 114,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtActividadid_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A3Actividadid), 9, 0, ".", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(A3Actividadid), "ZZZZZZZZ9")), TempTags+" onchange=\""+"gx.num.valid_integer( this,',');"+";gx.evt.onchange(this, event)\" "+" onblur=\""+";gx.evt.onblur(this,114);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtActividadid_Jsonclick, 0, "Attribute", "", "", "", "", 1, edtActividadid_Enabled, 1, "number", "1", 9, "chr", 1, "row", 9, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_Plan.htm");
            /* Static images/pictures */
            ClassString = "gx-prompt Image";
            StyleString = "";
            sImgUrl = (String)(context.GetImagePath( "prompt.gif", "", context.GetTheme( )));
            GxWebStd.gx_bitmap( context, imgprompt_3_Internalname, sImgUrl, imgprompt_3_Link, "", "", context.GetTheme( ), imgprompt_3_Visible, 1, "", "", 0, 0, 0, "", 0, "", 0, 0, 0, "", "", StyleString, ClassString, "", "", "", "", "", "", "", 1, false, false, context.GetImageSrcSet( sImgUrl), "HLP_Plan.htm");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12 FormCell", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "form-group gx-form-group", "left", "top", ""+" data-gx-for=\""+edtActividaddescripcion_Internalname+"\"", "", "div");
            /* Attribute/Variable Label */
            GxWebStd.gx_label_element( context, edtActividaddescripcion_Internalname, "Actividaddescripcion", "col-sm-3 AttributeLabel", 1, true);
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-sm-9 gx-attribute", "left", "top", "", "", "div");
            /* Multiple line edit */
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtActividaddescripcion_Internalname, A48Actividaddescripcion, "", "", 0, 1, edtActividaddescripcion_Enabled, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "", "300", -1, 0, "", "", -1, true, "", "'"+""+"'"+",false,"+"'"+""+"'", 0, "HLP_Plan.htm");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12", "Center", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "gx-action-group Confirm", "left", "top", " "+"data-gx-actiongroup-type=\"toolbar\""+" ", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "gx-button", "left", "top", "", "", "div");
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 124,'',false,'',0)\"";
            ClassString = "BtnEnter";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtn_enter_Internalname, "", "Confirm", bttBtn_enter_Jsonclick, 5, "Confirm", "", StyleString, ClassString, bttBtn_enter_Visible, bttBtn_enter_Enabled, "standard", "'"+""+"'"+",false,"+"'"+"EENTER."+"'", TempTags, "", context.GetButtonType( ), "HLP_Plan.htm");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "gx-button", "left", "top", "", "", "div");
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 126,'',false,'',0)\"";
            ClassString = "BtnCancel";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtn_cancel_Internalname, "", "Cancel", bttBtn_cancel_Jsonclick, 1, "Cancel", "", StyleString, ClassString, bttBtn_cancel_Visible, 1, "standard", "'"+""+"'"+",false,"+"'"+"ECANCEL."+"'", TempTags, "", context.GetButtonType( ), "HLP_Plan.htm");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "gx-button", "left", "top", "", "", "div");
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 128,'',false,'',0)\"";
            ClassString = "BtnDelete";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtn_delete_Internalname, "", "Delete", bttBtn_delete_Jsonclick, 5, "Delete", "", StyleString, ClassString, bttBtn_delete_Visible, bttBtn_delete_Enabled, "standard", "'"+""+"'"+",false,"+"'"+"EDELETE."+"'", TempTags, "", context.GetButtonType( ), "HLP_Plan.htm");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "Center", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            fix_multi_value_controls( ) ;
         }
         /* Execute Exit event if defined. */
      }

      protected void UserMain( )
      {
         standaloneStartup( ) ;
      }

      protected void UserMainFullajax( )
      {
         INITENV( ) ;
         INITTRN( ) ;
         UserMain( ) ;
         Draw( ) ;
         SendCloseFormHiddens( ) ;
      }

      protected void standaloneStartup( )
      {
         standaloneStartupServer( ) ;
         disable_std_buttons( ) ;
         enableDisable( ) ;
         Process( ) ;
      }

      protected void standaloneStartupServer( )
      {
         /* Execute Start event if defined. */
         context.wbGlbDoneStart = 0;
         /* Execute user event: Start */
         E110G2 ();
         context.wbGlbDoneStart = 1;
         assign_properties_default( ) ;
         if ( AnyError == 0 )
         {
            if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
            {
               /* Read saved SDTs. */
               /* Read variables values. */
               if ( ( ( context.localUtil.CToN( cgiGet( edtPlanidProyecto_Internalname), ".", ",") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtPlanidProyecto_Internalname), ".", ",") > Convert.ToDecimal( 999999999 )) ) )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "PLANIDPROYECTO");
                  AnyError = 1;
                  GX_FocusControl = edtPlanidProyecto_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  wbErr = true;
                  A29PlanidProyecto = 0;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A29PlanidProyecto", StringUtil.LTrim( StringUtil.Str( (decimal)(A29PlanidProyecto), 9, 0)));
               }
               else
               {
                  A29PlanidProyecto = (int)(context.localUtil.CToN( cgiGet( edtPlanidProyecto_Internalname), ".", ","));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A29PlanidProyecto", StringUtil.LTrim( StringUtil.Str( (decimal)(A29PlanidProyecto), 9, 0)));
               }
               A30nombreEvento = cgiGet( edtnombreEvento_Internalname);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A30nombreEvento", A30nombreEvento);
               if ( ( ( context.localUtil.CToN( cgiGet( edtComunidadid_Internalname), ".", ",") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtComunidadid_Internalname), ".", ",") > Convert.ToDecimal( 999999999 )) ) )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "COMUNIDADID");
                  AnyError = 1;
                  GX_FocusControl = edtComunidadid_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  wbErr = true;
                  A19Comunidadid = 0;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A19Comunidadid", StringUtil.LTrim( StringUtil.Str( (decimal)(A19Comunidadid), 9, 0)));
               }
               else
               {
                  A19Comunidadid = (int)(context.localUtil.CToN( cgiGet( edtComunidadid_Internalname), ".", ","));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A19Comunidadid", StringUtil.LTrim( StringUtil.Str( (decimal)(A19Comunidadid), 9, 0)));
               }
               A130Comunidadnombre = cgiGet( edtComunidadnombre_Internalname);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A130Comunidadnombre", A130Comunidadnombre);
               if ( context.localUtil.VCDate( cgiGet( edtinicio_Internalname), 1) == 0 )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_faildate", new   object[]  {"inicio"}), 1, "INICIO");
                  AnyError = 1;
                  GX_FocusControl = edtinicio_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  wbErr = true;
                  A31inicio = DateTime.MinValue;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A31inicio", context.localUtil.Format(A31inicio, "99/99/99"));
               }
               else
               {
                  A31inicio = context.localUtil.CToD( cgiGet( edtinicio_Internalname), 1);
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A31inicio", context.localUtil.Format(A31inicio, "99/99/99"));
               }
               if ( context.localUtil.VCDate( cgiGet( edtfin_Internalname), 1) == 0 )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_faildate", new   object[]  {"fin"}), 1, "FIN");
                  AnyError = 1;
                  GX_FocusControl = edtfin_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  wbErr = true;
                  A32fin = DateTime.MinValue;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A32fin", context.localUtil.Format(A32fin, "99/99/99"));
               }
               else
               {
                  A32fin = context.localUtil.CToD( cgiGet( edtfin_Internalname), 1);
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A32fin", context.localUtil.Format(A32fin, "99/99/99"));
               }
               if ( ( ( context.localUtil.CToN( cgiGet( edtTipoBeneficiarioid_Internalname), ".", ",") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtTipoBeneficiarioid_Internalname), ".", ",") > Convert.ToDecimal( 999999999 )) ) )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "TIPOBENEFICIARIOID");
                  AnyError = 1;
                  GX_FocusControl = edtTipoBeneficiarioid_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  wbErr = true;
                  A22TipoBeneficiarioid = 0;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A22TipoBeneficiarioid", StringUtil.LTrim( StringUtil.Str( (decimal)(A22TipoBeneficiarioid), 9, 0)));
               }
               else
               {
                  A22TipoBeneficiarioid = (int)(context.localUtil.CToN( cgiGet( edtTipoBeneficiarioid_Internalname), ".", ","));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A22TipoBeneficiarioid", StringUtil.LTrim( StringUtil.Str( (decimal)(A22TipoBeneficiarioid), 9, 0)));
               }
               if ( ( ( context.localUtil.CToN( cgiGet( edtPlanidResponsable_Internalname), ".", ",") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtPlanidResponsable_Internalname), ".", ",") > Convert.ToDecimal( 999999999 )) ) )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "PLANIDRESPONSABLE");
                  AnyError = 1;
                  GX_FocusControl = edtPlanidResponsable_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  wbErr = true;
                  A33PlanidResponsable = 0;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A33PlanidResponsable", StringUtil.LTrim( StringUtil.Str( (decimal)(A33PlanidResponsable), 9, 0)));
               }
               else
               {
                  A33PlanidResponsable = (int)(context.localUtil.CToN( cgiGet( edtPlanidResponsable_Internalname), ".", ","));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A33PlanidResponsable", StringUtil.LTrim( StringUtil.Str( (decimal)(A33PlanidResponsable), 9, 0)));
               }
               if ( ( ( context.localUtil.CToN( cgiGet( edtPlanidFinanciero_Internalname), ".", ",") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtPlanidFinanciero_Internalname), ".", ",") > Convert.ToDecimal( 999999999 )) ) )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "PLANIDFINANCIERO");
                  AnyError = 1;
                  GX_FocusControl = edtPlanidFinanciero_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  wbErr = true;
                  A34PlanidFinanciero = 0;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A34PlanidFinanciero", StringUtil.LTrim( StringUtil.Str( (decimal)(A34PlanidFinanciero), 9, 0)));
               }
               else
               {
                  A34PlanidFinanciero = (int)(context.localUtil.CToN( cgiGet( edtPlanidFinanciero_Internalname), ".", ","));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A34PlanidFinanciero", StringUtil.LTrim( StringUtil.Str( (decimal)(A34PlanidFinanciero), 9, 0)));
               }
               if ( ( ( context.localUtil.CToN( cgiGet( edtPlanidTecnico_Internalname), ".", ",") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtPlanidTecnico_Internalname), ".", ",") > Convert.ToDecimal( 999999999 )) ) )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "PLANIDTECNICO");
                  AnyError = 1;
                  GX_FocusControl = edtPlanidTecnico_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  wbErr = true;
                  A35PlanidTecnico = 0;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A35PlanidTecnico", StringUtil.LTrim( StringUtil.Str( (decimal)(A35PlanidTecnico), 9, 0)));
               }
               else
               {
                  A35PlanidTecnico = (int)(context.localUtil.CToN( cgiGet( edtPlanidTecnico_Internalname), ".", ","));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A35PlanidTecnico", StringUtil.LTrim( StringUtil.Str( (decimal)(A35PlanidTecnico), 9, 0)));
               }
               A36objGeneral = cgiGet( edtobjGeneral_Internalname);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A36objGeneral", A36objGeneral);
               A37objEspecificos = cgiGet( edtobjEspecificos_Internalname);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A37objEspecificos", A37objEspecificos);
               A38descripcionActividad = cgiGet( edtdescripcionActividad_Internalname);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A38descripcionActividad", A38descripcionActividad);
               if ( ( ( context.localUtil.CToN( cgiGet( edtEstadoid_Internalname), ".", ",") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtEstadoid_Internalname), ".", ",") > Convert.ToDecimal( 999999999 )) ) )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "ESTADOID");
                  AnyError = 1;
                  GX_FocusControl = edtEstadoid_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  wbErr = true;
                  A12Estadoid = 0;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A12Estadoid", StringUtil.LTrim( StringUtil.Str( (decimal)(A12Estadoid), 9, 0)));
               }
               else
               {
                  A12Estadoid = (int)(context.localUtil.CToN( cgiGet( edtEstadoid_Internalname), ".", ","));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A12Estadoid", StringUtil.LTrim( StringUtil.Str( (decimal)(A12Estadoid), 9, 0)));
               }
               A61Estadonombre = cgiGet( edtEstadonombre_Internalname);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A61Estadonombre", A61Estadonombre);
               if ( ( ( context.localUtil.CToN( cgiGet( edtcantBeneficiarios_Internalname), ".", ",") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtcantBeneficiarios_Internalname), ".", ",") > Convert.ToDecimal( 999999999 )) ) )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "CANTBENEFICIARIOS");
                  AnyError = 1;
                  GX_FocusControl = edtcantBeneficiarios_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  wbErr = true;
                  A39cantBeneficiarios = 0;
                  n39cantBeneficiarios = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A39cantBeneficiarios", StringUtil.LTrim( StringUtil.Str( (decimal)(A39cantBeneficiarios), 9, 0)));
               }
               else
               {
                  A39cantBeneficiarios = (int)(context.localUtil.CToN( cgiGet( edtcantBeneficiarios_Internalname), ".", ","));
                  n39cantBeneficiarios = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A39cantBeneficiarios", StringUtil.LTrim( StringUtil.Str( (decimal)(A39cantBeneficiarios), 9, 0)));
               }
               n39cantBeneficiarios = ((0==A39cantBeneficiarios) ? true : false);
               if ( ( ( context.localUtil.CToN( cgiGet( edtActividadid_Internalname), ".", ",") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtActividadid_Internalname), ".", ",") > Convert.ToDecimal( 999999999 )) ) )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "ACTIVIDADID");
                  AnyError = 1;
                  GX_FocusControl = edtActividadid_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  wbErr = true;
                  A3Actividadid = 0;
                  n3Actividadid = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A3Actividadid", StringUtil.LTrim( StringUtil.Str( (decimal)(A3Actividadid), 9, 0)));
               }
               else
               {
                  A3Actividadid = (int)(context.localUtil.CToN( cgiGet( edtActividadid_Internalname), ".", ","));
                  n3Actividadid = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A3Actividadid", StringUtil.LTrim( StringUtil.Str( (decimal)(A3Actividadid), 9, 0)));
               }
               n3Actividadid = ((0==A3Actividadid) ? true : false);
               A48Actividaddescripcion = cgiGet( edtActividaddescripcion_Internalname);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A48Actividaddescripcion", A48Actividaddescripcion);
               /* Read saved values. */
               Z13Planid = (int)(context.localUtil.CToN( cgiGet( "Z13Planid"), ".", ","));
               Z30nombreEvento = cgiGet( "Z30nombreEvento");
               Z31inicio = context.localUtil.CToD( cgiGet( "Z31inicio"), 0);
               Z32fin = context.localUtil.CToD( cgiGet( "Z32fin"), 0);
               Z36objGeneral = cgiGet( "Z36objGeneral");
               Z37objEspecificos = cgiGet( "Z37objEspecificos");
               Z38descripcionActividad = cgiGet( "Z38descripcionActividad");
               Z39cantBeneficiarios = (int)(context.localUtil.CToN( cgiGet( "Z39cantBeneficiarios"), ".", ","));
               n39cantBeneficiarios = ((0==A39cantBeneficiarios) ? true : false);
               Z40informe = (short)(context.localUtil.CToN( cgiGet( "Z40informe"), ".", ","));
               Z19Comunidadid = (int)(context.localUtil.CToN( cgiGet( "Z19Comunidadid"), ".", ","));
               Z22TipoBeneficiarioid = (int)(context.localUtil.CToN( cgiGet( "Z22TipoBeneficiarioid"), ".", ","));
               Z12Estadoid = (int)(context.localUtil.CToN( cgiGet( "Z12Estadoid"), ".", ","));
               Z3Actividadid = (int)(context.localUtil.CToN( cgiGet( "Z3Actividadid"), ".", ","));
               n3Actividadid = ((0==A3Actividadid) ? true : false);
               Z29PlanidProyecto = (int)(context.localUtil.CToN( cgiGet( "Z29PlanidProyecto"), ".", ","));
               Z33PlanidResponsable = (int)(context.localUtil.CToN( cgiGet( "Z33PlanidResponsable"), ".", ","));
               Z34PlanidFinanciero = (int)(context.localUtil.CToN( cgiGet( "Z34PlanidFinanciero"), ".", ","));
               Z35PlanidTecnico = (int)(context.localUtil.CToN( cgiGet( "Z35PlanidTecnico"), ".", ","));
               A40informe = (short)(context.localUtil.CToN( cgiGet( "Z40informe"), ".", ","));
               IsConfirmed = (short)(context.localUtil.CToN( cgiGet( "IsConfirmed"), ".", ","));
               IsModified = (short)(context.localUtil.CToN( cgiGet( "IsModified"), ".", ","));
               Gx_mode = cgiGet( "Mode");
               N29PlanidProyecto = (int)(context.localUtil.CToN( cgiGet( "N29PlanidProyecto"), ".", ","));
               N19Comunidadid = (int)(context.localUtil.CToN( cgiGet( "N19Comunidadid"), ".", ","));
               N22TipoBeneficiarioid = (int)(context.localUtil.CToN( cgiGet( "N22TipoBeneficiarioid"), ".", ","));
               N33PlanidResponsable = (int)(context.localUtil.CToN( cgiGet( "N33PlanidResponsable"), ".", ","));
               N34PlanidFinanciero = (int)(context.localUtil.CToN( cgiGet( "N34PlanidFinanciero"), ".", ","));
               N35PlanidTecnico = (int)(context.localUtil.CToN( cgiGet( "N35PlanidTecnico"), ".", ","));
               N12Estadoid = (int)(context.localUtil.CToN( cgiGet( "N12Estadoid"), ".", ","));
               N3Actividadid = (int)(context.localUtil.CToN( cgiGet( "N3Actividadid"), ".", ","));
               n3Actividadid = ((0==A3Actividadid) ? true : false);
               AV7Planid = (int)(context.localUtil.CToN( cgiGet( "vPLANID"), ".", ","));
               A13Planid = (int)(context.localUtil.CToN( cgiGet( "PLANID"), ".", ","));
               AV11Insert_PlanidProyecto = (int)(context.localUtil.CToN( cgiGet( "vINSERT_PLANIDPROYECTO"), ".", ","));
               AV12Insert_Comunidadid = (int)(context.localUtil.CToN( cgiGet( "vINSERT_COMUNIDADID"), ".", ","));
               AV13Insert_TipoBeneficiarioid = (int)(context.localUtil.CToN( cgiGet( "vINSERT_TIPOBENEFICIARIOID"), ".", ","));
               AV14Insert_PlanidResponsable = (int)(context.localUtil.CToN( cgiGet( "vINSERT_PLANIDRESPONSABLE"), ".", ","));
               AV15Insert_PlanidFinanciero = (int)(context.localUtil.CToN( cgiGet( "vINSERT_PLANIDFINANCIERO"), ".", ","));
               AV16Insert_PlanidTecnico = (int)(context.localUtil.CToN( cgiGet( "vINSERT_PLANIDTECNICO"), ".", ","));
               AV17Insert_Estadoid = (int)(context.localUtil.CToN( cgiGet( "vINSERT_ESTADOID"), ".", ","));
               AV18Insert_Actividadid = (int)(context.localUtil.CToN( cgiGet( "vINSERT_ACTIVIDADID"), ".", ","));
               A40informe = (short)(context.localUtil.CToN( cgiGet( "INFORME"), ".", ","));
               AV20Pgmname = cgiGet( "vPGMNAME");
               Gx_mode = cgiGet( "vMODE");
               /* Read subfile selected row values. */
               /* Read hidden variables. */
               GXKey = Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
               forbiddenHiddens = "hsh" + "Plan";
               forbiddenHiddens = forbiddenHiddens + context.localUtil.Format( (decimal)(AV11Insert_PlanidProyecto), "ZZZZZZZZ9");
               forbiddenHiddens = forbiddenHiddens + context.localUtil.Format( (decimal)(AV12Insert_Comunidadid), "ZZZZZZZZ9");
               forbiddenHiddens = forbiddenHiddens + context.localUtil.Format( (decimal)(AV13Insert_TipoBeneficiarioid), "ZZZZZZZZ9");
               forbiddenHiddens = forbiddenHiddens + context.localUtil.Format( (decimal)(AV14Insert_PlanidResponsable), "ZZZZZZZZ9");
               forbiddenHiddens = forbiddenHiddens + context.localUtil.Format( (decimal)(AV15Insert_PlanidFinanciero), "ZZZZZZZZ9");
               forbiddenHiddens = forbiddenHiddens + context.localUtil.Format( (decimal)(AV16Insert_PlanidTecnico), "ZZZZZZZZ9");
               forbiddenHiddens = forbiddenHiddens + context.localUtil.Format( (decimal)(AV17Insert_Estadoid), "ZZZZZZZZ9");
               forbiddenHiddens = forbiddenHiddens + context.localUtil.Format( (decimal)(AV18Insert_Actividadid), "ZZZZZZZZ9");
               forbiddenHiddens = forbiddenHiddens + StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"));
               forbiddenHiddens = forbiddenHiddens + context.localUtil.Format( (decimal)(A13Planid), "ZZZZZZZZ9");
               forbiddenHiddens = forbiddenHiddens + context.localUtil.Format( (decimal)(A40informe), "9");
               hsh = cgiGet( "hsh");
               if ( ! GXUtil.CheckEncryptedHash( forbiddenHiddens, hsh, GXKey) )
               {
                  GXUtil.WriteLog("plan:[SecurityCheckFailed value for]"+"Insert_PlanidProyecto:"+context.localUtil.Format( (decimal)(AV11Insert_PlanidProyecto), "ZZZZZZZZ9"));
                  GXUtil.WriteLog("plan:[SecurityCheckFailed value for]"+"Insert_Comunidadid:"+context.localUtil.Format( (decimal)(AV12Insert_Comunidadid), "ZZZZZZZZ9"));
                  GXUtil.WriteLog("plan:[SecurityCheckFailed value for]"+"Insert_TipoBeneficiarioid:"+context.localUtil.Format( (decimal)(AV13Insert_TipoBeneficiarioid), "ZZZZZZZZ9"));
                  GXUtil.WriteLog("plan:[SecurityCheckFailed value for]"+"Insert_PlanidResponsable:"+context.localUtil.Format( (decimal)(AV14Insert_PlanidResponsable), "ZZZZZZZZ9"));
                  GXUtil.WriteLog("plan:[SecurityCheckFailed value for]"+"Insert_PlanidFinanciero:"+context.localUtil.Format( (decimal)(AV15Insert_PlanidFinanciero), "ZZZZZZZZ9"));
                  GXUtil.WriteLog("plan:[SecurityCheckFailed value for]"+"Insert_PlanidTecnico:"+context.localUtil.Format( (decimal)(AV16Insert_PlanidTecnico), "ZZZZZZZZ9"));
                  GXUtil.WriteLog("plan:[SecurityCheckFailed value for]"+"Insert_Estadoid:"+context.localUtil.Format( (decimal)(AV17Insert_Estadoid), "ZZZZZZZZ9"));
                  GXUtil.WriteLog("plan:[SecurityCheckFailed value for]"+"Insert_Actividadid:"+context.localUtil.Format( (decimal)(AV18Insert_Actividadid), "ZZZZZZZZ9"));
                  GXUtil.WriteLog("plan:[SecurityCheckFailed value for]"+"Gx_mode:"+StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!")));
                  GXUtil.WriteLog("plan:[SecurityCheckFailed value for]"+"Planid:"+context.localUtil.Format( (decimal)(A13Planid), "ZZZZZZZZ9"));
                  GXUtil.WriteLog("plan:[SecurityCheckFailed value for]"+"informe:"+context.localUtil.Format( (decimal)(A40informe), "9"));
                  GxWebError = 1;
                  context.HttpContext.Response.StatusDescription = 403.ToString();
                  context.HttpContext.Response.StatusCode = 403;
                  context.WriteHtmlText( "<title>403 Forbidden</title>") ;
                  context.WriteHtmlText( "<h1>403 Forbidden</h1>") ;
                  context.WriteHtmlText( "<p /><hr />") ;
                  GXUtil.WriteLog("send_http_error_code " + 403.ToString());
                  AnyError = 1;
                  return  ;
               }
               standaloneNotModal( ) ;
            }
            else
            {
               standaloneNotModal( ) ;
               if ( StringUtil.StrCmp(gxfirstwebparm, "viewer") == 0 )
               {
                  Gx_mode = "DSP";
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!")), context));
                  A13Planid = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A13Planid", StringUtil.LTrim( StringUtil.Str( (decimal)(A13Planid), 9, 0)));
                  getEqualNoModal( ) ;
                  Gx_mode = "DSP";
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!")), context));
                  disable_std_buttons( ) ;
                  standaloneModal( ) ;
               }
               else
               {
                  if ( StringUtil.StrCmp(Gx_mode, "DSP") == 0 )
                  {
                     sMode16 = Gx_mode;
                     Gx_mode = "UPD";
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!")), context));
                     Gx_mode = sMode16;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!")), context));
                  }
                  standaloneModal( ) ;
                  if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
                  {
                     getByPrimaryKey( ) ;
                     if ( RcdFound16 == 1 )
                     {
                        if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
                        {
                           /* Confirm record */
                           CONFIRM_0G0( ) ;
                           if ( AnyError == 0 )
                           {
                              GX_FocusControl = bttBtn_enter_Internalname;
                              context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                           }
                        }
                     }
                     else
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_noinsert", ""), 1, "");
                        AnyError = 1;
                     }
                  }
               }
            }
         }
      }

      protected void Process( )
      {
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            /* Read Transaction buttons. */
            sEvt = cgiGet( "_EventName");
            EvtGridId = cgiGet( "_EventGridId");
            EvtRowId = cgiGet( "_EventRowId");
            if ( StringUtil.Len( sEvt) > 0 )
            {
               sEvtType = StringUtil.Left( sEvt, 1);
               sEvt = StringUtil.Right( sEvt, (short)(StringUtil.Len( sEvt)-1));
               if ( StringUtil.StrCmp(sEvtType, "M") != 0 )
               {
                  if ( StringUtil.StrCmp(sEvtType, "E") == 0 )
                  {
                     sEvtType = StringUtil.Right( sEvt, 1);
                     if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                     {
                        sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                        if ( StringUtil.StrCmp(sEvt, "START") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: Start */
                           E110G2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "AFTER TRN") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: After Trn */
                           E120G2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "ENTER") == 0 )
                        {
                           context.wbHandled = 1;
                           if ( StringUtil.StrCmp(Gx_mode, "DSP") != 0 )
                           {
                              btn_enter( ) ;
                           }
                           /* No code required for Cancel button. It is implemented as the Reset button. */
                        }
                     }
                     else
                     {
                     }
                  }
                  context.wbHandled = 1;
               }
            }
         }
      }

      protected void AfterTrn( )
      {
         if ( trnEnded == 1 )
         {
            /* Execute user event: After Trn */
            E120G2 ();
            trnEnded = 0;
            standaloneNotModal( ) ;
            standaloneModal( ) ;
            if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  )
            {
               /* Clear variables for new insertion. */
               InitAll0G16( ) ;
               standaloneNotModal( ) ;
               standaloneModal( ) ;
            }
         }
      }

      public override String ToString( )
      {
         return "" ;
      }

      public GxContentInfo GetContentInfo( )
      {
         return (GxContentInfo)(null) ;
      }

      protected void disable_std_buttons( )
      {
         bttBtn_delete_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_delete_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_delete_Visible), 5, 0)), true);
         bttBtn_first_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_first_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_first_Visible), 5, 0)), true);
         bttBtn_previous_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_previous_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_previous_Visible), 5, 0)), true);
         bttBtn_next_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_next_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_next_Visible), 5, 0)), true);
         bttBtn_last_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_last_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_last_Visible), 5, 0)), true);
         bttBtn_select_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_select_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_select_Visible), 5, 0)), true);
         if ( ( StringUtil.StrCmp(Gx_mode, "DSP") == 0 ) || ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 ) )
         {
            bttBtn_delete_Visible = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_delete_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_delete_Visible), 5, 0)), true);
            if ( StringUtil.StrCmp(Gx_mode, "DSP") == 0 )
            {
               bttBtn_enter_Visible = 0;
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_enter_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_enter_Visible), 5, 0)), true);
            }
            DisableAttributes0G16( ) ;
         }
      }

      protected void set_caption( )
      {
         if ( ( IsConfirmed == 1 ) && ( AnyError == 0 ) )
         {
            if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_confdelete", ""), 0, "", true);
            }
            else
            {
               GX_msglist.addItem(context.GetMessage( "GXM_mustconfirm", ""), 0, "", true);
            }
         }
      }

      protected void CONFIRM_0G0( )
      {
         BeforeValidate0G16( ) ;
         if ( AnyError == 0 )
         {
            if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
            {
               OnDeleteControls0G16( ) ;
            }
            else
            {
               CheckExtendedTable0G16( ) ;
               CloseExtendedTableCursors0G16( ) ;
            }
         }
         if ( AnyError == 0 )
         {
            IsConfirmed = 1;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "IsConfirmed", StringUtil.LTrim( StringUtil.Str( (decimal)(IsConfirmed), 4, 0)));
         }
      }

      protected void ResetCaption0G0( )
      {
      }

      protected void E110G2( )
      {
         /* Start Routine */
         if ( ! new isauthorized(context).executeUdp(  AV20Pgmname) )
         {
            CallWebObject(formatLink("notauthorized.aspx") + "?" + UrlEncode(StringUtil.RTrim(AV20Pgmname)));
            context.wjLocDisableFrm = 1;
         }
         AV9TrnContext.FromXml(AV10WebSession.Get("TrnContext"), null, "TransactionContext", "SAPWEB08");
         AV11Insert_PlanidProyecto = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV11Insert_PlanidProyecto", StringUtil.LTrim( StringUtil.Str( (decimal)(AV11Insert_PlanidProyecto), 9, 0)));
         AV12Insert_Comunidadid = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV12Insert_Comunidadid", StringUtil.LTrim( StringUtil.Str( (decimal)(AV12Insert_Comunidadid), 9, 0)));
         AV13Insert_TipoBeneficiarioid = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13Insert_TipoBeneficiarioid", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13Insert_TipoBeneficiarioid), 9, 0)));
         AV14Insert_PlanidResponsable = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14Insert_PlanidResponsable", StringUtil.LTrim( StringUtil.Str( (decimal)(AV14Insert_PlanidResponsable), 9, 0)));
         AV15Insert_PlanidFinanciero = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15Insert_PlanidFinanciero", StringUtil.LTrim( StringUtil.Str( (decimal)(AV15Insert_PlanidFinanciero), 9, 0)));
         AV16Insert_PlanidTecnico = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16Insert_PlanidTecnico", StringUtil.LTrim( StringUtil.Str( (decimal)(AV16Insert_PlanidTecnico), 9, 0)));
         AV17Insert_Estadoid = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17Insert_Estadoid", StringUtil.LTrim( StringUtil.Str( (decimal)(AV17Insert_Estadoid), 9, 0)));
         AV18Insert_Actividadid = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18Insert_Actividadid", StringUtil.LTrim( StringUtil.Str( (decimal)(AV18Insert_Actividadid), 9, 0)));
         if ( ( StringUtil.StrCmp(AV9TrnContext.gxTpr_Transactionname, AV20Pgmname) == 0 ) && ( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) )
         {
            AV21GXV1 = 1;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21GXV1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV21GXV1), 8, 0)));
            while ( AV21GXV1 <= AV9TrnContext.gxTpr_Attributes.Count )
            {
               AV19TrnContextAtt = ((SdtTransactionContext_Attribute)AV9TrnContext.gxTpr_Attributes.Item(AV21GXV1));
               if ( StringUtil.StrCmp(AV19TrnContextAtt.gxTpr_Attributename, "PlanidProyecto") == 0 )
               {
                  AV11Insert_PlanidProyecto = (int)(NumberUtil.Val( AV19TrnContextAtt.gxTpr_Attributevalue, "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV11Insert_PlanidProyecto", StringUtil.LTrim( StringUtil.Str( (decimal)(AV11Insert_PlanidProyecto), 9, 0)));
               }
               else if ( StringUtil.StrCmp(AV19TrnContextAtt.gxTpr_Attributename, "Comunidadid") == 0 )
               {
                  AV12Insert_Comunidadid = (int)(NumberUtil.Val( AV19TrnContextAtt.gxTpr_Attributevalue, "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV12Insert_Comunidadid", StringUtil.LTrim( StringUtil.Str( (decimal)(AV12Insert_Comunidadid), 9, 0)));
               }
               else if ( StringUtil.StrCmp(AV19TrnContextAtt.gxTpr_Attributename, "TipoBeneficiarioid") == 0 )
               {
                  AV13Insert_TipoBeneficiarioid = (int)(NumberUtil.Val( AV19TrnContextAtt.gxTpr_Attributevalue, "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13Insert_TipoBeneficiarioid", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13Insert_TipoBeneficiarioid), 9, 0)));
               }
               else if ( StringUtil.StrCmp(AV19TrnContextAtt.gxTpr_Attributename, "PlanidResponsable") == 0 )
               {
                  AV14Insert_PlanidResponsable = (int)(NumberUtil.Val( AV19TrnContextAtt.gxTpr_Attributevalue, "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14Insert_PlanidResponsable", StringUtil.LTrim( StringUtil.Str( (decimal)(AV14Insert_PlanidResponsable), 9, 0)));
               }
               else if ( StringUtil.StrCmp(AV19TrnContextAtt.gxTpr_Attributename, "PlanidFinanciero") == 0 )
               {
                  AV15Insert_PlanidFinanciero = (int)(NumberUtil.Val( AV19TrnContextAtt.gxTpr_Attributevalue, "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15Insert_PlanidFinanciero", StringUtil.LTrim( StringUtil.Str( (decimal)(AV15Insert_PlanidFinanciero), 9, 0)));
               }
               else if ( StringUtil.StrCmp(AV19TrnContextAtt.gxTpr_Attributename, "PlanidTecnico") == 0 )
               {
                  AV16Insert_PlanidTecnico = (int)(NumberUtil.Val( AV19TrnContextAtt.gxTpr_Attributevalue, "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16Insert_PlanidTecnico", StringUtil.LTrim( StringUtil.Str( (decimal)(AV16Insert_PlanidTecnico), 9, 0)));
               }
               else if ( StringUtil.StrCmp(AV19TrnContextAtt.gxTpr_Attributename, "Estadoid") == 0 )
               {
                  AV17Insert_Estadoid = (int)(NumberUtil.Val( AV19TrnContextAtt.gxTpr_Attributevalue, "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17Insert_Estadoid", StringUtil.LTrim( StringUtil.Str( (decimal)(AV17Insert_Estadoid), 9, 0)));
               }
               else if ( StringUtil.StrCmp(AV19TrnContextAtt.gxTpr_Attributename, "Actividadid") == 0 )
               {
                  AV18Insert_Actividadid = (int)(NumberUtil.Val( AV19TrnContextAtt.gxTpr_Attributevalue, "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18Insert_Actividadid", StringUtil.LTrim( StringUtil.Str( (decimal)(AV18Insert_Actividadid), 9, 0)));
               }
               AV21GXV1 = (int)(AV21GXV1+1);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21GXV1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV21GXV1), 8, 0)));
            }
         }
      }

      protected void E120G2( )
      {
         /* After Trn Routine */
         if ( ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 ) && ! AV9TrnContext.gxTpr_Callerondelete )
         {
            CallWebObject(formatLink("wwplan.aspx") );
            context.wjLocDisableFrm = 1;
         }
         context.setWebReturnParms(new Object[] {});
         context.setWebReturnParmsMetadata(new Object[] {});
         context.wjLocDisableFrm = 1;
         context.nUserReturn = 1;
         returnInSub = true;
         if (true) return;
      }

      protected void ZM0G16( short GX_JID )
      {
         if ( ( GX_JID == 30 ) || ( GX_JID == 0 ) )
         {
            if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
            {
               Z30nombreEvento = T000G3_A30nombreEvento[0];
               Z31inicio = T000G3_A31inicio[0];
               Z32fin = T000G3_A32fin[0];
               Z36objGeneral = T000G3_A36objGeneral[0];
               Z37objEspecificos = T000G3_A37objEspecificos[0];
               Z38descripcionActividad = T000G3_A38descripcionActividad[0];
               Z39cantBeneficiarios = T000G3_A39cantBeneficiarios[0];
               Z40informe = T000G3_A40informe[0];
               Z19Comunidadid = T000G3_A19Comunidadid[0];
               Z22TipoBeneficiarioid = T000G3_A22TipoBeneficiarioid[0];
               Z12Estadoid = T000G3_A12Estadoid[0];
               Z3Actividadid = T000G3_A3Actividadid[0];
               Z29PlanidProyecto = T000G3_A29PlanidProyecto[0];
               Z33PlanidResponsable = T000G3_A33PlanidResponsable[0];
               Z34PlanidFinanciero = T000G3_A34PlanidFinanciero[0];
               Z35PlanidTecnico = T000G3_A35PlanidTecnico[0];
            }
            else
            {
               Z30nombreEvento = A30nombreEvento;
               Z31inicio = A31inicio;
               Z32fin = A32fin;
               Z36objGeneral = A36objGeneral;
               Z37objEspecificos = A37objEspecificos;
               Z38descripcionActividad = A38descripcionActividad;
               Z39cantBeneficiarios = A39cantBeneficiarios;
               Z40informe = A40informe;
               Z19Comunidadid = A19Comunidadid;
               Z22TipoBeneficiarioid = A22TipoBeneficiarioid;
               Z12Estadoid = A12Estadoid;
               Z3Actividadid = A3Actividadid;
               Z29PlanidProyecto = A29PlanidProyecto;
               Z33PlanidResponsable = A33PlanidResponsable;
               Z34PlanidFinanciero = A34PlanidFinanciero;
               Z35PlanidTecnico = A35PlanidTecnico;
            }
         }
         if ( GX_JID == -30 )
         {
            Z13Planid = A13Planid;
            Z30nombreEvento = A30nombreEvento;
            Z31inicio = A31inicio;
            Z32fin = A32fin;
            Z36objGeneral = A36objGeneral;
            Z37objEspecificos = A37objEspecificos;
            Z38descripcionActividad = A38descripcionActividad;
            Z39cantBeneficiarios = A39cantBeneficiarios;
            Z40informe = A40informe;
            Z19Comunidadid = A19Comunidadid;
            Z22TipoBeneficiarioid = A22TipoBeneficiarioid;
            Z12Estadoid = A12Estadoid;
            Z3Actividadid = A3Actividadid;
            Z29PlanidProyecto = A29PlanidProyecto;
            Z33PlanidResponsable = A33PlanidResponsable;
            Z34PlanidFinanciero = A34PlanidFinanciero;
            Z35PlanidTecnico = A35PlanidTecnico;
            Z130Comunidadnombre = A130Comunidadnombre;
            Z61Estadonombre = A61Estadonombre;
            Z48Actividaddescripcion = A48Actividaddescripcion;
         }
      }

      protected void standaloneNotModal( )
      {
         imgprompt_29_Link = ((StringUtil.StrCmp(Gx_mode, "DSP")==0) ? "" : "javascript:"+"gx.popup.openPrompt('"+"gx00p0.aspx"+"',["+"{Ctrl:gx.dom.el('"+"PLANIDPROYECTO"+"'), id:'"+"PLANIDPROYECTO"+"'"+",IOType:'out'}"+"],"+"null"+","+"'', false"+","+"false"+");");
         imgprompt_19_Link = ((StringUtil.StrCmp(Gx_mode, "DSP")==0) ? "" : "javascript:"+"gx.popup.openPrompt('"+"gx00f0.aspx"+"',["+"{Ctrl:gx.dom.el('"+"COMUNIDADID"+"'), id:'"+"COMUNIDADID"+"'"+",IOType:'out'}"+"],"+"null"+","+"'', false"+","+"false"+");");
         imgprompt_22_Link = ((StringUtil.StrCmp(Gx_mode, "DSP")==0) ? "" : "javascript:"+"gx.popup.openPrompt('"+"gx00l0.aspx"+"',["+"{Ctrl:gx.dom.el('"+"TIPOBENEFICIARIOID"+"'), id:'"+"TIPOBENEFICIARIOID"+"'"+",IOType:'out'}"+"],"+"null"+","+"'', false"+","+"false"+");");
         imgprompt_33_Link = ((StringUtil.StrCmp(Gx_mode, "DSP")==0) ? "" : "javascript:"+"gx.popup.openPrompt('"+"gx00m0.aspx"+"',["+"{Ctrl:gx.dom.el('"+"PLANIDRESPONSABLE"+"'), id:'"+"PLANIDRESPONSABLE"+"'"+",IOType:'out'}"+"],"+"null"+","+"'', false"+","+"false"+");");
         imgprompt_34_Link = ((StringUtil.StrCmp(Gx_mode, "DSP")==0) ? "" : "javascript:"+"gx.popup.openPrompt('"+"gx00m0.aspx"+"',["+"{Ctrl:gx.dom.el('"+"PLANIDFINANCIERO"+"'), id:'"+"PLANIDFINANCIERO"+"'"+",IOType:'out'}"+"],"+"null"+","+"'', false"+","+"false"+");");
         imgprompt_35_Link = ((StringUtil.StrCmp(Gx_mode, "DSP")==0) ? "" : "javascript:"+"gx.popup.openPrompt('"+"gx00m0.aspx"+"',["+"{Ctrl:gx.dom.el('"+"PLANIDTECNICO"+"'), id:'"+"PLANIDTECNICO"+"'"+",IOType:'out'}"+"],"+"null"+","+"'', false"+","+"false"+");");
         imgprompt_12_Link = ((StringUtil.StrCmp(Gx_mode, "DSP")==0) ? "" : "javascript:"+"gx.popup.openPrompt('"+"gx00h0.aspx"+"',["+"{Ctrl:gx.dom.el('"+"ESTADOID"+"'), id:'"+"ESTADOID"+"'"+",IOType:'out'}"+"],"+"null"+","+"'', false"+","+"false"+");");
         imgprompt_3_Link = ((StringUtil.StrCmp(Gx_mode, "DSP")==0) ? "" : "javascript:"+"gx.popup.openPrompt('"+"gx0020.aspx"+"',["+"{Ctrl:gx.dom.el('"+"ACTIVIDADID"+"'), id:'"+"ACTIVIDADID"+"'"+",IOType:'out'}"+"],"+"null"+","+"'', false"+","+"false"+");");
         bttBtn_delete_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_delete_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_delete_Enabled), 5, 0)), true);
         if ( ! (0==AV7Planid) )
         {
            A13Planid = AV7Planid;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A13Planid", StringUtil.LTrim( StringUtil.Str( (decimal)(A13Planid), 9, 0)));
         }
         if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) && ! (0==AV11Insert_PlanidProyecto) )
         {
            edtPlanidProyecto_Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtPlanidProyecto_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtPlanidProyecto_Enabled), 5, 0)), true);
         }
         else
         {
            edtPlanidProyecto_Enabled = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtPlanidProyecto_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtPlanidProyecto_Enabled), 5, 0)), true);
         }
         if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) && ! (0==AV12Insert_Comunidadid) )
         {
            edtComunidadid_Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtComunidadid_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtComunidadid_Enabled), 5, 0)), true);
         }
         else
         {
            edtComunidadid_Enabled = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtComunidadid_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtComunidadid_Enabled), 5, 0)), true);
         }
         if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) && ! (0==AV13Insert_TipoBeneficiarioid) )
         {
            edtTipoBeneficiarioid_Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtTipoBeneficiarioid_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtTipoBeneficiarioid_Enabled), 5, 0)), true);
         }
         else
         {
            edtTipoBeneficiarioid_Enabled = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtTipoBeneficiarioid_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtTipoBeneficiarioid_Enabled), 5, 0)), true);
         }
         if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) && ! (0==AV14Insert_PlanidResponsable) )
         {
            edtPlanidResponsable_Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtPlanidResponsable_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtPlanidResponsable_Enabled), 5, 0)), true);
         }
         else
         {
            edtPlanidResponsable_Enabled = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtPlanidResponsable_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtPlanidResponsable_Enabled), 5, 0)), true);
         }
         if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) && ! (0==AV15Insert_PlanidFinanciero) )
         {
            edtPlanidFinanciero_Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtPlanidFinanciero_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtPlanidFinanciero_Enabled), 5, 0)), true);
         }
         else
         {
            edtPlanidFinanciero_Enabled = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtPlanidFinanciero_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtPlanidFinanciero_Enabled), 5, 0)), true);
         }
         if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) && ! (0==AV16Insert_PlanidTecnico) )
         {
            edtPlanidTecnico_Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtPlanidTecnico_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtPlanidTecnico_Enabled), 5, 0)), true);
         }
         else
         {
            edtPlanidTecnico_Enabled = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtPlanidTecnico_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtPlanidTecnico_Enabled), 5, 0)), true);
         }
         if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) && ! (0==AV17Insert_Estadoid) )
         {
            edtEstadoid_Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtEstadoid_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtEstadoid_Enabled), 5, 0)), true);
         }
         else
         {
            edtEstadoid_Enabled = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtEstadoid_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtEstadoid_Enabled), 5, 0)), true);
         }
         if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) && ! (0==AV18Insert_Actividadid) )
         {
            edtActividadid_Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtActividadid_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtActividadid_Enabled), 5, 0)), true);
         }
         else
         {
            edtActividadid_Enabled = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtActividadid_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtActividadid_Enabled), 5, 0)), true);
         }
      }

      protected void standaloneModal( )
      {
         if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) && ! (0==AV18Insert_Actividadid) )
         {
            A3Actividadid = AV18Insert_Actividadid;
            n3Actividadid = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A3Actividadid", StringUtil.LTrim( StringUtil.Str( (decimal)(A3Actividadid), 9, 0)));
         }
         if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) && ! (0==AV17Insert_Estadoid) )
         {
            A12Estadoid = AV17Insert_Estadoid;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A12Estadoid", StringUtil.LTrim( StringUtil.Str( (decimal)(A12Estadoid), 9, 0)));
         }
         if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) && ! (0==AV16Insert_PlanidTecnico) )
         {
            A35PlanidTecnico = AV16Insert_PlanidTecnico;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A35PlanidTecnico", StringUtil.LTrim( StringUtil.Str( (decimal)(A35PlanidTecnico), 9, 0)));
         }
         if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) && ! (0==AV15Insert_PlanidFinanciero) )
         {
            A34PlanidFinanciero = AV15Insert_PlanidFinanciero;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A34PlanidFinanciero", StringUtil.LTrim( StringUtil.Str( (decimal)(A34PlanidFinanciero), 9, 0)));
         }
         if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) && ! (0==AV14Insert_PlanidResponsable) )
         {
            A33PlanidResponsable = AV14Insert_PlanidResponsable;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A33PlanidResponsable", StringUtil.LTrim( StringUtil.Str( (decimal)(A33PlanidResponsable), 9, 0)));
         }
         if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) && ! (0==AV13Insert_TipoBeneficiarioid) )
         {
            A22TipoBeneficiarioid = AV13Insert_TipoBeneficiarioid;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A22TipoBeneficiarioid", StringUtil.LTrim( StringUtil.Str( (decimal)(A22TipoBeneficiarioid), 9, 0)));
         }
         if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) && ! (0==AV12Insert_Comunidadid) )
         {
            A19Comunidadid = AV12Insert_Comunidadid;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A19Comunidadid", StringUtil.LTrim( StringUtil.Str( (decimal)(A19Comunidadid), 9, 0)));
         }
         if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) && ! (0==AV11Insert_PlanidProyecto) )
         {
            A29PlanidProyecto = AV11Insert_PlanidProyecto;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A29PlanidProyecto", StringUtil.LTrim( StringUtil.Str( (decimal)(A29PlanidProyecto), 9, 0)));
         }
         if ( StringUtil.StrCmp(Gx_mode, "DSP") == 0 )
         {
            bttBtn_enter_Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_enter_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_enter_Enabled), 5, 0)), true);
         }
         else
         {
            bttBtn_enter_Enabled = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_enter_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_enter_Enabled), 5, 0)), true);
         }
         if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) && ( Gx_BScreen == 0 ) )
         {
            AV20Pgmname = "Plan";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20Pgmname", AV20Pgmname);
            /* Using cursor T000G7 */
            pr_datastore1.execute(5, new Object[] {n3Actividadid, A3Actividadid});
            A48Actividaddescripcion = T000G7_A48Actividaddescripcion[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A48Actividaddescripcion", A48Actividaddescripcion);
            pr_datastore1.close(5);
            /* Using cursor T000G6 */
            pr_datastore1.execute(4, new Object[] {A12Estadoid});
            A61Estadonombre = T000G6_A61Estadonombre[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A61Estadonombre", A61Estadonombre);
            pr_datastore1.close(4);
            /* Using cursor T000G4 */
            pr_datastore1.execute(2, new Object[] {A19Comunidadid});
            A130Comunidadnombre = T000G4_A130Comunidadnombre[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A130Comunidadnombre", A130Comunidadnombre);
            pr_datastore1.close(2);
         }
      }

      protected void Load0G16( )
      {
         /* Using cursor T000G12 */
         pr_datastore1.execute(10, new Object[] {A13Planid});
         if ( (pr_datastore1.getStatus(10) != 101) )
         {
            RcdFound16 = 1;
            A30nombreEvento = T000G12_A30nombreEvento[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A30nombreEvento", A30nombreEvento);
            A130Comunidadnombre = T000G12_A130Comunidadnombre[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A130Comunidadnombre", A130Comunidadnombre);
            A31inicio = T000G12_A31inicio[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A31inicio", context.localUtil.Format(A31inicio, "99/99/99"));
            A32fin = T000G12_A32fin[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A32fin", context.localUtil.Format(A32fin, "99/99/99"));
            A36objGeneral = T000G12_A36objGeneral[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A36objGeneral", A36objGeneral);
            A37objEspecificos = T000G12_A37objEspecificos[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A37objEspecificos", A37objEspecificos);
            A38descripcionActividad = T000G12_A38descripcionActividad[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A38descripcionActividad", A38descripcionActividad);
            A61Estadonombre = T000G12_A61Estadonombre[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A61Estadonombre", A61Estadonombre);
            A39cantBeneficiarios = T000G12_A39cantBeneficiarios[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A39cantBeneficiarios", StringUtil.LTrim( StringUtil.Str( (decimal)(A39cantBeneficiarios), 9, 0)));
            n39cantBeneficiarios = T000G12_n39cantBeneficiarios[0];
            A48Actividaddescripcion = T000G12_A48Actividaddescripcion[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A48Actividaddescripcion", A48Actividaddescripcion);
            A40informe = T000G12_A40informe[0];
            A19Comunidadid = T000G12_A19Comunidadid[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A19Comunidadid", StringUtil.LTrim( StringUtil.Str( (decimal)(A19Comunidadid), 9, 0)));
            A22TipoBeneficiarioid = T000G12_A22TipoBeneficiarioid[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A22TipoBeneficiarioid", StringUtil.LTrim( StringUtil.Str( (decimal)(A22TipoBeneficiarioid), 9, 0)));
            A12Estadoid = T000G12_A12Estadoid[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A12Estadoid", StringUtil.LTrim( StringUtil.Str( (decimal)(A12Estadoid), 9, 0)));
            A3Actividadid = T000G12_A3Actividadid[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A3Actividadid", StringUtil.LTrim( StringUtil.Str( (decimal)(A3Actividadid), 9, 0)));
            n3Actividadid = T000G12_n3Actividadid[0];
            A29PlanidProyecto = T000G12_A29PlanidProyecto[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A29PlanidProyecto", StringUtil.LTrim( StringUtil.Str( (decimal)(A29PlanidProyecto), 9, 0)));
            A33PlanidResponsable = T000G12_A33PlanidResponsable[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A33PlanidResponsable", StringUtil.LTrim( StringUtil.Str( (decimal)(A33PlanidResponsable), 9, 0)));
            A34PlanidFinanciero = T000G12_A34PlanidFinanciero[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A34PlanidFinanciero", StringUtil.LTrim( StringUtil.Str( (decimal)(A34PlanidFinanciero), 9, 0)));
            A35PlanidTecnico = T000G12_A35PlanidTecnico[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A35PlanidTecnico", StringUtil.LTrim( StringUtil.Str( (decimal)(A35PlanidTecnico), 9, 0)));
            ZM0G16( -30) ;
         }
         pr_datastore1.close(10);
         OnLoadActions0G16( ) ;
      }

      protected void OnLoadActions0G16( )
      {
         AV20Pgmname = "Plan";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20Pgmname", AV20Pgmname);
      }

      protected void CheckExtendedTable0G16( )
      {
         Gx_BScreen = 1;
         standaloneModal( ) ;
         AV20Pgmname = "Plan";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20Pgmname", AV20Pgmname);
         /* Using cursor T000G8 */
         pr_datastore1.execute(6, new Object[] {A29PlanidProyecto});
         if ( (pr_datastore1.getStatus(6) == 101) )
         {
            GX_msglist.addItem("No matching 'Plan Proyecto'.", "ForeignKeyNotFound", 1, "PLANIDPROYECTO");
            AnyError = 1;
            GX_FocusControl = edtPlanidProyecto_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         pr_datastore1.close(6);
         /* Using cursor T000G4 */
         pr_datastore1.execute(2, new Object[] {A19Comunidadid});
         if ( (pr_datastore1.getStatus(2) == 101) )
         {
            GX_msglist.addItem("No matching 'Comunidad'.", "ForeignKeyNotFound", 1, "COMUNIDADID");
            AnyError = 1;
            GX_FocusControl = edtComunidadid_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         A130Comunidadnombre = T000G4_A130Comunidadnombre[0];
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A130Comunidadnombre", A130Comunidadnombre);
         pr_datastore1.close(2);
         if ( ! ( (DateTime.MinValue==A31inicio) || ( A31inicio >= context.localUtil.YMDToD( 1753, 1, 1) ) ) )
         {
            GX_msglist.addItem("Field inicio is out of range", "OutOfRange", 1, "INICIO");
            AnyError = 1;
            GX_FocusControl = edtinicio_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         if ( ! ( (DateTime.MinValue==A32fin) || ( A32fin >= context.localUtil.YMDToD( 1753, 1, 1) ) ) )
         {
            GX_msglist.addItem("Field fin is out of range", "OutOfRange", 1, "FIN");
            AnyError = 1;
            GX_FocusControl = edtfin_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         /* Using cursor T000G5 */
         pr_datastore1.execute(3, new Object[] {A22TipoBeneficiarioid});
         if ( (pr_datastore1.getStatus(3) == 101) )
         {
            GX_msglist.addItem("No matching 'Tipo Beneficiario'.", "ForeignKeyNotFound", 1, "TIPOBENEFICIARIOID");
            AnyError = 1;
            GX_FocusControl = edtTipoBeneficiarioid_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         pr_datastore1.close(3);
         /* Using cursor T000G9 */
         pr_datastore1.execute(7, new Object[] {A33PlanidResponsable});
         if ( (pr_datastore1.getStatus(7) == 101) )
         {
            GX_msglist.addItem("No matching 'Plan Responsable'.", "ForeignKeyNotFound", 1, "PLANIDRESPONSABLE");
            AnyError = 1;
            GX_FocusControl = edtPlanidResponsable_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         pr_datastore1.close(7);
         /* Using cursor T000G10 */
         pr_datastore1.execute(8, new Object[] {A34PlanidFinanciero});
         if ( (pr_datastore1.getStatus(8) == 101) )
         {
            GX_msglist.addItem("No matching 'Plan Responsable2'.", "ForeignKeyNotFound", 1, "PLANIDFINANCIERO");
            AnyError = 1;
            GX_FocusControl = edtPlanidFinanciero_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         pr_datastore1.close(8);
         /* Using cursor T000G11 */
         pr_datastore1.execute(9, new Object[] {A35PlanidTecnico});
         if ( (pr_datastore1.getStatus(9) == 101) )
         {
            GX_msglist.addItem("No matching 'Plan Responsable3'.", "ForeignKeyNotFound", 1, "PLANIDTECNICO");
            AnyError = 1;
            GX_FocusControl = edtPlanidTecnico_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         pr_datastore1.close(9);
         /* Using cursor T000G6 */
         pr_datastore1.execute(4, new Object[] {A12Estadoid});
         if ( (pr_datastore1.getStatus(4) == 101) )
         {
            GX_msglist.addItem("No matching 'Estado'.", "ForeignKeyNotFound", 1, "ESTADOID");
            AnyError = 1;
            GX_FocusControl = edtEstadoid_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         A61Estadonombre = T000G6_A61Estadonombre[0];
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A61Estadonombre", A61Estadonombre);
         pr_datastore1.close(4);
         /* Using cursor T000G7 */
         pr_datastore1.execute(5, new Object[] {n3Actividadid, A3Actividadid});
         if ( (pr_datastore1.getStatus(5) == 101) )
         {
            if ( ! ( (0==A3Actividadid) ) )
            {
               GX_msglist.addItem("No matching 'Actividad'.", "ForeignKeyNotFound", 1, "ACTIVIDADID");
               AnyError = 1;
               GX_FocusControl = edtActividadid_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            }
         }
         A48Actividaddescripcion = T000G7_A48Actividaddescripcion[0];
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A48Actividaddescripcion", A48Actividaddescripcion);
         pr_datastore1.close(5);
      }

      protected void CloseExtendedTableCursors0G16( )
      {
         pr_datastore1.close(6);
         pr_datastore1.close(2);
         pr_datastore1.close(3);
         pr_datastore1.close(7);
         pr_datastore1.close(8);
         pr_datastore1.close(9);
         pr_datastore1.close(4);
         pr_datastore1.close(5);
      }

      protected void enableDisable( )
      {
      }

      protected void gxLoad_35( int A29PlanidProyecto )
      {
         /* Using cursor T000G13 */
         pr_datastore1.execute(11, new Object[] {A29PlanidProyecto});
         if ( (pr_datastore1.getStatus(11) == 101) )
         {
            GX_msglist.addItem("No matching 'Plan Proyecto'.", "ForeignKeyNotFound", 1, "PLANIDPROYECTO");
            AnyError = 1;
            GX_FocusControl = edtPlanidProyecto_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         GxWebStd.set_html_headers( context, 0, "", "");
         context.GX_webresponse.AddString("[[");
         context.GX_webresponse.AddString("");
         context.GX_webresponse.AddString("]");
         if ( (pr_datastore1.getStatus(11) == 101) )
         {
            context.GX_webresponse.AddString(",");
            context.GX_webresponse.AddString("101");
         }
         context.GX_webresponse.AddString("]");
         pr_datastore1.close(11);
      }

      protected void gxLoad_31( int A19Comunidadid )
      {
         /* Using cursor T000G14 */
         pr_datastore1.execute(12, new Object[] {A19Comunidadid});
         if ( (pr_datastore1.getStatus(12) == 101) )
         {
            GX_msglist.addItem("No matching 'Comunidad'.", "ForeignKeyNotFound", 1, "COMUNIDADID");
            AnyError = 1;
            GX_FocusControl = edtComunidadid_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         A130Comunidadnombre = T000G14_A130Comunidadnombre[0];
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A130Comunidadnombre", A130Comunidadnombre);
         GxWebStd.set_html_headers( context, 0, "", "");
         context.GX_webresponse.AddString("[[");
         context.GX_webresponse.AddString("\""+GXUtil.EncodeJSConstant( A130Comunidadnombre)+"\"");
         context.GX_webresponse.AddString("]");
         if ( (pr_datastore1.getStatus(12) == 101) )
         {
            context.GX_webresponse.AddString(",");
            context.GX_webresponse.AddString("101");
         }
         context.GX_webresponse.AddString("]");
         pr_datastore1.close(12);
      }

      protected void gxLoad_32( int A22TipoBeneficiarioid )
      {
         /* Using cursor T000G15 */
         pr_datastore1.execute(13, new Object[] {A22TipoBeneficiarioid});
         if ( (pr_datastore1.getStatus(13) == 101) )
         {
            GX_msglist.addItem("No matching 'Tipo Beneficiario'.", "ForeignKeyNotFound", 1, "TIPOBENEFICIARIOID");
            AnyError = 1;
            GX_FocusControl = edtTipoBeneficiarioid_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         GxWebStd.set_html_headers( context, 0, "", "");
         context.GX_webresponse.AddString("[[");
         context.GX_webresponse.AddString("");
         context.GX_webresponse.AddString("]");
         if ( (pr_datastore1.getStatus(13) == 101) )
         {
            context.GX_webresponse.AddString(",");
            context.GX_webresponse.AddString("101");
         }
         context.GX_webresponse.AddString("]");
         pr_datastore1.close(13);
      }

      protected void gxLoad_36( int A33PlanidResponsable )
      {
         /* Using cursor T000G16 */
         pr_datastore1.execute(14, new Object[] {A33PlanidResponsable});
         if ( (pr_datastore1.getStatus(14) == 101) )
         {
            GX_msglist.addItem("No matching 'Plan Responsable'.", "ForeignKeyNotFound", 1, "PLANIDRESPONSABLE");
            AnyError = 1;
            GX_FocusControl = edtPlanidResponsable_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         GxWebStd.set_html_headers( context, 0, "", "");
         context.GX_webresponse.AddString("[[");
         context.GX_webresponse.AddString("");
         context.GX_webresponse.AddString("]");
         if ( (pr_datastore1.getStatus(14) == 101) )
         {
            context.GX_webresponse.AddString(",");
            context.GX_webresponse.AddString("101");
         }
         context.GX_webresponse.AddString("]");
         pr_datastore1.close(14);
      }

      protected void gxLoad_37( int A34PlanidFinanciero )
      {
         /* Using cursor T000G17 */
         pr_datastore1.execute(15, new Object[] {A34PlanidFinanciero});
         if ( (pr_datastore1.getStatus(15) == 101) )
         {
            GX_msglist.addItem("No matching 'Plan Responsable2'.", "ForeignKeyNotFound", 1, "PLANIDFINANCIERO");
            AnyError = 1;
            GX_FocusControl = edtPlanidFinanciero_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         GxWebStd.set_html_headers( context, 0, "", "");
         context.GX_webresponse.AddString("[[");
         context.GX_webresponse.AddString("");
         context.GX_webresponse.AddString("]");
         if ( (pr_datastore1.getStatus(15) == 101) )
         {
            context.GX_webresponse.AddString(",");
            context.GX_webresponse.AddString("101");
         }
         context.GX_webresponse.AddString("]");
         pr_datastore1.close(15);
      }

      protected void gxLoad_38( int A35PlanidTecnico )
      {
         /* Using cursor T000G18 */
         pr_datastore1.execute(16, new Object[] {A35PlanidTecnico});
         if ( (pr_datastore1.getStatus(16) == 101) )
         {
            GX_msglist.addItem("No matching 'Plan Responsable3'.", "ForeignKeyNotFound", 1, "PLANIDTECNICO");
            AnyError = 1;
            GX_FocusControl = edtPlanidTecnico_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         GxWebStd.set_html_headers( context, 0, "", "");
         context.GX_webresponse.AddString("[[");
         context.GX_webresponse.AddString("");
         context.GX_webresponse.AddString("]");
         if ( (pr_datastore1.getStatus(16) == 101) )
         {
            context.GX_webresponse.AddString(",");
            context.GX_webresponse.AddString("101");
         }
         context.GX_webresponse.AddString("]");
         pr_datastore1.close(16);
      }

      protected void gxLoad_33( int A12Estadoid )
      {
         /* Using cursor T000G19 */
         pr_datastore1.execute(17, new Object[] {A12Estadoid});
         if ( (pr_datastore1.getStatus(17) == 101) )
         {
            GX_msglist.addItem("No matching 'Estado'.", "ForeignKeyNotFound", 1, "ESTADOID");
            AnyError = 1;
            GX_FocusControl = edtEstadoid_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         A61Estadonombre = T000G19_A61Estadonombre[0];
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A61Estadonombre", A61Estadonombre);
         GxWebStd.set_html_headers( context, 0, "", "");
         context.GX_webresponse.AddString("[[");
         context.GX_webresponse.AddString("\""+GXUtil.EncodeJSConstant( A61Estadonombre)+"\"");
         context.GX_webresponse.AddString("]");
         if ( (pr_datastore1.getStatus(17) == 101) )
         {
            context.GX_webresponse.AddString(",");
            context.GX_webresponse.AddString("101");
         }
         context.GX_webresponse.AddString("]");
         pr_datastore1.close(17);
      }

      protected void gxLoad_34( int A3Actividadid )
      {
         /* Using cursor T000G20 */
         pr_datastore1.execute(18, new Object[] {n3Actividadid, A3Actividadid});
         if ( (pr_datastore1.getStatus(18) == 101) )
         {
            if ( ! ( (0==A3Actividadid) ) )
            {
               GX_msglist.addItem("No matching 'Actividad'.", "ForeignKeyNotFound", 1, "ACTIVIDADID");
               AnyError = 1;
               GX_FocusControl = edtActividadid_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            }
         }
         A48Actividaddescripcion = T000G20_A48Actividaddescripcion[0];
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A48Actividaddescripcion", A48Actividaddescripcion);
         GxWebStd.set_html_headers( context, 0, "", "");
         context.GX_webresponse.AddString("[[");
         context.GX_webresponse.AddString("\""+GXUtil.EncodeJSConstant( A48Actividaddescripcion)+"\"");
         context.GX_webresponse.AddString("]");
         if ( (pr_datastore1.getStatus(18) == 101) )
         {
            context.GX_webresponse.AddString(",");
            context.GX_webresponse.AddString("101");
         }
         context.GX_webresponse.AddString("]");
         pr_datastore1.close(18);
      }

      protected void GetKey0G16( )
      {
         /* Using cursor T000G21 */
         pr_datastore1.execute(19, new Object[] {A13Planid});
         if ( (pr_datastore1.getStatus(19) != 101) )
         {
            RcdFound16 = 1;
         }
         else
         {
            RcdFound16 = 0;
         }
         pr_datastore1.close(19);
      }

      protected void getByPrimaryKey( )
      {
         /* Using cursor T000G3 */
         pr_datastore1.execute(1, new Object[] {A13Planid});
         if ( (pr_datastore1.getStatus(1) != 101) )
         {
            ZM0G16( 30) ;
            RcdFound16 = 1;
            A13Planid = T000G3_A13Planid[0];
            A30nombreEvento = T000G3_A30nombreEvento[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A30nombreEvento", A30nombreEvento);
            A31inicio = T000G3_A31inicio[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A31inicio", context.localUtil.Format(A31inicio, "99/99/99"));
            A32fin = T000G3_A32fin[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A32fin", context.localUtil.Format(A32fin, "99/99/99"));
            A36objGeneral = T000G3_A36objGeneral[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A36objGeneral", A36objGeneral);
            A37objEspecificos = T000G3_A37objEspecificos[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A37objEspecificos", A37objEspecificos);
            A38descripcionActividad = T000G3_A38descripcionActividad[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A38descripcionActividad", A38descripcionActividad);
            A39cantBeneficiarios = T000G3_A39cantBeneficiarios[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A39cantBeneficiarios", StringUtil.LTrim( StringUtil.Str( (decimal)(A39cantBeneficiarios), 9, 0)));
            n39cantBeneficiarios = T000G3_n39cantBeneficiarios[0];
            A40informe = T000G3_A40informe[0];
            A19Comunidadid = T000G3_A19Comunidadid[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A19Comunidadid", StringUtil.LTrim( StringUtil.Str( (decimal)(A19Comunidadid), 9, 0)));
            A22TipoBeneficiarioid = T000G3_A22TipoBeneficiarioid[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A22TipoBeneficiarioid", StringUtil.LTrim( StringUtil.Str( (decimal)(A22TipoBeneficiarioid), 9, 0)));
            A12Estadoid = T000G3_A12Estadoid[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A12Estadoid", StringUtil.LTrim( StringUtil.Str( (decimal)(A12Estadoid), 9, 0)));
            A3Actividadid = T000G3_A3Actividadid[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A3Actividadid", StringUtil.LTrim( StringUtil.Str( (decimal)(A3Actividadid), 9, 0)));
            n3Actividadid = T000G3_n3Actividadid[0];
            A29PlanidProyecto = T000G3_A29PlanidProyecto[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A29PlanidProyecto", StringUtil.LTrim( StringUtil.Str( (decimal)(A29PlanidProyecto), 9, 0)));
            A33PlanidResponsable = T000G3_A33PlanidResponsable[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A33PlanidResponsable", StringUtil.LTrim( StringUtil.Str( (decimal)(A33PlanidResponsable), 9, 0)));
            A34PlanidFinanciero = T000G3_A34PlanidFinanciero[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A34PlanidFinanciero", StringUtil.LTrim( StringUtil.Str( (decimal)(A34PlanidFinanciero), 9, 0)));
            A35PlanidTecnico = T000G3_A35PlanidTecnico[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A35PlanidTecnico", StringUtil.LTrim( StringUtil.Str( (decimal)(A35PlanidTecnico), 9, 0)));
            Z13Planid = A13Planid;
            sMode16 = Gx_mode;
            Gx_mode = "DSP";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!")), context));
            Load0G16( ) ;
            if ( AnyError == 1 )
            {
               RcdFound16 = 0;
               InitializeNonKey0G16( ) ;
            }
            Gx_mode = sMode16;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!")), context));
         }
         else
         {
            RcdFound16 = 0;
            InitializeNonKey0G16( ) ;
            sMode16 = Gx_mode;
            Gx_mode = "DSP";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!")), context));
            standaloneModal( ) ;
            Gx_mode = sMode16;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!")), context));
         }
         pr_datastore1.close(1);
      }

      protected void getEqualNoModal( )
      {
         GetKey0G16( ) ;
         if ( RcdFound16 == 0 )
         {
         }
         else
         {
         }
         getByPrimaryKey( ) ;
      }

      protected void move_next( )
      {
         RcdFound16 = 0;
         /* Using cursor T000G22 */
         pr_datastore1.execute(20, new Object[] {A13Planid});
         if ( (pr_datastore1.getStatus(20) != 101) )
         {
            while ( (pr_datastore1.getStatus(20) != 101) && ( ( T000G22_A13Planid[0] < A13Planid ) ) )
            {
               pr_datastore1.readNext(20);
            }
            if ( (pr_datastore1.getStatus(20) != 101) && ( ( T000G22_A13Planid[0] > A13Planid ) ) )
            {
               A13Planid = T000G22_A13Planid[0];
               RcdFound16 = 1;
            }
         }
         pr_datastore1.close(20);
      }

      protected void move_previous( )
      {
         RcdFound16 = 0;
         /* Using cursor T000G23 */
         pr_datastore1.execute(21, new Object[] {A13Planid});
         if ( (pr_datastore1.getStatus(21) != 101) )
         {
            while ( (pr_datastore1.getStatus(21) != 101) && ( ( T000G23_A13Planid[0] > A13Planid ) ) )
            {
               pr_datastore1.readNext(21);
            }
            if ( (pr_datastore1.getStatus(21) != 101) && ( ( T000G23_A13Planid[0] < A13Planid ) ) )
            {
               A13Planid = T000G23_A13Planid[0];
               RcdFound16 = 1;
            }
         }
         pr_datastore1.close(21);
      }

      protected void btn_enter( )
      {
         nKeyPressed = 1;
         GetKey0G16( ) ;
         if ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )
         {
            /* Insert record */
            GX_FocusControl = edtPlanidProyecto_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            Insert0G16( ) ;
            if ( AnyError == 1 )
            {
               GX_FocusControl = "";
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            }
         }
         else
         {
            if ( RcdFound16 == 1 )
            {
               if ( A13Planid != Z13Planid )
               {
                  A13Planid = Z13Planid;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A13Planid", StringUtil.LTrim( StringUtil.Str( (decimal)(A13Planid), 9, 0)));
                  GX_msglist.addItem(context.GetMessage( "GXM_getbeforeupd", ""), "CandidateKeyNotFound", 1, "");
                  AnyError = 1;
               }
               else if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
               {
                  delete( ) ;
                  AfterTrn( ) ;
                  GX_FocusControl = edtPlanidProyecto_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               }
               else
               {
                  /* Update record */
                  Update0G16( ) ;
                  GX_FocusControl = edtPlanidProyecto_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               }
            }
            else
            {
               if ( A13Planid != Z13Planid )
               {
                  /* Insert record */
                  GX_FocusControl = edtPlanidProyecto_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  Insert0G16( ) ;
                  if ( AnyError == 1 )
                  {
                     GX_FocusControl = "";
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  }
               }
               else
               {
                  if ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 )
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_recdeleted", ""), 1, "");
                     AnyError = 1;
                  }
                  else
                  {
                     /* Insert record */
                     GX_FocusControl = edtPlanidProyecto_Internalname;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                     Insert0G16( ) ;
                     if ( AnyError == 1 )
                     {
                        GX_FocusControl = "";
                        context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                     }
                  }
               }
            }
         }
         AfterTrn( ) ;
         if ( ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 ) || ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 ) )
         {
            if ( AnyError == 0 )
            {
               context.nUserReturn = 1;
            }
         }
      }

      protected void btn_delete( )
      {
         if ( A13Planid != Z13Planid )
         {
            A13Planid = Z13Planid;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A13Planid", StringUtil.LTrim( StringUtil.Str( (decimal)(A13Planid), 9, 0)));
            GX_msglist.addItem(context.GetMessage( "GXM_getbeforedlt", ""), 1, "");
            AnyError = 1;
         }
         else
         {
            delete( ) ;
            AfterTrn( ) ;
            GX_FocusControl = edtPlanidProyecto_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         if ( AnyError != 0 )
         {
         }
      }

      protected void CheckOptimisticConcurrency0G16( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
         {
            /* Using cursor T000G2 */
            pr_datastore1.execute(0, new Object[] {A13Planid});
            if ( (pr_datastore1.getStatus(0) == 103) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_lock", new   object[]  {"PLAN"}), "RecordIsLocked", 1, "");
               AnyError = 1;
               return  ;
            }
            Gx_longc = false;
            if ( (pr_datastore1.getStatus(0) == 101) || ( StringUtil.StrCmp(Z30nombreEvento, T000G2_A30nombreEvento[0]) != 0 ) || ( Z31inicio != T000G2_A31inicio[0] ) || ( Z32fin != T000G2_A32fin[0] ) || ( StringUtil.StrCmp(Z36objGeneral, T000G2_A36objGeneral[0]) != 0 ) || ( StringUtil.StrCmp(Z37objEspecificos, T000G2_A37objEspecificos[0]) != 0 ) )
            {
               Gx_longc = true;
            }
            if ( Gx_longc || ( StringUtil.StrCmp(Z38descripcionActividad, T000G2_A38descripcionActividad[0]) != 0 ) || ( Z39cantBeneficiarios != T000G2_A39cantBeneficiarios[0] ) || ( Z40informe != T000G2_A40informe[0] ) || ( Z19Comunidadid != T000G2_A19Comunidadid[0] ) || ( Z22TipoBeneficiarioid != T000G2_A22TipoBeneficiarioid[0] ) )
            {
               Gx_longc = true;
            }
            if ( Gx_longc || ( Z12Estadoid != T000G2_A12Estadoid[0] ) || ( Z3Actividadid != T000G2_A3Actividadid[0] ) || ( Z29PlanidProyecto != T000G2_A29PlanidProyecto[0] ) || ( Z33PlanidResponsable != T000G2_A33PlanidResponsable[0] ) || ( Z34PlanidFinanciero != T000G2_A34PlanidFinanciero[0] ) )
            {
               Gx_longc = true;
            }
            if ( Gx_longc || ( Z35PlanidTecnico != T000G2_A35PlanidTecnico[0] ) )
            {
               if ( StringUtil.StrCmp(Z30nombreEvento, T000G2_A30nombreEvento[0]) != 0 )
               {
                  GXUtil.WriteLog("plan:[seudo value changed for attri]"+"nombreEvento");
                  GXUtil.WriteLogRaw("Old: ",Z30nombreEvento);
                  GXUtil.WriteLogRaw("Current: ",T000G2_A30nombreEvento[0]);
               }
               if ( Z31inicio != T000G2_A31inicio[0] )
               {
                  GXUtil.WriteLog("plan:[seudo value changed for attri]"+"inicio");
                  GXUtil.WriteLogRaw("Old: ",Z31inicio);
                  GXUtil.WriteLogRaw("Current: ",T000G2_A31inicio[0]);
               }
               if ( Z32fin != T000G2_A32fin[0] )
               {
                  GXUtil.WriteLog("plan:[seudo value changed for attri]"+"fin");
                  GXUtil.WriteLogRaw("Old: ",Z32fin);
                  GXUtil.WriteLogRaw("Current: ",T000G2_A32fin[0]);
               }
               if ( StringUtil.StrCmp(Z36objGeneral, T000G2_A36objGeneral[0]) != 0 )
               {
                  GXUtil.WriteLog("plan:[seudo value changed for attri]"+"objGeneral");
                  GXUtil.WriteLogRaw("Old: ",Z36objGeneral);
                  GXUtil.WriteLogRaw("Current: ",T000G2_A36objGeneral[0]);
               }
               if ( StringUtil.StrCmp(Z37objEspecificos, T000G2_A37objEspecificos[0]) != 0 )
               {
                  GXUtil.WriteLog("plan:[seudo value changed for attri]"+"objEspecificos");
                  GXUtil.WriteLogRaw("Old: ",Z37objEspecificos);
                  GXUtil.WriteLogRaw("Current: ",T000G2_A37objEspecificos[0]);
               }
               if ( StringUtil.StrCmp(Z38descripcionActividad, T000G2_A38descripcionActividad[0]) != 0 )
               {
                  GXUtil.WriteLog("plan:[seudo value changed for attri]"+"descripcionActividad");
                  GXUtil.WriteLogRaw("Old: ",Z38descripcionActividad);
                  GXUtil.WriteLogRaw("Current: ",T000G2_A38descripcionActividad[0]);
               }
               if ( Z39cantBeneficiarios != T000G2_A39cantBeneficiarios[0] )
               {
                  GXUtil.WriteLog("plan:[seudo value changed for attri]"+"cantBeneficiarios");
                  GXUtil.WriteLogRaw("Old: ",Z39cantBeneficiarios);
                  GXUtil.WriteLogRaw("Current: ",T000G2_A39cantBeneficiarios[0]);
               }
               if ( Z40informe != T000G2_A40informe[0] )
               {
                  GXUtil.WriteLog("plan:[seudo value changed for attri]"+"informe");
                  GXUtil.WriteLogRaw("Old: ",Z40informe);
                  GXUtil.WriteLogRaw("Current: ",T000G2_A40informe[0]);
               }
               if ( Z19Comunidadid != T000G2_A19Comunidadid[0] )
               {
                  GXUtil.WriteLog("plan:[seudo value changed for attri]"+"Comunidadid");
                  GXUtil.WriteLogRaw("Old: ",Z19Comunidadid);
                  GXUtil.WriteLogRaw("Current: ",T000G2_A19Comunidadid[0]);
               }
               if ( Z22TipoBeneficiarioid != T000G2_A22TipoBeneficiarioid[0] )
               {
                  GXUtil.WriteLog("plan:[seudo value changed for attri]"+"TipoBeneficiarioid");
                  GXUtil.WriteLogRaw("Old: ",Z22TipoBeneficiarioid);
                  GXUtil.WriteLogRaw("Current: ",T000G2_A22TipoBeneficiarioid[0]);
               }
               if ( Z12Estadoid != T000G2_A12Estadoid[0] )
               {
                  GXUtil.WriteLog("plan:[seudo value changed for attri]"+"Estadoid");
                  GXUtil.WriteLogRaw("Old: ",Z12Estadoid);
                  GXUtil.WriteLogRaw("Current: ",T000G2_A12Estadoid[0]);
               }
               if ( Z3Actividadid != T000G2_A3Actividadid[0] )
               {
                  GXUtil.WriteLog("plan:[seudo value changed for attri]"+"Actividadid");
                  GXUtil.WriteLogRaw("Old: ",Z3Actividadid);
                  GXUtil.WriteLogRaw("Current: ",T000G2_A3Actividadid[0]);
               }
               if ( Z29PlanidProyecto != T000G2_A29PlanidProyecto[0] )
               {
                  GXUtil.WriteLog("plan:[seudo value changed for attri]"+"PlanidProyecto");
                  GXUtil.WriteLogRaw("Old: ",Z29PlanidProyecto);
                  GXUtil.WriteLogRaw("Current: ",T000G2_A29PlanidProyecto[0]);
               }
               if ( Z33PlanidResponsable != T000G2_A33PlanidResponsable[0] )
               {
                  GXUtil.WriteLog("plan:[seudo value changed for attri]"+"PlanidResponsable");
                  GXUtil.WriteLogRaw("Old: ",Z33PlanidResponsable);
                  GXUtil.WriteLogRaw("Current: ",T000G2_A33PlanidResponsable[0]);
               }
               if ( Z34PlanidFinanciero != T000G2_A34PlanidFinanciero[0] )
               {
                  GXUtil.WriteLog("plan:[seudo value changed for attri]"+"PlanidFinanciero");
                  GXUtil.WriteLogRaw("Old: ",Z34PlanidFinanciero);
                  GXUtil.WriteLogRaw("Current: ",T000G2_A34PlanidFinanciero[0]);
               }
               if ( Z35PlanidTecnico != T000G2_A35PlanidTecnico[0] )
               {
                  GXUtil.WriteLog("plan:[seudo value changed for attri]"+"PlanidTecnico");
                  GXUtil.WriteLogRaw("Old: ",Z35PlanidTecnico);
                  GXUtil.WriteLogRaw("Current: ",T000G2_A35PlanidTecnico[0]);
               }
               GX_msglist.addItem(context.GetMessage( "GXM_waschg", new   object[]  {"PLAN"}), "RecordWasChanged", 1, "");
               AnyError = 1;
               return  ;
            }
         }
      }

      protected void Insert0G16( )
      {
         BeforeValidate0G16( ) ;
         if ( AnyError == 0 )
         {
            CheckExtendedTable0G16( ) ;
         }
         if ( AnyError == 0 )
         {
            ZM0G16( 0) ;
            CheckOptimisticConcurrency0G16( ) ;
            if ( AnyError == 0 )
            {
               AfterConfirm0G16( ) ;
               if ( AnyError == 0 )
               {
                  BeforeInsert0G16( ) ;
                  if ( AnyError == 0 )
                  {
                     /* Using cursor T000G24 */
                     pr_datastore1.execute(22, new Object[] {A30nombreEvento, A31inicio, A32fin, A36objGeneral, A37objEspecificos, A38descripcionActividad, n39cantBeneficiarios, A39cantBeneficiarios, A40informe, A19Comunidadid, A22TipoBeneficiarioid, A12Estadoid, n3Actividadid, A3Actividadid, A29PlanidProyecto, A33PlanidResponsable, A34PlanidFinanciero, A35PlanidTecnico});
                     A13Planid = T000G24_A13Planid[0];
                     pr_datastore1.close(22);
                     dsDataStore1.SmartCacheProvider.SetUpdated("PLAN") ;
                     if ( AnyError == 0 )
                     {
                        /* Start of After( Insert) rules */
                        /* End of After( Insert) rules */
                        if ( AnyError == 0 )
                        {
                           /* Save values for previous() function. */
                           GX_msglist.addItem(context.GetMessage( "GXM_sucadded", ""), "SuccessfullyAdded", 0, "", true);
                           ResetCaption0G0( ) ;
                        }
                     }
                  }
                  else
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                     AnyError = 1;
                  }
               }
            }
            else
            {
               Load0G16( ) ;
            }
            EndLevel0G16( ) ;
         }
         CloseExtendedTableCursors0G16( ) ;
      }

      protected void Update0G16( )
      {
         BeforeValidate0G16( ) ;
         if ( AnyError == 0 )
         {
            CheckExtendedTable0G16( ) ;
         }
         if ( AnyError == 0 )
         {
            CheckOptimisticConcurrency0G16( ) ;
            if ( AnyError == 0 )
            {
               AfterConfirm0G16( ) ;
               if ( AnyError == 0 )
               {
                  BeforeUpdate0G16( ) ;
                  if ( AnyError == 0 )
                  {
                     /* Using cursor T000G25 */
                     pr_datastore1.execute(23, new Object[] {A30nombreEvento, A31inicio, A32fin, A36objGeneral, A37objEspecificos, A38descripcionActividad, n39cantBeneficiarios, A39cantBeneficiarios, A40informe, A19Comunidadid, A22TipoBeneficiarioid, A12Estadoid, n3Actividadid, A3Actividadid, A29PlanidProyecto, A33PlanidResponsable, A34PlanidFinanciero, A35PlanidTecnico, A13Planid});
                     pr_datastore1.close(23);
                     dsDataStore1.SmartCacheProvider.SetUpdated("PLAN") ;
                     if ( (pr_datastore1.getStatus(23) == 103) )
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_lock", new   object[]  {"PLAN"}), "RecordIsLocked", 1, "");
                        AnyError = 1;
                     }
                     DeferredUpdate0G16( ) ;
                     if ( AnyError == 0 )
                     {
                        /* Start of After( update) rules */
                        /* End of After( update) rules */
                        if ( AnyError == 0 )
                        {
                           if ( ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 ) || ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 ) )
                           {
                              if ( AnyError == 0 )
                              {
                                 context.nUserReturn = 1;
                              }
                           }
                        }
                     }
                     else
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                        AnyError = 1;
                     }
                  }
               }
            }
            EndLevel0G16( ) ;
         }
         CloseExtendedTableCursors0G16( ) ;
      }

      protected void DeferredUpdate0G16( )
      {
      }

      protected void delete( )
      {
         BeforeValidate0G16( ) ;
         if ( AnyError == 0 )
         {
            CheckOptimisticConcurrency0G16( ) ;
         }
         if ( AnyError == 0 )
         {
            OnDeleteControls0G16( ) ;
            AfterConfirm0G16( ) ;
            if ( AnyError == 0 )
            {
               BeforeDelete0G16( ) ;
               if ( AnyError == 0 )
               {
                  /* No cascading delete specified. */
                  /* Using cursor T000G26 */
                  pr_datastore1.execute(24, new Object[] {A13Planid});
                  pr_datastore1.close(24);
                  dsDataStore1.SmartCacheProvider.SetUpdated("PLAN") ;
                  if ( AnyError == 0 )
                  {
                     /* Start of After( delete) rules */
                     /* End of After( delete) rules */
                     if ( AnyError == 0 )
                     {
                        if ( ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 ) || ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 ) )
                        {
                           if ( AnyError == 0 )
                           {
                              context.nUserReturn = 1;
                           }
                        }
                     }
                  }
                  else
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                     AnyError = 1;
                  }
               }
            }
         }
         sMode16 = Gx_mode;
         Gx_mode = "DLT";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!")), context));
         EndLevel0G16( ) ;
         Gx_mode = sMode16;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!")), context));
      }

      protected void OnDeleteControls0G16( )
      {
         standaloneModal( ) ;
         if ( AnyError == 0 )
         {
            /* Delete mode formulas */
            AV20Pgmname = "Plan";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20Pgmname", AV20Pgmname);
            /* Using cursor T000G27 */
            pr_datastore1.execute(25, new Object[] {A19Comunidadid});
            A130Comunidadnombre = T000G27_A130Comunidadnombre[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A130Comunidadnombre", A130Comunidadnombre);
            pr_datastore1.close(25);
            /* Using cursor T000G28 */
            pr_datastore1.execute(26, new Object[] {A12Estadoid});
            A61Estadonombre = T000G28_A61Estadonombre[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A61Estadonombre", A61Estadonombre);
            pr_datastore1.close(26);
            /* Using cursor T000G29 */
            pr_datastore1.execute(27, new Object[] {n3Actividadid, A3Actividadid});
            A48Actividaddescripcion = T000G29_A48Actividaddescripcion[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A48Actividaddescripcion", A48Actividaddescripcion);
            pr_datastore1.close(27);
         }
         if ( AnyError == 0 )
         {
            /* Using cursor T000G30 */
            pr_datastore1.execute(28, new Object[] {A13Planid});
            if ( (pr_datastore1.getStatus(28) != 101) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_del", new   object[]  {"Informe_Actividad"}), "CannotDeleteReferencedRecord", 1, "");
               AnyError = 1;
            }
            pr_datastore1.close(28);
         }
      }

      protected void EndLevel0G16( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
         {
            pr_datastore1.close(0);
         }
         if ( AnyError == 0 )
         {
            BeforeComplete0G16( ) ;
         }
         if ( AnyError == 0 )
         {
            pr_datastore1.close(1);
            pr_datastore1.close(25);
            pr_datastore1.close(26);
            pr_datastore1.close(27);
            context.CommitDataStores("plan",pr_default);
            if ( AnyError == 0 )
            {
               ConfirmValues0G0( ) ;
            }
            /* After transaction rules */
            /* Execute 'After Trn' event if defined. */
            trnEnded = 1;
         }
         else
         {
            pr_datastore1.close(1);
            pr_datastore1.close(25);
            pr_datastore1.close(26);
            pr_datastore1.close(27);
            context.RollbackDataStores("plan",pr_default);
         }
         IsModified = 0;
         if ( AnyError != 0 )
         {
            context.wjLoc = "";
            context.nUserReturn = 0;
         }
      }

      public void ScanStart0G16( )
      {
         /* Scan By routine */
         /* Using cursor T000G31 */
         pr_datastore1.execute(29);
         RcdFound16 = 0;
         if ( (pr_datastore1.getStatus(29) != 101) )
         {
            RcdFound16 = 1;
            A13Planid = T000G31_A13Planid[0];
         }
         /* Load Subordinate Levels */
      }

      protected void ScanNext0G16( )
      {
         /* Scan next routine */
         pr_datastore1.readNext(29);
         RcdFound16 = 0;
         if ( (pr_datastore1.getStatus(29) != 101) )
         {
            RcdFound16 = 1;
            A13Planid = T000G31_A13Planid[0];
         }
      }

      protected void ScanEnd0G16( )
      {
         pr_datastore1.close(29);
      }

      protected void AfterConfirm0G16( )
      {
         /* After Confirm Rules */
      }

      protected void BeforeInsert0G16( )
      {
         /* Before Insert Rules */
      }

      protected void BeforeUpdate0G16( )
      {
         /* Before Update Rules */
      }

      protected void BeforeDelete0G16( )
      {
         /* Before Delete Rules */
      }

      protected void BeforeComplete0G16( )
      {
         /* Before Complete Rules */
      }

      protected void BeforeValidate0G16( )
      {
         /* Before Validate Rules */
      }

      protected void DisableAttributes0G16( )
      {
         edtPlanidProyecto_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtPlanidProyecto_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtPlanidProyecto_Enabled), 5, 0)), true);
         edtnombreEvento_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtnombreEvento_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtnombreEvento_Enabled), 5, 0)), true);
         edtComunidadid_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtComunidadid_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtComunidadid_Enabled), 5, 0)), true);
         edtComunidadnombre_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtComunidadnombre_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtComunidadnombre_Enabled), 5, 0)), true);
         edtinicio_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtinicio_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtinicio_Enabled), 5, 0)), true);
         edtfin_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtfin_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtfin_Enabled), 5, 0)), true);
         edtTipoBeneficiarioid_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtTipoBeneficiarioid_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtTipoBeneficiarioid_Enabled), 5, 0)), true);
         edtPlanidResponsable_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtPlanidResponsable_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtPlanidResponsable_Enabled), 5, 0)), true);
         edtPlanidFinanciero_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtPlanidFinanciero_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtPlanidFinanciero_Enabled), 5, 0)), true);
         edtPlanidTecnico_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtPlanidTecnico_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtPlanidTecnico_Enabled), 5, 0)), true);
         edtobjGeneral_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtobjGeneral_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtobjGeneral_Enabled), 5, 0)), true);
         edtobjEspecificos_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtobjEspecificos_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtobjEspecificos_Enabled), 5, 0)), true);
         edtdescripcionActividad_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtdescripcionActividad_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtdescripcionActividad_Enabled), 5, 0)), true);
         edtEstadoid_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtEstadoid_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtEstadoid_Enabled), 5, 0)), true);
         edtEstadonombre_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtEstadonombre_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtEstadonombre_Enabled), 5, 0)), true);
         edtcantBeneficiarios_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtcantBeneficiarios_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtcantBeneficiarios_Enabled), 5, 0)), true);
         edtActividadid_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtActividadid_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtActividadid_Enabled), 5, 0)), true);
         edtActividaddescripcion_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtActividaddescripcion_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtActividaddescripcion_Enabled), 5, 0)), true);
      }

      protected void send_integrity_lvl_hashes0G16( )
      {
      }

      protected void assign_properties_default( )
      {
      }

      protected void ConfirmValues0G0( )
      {
      }

      public override void RenderHtmlHeaders( )
      {
         GxWebStd.gx_html_headers( context, 0, "", "", Form.Meta, Form.Metaequiv, true);
      }

      public override void RenderHtmlOpenForm( )
      {
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.WriteHtmlText( "<title>") ;
         context.SendWebValue( Form.Caption) ;
         context.WriteHtmlTextNl( "</title>") ;
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         if ( StringUtil.Len( sDynURL) > 0 )
         {
            context.WriteHtmlText( "<BASE href=\""+sDynURL+"\" />") ;
         }
         define_styles( ) ;
         MasterPageObj.master_styles();
         if ( ( ( context.GetBrowserType( ) == 1 ) || ( context.GetBrowserType( ) == 5 ) ) && ( StringUtil.StrCmp(context.GetBrowserVersion( ), "7.0") == 0 ) )
         {
            context.AddJavascriptSource("json2.js", "?"+context.GetBuildNumber( 127771), false);
         }
         context.AddJavascriptSource("jquery.js", "?"+context.GetBuildNumber( 127771), false);
         context.AddJavascriptSource("gxtimezone.js", "?"+context.GetBuildNumber( 127771), false);
         context.AddJavascriptSource("gxgral.js", "?"+context.GetBuildNumber( 127771), false);
         context.AddJavascriptSource("gxcfg.js", "?2019151446552", false);
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.AddJavascriptSource("calendar.js", "?"+context.GetBuildNumber( 127771), false);
         context.AddJavascriptSource("calendar-setup.js", "?"+context.GetBuildNumber( 127771), false);
         context.AddJavascriptSource("calendar-en.js", "?"+context.GetBuildNumber( 127771), false);
         context.WriteHtmlText( Form.Headerrawhtml) ;
         context.CloseHtmlHeader();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         FormProcess = " data-HasEnter=\"true\" data-Skiponenter=\"false\"";
         context.WriteHtmlText( "<body ") ;
         bodyStyle = "" + "background-color:" + context.BuildHTMLColor( Form.Backcolor) + ";color:" + context.BuildHTMLColor( Form.Textcolor) + ";";
         bodyStyle = bodyStyle + "-moz-opacity:0;opacity:0;";
         if ( ! ( String.IsNullOrEmpty(StringUtil.RTrim( Form.Background)) ) )
         {
            bodyStyle = bodyStyle + " background-image:url(" + context.convertURL( Form.Background) + ")";
         }
         context.WriteHtmlText( " "+"class=\"form-horizontal Form\""+" "+ "style='"+bodyStyle+"'") ;
         context.WriteHtmlText( FormProcess+">") ;
         context.skipLines(1);
         context.WriteHtmlTextNl( "<form id=\"MAINFORM\" autocomplete=\"off\" name=\"MAINFORM\" method=\"post\" tabindex=-1  class=\"form-horizontal Form\" data-gx-class=\"form-horizontal Form\" novalidate action=\""+formatLink("plan.aspx") + "?" + UrlEncode(StringUtil.RTrim(Gx_mode)) + "," + UrlEncode("" +AV7Planid)+"\">") ;
         GxWebStd.gx_hidden_field( context, "_EventName", "");
         GxWebStd.gx_hidden_field( context, "_EventGridId", "");
         GxWebStd.gx_hidden_field( context, "_EventRowId", "");
         context.WriteHtmlText( "<input type=\"submit\" title=\"submit\" style=\"display:none\" disabled>") ;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, "FORM", "Class", "form-horizontal Form", true);
         toggleJsOutput = isJsOutputEnabled( );
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
      }

      protected void send_integrity_footer_hashes( )
      {
         GXKey = Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
         forbiddenHiddens = "hsh" + "Plan";
         forbiddenHiddens = forbiddenHiddens + context.localUtil.Format( (decimal)(AV11Insert_PlanidProyecto), "ZZZZZZZZ9");
         forbiddenHiddens = forbiddenHiddens + context.localUtil.Format( (decimal)(AV12Insert_Comunidadid), "ZZZZZZZZ9");
         forbiddenHiddens = forbiddenHiddens + context.localUtil.Format( (decimal)(AV13Insert_TipoBeneficiarioid), "ZZZZZZZZ9");
         forbiddenHiddens = forbiddenHiddens + context.localUtil.Format( (decimal)(AV14Insert_PlanidResponsable), "ZZZZZZZZ9");
         forbiddenHiddens = forbiddenHiddens + context.localUtil.Format( (decimal)(AV15Insert_PlanidFinanciero), "ZZZZZZZZ9");
         forbiddenHiddens = forbiddenHiddens + context.localUtil.Format( (decimal)(AV16Insert_PlanidTecnico), "ZZZZZZZZ9");
         forbiddenHiddens = forbiddenHiddens + context.localUtil.Format( (decimal)(AV17Insert_Estadoid), "ZZZZZZZZ9");
         forbiddenHiddens = forbiddenHiddens + context.localUtil.Format( (decimal)(AV18Insert_Actividadid), "ZZZZZZZZ9");
         forbiddenHiddens = forbiddenHiddens + StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"));
         forbiddenHiddens = forbiddenHiddens + context.localUtil.Format( (decimal)(A13Planid), "ZZZZZZZZ9");
         forbiddenHiddens = forbiddenHiddens + context.localUtil.Format( (decimal)(A40informe), "9");
         GxWebStd.gx_hidden_field( context, "hsh", GetEncryptedHash( forbiddenHiddens, GXKey));
         GXUtil.WriteLog("plan:[SendSecurityCheck value for]"+"Insert_PlanidProyecto:"+context.localUtil.Format( (decimal)(AV11Insert_PlanidProyecto), "ZZZZZZZZ9"));
         GXUtil.WriteLog("plan:[SendSecurityCheck value for]"+"Insert_Comunidadid:"+context.localUtil.Format( (decimal)(AV12Insert_Comunidadid), "ZZZZZZZZ9"));
         GXUtil.WriteLog("plan:[SendSecurityCheck value for]"+"Insert_TipoBeneficiarioid:"+context.localUtil.Format( (decimal)(AV13Insert_TipoBeneficiarioid), "ZZZZZZZZ9"));
         GXUtil.WriteLog("plan:[SendSecurityCheck value for]"+"Insert_PlanidResponsable:"+context.localUtil.Format( (decimal)(AV14Insert_PlanidResponsable), "ZZZZZZZZ9"));
         GXUtil.WriteLog("plan:[SendSecurityCheck value for]"+"Insert_PlanidFinanciero:"+context.localUtil.Format( (decimal)(AV15Insert_PlanidFinanciero), "ZZZZZZZZ9"));
         GXUtil.WriteLog("plan:[SendSecurityCheck value for]"+"Insert_PlanidTecnico:"+context.localUtil.Format( (decimal)(AV16Insert_PlanidTecnico), "ZZZZZZZZ9"));
         GXUtil.WriteLog("plan:[SendSecurityCheck value for]"+"Insert_Estadoid:"+context.localUtil.Format( (decimal)(AV17Insert_Estadoid), "ZZZZZZZZ9"));
         GXUtil.WriteLog("plan:[SendSecurityCheck value for]"+"Insert_Actividadid:"+context.localUtil.Format( (decimal)(AV18Insert_Actividadid), "ZZZZZZZZ9"));
         GXUtil.WriteLog("plan:[SendSecurityCheck value for]"+"Gx_mode:"+StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!")));
         GXUtil.WriteLog("plan:[SendSecurityCheck value for]"+"Planid:"+context.localUtil.Format( (decimal)(A13Planid), "ZZZZZZZZ9"));
         GXUtil.WriteLog("plan:[SendSecurityCheck value for]"+"informe:"+context.localUtil.Format( (decimal)(A40informe), "9"));
      }

      protected void SendCloseFormHiddens( )
      {
         /* Send hidden variables. */
         /* Send saved values. */
         send_integrity_footer_hashes( ) ;
         GxWebStd.gx_hidden_field( context, "Z13Planid", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z13Planid), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "Z30nombreEvento", Z30nombreEvento);
         GxWebStd.gx_hidden_field( context, "Z31inicio", context.localUtil.DToC( Z31inicio, 0, "/"));
         GxWebStd.gx_hidden_field( context, "Z32fin", context.localUtil.DToC( Z32fin, 0, "/"));
         GxWebStd.gx_hidden_field( context, "Z36objGeneral", Z36objGeneral);
         GxWebStd.gx_hidden_field( context, "Z37objEspecificos", Z37objEspecificos);
         GxWebStd.gx_hidden_field( context, "Z38descripcionActividad", Z38descripcionActividad);
         GxWebStd.gx_hidden_field( context, "Z39cantBeneficiarios", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z39cantBeneficiarios), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "Z40informe", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z40informe), 1, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "Z19Comunidadid", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z19Comunidadid), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "Z22TipoBeneficiarioid", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z22TipoBeneficiarioid), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "Z12Estadoid", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z12Estadoid), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "Z3Actividadid", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z3Actividadid), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "Z29PlanidProyecto", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z29PlanidProyecto), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "Z33PlanidResponsable", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z33PlanidResponsable), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "Z34PlanidFinanciero", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z34PlanidFinanciero), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "Z35PlanidTecnico", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z35PlanidTecnico), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "IsConfirmed", StringUtil.LTrim( StringUtil.NToC( (decimal)(IsConfirmed), 4, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "IsModified", StringUtil.LTrim( StringUtil.NToC( (decimal)(IsModified), 4, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "Mode", StringUtil.RTrim( Gx_mode));
         GxWebStd.gx_hidden_field( context, "gxhash_Mode", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!")), context));
         GxWebStd.gx_hidden_field( context, "N29PlanidProyecto", StringUtil.LTrim( StringUtil.NToC( (decimal)(A29PlanidProyecto), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "N19Comunidadid", StringUtil.LTrim( StringUtil.NToC( (decimal)(A19Comunidadid), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "N22TipoBeneficiarioid", StringUtil.LTrim( StringUtil.NToC( (decimal)(A22TipoBeneficiarioid), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "N33PlanidResponsable", StringUtil.LTrim( StringUtil.NToC( (decimal)(A33PlanidResponsable), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "N34PlanidFinanciero", StringUtil.LTrim( StringUtil.NToC( (decimal)(A34PlanidFinanciero), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "N35PlanidTecnico", StringUtil.LTrim( StringUtil.NToC( (decimal)(A35PlanidTecnico), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "N12Estadoid", StringUtil.LTrim( StringUtil.NToC( (decimal)(A12Estadoid), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "N3Actividadid", StringUtil.LTrim( StringUtil.NToC( (decimal)(A3Actividadid), 9, 0, ".", "")));
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vTRNCONTEXT", AV9TrnContext);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vTRNCONTEXT", AV9TrnContext);
         }
         GxWebStd.gx_hidden_field( context, "vPLANID", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV7Planid), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "gxhash_vPLANID", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV7Planid), "ZZZZZZZZ9"), context));
         GxWebStd.gx_hidden_field( context, "PLANID", StringUtil.LTrim( StringUtil.NToC( (decimal)(A13Planid), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "vINSERT_PLANIDPROYECTO", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV11Insert_PlanidProyecto), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "vINSERT_COMUNIDADID", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV12Insert_Comunidadid), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "vINSERT_TIPOBENEFICIARIOID", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV13Insert_TipoBeneficiarioid), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "vINSERT_PLANIDRESPONSABLE", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV14Insert_PlanidResponsable), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "vINSERT_PLANIDFINANCIERO", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV15Insert_PlanidFinanciero), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "vINSERT_PLANIDTECNICO", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV16Insert_PlanidTecnico), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "vINSERT_ESTADOID", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV17Insert_Estadoid), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "vINSERT_ACTIVIDADID", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV18Insert_Actividadid), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "INFORME", StringUtil.LTrim( StringUtil.NToC( (decimal)(A40informe), 1, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "vPGMNAME", StringUtil.RTrim( AV20Pgmname));
         GxWebStd.gx_hidden_field( context, "vMODE", StringUtil.RTrim( Gx_mode));
         GxWebStd.gx_hidden_field( context, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!")), context));
      }

      public override void RenderHtmlCloseForm( )
      {
         SendCloseFormHiddens( ) ;
         GxWebStd.gx_hidden_field( context, "GX_FocusControl", GX_FocusControl);
         SendAjaxEncryptionKey();
         SendSecurityToken(sPrefix);
         SendComponentObjects();
         SendServerCommands();
         SendState();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         context.WriteHtmlTextNl( "</form>") ;
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         include_jscripts( ) ;
      }

      public override short ExecuteStartEvent( )
      {
         standaloneStartup( ) ;
         gxajaxcallmode = (short)((isAjaxCallMode( ) ? 1 : 0));
         return gxajaxcallmode ;
      }

      public override void RenderHtmlContent( )
      {
         context.WriteHtmlText( "<div") ;
         GxWebStd.ClassAttribute( context, "gx-ct-body"+" "+(String.IsNullOrEmpty(StringUtil.RTrim( Form.Class)) ? "form-horizontal Form" : Form.Class)+"-fx");
         context.WriteHtmlText( ">") ;
         Draw( ) ;
         context.WriteHtmlText( "</div>") ;
      }

      public override void DispatchEvents( )
      {
         Process( ) ;
      }

      public override bool HasEnterEvent( )
      {
         return true ;
      }

      public override GXWebForm GetForm( )
      {
         return Form ;
      }

      public override String GetSelfLink( )
      {
         return formatLink("plan.aspx") + "?" + UrlEncode(StringUtil.RTrim(Gx_mode)) + "," + UrlEncode("" +AV7Planid) ;
      }

      public override String GetPgmname( )
      {
         return "Plan" ;
      }

      public override String GetPgmdesc( )
      {
         return "Plan" ;
      }

      protected void InitializeNonKey0G16( )
      {
         A29PlanidProyecto = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A29PlanidProyecto", StringUtil.LTrim( StringUtil.Str( (decimal)(A29PlanidProyecto), 9, 0)));
         A19Comunidadid = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A19Comunidadid", StringUtil.LTrim( StringUtil.Str( (decimal)(A19Comunidadid), 9, 0)));
         A22TipoBeneficiarioid = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A22TipoBeneficiarioid", StringUtil.LTrim( StringUtil.Str( (decimal)(A22TipoBeneficiarioid), 9, 0)));
         A33PlanidResponsable = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A33PlanidResponsable", StringUtil.LTrim( StringUtil.Str( (decimal)(A33PlanidResponsable), 9, 0)));
         A34PlanidFinanciero = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A34PlanidFinanciero", StringUtil.LTrim( StringUtil.Str( (decimal)(A34PlanidFinanciero), 9, 0)));
         A35PlanidTecnico = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A35PlanidTecnico", StringUtil.LTrim( StringUtil.Str( (decimal)(A35PlanidTecnico), 9, 0)));
         A12Estadoid = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A12Estadoid", StringUtil.LTrim( StringUtil.Str( (decimal)(A12Estadoid), 9, 0)));
         A3Actividadid = 0;
         n3Actividadid = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A3Actividadid", StringUtil.LTrim( StringUtil.Str( (decimal)(A3Actividadid), 9, 0)));
         n3Actividadid = ((0==A3Actividadid) ? true : false);
         A30nombreEvento = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A30nombreEvento", A30nombreEvento);
         A130Comunidadnombre = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A130Comunidadnombre", A130Comunidadnombre);
         A31inicio = DateTime.MinValue;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A31inicio", context.localUtil.Format(A31inicio, "99/99/99"));
         A32fin = DateTime.MinValue;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A32fin", context.localUtil.Format(A32fin, "99/99/99"));
         A36objGeneral = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A36objGeneral", A36objGeneral);
         A37objEspecificos = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A37objEspecificos", A37objEspecificos);
         A38descripcionActividad = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A38descripcionActividad", A38descripcionActividad);
         A61Estadonombre = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A61Estadonombre", A61Estadonombre);
         A39cantBeneficiarios = 0;
         n39cantBeneficiarios = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A39cantBeneficiarios", StringUtil.LTrim( StringUtil.Str( (decimal)(A39cantBeneficiarios), 9, 0)));
         n39cantBeneficiarios = ((0==A39cantBeneficiarios) ? true : false);
         A48Actividaddescripcion = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A48Actividaddescripcion", A48Actividaddescripcion);
         A40informe = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A40informe", StringUtil.Str( (decimal)(A40informe), 1, 0));
         Z30nombreEvento = "";
         Z31inicio = DateTime.MinValue;
         Z32fin = DateTime.MinValue;
         Z36objGeneral = "";
         Z37objEspecificos = "";
         Z38descripcionActividad = "";
         Z39cantBeneficiarios = 0;
         Z40informe = 0;
         Z19Comunidadid = 0;
         Z22TipoBeneficiarioid = 0;
         Z12Estadoid = 0;
         Z3Actividadid = 0;
         Z29PlanidProyecto = 0;
         Z33PlanidResponsable = 0;
         Z34PlanidFinanciero = 0;
         Z35PlanidTecnico = 0;
      }

      protected void InitAll0G16( )
      {
         A13Planid = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A13Planid", StringUtil.LTrim( StringUtil.Str( (decimal)(A13Planid), 9, 0)));
         InitializeNonKey0G16( ) ;
      }

      protected void StandaloneModalInsert( )
      {
      }

      protected void define_styles( )
      {
         AddStyleSheetFile("calendar-system.css", "");
         AddThemeStyleSheetFile("", context.GetTheme( )+".css", "?"+GetCacheInvalidationToken( ));
         bool outputEnabled = isOutputEnabled( ) ;
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         idxLst = 1;
         while ( idxLst <= Form.Jscriptsrc.Count )
         {
            context.AddJavascriptSource(StringUtil.RTrim( ((String)Form.Jscriptsrc.Item(idxLst))), "?20191514465541", true);
            idxLst = (int)(idxLst+1);
         }
         if ( ! outputEnabled )
         {
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
         }
         /* End function define_styles */
      }

      protected void include_jscripts( )
      {
         context.AddJavascriptSource("messages.eng.js", "?"+GetCacheInvalidationToken( ), false);
         context.AddJavascriptSource("plan.js", "?20191514465542", false);
         /* End function include_jscripts */
      }

      protected void init_default_properties( )
      {
         lblTitle_Internalname = "TITLE";
         divTitlecontainer_Internalname = "TITLECONTAINER";
         bttBtn_first_Internalname = "BTN_FIRST";
         bttBtn_previous_Internalname = "BTN_PREVIOUS";
         bttBtn_next_Internalname = "BTN_NEXT";
         bttBtn_last_Internalname = "BTN_LAST";
         bttBtn_select_Internalname = "BTN_SELECT";
         divToolbarcell_Internalname = "TOOLBARCELL";
         edtPlanidProyecto_Internalname = "PLANIDPROYECTO";
         edtnombreEvento_Internalname = "NOMBREEVENTO";
         edtComunidadid_Internalname = "COMUNIDADID";
         edtComunidadnombre_Internalname = "COMUNIDADNOMBRE";
         edtinicio_Internalname = "INICIO";
         edtfin_Internalname = "FIN";
         edtTipoBeneficiarioid_Internalname = "TIPOBENEFICIARIOID";
         edtPlanidResponsable_Internalname = "PLANIDRESPONSABLE";
         edtPlanidFinanciero_Internalname = "PLANIDFINANCIERO";
         edtPlanidTecnico_Internalname = "PLANIDTECNICO";
         edtobjGeneral_Internalname = "OBJGENERAL";
         edtobjEspecificos_Internalname = "OBJESPECIFICOS";
         edtdescripcionActividad_Internalname = "DESCRIPCIONACTIVIDAD";
         edtEstadoid_Internalname = "ESTADOID";
         edtEstadonombre_Internalname = "ESTADONOMBRE";
         edtcantBeneficiarios_Internalname = "CANTBENEFICIARIOS";
         edtActividadid_Internalname = "ACTIVIDADID";
         edtActividaddescripcion_Internalname = "ACTIVIDADDESCRIPCION";
         divFormcontainer_Internalname = "FORMCONTAINER";
         bttBtn_enter_Internalname = "BTN_ENTER";
         bttBtn_cancel_Internalname = "BTN_CANCEL";
         bttBtn_delete_Internalname = "BTN_DELETE";
         divMaintable_Internalname = "MAINTABLE";
         Form.Internalname = "FORM";
         imgprompt_29_Internalname = "PROMPT_29";
         imgprompt_19_Internalname = "PROMPT_19";
         imgprompt_22_Internalname = "PROMPT_22";
         imgprompt_33_Internalname = "PROMPT_33";
         imgprompt_34_Internalname = "PROMPT_34";
         imgprompt_35_Internalname = "PROMPT_35";
         imgprompt_12_Internalname = "PROMPT_12";
         imgprompt_3_Internalname = "PROMPT_3";
      }

      public override void initialize_properties( )
      {
         context.SetDefaultTheme("Carmine");
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         init_default_properties( ) ;
         Form.Headerrawhtml = "";
         Form.Background = "";
         Form.Textcolor = 0;
         Form.Backcolor = (int)(0xFFFFFF);
         Form.Caption = "Plan";
         bttBtn_delete_Enabled = 0;
         bttBtn_delete_Visible = 1;
         bttBtn_cancel_Visible = 1;
         bttBtn_enter_Enabled = 1;
         bttBtn_enter_Visible = 1;
         edtActividaddescripcion_Enabled = 0;
         imgprompt_3_Visible = 1;
         imgprompt_3_Link = "";
         edtActividadid_Jsonclick = "";
         edtActividadid_Enabled = 1;
         edtcantBeneficiarios_Jsonclick = "";
         edtcantBeneficiarios_Enabled = 1;
         edtEstadonombre_Jsonclick = "";
         edtEstadonombre_Enabled = 0;
         imgprompt_12_Visible = 1;
         imgprompt_12_Link = "";
         edtEstadoid_Jsonclick = "";
         edtEstadoid_Enabled = 1;
         edtdescripcionActividad_Enabled = 1;
         edtobjEspecificos_Enabled = 1;
         edtobjGeneral_Jsonclick = "";
         edtobjGeneral_Enabled = 1;
         imgprompt_35_Visible = 1;
         imgprompt_35_Link = "";
         edtPlanidTecnico_Jsonclick = "";
         edtPlanidTecnico_Enabled = 1;
         imgprompt_34_Visible = 1;
         imgprompt_34_Link = "";
         edtPlanidFinanciero_Jsonclick = "";
         edtPlanidFinanciero_Enabled = 1;
         imgprompt_33_Visible = 1;
         imgprompt_33_Link = "";
         edtPlanidResponsable_Jsonclick = "";
         edtPlanidResponsable_Enabled = 1;
         imgprompt_22_Visible = 1;
         imgprompt_22_Link = "";
         edtTipoBeneficiarioid_Jsonclick = "";
         edtTipoBeneficiarioid_Enabled = 1;
         edtfin_Jsonclick = "";
         edtfin_Enabled = 1;
         edtinicio_Jsonclick = "";
         edtinicio_Enabled = 1;
         edtComunidadnombre_Jsonclick = "";
         edtComunidadnombre_Enabled = 0;
         imgprompt_19_Visible = 1;
         imgprompt_19_Link = "";
         edtComunidadid_Jsonclick = "";
         edtComunidadid_Enabled = 1;
         edtnombreEvento_Jsonclick = "";
         edtnombreEvento_Enabled = 1;
         imgprompt_29_Visible = 1;
         imgprompt_29_Link = "";
         edtPlanidProyecto_Jsonclick = "";
         edtPlanidProyecto_Enabled = 1;
         bttBtn_select_Visible = 1;
         bttBtn_last_Visible = 1;
         bttBtn_next_Visible = 1;
         bttBtn_previous_Visible = 1;
         bttBtn_first_Visible = 1;
         context.GX_msglist.DisplayMode = 1;
         if ( context.isSpaRequest( ) )
         {
            enableJsOutput();
         }
      }

      protected void dynload_actions( )
      {
         /* End function dynload_actions */
      }

      protected void init_web_controls( )
      {
         /* End function init_web_controls */
      }

      public void Valid_Planidproyecto( int GX_Parm1 )
      {
         A29PlanidProyecto = GX_Parm1;
         /* Using cursor T000G32 */
         pr_datastore1.execute(30, new Object[] {A29PlanidProyecto});
         if ( (pr_datastore1.getStatus(30) == 101) )
         {
            GX_msglist.addItem("No matching 'Plan Proyecto'.", "ForeignKeyNotFound", 1, "PLANIDPROYECTO");
            AnyError = 1;
            GX_FocusControl = edtPlanidProyecto_Internalname;
         }
         pr_datastore1.close(30);
         dynload_actions( ) ;
         isValidOutput.Add(context.GX_msglist.ToJavascriptSource());
         isValidOutput.Add(context.httpAjaxContext.ajax_rsp_get_hiddens( ));
         context.GX_webresponse.AddString(isValidOutput.ToJSonString());
         wbTemp = context.ResponseContentType( "application/json");
      }

      public void Valid_Comunidadid( int GX_Parm1 ,
                                     String GX_Parm2 )
      {
         A19Comunidadid = GX_Parm1;
         A130Comunidadnombre = GX_Parm2;
         /* Using cursor T000G27 */
         pr_datastore1.execute(25, new Object[] {A19Comunidadid});
         if ( (pr_datastore1.getStatus(25) == 101) )
         {
            GX_msglist.addItem("No matching 'Comunidad'.", "ForeignKeyNotFound", 1, "COMUNIDADID");
            AnyError = 1;
            GX_FocusControl = edtComunidadid_Internalname;
         }
         A130Comunidadnombre = T000G27_A130Comunidadnombre[0];
         pr_datastore1.close(25);
         dynload_actions( ) ;
         if ( AnyError == 1 )
         {
            A130Comunidadnombre = "";
         }
         isValidOutput.Add(A130Comunidadnombre);
         isValidOutput.Add(context.GX_msglist.ToJavascriptSource());
         isValidOutput.Add(context.httpAjaxContext.ajax_rsp_get_hiddens( ));
         context.GX_webresponse.AddString(isValidOutput.ToJSonString());
         wbTemp = context.ResponseContentType( "application/json");
      }

      public void Valid_Tipobeneficiarioid( int GX_Parm1 )
      {
         A22TipoBeneficiarioid = GX_Parm1;
         /* Using cursor T000G33 */
         pr_datastore1.execute(31, new Object[] {A22TipoBeneficiarioid});
         if ( (pr_datastore1.getStatus(31) == 101) )
         {
            GX_msglist.addItem("No matching 'Tipo Beneficiario'.", "ForeignKeyNotFound", 1, "TIPOBENEFICIARIOID");
            AnyError = 1;
            GX_FocusControl = edtTipoBeneficiarioid_Internalname;
         }
         pr_datastore1.close(31);
         dynload_actions( ) ;
         isValidOutput.Add(context.GX_msglist.ToJavascriptSource());
         isValidOutput.Add(context.httpAjaxContext.ajax_rsp_get_hiddens( ));
         context.GX_webresponse.AddString(isValidOutput.ToJSonString());
         wbTemp = context.ResponseContentType( "application/json");
      }

      public void Valid_Planidresponsable( int GX_Parm1 )
      {
         A33PlanidResponsable = GX_Parm1;
         /* Using cursor T000G34 */
         pr_datastore1.execute(32, new Object[] {A33PlanidResponsable});
         if ( (pr_datastore1.getStatus(32) == 101) )
         {
            GX_msglist.addItem("No matching 'Plan Responsable'.", "ForeignKeyNotFound", 1, "PLANIDRESPONSABLE");
            AnyError = 1;
            GX_FocusControl = edtPlanidResponsable_Internalname;
         }
         pr_datastore1.close(32);
         dynload_actions( ) ;
         isValidOutput.Add(context.GX_msglist.ToJavascriptSource());
         isValidOutput.Add(context.httpAjaxContext.ajax_rsp_get_hiddens( ));
         context.GX_webresponse.AddString(isValidOutput.ToJSonString());
         wbTemp = context.ResponseContentType( "application/json");
      }

      public void Valid_Planidfinanciero( int GX_Parm1 )
      {
         A34PlanidFinanciero = GX_Parm1;
         /* Using cursor T000G35 */
         pr_datastore1.execute(33, new Object[] {A34PlanidFinanciero});
         if ( (pr_datastore1.getStatus(33) == 101) )
         {
            GX_msglist.addItem("No matching 'Plan Responsable2'.", "ForeignKeyNotFound", 1, "PLANIDFINANCIERO");
            AnyError = 1;
            GX_FocusControl = edtPlanidFinanciero_Internalname;
         }
         pr_datastore1.close(33);
         dynload_actions( ) ;
         isValidOutput.Add(context.GX_msglist.ToJavascriptSource());
         isValidOutput.Add(context.httpAjaxContext.ajax_rsp_get_hiddens( ));
         context.GX_webresponse.AddString(isValidOutput.ToJSonString());
         wbTemp = context.ResponseContentType( "application/json");
      }

      public void Valid_Planidtecnico( int GX_Parm1 )
      {
         A35PlanidTecnico = GX_Parm1;
         /* Using cursor T000G36 */
         pr_datastore1.execute(34, new Object[] {A35PlanidTecnico});
         if ( (pr_datastore1.getStatus(34) == 101) )
         {
            GX_msglist.addItem("No matching 'Plan Responsable3'.", "ForeignKeyNotFound", 1, "PLANIDTECNICO");
            AnyError = 1;
            GX_FocusControl = edtPlanidTecnico_Internalname;
         }
         pr_datastore1.close(34);
         dynload_actions( ) ;
         isValidOutput.Add(context.GX_msglist.ToJavascriptSource());
         isValidOutput.Add(context.httpAjaxContext.ajax_rsp_get_hiddens( ));
         context.GX_webresponse.AddString(isValidOutput.ToJSonString());
         wbTemp = context.ResponseContentType( "application/json");
      }

      public void Valid_Estadoid( int GX_Parm1 ,
                                  String GX_Parm2 )
      {
         A12Estadoid = GX_Parm1;
         A61Estadonombre = GX_Parm2;
         /* Using cursor T000G28 */
         pr_datastore1.execute(26, new Object[] {A12Estadoid});
         if ( (pr_datastore1.getStatus(26) == 101) )
         {
            GX_msglist.addItem("No matching 'Estado'.", "ForeignKeyNotFound", 1, "ESTADOID");
            AnyError = 1;
            GX_FocusControl = edtEstadoid_Internalname;
         }
         A61Estadonombre = T000G28_A61Estadonombre[0];
         pr_datastore1.close(26);
         dynload_actions( ) ;
         if ( AnyError == 1 )
         {
            A61Estadonombre = "";
         }
         isValidOutput.Add(A61Estadonombre);
         isValidOutput.Add(context.GX_msglist.ToJavascriptSource());
         isValidOutput.Add(context.httpAjaxContext.ajax_rsp_get_hiddens( ));
         context.GX_webresponse.AddString(isValidOutput.ToJSonString());
         wbTemp = context.ResponseContentType( "application/json");
      }

      public void Valid_Actividadid( int GX_Parm1 ,
                                     String GX_Parm2 )
      {
         A3Actividadid = GX_Parm1;
         n3Actividadid = false;
         A48Actividaddescripcion = GX_Parm2;
         /* Using cursor T000G29 */
         pr_datastore1.execute(27, new Object[] {n3Actividadid, A3Actividadid});
         if ( (pr_datastore1.getStatus(27) == 101) )
         {
            if ( ! ( (0==A3Actividadid) ) )
            {
               GX_msglist.addItem("No matching 'Actividad'.", "ForeignKeyNotFound", 1, "ACTIVIDADID");
               AnyError = 1;
               GX_FocusControl = edtActividadid_Internalname;
            }
         }
         A48Actividaddescripcion = T000G29_A48Actividaddescripcion[0];
         pr_datastore1.close(27);
         dynload_actions( ) ;
         if ( AnyError == 1 )
         {
            A48Actividaddescripcion = "";
         }
         isValidOutput.Add(A48Actividaddescripcion);
         isValidOutput.Add(context.GX_msglist.ToJavascriptSource());
         isValidOutput.Add(context.httpAjaxContext.ajax_rsp_get_hiddens( ));
         context.GX_webresponse.AddString(isValidOutput.ToJSonString());
         wbTemp = context.ResponseContentType( "application/json");
      }

      public override bool SupportAjaxEvent( )
      {
         return true ;
      }

      public override void InitializeDynEvents( )
      {
         setEventMetadata("ENTER","{handler:'UserMainFullajax',iparms:[{postForm:true},{av:'Gx_mode',fld:'vMODE',pic:'@!',hsh:true},{av:'AV7Planid',fld:'vPLANID',pic:'ZZZZZZZZ9',hsh:true}]");
         setEventMetadata("ENTER",",oparms:[]}");
         setEventMetadata("REFRESH","{handler:'Refresh',iparms:[{av:'Gx_mode',fld:'vMODE',pic:'@!',hsh:true},{av:'AV7Planid',fld:'vPLANID',pic:'ZZZZZZZZ9',hsh:true},{av:'AV11Insert_PlanidProyecto',fld:'vINSERT_PLANIDPROYECTO',pic:'ZZZZZZZZ9'},{av:'AV12Insert_Comunidadid',fld:'vINSERT_COMUNIDADID',pic:'ZZZZZZZZ9'},{av:'AV13Insert_TipoBeneficiarioid',fld:'vINSERT_TIPOBENEFICIARIOID',pic:'ZZZZZZZZ9'},{av:'AV14Insert_PlanidResponsable',fld:'vINSERT_PLANIDRESPONSABLE',pic:'ZZZZZZZZ9'},{av:'AV15Insert_PlanidFinanciero',fld:'vINSERT_PLANIDFINANCIERO',pic:'ZZZZZZZZ9'},{av:'AV16Insert_PlanidTecnico',fld:'vINSERT_PLANIDTECNICO',pic:'ZZZZZZZZ9'},{av:'AV17Insert_Estadoid',fld:'vINSERT_ESTADOID',pic:'ZZZZZZZZ9'},{av:'AV18Insert_Actividadid',fld:'vINSERT_ACTIVIDADID',pic:'ZZZZZZZZ9'},{av:'A13Planid',fld:'PLANID',pic:'ZZZZZZZZ9'},{av:'A40informe',fld:'INFORME',pic:'9'}]");
         setEventMetadata("REFRESH",",oparms:[]}");
         setEventMetadata("AFTER TRN","{handler:'E120G2',iparms:[{av:'Gx_mode',fld:'vMODE',pic:'@!',hsh:true},{av:'AV9TrnContext',fld:'vTRNCONTEXT',pic:''}]");
         setEventMetadata("AFTER TRN",",oparms:[]}");
         return  ;
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
         pr_datastore1.close(1);
         pr_datastore1.close(25);
         pr_datastore1.close(31);
         pr_datastore1.close(26);
         pr_datastore1.close(27);
         pr_datastore1.close(30);
         pr_datastore1.close(32);
         pr_datastore1.close(33);
         pr_datastore1.close(34);
      }

      public override void initialize( )
      {
         sPrefix = "";
         wcpOGx_mode = "";
         Z30nombreEvento = "";
         Z31inicio = DateTime.MinValue;
         Z32fin = DateTime.MinValue;
         Z36objGeneral = "";
         Z37objEspecificos = "";
         Z38descripcionActividad = "";
         scmdbuf = "";
         gxfirstwebparm = "";
         gxfirstwebparm_bkp = "";
         GXKey = "";
         PreviousTooltip = "";
         PreviousCaption = "";
         Form = new GXWebForm();
         GX_FocusControl = "";
         lblTitle_Jsonclick = "";
         ClassString = "";
         StyleString = "";
         TempTags = "";
         bttBtn_first_Jsonclick = "";
         bttBtn_previous_Jsonclick = "";
         bttBtn_next_Jsonclick = "";
         bttBtn_last_Jsonclick = "";
         bttBtn_select_Jsonclick = "";
         sImgUrl = "";
         A30nombreEvento = "";
         A130Comunidadnombre = "";
         A31inicio = DateTime.MinValue;
         A32fin = DateTime.MinValue;
         A36objGeneral = "";
         A37objEspecificos = "";
         A38descripcionActividad = "";
         A61Estadonombre = "";
         A48Actividaddescripcion = "";
         bttBtn_enter_Jsonclick = "";
         bttBtn_cancel_Jsonclick = "";
         bttBtn_delete_Jsonclick = "";
         AV20Pgmname = "";
         forbiddenHiddens = "";
         hsh = "";
         sMode16 = "";
         sEvt = "";
         EvtGridId = "";
         EvtRowId = "";
         sEvtType = "";
         AV9TrnContext = new SdtTransactionContext(context);
         AV10WebSession = context.GetSession();
         AV19TrnContextAtt = new SdtTransactionContext_Attribute(context);
         Z130Comunidadnombre = "";
         Z61Estadonombre = "";
         Z48Actividaddescripcion = "";
         T000G7_A48Actividaddescripcion = new String[] {""} ;
         T000G6_A61Estadonombre = new String[] {""} ;
         T000G4_A130Comunidadnombre = new String[] {""} ;
         T000G12_A13Planid = new int[1] ;
         T000G12_A30nombreEvento = new String[] {""} ;
         T000G12_A130Comunidadnombre = new String[] {""} ;
         T000G12_A31inicio = new DateTime[] {DateTime.MinValue} ;
         T000G12_A32fin = new DateTime[] {DateTime.MinValue} ;
         T000G12_A36objGeneral = new String[] {""} ;
         T000G12_A37objEspecificos = new String[] {""} ;
         T000G12_A38descripcionActividad = new String[] {""} ;
         T000G12_A61Estadonombre = new String[] {""} ;
         T000G12_A39cantBeneficiarios = new int[1] ;
         T000G12_n39cantBeneficiarios = new bool[] {false} ;
         T000G12_A48Actividaddescripcion = new String[] {""} ;
         T000G12_A40informe = new short[1] ;
         T000G12_A19Comunidadid = new int[1] ;
         T000G12_A22TipoBeneficiarioid = new int[1] ;
         T000G12_A12Estadoid = new int[1] ;
         T000G12_A3Actividadid = new int[1] ;
         T000G12_n3Actividadid = new bool[] {false} ;
         T000G12_A29PlanidProyecto = new int[1] ;
         T000G12_A33PlanidResponsable = new int[1] ;
         T000G12_A34PlanidFinanciero = new int[1] ;
         T000G12_A35PlanidTecnico = new int[1] ;
         T000G8_A29PlanidProyecto = new int[1] ;
         T000G5_A22TipoBeneficiarioid = new int[1] ;
         T000G9_A33PlanidResponsable = new int[1] ;
         T000G10_A34PlanidFinanciero = new int[1] ;
         T000G11_A35PlanidTecnico = new int[1] ;
         T000G13_A29PlanidProyecto = new int[1] ;
         T000G14_A130Comunidadnombre = new String[] {""} ;
         T000G15_A22TipoBeneficiarioid = new int[1] ;
         T000G16_A33PlanidResponsable = new int[1] ;
         T000G17_A34PlanidFinanciero = new int[1] ;
         T000G18_A35PlanidTecnico = new int[1] ;
         T000G19_A61Estadonombre = new String[] {""} ;
         T000G20_A48Actividaddescripcion = new String[] {""} ;
         T000G21_A13Planid = new int[1] ;
         T000G3_A13Planid = new int[1] ;
         T000G3_A30nombreEvento = new String[] {""} ;
         T000G3_A31inicio = new DateTime[] {DateTime.MinValue} ;
         T000G3_A32fin = new DateTime[] {DateTime.MinValue} ;
         T000G3_A36objGeneral = new String[] {""} ;
         T000G3_A37objEspecificos = new String[] {""} ;
         T000G3_A38descripcionActividad = new String[] {""} ;
         T000G3_A39cantBeneficiarios = new int[1] ;
         T000G3_n39cantBeneficiarios = new bool[] {false} ;
         T000G3_A40informe = new short[1] ;
         T000G3_A19Comunidadid = new int[1] ;
         T000G3_A22TipoBeneficiarioid = new int[1] ;
         T000G3_A12Estadoid = new int[1] ;
         T000G3_A3Actividadid = new int[1] ;
         T000G3_n3Actividadid = new bool[] {false} ;
         T000G3_A29PlanidProyecto = new int[1] ;
         T000G3_A33PlanidResponsable = new int[1] ;
         T000G3_A34PlanidFinanciero = new int[1] ;
         T000G3_A35PlanidTecnico = new int[1] ;
         T000G22_A13Planid = new int[1] ;
         T000G23_A13Planid = new int[1] ;
         T000G2_A13Planid = new int[1] ;
         T000G2_A30nombreEvento = new String[] {""} ;
         T000G2_A31inicio = new DateTime[] {DateTime.MinValue} ;
         T000G2_A32fin = new DateTime[] {DateTime.MinValue} ;
         T000G2_A36objGeneral = new String[] {""} ;
         T000G2_A37objEspecificos = new String[] {""} ;
         T000G2_A38descripcionActividad = new String[] {""} ;
         T000G2_A39cantBeneficiarios = new int[1] ;
         T000G2_n39cantBeneficiarios = new bool[] {false} ;
         T000G2_A40informe = new short[1] ;
         T000G2_A19Comunidadid = new int[1] ;
         T000G2_A22TipoBeneficiarioid = new int[1] ;
         T000G2_A12Estadoid = new int[1] ;
         T000G2_A3Actividadid = new int[1] ;
         T000G2_n3Actividadid = new bool[] {false} ;
         T000G2_A29PlanidProyecto = new int[1] ;
         T000G2_A33PlanidResponsable = new int[1] ;
         T000G2_A34PlanidFinanciero = new int[1] ;
         T000G2_A35PlanidTecnico = new int[1] ;
         T000G24_A13Planid = new int[1] ;
         T000G27_A130Comunidadnombre = new String[] {""} ;
         T000G28_A61Estadonombre = new String[] {""} ;
         T000G29_A48Actividaddescripcion = new String[] {""} ;
         T000G30_A11Informe_Actividadid = new int[1] ;
         T000G31_A13Planid = new int[1] ;
         sDynURL = "";
         FormProcess = "";
         bodyStyle = "";
         T000G32_A29PlanidProyecto = new int[1] ;
         isValidOutput = new GxUnknownObjectCollection();
         T000G33_A22TipoBeneficiarioid = new int[1] ;
         T000G34_A33PlanidResponsable = new int[1] ;
         T000G35_A34PlanidFinanciero = new int[1] ;
         T000G36_A35PlanidTecnico = new int[1] ;
         pr_datastore1 = new DataStoreProvider(context, new GeneXus.Programs.plan__datastore1(),
            new Object[][] {
                new Object[] {
               T000G2_A13Planid, T000G2_A30nombreEvento, T000G2_A31inicio, T000G2_A32fin, T000G2_A36objGeneral, T000G2_A37objEspecificos, T000G2_A38descripcionActividad, T000G2_A39cantBeneficiarios, T000G2_n39cantBeneficiarios, T000G2_A40informe,
               T000G2_A19Comunidadid, T000G2_A22TipoBeneficiarioid, T000G2_A12Estadoid, T000G2_A3Actividadid, T000G2_n3Actividadid, T000G2_A29PlanidProyecto, T000G2_A33PlanidResponsable, T000G2_A34PlanidFinanciero, T000G2_A35PlanidTecnico
               }
               , new Object[] {
               T000G3_A13Planid, T000G3_A30nombreEvento, T000G3_A31inicio, T000G3_A32fin, T000G3_A36objGeneral, T000G3_A37objEspecificos, T000G3_A38descripcionActividad, T000G3_A39cantBeneficiarios, T000G3_n39cantBeneficiarios, T000G3_A40informe,
               T000G3_A19Comunidadid, T000G3_A22TipoBeneficiarioid, T000G3_A12Estadoid, T000G3_A3Actividadid, T000G3_n3Actividadid, T000G3_A29PlanidProyecto, T000G3_A33PlanidResponsable, T000G3_A34PlanidFinanciero, T000G3_A35PlanidTecnico
               }
               , new Object[] {
               T000G4_A130Comunidadnombre
               }
               , new Object[] {
               T000G5_A22TipoBeneficiarioid
               }
               , new Object[] {
               T000G6_A61Estadonombre
               }
               , new Object[] {
               T000G7_A48Actividaddescripcion
               }
               , new Object[] {
               T000G8_A29PlanidProyecto
               }
               , new Object[] {
               T000G9_A33PlanidResponsable
               }
               , new Object[] {
               T000G10_A34PlanidFinanciero
               }
               , new Object[] {
               T000G11_A35PlanidTecnico
               }
               , new Object[] {
               T000G12_A13Planid, T000G12_A30nombreEvento, T000G12_A130Comunidadnombre, T000G12_A31inicio, T000G12_A32fin, T000G12_A36objGeneral, T000G12_A37objEspecificos, T000G12_A38descripcionActividad, T000G12_A61Estadonombre, T000G12_A39cantBeneficiarios,
               T000G12_n39cantBeneficiarios, T000G12_A48Actividaddescripcion, T000G12_A40informe, T000G12_A19Comunidadid, T000G12_A22TipoBeneficiarioid, T000G12_A12Estadoid, T000G12_A3Actividadid, T000G12_n3Actividadid, T000G12_A29PlanidProyecto, T000G12_A33PlanidResponsable,
               T000G12_A34PlanidFinanciero, T000G12_A35PlanidTecnico
               }
               , new Object[] {
               T000G13_A29PlanidProyecto
               }
               , new Object[] {
               T000G14_A130Comunidadnombre
               }
               , new Object[] {
               T000G15_A22TipoBeneficiarioid
               }
               , new Object[] {
               T000G16_A33PlanidResponsable
               }
               , new Object[] {
               T000G17_A34PlanidFinanciero
               }
               , new Object[] {
               T000G18_A35PlanidTecnico
               }
               , new Object[] {
               T000G19_A61Estadonombre
               }
               , new Object[] {
               T000G20_A48Actividaddescripcion
               }
               , new Object[] {
               T000G21_A13Planid
               }
               , new Object[] {
               T000G22_A13Planid
               }
               , new Object[] {
               T000G23_A13Planid
               }
               , new Object[] {
               T000G24_A13Planid
               }
               , new Object[] {
               }
               , new Object[] {
               }
               , new Object[] {
               T000G27_A130Comunidadnombre
               }
               , new Object[] {
               T000G28_A61Estadonombre
               }
               , new Object[] {
               T000G29_A48Actividaddescripcion
               }
               , new Object[] {
               T000G30_A11Informe_Actividadid
               }
               , new Object[] {
               T000G31_A13Planid
               }
               , new Object[] {
               T000G32_A29PlanidProyecto
               }
               , new Object[] {
               T000G33_A22TipoBeneficiarioid
               }
               , new Object[] {
               T000G34_A33PlanidResponsable
               }
               , new Object[] {
               T000G35_A34PlanidFinanciero
               }
               , new Object[] {
               T000G36_A35PlanidTecnico
               }
            }
         );
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.plan__default(),
            new Object[][] {
            }
         );
         AV20Pgmname = "Plan";
      }

      private short Z40informe ;
      private short GxWebError ;
      private short gxcookieaux ;
      private short IsConfirmed ;
      private short IsModified ;
      private short AnyError ;
      private short nKeyPressed ;
      private short initialized ;
      private short A40informe ;
      private short RcdFound16 ;
      private short GX_JID ;
      private short Gx_BScreen ;
      private short gxajaxcallmode ;
      private short wbTemp ;
      private int wcpOAV7Planid ;
      private int Z13Planid ;
      private int Z39cantBeneficiarios ;
      private int Z19Comunidadid ;
      private int Z22TipoBeneficiarioid ;
      private int Z12Estadoid ;
      private int Z3Actividadid ;
      private int Z29PlanidProyecto ;
      private int Z33PlanidResponsable ;
      private int Z34PlanidFinanciero ;
      private int Z35PlanidTecnico ;
      private int N29PlanidProyecto ;
      private int N19Comunidadid ;
      private int N22TipoBeneficiarioid ;
      private int N33PlanidResponsable ;
      private int N34PlanidFinanciero ;
      private int N35PlanidTecnico ;
      private int N12Estadoid ;
      private int N3Actividadid ;
      private int A29PlanidProyecto ;
      private int A19Comunidadid ;
      private int A22TipoBeneficiarioid ;
      private int A33PlanidResponsable ;
      private int A34PlanidFinanciero ;
      private int A35PlanidTecnico ;
      private int A12Estadoid ;
      private int A3Actividadid ;
      private int AV7Planid ;
      private int trnEnded ;
      private int bttBtn_first_Visible ;
      private int bttBtn_previous_Visible ;
      private int bttBtn_next_Visible ;
      private int bttBtn_last_Visible ;
      private int bttBtn_select_Visible ;
      private int edtPlanidProyecto_Enabled ;
      private int imgprompt_29_Visible ;
      private int edtnombreEvento_Enabled ;
      private int edtComunidadid_Enabled ;
      private int imgprompt_19_Visible ;
      private int edtComunidadnombre_Enabled ;
      private int edtinicio_Enabled ;
      private int edtfin_Enabled ;
      private int edtTipoBeneficiarioid_Enabled ;
      private int imgprompt_22_Visible ;
      private int edtPlanidResponsable_Enabled ;
      private int imgprompt_33_Visible ;
      private int edtPlanidFinanciero_Enabled ;
      private int imgprompt_34_Visible ;
      private int edtPlanidTecnico_Enabled ;
      private int imgprompt_35_Visible ;
      private int edtobjGeneral_Enabled ;
      private int edtobjEspecificos_Enabled ;
      private int edtdescripcionActividad_Enabled ;
      private int edtEstadoid_Enabled ;
      private int imgprompt_12_Visible ;
      private int edtEstadonombre_Enabled ;
      private int A39cantBeneficiarios ;
      private int edtcantBeneficiarios_Enabled ;
      private int edtActividadid_Enabled ;
      private int imgprompt_3_Visible ;
      private int edtActividaddescripcion_Enabled ;
      private int bttBtn_enter_Visible ;
      private int bttBtn_enter_Enabled ;
      private int bttBtn_cancel_Visible ;
      private int bttBtn_delete_Visible ;
      private int bttBtn_delete_Enabled ;
      private int A13Planid ;
      private int AV11Insert_PlanidProyecto ;
      private int AV12Insert_Comunidadid ;
      private int AV13Insert_TipoBeneficiarioid ;
      private int AV14Insert_PlanidResponsable ;
      private int AV15Insert_PlanidFinanciero ;
      private int AV16Insert_PlanidTecnico ;
      private int AV17Insert_Estadoid ;
      private int AV18Insert_Actividadid ;
      private int AV21GXV1 ;
      private int idxLst ;
      private String sPrefix ;
      private String wcpOGx_mode ;
      private String scmdbuf ;
      private String gxfirstwebparm ;
      private String gxfirstwebparm_bkp ;
      private String Gx_mode ;
      private String GXKey ;
      private String PreviousTooltip ;
      private String PreviousCaption ;
      private String GX_FocusControl ;
      private String edtPlanidProyecto_Internalname ;
      private String divMaintable_Internalname ;
      private String divTitlecontainer_Internalname ;
      private String lblTitle_Internalname ;
      private String lblTitle_Jsonclick ;
      private String ClassString ;
      private String StyleString ;
      private String divFormcontainer_Internalname ;
      private String divToolbarcell_Internalname ;
      private String TempTags ;
      private String bttBtn_first_Internalname ;
      private String bttBtn_first_Jsonclick ;
      private String bttBtn_previous_Internalname ;
      private String bttBtn_previous_Jsonclick ;
      private String bttBtn_next_Internalname ;
      private String bttBtn_next_Jsonclick ;
      private String bttBtn_last_Internalname ;
      private String bttBtn_last_Jsonclick ;
      private String bttBtn_select_Internalname ;
      private String bttBtn_select_Jsonclick ;
      private String edtPlanidProyecto_Jsonclick ;
      private String sImgUrl ;
      private String imgprompt_29_Internalname ;
      private String imgprompt_29_Link ;
      private String edtnombreEvento_Internalname ;
      private String edtnombreEvento_Jsonclick ;
      private String edtComunidadid_Internalname ;
      private String edtComunidadid_Jsonclick ;
      private String imgprompt_19_Internalname ;
      private String imgprompt_19_Link ;
      private String edtComunidadnombre_Internalname ;
      private String edtComunidadnombre_Jsonclick ;
      private String edtinicio_Internalname ;
      private String edtinicio_Jsonclick ;
      private String edtfin_Internalname ;
      private String edtfin_Jsonclick ;
      private String edtTipoBeneficiarioid_Internalname ;
      private String edtTipoBeneficiarioid_Jsonclick ;
      private String imgprompt_22_Internalname ;
      private String imgprompt_22_Link ;
      private String edtPlanidResponsable_Internalname ;
      private String edtPlanidResponsable_Jsonclick ;
      private String imgprompt_33_Internalname ;
      private String imgprompt_33_Link ;
      private String edtPlanidFinanciero_Internalname ;
      private String edtPlanidFinanciero_Jsonclick ;
      private String imgprompt_34_Internalname ;
      private String imgprompt_34_Link ;
      private String edtPlanidTecnico_Internalname ;
      private String edtPlanidTecnico_Jsonclick ;
      private String imgprompt_35_Internalname ;
      private String imgprompt_35_Link ;
      private String edtobjGeneral_Internalname ;
      private String edtobjGeneral_Jsonclick ;
      private String edtobjEspecificos_Internalname ;
      private String edtdescripcionActividad_Internalname ;
      private String edtEstadoid_Internalname ;
      private String edtEstadoid_Jsonclick ;
      private String imgprompt_12_Internalname ;
      private String imgprompt_12_Link ;
      private String edtEstadonombre_Internalname ;
      private String edtEstadonombre_Jsonclick ;
      private String edtcantBeneficiarios_Internalname ;
      private String edtcantBeneficiarios_Jsonclick ;
      private String edtActividadid_Internalname ;
      private String edtActividadid_Jsonclick ;
      private String imgprompt_3_Internalname ;
      private String imgprompt_3_Link ;
      private String edtActividaddescripcion_Internalname ;
      private String bttBtn_enter_Internalname ;
      private String bttBtn_enter_Jsonclick ;
      private String bttBtn_cancel_Internalname ;
      private String bttBtn_cancel_Jsonclick ;
      private String bttBtn_delete_Internalname ;
      private String bttBtn_delete_Jsonclick ;
      private String AV20Pgmname ;
      private String forbiddenHiddens ;
      private String hsh ;
      private String sMode16 ;
      private String sEvt ;
      private String EvtGridId ;
      private String EvtRowId ;
      private String sEvtType ;
      private String sDynURL ;
      private String FormProcess ;
      private String bodyStyle ;
      private DateTime Z31inicio ;
      private DateTime Z32fin ;
      private DateTime A31inicio ;
      private DateTime A32fin ;
      private bool entryPointCalled ;
      private bool n3Actividadid ;
      private bool toggleJsOutput ;
      private bool wbErr ;
      private bool n39cantBeneficiarios ;
      private bool returnInSub ;
      private bool Gx_longc ;
      private String Z30nombreEvento ;
      private String Z36objGeneral ;
      private String Z37objEspecificos ;
      private String Z38descripcionActividad ;
      private String A30nombreEvento ;
      private String A130Comunidadnombre ;
      private String A36objGeneral ;
      private String A37objEspecificos ;
      private String A38descripcionActividad ;
      private String A61Estadonombre ;
      private String A48Actividaddescripcion ;
      private String Z130Comunidadnombre ;
      private String Z61Estadonombre ;
      private String Z48Actividaddescripcion ;
      private IGxSession AV10WebSession ;
      private GxUnknownObjectCollection isValidOutput ;
      private IGxDataStore dsDataStore1 ;
      private IGxDataStore dsDefault ;
      private IDataStoreProvider pr_datastore1 ;
      private String[] T000G7_A48Actividaddescripcion ;
      private String[] T000G6_A61Estadonombre ;
      private String[] T000G4_A130Comunidadnombre ;
      private int[] T000G12_A13Planid ;
      private String[] T000G12_A30nombreEvento ;
      private String[] T000G12_A130Comunidadnombre ;
      private DateTime[] T000G12_A31inicio ;
      private DateTime[] T000G12_A32fin ;
      private String[] T000G12_A36objGeneral ;
      private String[] T000G12_A37objEspecificos ;
      private String[] T000G12_A38descripcionActividad ;
      private String[] T000G12_A61Estadonombre ;
      private int[] T000G12_A39cantBeneficiarios ;
      private bool[] T000G12_n39cantBeneficiarios ;
      private String[] T000G12_A48Actividaddescripcion ;
      private short[] T000G12_A40informe ;
      private int[] T000G12_A19Comunidadid ;
      private int[] T000G12_A22TipoBeneficiarioid ;
      private int[] T000G12_A12Estadoid ;
      private int[] T000G12_A3Actividadid ;
      private bool[] T000G12_n3Actividadid ;
      private int[] T000G12_A29PlanidProyecto ;
      private int[] T000G12_A33PlanidResponsable ;
      private int[] T000G12_A34PlanidFinanciero ;
      private int[] T000G12_A35PlanidTecnico ;
      private int[] T000G8_A29PlanidProyecto ;
      private int[] T000G5_A22TipoBeneficiarioid ;
      private int[] T000G9_A33PlanidResponsable ;
      private int[] T000G10_A34PlanidFinanciero ;
      private int[] T000G11_A35PlanidTecnico ;
      private int[] T000G13_A29PlanidProyecto ;
      private String[] T000G14_A130Comunidadnombre ;
      private int[] T000G15_A22TipoBeneficiarioid ;
      private int[] T000G16_A33PlanidResponsable ;
      private int[] T000G17_A34PlanidFinanciero ;
      private int[] T000G18_A35PlanidTecnico ;
      private String[] T000G19_A61Estadonombre ;
      private String[] T000G20_A48Actividaddescripcion ;
      private int[] T000G21_A13Planid ;
      private int[] T000G3_A13Planid ;
      private String[] T000G3_A30nombreEvento ;
      private DateTime[] T000G3_A31inicio ;
      private DateTime[] T000G3_A32fin ;
      private String[] T000G3_A36objGeneral ;
      private String[] T000G3_A37objEspecificos ;
      private String[] T000G3_A38descripcionActividad ;
      private int[] T000G3_A39cantBeneficiarios ;
      private bool[] T000G3_n39cantBeneficiarios ;
      private short[] T000G3_A40informe ;
      private int[] T000G3_A19Comunidadid ;
      private int[] T000G3_A22TipoBeneficiarioid ;
      private int[] T000G3_A12Estadoid ;
      private int[] T000G3_A3Actividadid ;
      private bool[] T000G3_n3Actividadid ;
      private int[] T000G3_A29PlanidProyecto ;
      private int[] T000G3_A33PlanidResponsable ;
      private int[] T000G3_A34PlanidFinanciero ;
      private int[] T000G3_A35PlanidTecnico ;
      private int[] T000G22_A13Planid ;
      private int[] T000G23_A13Planid ;
      private int[] T000G2_A13Planid ;
      private String[] T000G2_A30nombreEvento ;
      private DateTime[] T000G2_A31inicio ;
      private DateTime[] T000G2_A32fin ;
      private String[] T000G2_A36objGeneral ;
      private String[] T000G2_A37objEspecificos ;
      private String[] T000G2_A38descripcionActividad ;
      private int[] T000G2_A39cantBeneficiarios ;
      private bool[] T000G2_n39cantBeneficiarios ;
      private short[] T000G2_A40informe ;
      private int[] T000G2_A19Comunidadid ;
      private int[] T000G2_A22TipoBeneficiarioid ;
      private int[] T000G2_A12Estadoid ;
      private int[] T000G2_A3Actividadid ;
      private bool[] T000G2_n3Actividadid ;
      private int[] T000G2_A29PlanidProyecto ;
      private int[] T000G2_A33PlanidResponsable ;
      private int[] T000G2_A34PlanidFinanciero ;
      private int[] T000G2_A35PlanidTecnico ;
      private int[] T000G24_A13Planid ;
      private String[] T000G27_A130Comunidadnombre ;
      private String[] T000G28_A61Estadonombre ;
      private String[] T000G29_A48Actividaddescripcion ;
      private int[] T000G30_A11Informe_Actividadid ;
      private IDataStoreProvider pr_default ;
      private int[] T000G31_A13Planid ;
      private int[] T000G32_A29PlanidProyecto ;
      private int[] T000G33_A22TipoBeneficiarioid ;
      private int[] T000G34_A33PlanidResponsable ;
      private int[] T000G35_A34PlanidFinanciero ;
      private int[] T000G36_A35PlanidTecnico ;
      private GXWebForm Form ;
      private SdtTransactionContext AV9TrnContext ;
      private SdtTransactionContext_Attribute AV19TrnContextAtt ;
   }

   public class plan__datastore1 : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
         ,new ForEachCursor(def[2])
         ,new ForEachCursor(def[3])
         ,new ForEachCursor(def[4])
         ,new ForEachCursor(def[5])
         ,new ForEachCursor(def[6])
         ,new ForEachCursor(def[7])
         ,new ForEachCursor(def[8])
         ,new ForEachCursor(def[9])
         ,new ForEachCursor(def[10])
         ,new ForEachCursor(def[11])
         ,new ForEachCursor(def[12])
         ,new ForEachCursor(def[13])
         ,new ForEachCursor(def[14])
         ,new ForEachCursor(def[15])
         ,new ForEachCursor(def[16])
         ,new ForEachCursor(def[17])
         ,new ForEachCursor(def[18])
         ,new ForEachCursor(def[19])
         ,new ForEachCursor(def[20])
         ,new ForEachCursor(def[21])
         ,new ForEachCursor(def[22])
         ,new UpdateCursor(def[23])
         ,new UpdateCursor(def[24])
         ,new ForEachCursor(def[25])
         ,new ForEachCursor(def[26])
         ,new ForEachCursor(def[27])
         ,new ForEachCursor(def[28])
         ,new ForEachCursor(def[29])
         ,new ForEachCursor(def[30])
         ,new ForEachCursor(def[31])
         ,new ForEachCursor(def[32])
         ,new ForEachCursor(def[33])
         ,new ForEachCursor(def[34])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmT000G12 ;
          prmT000G12 = new Object[] {
          new Object[] {"@Planid",SqlDbType.Int,9,0}
          } ;
          Object[] prmT000G8 ;
          prmT000G8 = new Object[] {
          new Object[] {"@PlanidProyecto",SqlDbType.Int,9,0}
          } ;
          Object[] prmT000G4 ;
          prmT000G4 = new Object[] {
          new Object[] {"@Comunidadid",SqlDbType.Int,9,0}
          } ;
          Object[] prmT000G5 ;
          prmT000G5 = new Object[] {
          new Object[] {"@TipoBeneficiarioid",SqlDbType.Int,9,0}
          } ;
          Object[] prmT000G9 ;
          prmT000G9 = new Object[] {
          new Object[] {"@PlanidResponsable",SqlDbType.Int,9,0}
          } ;
          Object[] prmT000G10 ;
          prmT000G10 = new Object[] {
          new Object[] {"@PlanidFinanciero",SqlDbType.Int,9,0}
          } ;
          Object[] prmT000G11 ;
          prmT000G11 = new Object[] {
          new Object[] {"@PlanidTecnico",SqlDbType.Int,9,0}
          } ;
          Object[] prmT000G6 ;
          prmT000G6 = new Object[] {
          new Object[] {"@Estadoid",SqlDbType.Int,9,0}
          } ;
          Object[] prmT000G7 ;
          prmT000G7 = new Object[] {
          new Object[] {"@Actividadid",SqlDbType.Int,9,0}
          } ;
          Object[] prmT000G13 ;
          prmT000G13 = new Object[] {
          new Object[] {"@PlanidProyecto",SqlDbType.Int,9,0}
          } ;
          Object[] prmT000G14 ;
          prmT000G14 = new Object[] {
          new Object[] {"@Comunidadid",SqlDbType.Int,9,0}
          } ;
          Object[] prmT000G15 ;
          prmT000G15 = new Object[] {
          new Object[] {"@TipoBeneficiarioid",SqlDbType.Int,9,0}
          } ;
          Object[] prmT000G16 ;
          prmT000G16 = new Object[] {
          new Object[] {"@PlanidResponsable",SqlDbType.Int,9,0}
          } ;
          Object[] prmT000G17 ;
          prmT000G17 = new Object[] {
          new Object[] {"@PlanidFinanciero",SqlDbType.Int,9,0}
          } ;
          Object[] prmT000G18 ;
          prmT000G18 = new Object[] {
          new Object[] {"@PlanidTecnico",SqlDbType.Int,9,0}
          } ;
          Object[] prmT000G19 ;
          prmT000G19 = new Object[] {
          new Object[] {"@Estadoid",SqlDbType.Int,9,0}
          } ;
          Object[] prmT000G20 ;
          prmT000G20 = new Object[] {
          new Object[] {"@Actividadid",SqlDbType.Int,9,0}
          } ;
          Object[] prmT000G21 ;
          prmT000G21 = new Object[] {
          new Object[] {"@Planid",SqlDbType.Int,9,0}
          } ;
          Object[] prmT000G3 ;
          prmT000G3 = new Object[] {
          new Object[] {"@Planid",SqlDbType.Int,9,0}
          } ;
          Object[] prmT000G22 ;
          prmT000G22 = new Object[] {
          new Object[] {"@Planid",SqlDbType.Int,9,0}
          } ;
          Object[] prmT000G23 ;
          prmT000G23 = new Object[] {
          new Object[] {"@Planid",SqlDbType.Int,9,0}
          } ;
          Object[] prmT000G2 ;
          prmT000G2 = new Object[] {
          new Object[] {"@Planid",SqlDbType.Int,9,0}
          } ;
          Object[] prmT000G24 ;
          prmT000G24 = new Object[] {
          new Object[] {"@nombreEvento",SqlDbType.VarChar,50,0} ,
          new Object[] {"@inicio",SqlDbType.DateTime,8,0} ,
          new Object[] {"@fin",SqlDbType.DateTime,8,0} ,
          new Object[] {"@objGeneral",SqlDbType.VarChar,100,0} ,
          new Object[] {"@objEspecificos",SqlDbType.VarChar,200,0} ,
          new Object[] {"@descripcionActividad",SqlDbType.VarChar,500,0} ,
          new Object[] {"@cantBeneficiarios",SqlDbType.Int,9,0} ,
          new Object[] {"@informe",SqlDbType.SmallInt,1,0} ,
          new Object[] {"@Comunidadid",SqlDbType.Int,9,0} ,
          new Object[] {"@TipoBeneficiarioid",SqlDbType.Int,9,0} ,
          new Object[] {"@Estadoid",SqlDbType.Int,9,0} ,
          new Object[] {"@Actividadid",SqlDbType.Int,9,0} ,
          new Object[] {"@PlanidProyecto",SqlDbType.Int,9,0} ,
          new Object[] {"@PlanidResponsable",SqlDbType.Int,9,0} ,
          new Object[] {"@PlanidFinanciero",SqlDbType.Int,9,0} ,
          new Object[] {"@PlanidTecnico",SqlDbType.Int,9,0}
          } ;
          Object[] prmT000G25 ;
          prmT000G25 = new Object[] {
          new Object[] {"@nombreEvento",SqlDbType.VarChar,50,0} ,
          new Object[] {"@inicio",SqlDbType.DateTime,8,0} ,
          new Object[] {"@fin",SqlDbType.DateTime,8,0} ,
          new Object[] {"@objGeneral",SqlDbType.VarChar,100,0} ,
          new Object[] {"@objEspecificos",SqlDbType.VarChar,200,0} ,
          new Object[] {"@descripcionActividad",SqlDbType.VarChar,500,0} ,
          new Object[] {"@cantBeneficiarios",SqlDbType.Int,9,0} ,
          new Object[] {"@informe",SqlDbType.SmallInt,1,0} ,
          new Object[] {"@Comunidadid",SqlDbType.Int,9,0} ,
          new Object[] {"@TipoBeneficiarioid",SqlDbType.Int,9,0} ,
          new Object[] {"@Estadoid",SqlDbType.Int,9,0} ,
          new Object[] {"@Actividadid",SqlDbType.Int,9,0} ,
          new Object[] {"@PlanidProyecto",SqlDbType.Int,9,0} ,
          new Object[] {"@PlanidResponsable",SqlDbType.Int,9,0} ,
          new Object[] {"@PlanidFinanciero",SqlDbType.Int,9,0} ,
          new Object[] {"@PlanidTecnico",SqlDbType.Int,9,0} ,
          new Object[] {"@Planid",SqlDbType.Int,9,0}
          } ;
          Object[] prmT000G26 ;
          prmT000G26 = new Object[] {
          new Object[] {"@Planid",SqlDbType.Int,9,0}
          } ;
          Object[] prmT000G30 ;
          prmT000G30 = new Object[] {
          new Object[] {"@Planid",SqlDbType.Int,9,0}
          } ;
          Object[] prmT000G31 ;
          prmT000G31 = new Object[] {
          } ;
          Object[] prmT000G32 ;
          prmT000G32 = new Object[] {
          new Object[] {"@PlanidProyecto",SqlDbType.Int,9,0}
          } ;
          Object[] prmT000G27 ;
          prmT000G27 = new Object[] {
          new Object[] {"@Comunidadid",SqlDbType.Int,9,0}
          } ;
          Object[] prmT000G33 ;
          prmT000G33 = new Object[] {
          new Object[] {"@TipoBeneficiarioid",SqlDbType.Int,9,0}
          } ;
          Object[] prmT000G34 ;
          prmT000G34 = new Object[] {
          new Object[] {"@PlanidResponsable",SqlDbType.Int,9,0}
          } ;
          Object[] prmT000G35 ;
          prmT000G35 = new Object[] {
          new Object[] {"@PlanidFinanciero",SqlDbType.Int,9,0}
          } ;
          Object[] prmT000G36 ;
          prmT000G36 = new Object[] {
          new Object[] {"@PlanidTecnico",SqlDbType.Int,9,0}
          } ;
          Object[] prmT000G28 ;
          prmT000G28 = new Object[] {
          new Object[] {"@Estadoid",SqlDbType.Int,9,0}
          } ;
          Object[] prmT000G29 ;
          prmT000G29 = new Object[] {
          new Object[] {"@Actividadid",SqlDbType.Int,9,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("T000G2", "SELECT [id] AS Planid, [nombreEvento], [inicio], [fin], [objGeneral], [objEspecificos], [descripcionActividad], [cantBeneficiarios], [informe], [idLugar] AS Comunidadid, [idTipoBeneficiario] AS TipoBeneficiarioid, [idEstado] AS Estadoid, [idActividad] AS Actividadid, [idProyecto] AS PlanidProyecto, [idResponsable] AS PlanidResponsable, [idFinanciero] AS PlanidFinanciero, [idTecnico] AS PlanidTecnico FROM dbo.[Plan] WITH (UPDLOCK) WHERE [id] = @Planid ",true, GxErrorMask.GX_NOMASK, false, this,prmT000G2,1,0,true,false )
             ,new CursorDef("T000G3", "SELECT [id] AS Planid, [nombreEvento], [inicio], [fin], [objGeneral], [objEspecificos], [descripcionActividad], [cantBeneficiarios], [informe], [idLugar] AS Comunidadid, [idTipoBeneficiario] AS TipoBeneficiarioid, [idEstado] AS Estadoid, [idActividad] AS Actividadid, [idProyecto] AS PlanidProyecto, [idResponsable] AS PlanidResponsable, [idFinanciero] AS PlanidFinanciero, [idTecnico] AS PlanidTecnico FROM dbo.[Plan] WITH (NOLOCK) WHERE [id] = @Planid ",true, GxErrorMask.GX_NOMASK, false, this,prmT000G3,1,0,true,false )
             ,new CursorDef("T000G4", "SELECT [nombre] AS Comunidadnombre FROM dbo.[Comunidad] WITH (NOLOCK) WHERE [id] = @Comunidadid ",true, GxErrorMask.GX_NOMASK, false, this,prmT000G4,1,0,true,false )
             ,new CursorDef("T000G5", "SELECT [id] AS TipoBeneficiarioid FROM dbo.[TipoBeneficiario] WITH (NOLOCK) WHERE [id] = @TipoBeneficiarioid ",true, GxErrorMask.GX_NOMASK, false, this,prmT000G5,1,0,true,false )
             ,new CursorDef("T000G6", "SELECT [nombre] AS Estadonombre FROM dbo.[Estado] WITH (NOLOCK) WHERE [id] = @Estadoid ",true, GxErrorMask.GX_NOMASK, false, this,prmT000G6,1,0,true,false )
             ,new CursorDef("T000G7", "SELECT [descripcion] AS Actividaddescripcion FROM dbo.[Actividad] WITH (NOLOCK) WHERE [id] = @Actividadid ",true, GxErrorMask.GX_NOMASK, false, this,prmT000G7,1,0,true,false )
             ,new CursorDef("T000G8", "SELECT [id] AS PlanidProyecto FROM dbo.[Proyecto] WITH (NOLOCK) WHERE [id] = @PlanidProyecto ",true, GxErrorMask.GX_NOMASK, false, this,prmT000G8,1,0,true,false )
             ,new CursorDef("T000G9", "SELECT [id] AS PlanidResponsable FROM dbo.[Responsable] WITH (NOLOCK) WHERE [id] = @PlanidResponsable ",true, GxErrorMask.GX_NOMASK, false, this,prmT000G9,1,0,true,false )
             ,new CursorDef("T000G10", "SELECT [id] AS PlanidFinanciero FROM dbo.[Responsable] WITH (NOLOCK) WHERE [id] = @PlanidFinanciero ",true, GxErrorMask.GX_NOMASK, false, this,prmT000G10,1,0,true,false )
             ,new CursorDef("T000G11", "SELECT [id] AS PlanidTecnico FROM dbo.[Responsable] WITH (NOLOCK) WHERE [id] = @PlanidTecnico ",true, GxErrorMask.GX_NOMASK, false, this,prmT000G11,1,0,true,false )
             ,new CursorDef("T000G12", "SELECT TM1.[id] AS Planid, TM1.[nombreEvento], T2.[nombre] AS Comunidadnombre, TM1.[inicio], TM1.[fin], TM1.[objGeneral], TM1.[objEspecificos], TM1.[descripcionActividad], T3.[nombre] AS Estadonombre, TM1.[cantBeneficiarios], T4.[descripcion] AS Actividaddescripcion, TM1.[informe], TM1.[idLugar] AS Comunidadid, TM1.[idTipoBeneficiario] AS TipoBeneficiarioid, TM1.[idEstado] AS Estadoid, TM1.[idActividad] AS Actividadid, TM1.[idProyecto] AS PlanidProyecto, TM1.[idResponsable] AS PlanidResponsable, TM1.[idFinanciero] AS PlanidFinanciero, TM1.[idTecnico] AS PlanidTecnico FROM (((dbo.[Plan] TM1 WITH (NOLOCK) INNER JOIN dbo.[Comunidad] T2 WITH (NOLOCK) ON T2.[id] = TM1.[idLugar]) INNER JOIN dbo.[Estado] T3 WITH (NOLOCK) ON T3.[id] = TM1.[idEstado]) LEFT JOIN dbo.[Actividad] T4 WITH (NOLOCK) ON T4.[id] = TM1.[idActividad]) WHERE TM1.[id] = @Planid ORDER BY TM1.[id]  OPTION (FAST 100)",true, GxErrorMask.GX_NOMASK, false, this,prmT000G12,100,0,true,false )
             ,new CursorDef("T000G13", "SELECT [id] AS PlanidProyecto FROM dbo.[Proyecto] WITH (NOLOCK) WHERE [id] = @PlanidProyecto ",true, GxErrorMask.GX_NOMASK, false, this,prmT000G13,1,0,true,false )
             ,new CursorDef("T000G14", "SELECT [nombre] AS Comunidadnombre FROM dbo.[Comunidad] WITH (NOLOCK) WHERE [id] = @Comunidadid ",true, GxErrorMask.GX_NOMASK, false, this,prmT000G14,1,0,true,false )
             ,new CursorDef("T000G15", "SELECT [id] AS TipoBeneficiarioid FROM dbo.[TipoBeneficiario] WITH (NOLOCK) WHERE [id] = @TipoBeneficiarioid ",true, GxErrorMask.GX_NOMASK, false, this,prmT000G15,1,0,true,false )
             ,new CursorDef("T000G16", "SELECT [id] AS PlanidResponsable FROM dbo.[Responsable] WITH (NOLOCK) WHERE [id] = @PlanidResponsable ",true, GxErrorMask.GX_NOMASK, false, this,prmT000G16,1,0,true,false )
             ,new CursorDef("T000G17", "SELECT [id] AS PlanidFinanciero FROM dbo.[Responsable] WITH (NOLOCK) WHERE [id] = @PlanidFinanciero ",true, GxErrorMask.GX_NOMASK, false, this,prmT000G17,1,0,true,false )
             ,new CursorDef("T000G18", "SELECT [id] AS PlanidTecnico FROM dbo.[Responsable] WITH (NOLOCK) WHERE [id] = @PlanidTecnico ",true, GxErrorMask.GX_NOMASK, false, this,prmT000G18,1,0,true,false )
             ,new CursorDef("T000G19", "SELECT [nombre] AS Estadonombre FROM dbo.[Estado] WITH (NOLOCK) WHERE [id] = @Estadoid ",true, GxErrorMask.GX_NOMASK, false, this,prmT000G19,1,0,true,false )
             ,new CursorDef("T000G20", "SELECT [descripcion] AS Actividaddescripcion FROM dbo.[Actividad] WITH (NOLOCK) WHERE [id] = @Actividadid ",true, GxErrorMask.GX_NOMASK, false, this,prmT000G20,1,0,true,false )
             ,new CursorDef("T000G21", "SELECT [id] AS Planid FROM dbo.[Plan] WITH (NOLOCK) WHERE [id] = @Planid  OPTION (FAST 1)",true, GxErrorMask.GX_NOMASK, false, this,prmT000G21,1,0,true,false )
             ,new CursorDef("T000G22", "SELECT TOP 1 [id] AS Planid FROM dbo.[Plan] WITH (NOLOCK) WHERE ( [id] > @Planid) ORDER BY [id]  OPTION (FAST 1)",true, GxErrorMask.GX_NOMASK, false, this,prmT000G22,1,0,true,true )
             ,new CursorDef("T000G23", "SELECT TOP 1 [id] AS Planid FROM dbo.[Plan] WITH (NOLOCK) WHERE ( [id] < @Planid) ORDER BY [id] DESC  OPTION (FAST 1)",true, GxErrorMask.GX_NOMASK, false, this,prmT000G23,1,0,true,true )
             ,new CursorDef("T000G24", "INSERT INTO dbo.[Plan]([nombreEvento], [inicio], [fin], [objGeneral], [objEspecificos], [descripcionActividad], [cantBeneficiarios], [informe], [idLugar], [idTipoBeneficiario], [idEstado], [idActividad], [idProyecto], [idResponsable], [idFinanciero], [idTecnico]) VALUES(@nombreEvento, @inicio, @fin, @objGeneral, @objEspecificos, @descripcionActividad, @cantBeneficiarios, @informe, @Comunidadid, @TipoBeneficiarioid, @Estadoid, @Actividadid, @PlanidProyecto, @PlanidResponsable, @PlanidFinanciero, @PlanidTecnico); SELECT SCOPE_IDENTITY()", GxErrorMask.GX_NOMASK,prmT000G24)
             ,new CursorDef("T000G25", "UPDATE dbo.[Plan] SET [nombreEvento]=@nombreEvento, [inicio]=@inicio, [fin]=@fin, [objGeneral]=@objGeneral, [objEspecificos]=@objEspecificos, [descripcionActividad]=@descripcionActividad, [cantBeneficiarios]=@cantBeneficiarios, [informe]=@informe, [idLugar]=@Comunidadid, [idTipoBeneficiario]=@TipoBeneficiarioid, [idEstado]=@Estadoid, [idActividad]=@Actividadid, [idProyecto]=@PlanidProyecto, [idResponsable]=@PlanidResponsable, [idFinanciero]=@PlanidFinanciero, [idTecnico]=@PlanidTecnico  WHERE [id] = @Planid", GxErrorMask.GX_NOMASK,prmT000G25)
             ,new CursorDef("T000G26", "DELETE FROM dbo.[Plan]  WHERE [id] = @Planid", GxErrorMask.GX_NOMASK,prmT000G26)
             ,new CursorDef("T000G27", "SELECT [nombre] AS Comunidadnombre FROM dbo.[Comunidad] WITH (NOLOCK) WHERE [id] = @Comunidadid ",true, GxErrorMask.GX_NOMASK, false, this,prmT000G27,1,0,true,false )
             ,new CursorDef("T000G28", "SELECT [nombre] AS Estadonombre FROM dbo.[Estado] WITH (NOLOCK) WHERE [id] = @Estadoid ",true, GxErrorMask.GX_NOMASK, false, this,prmT000G28,1,0,true,false )
             ,new CursorDef("T000G29", "SELECT [descripcion] AS Actividaddescripcion FROM dbo.[Actividad] WITH (NOLOCK) WHERE [id] = @Actividadid ",true, GxErrorMask.GX_NOMASK, false, this,prmT000G29,1,0,true,false )
             ,new CursorDef("T000G30", "SELECT TOP 1 [id] AS Informe_Actividadid FROM dbo.[Informe_Actividad] WITH (NOLOCK) WHERE [idPlan] = @Planid ",true, GxErrorMask.GX_NOMASK, false, this,prmT000G30,1,0,true,true )
             ,new CursorDef("T000G31", "SELECT [id] AS Planid FROM dbo.[Plan] WITH (NOLOCK) ORDER BY [id]  OPTION (FAST 100)",true, GxErrorMask.GX_NOMASK, false, this,prmT000G31,100,0,true,false )
             ,new CursorDef("T000G32", "SELECT [id] AS PlanidProyecto FROM dbo.[Proyecto] WITH (NOLOCK) WHERE [id] = @PlanidProyecto ",true, GxErrorMask.GX_NOMASK, false, this,prmT000G32,1,0,true,false )
             ,new CursorDef("T000G33", "SELECT [id] AS TipoBeneficiarioid FROM dbo.[TipoBeneficiario] WITH (NOLOCK) WHERE [id] = @TipoBeneficiarioid ",true, GxErrorMask.GX_NOMASK, false, this,prmT000G33,1,0,true,false )
             ,new CursorDef("T000G34", "SELECT [id] AS PlanidResponsable FROM dbo.[Responsable] WITH (NOLOCK) WHERE [id] = @PlanidResponsable ",true, GxErrorMask.GX_NOMASK, false, this,prmT000G34,1,0,true,false )
             ,new CursorDef("T000G35", "SELECT [id] AS PlanidFinanciero FROM dbo.[Responsable] WITH (NOLOCK) WHERE [id] = @PlanidFinanciero ",true, GxErrorMask.GX_NOMASK, false, this,prmT000G35,1,0,true,false )
             ,new CursorDef("T000G36", "SELECT [id] AS PlanidTecnico FROM dbo.[Responsable] WITH (NOLOCK) WHERE [id] = @PlanidTecnico ",true, GxErrorMask.GX_NOMASK, false, this,prmT000G36,1,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getVarchar(2) ;
                ((DateTime[]) buf[2])[0] = rslt.getGXDate(3) ;
                ((DateTime[]) buf[3])[0] = rslt.getGXDate(4) ;
                ((String[]) buf[4])[0] = rslt.getVarchar(5) ;
                ((String[]) buf[5])[0] = rslt.getVarchar(6) ;
                ((String[]) buf[6])[0] = rslt.getVarchar(7) ;
                ((int[]) buf[7])[0] = rslt.getInt(8) ;
                ((bool[]) buf[8])[0] = rslt.wasNull(8);
                ((short[]) buf[9])[0] = rslt.getShort(9) ;
                ((int[]) buf[10])[0] = rslt.getInt(10) ;
                ((int[]) buf[11])[0] = rslt.getInt(11) ;
                ((int[]) buf[12])[0] = rslt.getInt(12) ;
                ((int[]) buf[13])[0] = rslt.getInt(13) ;
                ((bool[]) buf[14])[0] = rslt.wasNull(13);
                ((int[]) buf[15])[0] = rslt.getInt(14) ;
                ((int[]) buf[16])[0] = rslt.getInt(15) ;
                ((int[]) buf[17])[0] = rslt.getInt(16) ;
                ((int[]) buf[18])[0] = rslt.getInt(17) ;
                return;
             case 1 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getVarchar(2) ;
                ((DateTime[]) buf[2])[0] = rslt.getGXDate(3) ;
                ((DateTime[]) buf[3])[0] = rslt.getGXDate(4) ;
                ((String[]) buf[4])[0] = rslt.getVarchar(5) ;
                ((String[]) buf[5])[0] = rslt.getVarchar(6) ;
                ((String[]) buf[6])[0] = rslt.getVarchar(7) ;
                ((int[]) buf[7])[0] = rslt.getInt(8) ;
                ((bool[]) buf[8])[0] = rslt.wasNull(8);
                ((short[]) buf[9])[0] = rslt.getShort(9) ;
                ((int[]) buf[10])[0] = rslt.getInt(10) ;
                ((int[]) buf[11])[0] = rslt.getInt(11) ;
                ((int[]) buf[12])[0] = rslt.getInt(12) ;
                ((int[]) buf[13])[0] = rslt.getInt(13) ;
                ((bool[]) buf[14])[0] = rslt.wasNull(13);
                ((int[]) buf[15])[0] = rslt.getInt(14) ;
                ((int[]) buf[16])[0] = rslt.getInt(15) ;
                ((int[]) buf[17])[0] = rslt.getInt(16) ;
                ((int[]) buf[18])[0] = rslt.getInt(17) ;
                return;
             case 2 :
                ((String[]) buf[0])[0] = rslt.getVarchar(1) ;
                return;
             case 3 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 4 :
                ((String[]) buf[0])[0] = rslt.getVarchar(1) ;
                return;
             case 5 :
                ((String[]) buf[0])[0] = rslt.getVarchar(1) ;
                return;
             case 6 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 7 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 8 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 9 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 10 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getVarchar(2) ;
                ((String[]) buf[2])[0] = rslt.getVarchar(3) ;
                ((DateTime[]) buf[3])[0] = rslt.getGXDate(4) ;
                ((DateTime[]) buf[4])[0] = rslt.getGXDate(5) ;
                ((String[]) buf[5])[0] = rslt.getVarchar(6) ;
                ((String[]) buf[6])[0] = rslt.getVarchar(7) ;
                ((String[]) buf[7])[0] = rslt.getVarchar(8) ;
                ((String[]) buf[8])[0] = rslt.getVarchar(9) ;
                ((int[]) buf[9])[0] = rslt.getInt(10) ;
                ((bool[]) buf[10])[0] = rslt.wasNull(10);
                ((String[]) buf[11])[0] = rslt.getVarchar(11) ;
                ((short[]) buf[12])[0] = rslt.getShort(12) ;
                ((int[]) buf[13])[0] = rslt.getInt(13) ;
                ((int[]) buf[14])[0] = rslt.getInt(14) ;
                ((int[]) buf[15])[0] = rslt.getInt(15) ;
                ((int[]) buf[16])[0] = rslt.getInt(16) ;
                ((bool[]) buf[17])[0] = rslt.wasNull(16);
                ((int[]) buf[18])[0] = rslt.getInt(17) ;
                ((int[]) buf[19])[0] = rslt.getInt(18) ;
                ((int[]) buf[20])[0] = rslt.getInt(19) ;
                ((int[]) buf[21])[0] = rslt.getInt(20) ;
                return;
             case 11 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 12 :
                ((String[]) buf[0])[0] = rslt.getVarchar(1) ;
                return;
             case 13 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 14 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 15 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 16 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 17 :
                ((String[]) buf[0])[0] = rslt.getVarchar(1) ;
                return;
             case 18 :
                ((String[]) buf[0])[0] = rslt.getVarchar(1) ;
                return;
             case 19 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 20 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 21 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 22 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 25 :
                ((String[]) buf[0])[0] = rslt.getVarchar(1) ;
                return;
             case 26 :
                ((String[]) buf[0])[0] = rslt.getVarchar(1) ;
                return;
             case 27 :
                ((String[]) buf[0])[0] = rslt.getVarchar(1) ;
                return;
             case 28 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 29 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
       }
       getresults30( cursor, rslt, buf) ;
    }

    public void getresults30( int cursor ,
                              IFieldGetter rslt ,
                              Object[] buf )
    {
       switch ( cursor )
       {
             case 30 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 31 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 32 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 33 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 34 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 1 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 2 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 3 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 4 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 5 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 6 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 7 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 8 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 9 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 10 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 11 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 12 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 13 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 14 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 15 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 16 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 17 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 18 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 19 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 20 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 21 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 22 :
                stmt.SetParameter(1, (String)parms[0]);
                stmt.SetParameter(2, (DateTime)parms[1]);
                stmt.SetParameter(3, (DateTime)parms[2]);
                stmt.SetParameter(4, (String)parms[3]);
                stmt.SetParameter(5, (String)parms[4]);
                stmt.SetParameter(6, (String)parms[5]);
                if ( (bool)parms[6] )
                {
                   stmt.setNull( 7 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(7, (int)parms[7]);
                }
                stmt.SetParameter(8, (short)parms[8]);
                stmt.SetParameter(9, (int)parms[9]);
                stmt.SetParameter(10, (int)parms[10]);
                stmt.SetParameter(11, (int)parms[11]);
                if ( (bool)parms[12] )
                {
                   stmt.setNull( 12 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(12, (int)parms[13]);
                }
                stmt.SetParameter(13, (int)parms[14]);
                stmt.SetParameter(14, (int)parms[15]);
                stmt.SetParameter(15, (int)parms[16]);
                stmt.SetParameter(16, (int)parms[17]);
                return;
             case 23 :
                stmt.SetParameter(1, (String)parms[0]);
                stmt.SetParameter(2, (DateTime)parms[1]);
                stmt.SetParameter(3, (DateTime)parms[2]);
                stmt.SetParameter(4, (String)parms[3]);
                stmt.SetParameter(5, (String)parms[4]);
                stmt.SetParameter(6, (String)parms[5]);
                if ( (bool)parms[6] )
                {
                   stmt.setNull( 7 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(7, (int)parms[7]);
                }
                stmt.SetParameter(8, (short)parms[8]);
                stmt.SetParameter(9, (int)parms[9]);
                stmt.SetParameter(10, (int)parms[10]);
                stmt.SetParameter(11, (int)parms[11]);
                if ( (bool)parms[12] )
                {
                   stmt.setNull( 12 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(12, (int)parms[13]);
                }
                stmt.SetParameter(13, (int)parms[14]);
                stmt.SetParameter(14, (int)parms[15]);
                stmt.SetParameter(15, (int)parms[16]);
                stmt.SetParameter(16, (int)parms[17]);
                stmt.SetParameter(17, (int)parms[18]);
                return;
             case 24 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 25 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 26 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 27 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 28 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
       }
       setparameters30( cursor, stmt, parms) ;
    }

    public void setparameters30( int cursor ,
                                 IFieldSetter stmt ,
                                 Object[] parms )
    {
       switch ( cursor )
       {
             case 30 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 31 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 32 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 33 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 34 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
       }
    }

    public String getDataStoreName( )
    {
       return "DATASTORE1";
    }

 }

 public class plan__default : DataStoreHelperBase, IDataStoreHelper
 {
    public ICursor[] getCursors( )
    {
       cursorDefinitions();
       return new Cursor[] {
     };
  }

  private static CursorDef[] def;
  private void cursorDefinitions( )
  {
     if ( def == null )
     {
        def= new CursorDef[] {
        };
     }
  }

  public void getResults( int cursor ,
                          IFieldGetter rslt ,
                          Object[] buf )
  {
     switch ( cursor )
     {
     }
  }

  public void setParameters( int cursor ,
                             IFieldSetter stmt ,
                             Object[] parms )
  {
     switch ( cursor )
     {
     }
  }

}

}
