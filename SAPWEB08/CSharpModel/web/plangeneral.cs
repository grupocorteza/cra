/*
               File: PlanGeneral
        Description: Plan General
             Author: GeneXus C# Generator version 16_0_0-127771
       Generated on: 1/5/2019 14:35:46.58
       Program type: Callable routine
          Main DBMS: SQL Server
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using GeneXus.Http.Server;
using System.Xml.Serialization;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class plangeneral : GXWebComponent, System.Web.SessionState.IRequiresSessionState
   {
      public plangeneral( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsDataStore1 = context.GetDataStore("DataStore1");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         if ( StringUtil.Len( (String)(sPrefix)) == 0 )
         {
            context.SetDefaultTheme("Carmine");
         }
      }

      public plangeneral( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsDataStore1 = context.GetDataStore("DataStore1");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( int aP0_id )
      {
         this.A13Planid = aP0_id;
         executePrivate();
      }

      void executePrivate( )
      {
         isStatic = false;
         webExecute();
      }

      public override void SetPrefix( String sPPrefix )
      {
         sPrefix = sPPrefix;
      }

      protected override void createObjects( )
      {
      }

      protected void INITWEB( )
      {
         initialize_properties( ) ;
         if ( StringUtil.Len( (String)(sPrefix)) == 0 )
         {
            if ( nGotPars == 0 )
            {
               entryPointCalled = false;
               gxfirstwebparm = GetNextPar( );
               gxfirstwebparm_bkp = gxfirstwebparm;
               gxfirstwebparm = DecryptAjaxCall( gxfirstwebparm);
               if ( context.isSpaRequest( ) )
               {
                  disableJsOutput();
               }
               if ( StringUtil.StrCmp(gxfirstwebparm, "dyncall") == 0 )
               {
                  setAjaxCallMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  dyncall( GetNextPar( )) ;
                  return  ;
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "dyncomponent") == 0 )
               {
                  setAjaxEventMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  nDynComponent = 1;
                  sCompPrefix = GetNextPar( );
                  sSFPrefix = GetNextPar( );
                  A13Planid = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A13Planid", StringUtil.LTrim( StringUtil.Str( (decimal)(A13Planid), 9, 0)));
                  setjustcreated();
                  componentprepare(new Object[] {(String)sCompPrefix,(String)sSFPrefix,(int)A13Planid});
                  componentstart();
                  context.httpAjaxContext.ajax_rspStartCmp(sPrefix);
                  componentdraw();
                  context.httpAjaxContext.ajax_rspEndCmp();
                  return  ;
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxEvt") == 0 )
               {
                  setAjaxEventMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  gxfirstwebparm = GetNextPar( );
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "gxfullajaxEvt") == 0 )
               {
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  gxfirstwebparm = GetNextPar( );
               }
               else
               {
                  if ( ! IsValidAjaxCall( false) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  gxfirstwebparm = gxfirstwebparm_bkp;
               }
               if ( context.isSpaRequest( ) )
               {
                  enableJsOutput();
               }
            }
         }
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( ! context.IsLocalStorageSupported( ) )
            {
               context.PushCurrentUrl();
            }
         }
      }

      public override void webExecute( )
      {
         if ( initialized == 0 )
         {
            createObjects();
            initialize();
         }
         INITWEB( ) ;
         if ( ! isAjaxCallMode( ) )
         {
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               ValidateSpaRequest();
            }
            PA1L2( ) ;
            if ( ( GxWebError == 0 ) && ! isAjaxCallMode( ) )
            {
               /* GeneXus formulas. */
               AV13Pgmname = "PlanGeneral";
               context.Gx_err = 0;
               WS1L2( ) ;
               if ( ! isAjaxCallMode( ) )
               {
                  if ( nDynComponent == 0 )
                  {
                     throw new System.Net.WebException("WebComponent is not allowed to run") ;
                  }
               }
            }
            if ( ( GxWebError == 0 ) && context.isAjaxRequest( ) )
            {
               enableOutput();
               if ( ! context.isAjaxRequest( ) )
               {
                  context.GX_webresponse.AppendHeader("Cache-Control", "no-store");
               }
               if ( ! context.WillRedirect( ) )
               {
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               }
               else
               {
                  if ( context.isAjaxRequest( ) )
                  {
                     disableOutput();
                  }
                  RenderHtmlHeaders( ) ;
                  context.Redirect( context.wjLoc );
                  context.DispatchAjaxCommands();
               }
            }
         }
         this.cleanup();
      }

      protected void RenderHtmlHeaders( )
      {
         GxWebStd.gx_html_headers( context, 0, "", "", Form.Meta, Form.Metaequiv, true);
      }

      protected void RenderHtmlOpenForm( )
      {
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( context.isSpaRequest( ) )
            {
               enableOutput();
            }
            context.WriteHtmlText( "<title>") ;
            context.SendWebValue( "Plan General") ;
            context.WriteHtmlTextNl( "</title>") ;
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
            if ( StringUtil.Len( sDynURL) > 0 )
            {
               context.WriteHtmlText( "<BASE href=\""+sDynURL+"\" />") ;
            }
            define_styles( ) ;
         }
         if ( ( ( context.GetBrowserType( ) == 1 ) || ( context.GetBrowserType( ) == 5 ) ) && ( StringUtil.StrCmp(context.GetBrowserVersion( ), "7.0") == 0 ) )
         {
            context.AddJavascriptSource("json2.js", "?"+context.GetBuildNumber( 127771), false);
         }
         context.AddJavascriptSource("jquery.js", "?"+context.GetBuildNumber( 127771), false);
         context.AddJavascriptSource("gxtimezone.js", "?"+context.GetBuildNumber( 127771), false);
         context.AddJavascriptSource("gxgral.js", "?"+context.GetBuildNumber( 127771), false);
         context.AddJavascriptSource("gxcfg.js", "?20191514354664", false);
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.AddJavascriptSource("calendar.js", "?"+context.GetBuildNumber( 127771), false);
         context.AddJavascriptSource("calendar-setup.js", "?"+context.GetBuildNumber( 127771), false);
         context.AddJavascriptSource("calendar-en.js", "?"+context.GetBuildNumber( 127771), false);
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            context.CloseHtmlHeader();
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
            FormProcess = " data-HasEnter=\"false\" data-Skiponenter=\"false\"";
            context.WriteHtmlText( "<body ") ;
            bodyStyle = "";
            if ( nGXWrapped == 0 )
            {
               bodyStyle = bodyStyle + "-moz-opacity:0;opacity:0;";
            }
            context.WriteHtmlText( " "+"class=\"form-horizontal Form\""+" "+ "style='"+bodyStyle+"'") ;
            context.WriteHtmlText( FormProcess+">") ;
            context.skipLines(1);
            context.WriteHtmlTextNl( "<form id=\"MAINFORM\" autocomplete=\"off\" name=\"MAINFORM\" method=\"post\" tabindex=-1  class=\"form-horizontal Form\" data-gx-class=\"form-horizontal Form\" novalidate action=\""+formatLink("plangeneral.aspx") + "?" + UrlEncode("" +A13Planid)+"\">") ;
            GxWebStd.gx_hidden_field( context, "_EventName", "");
            GxWebStd.gx_hidden_field( context, "_EventGridId", "");
            GxWebStd.gx_hidden_field( context, "_EventRowId", "");
            context.WriteHtmlText( "<input type=\"submit\" title=\"submit\" style=\"display:none\" disabled>") ;
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, "FORM", "Class", "form-horizontal Form", true);
         }
         else
         {
            bool toggleHtmlOutput = isOutputEnabled( ) ;
            if ( StringUtil.StringSearch( sPrefix, "MP", 1) == 1 )
            {
               if ( context.isSpaRequest( ) )
               {
                  disableOutput();
               }
            }
            context.WriteHtmlText( "<div") ;
            GxWebStd.ClassAttribute( context, "gxwebcomponent-body"+" "+(String.IsNullOrEmpty(StringUtil.RTrim( Form.Class)) ? "form-horizontal Form" : Form.Class)+"-fx");
            context.WriteHtmlText( ">") ;
            if ( toggleHtmlOutput )
            {
               if ( StringUtil.StringSearch( sPrefix, "MP", 1) == 1 )
               {
                  if ( context.isSpaRequest( ) )
                  {
                     enableOutput();
                  }
               }
            }
            toggleJsOutput = isJsOutputEnabled( );
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
         }
         if ( StringUtil.StringSearch( sPrefix, "MP", 1) == 1 )
         {
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
         }
      }

      protected void send_integrity_footer_hashes( )
      {
         GXKey = Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
         forbiddenHiddens = sPrefix + "hsh" + "PlanGeneral";
         forbiddenHiddens = forbiddenHiddens + context.localUtil.Format( (decimal)(A3Actividadid), "ZZZZZZZZ9");
         GxWebStd.gx_hidden_field( context, sPrefix+"hsh", GetEncryptedHash( forbiddenHiddens, GXKey));
         GXUtil.WriteLog("plangeneral:[SendSecurityCheck value for]"+"Actividadid:"+context.localUtil.Format( (decimal)(A3Actividadid), "ZZZZZZZZ9"));
      }

      protected void SendCloseFormHiddens( )
      {
         /* Send hidden variables. */
         /* Send saved values. */
         send_integrity_footer_hashes( ) ;
         GxWebStd.gx_hidden_field( context, sPrefix+"wcpOA13Planid", StringUtil.LTrim( StringUtil.NToC( (decimal)(wcpOA13Planid), 9, 0, ".", "")));
      }

      protected void RenderHtmlCloseForm1L2( )
      {
         SendCloseFormHiddens( ) ;
         if ( ( StringUtil.Len( sPrefix) != 0 ) && ( context.isAjaxRequest( ) || context.isSpaRequest( ) ) )
         {
            context.AddJavascriptSource("plangeneral.js", "?20191514354669", false);
         }
         GxWebStd.gx_hidden_field( context, sPrefix+"GX_FocusControl", GX_FocusControl);
         define_styles( ) ;
         SendSecurityToken(sPrefix);
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            SendAjaxEncryptionKey();
            SendComponentObjects();
            SendServerCommands();
            SendState();
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
            context.WriteHtmlTextNl( "</form>") ;
            if ( context.isSpaRequest( ) )
            {
               enableOutput();
            }
            include_jscripts( ) ;
            context.WriteHtmlTextNl( "</body>") ;
            context.WriteHtmlTextNl( "</html>") ;
            if ( context.isSpaRequest( ) )
            {
               enableOutput();
            }
         }
         else
         {
            SendWebComponentState();
            context.WriteHtmlText( "</div>") ;
            if ( toggleJsOutput )
            {
               if ( context.isSpaRequest( ) )
               {
                  enableJsOutput();
               }
            }
         }
      }

      public override String GetPgmname( )
      {
         return "PlanGeneral" ;
      }

      public override String GetPgmdesc( )
      {
         return "Plan General" ;
      }

      protected void WB1L0( )
      {
         if ( context.isAjaxRequest( ) )
         {
            disableOutput();
         }
         if ( ! wbLoad )
         {
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               RenderHtmlHeaders( ) ;
            }
            RenderHtmlOpenForm( ) ;
            if ( StringUtil.Len( sPrefix) != 0 )
            {
               GxWebStd.gx_hidden_field( context, sPrefix+"_CMPPGM", "plangeneral.aspx");
            }
            GxWebStd.gx_msg_list( context, "", context.GX_msglist.DisplayMode, "", "", sPrefix, "false");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "Section", "left", "top", " "+"data-gx-base-lib=\"bootstrapv3\""+" "+"data-abstract-form"+" ", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, divMaintable_Internalname, 1, 0, "px", 0, "px", "Table", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12 ViewActionsCell", "Center", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "gx-action-group WWViewActions", "left", "top", " "+"data-gx-actiongroup-type=\"toolbar\""+" ", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "gx-button", "left", "top", "", "", "div");
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 8,'" + sPrefix + "',false,'',0)\"";
            ClassString = "BtnEnter";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtnupdate_Internalname, "", "Update", bttBtnupdate_Jsonclick, 7, "Update", "", StyleString, ClassString, 1, 1, "standard", "'"+sPrefix+"'"+",false,"+"'"+"e111l1_client"+"'", TempTags, "", 2, "HLP_PlanGeneral.htm");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "gx-button", "left", "top", "", "", "div");
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 10,'" + sPrefix + "',false,'',0)\"";
            ClassString = "BtnDelete";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtndelete_Internalname, "", "Delete", bttBtndelete_Jsonclick, 7, "Delete", "", StyleString, ClassString, 1, 1, "standard", "'"+sPrefix+"'"+",false,"+"'"+"e121l1_client"+"'", TempTags, "", 2, "HLP_PlanGeneral.htm");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "Center", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, divAttributestable_Internalname, 1, 0, "px", 0, "px", "Table", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12 FormCell", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "form-group gx-form-group", "left", "top", ""+" data-gx-for=\""+edtnombreEvento_Internalname+"\"", "", "div");
            /* Attribute/Variable Label */
            GxWebStd.gx_label_element( context, edtnombreEvento_Internalname, "Nombre Evento", "col-sm-3 ReadonlyAttributeLabel", 1, true);
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-sm-9 gx-attribute", "left", "top", "", "", "div");
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtnombreEvento_Internalname, A30nombreEvento, StringUtil.RTrim( context.localUtil.Format( A30nombreEvento, "")), "", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtnombreEvento_Jsonclick, 0, "ReadonlyAttribute", "", "", "", "", 1, edtnombreEvento_Enabled, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_PlanGeneral.htm");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12 FormCell", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "form-group gx-form-group", "left", "top", ""+" data-gx-for=\""+edtComunidadnombre_Internalname+"\"", "", "div");
            /* Attribute/Variable Label */
            GxWebStd.gx_label_element( context, edtComunidadnombre_Internalname, "nombre", "col-sm-3 ReadonlyAttributeLabel", 1, true);
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-sm-9 gx-attribute", "left", "top", "", "", "div");
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtComunidadnombre_Internalname, A130Comunidadnombre, StringUtil.RTrim( context.localUtil.Format( A130Comunidadnombre, "")), "", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtComunidadnombre_Jsonclick, 0, "ReadonlyAttribute", "", "", "", "", 1, edtComunidadnombre_Enabled, 0, "text", "", 60, "chr", 1, "row", 60, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_PlanGeneral.htm");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12 FormCell", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "form-group gx-form-group", "left", "top", ""+" data-gx-for=\""+edtinicio_Internalname+"\"", "", "div");
            /* Attribute/Variable Label */
            GxWebStd.gx_label_element( context, edtinicio_Internalname, "Fecha de Inicio", "col-sm-3 ReadonlyAttributeLabel", 1, true);
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-sm-9 gx-attribute", "left", "top", "", "", "div");
            /* Single line edit */
            context.WriteHtmlText( "<div id=\""+edtinicio_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtinicio_Internalname, context.localUtil.Format(A31inicio, "99/99/99"), context.localUtil.Format( A31inicio, "99/99/99"), "", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtinicio_Jsonclick, 0, "ReadonlyAttribute", "", "", "", "", 1, edtinicio_Enabled, 0, "text", "", 8, "chr", 1, "row", 8, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_PlanGeneral.htm");
            GxWebStd.gx_bitmap( context, edtinicio_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((1==0)||(edtinicio_Enabled==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", "", 1, false, false, "", "HLP_PlanGeneral.htm");
            context.WriteHtmlTextNl( "</div>") ;
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12 FormCell", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "form-group gx-form-group", "left", "top", ""+" data-gx-for=\""+edtfin_Internalname+"\"", "", "div");
            /* Attribute/Variable Label */
            GxWebStd.gx_label_element( context, edtfin_Internalname, "Fecha de Finalización", "col-sm-3 ReadonlyAttributeLabel", 1, true);
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-sm-9 gx-attribute", "left", "top", "", "", "div");
            /* Single line edit */
            context.WriteHtmlText( "<div id=\""+edtfin_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtfin_Internalname, context.localUtil.Format(A32fin, "99/99/99"), context.localUtil.Format( A32fin, "99/99/99"), "", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtfin_Jsonclick, 0, "ReadonlyAttribute", "", "", "", "", 1, edtfin_Enabled, 0, "text", "", 8, "chr", 1, "row", 8, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_PlanGeneral.htm");
            GxWebStd.gx_bitmap( context, edtfin_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((1==0)||(edtfin_Enabled==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", "", 1, false, false, "", "HLP_PlanGeneral.htm");
            context.WriteHtmlTextNl( "</div>") ;
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12 FormCell", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "form-group gx-form-group", "left", "top", ""+" data-gx-for=\""+edtTipoBeneficiariodescripcion_Internalname+"\"", "", "div");
            /* Attribute/Variable Label */
            GxWebStd.gx_label_element( context, edtTipoBeneficiariodescripcion_Internalname, "descripcion", "col-sm-3 ReadonlyAttributeLabel", 1, true);
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-sm-9 gx-attribute", "left", "top", "", "", "div");
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtTipoBeneficiariodescripcion_Internalname, A28TipoBeneficiariodescripcion, StringUtil.RTrim( context.localUtil.Format( A28TipoBeneficiariodescripcion, "")), "", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtTipoBeneficiariodescripcion_Jsonclick, 0, "ReadonlyAttribute", "", "", "", "", 1, edtTipoBeneficiariodescripcion_Enabled, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_PlanGeneral.htm");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12 FormCell", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "form-group gx-form-group", "left", "top", ""+" data-gx-for=\""+edtPlanidResponsable_Internalname+"\"", "", "div");
            /* Attribute/Variable Label */
            GxWebStd.gx_label_element( context, edtPlanidResponsable_Internalname, "Responsable", "col-sm-3 ReadonlyAttributeLabel", 1, true);
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-sm-9 gx-attribute", "left", "top", "", "", "div");
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtPlanidResponsable_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A33PlanidResponsable), 9, 0, ".", "")), ((edtPlanidResponsable_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( (decimal)(A33PlanidResponsable), "ZZZZZZZZ9")) : context.localUtil.Format( (decimal)(A33PlanidResponsable), "ZZZZZZZZ9")), "", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtPlanidResponsable_Jsonclick, 0, "ReadonlyAttribute", "", "", "", "", 1, edtPlanidResponsable_Enabled, 0, "number", "1", 9, "chr", 1, "row", 9, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_PlanGeneral.htm");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12 FormCell", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "form-group gx-form-group", "left", "top", ""+" data-gx-for=\""+edtPlanidFinanciero_Internalname+"\"", "", "div");
            /* Attribute/Variable Label */
            GxWebStd.gx_label_element( context, edtPlanidFinanciero_Internalname, "Financiero", "col-sm-3 ReadonlyAttributeLabel", 1, true);
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-sm-9 gx-attribute", "left", "top", "", "", "div");
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtPlanidFinanciero_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A34PlanidFinanciero), 9, 0, ".", "")), ((edtPlanidFinanciero_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( (decimal)(A34PlanidFinanciero), "ZZZZZZZZ9")) : context.localUtil.Format( (decimal)(A34PlanidFinanciero), "ZZZZZZZZ9")), "", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtPlanidFinanciero_Jsonclick, 0, "ReadonlyAttribute", "", "", "", "", 1, edtPlanidFinanciero_Enabled, 0, "number", "1", 9, "chr", 1, "row", 9, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_PlanGeneral.htm");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12 FormCell", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "form-group gx-form-group", "left", "top", ""+" data-gx-for=\""+edtPlanidTecnico_Internalname+"\"", "", "div");
            /* Attribute/Variable Label */
            GxWebStd.gx_label_element( context, edtPlanidTecnico_Internalname, "Tecnico", "col-sm-3 ReadonlyAttributeLabel", 1, true);
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-sm-9 gx-attribute", "left", "top", "", "", "div");
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtPlanidTecnico_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A35PlanidTecnico), 9, 0, ".", "")), ((edtPlanidTecnico_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( (decimal)(A35PlanidTecnico), "ZZZZZZZZ9")) : context.localUtil.Format( (decimal)(A35PlanidTecnico), "ZZZZZZZZ9")), "", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtPlanidTecnico_Jsonclick, 0, "ReadonlyAttribute", "", "", "", "", 1, edtPlanidTecnico_Enabled, 0, "number", "1", 9, "chr", 1, "row", 9, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_PlanGeneral.htm");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12 FormCell", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "form-group gx-form-group", "left", "top", ""+" data-gx-for=\""+edtdescripcionActividad_Internalname+"\"", "", "div");
            /* Attribute/Variable Label */
            GxWebStd.gx_label_element( context, edtdescripcionActividad_Internalname, "Descripcion Actividad", "col-sm-3 ReadonlyAttributeLabel", 1, true);
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-sm-9 gx-attribute", "left", "top", "", "", "div");
            /* Multiple line edit */
            ClassString = "ReadonlyAttribute";
            StyleString = "";
            ClassString = "ReadonlyAttribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtdescripcionActividad_Internalname, A38descripcionActividad, "", "", 0, 1, edtdescripcionActividad_Enabled, 0, 80, "chr", 7, "row", StyleString, ClassString, "", "", "500", -1, 0, "", "", -1, true, "", "'"+sPrefix+"'"+",false,"+"'"+""+"'", 0, "HLP_PlanGeneral.htm");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12 FormCell", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "form-group gx-form-group", "left", "top", ""+" data-gx-for=\""+edtEstadonombre_Internalname+"\"", "", "div");
            /* Attribute/Variable Label */
            GxWebStd.gx_label_element( context, edtEstadonombre_Internalname, "nombre", "col-sm-3 ReadonlyAttributeLabel", 1, true);
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-sm-9 gx-attribute", "left", "top", "", "", "div");
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtEstadonombre_Internalname, A61Estadonombre, StringUtil.RTrim( context.localUtil.Format( A61Estadonombre, "")), "", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtEstadonombre_Jsonclick, 0, "ReadonlyAttribute", "", "", "", "", 1, edtEstadonombre_Enabled, 0, "text", "", 20, "chr", 1, "row", 20, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_PlanGeneral.htm");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12 FormCell", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "form-group gx-form-group", "left", "top", ""+" data-gx-for=\""+edtcantBeneficiarios_Internalname+"\"", "", "div");
            /* Attribute/Variable Label */
            GxWebStd.gx_label_element( context, edtcantBeneficiarios_Internalname, "cant Beneficiarios", "col-sm-3 ReadonlyAttributeLabel", 1, true);
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-sm-9 gx-attribute", "left", "top", "", "", "div");
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtcantBeneficiarios_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A39cantBeneficiarios), 9, 0, ".", "")), ((edtcantBeneficiarios_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( (decimal)(A39cantBeneficiarios), "ZZZZZZZZ9")) : context.localUtil.Format( (decimal)(A39cantBeneficiarios), "ZZZZZZZZ9")), "", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtcantBeneficiarios_Jsonclick, 0, "ReadonlyAttribute", "", "", "", "", 1, edtcantBeneficiarios_Enabled, 0, "number", "1", 9, "chr", 1, "row", 9, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_PlanGeneral.htm");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12 FormCell", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "form-group gx-form-group", "left", "top", ""+" data-gx-for=\""+edtActividaddescripcion_Internalname+"\"", "", "div");
            /* Attribute/Variable Label */
            GxWebStd.gx_label_element( context, edtActividaddescripcion_Internalname, "Actividaddescripcion", "col-sm-3 ReadonlyAttributeLabel", 1, true);
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-sm-9 gx-attribute", "left", "top", "", "", "div");
            /* Multiple line edit */
            ClassString = "ReadonlyAttribute";
            StyleString = "";
            ClassString = "ReadonlyAttribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtActividaddescripcion_Internalname, A48Actividaddescripcion, edtActividaddescripcion_Link, "", 0, 1, edtActividaddescripcion_Enabled, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "", "300", -1, 0, "", "", -1, true, "", "'"+sPrefix+"'"+",false,"+"'"+""+"'", 0, "HLP_PlanGeneral.htm");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", " gx-attribute", "left", "top", "", "", "div");
            /* Attribute/Variable Label */
            GxWebStd.gx_label_element( context, edtPlanid_Internalname, "Plan", "col-sm-3 AttributeLabel", 0, true);
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtPlanid_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A13Planid), 9, 0, ".", "")), ((edtPlanid_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( (decimal)(A13Planid), "ZZZZZZZZ9")) : context.localUtil.Format( (decimal)(A13Planid), "ZZZZZZZZ9")), "", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtPlanid_Jsonclick, 0, "Attribute", "", "", "", "", edtPlanid_Visible, edtPlanid_Enabled, 0, "number", "1", 9, "chr", 1, "row", 9, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_PlanGeneral.htm");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", " gx-attribute", "left", "top", "", "", "div");
            /* Attribute/Variable Label */
            GxWebStd.gx_label_element( context, edtPlanidProyecto_Internalname, "Planid Proyecto", "col-sm-3 AttributeLabel", 0, true);
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtPlanidProyecto_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A29PlanidProyecto), 9, 0, ".", "")), ((edtPlanidProyecto_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( (decimal)(A29PlanidProyecto), "ZZZZZZZZ9")) : context.localUtil.Format( (decimal)(A29PlanidProyecto), "ZZZZZZZZ9")), "", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtPlanidProyecto_Jsonclick, 0, "Attribute", "", "", "", "", edtPlanidProyecto_Visible, edtPlanidProyecto_Enabled, 0, "number", "1", 9, "chr", 1, "row", 9, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_PlanGeneral.htm");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", " gx-attribute", "left", "top", "", "", "div");
            /* Attribute/Variable Label */
            GxWebStd.gx_label_element( context, edtComunidadid_Internalname, "Comunidad", "col-sm-3 AttributeLabel", 0, true);
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtComunidadid_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A19Comunidadid), 9, 0, ".", "")), ((edtComunidadid_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( (decimal)(A19Comunidadid), "ZZZZZZZZ9")) : context.localUtil.Format( (decimal)(A19Comunidadid), "ZZZZZZZZ9")), "", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtComunidadid_Jsonclick, 0, "Attribute", "", "", "", "", edtComunidadid_Visible, edtComunidadid_Enabled, 0, "number", "1", 9, "chr", 1, "row", 9, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_PlanGeneral.htm");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", " gx-attribute", "left", "top", "", "", "div");
            /* Attribute/Variable Label */
            GxWebStd.gx_label_element( context, edtTipoBeneficiarioid_Internalname, "Tipo Beneficiario", "col-sm-3 AttributeLabel", 0, true);
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtTipoBeneficiarioid_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A22TipoBeneficiarioid), 9, 0, ".", "")), ((edtTipoBeneficiarioid_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( (decimal)(A22TipoBeneficiarioid), "ZZZZZZZZ9")) : context.localUtil.Format( (decimal)(A22TipoBeneficiarioid), "ZZZZZZZZ9")), "", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtTipoBeneficiarioid_Jsonclick, 0, "Attribute", "", "", "", "", edtTipoBeneficiarioid_Visible, edtTipoBeneficiarioid_Enabled, 0, "number", "1", 9, "chr", 1, "row", 9, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_PlanGeneral.htm");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", " gx-attribute", "left", "top", "", "", "div");
            /* Attribute/Variable Label */
            GxWebStd.gx_label_element( context, edtobjGeneral_Internalname, "Obj General", "col-sm-3 AttributeLabel", 0, true);
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtobjGeneral_Internalname, A36objGeneral, StringUtil.RTrim( context.localUtil.Format( A36objGeneral, "")), "", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtobjGeneral_Jsonclick, 0, "Attribute", "", "", "", "", edtobjGeneral_Visible, edtobjGeneral_Enabled, 0, "text", "", 80, "chr", 1, "row", 100, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_PlanGeneral.htm");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", " gx-attribute", "left", "top", "", "", "div");
            /* Attribute/Variable Label */
            GxWebStd.gx_label_element( context, edtobjEspecificos_Internalname, "Obj Especifico", "col-sm-3 AttributeLabel", 0, true);
            /* Multiple line edit */
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtobjEspecificos_Internalname, A37objEspecificos, "", "", 0, edtobjEspecificos_Visible, edtobjEspecificos_Enabled, 0, 80, "chr", 3, "row", StyleString, ClassString, "", "", "200", -1, 0, "", "", -1, true, "", "'"+sPrefix+"'"+",false,"+"'"+""+"'", 0, "HLP_PlanGeneral.htm");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", " gx-attribute", "left", "top", "", "", "div");
            /* Attribute/Variable Label */
            GxWebStd.gx_label_element( context, edtEstadoid_Internalname, "Seleccione el Estado", "col-sm-3 AttributeLabel", 0, true);
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtEstadoid_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A12Estadoid), 9, 0, ".", "")), ((edtEstadoid_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( (decimal)(A12Estadoid), "ZZZZZZZZ9")) : context.localUtil.Format( (decimal)(A12Estadoid), "ZZZZZZZZ9")), "", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtEstadoid_Jsonclick, 0, "Attribute", "", "", "", "", edtEstadoid_Visible, edtEstadoid_Enabled, 0, "number", "1", 9, "chr", 1, "row", 9, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_PlanGeneral.htm");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", " gx-attribute", "left", "top", "", "", "div");
            /* Attribute/Variable Label */
            GxWebStd.gx_label_element( context, edtActividadid_Internalname, "Actividad", "col-sm-3 AttributeLabel", 0, true);
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtActividadid_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A3Actividadid), 9, 0, ".", "")), ((edtActividadid_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( (decimal)(A3Actividadid), "ZZZZZZZZ9")) : context.localUtil.Format( (decimal)(A3Actividadid), "ZZZZZZZZ9")), "", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtActividadid_Jsonclick, 0, "Attribute", "", "", "", "", edtActividadid_Visible, edtActividadid_Enabled, 0, "number", "1", 9, "chr", 1, "row", 9, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_PlanGeneral.htm");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", " gx-attribute", "left", "top", "", "", "div");
            /* Attribute/Variable Label */
            GxWebStd.gx_label_element( context, edtinforme_Internalname, "informe", "col-sm-3 AttributeLabel", 0, true);
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtinforme_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A40informe), 1, 0, ".", "")), ((edtinforme_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( (decimal)(A40informe), "9")) : context.localUtil.Format( (decimal)(A40informe), "9")), "", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtinforme_Jsonclick, 0, "Attribute", "", "", "", "", edtinforme_Visible, edtinforme_Enabled, 0, "number", "1", 1, "chr", 1, "row", 1, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_PlanGeneral.htm");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
         }
         wbLoad = true;
      }

      protected void START1L2( )
      {
         wbLoad = false;
         wbEnd = 0;
         wbStart = 0;
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( ! context.isSpaRequest( ) )
            {
               Form.Meta.addItem("generator", "GeneXus C# 16_0_0-127771", 0) ;
               Form.Meta.addItem("description", "Plan General", 0) ;
            }
            context.wjLoc = "";
            context.nUserReturn = 0;
            context.wbHandled = 0;
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               sXEvt = cgiGet( "_EventName");
               if ( ! GetJustCreated( ) && ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 ) )
               {
               }
            }
         }
         wbErr = false;
         if ( ( StringUtil.Len( sPrefix) == 0 ) || ( nDraw == 1 ) )
         {
            if ( nDoneStart == 0 )
            {
               STRUP1L0( ) ;
            }
         }
      }

      protected void WS1L2( )
      {
         START1L2( ) ;
         EVT1L2( ) ;
      }

      protected void EVT1L2( )
      {
         sXEvt = cgiGet( "_EventName");
         if ( ( ( ( StringUtil.Len( sPrefix) == 0 ) ) || ( StringUtil.StringSearch( sXEvt, sPrefix, 1) > 0 ) ) && ! GetJustCreated( ) && ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 ) )
         {
            if ( ! context.WillRedirect( ) && ( context.nUserReturn != 1 ) && ! wbErr )
            {
               /* Read Web Panel buttons. */
               if ( context.wbHandled == 0 )
               {
                  if ( StringUtil.Len( sPrefix) == 0 )
                  {
                     sEvt = cgiGet( "_EventName");
                     EvtGridId = cgiGet( "_EventGridId");
                     EvtRowId = cgiGet( "_EventRowId");
                  }
                  if ( StringUtil.Len( sEvt) > 0 )
                  {
                     sEvtType = StringUtil.Left( sEvt, 1);
                     sEvt = StringUtil.Right( sEvt, (short)(StringUtil.Len( sEvt)-1));
                     if ( StringUtil.StrCmp(sEvtType, "E") == 0 )
                     {
                        sEvtType = StringUtil.Right( sEvt, 1);
                        if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                        {
                           sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                           if ( StringUtil.StrCmp(sEvt, "RFR") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUP1L0( ) ;
                              }
                              if ( ! context.WillRedirect( ) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "START") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUP1L0( ) ;
                              }
                              if ( ! context.WillRedirect( ) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: Start */
                                    E131L2 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "LOAD") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUP1L0( ) ;
                              }
                              if ( ! context.WillRedirect( ) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: Load */
                                    E141L2 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "ENTER") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUP1L0( ) ;
                              }
                              if ( ! context.WillRedirect( ) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    if ( ! wbErr )
                                    {
                                       Rfr0gs = false;
                                       if ( ! Rfr0gs )
                                       {
                                       }
                                       dynload_actions( ) ;
                                    }
                                 }
                              }
                              /* No code required for Cancel button. It is implemented as the Reset button. */
                           }
                           else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUP1L0( ) ;
                              }
                              if ( ! context.WillRedirect( ) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                 }
                              }
                              dynload_actions( ) ;
                           }
                        }
                        else
                        {
                        }
                     }
                     context.wbHandled = 1;
                  }
               }
            }
         }
      }

      protected void WE1L2( )
      {
         if ( ! GxWebStd.gx_redirect( context) )
         {
            Rfr0gs = true;
            Refresh( ) ;
            if ( ! GxWebStd.gx_redirect( context) )
            {
               RenderHtmlCloseForm1L2( ) ;
            }
         }
      }

      protected void PA1L2( )
      {
         if ( nDonePA == 0 )
         {
            if ( StringUtil.Len( sPrefix) != 0 )
            {
               initialize_properties( ) ;
            }
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.GetCookie( "GX_SESSION_ID"))) )
               {
                  gxcookieaux = context.SetCookie( "GX_SESSION_ID", Encrypt64( Crypto.GetEncryptionKey( ), Crypto.GetServerKey( )), "", (DateTime)(DateTime.MinValue), "", 0);
               }
            }
            GXKey = Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            toggleJsOutput = isJsOutputEnabled( );
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               if ( context.isSpaRequest( ) )
               {
                  disableJsOutput();
               }
            }
            init_web_controls( ) ;
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               if ( toggleJsOutput )
               {
                  if ( context.isSpaRequest( ) )
                  {
                     enableJsOutput();
                  }
               }
            }
            if ( ! context.isAjaxRequest( ) )
            {
            }
            nDonePA = 1;
         }
      }

      protected void dynload_actions( )
      {
         /* End function dynload_actions */
      }

      protected void send_integrity_hashes( )
      {
      }

      protected void clear_multi_value_controls( )
      {
         if ( context.isAjaxRequest( ) )
         {
            dynload_actions( ) ;
         }
      }

      protected void fix_multi_value_controls( )
      {
      }

      public void Refresh( )
      {
         send_integrity_hashes( ) ;
         RF1L2( ) ;
         if ( isFullAjaxMode( ) )
         {
            send_integrity_footer_hashes( ) ;
         }
      }

      protected void initialize_formulas( )
      {
         /* GeneXus formulas. */
         AV13Pgmname = "PlanGeneral";
         context.Gx_err = 0;
      }

      protected void RF1L2( )
      {
         initialize_formulas( ) ;
         clear_multi_value_controls( ) ;
         gxdyncontrolsrefreshing = true;
         fix_multi_value_controls( ) ;
         gxdyncontrolsrefreshing = false;
         if ( ! context.WillRedirect( ) && ( context.nUserReturn != 1 ) )
         {
            /* Using cursor H001L2 */
            pr_datastore1.execute(0, new Object[] {A13Planid});
            while ( (pr_datastore1.getStatus(0) != 101) )
            {
               A40informe = H001L2_A40informe[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A40informe", StringUtil.Str( (decimal)(A40informe), 1, 0));
               A3Actividadid = H001L2_A3Actividadid[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A3Actividadid", StringUtil.LTrim( StringUtil.Str( (decimal)(A3Actividadid), 9, 0)));
               n3Actividadid = H001L2_n3Actividadid[0];
               A12Estadoid = H001L2_A12Estadoid[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A12Estadoid", StringUtil.LTrim( StringUtil.Str( (decimal)(A12Estadoid), 9, 0)));
               A37objEspecificos = H001L2_A37objEspecificos[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A37objEspecificos", A37objEspecificos);
               A36objGeneral = H001L2_A36objGeneral[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A36objGeneral", A36objGeneral);
               A22TipoBeneficiarioid = H001L2_A22TipoBeneficiarioid[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A22TipoBeneficiarioid", StringUtil.LTrim( StringUtil.Str( (decimal)(A22TipoBeneficiarioid), 9, 0)));
               A19Comunidadid = H001L2_A19Comunidadid[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A19Comunidadid", StringUtil.LTrim( StringUtil.Str( (decimal)(A19Comunidadid), 9, 0)));
               A29PlanidProyecto = H001L2_A29PlanidProyecto[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A29PlanidProyecto", StringUtil.LTrim( StringUtil.Str( (decimal)(A29PlanidProyecto), 9, 0)));
               A48Actividaddescripcion = H001L2_A48Actividaddescripcion[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A48Actividaddescripcion", A48Actividaddescripcion);
               A39cantBeneficiarios = H001L2_A39cantBeneficiarios[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A39cantBeneficiarios", StringUtil.LTrim( StringUtil.Str( (decimal)(A39cantBeneficiarios), 9, 0)));
               n39cantBeneficiarios = H001L2_n39cantBeneficiarios[0];
               A61Estadonombre = H001L2_A61Estadonombre[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A61Estadonombre", A61Estadonombre);
               A38descripcionActividad = H001L2_A38descripcionActividad[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A38descripcionActividad", A38descripcionActividad);
               A35PlanidTecnico = H001L2_A35PlanidTecnico[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A35PlanidTecnico", StringUtil.LTrim( StringUtil.Str( (decimal)(A35PlanidTecnico), 9, 0)));
               A34PlanidFinanciero = H001L2_A34PlanidFinanciero[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A34PlanidFinanciero", StringUtil.LTrim( StringUtil.Str( (decimal)(A34PlanidFinanciero), 9, 0)));
               A33PlanidResponsable = H001L2_A33PlanidResponsable[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A33PlanidResponsable", StringUtil.LTrim( StringUtil.Str( (decimal)(A33PlanidResponsable), 9, 0)));
               A28TipoBeneficiariodescripcion = H001L2_A28TipoBeneficiariodescripcion[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A28TipoBeneficiariodescripcion", A28TipoBeneficiariodescripcion);
               A32fin = H001L2_A32fin[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A32fin", context.localUtil.Format(A32fin, "99/99/99"));
               A31inicio = H001L2_A31inicio[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A31inicio", context.localUtil.Format(A31inicio, "99/99/99"));
               A130Comunidadnombre = H001L2_A130Comunidadnombre[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A130Comunidadnombre", A130Comunidadnombre);
               A30nombreEvento = H001L2_A30nombreEvento[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A30nombreEvento", A30nombreEvento);
               A48Actividaddescripcion = H001L2_A48Actividaddescripcion[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A48Actividaddescripcion", A48Actividaddescripcion);
               A61Estadonombre = H001L2_A61Estadonombre[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A61Estadonombre", A61Estadonombre);
               A28TipoBeneficiariodescripcion = H001L2_A28TipoBeneficiariodescripcion[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A28TipoBeneficiariodescripcion", A28TipoBeneficiariodescripcion);
               A130Comunidadnombre = H001L2_A130Comunidadnombre[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A130Comunidadnombre", A130Comunidadnombre);
               /* Execute user event: Load */
               E141L2 ();
               /* Exiting from a For First loop. */
               if (true) break;
            }
            pr_datastore1.close(0);
            WB1L0( ) ;
         }
      }

      protected void send_integrity_lvl_hashes1L2( )
      {
      }

      protected void STRUP1L0( )
      {
         /* Before Start, stand alone formulas. */
         AV13Pgmname = "PlanGeneral";
         context.Gx_err = 0;
         /* Execute Start event if defined. */
         context.wbGlbDoneStart = 0;
         /* Execute user event: Start */
         E131L2 ();
         context.wbGlbDoneStart = 1;
         nDoneStart = 1;
         /* After Start, stand alone formulas. */
         sXEvt = cgiGet( "_EventName");
         if ( ! GetJustCreated( ) && ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 ) )
         {
            /* Read saved SDTs. */
            /* Read variables values. */
            A30nombreEvento = cgiGet( edtnombreEvento_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A30nombreEvento", A30nombreEvento);
            A130Comunidadnombre = cgiGet( edtComunidadnombre_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A130Comunidadnombre", A130Comunidadnombre);
            A31inicio = context.localUtil.CToD( cgiGet( edtinicio_Internalname), 1);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A31inicio", context.localUtil.Format(A31inicio, "99/99/99"));
            A32fin = context.localUtil.CToD( cgiGet( edtfin_Internalname), 1);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A32fin", context.localUtil.Format(A32fin, "99/99/99"));
            A28TipoBeneficiariodescripcion = cgiGet( edtTipoBeneficiariodescripcion_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A28TipoBeneficiariodescripcion", A28TipoBeneficiariodescripcion);
            A33PlanidResponsable = (int)(context.localUtil.CToN( cgiGet( edtPlanidResponsable_Internalname), ".", ","));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A33PlanidResponsable", StringUtil.LTrim( StringUtil.Str( (decimal)(A33PlanidResponsable), 9, 0)));
            A34PlanidFinanciero = (int)(context.localUtil.CToN( cgiGet( edtPlanidFinanciero_Internalname), ".", ","));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A34PlanidFinanciero", StringUtil.LTrim( StringUtil.Str( (decimal)(A34PlanidFinanciero), 9, 0)));
            A35PlanidTecnico = (int)(context.localUtil.CToN( cgiGet( edtPlanidTecnico_Internalname), ".", ","));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A35PlanidTecnico", StringUtil.LTrim( StringUtil.Str( (decimal)(A35PlanidTecnico), 9, 0)));
            A38descripcionActividad = cgiGet( edtdescripcionActividad_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A38descripcionActividad", A38descripcionActividad);
            A61Estadonombre = cgiGet( edtEstadonombre_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A61Estadonombre", A61Estadonombre);
            A39cantBeneficiarios = (int)(context.localUtil.CToN( cgiGet( edtcantBeneficiarios_Internalname), ".", ","));
            n39cantBeneficiarios = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A39cantBeneficiarios", StringUtil.LTrim( StringUtil.Str( (decimal)(A39cantBeneficiarios), 9, 0)));
            A48Actividaddescripcion = cgiGet( edtActividaddescripcion_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A48Actividaddescripcion", A48Actividaddescripcion);
            A29PlanidProyecto = (int)(context.localUtil.CToN( cgiGet( edtPlanidProyecto_Internalname), ".", ","));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A29PlanidProyecto", StringUtil.LTrim( StringUtil.Str( (decimal)(A29PlanidProyecto), 9, 0)));
            A19Comunidadid = (int)(context.localUtil.CToN( cgiGet( edtComunidadid_Internalname), ".", ","));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A19Comunidadid", StringUtil.LTrim( StringUtil.Str( (decimal)(A19Comunidadid), 9, 0)));
            A22TipoBeneficiarioid = (int)(context.localUtil.CToN( cgiGet( edtTipoBeneficiarioid_Internalname), ".", ","));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A22TipoBeneficiarioid", StringUtil.LTrim( StringUtil.Str( (decimal)(A22TipoBeneficiarioid), 9, 0)));
            A36objGeneral = cgiGet( edtobjGeneral_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A36objGeneral", A36objGeneral);
            A37objEspecificos = cgiGet( edtobjEspecificos_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A37objEspecificos", A37objEspecificos);
            A12Estadoid = (int)(context.localUtil.CToN( cgiGet( edtEstadoid_Internalname), ".", ","));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A12Estadoid", StringUtil.LTrim( StringUtil.Str( (decimal)(A12Estadoid), 9, 0)));
            A3Actividadid = (int)(context.localUtil.CToN( cgiGet( edtActividadid_Internalname), ".", ","));
            n3Actividadid = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A3Actividadid", StringUtil.LTrim( StringUtil.Str( (decimal)(A3Actividadid), 9, 0)));
            A40informe = (short)(context.localUtil.CToN( cgiGet( edtinforme_Internalname), ".", ","));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A40informe", StringUtil.Str( (decimal)(A40informe), 1, 0));
            /* Read saved values. */
            wcpOA13Planid = (int)(context.localUtil.CToN( cgiGet( sPrefix+"wcpOA13Planid"), ".", ","));
            /* Read subfile selected row values. */
            /* Read hidden variables. */
            GXKey = Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            forbiddenHiddens = sPrefix + "hsh" + "PlanGeneral";
            A3Actividadid = (int)(context.localUtil.CToN( cgiGet( edtActividadid_Internalname), ".", ","));
            n3Actividadid = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A3Actividadid", StringUtil.LTrim( StringUtil.Str( (decimal)(A3Actividadid), 9, 0)));
            forbiddenHiddens = forbiddenHiddens + context.localUtil.Format( (decimal)(A3Actividadid), "ZZZZZZZZ9");
            hsh = cgiGet( sPrefix+"hsh");
            if ( ! GXUtil.CheckEncryptedHash( forbiddenHiddens, hsh, GXKey) )
            {
               GXUtil.WriteLog("plangeneral:[SecurityCheckFailed value for]"+"Actividadid:"+context.localUtil.Format( (decimal)(A3Actividadid), "ZZZZZZZZ9"));
               GxWebError = 1;
               context.HttpContext.Response.StatusDescription = 403.ToString();
               context.HttpContext.Response.StatusCode = 403;
               context.WriteHtmlText( "<title>403 Forbidden</title>") ;
               context.WriteHtmlText( "<h1>403 Forbidden</h1>") ;
               context.WriteHtmlText( "<p /><hr />") ;
               GXUtil.WriteLog("send_http_error_code " + 403.ToString());
               return  ;
            }
         }
         else
         {
            dynload_actions( ) ;
         }
      }

      protected void GXStart( )
      {
         /* Execute user event: Start */
         E131L2 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E131L2( )
      {
         /* Start Routine */
         if ( ! new isauthorized(context).executeUdp(  AV13Pgmname) )
         {
            CallWebObject(formatLink("notauthorized.aspx") + "?" + UrlEncode(StringUtil.RTrim(AV13Pgmname)));
            context.wjLocDisableFrm = 1;
         }
         /* Execute user subroutine: 'PREPARETRANSACTION' */
         S112 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void nextLoad( )
      {
      }

      protected void E141L2( )
      {
         /* Load Routine */
         edtActividaddescripcion_Link = formatLink("viewactividad.aspx") + "?" + UrlEncode("" +A3Actividadid) + "," + UrlEncode(StringUtil.RTrim(""));
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtActividaddescripcion_Internalname, "Link", edtActividaddescripcion_Link, true);
         edtPlanid_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtPlanid_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtPlanid_Visible), 5, 0)), true);
         edtPlanidProyecto_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtPlanidProyecto_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtPlanidProyecto_Visible), 5, 0)), true);
         edtComunidadid_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtComunidadid_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtComunidadid_Visible), 5, 0)), true);
         edtTipoBeneficiarioid_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtTipoBeneficiarioid_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtTipoBeneficiarioid_Visible), 5, 0)), true);
         edtobjGeneral_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtobjGeneral_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtobjGeneral_Visible), 5, 0)), true);
         edtobjEspecificos_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtobjEspecificos_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtobjEspecificos_Visible), 5, 0)), true);
         edtEstadoid_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtEstadoid_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtEstadoid_Visible), 5, 0)), true);
         edtActividadid_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtActividadid_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtActividadid_Visible), 5, 0)), true);
         edtinforme_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtinforme_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtinforme_Visible), 5, 0)), true);
      }

      protected void S112( )
      {
         /* 'PREPARETRANSACTION' Routine */
         AV7TrnContext = new SdtTransactionContext(context);
         AV7TrnContext.gxTpr_Callerobject = AV13Pgmname;
         AV7TrnContext.gxTpr_Callerondelete = false;
         AV7TrnContext.gxTpr_Callerurl = AV10HTTPRequest.ScriptName+"?"+AV10HTTPRequest.QueryString;
         AV7TrnContext.gxTpr_Transactionname = "Plan";
         AV8TrnContextAtt = new SdtTransactionContext_Attribute(context);
         AV8TrnContextAtt.gxTpr_Attributename = "Planid";
         AV8TrnContextAtt.gxTpr_Attributevalue = StringUtil.Str( (decimal)(AV6Planid), 9, 0);
         AV7TrnContext.gxTpr_Attributes.Add(AV8TrnContextAtt, 0);
         AV9Session.Set("TrnContext", AV7TrnContext.ToXml(false, true, "TransactionContext", "SAPWEB08"));
      }

      public override void setparameters( Object[] obj )
      {
         createObjects();
         initialize();
         A13Planid = Convert.ToInt32(getParm(obj,0));
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A13Planid", StringUtil.LTrim( StringUtil.Str( (decimal)(A13Planid), 9, 0)));
      }

      public override String getresponse( String sGXDynURL )
      {
         initialize_properties( ) ;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         sDynURL = sGXDynURL;
         nGotPars = (short)(1);
         nGXWrapped = (short)(1);
         context.SetWrapped(true);
         PA1L2( ) ;
         WS1L2( ) ;
         WE1L2( ) ;
         this.cleanup();
         context.SetWrapped(false);
         SaveComponentMsgList(sPrefix);
         context.GX_msglist = BackMsgLst;
         return "";
      }

      public void responsestatic( String sGXDynURL )
      {
      }

      public override void componentbind( Object[] obj )
      {
         if ( IsUrlCreated( ) )
         {
            return  ;
         }
         sCtrlA13Planid = (String)((String)getParm(obj,0));
      }

      public override void componentrestorestate( String sPPrefix ,
                                                  String sPSFPrefix )
      {
         sPrefix = sPPrefix + sPSFPrefix;
         PA1L2( ) ;
         WCParametersGet( ) ;
      }

      public override void componentprepare( Object[] obj )
      {
         wbLoad = false;
         sCompPrefix = (String)getParm(obj,0);
         sSFPrefix = (String)getParm(obj,1);
         sPrefix = sCompPrefix + sSFPrefix;
         AddComponentObject(sPrefix, "plangeneral", GetJustCreated( ));
         if ( ( nDoneStart == 0 ) && ( nDynComponent == 0 ) )
         {
            INITWEB( ) ;
         }
         else
         {
            init_default_properties( ) ;
            init_web_controls( ) ;
         }
         PA1L2( ) ;
         if ( ! GetJustCreated( ) && ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 ) && ( context.wbGlbDoneStart == 0 ) )
         {
            WCParametersGet( ) ;
         }
         else
         {
            A13Planid = Convert.ToInt32(getParm(obj,2));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A13Planid", StringUtil.LTrim( StringUtil.Str( (decimal)(A13Planid), 9, 0)));
         }
         wcpOA13Planid = (int)(context.localUtil.CToN( cgiGet( sPrefix+"wcpOA13Planid"), ".", ","));
         if ( ! GetJustCreated( ) && ( ( A13Planid != wcpOA13Planid ) ) )
         {
            setjustcreated();
         }
         wcpOA13Planid = A13Planid;
      }

      protected void WCParametersGet( )
      {
         /* Read Component Parameters. */
         sCtrlA13Planid = cgiGet( sPrefix+"A13Planid_CTRL");
         if ( StringUtil.Len( sCtrlA13Planid) > 0 )
         {
            A13Planid = (int)(context.localUtil.CToN( cgiGet( sCtrlA13Planid), ".", ","));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A13Planid", StringUtil.LTrim( StringUtil.Str( (decimal)(A13Planid), 9, 0)));
         }
         else
         {
            A13Planid = (int)(context.localUtil.CToN( cgiGet( sPrefix+"A13Planid_PARM"), ".", ","));
         }
      }

      public override void componentprocess( String sPPrefix ,
                                             String sPSFPrefix ,
                                             String sCompEvt )
      {
         sCompPrefix = sPPrefix;
         sSFPrefix = sPSFPrefix;
         sPrefix = sCompPrefix + sSFPrefix;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         INITWEB( ) ;
         nDraw = 0;
         PA1L2( ) ;
         sEvt = sCompEvt;
         WCParametersGet( ) ;
         WS1L2( ) ;
         if ( isFullAjaxMode( ) )
         {
            componentdraw();
         }
         SaveComponentMsgList(sPrefix);
         context.GX_msglist = BackMsgLst;
      }

      public override void componentstart( )
      {
         if ( nDoneStart == 0 )
         {
            WCStart( ) ;
         }
      }

      protected void WCStart( )
      {
         nDraw = 1;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         WS1L2( ) ;
         SaveComponentMsgList(sPrefix);
         context.GX_msglist = BackMsgLst;
      }

      protected void WCParametersSet( )
      {
         GxWebStd.gx_hidden_field( context, sPrefix+"A13Planid_PARM", StringUtil.LTrim( StringUtil.NToC( (decimal)(A13Planid), 9, 0, ".", "")));
         if ( StringUtil.Len( StringUtil.RTrim( sCtrlA13Planid)) > 0 )
         {
            GxWebStd.gx_hidden_field( context, sPrefix+"A13Planid_CTRL", StringUtil.RTrim( sCtrlA13Planid));
         }
      }

      public override void componentdraw( )
      {
         if ( nDoneStart == 0 )
         {
            WCStart( ) ;
         }
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         WCParametersSet( ) ;
         WE1L2( ) ;
         SaveComponentMsgList(sPrefix);
         context.GX_msglist = BackMsgLst;
      }

      public override String getstring( String sGXControl )
      {
         String sCtrlName ;
         if ( StringUtil.StrCmp(StringUtil.Substring( sGXControl, 1, 1), "&") == 0 )
         {
            sCtrlName = StringUtil.Substring( sGXControl, 2, StringUtil.Len( sGXControl)-1);
         }
         else
         {
            sCtrlName = sGXControl;
         }
         return cgiGet( sPrefix+"v"+StringUtil.Upper( sCtrlName)) ;
      }

      public override void componentjscripts( )
      {
         include_jscripts( ) ;
      }

      public override void componentthemes( )
      {
         define_styles( ) ;
      }

      protected void define_styles( )
      {
         AddStyleSheetFile("calendar-system.css", "");
         AddThemeStyleSheetFile("", context.GetTheme( )+".css", "?"+GetCacheInvalidationToken( ));
         bool outputEnabled = isOutputEnabled( ) ;
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         idxLst = 1;
         while ( idxLst <= Form.Jscriptsrc.Count )
         {
            context.AddJavascriptSource(StringUtil.RTrim( ((String)Form.Jscriptsrc.Item(idxLst))), "?20191514354893", true);
            idxLst = (int)(idxLst+1);
         }
         if ( ! outputEnabled )
         {
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
         }
         /* End function define_styles */
      }

      protected void include_jscripts( )
      {
         context.AddJavascriptSource("plangeneral.js", "?20191514354894", false);
         /* End function include_jscripts */
      }

      protected void init_web_controls( )
      {
         /* End function init_web_controls */
      }

      protected void init_default_properties( )
      {
         bttBtnupdate_Internalname = sPrefix+"BTNUPDATE";
         bttBtndelete_Internalname = sPrefix+"BTNDELETE";
         edtnombreEvento_Internalname = sPrefix+"NOMBREEVENTO";
         edtComunidadnombre_Internalname = sPrefix+"COMUNIDADNOMBRE";
         edtinicio_Internalname = sPrefix+"INICIO";
         edtfin_Internalname = sPrefix+"FIN";
         edtTipoBeneficiariodescripcion_Internalname = sPrefix+"TIPOBENEFICIARIODESCRIPCION";
         edtPlanidResponsable_Internalname = sPrefix+"PLANIDRESPONSABLE";
         edtPlanidFinanciero_Internalname = sPrefix+"PLANIDFINANCIERO";
         edtPlanidTecnico_Internalname = sPrefix+"PLANIDTECNICO";
         edtdescripcionActividad_Internalname = sPrefix+"DESCRIPCIONACTIVIDAD";
         edtEstadonombre_Internalname = sPrefix+"ESTADONOMBRE";
         edtcantBeneficiarios_Internalname = sPrefix+"CANTBENEFICIARIOS";
         edtActividaddescripcion_Internalname = sPrefix+"ACTIVIDADDESCRIPCION";
         divAttributestable_Internalname = sPrefix+"ATTRIBUTESTABLE";
         edtPlanid_Internalname = sPrefix+"PLANID";
         edtPlanidProyecto_Internalname = sPrefix+"PLANIDPROYECTO";
         edtComunidadid_Internalname = sPrefix+"COMUNIDADID";
         edtTipoBeneficiarioid_Internalname = sPrefix+"TIPOBENEFICIARIOID";
         edtobjGeneral_Internalname = sPrefix+"OBJGENERAL";
         edtobjEspecificos_Internalname = sPrefix+"OBJESPECIFICOS";
         edtEstadoid_Internalname = sPrefix+"ESTADOID";
         edtActividadid_Internalname = sPrefix+"ACTIVIDADID";
         edtinforme_Internalname = sPrefix+"INFORME";
         divMaintable_Internalname = sPrefix+"MAINTABLE";
         Form.Internalname = sPrefix+"FORM";
      }

      public override void initialize_properties( )
      {
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            context.SetDefaultTheme("Carmine");
         }
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
         }
         init_default_properties( ) ;
         edtinforme_Jsonclick = "";
         edtinforme_Enabled = 0;
         edtinforme_Visible = 1;
         edtActividadid_Jsonclick = "";
         edtActividadid_Enabled = 0;
         edtActividadid_Visible = 1;
         edtEstadoid_Jsonclick = "";
         edtEstadoid_Enabled = 0;
         edtEstadoid_Visible = 1;
         edtobjEspecificos_Enabled = 0;
         edtobjEspecificos_Visible = 1;
         edtobjGeneral_Jsonclick = "";
         edtobjGeneral_Enabled = 0;
         edtobjGeneral_Visible = 1;
         edtTipoBeneficiarioid_Jsonclick = "";
         edtTipoBeneficiarioid_Enabled = 0;
         edtTipoBeneficiarioid_Visible = 1;
         edtComunidadid_Jsonclick = "";
         edtComunidadid_Enabled = 0;
         edtComunidadid_Visible = 1;
         edtPlanidProyecto_Jsonclick = "";
         edtPlanidProyecto_Enabled = 0;
         edtPlanidProyecto_Visible = 1;
         edtPlanid_Jsonclick = "";
         edtPlanid_Enabled = 0;
         edtPlanid_Visible = 1;
         edtActividaddescripcion_Link = "";
         edtActividaddescripcion_Enabled = 0;
         edtcantBeneficiarios_Jsonclick = "";
         edtcantBeneficiarios_Enabled = 0;
         edtEstadonombre_Jsonclick = "";
         edtEstadonombre_Enabled = 0;
         edtdescripcionActividad_Enabled = 0;
         edtPlanidTecnico_Jsonclick = "";
         edtPlanidTecnico_Enabled = 0;
         edtPlanidFinanciero_Jsonclick = "";
         edtPlanidFinanciero_Enabled = 0;
         edtPlanidResponsable_Jsonclick = "";
         edtPlanidResponsable_Enabled = 0;
         edtTipoBeneficiariodescripcion_Jsonclick = "";
         edtTipoBeneficiariodescripcion_Enabled = 0;
         edtfin_Jsonclick = "";
         edtfin_Enabled = 0;
         edtinicio_Jsonclick = "";
         edtinicio_Enabled = 0;
         edtComunidadnombre_Jsonclick = "";
         edtComunidadnombre_Enabled = 0;
         edtnombreEvento_Jsonclick = "";
         edtnombreEvento_Enabled = 0;
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( context.isSpaRequest( ) )
            {
               enableJsOutput();
            }
         }
      }

      public override bool SupportAjaxEvent( )
      {
         return true ;
      }

      public override void InitializeDynEvents( )
      {
         setEventMetadata("REFRESH","{handler:'Refresh',iparms:[{av:'A3Actividadid',fld:'ACTIVIDADID',pic:'ZZZZZZZZ9'}]");
         setEventMetadata("REFRESH",",oparms:[]}");
         setEventMetadata("'DOUPDATE'","{handler:'E111L1',iparms:[{av:'A13Planid',fld:'PLANID',pic:'ZZZZZZZZ9'}]");
         setEventMetadata("'DOUPDATE'",",oparms:[]}");
         setEventMetadata("'DODELETE'","{handler:'E121L1',iparms:[{av:'A13Planid',fld:'PLANID',pic:'ZZZZZZZZ9'}]");
         setEventMetadata("'DODELETE'",",oparms:[]}");
         return  ;
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         gxfirstwebparm = "";
         gxfirstwebparm_bkp = "";
         sPrefix = "";
         AV13Pgmname = "";
         sDynURL = "";
         FormProcess = "";
         bodyStyle = "";
         GXKey = "";
         forbiddenHiddens = "";
         GX_FocusControl = "";
         TempTags = "";
         ClassString = "";
         StyleString = "";
         bttBtnupdate_Jsonclick = "";
         bttBtndelete_Jsonclick = "";
         A30nombreEvento = "";
         A130Comunidadnombre = "";
         A31inicio = DateTime.MinValue;
         A32fin = DateTime.MinValue;
         A28TipoBeneficiariodescripcion = "";
         A38descripcionActividad = "";
         A61Estadonombre = "";
         A48Actividaddescripcion = "";
         A36objGeneral = "";
         A37objEspecificos = "";
         Form = new GXWebForm();
         sXEvt = "";
         sEvt = "";
         EvtGridId = "";
         EvtRowId = "";
         sEvtType = "";
         scmdbuf = "";
         H001L2_A13Planid = new int[1] ;
         H001L2_A40informe = new short[1] ;
         H001L2_A3Actividadid = new int[1] ;
         H001L2_n3Actividadid = new bool[] {false} ;
         H001L2_A12Estadoid = new int[1] ;
         H001L2_A37objEspecificos = new String[] {""} ;
         H001L2_A36objGeneral = new String[] {""} ;
         H001L2_A22TipoBeneficiarioid = new int[1] ;
         H001L2_A19Comunidadid = new int[1] ;
         H001L2_A29PlanidProyecto = new int[1] ;
         H001L2_A48Actividaddescripcion = new String[] {""} ;
         H001L2_A39cantBeneficiarios = new int[1] ;
         H001L2_n39cantBeneficiarios = new bool[] {false} ;
         H001L2_A61Estadonombre = new String[] {""} ;
         H001L2_A38descripcionActividad = new String[] {""} ;
         H001L2_A35PlanidTecnico = new int[1] ;
         H001L2_A34PlanidFinanciero = new int[1] ;
         H001L2_A33PlanidResponsable = new int[1] ;
         H001L2_A28TipoBeneficiariodescripcion = new String[] {""} ;
         H001L2_A32fin = new DateTime[] {DateTime.MinValue} ;
         H001L2_A31inicio = new DateTime[] {DateTime.MinValue} ;
         H001L2_A130Comunidadnombre = new String[] {""} ;
         H001L2_A30nombreEvento = new String[] {""} ;
         hsh = "";
         AV7TrnContext = new SdtTransactionContext(context);
         AV10HTTPRequest = new GxHttpRequest( context);
         AV8TrnContextAtt = new SdtTransactionContext_Attribute(context);
         AV9Session = context.GetSession();
         BackMsgLst = new msglist();
         LclMsgLst = new msglist();
         sCtrlA13Planid = "";
         pr_datastore1 = new DataStoreProvider(context, new GeneXus.Programs.plangeneral__datastore1(),
            new Object[][] {
                new Object[] {
               H001L2_A13Planid, H001L2_A40informe, H001L2_A3Actividadid, H001L2_n3Actividadid, H001L2_A12Estadoid, H001L2_A37objEspecificos, H001L2_A36objGeneral, H001L2_A22TipoBeneficiarioid, H001L2_A19Comunidadid, H001L2_A29PlanidProyecto,
               H001L2_A48Actividaddescripcion, H001L2_A39cantBeneficiarios, H001L2_n39cantBeneficiarios, H001L2_A61Estadonombre, H001L2_A38descripcionActividad, H001L2_A35PlanidTecnico, H001L2_A34PlanidFinanciero, H001L2_A33PlanidResponsable, H001L2_A28TipoBeneficiariodescripcion, H001L2_A32fin,
               H001L2_A31inicio, H001L2_A130Comunidadnombre, H001L2_A30nombreEvento
               }
            }
         );
         AV13Pgmname = "PlanGeneral";
         /* GeneXus formulas. */
         AV13Pgmname = "PlanGeneral";
         context.Gx_err = 0;
      }

      private short nGotPars ;
      private short GxWebError ;
      private short nDynComponent ;
      private short initialized ;
      private short wbEnd ;
      private short wbStart ;
      private short A40informe ;
      private short nDraw ;
      private short nDoneStart ;
      private short nDonePA ;
      private short gxcookieaux ;
      private short nGXWrapped ;
      private int A13Planid ;
      private int wcpOA13Planid ;
      private int A3Actividadid ;
      private int edtnombreEvento_Enabled ;
      private int edtComunidadnombre_Enabled ;
      private int edtinicio_Enabled ;
      private int edtfin_Enabled ;
      private int edtTipoBeneficiariodescripcion_Enabled ;
      private int A33PlanidResponsable ;
      private int edtPlanidResponsable_Enabled ;
      private int A34PlanidFinanciero ;
      private int edtPlanidFinanciero_Enabled ;
      private int A35PlanidTecnico ;
      private int edtPlanidTecnico_Enabled ;
      private int edtdescripcionActividad_Enabled ;
      private int edtEstadonombre_Enabled ;
      private int A39cantBeneficiarios ;
      private int edtcantBeneficiarios_Enabled ;
      private int edtActividaddescripcion_Enabled ;
      private int edtPlanid_Enabled ;
      private int edtPlanid_Visible ;
      private int A29PlanidProyecto ;
      private int edtPlanidProyecto_Enabled ;
      private int edtPlanidProyecto_Visible ;
      private int A19Comunidadid ;
      private int edtComunidadid_Enabled ;
      private int edtComunidadid_Visible ;
      private int A22TipoBeneficiarioid ;
      private int edtTipoBeneficiarioid_Enabled ;
      private int edtTipoBeneficiarioid_Visible ;
      private int edtobjGeneral_Visible ;
      private int edtobjGeneral_Enabled ;
      private int edtobjEspecificos_Visible ;
      private int edtobjEspecificos_Enabled ;
      private int A12Estadoid ;
      private int edtEstadoid_Enabled ;
      private int edtEstadoid_Visible ;
      private int edtActividadid_Enabled ;
      private int edtActividadid_Visible ;
      private int edtinforme_Enabled ;
      private int edtinforme_Visible ;
      private int AV6Planid ;
      private int idxLst ;
      private String gxfirstwebparm ;
      private String gxfirstwebparm_bkp ;
      private String sPrefix ;
      private String sCompPrefix ;
      private String sSFPrefix ;
      private String AV13Pgmname ;
      private String sDynURL ;
      private String FormProcess ;
      private String bodyStyle ;
      private String GXKey ;
      private String forbiddenHiddens ;
      private String GX_FocusControl ;
      private String divMaintable_Internalname ;
      private String TempTags ;
      private String ClassString ;
      private String StyleString ;
      private String bttBtnupdate_Internalname ;
      private String bttBtnupdate_Jsonclick ;
      private String bttBtndelete_Internalname ;
      private String bttBtndelete_Jsonclick ;
      private String divAttributestable_Internalname ;
      private String edtnombreEvento_Internalname ;
      private String edtnombreEvento_Jsonclick ;
      private String edtComunidadnombre_Internalname ;
      private String edtComunidadnombre_Jsonclick ;
      private String edtinicio_Internalname ;
      private String edtinicio_Jsonclick ;
      private String edtfin_Internalname ;
      private String edtfin_Jsonclick ;
      private String edtTipoBeneficiariodescripcion_Internalname ;
      private String edtTipoBeneficiariodescripcion_Jsonclick ;
      private String edtPlanidResponsable_Internalname ;
      private String edtPlanidResponsable_Jsonclick ;
      private String edtPlanidFinanciero_Internalname ;
      private String edtPlanidFinanciero_Jsonclick ;
      private String edtPlanidTecnico_Internalname ;
      private String edtPlanidTecnico_Jsonclick ;
      private String edtdescripcionActividad_Internalname ;
      private String edtEstadonombre_Internalname ;
      private String edtEstadonombre_Jsonclick ;
      private String edtcantBeneficiarios_Internalname ;
      private String edtcantBeneficiarios_Jsonclick ;
      private String edtActividaddescripcion_Internalname ;
      private String edtActividaddescripcion_Link ;
      private String edtPlanid_Internalname ;
      private String edtPlanid_Jsonclick ;
      private String edtPlanidProyecto_Internalname ;
      private String edtPlanidProyecto_Jsonclick ;
      private String edtComunidadid_Internalname ;
      private String edtComunidadid_Jsonclick ;
      private String edtTipoBeneficiarioid_Internalname ;
      private String edtTipoBeneficiarioid_Jsonclick ;
      private String edtobjGeneral_Internalname ;
      private String edtobjGeneral_Jsonclick ;
      private String edtobjEspecificos_Internalname ;
      private String edtEstadoid_Internalname ;
      private String edtEstadoid_Jsonclick ;
      private String edtActividadid_Internalname ;
      private String edtActividadid_Jsonclick ;
      private String edtinforme_Internalname ;
      private String edtinforme_Jsonclick ;
      private String sXEvt ;
      private String sEvt ;
      private String EvtGridId ;
      private String EvtRowId ;
      private String sEvtType ;
      private String scmdbuf ;
      private String hsh ;
      private String sCtrlA13Planid ;
      private DateTime A31inicio ;
      private DateTime A32fin ;
      private bool entryPointCalled ;
      private bool toggleJsOutput ;
      private bool wbLoad ;
      private bool Rfr0gs ;
      private bool wbErr ;
      private bool gxdyncontrolsrefreshing ;
      private bool n3Actividadid ;
      private bool n39cantBeneficiarios ;
      private bool returnInSub ;
      private String A30nombreEvento ;
      private String A130Comunidadnombre ;
      private String A28TipoBeneficiariodescripcion ;
      private String A38descripcionActividad ;
      private String A61Estadonombre ;
      private String A48Actividaddescripcion ;
      private String A36objGeneral ;
      private String A37objEspecificos ;
      private GXWebForm Form ;
      private IGxDataStore dsDataStore1 ;
      private IGxDataStore dsDefault ;
      private IDataStoreProvider pr_datastore1 ;
      private int[] H001L2_A13Planid ;
      private short[] H001L2_A40informe ;
      private int[] H001L2_A3Actividadid ;
      private bool[] H001L2_n3Actividadid ;
      private int[] H001L2_A12Estadoid ;
      private String[] H001L2_A37objEspecificos ;
      private String[] H001L2_A36objGeneral ;
      private int[] H001L2_A22TipoBeneficiarioid ;
      private int[] H001L2_A19Comunidadid ;
      private int[] H001L2_A29PlanidProyecto ;
      private String[] H001L2_A48Actividaddescripcion ;
      private int[] H001L2_A39cantBeneficiarios ;
      private bool[] H001L2_n39cantBeneficiarios ;
      private String[] H001L2_A61Estadonombre ;
      private String[] H001L2_A38descripcionActividad ;
      private int[] H001L2_A35PlanidTecnico ;
      private int[] H001L2_A34PlanidFinanciero ;
      private int[] H001L2_A33PlanidResponsable ;
      private String[] H001L2_A28TipoBeneficiariodescripcion ;
      private DateTime[] H001L2_A32fin ;
      private DateTime[] H001L2_A31inicio ;
      private String[] H001L2_A130Comunidadnombre ;
      private String[] H001L2_A30nombreEvento ;
      private msglist BackMsgLst ;
      private msglist LclMsgLst ;
      private GxHttpRequest AV10HTTPRequest ;
      private IGxSession AV9Session ;
      private SdtTransactionContext AV7TrnContext ;
      private SdtTransactionContext_Attribute AV8TrnContextAtt ;
   }

   public class plangeneral__datastore1 : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmH001L2 ;
          prmH001L2 = new Object[] {
          new Object[] {"@Planid",SqlDbType.Int,9,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("H001L2", "SELECT T1.[id] AS Planid, T1.[informe], T1.[idActividad] AS Actividadid, T1.[idEstado] AS Estadoid, T1.[objEspecificos], T1.[objGeneral], T1.[idTipoBeneficiario] AS TipoBeneficiarioid, T1.[idLugar] AS Comunidadid, T1.[idProyecto], T2.[descripcion] AS Actividaddescripcion, T1.[cantBeneficiarios], T3.[nombre] AS Estadonombre, T1.[descripcionActividad], T1.[idTecnico], T1.[idFinanciero], T1.[idResponsable], T4.[descripcion] AS TipoBeneficiariodescripcion, T1.[fin], T1.[inicio], T5.[nombre] AS Comunidadnombre, T1.[nombreEvento] FROM ((((dbo.[Plan] T1 WITH (NOLOCK) LEFT JOIN dbo.[Actividad] T2 WITH (NOLOCK) ON T2.[id] = T1.[idActividad]) INNER JOIN dbo.[Estado] T3 WITH (NOLOCK) ON T3.[id] = T1.[idEstado]) INNER JOIN dbo.[TipoBeneficiario] T4 WITH (NOLOCK) ON T4.[id] = T1.[idTipoBeneficiario]) INNER JOIN dbo.[Comunidad] T5 WITH (NOLOCK) ON T5.[id] = T1.[idLugar]) WHERE T1.[id] = @Planid ORDER BY T1.[id] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH001L2,1,0,true,true )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((short[]) buf[1])[0] = rslt.getShort(2) ;
                ((int[]) buf[2])[0] = rslt.getInt(3) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(3);
                ((int[]) buf[4])[0] = rslt.getInt(4) ;
                ((String[]) buf[5])[0] = rslt.getVarchar(5) ;
                ((String[]) buf[6])[0] = rslt.getVarchar(6) ;
                ((int[]) buf[7])[0] = rslt.getInt(7) ;
                ((int[]) buf[8])[0] = rslt.getInt(8) ;
                ((int[]) buf[9])[0] = rslt.getInt(9) ;
                ((String[]) buf[10])[0] = rslt.getVarchar(10) ;
                ((int[]) buf[11])[0] = rslt.getInt(11) ;
                ((bool[]) buf[12])[0] = rslt.wasNull(11);
                ((String[]) buf[13])[0] = rslt.getVarchar(12) ;
                ((String[]) buf[14])[0] = rslt.getVarchar(13) ;
                ((int[]) buf[15])[0] = rslt.getInt(14) ;
                ((int[]) buf[16])[0] = rslt.getInt(15) ;
                ((int[]) buf[17])[0] = rslt.getInt(16) ;
                ((String[]) buf[18])[0] = rslt.getVarchar(17) ;
                ((DateTime[]) buf[19])[0] = rslt.getGXDate(18) ;
                ((DateTime[]) buf[20])[0] = rslt.getGXDate(19) ;
                ((String[]) buf[21])[0] = rslt.getVarchar(20) ;
                ((String[]) buf[22])[0] = rslt.getVarchar(21) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
       }
    }

    public String getDataStoreName( )
    {
       return "DATASTORE1";
    }

 }

}
