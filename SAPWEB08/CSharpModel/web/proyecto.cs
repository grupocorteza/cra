/*
               File: Proyecto
        Description: Proyecto
             Author: GeneXus C# Generator version 16_0_0-127771
       Generated on: 1/5/2019 14:35:53.23
       Program type: Callable routine
          Main DBMS: SQL Server
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Xml.Serialization;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class proyecto : GXDataArea, System.Web.SessionState.IRequiresSessionState
   {
      protected void INITENV( )
      {
         if ( GxWebError != 0 )
         {
            return  ;
         }
      }

      protected void INITTRN( )
      {
         initialize_properties( ) ;
         entryPointCalled = false;
         gxfirstwebparm = GetNextPar( );
         gxfirstwebparm_bkp = gxfirstwebparm;
         gxfirstwebparm = DecryptAjaxCall( gxfirstwebparm);
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         if ( StringUtil.StrCmp(gxfirstwebparm, "dyncall") == 0 )
         {
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            dyncall( GetNextPar( )) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxExecAct_"+"gxLoad_8") == 0 )
         {
            A2Programaid = (int)(NumberUtil.Val( GetNextPar( ), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2Programaid", StringUtil.LTrim( StringUtil.Str( (decimal)(A2Programaid), 9, 0)));
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxLoad_8( A2Programaid) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxExecAct_"+"gxLoad_10") == 0 )
         {
            A15Paisid = (int)(NumberUtil.Val( GetNextPar( ), "."));
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxLoad_10( A15Paisid) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxEvt") == 0 )
         {
            setAjaxEventMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxfirstwebparm = GetNextPar( );
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxfullajaxEvt") == 0 )
         {
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxfirstwebparm = GetNextPar( );
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxNewRow_"+"Gridproyecto_proyectopais") == 0 )
         {
            nRC_GXsfl_58 = (short)(NumberUtil.Val( GetNextPar( ), "."));
            nGXsfl_58_idx = (short)(NumberUtil.Val( GetNextPar( ), "."));
            sGXsfl_58_idx = GetNextPar( );
            Gx_mode = GetNextPar( );
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxnrGridproyecto_proyectopais_newrow( ) ;
            return  ;
         }
         else
         {
            if ( ! IsValidAjaxCall( false) )
            {
               GxWebError = 1;
               return  ;
            }
            gxfirstwebparm = gxfirstwebparm_bkp;
         }
         if ( ! entryPointCalled && ! ( isAjaxCallMode( ) || isFullAjaxMode( ) ) )
         {
            Gx_mode = gxfirstwebparm;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!")), context));
            if ( StringUtil.StrCmp(gxfirstwebparm, "viewer") != 0 )
            {
               AV7Proyectoid = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV7Proyectoid", StringUtil.LTrim( StringUtil.Str( (decimal)(AV7Proyectoid), 9, 0)));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vPROYECTOID", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV7Proyectoid), "ZZZZZZZZ9"), context));
            }
         }
         if ( context.isSpaRequest( ) )
         {
            enableJsOutput();
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.GetCookie( "GX_SESSION_ID"))) )
         {
            gxcookieaux = context.SetCookie( "GX_SESSION_ID", Encrypt64( Crypto.GetEncryptionKey( ), Crypto.GetServerKey( )), "", (DateTime)(DateTime.MinValue), "", 0);
         }
         GXKey = Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
         toggleJsOutput = isJsOutputEnabled( );
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         init_web_controls( ) ;
         if ( toggleJsOutput )
         {
            if ( context.isSpaRequest( ) )
            {
               enableJsOutput();
            }
         }
         if ( ! context.isSpaRequest( ) )
         {
            Form.Meta.addItem("generator", "GeneXus C# 16_0_0-127771", 0) ;
            Form.Meta.addItem("description", "Proyecto", 0) ;
         }
         context.wjLoc = "";
         context.nUserReturn = 0;
         context.wbHandled = 0;
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
         }
         GX_FocusControl = edtProgramaid_Internalname;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         wbErr = false;
         context.SetDefaultTheme("Carmine");
         if ( ! context.IsLocalStorageSupported( ) )
         {
            context.PushCurrentUrl();
         }
      }

      public proyecto( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsDataStore1 = context.GetDataStore("DataStore1");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("Carmine");
      }

      public proyecto( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsDataStore1 = context.GetDataStore("DataStore1");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( String aP0_Gx_mode ,
                           int aP1_Proyectoid )
      {
         this.Gx_mode = aP0_Gx_mode;
         this.AV7Proyectoid = aP1_Proyectoid;
         executePrivate();
      }

      void executePrivate( )
      {
         isStatic = false;
         webExecute();
      }

      protected override void createObjects( )
      {
      }

      public override void webExecute( )
      {
         if ( initialized == 0 )
         {
            createObjects();
            initialize();
         }
         INITENV( ) ;
         INITTRN( ) ;
         if ( ( GxWebError == 0 ) && ! isAjaxCallMode( ) )
         {
            MasterPageObj = (GXMasterPage) ClassLoader.GetInstance("rwdmasterpage", "GeneXus.Programs.rwdmasterpage", new Object[] {new GxContext( context.handle, context.DataStores, context.HttpContext)});
            MasterPageObj.setDataArea(this,false);
            ValidateSpaRequest();
            MasterPageObj.webExecute();
            if ( ( GxWebError == 0 ) && context.isAjaxRequest( ) )
            {
               enableOutput();
               if ( ! context.isAjaxRequest( ) )
               {
                  context.GX_webresponse.AppendHeader("Cache-Control", "no-store");
               }
               if ( ! context.WillRedirect( ) )
               {
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               }
               else
               {
                  if ( context.isAjaxRequest( ) )
                  {
                     disableOutput();
                  }
                  RenderHtmlHeaders( ) ;
                  context.Redirect( context.wjLoc );
                  context.DispatchAjaxCommands();
               }
            }
         }
         this.cleanup();
      }

      protected void fix_multi_value_controls( )
      {
      }

      protected void Draw( )
      {
         if ( context.isAjaxRequest( ) )
         {
            disableOutput();
         }
         if ( ! GxWebStd.gx_redirect( context) )
         {
            disable_std_buttons( ) ;
            enableDisable( ) ;
            set_caption( ) ;
            /* Form start */
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "Section", "left", "top", " "+"data-gx-base-lib=\"bootstrapv3\""+" "+"data-abstract-form"+" ", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, divMaintable_Internalname, 1, 0, "px", 0, "px", "WWAdvancedContainer", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12 col-sm-8 col-sm-offset-2", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, divTitlecontainer_Internalname, 1, 0, "px", 0, "px", "TableTop", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12", "left", "top", "", "", "div");
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTitle_Internalname, "Proyecto", "", "", lblTitle_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "Title", 0, "", 1, 1, 0, "HLP_Proyecto.htm");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12", "left", "top", "", "", "div");
            ClassString = "ErrorViewer";
            StyleString = "";
            GxWebStd.gx_msg_list( context, "", context.GX_msglist.DisplayMode, StyleString, ClassString, "", "false");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12 col-sm-8 col-sm-offset-2", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, divFormcontainer_Internalname, 1, 0, "px", 0, "px", "FormContainer", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, divToolbarcell_Internalname, 1, 0, "px", 0, "px", "col-xs-12 col-sm-9 col-sm-offset-3 ToolbarCellClass", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "gx-action-group ActionGroup", "left", "top", " "+"data-gx-actiongroup-type=\"toolbar\""+" ", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "btn-group", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "gx-button", "left", "top", "", "", "div");
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 21,'',false,'',0)\"";
            ClassString = "BtnFirst";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtn_first_Internalname, "", "", bttBtn_first_Jsonclick, 5, "", "", StyleString, ClassString, bttBtn_first_Visible, 0, "standard", "'"+""+"'"+",false,"+"'"+"EFIRST."+"'", TempTags, "", context.GetButtonType( ), "HLP_Proyecto.htm");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "gx-button", "left", "top", "", "", "div");
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 23,'',false,'',0)\"";
            ClassString = "BtnPrevious";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtn_previous_Internalname, "", "", bttBtn_previous_Jsonclick, 5, "", "", StyleString, ClassString, bttBtn_previous_Visible, 0, "standard", "'"+""+"'"+",false,"+"'"+"EPREVIOUS."+"'", TempTags, "", context.GetButtonType( ), "HLP_Proyecto.htm");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "gx-button", "left", "top", "", "", "div");
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 25,'',false,'',0)\"";
            ClassString = "BtnNext";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtn_next_Internalname, "", "", bttBtn_next_Jsonclick, 5, "", "", StyleString, ClassString, bttBtn_next_Visible, 0, "standard", "'"+""+"'"+",false,"+"'"+"ENEXT."+"'", TempTags, "", context.GetButtonType( ), "HLP_Proyecto.htm");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "gx-button", "left", "top", "", "", "div");
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 27,'',false,'',0)\"";
            ClassString = "BtnLast";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtn_last_Internalname, "", "", bttBtn_last_Jsonclick, 5, "", "", StyleString, ClassString, bttBtn_last_Visible, 0, "standard", "'"+""+"'"+",false,"+"'"+"ELAST."+"'", TempTags, "", context.GetButtonType( ), "HLP_Proyecto.htm");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "gx-button", "left", "top", "", "", "div");
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 29,'',false,'',0)\"";
            ClassString = "BtnSelect";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtn_select_Internalname, "", "Select", bttBtn_select_Jsonclick, 5, "Select", "", StyleString, ClassString, bttBtn_select_Visible, 0, "standard", "'"+""+"'"+",false,"+"'"+"ESELECT."+"'", TempTags, "", 2, "HLP_Proyecto.htm");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12 FormCellAdvanced", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "form-group gx-form-group", "left", "top", ""+" data-gx-for=\""+edtProgramaid_Internalname+"\"", "", "div");
            /* Attribute/Variable Label */
            GxWebStd.gx_label_element( context, edtProgramaid_Internalname, "Programa", "col-sm-3 AttributeLabel", 1, true);
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-sm-9 gx-attribute", "left", "top", "", "", "div");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 34,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtProgramaid_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A2Programaid), 9, 0, ".", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(A2Programaid), "ZZZZZZZZ9")), TempTags+" onchange=\""+"gx.num.valid_integer( this,',');"+";gx.evt.onchange(this, event)\" "+" onblur=\""+";gx.evt.onblur(this,34);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtProgramaid_Jsonclick, 0, "Attribute", "", "", "", "", 1, edtProgramaid_Enabled, 1, "number", "1", 9, "chr", 1, "row", 9, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_Proyecto.htm");
            /* Static images/pictures */
            ClassString = "gx-prompt Image";
            StyleString = "";
            sImgUrl = (String)(context.GetImagePath( "prompt.gif", "", context.GetTheme( )));
            GxWebStd.gx_bitmap( context, imgprompt_2_Internalname, sImgUrl, imgprompt_2_Link, "", "", context.GetTheme( ), imgprompt_2_Visible, 1, "", "", 0, 0, 0, "", 0, "", 0, 0, 0, "", "", StyleString, ClassString, "", "", "", "", "", "", "", 1, false, false, context.GetImageSrcSet( sImgUrl), "HLP_Proyecto.htm");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12 FormCell", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "form-group gx-form-group", "left", "top", ""+" data-gx-for=\""+edtProgramanombre_Internalname+"\"", "", "div");
            /* Attribute/Variable Label */
            GxWebStd.gx_label_element( context, edtProgramanombre_Internalname, "Programanombre", "col-sm-3 AttributeLabel", 1, true);
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-sm-9 gx-attribute", "left", "top", "", "", "div");
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtProgramanombre_Internalname, A72Programanombre, StringUtil.RTrim( context.localUtil.Format( A72Programanombre, "")), "", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtProgramanombre_Jsonclick, 0, "Attribute", "", "", "", "", 1, edtProgramanombre_Enabled, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_Proyecto.htm");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12 FormCell", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "form-group gx-form-group", "left", "top", ""+" data-gx-for=\""+edtProyectonombre_Internalname+"\"", "", "div");
            /* Attribute/Variable Label */
            GxWebStd.gx_label_element( context, edtProyectonombre_Internalname, "Proyecto", "col-sm-3 AttributeLabel", 1, true);
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-sm-9 gx-attribute", "left", "top", "", "", "div");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 44,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtProyectonombre_Internalname, A74Proyectonombre, StringUtil.RTrim( context.localUtil.Format( A74Proyectonombre, "")), TempTags+" onchange=\""+""+";gx.evt.onchange(this, event)\" "+" onblur=\""+";gx.evt.onblur(this,44);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtProyectonombre_Jsonclick, 0, "Attribute", "", "", "", "", 1, edtProyectonombre_Enabled, 0, "text", "", 80, "chr", 1, "row", 100, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_Proyecto.htm");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12 FormCell", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "form-group gx-form-group", "left", "top", ""+" data-gx-for=\""+edtmarco_Internalname+"\"", "", "div");
            /* Attribute/Variable Label */
            GxWebStd.gx_label_element( context, edtmarco_Internalname, "Descripci�n", "col-sm-3 AttributeLabel", 1, true);
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-sm-9 gx-attribute", "left", "top", "", "", "div");
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 49,'',false,'',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtmarco_Internalname, A75marco, "", TempTags+" onchange=\""+""+";gx.evt.onchange(this, event)\" "+" onblur=\""+";gx.evt.onblur(this,49);\"", 0, 1, edtmarco_Enabled, 0, 80, "chr", 10, "row", StyleString, ClassString, "", "", "2000", -1, 0, "", "", -1, true, "", "'"+""+"'"+",false,"+"'"+""+"'", 0, "HLP_Proyecto.htm");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12 LevelTable", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, divProyectopaistable_Internalname, 1, 0, "px", 0, "px", "LevelTable", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12 FormCell", "left", "top", "", "", "div");
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTitleproyectopais_Internalname, "Pa�ses asociados al proyecto", "", "", lblTitleproyectopais_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "Title", 0, "", 1, 1, 0, "HLP_Proyecto.htm");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12", "left", "top", "", "", "div");
            gxdraw_Gridproyecto_proyectopais( ) ;
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12", "Center", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "gx-action-group Confirm", "left", "top", " "+"data-gx-actiongroup-type=\"toolbar\""+" ", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "gx-button", "left", "top", "", "", "div");
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 67,'',false,'',0)\"";
            ClassString = "BtnEnter";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtn_enter_Internalname, "", "Confirm", bttBtn_enter_Jsonclick, 5, "Confirm", "", StyleString, ClassString, bttBtn_enter_Visible, bttBtn_enter_Enabled, "standard", "'"+""+"'"+",false,"+"'"+"EENTER."+"'", TempTags, "", context.GetButtonType( ), "HLP_Proyecto.htm");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "gx-button", "left", "top", "", "", "div");
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 69,'',false,'',0)\"";
            ClassString = "BtnCancel";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtn_cancel_Internalname, "", "Cancel", bttBtn_cancel_Jsonclick, 1, "Cancel", "", StyleString, ClassString, bttBtn_cancel_Visible, 1, "standard", "'"+""+"'"+",false,"+"'"+"ECANCEL."+"'", TempTags, "", context.GetButtonType( ), "HLP_Proyecto.htm");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "gx-button", "left", "top", "", "", "div");
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 71,'',false,'',0)\"";
            ClassString = "BtnDelete";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtn_delete_Internalname, "", "Delete", bttBtn_delete_Jsonclick, 5, "Delete", "", StyleString, ClassString, bttBtn_delete_Visible, bttBtn_delete_Enabled, "standard", "'"+""+"'"+",false,"+"'"+"EDELETE."+"'", TempTags, "", context.GetButtonType( ), "HLP_Proyecto.htm");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "Center", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            fix_multi_value_controls( ) ;
         }
         /* Execute Exit event if defined. */
      }

      protected void gxdraw_Gridproyecto_proyectopais( )
      {
         /*  Grid Control  */
         Gridproyecto_proyectopaisContainer.AddObjectProperty("GridName", "Gridproyecto_proyectopais");
         Gridproyecto_proyectopaisContainer.AddObjectProperty("Header", subGridproyecto_proyectopais_Header);
         Gridproyecto_proyectopaisContainer.AddObjectProperty("Class", "Grid");
         Gridproyecto_proyectopaisContainer.AddObjectProperty("Cellpadding", StringUtil.LTrim( StringUtil.NToC( (decimal)(1), 4, 0, ".", "")));
         Gridproyecto_proyectopaisContainer.AddObjectProperty("Cellspacing", StringUtil.LTrim( StringUtil.NToC( (decimal)(2), 4, 0, ".", "")));
         Gridproyecto_proyectopaisContainer.AddObjectProperty("Backcolorstyle", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGridproyecto_proyectopais_Backcolorstyle), 1, 0, ".", "")));
         Gridproyecto_proyectopaisContainer.AddObjectProperty("CmpContext", "");
         Gridproyecto_proyectopaisContainer.AddObjectProperty("InMasterPage", "false");
         Gridproyecto_proyectopaisColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
         Gridproyecto_proyectopaisColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(A132ProyectoPaisid), 4, 0, ".", "")));
         Gridproyecto_proyectopaisColumn.AddObjectProperty("Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtProyectoPaisid_Enabled), 5, 0, ".", "")));
         Gridproyecto_proyectopaisContainer.AddColumnProperties(Gridproyecto_proyectopaisColumn);
         Gridproyecto_proyectopaisColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
         Gridproyecto_proyectopaisColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(A15Paisid), 9, 0, ".", "")));
         Gridproyecto_proyectopaisColumn.AddObjectProperty("Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtPaisid_Enabled), 5, 0, ".", "")));
         Gridproyecto_proyectopaisContainer.AddColumnProperties(Gridproyecto_proyectopaisColumn);
         Gridproyecto_proyectopaisColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
         Gridproyecto_proyectopaisContainer.AddColumnProperties(Gridproyecto_proyectopaisColumn);
         Gridproyecto_proyectopaisColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
         Gridproyecto_proyectopaisColumn.AddObjectProperty("Value", A117codigo);
         Gridproyecto_proyectopaisColumn.AddObjectProperty("Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtcodigo_Enabled), 5, 0, ".", "")));
         Gridproyecto_proyectopaisContainer.AddColumnProperties(Gridproyecto_proyectopaisColumn);
         Gridproyecto_proyectopaisColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
         Gridproyecto_proyectopaisColumn.AddObjectProperty("Value", A118Paisnombre);
         Gridproyecto_proyectopaisColumn.AddObjectProperty("Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtPaisnombre_Enabled), 5, 0, ".", "")));
         Gridproyecto_proyectopaisContainer.AddColumnProperties(Gridproyecto_proyectopaisColumn);
         Gridproyecto_proyectopaisContainer.AddObjectProperty("Selectedindex", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGridproyecto_proyectopais_Selectedindex), 4, 0, ".", "")));
         Gridproyecto_proyectopaisContainer.AddObjectProperty("Allowselection", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGridproyecto_proyectopais_Allowselection), 1, 0, ".", "")));
         Gridproyecto_proyectopaisContainer.AddObjectProperty("Selectioncolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGridproyecto_proyectopais_Selectioncolor), 9, 0, ".", "")));
         Gridproyecto_proyectopaisContainer.AddObjectProperty("Allowhover", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGridproyecto_proyectopais_Allowhovering), 1, 0, ".", "")));
         Gridproyecto_proyectopaisContainer.AddObjectProperty("Hovercolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGridproyecto_proyectopais_Hoveringcolor), 9, 0, ".", "")));
         Gridproyecto_proyectopaisContainer.AddObjectProperty("Allowcollapsing", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGridproyecto_proyectopais_Allowcollapsing), 1, 0, ".", "")));
         Gridproyecto_proyectopaisContainer.AddObjectProperty("Collapsed", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGridproyecto_proyectopais_Collapsed), 1, 0, ".", "")));
         nGXsfl_58_idx = 0;
         if ( ( nKeyPressed == 1 ) && ( AnyError == 0 ) )
         {
            /* Enter key processing. */
            nBlankRcdCount26 = 5;
            if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
            {
               /* Display confirmed (stored) records */
               nRcdExists_26 = 1;
               ScanStart0P26( ) ;
               while ( RcdFound26 != 0 )
               {
                  init_level_properties26( ) ;
                  getByPrimaryKey0P26( ) ;
                  AddRow0P26( ) ;
                  ScanNext0P26( ) ;
               }
               ScanEnd0P26( ) ;
               nBlankRcdCount26 = 5;
            }
         }
         else if ( ( nKeyPressed == 3 ) || ( nKeyPressed == 4 ) || ( ( nKeyPressed == 1 ) && ( AnyError != 0 ) ) )
         {
            /* Button check  or addlines. */
            standaloneNotModal0P26( ) ;
            standaloneModal0P26( ) ;
            sMode26 = Gx_mode;
            while ( nGXsfl_58_idx < nRC_GXsfl_58 )
            {
               bGXsfl_58_Refreshing = true;
               ReadRow0P26( ) ;
               edtProyectoPaisid_Enabled = (int)(context.localUtil.CToN( cgiGet( "PROYECTOPAISID_"+sGXsfl_58_idx+"Enabled"), ".", ","));
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtProyectoPaisid_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtProyectoPaisid_Enabled), 5, 0)), !bGXsfl_58_Refreshing);
               edtPaisid_Enabled = (int)(context.localUtil.CToN( cgiGet( "PAISID_"+sGXsfl_58_idx+"Enabled"), ".", ","));
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtPaisid_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtPaisid_Enabled), 5, 0)), !bGXsfl_58_Refreshing);
               edtcodigo_Enabled = (int)(context.localUtil.CToN( cgiGet( "CODIGO_"+sGXsfl_58_idx+"Enabled"), ".", ","));
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtcodigo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtcodigo_Enabled), 5, 0)), !bGXsfl_58_Refreshing);
               edtPaisnombre_Enabled = (int)(context.localUtil.CToN( cgiGet( "PAISNOMBRE_"+sGXsfl_58_idx+"Enabled"), ".", ","));
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtPaisnombre_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtPaisnombre_Enabled), 5, 0)), !bGXsfl_58_Refreshing);
               imgprompt_2_Link = cgiGet( "PROMPT_15_"+sGXsfl_58_idx+"Link");
               if ( ( nRcdExists_26 == 0 ) && ( StringUtil.StrCmp(Gx_mode, "INS") != 0 ) )
               {
                  Gx_mode = "INS";
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!")), context));
                  standaloneModal0P26( ) ;
               }
               SendRow0P26( ) ;
               bGXsfl_58_Refreshing = false;
            }
            Gx_mode = sMode26;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!")), context));
         }
         else
         {
            /* Get or get-alike key processing. */
            nBlankRcdCount26 = 5;
            nRcdExists_26 = 1;
            if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
            {
               ScanStart0P26( ) ;
               while ( RcdFound26 != 0 )
               {
                  sGXsfl_58_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_58_idx+1), 4, 0)), 4, "0");
                  SubsflControlProps_5826( ) ;
                  init_level_properties26( ) ;
                  standaloneNotModal0P26( ) ;
                  getByPrimaryKey0P26( ) ;
                  standaloneModal0P26( ) ;
                  AddRow0P26( ) ;
                  ScanNext0P26( ) ;
               }
               ScanEnd0P26( ) ;
            }
         }
         /* Initialize fields for 'new' records and send them. */
         if ( ( StringUtil.StrCmp(Gx_mode, "DSP") != 0 ) && ( StringUtil.StrCmp(Gx_mode, "DLT") != 0 ) )
         {
            sMode26 = Gx_mode;
            Gx_mode = "INS";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!")), context));
            sGXsfl_58_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_58_idx+1), 4, 0)), 4, "0");
            SubsflControlProps_5826( ) ;
            InitAll0P26( ) ;
            init_level_properties26( ) ;
            standaloneNotModal0P26( ) ;
            standaloneModal0P26( ) ;
            nRcdExists_26 = 0;
            nIsMod_26 = 0;
            nRcdDeleted_26 = 0;
            nBlankRcdCount26 = (short)(nBlankRcdUsr26+nBlankRcdCount26);
            fRowAdded = 0;
            while ( nBlankRcdCount26 > 0 )
            {
               AddRow0P26( ) ;
               if ( ( nKeyPressed == 4 ) && ( fRowAdded == 0 ) )
               {
                  fRowAdded = 1;
                  GX_FocusControl = edtProyectoPaisid_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               }
               nBlankRcdCount26 = (short)(nBlankRcdCount26-1);
            }
            Gx_mode = sMode26;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!")), context));
         }
         sStyleString = "";
         context.WriteHtmlText( "<div id=\""+"Gridproyecto_proyectopaisContainer"+"Div\" "+sStyleString+">"+"</div>") ;
         context.httpAjaxContext.ajax_rsp_assign_grid("_"+"Gridproyecto_proyectopais", Gridproyecto_proyectopaisContainer);
         if ( ! context.isAjaxRequest( ) && ! context.isSpaRequest( ) )
         {
            GxWebStd.gx_hidden_field( context, "Gridproyecto_proyectopaisContainerData", Gridproyecto_proyectopaisContainer.ToJavascriptSource());
         }
         if ( context.isAjaxRequest( ) || context.isSpaRequest( ) )
         {
            GxWebStd.gx_hidden_field( context, "Gridproyecto_proyectopaisContainerData"+"V", Gridproyecto_proyectopaisContainer.GridValuesHidden());
         }
         else
         {
            context.WriteHtmlText( "<input type=\"hidden\" "+"name=\""+"Gridproyecto_proyectopaisContainerData"+"V"+"\" value='"+Gridproyecto_proyectopaisContainer.GridValuesHidden()+"'/>") ;
         }
      }

      protected void UserMain( )
      {
         standaloneStartup( ) ;
      }

      protected void UserMainFullajax( )
      {
         INITENV( ) ;
         INITTRN( ) ;
         UserMain( ) ;
         Draw( ) ;
         SendCloseFormHiddens( ) ;
      }

      protected void standaloneStartup( )
      {
         standaloneStartupServer( ) ;
         disable_std_buttons( ) ;
         enableDisable( ) ;
         Process( ) ;
      }

      protected void standaloneStartupServer( )
      {
         /* Execute Start event if defined. */
         context.wbGlbDoneStart = 0;
         /* Execute user event: Start */
         E110P2 ();
         context.wbGlbDoneStart = 1;
         assign_properties_default( ) ;
         if ( AnyError == 0 )
         {
            if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
            {
               /* Read saved SDTs. */
               /* Read variables values. */
               if ( ( ( context.localUtil.CToN( cgiGet( edtProgramaid_Internalname), ".", ",") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtProgramaid_Internalname), ".", ",") > Convert.ToDecimal( 999999999 )) ) )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "PROGRAMAID");
                  AnyError = 1;
                  GX_FocusControl = edtProgramaid_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  wbErr = true;
                  A2Programaid = 0;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2Programaid", StringUtil.LTrim( StringUtil.Str( (decimal)(A2Programaid), 9, 0)));
               }
               else
               {
                  A2Programaid = (int)(context.localUtil.CToN( cgiGet( edtProgramaid_Internalname), ".", ","));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2Programaid", StringUtil.LTrim( StringUtil.Str( (decimal)(A2Programaid), 9, 0)));
               }
               A72Programanombre = cgiGet( edtProgramanombre_Internalname);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A72Programanombre", A72Programanombre);
               A74Proyectonombre = cgiGet( edtProyectonombre_Internalname);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A74Proyectonombre", A74Proyectonombre);
               A75marco = cgiGet( edtmarco_Internalname);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A75marco", A75marco);
               /* Read saved values. */
               Z25Proyectoid = (int)(context.localUtil.CToN( cgiGet( "Z25Proyectoid"), ".", ","));
               Z74Proyectonombre = cgiGet( "Z74Proyectonombre");
               Z75marco = cgiGet( "Z75marco");
               Z2Programaid = (int)(context.localUtil.CToN( cgiGet( "Z2Programaid"), ".", ","));
               IsConfirmed = (short)(context.localUtil.CToN( cgiGet( "IsConfirmed"), ".", ","));
               IsModified = (short)(context.localUtil.CToN( cgiGet( "IsModified"), ".", ","));
               Gx_mode = cgiGet( "Mode");
               nRC_GXsfl_58 = (short)(context.localUtil.CToN( cgiGet( "nRC_GXsfl_58"), ".", ","));
               N2Programaid = (int)(context.localUtil.CToN( cgiGet( "N2Programaid"), ".", ","));
               AV7Proyectoid = (int)(context.localUtil.CToN( cgiGet( "vPROYECTOID"), ".", ","));
               A25Proyectoid = (int)(context.localUtil.CToN( cgiGet( "PROYECTOID"), ".", ","));
               AV11Insert_Programaid = (int)(context.localUtil.CToN( cgiGet( "vINSERT_PROGRAMAID"), ".", ","));
               AV13Pgmname = cgiGet( "vPGMNAME");
               Gx_mode = cgiGet( "vMODE");
               A119moneda = cgiGet( "MONEDA");
               /* Read subfile selected row values. */
               /* Read hidden variables. */
               GXKey = Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
               forbiddenHiddens = "hsh" + "Proyecto";
               forbiddenHiddens = forbiddenHiddens + context.localUtil.Format( (decimal)(AV11Insert_Programaid), "ZZZZZZZZ9");
               forbiddenHiddens = forbiddenHiddens + StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"));
               forbiddenHiddens = forbiddenHiddens + context.localUtil.Format( (decimal)(A25Proyectoid), "ZZZZZZZZ9");
               hsh = cgiGet( "hsh");
               if ( ! GXUtil.CheckEncryptedHash( forbiddenHiddens, hsh, GXKey) )
               {
                  GXUtil.WriteLog("proyecto:[SecurityCheckFailed value for]"+"Insert_Programaid:"+context.localUtil.Format( (decimal)(AV11Insert_Programaid), "ZZZZZZZZ9"));
                  GXUtil.WriteLog("proyecto:[SecurityCheckFailed value for]"+"Gx_mode:"+StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!")));
                  GXUtil.WriteLog("proyecto:[SecurityCheckFailed value for]"+"Proyectoid:"+context.localUtil.Format( (decimal)(A25Proyectoid), "ZZZZZZZZ9"));
                  GxWebError = 1;
                  context.HttpContext.Response.StatusDescription = 403.ToString();
                  context.HttpContext.Response.StatusCode = 403;
                  context.WriteHtmlText( "<title>403 Forbidden</title>") ;
                  context.WriteHtmlText( "<h1>403 Forbidden</h1>") ;
                  context.WriteHtmlText( "<p /><hr />") ;
                  GXUtil.WriteLog("send_http_error_code " + 403.ToString());
                  AnyError = 1;
                  return  ;
               }
               /* Check if conditions changed and reset current page numbers */
               standaloneNotModal( ) ;
            }
            else
            {
               standaloneNotModal( ) ;
               if ( StringUtil.StrCmp(gxfirstwebparm, "viewer") == 0 )
               {
                  Gx_mode = "DSP";
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!")), context));
                  A25Proyectoid = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A25Proyectoid", StringUtil.LTrim( StringUtil.Str( (decimal)(A25Proyectoid), 9, 0)));
                  getEqualNoModal( ) ;
                  Gx_mode = "DSP";
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!")), context));
                  disable_std_buttons( ) ;
                  standaloneModal( ) ;
               }
               else
               {
                  if ( StringUtil.StrCmp(Gx_mode, "DSP") == 0 )
                  {
                     sMode25 = Gx_mode;
                     Gx_mode = "UPD";
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!")), context));
                     Gx_mode = sMode25;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!")), context));
                  }
                  standaloneModal( ) ;
                  if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
                  {
                     getByPrimaryKey( ) ;
                     if ( RcdFound25 == 1 )
                     {
                        if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
                        {
                           /* Confirm record */
                           CONFIRM_0P0( ) ;
                           if ( AnyError == 0 )
                           {
                              GX_FocusControl = bttBtn_enter_Internalname;
                              context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                           }
                        }
                     }
                     else
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_noinsert", ""), 1, "");
                        AnyError = 1;
                     }
                  }
               }
            }
         }
      }

      protected void Process( )
      {
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            /* Read Transaction buttons. */
            sEvt = cgiGet( "_EventName");
            EvtGridId = cgiGet( "_EventGridId");
            EvtRowId = cgiGet( "_EventRowId");
            if ( StringUtil.Len( sEvt) > 0 )
            {
               sEvtType = StringUtil.Left( sEvt, 1);
               sEvt = StringUtil.Right( sEvt, (short)(StringUtil.Len( sEvt)-1));
               if ( StringUtil.StrCmp(sEvtType, "M") != 0 )
               {
                  if ( StringUtil.StrCmp(sEvtType, "E") == 0 )
                  {
                     sEvtType = StringUtil.Right( sEvt, 1);
                     if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                     {
                        sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                        if ( StringUtil.StrCmp(sEvt, "START") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: Start */
                           E110P2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "AFTER TRN") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: After Trn */
                           E120P2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "ENTER") == 0 )
                        {
                           context.wbHandled = 1;
                           if ( StringUtil.StrCmp(Gx_mode, "DSP") != 0 )
                           {
                              btn_enter( ) ;
                           }
                           /* No code required for Cancel button. It is implemented as the Reset button. */
                        }
                     }
                     else
                     {
                        sEvtType = StringUtil.Right( sEvt, 4);
                        sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-4));
                     }
                  }
                  context.wbHandled = 1;
               }
            }
         }
      }

      protected void AfterTrn( )
      {
         if ( trnEnded == 1 )
         {
            /* Execute user event: After Trn */
            E120P2 ();
            trnEnded = 0;
            standaloneNotModal( ) ;
            standaloneModal( ) ;
            if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  )
            {
               /* Clear variables for new insertion. */
               InitAll0P25( ) ;
               standaloneNotModal( ) ;
               standaloneModal( ) ;
            }
         }
      }

      public override String ToString( )
      {
         return "" ;
      }

      public GxContentInfo GetContentInfo( )
      {
         return (GxContentInfo)(null) ;
      }

      protected void disable_std_buttons( )
      {
         bttBtn_delete_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_delete_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_delete_Visible), 5, 0)), true);
         bttBtn_first_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_first_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_first_Visible), 5, 0)), true);
         bttBtn_previous_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_previous_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_previous_Visible), 5, 0)), true);
         bttBtn_next_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_next_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_next_Visible), 5, 0)), true);
         bttBtn_last_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_last_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_last_Visible), 5, 0)), true);
         bttBtn_select_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_select_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_select_Visible), 5, 0)), true);
         if ( ( StringUtil.StrCmp(Gx_mode, "DSP") == 0 ) || ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 ) )
         {
            bttBtn_delete_Visible = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_delete_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_delete_Visible), 5, 0)), true);
            if ( StringUtil.StrCmp(Gx_mode, "DSP") == 0 )
            {
               bttBtn_enter_Visible = 0;
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_enter_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_enter_Visible), 5, 0)), true);
            }
            DisableAttributes0P25( ) ;
         }
      }

      protected void set_caption( )
      {
         if ( ( IsConfirmed == 1 ) && ( AnyError == 0 ) )
         {
            if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_confdelete", ""), 0, "", true);
            }
            else
            {
               GX_msglist.addItem(context.GetMessage( "GXM_mustconfirm", ""), 0, "", true);
            }
         }
      }

      protected void CONFIRM_0P0( )
      {
         BeforeValidate0P25( ) ;
         if ( AnyError == 0 )
         {
            if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
            {
               OnDeleteControls0P25( ) ;
            }
            else
            {
               CheckExtendedTable0P25( ) ;
               CloseExtendedTableCursors0P25( ) ;
            }
         }
         if ( AnyError == 0 )
         {
            /* Save parent mode. */
            sMode25 = Gx_mode;
            CONFIRM_0P26( ) ;
            if ( AnyError == 0 )
            {
               /* Restore parent mode. */
               Gx_mode = sMode25;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!")), context));
               IsConfirmed = 1;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "IsConfirmed", StringUtil.LTrim( StringUtil.Str( (decimal)(IsConfirmed), 4, 0)));
            }
            /* Restore parent mode. */
            Gx_mode = sMode25;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!")), context));
         }
      }

      protected void CONFIRM_0P26( )
      {
         nGXsfl_58_idx = 0;
         while ( nGXsfl_58_idx < nRC_GXsfl_58 )
         {
            ReadRow0P26( ) ;
            if ( ( nRcdExists_26 != 0 ) || ( nIsMod_26 != 0 ) )
            {
               GetKey0P26( ) ;
               if ( ( nRcdExists_26 == 0 ) && ( nRcdDeleted_26 == 0 ) )
               {
                  if ( RcdFound26 == 0 )
                  {
                     Gx_mode = "INS";
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!")), context));
                     BeforeValidate0P26( ) ;
                     if ( AnyError == 0 )
                     {
                        CheckExtendedTable0P26( ) ;
                        CloseExtendedTableCursors0P26( ) ;
                        if ( AnyError == 0 )
                        {
                           IsConfirmed = 1;
                           context.httpAjaxContext.ajax_rsp_assign_attri("", false, "IsConfirmed", StringUtil.LTrim( StringUtil.Str( (decimal)(IsConfirmed), 4, 0)));
                        }
                     }
                  }
                  else
                  {
                     GXCCtl = "PROYECTOPAISID_" + sGXsfl_58_idx;
                     GX_msglist.addItem(context.GetMessage( "GXM_noupdate", ""), "DuplicatePrimaryKey", 1, GXCCtl);
                     AnyError = 1;
                     GX_FocusControl = edtProyectoPaisid_Internalname;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  }
               }
               else
               {
                  if ( RcdFound26 != 0 )
                  {
                     if ( nRcdDeleted_26 != 0 )
                     {
                        Gx_mode = "DLT";
                        context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                        context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!")), context));
                        getByPrimaryKey0P26( ) ;
                        Load0P26( ) ;
                        BeforeValidate0P26( ) ;
                        if ( AnyError == 0 )
                        {
                           OnDeleteControls0P26( ) ;
                        }
                     }
                     else
                     {
                        if ( nIsMod_26 != 0 )
                        {
                           Gx_mode = "UPD";
                           context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                           context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!")), context));
                           BeforeValidate0P26( ) ;
                           if ( AnyError == 0 )
                           {
                              CheckExtendedTable0P26( ) ;
                              CloseExtendedTableCursors0P26( ) ;
                              if ( AnyError == 0 )
                              {
                                 IsConfirmed = 1;
                                 context.httpAjaxContext.ajax_rsp_assign_attri("", false, "IsConfirmed", StringUtil.LTrim( StringUtil.Str( (decimal)(IsConfirmed), 4, 0)));
                              }
                           }
                        }
                     }
                  }
                  else
                  {
                     if ( nRcdDeleted_26 == 0 )
                     {
                        GXCCtl = "PROYECTOPAISID_" + sGXsfl_58_idx;
                        GX_msglist.addItem(context.GetMessage( "GXM_recdeleted", ""), 1, GXCCtl);
                        AnyError = 1;
                        GX_FocusControl = edtProyectoPaisid_Internalname;
                        context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                     }
                  }
               }
            }
            ChangePostValue( edtProyectoPaisid_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A132ProyectoPaisid), 4, 0, ".", ""))) ;
            ChangePostValue( edtPaisid_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A15Paisid), 9, 0, ".", ""))) ;
            ChangePostValue( edtcodigo_Internalname, A117codigo) ;
            ChangePostValue( edtPaisnombre_Internalname, A118Paisnombre) ;
            ChangePostValue( "ZT_"+"Z132ProyectoPaisid_"+sGXsfl_58_idx, StringUtil.LTrim( StringUtil.NToC( (decimal)(Z132ProyectoPaisid), 4, 0, ".", ""))) ;
            ChangePostValue( "ZT_"+"Z15Paisid_"+sGXsfl_58_idx, StringUtil.LTrim( StringUtil.NToC( (decimal)(Z15Paisid), 9, 0, ".", ""))) ;
            ChangePostValue( "nRcdDeleted_26_"+sGXsfl_58_idx, StringUtil.LTrim( StringUtil.NToC( (decimal)(nRcdDeleted_26), 4, 0, ".", ""))) ;
            ChangePostValue( "nRcdExists_26_"+sGXsfl_58_idx, StringUtil.LTrim( StringUtil.NToC( (decimal)(nRcdExists_26), 4, 0, ".", ""))) ;
            ChangePostValue( "nIsMod_26_"+sGXsfl_58_idx, StringUtil.LTrim( StringUtil.NToC( (decimal)(nIsMod_26), 4, 0, ".", ""))) ;
            if ( nIsMod_26 != 0 )
            {
               ChangePostValue( "PROYECTOPAISID_"+sGXsfl_58_idx+"Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtProyectoPaisid_Enabled), 5, 0, ".", ""))) ;
               ChangePostValue( "PAISID_"+sGXsfl_58_idx+"Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtPaisid_Enabled), 5, 0, ".", ""))) ;
               ChangePostValue( "CODIGO_"+sGXsfl_58_idx+"Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtcodigo_Enabled), 5, 0, ".", ""))) ;
               ChangePostValue( "PAISNOMBRE_"+sGXsfl_58_idx+"Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtPaisnombre_Enabled), 5, 0, ".", ""))) ;
            }
         }
         /* Start of After( level) rules */
         /* End of After( level) rules */
      }

      protected void ResetCaption0P0( )
      {
      }

      protected void E110P2( )
      {
         /* Start Routine */
         if ( ! new isauthorized(context).executeUdp(  AV13Pgmname) )
         {
            CallWebObject(formatLink("notauthorized.aspx") + "?" + UrlEncode(StringUtil.RTrim(AV13Pgmname)));
            context.wjLocDisableFrm = 1;
         }
         AV9TrnContext.FromXml(AV10WebSession.Get("TrnContext"), null, "TransactionContext", "SAPWEB08");
         AV11Insert_Programaid = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV11Insert_Programaid", StringUtil.LTrim( StringUtil.Str( (decimal)(AV11Insert_Programaid), 9, 0)));
         if ( ( StringUtil.StrCmp(AV9TrnContext.gxTpr_Transactionname, AV13Pgmname) == 0 ) && ( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) )
         {
            AV14GXV1 = 1;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14GXV1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV14GXV1), 8, 0)));
            while ( AV14GXV1 <= AV9TrnContext.gxTpr_Attributes.Count )
            {
               AV12TrnContextAtt = ((SdtTransactionContext_Attribute)AV9TrnContext.gxTpr_Attributes.Item(AV14GXV1));
               if ( StringUtil.StrCmp(AV12TrnContextAtt.gxTpr_Attributename, "Programaid") == 0 )
               {
                  AV11Insert_Programaid = (int)(NumberUtil.Val( AV12TrnContextAtt.gxTpr_Attributevalue, "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV11Insert_Programaid", StringUtil.LTrim( StringUtil.Str( (decimal)(AV11Insert_Programaid), 9, 0)));
               }
               AV14GXV1 = (int)(AV14GXV1+1);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14GXV1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV14GXV1), 8, 0)));
            }
         }
      }

      protected void E120P2( )
      {
         /* After Trn Routine */
         if ( ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 ) && ! AV9TrnContext.gxTpr_Callerondelete )
         {
            CallWebObject(formatLink("wwproyecto.aspx") );
            context.wjLocDisableFrm = 1;
         }
         context.setWebReturnParms(new Object[] {});
         context.setWebReturnParmsMetadata(new Object[] {});
         context.wjLocDisableFrm = 1;
         context.nUserReturn = 1;
         returnInSub = true;
         if (true) return;
      }

      protected void ZM0P25( short GX_JID )
      {
         if ( ( GX_JID == 7 ) || ( GX_JID == 0 ) )
         {
            if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
            {
               Z74Proyectonombre = T000P6_A74Proyectonombre[0];
               Z75marco = T000P6_A75marco[0];
               Z2Programaid = T000P6_A2Programaid[0];
            }
            else
            {
               Z74Proyectonombre = A74Proyectonombre;
               Z75marco = A75marco;
               Z2Programaid = A2Programaid;
            }
         }
         if ( GX_JID == -7 )
         {
            Z25Proyectoid = A25Proyectoid;
            Z74Proyectonombre = A74Proyectonombre;
            Z75marco = A75marco;
            Z2Programaid = A2Programaid;
            Z72Programanombre = A72Programanombre;
         }
      }

      protected void standaloneNotModal( )
      {
         imgprompt_2_Link = ((StringUtil.StrCmp(Gx_mode, "DSP")==0) ? "" : "javascript:"+"gx.popup.openPrompt('"+"gx0050.aspx"+"',["+"{Ctrl:gx.dom.el('"+"PROGRAMAID"+"'), id:'"+"PROGRAMAID"+"'"+",IOType:'out'}"+"],"+"null"+","+"'', false"+","+"false"+");");
         bttBtn_delete_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_delete_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_delete_Enabled), 5, 0)), true);
         if ( ! (0==AV7Proyectoid) )
         {
            A25Proyectoid = AV7Proyectoid;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A25Proyectoid", StringUtil.LTrim( StringUtil.Str( (decimal)(A25Proyectoid), 9, 0)));
         }
         if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) && ! (0==AV11Insert_Programaid) )
         {
            edtProgramaid_Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtProgramaid_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtProgramaid_Enabled), 5, 0)), true);
         }
         else
         {
            edtProgramaid_Enabled = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtProgramaid_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtProgramaid_Enabled), 5, 0)), true);
         }
      }

      protected void standaloneModal( )
      {
         if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) && ! (0==AV11Insert_Programaid) )
         {
            A2Programaid = AV11Insert_Programaid;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2Programaid", StringUtil.LTrim( StringUtil.Str( (decimal)(A2Programaid), 9, 0)));
         }
         if ( StringUtil.StrCmp(Gx_mode, "DSP") == 0 )
         {
            bttBtn_enter_Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_enter_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_enter_Enabled), 5, 0)), true);
         }
         else
         {
            bttBtn_enter_Enabled = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_enter_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_enter_Enabled), 5, 0)), true);
         }
         if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) && ( Gx_BScreen == 0 ) )
         {
            AV13Pgmname = "Proyecto";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13Pgmname", AV13Pgmname);
            /* Using cursor T000P7 */
            pr_datastore1.execute(3, new Object[] {A2Programaid});
            A72Programanombre = T000P7_A72Programanombre[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A72Programanombre", A72Programanombre);
            pr_datastore1.close(3);
         }
      }

      protected void Load0P25( )
      {
         /* Using cursor T000P8 */
         pr_datastore1.execute(4, new Object[] {A25Proyectoid});
         if ( (pr_datastore1.getStatus(4) != 101) )
         {
            RcdFound25 = 1;
            A72Programanombre = T000P8_A72Programanombre[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A72Programanombre", A72Programanombre);
            A74Proyectonombre = T000P8_A74Proyectonombre[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A74Proyectonombre", A74Proyectonombre);
            A75marco = T000P8_A75marco[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A75marco", A75marco);
            A2Programaid = T000P8_A2Programaid[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2Programaid", StringUtil.LTrim( StringUtil.Str( (decimal)(A2Programaid), 9, 0)));
            ZM0P25( -7) ;
         }
         pr_datastore1.close(4);
         OnLoadActions0P25( ) ;
      }

      protected void OnLoadActions0P25( )
      {
         AV13Pgmname = "Proyecto";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13Pgmname", AV13Pgmname);
      }

      protected void CheckExtendedTable0P25( )
      {
         Gx_BScreen = 1;
         standaloneModal( ) ;
         AV13Pgmname = "Proyecto";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13Pgmname", AV13Pgmname);
         /* Using cursor T000P7 */
         pr_datastore1.execute(3, new Object[] {A2Programaid});
         if ( (pr_datastore1.getStatus(3) == 101) )
         {
            GX_msglist.addItem("No matching 'Programa'.", "ForeignKeyNotFound", 1, "PROGRAMAID");
            AnyError = 1;
            GX_FocusControl = edtProgramaid_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         A72Programanombre = T000P7_A72Programanombre[0];
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A72Programanombre", A72Programanombre);
         pr_datastore1.close(3);
      }

      protected void CloseExtendedTableCursors0P25( )
      {
         pr_datastore1.close(3);
      }

      protected void enableDisable( )
      {
      }

      protected void gxLoad_8( int A2Programaid )
      {
         /* Using cursor T000P9 */
         pr_datastore1.execute(5, new Object[] {A2Programaid});
         if ( (pr_datastore1.getStatus(5) == 101) )
         {
            GX_msglist.addItem("No matching 'Programa'.", "ForeignKeyNotFound", 1, "PROGRAMAID");
            AnyError = 1;
            GX_FocusControl = edtProgramaid_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         A72Programanombre = T000P9_A72Programanombre[0];
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A72Programanombre", A72Programanombre);
         GxWebStd.set_html_headers( context, 0, "", "");
         context.GX_webresponse.AddString("[[");
         context.GX_webresponse.AddString("\""+GXUtil.EncodeJSConstant( A72Programanombre)+"\"");
         context.GX_webresponse.AddString("]");
         if ( (pr_datastore1.getStatus(5) == 101) )
         {
            context.GX_webresponse.AddString(",");
            context.GX_webresponse.AddString("101");
         }
         context.GX_webresponse.AddString("]");
         pr_datastore1.close(5);
      }

      protected void GetKey0P25( )
      {
         /* Using cursor T000P10 */
         pr_datastore1.execute(6, new Object[] {A25Proyectoid});
         if ( (pr_datastore1.getStatus(6) != 101) )
         {
            RcdFound25 = 1;
         }
         else
         {
            RcdFound25 = 0;
         }
         pr_datastore1.close(6);
      }

      protected void getByPrimaryKey( )
      {
         /* Using cursor T000P6 */
         pr_datastore1.execute(2, new Object[] {A25Proyectoid});
         if ( (pr_datastore1.getStatus(2) != 101) )
         {
            ZM0P25( 7) ;
            RcdFound25 = 1;
            A25Proyectoid = T000P6_A25Proyectoid[0];
            A74Proyectonombre = T000P6_A74Proyectonombre[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A74Proyectonombre", A74Proyectonombre);
            A75marco = T000P6_A75marco[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A75marco", A75marco);
            A2Programaid = T000P6_A2Programaid[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2Programaid", StringUtil.LTrim( StringUtil.Str( (decimal)(A2Programaid), 9, 0)));
            Z25Proyectoid = A25Proyectoid;
            sMode25 = Gx_mode;
            Gx_mode = "DSP";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!")), context));
            Load0P25( ) ;
            if ( AnyError == 1 )
            {
               RcdFound25 = 0;
               InitializeNonKey0P25( ) ;
            }
            Gx_mode = sMode25;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!")), context));
         }
         else
         {
            RcdFound25 = 0;
            InitializeNonKey0P25( ) ;
            sMode25 = Gx_mode;
            Gx_mode = "DSP";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!")), context));
            standaloneModal( ) ;
            Gx_mode = sMode25;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!")), context));
         }
         pr_datastore1.close(2);
      }

      protected void getEqualNoModal( )
      {
         GetKey0P25( ) ;
         if ( RcdFound25 == 0 )
         {
         }
         else
         {
         }
         getByPrimaryKey( ) ;
      }

      protected void move_next( )
      {
         RcdFound25 = 0;
         /* Using cursor T000P11 */
         pr_datastore1.execute(7, new Object[] {A25Proyectoid});
         if ( (pr_datastore1.getStatus(7) != 101) )
         {
            while ( (pr_datastore1.getStatus(7) != 101) && ( ( T000P11_A25Proyectoid[0] < A25Proyectoid ) ) )
            {
               pr_datastore1.readNext(7);
            }
            if ( (pr_datastore1.getStatus(7) != 101) && ( ( T000P11_A25Proyectoid[0] > A25Proyectoid ) ) )
            {
               A25Proyectoid = T000P11_A25Proyectoid[0];
               RcdFound25 = 1;
            }
         }
         pr_datastore1.close(7);
      }

      protected void move_previous( )
      {
         RcdFound25 = 0;
         /* Using cursor T000P12 */
         pr_datastore1.execute(8, new Object[] {A25Proyectoid});
         if ( (pr_datastore1.getStatus(8) != 101) )
         {
            while ( (pr_datastore1.getStatus(8) != 101) && ( ( T000P12_A25Proyectoid[0] > A25Proyectoid ) ) )
            {
               pr_datastore1.readNext(8);
            }
            if ( (pr_datastore1.getStatus(8) != 101) && ( ( T000P12_A25Proyectoid[0] < A25Proyectoid ) ) )
            {
               A25Proyectoid = T000P12_A25Proyectoid[0];
               RcdFound25 = 1;
            }
         }
         pr_datastore1.close(8);
      }

      protected void btn_enter( )
      {
         nKeyPressed = 1;
         GetKey0P25( ) ;
         if ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )
         {
            /* Insert record */
            GX_FocusControl = edtProgramaid_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            Insert0P25( ) ;
            if ( AnyError == 1 )
            {
               GX_FocusControl = "";
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            }
         }
         else
         {
            if ( RcdFound25 == 1 )
            {
               if ( A25Proyectoid != Z25Proyectoid )
               {
                  A25Proyectoid = Z25Proyectoid;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A25Proyectoid", StringUtil.LTrim( StringUtil.Str( (decimal)(A25Proyectoid), 9, 0)));
                  GX_msglist.addItem(context.GetMessage( "GXM_getbeforeupd", ""), "CandidateKeyNotFound", 1, "");
                  AnyError = 1;
               }
               else if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
               {
                  delete( ) ;
                  AfterTrn( ) ;
                  GX_FocusControl = edtProgramaid_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               }
               else
               {
                  /* Update record */
                  Update0P25( ) ;
                  GX_FocusControl = edtProgramaid_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               }
            }
            else
            {
               if ( A25Proyectoid != Z25Proyectoid )
               {
                  /* Insert record */
                  GX_FocusControl = edtProgramaid_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  Insert0P25( ) ;
                  if ( AnyError == 1 )
                  {
                     GX_FocusControl = "";
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  }
               }
               else
               {
                  if ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 )
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_recdeleted", ""), 1, "");
                     AnyError = 1;
                  }
                  else
                  {
                     /* Insert record */
                     GX_FocusControl = edtProgramaid_Internalname;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                     Insert0P25( ) ;
                     if ( AnyError == 1 )
                     {
                        GX_FocusControl = "";
                        context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                     }
                  }
               }
            }
         }
         AfterTrn( ) ;
         if ( ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 ) || ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 ) )
         {
            if ( AnyError == 0 )
            {
               context.nUserReturn = 1;
            }
         }
      }

      protected void btn_delete( )
      {
         if ( A25Proyectoid != Z25Proyectoid )
         {
            A25Proyectoid = Z25Proyectoid;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A25Proyectoid", StringUtil.LTrim( StringUtil.Str( (decimal)(A25Proyectoid), 9, 0)));
            GX_msglist.addItem(context.GetMessage( "GXM_getbeforedlt", ""), 1, "");
            AnyError = 1;
         }
         else
         {
            delete( ) ;
            AfterTrn( ) ;
            GX_FocusControl = edtProgramaid_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         if ( AnyError != 0 )
         {
         }
      }

      protected void CheckOptimisticConcurrency0P25( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
         {
            /* Using cursor T000P5 */
            pr_datastore1.execute(1, new Object[] {A25Proyectoid});
            if ( (pr_datastore1.getStatus(1) == 103) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_lock", new   object[]  {"PROYECTO"}), "RecordIsLocked", 1, "");
               AnyError = 1;
               return  ;
            }
            if ( (pr_datastore1.getStatus(1) == 101) || ( StringUtil.StrCmp(Z74Proyectonombre, T000P5_A74Proyectonombre[0]) != 0 ) || ( StringUtil.StrCmp(Z75marco, T000P5_A75marco[0]) != 0 ) || ( Z2Programaid != T000P5_A2Programaid[0] ) )
            {
               if ( StringUtil.StrCmp(Z74Proyectonombre, T000P5_A74Proyectonombre[0]) != 0 )
               {
                  GXUtil.WriteLog("proyecto:[seudo value changed for attri]"+"Proyectonombre");
                  GXUtil.WriteLogRaw("Old: ",Z74Proyectonombre);
                  GXUtil.WriteLogRaw("Current: ",T000P5_A74Proyectonombre[0]);
               }
               if ( StringUtil.StrCmp(Z75marco, T000P5_A75marco[0]) != 0 )
               {
                  GXUtil.WriteLog("proyecto:[seudo value changed for attri]"+"marco");
                  GXUtil.WriteLogRaw("Old: ",Z75marco);
                  GXUtil.WriteLogRaw("Current: ",T000P5_A75marco[0]);
               }
               if ( Z2Programaid != T000P5_A2Programaid[0] )
               {
                  GXUtil.WriteLog("proyecto:[seudo value changed for attri]"+"Programaid");
                  GXUtil.WriteLogRaw("Old: ",Z2Programaid);
                  GXUtil.WriteLogRaw("Current: ",T000P5_A2Programaid[0]);
               }
               GX_msglist.addItem(context.GetMessage( "GXM_waschg", new   object[]  {"PROYECTO"}), "RecordWasChanged", 1, "");
               AnyError = 1;
               return  ;
            }
         }
      }

      protected void Insert0P25( )
      {
         BeforeValidate0P25( ) ;
         if ( AnyError == 0 )
         {
            CheckExtendedTable0P25( ) ;
         }
         if ( AnyError == 0 )
         {
            ZM0P25( 0) ;
            CheckOptimisticConcurrency0P25( ) ;
            if ( AnyError == 0 )
            {
               AfterConfirm0P25( ) ;
               if ( AnyError == 0 )
               {
                  BeforeInsert0P25( ) ;
                  if ( AnyError == 0 )
                  {
                     /* Using cursor T000P13 */
                     pr_datastore1.execute(9, new Object[] {A74Proyectonombre, A75marco, A2Programaid});
                     A25Proyectoid = T000P13_A25Proyectoid[0];
                     pr_datastore1.close(9);
                     dsDataStore1.SmartCacheProvider.SetUpdated("PROYECTO") ;
                     if ( AnyError == 0 )
                     {
                        /* Start of After( Insert) rules */
                        /* End of After( Insert) rules */
                        if ( AnyError == 0 )
                        {
                           ProcessLevel0P25( ) ;
                           if ( AnyError == 0 )
                           {
                              /* Save values for previous() function. */
                              GX_msglist.addItem(context.GetMessage( "GXM_sucadded", ""), "SuccessfullyAdded", 0, "", true);
                              ResetCaption0P0( ) ;
                           }
                        }
                     }
                  }
                  else
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                     AnyError = 1;
                  }
               }
            }
            else
            {
               Load0P25( ) ;
            }
            EndLevel0P25( ) ;
         }
         CloseExtendedTableCursors0P25( ) ;
      }

      protected void Update0P25( )
      {
         BeforeValidate0P25( ) ;
         if ( AnyError == 0 )
         {
            CheckExtendedTable0P25( ) ;
         }
         if ( AnyError == 0 )
         {
            CheckOptimisticConcurrency0P25( ) ;
            if ( AnyError == 0 )
            {
               AfterConfirm0P25( ) ;
               if ( AnyError == 0 )
               {
                  BeforeUpdate0P25( ) ;
                  if ( AnyError == 0 )
                  {
                     /* Using cursor T000P14 */
                     pr_datastore1.execute(10, new Object[] {A74Proyectonombre, A75marco, A2Programaid, A25Proyectoid});
                     pr_datastore1.close(10);
                     dsDataStore1.SmartCacheProvider.SetUpdated("PROYECTO") ;
                     if ( (pr_datastore1.getStatus(10) == 103) )
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_lock", new   object[]  {"PROYECTO"}), "RecordIsLocked", 1, "");
                        AnyError = 1;
                     }
                     DeferredUpdate0P25( ) ;
                     if ( AnyError == 0 )
                     {
                        /* Start of After( update) rules */
                        /* End of After( update) rules */
                        if ( AnyError == 0 )
                        {
                           ProcessLevel0P25( ) ;
                           if ( AnyError == 0 )
                           {
                              if ( ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 ) || ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 ) )
                              {
                                 if ( AnyError == 0 )
                                 {
                                    context.nUserReturn = 1;
                                 }
                              }
                           }
                        }
                     }
                     else
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                        AnyError = 1;
                     }
                  }
               }
            }
            EndLevel0P25( ) ;
         }
         CloseExtendedTableCursors0P25( ) ;
      }

      protected void DeferredUpdate0P25( )
      {
      }

      protected void delete( )
      {
         BeforeValidate0P25( ) ;
         if ( AnyError == 0 )
         {
            CheckOptimisticConcurrency0P25( ) ;
         }
         if ( AnyError == 0 )
         {
            OnDeleteControls0P25( ) ;
            AfterConfirm0P25( ) ;
            if ( AnyError == 0 )
            {
               BeforeDelete0P25( ) ;
               if ( AnyError == 0 )
               {
                  ScanStart0P26( ) ;
                  while ( RcdFound26 != 0 )
                  {
                     getByPrimaryKey0P26( ) ;
                     Delete0P26( ) ;
                     ScanNext0P26( ) ;
                  }
                  ScanEnd0P26( ) ;
                  if ( AnyError == 0 )
                  {
                     /* Using cursor T000P15 */
                     pr_datastore1.execute(11, new Object[] {A25Proyectoid});
                     pr_datastore1.close(11);
                     dsDataStore1.SmartCacheProvider.SetUpdated("PROYECTO") ;
                     if ( AnyError == 0 )
                     {
                        /* Start of After( delete) rules */
                        /* End of After( delete) rules */
                        if ( AnyError == 0 )
                        {
                           if ( ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 ) || ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 ) )
                           {
                              if ( AnyError == 0 )
                              {
                                 context.nUserReturn = 1;
                              }
                           }
                        }
                     }
                     else
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                        AnyError = 1;
                     }
                  }
               }
            }
         }
         sMode25 = Gx_mode;
         Gx_mode = "DLT";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!")), context));
         EndLevel0P25( ) ;
         Gx_mode = sMode25;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!")), context));
      }

      protected void OnDeleteControls0P25( )
      {
         standaloneModal( ) ;
         if ( AnyError == 0 )
         {
            /* Delete mode formulas */
            AV13Pgmname = "Proyecto";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13Pgmname", AV13Pgmname);
            /* Using cursor T000P16 */
            pr_datastore1.execute(12, new Object[] {A2Programaid});
            A72Programanombre = T000P16_A72Programanombre[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A72Programanombre", A72Programanombre);
            pr_datastore1.close(12);
         }
         if ( AnyError == 0 )
         {
            /* Using cursor T000P17 */
            pr_datastore1.execute(13, new Object[] {A25Proyectoid});
            if ( (pr_datastore1.getStatus(13) != 101) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_del", new   object[]  {"Plan"}), "CannotDeleteReferencedRecord", 1, "");
               AnyError = 1;
            }
            pr_datastore1.close(13);
            /* Using cursor T000P18 */
            pr_datastore1.execute(14, new Object[] {A25Proyectoid});
            if ( (pr_datastore1.getStatus(14) != 101) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_del", new   object[]  {"Proyecto_Comunidad"}), "CannotDeleteReferencedRecord", 1, "");
               AnyError = 1;
            }
            pr_datastore1.close(14);
            /* Using cursor T000P19 */
            pr_datastore1.execute(15, new Object[] {A25Proyectoid});
            if ( (pr_datastore1.getStatus(15) != 101) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_del", new   object[]  {"Indicador"}), "CannotDeleteReferencedRecord", 1, "");
               AnyError = 1;
            }
            pr_datastore1.close(15);
         }
      }

      protected void ProcessNestedLevel0P26( )
      {
         nGXsfl_58_idx = 0;
         while ( nGXsfl_58_idx < nRC_GXsfl_58 )
         {
            ReadRow0P26( ) ;
            if ( ( nRcdExists_26 != 0 ) || ( nIsMod_26 != 0 ) )
            {
               standaloneNotModal0P26( ) ;
               GetKey0P26( ) ;
               if ( ( nRcdExists_26 == 0 ) && ( nRcdDeleted_26 == 0 ) )
               {
                  Gx_mode = "INS";
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!")), context));
                  Insert0P26( ) ;
               }
               else
               {
                  if ( RcdFound26 != 0 )
                  {
                     if ( ( nRcdDeleted_26 != 0 ) && ( nRcdExists_26 != 0 ) )
                     {
                        Gx_mode = "DLT";
                        context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                        context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!")), context));
                        Delete0P26( ) ;
                     }
                     else
                     {
                        if ( ( nIsMod_26 != 0 ) && ( nRcdExists_26 != 0 ) )
                        {
                           Gx_mode = "UPD";
                           context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                           context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!")), context));
                           Update0P26( ) ;
                        }
                     }
                  }
                  else
                  {
                     if ( nRcdDeleted_26 == 0 )
                     {
                        GXCCtl = "PROYECTOPAISID_" + sGXsfl_58_idx;
                        GX_msglist.addItem(context.GetMessage( "GXM_recdeleted", ""), 1, GXCCtl);
                        AnyError = 1;
                        GX_FocusControl = edtProyectoPaisid_Internalname;
                        context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                     }
                  }
               }
            }
            ChangePostValue( edtProyectoPaisid_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A132ProyectoPaisid), 4, 0, ".", ""))) ;
            ChangePostValue( edtPaisid_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A15Paisid), 9, 0, ".", ""))) ;
            ChangePostValue( edtcodigo_Internalname, A117codigo) ;
            ChangePostValue( edtPaisnombre_Internalname, A118Paisnombre) ;
            ChangePostValue( "ZT_"+"Z132ProyectoPaisid_"+sGXsfl_58_idx, StringUtil.LTrim( StringUtil.NToC( (decimal)(Z132ProyectoPaisid), 4, 0, ".", ""))) ;
            ChangePostValue( "ZT_"+"Z15Paisid_"+sGXsfl_58_idx, StringUtil.LTrim( StringUtil.NToC( (decimal)(Z15Paisid), 9, 0, ".", ""))) ;
            ChangePostValue( "nRcdDeleted_26_"+sGXsfl_58_idx, StringUtil.LTrim( StringUtil.NToC( (decimal)(nRcdDeleted_26), 4, 0, ".", ""))) ;
            ChangePostValue( "nRcdExists_26_"+sGXsfl_58_idx, StringUtil.LTrim( StringUtil.NToC( (decimal)(nRcdExists_26), 4, 0, ".", ""))) ;
            ChangePostValue( "nIsMod_26_"+sGXsfl_58_idx, StringUtil.LTrim( StringUtil.NToC( (decimal)(nIsMod_26), 4, 0, ".", ""))) ;
            if ( nIsMod_26 != 0 )
            {
               ChangePostValue( "PROYECTOPAISID_"+sGXsfl_58_idx+"Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtProyectoPaisid_Enabled), 5, 0, ".", ""))) ;
               ChangePostValue( "PAISID_"+sGXsfl_58_idx+"Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtPaisid_Enabled), 5, 0, ".", ""))) ;
               ChangePostValue( "CODIGO_"+sGXsfl_58_idx+"Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtcodigo_Enabled), 5, 0, ".", ""))) ;
               ChangePostValue( "PAISNOMBRE_"+sGXsfl_58_idx+"Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtPaisnombre_Enabled), 5, 0, ".", ""))) ;
            }
         }
         /* Start of After( level) rules */
         /* End of After( level) rules */
         InitAll0P26( ) ;
         if ( AnyError != 0 )
         {
         }
         nRcdExists_26 = 0;
         nIsMod_26 = 0;
         nRcdDeleted_26 = 0;
      }

      protected void ProcessLevel0P25( )
      {
         /* Save parent mode. */
         sMode25 = Gx_mode;
         ProcessNestedLevel0P26( ) ;
         if ( AnyError != 0 )
         {
         }
         /* Restore parent mode. */
         Gx_mode = sMode25;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!")), context));
         /* ' Update level parameters */
      }

      protected void EndLevel0P25( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
         {
            pr_datastore1.close(1);
         }
         if ( AnyError == 0 )
         {
            BeforeComplete0P25( ) ;
         }
         if ( AnyError == 0 )
         {
            pr_datastore1.close(2);
            pr_default.close(1);
            pr_default.close(0);
            pr_datastore1.close(12);
            pr_datastore1.close(0);
            context.CommitDataStores("proyecto",pr_default);
            if ( AnyError == 0 )
            {
               ConfirmValues0P0( ) ;
            }
            /* After transaction rules */
            /* Execute 'After Trn' event if defined. */
            trnEnded = 1;
         }
         else
         {
            pr_datastore1.close(2);
            pr_default.close(1);
            pr_default.close(0);
            pr_datastore1.close(12);
            pr_datastore1.close(0);
            context.RollbackDataStores("proyecto",pr_default);
         }
         IsModified = 0;
         if ( AnyError != 0 )
         {
            context.wjLoc = "";
            context.nUserReturn = 0;
         }
      }

      public void ScanStart0P25( )
      {
         /* Scan By routine */
         /* Using cursor T000P20 */
         pr_datastore1.execute(16);
         RcdFound25 = 0;
         if ( (pr_datastore1.getStatus(16) != 101) )
         {
            RcdFound25 = 1;
            A25Proyectoid = T000P20_A25Proyectoid[0];
         }
         /* Load Subordinate Levels */
      }

      protected void ScanNext0P25( )
      {
         /* Scan next routine */
         pr_datastore1.readNext(16);
         RcdFound25 = 0;
         if ( (pr_datastore1.getStatus(16) != 101) )
         {
            RcdFound25 = 1;
            A25Proyectoid = T000P20_A25Proyectoid[0];
         }
      }

      protected void ScanEnd0P25( )
      {
         pr_datastore1.close(16);
      }

      protected void AfterConfirm0P25( )
      {
         /* After Confirm Rules */
      }

      protected void BeforeInsert0P25( )
      {
         /* Before Insert Rules */
      }

      protected void BeforeUpdate0P25( )
      {
         /* Before Update Rules */
      }

      protected void BeforeDelete0P25( )
      {
         /* Before Delete Rules */
      }

      protected void BeforeComplete0P25( )
      {
         /* Before Complete Rules */
      }

      protected void BeforeValidate0P25( )
      {
         /* Before Validate Rules */
      }

      protected void DisableAttributes0P25( )
      {
         edtProgramaid_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtProgramaid_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtProgramaid_Enabled), 5, 0)), true);
         edtProgramanombre_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtProgramanombre_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtProgramanombre_Enabled), 5, 0)), true);
         edtProyectonombre_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtProyectonombre_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtProyectonombre_Enabled), 5, 0)), true);
         edtmarco_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtmarco_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtmarco_Enabled), 5, 0)), true);
      }

      protected void ZM0P26( short GX_JID )
      {
         if ( ( GX_JID == 9 ) || ( GX_JID == 0 ) )
         {
            if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
            {
               Z15Paisid = T000P3_A15Paisid[0];
            }
            else
            {
               Z15Paisid = A15Paisid;
            }
         }
         if ( GX_JID == -9 )
         {
            Z25Proyectoid = A25Proyectoid;
            Z132ProyectoPaisid = A132ProyectoPaisid;
            Z15Paisid = A15Paisid;
         }
      }

      protected void standaloneNotModal0P26( )
      {
      }

      protected void standaloneModal0P26( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
         {
            edtProyectoPaisid_Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtProyectoPaisid_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtProyectoPaisid_Enabled), 5, 0)), !bGXsfl_58_Refreshing);
         }
         else
         {
            edtProyectoPaisid_Enabled = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtProyectoPaisid_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtProyectoPaisid_Enabled), 5, 0)), !bGXsfl_58_Refreshing);
         }
      }

      protected void Load0P26( )
      {
         /* Using cursor T000P21 */
         pr_default.execute(2, new Object[] {A25Proyectoid, A132ProyectoPaisid});
         if ( (pr_default.getStatus(2) != 101) )
         {
            RcdFound26 = 1;
            A15Paisid = T000P21_A15Paisid[0];
            ZM0P26( -9) ;
         }
         pr_default.close(2);
         OnLoadActions0P26( ) ;
      }

      protected void OnLoadActions0P26( )
      {
         /* Using cursor T000P4 */
         pr_datastore1.execute(0, new Object[] {A15Paisid});
         A117codigo = T000P4_A117codigo[0];
         A118Paisnombre = T000P4_A118Paisnombre[0];
         A119moneda = T000P4_A119moneda[0];
         pr_datastore1.close(0);
      }

      protected void CheckExtendedTable0P26( )
      {
         Gx_BScreen = 1;
         standaloneModal0P26( ) ;
         /* Using cursor T000P4 */
         pr_datastore1.execute(0, new Object[] {A15Paisid});
         if ( (pr_datastore1.getStatus(0) == 101) )
         {
            GXCCtl = "PAISID_" + sGXsfl_58_idx;
            GX_msglist.addItem("No matching 'Pais'.", "ForeignKeyNotFound", 1, GXCCtl);
            AnyError = 1;
            GX_FocusControl = edtPaisid_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         A117codigo = T000P4_A117codigo[0];
         A118Paisnombre = T000P4_A118Paisnombre[0];
         A119moneda = T000P4_A119moneda[0];
         pr_datastore1.close(0);
      }

      protected void CloseExtendedTableCursors0P26( )
      {
         pr_datastore1.close(0);
      }

      protected void enableDisable0P26( )
      {
      }

      protected void gxLoad_10( int A15Paisid )
      {
         /* Using cursor T000P22 */
         pr_datastore1.execute(17, new Object[] {A15Paisid});
         if ( (pr_datastore1.getStatus(17) == 101) )
         {
            GXCCtl = "PAISID_" + sGXsfl_58_idx;
            GX_msglist.addItem("No matching 'Pais'.", "ForeignKeyNotFound", 1, GXCCtl);
            AnyError = 1;
            GX_FocusControl = edtPaisid_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         A117codigo = T000P22_A117codigo[0];
         A118Paisnombre = T000P22_A118Paisnombre[0];
         A119moneda = T000P22_A119moneda[0];
         GxWebStd.set_html_headers( context, 0, "", "");
         context.GX_webresponse.AddString("[[");
         context.GX_webresponse.AddString("\""+GXUtil.EncodeJSConstant( A117codigo)+"\""+","+"\""+GXUtil.EncodeJSConstant( A118Paisnombre)+"\""+","+"\""+GXUtil.EncodeJSConstant( A119moneda)+"\"");
         context.GX_webresponse.AddString("]");
         if ( (pr_datastore1.getStatus(17) == 101) )
         {
            context.GX_webresponse.AddString(",");
            context.GX_webresponse.AddString("101");
         }
         context.GX_webresponse.AddString("]");
         pr_datastore1.close(17);
      }

      protected void GetKey0P26( )
      {
         /* Using cursor T000P23 */
         pr_default.execute(3, new Object[] {A25Proyectoid, A132ProyectoPaisid});
         if ( (pr_default.getStatus(3) != 101) )
         {
            RcdFound26 = 1;
         }
         else
         {
            RcdFound26 = 0;
         }
         pr_default.close(3);
      }

      protected void getByPrimaryKey0P26( )
      {
         /* Using cursor T000P3 */
         pr_default.execute(1, new Object[] {A25Proyectoid, A132ProyectoPaisid});
         if ( (pr_default.getStatus(1) != 101) )
         {
            ZM0P26( 9) ;
            RcdFound26 = 1;
            InitializeNonKey0P26( ) ;
            A132ProyectoPaisid = T000P3_A132ProyectoPaisid[0];
            A15Paisid = T000P3_A15Paisid[0];
            Z25Proyectoid = A25Proyectoid;
            Z132ProyectoPaisid = A132ProyectoPaisid;
            sMode26 = Gx_mode;
            Gx_mode = "DSP";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!")), context));
            Load0P26( ) ;
            Gx_mode = sMode26;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!")), context));
         }
         else
         {
            RcdFound26 = 0;
            InitializeNonKey0P26( ) ;
            sMode26 = Gx_mode;
            Gx_mode = "DSP";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!")), context));
            standaloneModal0P26( ) ;
            Gx_mode = sMode26;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!")), context));
         }
         if ( ( StringUtil.StrCmp(Gx_mode, "DSP") == 0 ) || ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 ) )
         {
            DisableAttributes0P26( ) ;
         }
         pr_default.close(1);
      }

      protected void CheckOptimisticConcurrency0P26( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
         {
            /* Using cursor T000P2 */
            pr_default.execute(0, new Object[] {A25Proyectoid, A132ProyectoPaisid});
            if ( (pr_default.getStatus(0) == 103) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_lock", new   object[]  {"ProyectoProyectoPais"}), "RecordIsLocked", 1, "");
               AnyError = 1;
               return  ;
            }
            if ( (pr_default.getStatus(0) == 101) || ( Z15Paisid != T000P2_A15Paisid[0] ) )
            {
               if ( Z15Paisid != T000P2_A15Paisid[0] )
               {
                  GXUtil.WriteLog("proyecto:[seudo value changed for attri]"+"Paisid");
                  GXUtil.WriteLogRaw("Old: ",Z15Paisid);
                  GXUtil.WriteLogRaw("Current: ",T000P2_A15Paisid[0]);
               }
               GX_msglist.addItem(context.GetMessage( "GXM_waschg", new   object[]  {"ProyectoProyectoPais"}), "RecordWasChanged", 1, "");
               AnyError = 1;
               return  ;
            }
         }
      }

      protected void Insert0P26( )
      {
         BeforeValidate0P26( ) ;
         if ( AnyError == 0 )
         {
            CheckExtendedTable0P26( ) ;
         }
         if ( AnyError == 0 )
         {
            ZM0P26( 0) ;
            CheckOptimisticConcurrency0P26( ) ;
            if ( AnyError == 0 )
            {
               AfterConfirm0P26( ) ;
               if ( AnyError == 0 )
               {
                  BeforeInsert0P26( ) ;
                  if ( AnyError == 0 )
                  {
                     /* Using cursor T000P24 */
                     pr_default.execute(4, new Object[] {A25Proyectoid, A132ProyectoPaisid, A15Paisid});
                     pr_default.close(4);
                     dsDefault.SmartCacheProvider.SetUpdated("ProyectoProyectoPais") ;
                     if ( (pr_default.getStatus(4) == 1) )
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_noupdate", ""), "DuplicatePrimaryKey", 1, "");
                        AnyError = 1;
                     }
                     if ( AnyError == 0 )
                     {
                        /* Start of After( Insert) rules */
                        /* End of After( Insert) rules */
                        if ( AnyError == 0 )
                        {
                           /* Save values for previous() function. */
                        }
                     }
                  }
                  else
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                     AnyError = 1;
                  }
               }
            }
            else
            {
               Load0P26( ) ;
            }
            EndLevel0P26( ) ;
         }
         CloseExtendedTableCursors0P26( ) ;
      }

      protected void Update0P26( )
      {
         BeforeValidate0P26( ) ;
         if ( AnyError == 0 )
         {
            CheckExtendedTable0P26( ) ;
         }
         if ( AnyError == 0 )
         {
            CheckOptimisticConcurrency0P26( ) ;
            if ( AnyError == 0 )
            {
               AfterConfirm0P26( ) ;
               if ( AnyError == 0 )
               {
                  BeforeUpdate0P26( ) ;
                  if ( AnyError == 0 )
                  {
                     /* Using cursor T000P25 */
                     pr_default.execute(5, new Object[] {A15Paisid, A25Proyectoid, A132ProyectoPaisid});
                     pr_default.close(5);
                     dsDefault.SmartCacheProvider.SetUpdated("ProyectoProyectoPais") ;
                     if ( (pr_default.getStatus(5) == 103) )
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_lock", new   object[]  {"ProyectoProyectoPais"}), "RecordIsLocked", 1, "");
                        AnyError = 1;
                     }
                     DeferredUpdate0P26( ) ;
                     if ( AnyError == 0 )
                     {
                        /* Start of After( update) rules */
                        /* End of After( update) rules */
                        if ( AnyError == 0 )
                        {
                           getByPrimaryKey0P26( ) ;
                        }
                     }
                     else
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                        AnyError = 1;
                     }
                  }
               }
            }
            EndLevel0P26( ) ;
         }
         CloseExtendedTableCursors0P26( ) ;
      }

      protected void DeferredUpdate0P26( )
      {
      }

      protected void Delete0P26( )
      {
         Gx_mode = "DLT";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!")), context));
         BeforeValidate0P26( ) ;
         if ( AnyError == 0 )
         {
            CheckOptimisticConcurrency0P26( ) ;
         }
         if ( AnyError == 0 )
         {
            OnDeleteControls0P26( ) ;
            AfterConfirm0P26( ) ;
            if ( AnyError == 0 )
            {
               BeforeDelete0P26( ) ;
               if ( AnyError == 0 )
               {
                  /* No cascading delete specified. */
                  /* Using cursor T000P26 */
                  pr_default.execute(6, new Object[] {A25Proyectoid, A132ProyectoPaisid});
                  pr_default.close(6);
                  dsDefault.SmartCacheProvider.SetUpdated("ProyectoProyectoPais") ;
                  if ( AnyError == 0 )
                  {
                     /* Start of After( delete) rules */
                     /* End of After( delete) rules */
                  }
                  else
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                     AnyError = 1;
                  }
               }
            }
         }
         sMode26 = Gx_mode;
         Gx_mode = "DLT";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!")), context));
         EndLevel0P26( ) ;
         Gx_mode = sMode26;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!")), context));
      }

      protected void OnDeleteControls0P26( )
      {
         standaloneModal0P26( ) ;
         if ( AnyError == 0 )
         {
            /* Delete mode formulas */
            /* Using cursor T000P27 */
            pr_datastore1.execute(18, new Object[] {A15Paisid});
            A117codigo = T000P27_A117codigo[0];
            A118Paisnombre = T000P27_A118Paisnombre[0];
            A119moneda = T000P27_A119moneda[0];
            pr_datastore1.close(18);
         }
      }

      protected void EndLevel0P26( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
         {
            pr_default.close(0);
         }
         if ( AnyError != 0 )
         {
            context.wjLoc = "";
            context.nUserReturn = 0;
         }
      }

      public void ScanStart0P26( )
      {
         /* Scan By routine */
         /* Using cursor T000P28 */
         pr_default.execute(7, new Object[] {A25Proyectoid});
         RcdFound26 = 0;
         if ( (pr_default.getStatus(7) != 101) )
         {
            RcdFound26 = 1;
            A132ProyectoPaisid = T000P28_A132ProyectoPaisid[0];
         }
         /* Load Subordinate Levels */
      }

      protected void ScanNext0P26( )
      {
         /* Scan next routine */
         pr_default.readNext(7);
         RcdFound26 = 0;
         if ( (pr_default.getStatus(7) != 101) )
         {
            RcdFound26 = 1;
            A132ProyectoPaisid = T000P28_A132ProyectoPaisid[0];
         }
      }

      protected void ScanEnd0P26( )
      {
         pr_default.close(7);
      }

      protected void AfterConfirm0P26( )
      {
         /* After Confirm Rules */
      }

      protected void BeforeInsert0P26( )
      {
         /* Before Insert Rules */
      }

      protected void BeforeUpdate0P26( )
      {
         /* Before Update Rules */
      }

      protected void BeforeDelete0P26( )
      {
         /* Before Delete Rules */
      }

      protected void BeforeComplete0P26( )
      {
         /* Before Complete Rules */
      }

      protected void BeforeValidate0P26( )
      {
         /* Before Validate Rules */
      }

      protected void DisableAttributes0P26( )
      {
         edtProyectoPaisid_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtProyectoPaisid_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtProyectoPaisid_Enabled), 5, 0)), !bGXsfl_58_Refreshing);
         edtPaisid_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtPaisid_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtPaisid_Enabled), 5, 0)), !bGXsfl_58_Refreshing);
         edtcodigo_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtcodigo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtcodigo_Enabled), 5, 0)), !bGXsfl_58_Refreshing);
         edtPaisnombre_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtPaisnombre_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtPaisnombre_Enabled), 5, 0)), !bGXsfl_58_Refreshing);
      }

      protected void send_integrity_lvl_hashes0P26( )
      {
      }

      protected void send_integrity_lvl_hashes0P25( )
      {
      }

      protected void SubsflControlProps_5826( )
      {
         edtProyectoPaisid_Internalname = "PROYECTOPAISID_"+sGXsfl_58_idx;
         edtPaisid_Internalname = "PAISID_"+sGXsfl_58_idx;
         imgprompt_15_Internalname = "PROMPT_15_"+sGXsfl_58_idx;
         edtcodigo_Internalname = "CODIGO_"+sGXsfl_58_idx;
         edtPaisnombre_Internalname = "PAISNOMBRE_"+sGXsfl_58_idx;
      }

      protected void SubsflControlProps_fel_5826( )
      {
         edtProyectoPaisid_Internalname = "PROYECTOPAISID_"+sGXsfl_58_fel_idx;
         edtPaisid_Internalname = "PAISID_"+sGXsfl_58_fel_idx;
         imgprompt_15_Internalname = "PROMPT_15_"+sGXsfl_58_fel_idx;
         edtcodigo_Internalname = "CODIGO_"+sGXsfl_58_fel_idx;
         edtPaisnombre_Internalname = "PAISNOMBRE_"+sGXsfl_58_fel_idx;
      }

      protected void AddRow0P26( )
      {
         nGXsfl_58_idx = (short)(nGXsfl_58_idx+1);
         sGXsfl_58_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_58_idx), 4, 0)), 4, "0");
         SubsflControlProps_5826( ) ;
         SendRow0P26( ) ;
      }

      protected void SendRow0P26( )
      {
         Gridproyecto_proyectopaisRow = GXWebRow.GetNew(context);
         if ( subGridproyecto_proyectopais_Backcolorstyle == 0 )
         {
            /* None style subfile background logic. */
            subGridproyecto_proyectopais_Backstyle = 0;
            if ( StringUtil.StrCmp(subGridproyecto_proyectopais_Class, "") != 0 )
            {
               subGridproyecto_proyectopais_Linesclass = subGridproyecto_proyectopais_Class+"Odd";
            }
         }
         else if ( subGridproyecto_proyectopais_Backcolorstyle == 1 )
         {
            /* Uniform style subfile background logic. */
            subGridproyecto_proyectopais_Backstyle = 0;
            subGridproyecto_proyectopais_Backcolor = subGridproyecto_proyectopais_Allbackcolor;
            if ( StringUtil.StrCmp(subGridproyecto_proyectopais_Class, "") != 0 )
            {
               subGridproyecto_proyectopais_Linesclass = subGridproyecto_proyectopais_Class+"Uniform";
            }
         }
         else if ( subGridproyecto_proyectopais_Backcolorstyle == 2 )
         {
            /* Header style subfile background logic. */
            subGridproyecto_proyectopais_Backstyle = 1;
            if ( StringUtil.StrCmp(subGridproyecto_proyectopais_Class, "") != 0 )
            {
               subGridproyecto_proyectopais_Linesclass = subGridproyecto_proyectopais_Class+"Odd";
            }
            subGridproyecto_proyectopais_Backcolor = (int)(0x0);
         }
         else if ( subGridproyecto_proyectopais_Backcolorstyle == 3 )
         {
            /* Report style subfile background logic. */
            subGridproyecto_proyectopais_Backstyle = 1;
            if ( ((int)(((nGXsfl_58_idx-1)/ (decimal)(1)) % (2))) == 0 )
            {
               subGridproyecto_proyectopais_Backcolor = (int)(0x0);
               if ( StringUtil.StrCmp(subGridproyecto_proyectopais_Class, "") != 0 )
               {
                  subGridproyecto_proyectopais_Linesclass = subGridproyecto_proyectopais_Class+"Odd";
               }
            }
            else
            {
               subGridproyecto_proyectopais_Backcolor = (int)(0x0);
               if ( StringUtil.StrCmp(subGridproyecto_proyectopais_Class, "") != 0 )
               {
                  subGridproyecto_proyectopais_Linesclass = subGridproyecto_proyectopais_Class+"Even";
               }
            }
         }
         imgprompt_15_Link = ((StringUtil.StrCmp(Gx_mode, "DSP")==0) ? "" : "javascript:"+"gx.popup.openPrompt('"+"gx00d0.aspx"+"',["+"{Ctrl:gx.dom.el('"+"PAISID_"+sGXsfl_58_idx+"'), id:'"+"PAISID_"+sGXsfl_58_idx+"'"+",IOType:'out'}"+"],"+"gx.dom.form()."+"nIsMod_26_"+sGXsfl_58_idx+","+"'', false"+","+"false"+");");
         /* Subfile cell */
         /* Single line edit */
         TempTags = " data-gxoch1=\"gx.fn.setControlValue('nIsMod_26_" + sGXsfl_58_idx + "',1);\"  onfocus=\"gx.evt.onfocus(this, 59,'',false,'" + sGXsfl_58_idx + "',58)\"";
         ROClassString = "Attribute";
         Gridproyecto_proyectopaisRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtProyectoPaisid_Internalname,StringUtil.LTrim( StringUtil.NToC( (decimal)(A132ProyectoPaisid), 4, 0, ".", "")),StringUtil.LTrim( context.localUtil.Format( (decimal)(A132ProyectoPaisid), "ZZZ9")),TempTags+" onchange=\""+"gx.num.valid_integer( this,',');"+";gx.evt.onchange(this, event)\" "+" onblur=\""+";gx.evt.onblur(this,59);\"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtProyectoPaisid_Jsonclick,(short)0,(String)"Attribute",(String)"",(String)ROClassString,(String)"",(String)"",(short)-1,(int)edtProyectoPaisid_Enabled,(short)1,(String)"number",(String)"1",(short)0,(String)"px",(short)17,(String)"px",(short)4,(short)0,(short)0,(short)58,(short)1,(short)-1,(short)0,(bool)true,(String)"",(String)"right",(bool)false});
         /* Subfile cell */
         /* Single line edit */
         TempTags = " data-gxoch1=\"gx.fn.setControlValue('nIsMod_26_" + sGXsfl_58_idx + "',1);\"  onfocus=\"gx.evt.onfocus(this, 60,'',false,'" + sGXsfl_58_idx + "',58)\"";
         ROClassString = "Attribute";
         Gridproyecto_proyectopaisRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtPaisid_Internalname,StringUtil.LTrim( StringUtil.NToC( (decimal)(A15Paisid), 9, 0, ".", "")),((edtPaisid_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( (decimal)(A15Paisid), "ZZZZZZZZ9")) : context.localUtil.Format( (decimal)(A15Paisid), "ZZZZZZZZ9")),TempTags+" onchange=\""+"gx.num.valid_integer( this,',');"+";gx.evt.onchange(this, event)\" "+" onblur=\""+";gx.evt.onblur(this,60);\"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtPaisid_Jsonclick,(short)0,(String)"Attribute",(String)"",(String)ROClassString,(String)"",(String)"",(short)-1,(int)edtPaisid_Enabled,(short)0,(String)"number",(String)"1",(short)0,(String)"px",(short)17,(String)"px",(short)9,(short)0,(short)0,(short)58,(short)1,(short)-1,(short)0,(bool)true,(String)"",(String)"right",(bool)false});
         /* Subfile cell */
         /* Static images/pictures */
         ClassString = "gx-prompt Image";
         StyleString = "";
         sImgUrl = (String)(context.GetImagePath( "prompt.gif", "", context.GetTheme( )));
         Gridproyecto_proyectopaisRow.AddColumnProperties("bitmap", 1, isAjaxCallMode( ), new Object[] {(String)imgprompt_15_Internalname,(String)sImgUrl,(String)imgprompt_15_Link,(String)"",(String)"",context.GetTheme( ),(int)imgprompt_15_Visible,(short)1,(String)"",(String)"",(short)0,(short)0,(short)0,(String)"",(short)0,(String)"",(short)0,(short)0,(short)0,(String)"",(String)"",(String)StyleString,(String)ClassString,(String)"",(String)"",(String)"",(String)"",(String)"",(String)"",(String)"",(short)1,(bool)false,(bool)false,context.GetImageSrcSet( sImgUrl)});
         /* Subfile cell */
         /* Single line edit */
         ROClassString = "Attribute";
         Gridproyecto_proyectopaisRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtcodigo_Internalname,(String)A117codigo,(String)"",(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtcodigo_Jsonclick,(short)0,(String)"Attribute",(String)"",(String)ROClassString,(String)"",(String)"",(short)-1,(int)edtcodigo_Enabled,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)4,(short)0,(short)0,(short)58,(short)1,(short)-1,(short)-1,(bool)true,(String)"",(String)"left",(bool)true});
         /* Subfile cell */
         /* Single line edit */
         ROClassString = "Attribute";
         Gridproyecto_proyectopaisRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtPaisnombre_Internalname,(String)A118Paisnombre,(String)"",(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtPaisnombre_Jsonclick,(short)0,(String)"Attribute",(String)"",(String)ROClassString,(String)"",(String)"",(short)-1,(int)edtPaisnombre_Enabled,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)40,(short)0,(short)0,(short)58,(short)1,(short)-1,(short)-1,(bool)true,(String)"",(String)"left",(bool)true});
         context.httpAjaxContext.ajax_sending_grid_row(Gridproyecto_proyectopaisRow);
         send_integrity_lvl_hashes0P26( ) ;
         GXCCtl = "Z132ProyectoPaisid_" + sGXsfl_58_idx;
         GxWebStd.gx_hidden_field( context, GXCCtl, StringUtil.LTrim( StringUtil.NToC( (decimal)(Z132ProyectoPaisid), 4, 0, ".", "")));
         GXCCtl = "Z15Paisid_" + sGXsfl_58_idx;
         GxWebStd.gx_hidden_field( context, GXCCtl, StringUtil.LTrim( StringUtil.NToC( (decimal)(Z15Paisid), 9, 0, ".", "")));
         GXCCtl = "nRcdDeleted_26_" + sGXsfl_58_idx;
         GxWebStd.gx_hidden_field( context, GXCCtl, StringUtil.LTrim( StringUtil.NToC( (decimal)(nRcdDeleted_26), 4, 0, ".", "")));
         GXCCtl = "nRcdExists_26_" + sGXsfl_58_idx;
         GxWebStd.gx_hidden_field( context, GXCCtl, StringUtil.LTrim( StringUtil.NToC( (decimal)(nRcdExists_26), 4, 0, ".", "")));
         GXCCtl = "nIsMod_26_" + sGXsfl_58_idx;
         GxWebStd.gx_hidden_field( context, GXCCtl, StringUtil.LTrim( StringUtil.NToC( (decimal)(nIsMod_26), 4, 0, ".", "")));
         GXCCtl = "vMODE_" + sGXsfl_58_idx;
         GxWebStd.gx_hidden_field( context, GXCCtl, StringUtil.RTrim( Gx_mode));
         GXCCtl = "vTRNCONTEXT_" + sGXsfl_58_idx;
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, GXCCtl, AV9TrnContext);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt(GXCCtl, AV9TrnContext);
         }
         GXCCtl = "vPROYECTOID_" + sGXsfl_58_idx;
         GxWebStd.gx_hidden_field( context, GXCCtl, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV7Proyectoid), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "PROYECTOPAISID_"+sGXsfl_58_idx+"Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtProyectoPaisid_Enabled), 5, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "PAISID_"+sGXsfl_58_idx+"Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtPaisid_Enabled), 5, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "CODIGO_"+sGXsfl_58_idx+"Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtcodigo_Enabled), 5, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "PAISNOMBRE_"+sGXsfl_58_idx+"Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtPaisnombre_Enabled), 5, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "PROMPT_15_"+sGXsfl_58_idx+"Link", StringUtil.RTrim( imgprompt_15_Link));
         context.httpAjaxContext.ajax_sending_grid_row(null);
         Gridproyecto_proyectopaisContainer.AddRow(Gridproyecto_proyectopaisRow);
      }

      protected void ReadRow0P26( )
      {
         nGXsfl_58_idx = (short)(nGXsfl_58_idx+1);
         sGXsfl_58_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_58_idx), 4, 0)), 4, "0");
         SubsflControlProps_5826( ) ;
         edtProyectoPaisid_Enabled = (int)(context.localUtil.CToN( cgiGet( "PROYECTOPAISID_"+sGXsfl_58_idx+"Enabled"), ".", ","));
         edtPaisid_Enabled = (int)(context.localUtil.CToN( cgiGet( "PAISID_"+sGXsfl_58_idx+"Enabled"), ".", ","));
         edtcodigo_Enabled = (int)(context.localUtil.CToN( cgiGet( "CODIGO_"+sGXsfl_58_idx+"Enabled"), ".", ","));
         edtPaisnombre_Enabled = (int)(context.localUtil.CToN( cgiGet( "PAISNOMBRE_"+sGXsfl_58_idx+"Enabled"), ".", ","));
         imgprompt_2_Link = cgiGet( "PROMPT_15_"+sGXsfl_58_idx+"Link");
         if ( ( ( context.localUtil.CToN( cgiGet( edtProyectoPaisid_Internalname), ".", ",") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtProyectoPaisid_Internalname), ".", ",") > Convert.ToDecimal( 9999 )) ) )
         {
            GXCCtl = "PROYECTOPAISID_" + sGXsfl_58_idx;
            GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, GXCCtl);
            AnyError = 1;
            GX_FocusControl = edtProyectoPaisid_Internalname;
            wbErr = true;
            A132ProyectoPaisid = 0;
         }
         else
         {
            A132ProyectoPaisid = (short)(context.localUtil.CToN( cgiGet( edtProyectoPaisid_Internalname), ".", ","));
         }
         if ( ( ( context.localUtil.CToN( cgiGet( edtPaisid_Internalname), ".", ",") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtPaisid_Internalname), ".", ",") > Convert.ToDecimal( 999999999 )) ) )
         {
            GXCCtl = "PAISID_" + sGXsfl_58_idx;
            GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, GXCCtl);
            AnyError = 1;
            GX_FocusControl = edtPaisid_Internalname;
            wbErr = true;
            A15Paisid = 0;
         }
         else
         {
            A15Paisid = (int)(context.localUtil.CToN( cgiGet( edtPaisid_Internalname), ".", ","));
         }
         A117codigo = cgiGet( edtcodigo_Internalname);
         A118Paisnombre = cgiGet( edtPaisnombre_Internalname);
         GXCCtl = "Z132ProyectoPaisid_" + sGXsfl_58_idx;
         Z132ProyectoPaisid = (short)(context.localUtil.CToN( cgiGet( GXCCtl), ".", ","));
         GXCCtl = "Z15Paisid_" + sGXsfl_58_idx;
         Z15Paisid = (int)(context.localUtil.CToN( cgiGet( GXCCtl), ".", ","));
         GXCCtl = "nRcdDeleted_26_" + sGXsfl_58_idx;
         nRcdDeleted_26 = (short)(context.localUtil.CToN( cgiGet( GXCCtl), ".", ","));
         GXCCtl = "nRcdExists_26_" + sGXsfl_58_idx;
         nRcdExists_26 = (short)(context.localUtil.CToN( cgiGet( GXCCtl), ".", ","));
         GXCCtl = "nIsMod_26_" + sGXsfl_58_idx;
         nIsMod_26 = (short)(context.localUtil.CToN( cgiGet( GXCCtl), ".", ","));
      }

      protected void assign_properties_default( )
      {
         defedtProyectoPaisid_Enabled = edtProyectoPaisid_Enabled;
      }

      protected void ConfirmValues0P0( )
      {
         nGXsfl_58_idx = 0;
         sGXsfl_58_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_58_idx), 4, 0)), 4, "0");
         SubsflControlProps_5826( ) ;
         while ( nGXsfl_58_idx < nRC_GXsfl_58 )
         {
            nGXsfl_58_idx = (short)(nGXsfl_58_idx+1);
            sGXsfl_58_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_58_idx), 4, 0)), 4, "0");
            SubsflControlProps_5826( ) ;
            ChangePostValue( "Z132ProyectoPaisid_"+sGXsfl_58_idx, cgiGet( "ZT_"+"Z132ProyectoPaisid_"+sGXsfl_58_idx)) ;
            DeletePostValue( "ZT_"+"Z132ProyectoPaisid_"+sGXsfl_58_idx) ;
            ChangePostValue( "Z15Paisid_"+sGXsfl_58_idx, cgiGet( "ZT_"+"Z15Paisid_"+sGXsfl_58_idx)) ;
            DeletePostValue( "ZT_"+"Z15Paisid_"+sGXsfl_58_idx) ;
         }
      }

      public override void RenderHtmlHeaders( )
      {
         GxWebStd.gx_html_headers( context, 0, "", "", Form.Meta, Form.Metaequiv, true);
      }

      public override void RenderHtmlOpenForm( )
      {
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.WriteHtmlText( "<title>") ;
         context.SendWebValue( Form.Caption) ;
         context.WriteHtmlTextNl( "</title>") ;
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         if ( StringUtil.Len( sDynURL) > 0 )
         {
            context.WriteHtmlText( "<BASE href=\""+sDynURL+"\" />") ;
         }
         define_styles( ) ;
         MasterPageObj.master_styles();
         if ( ( ( context.GetBrowserType( ) == 1 ) || ( context.GetBrowserType( ) == 5 ) ) && ( StringUtil.StrCmp(context.GetBrowserVersion( ), "7.0") == 0 ) )
         {
            context.AddJavascriptSource("json2.js", "?"+context.GetBuildNumber( 127771), false);
         }
         context.AddJavascriptSource("jquery.js", "?"+context.GetBuildNumber( 127771), false);
         context.AddJavascriptSource("gxtimezone.js", "?"+context.GetBuildNumber( 127771), false);
         context.AddJavascriptSource("gxgral.js", "?"+context.GetBuildNumber( 127771), false);
         context.AddJavascriptSource("gxcfg.js", "?20191514355664", false);
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.WriteHtmlText( Form.Headerrawhtml) ;
         context.CloseHtmlHeader();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         FormProcess = " data-HasEnter=\"true\" data-Skiponenter=\"false\"";
         context.WriteHtmlText( "<body ") ;
         bodyStyle = "" + "background-color:" + context.BuildHTMLColor( Form.Backcolor) + ";color:" + context.BuildHTMLColor( Form.Textcolor) + ";";
         bodyStyle = bodyStyle + "-moz-opacity:0;opacity:0;";
         if ( ! ( String.IsNullOrEmpty(StringUtil.RTrim( Form.Background)) ) )
         {
            bodyStyle = bodyStyle + " background-image:url(" + context.convertURL( Form.Background) + ")";
         }
         context.WriteHtmlText( " "+"class=\"form-horizontal Form\""+" "+ "style='"+bodyStyle+"'") ;
         context.WriteHtmlText( FormProcess+">") ;
         context.skipLines(1);
         context.WriteHtmlTextNl( "<form id=\"MAINFORM\" autocomplete=\"off\" name=\"MAINFORM\" method=\"post\" tabindex=-1  class=\"form-horizontal Form\" data-gx-class=\"form-horizontal Form\" novalidate action=\""+formatLink("proyecto.aspx") + "?" + UrlEncode(StringUtil.RTrim(Gx_mode)) + "," + UrlEncode("" +AV7Proyectoid)+"\">") ;
         GxWebStd.gx_hidden_field( context, "_EventName", "");
         GxWebStd.gx_hidden_field( context, "_EventGridId", "");
         GxWebStd.gx_hidden_field( context, "_EventRowId", "");
         context.WriteHtmlText( "<input type=\"submit\" title=\"submit\" style=\"display:none\" disabled>") ;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, "FORM", "Class", "form-horizontal Form", true);
         toggleJsOutput = isJsOutputEnabled( );
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
      }

      protected void send_integrity_footer_hashes( )
      {
         GXKey = Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
         forbiddenHiddens = "hsh" + "Proyecto";
         forbiddenHiddens = forbiddenHiddens + context.localUtil.Format( (decimal)(AV11Insert_Programaid), "ZZZZZZZZ9");
         forbiddenHiddens = forbiddenHiddens + StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"));
         forbiddenHiddens = forbiddenHiddens + context.localUtil.Format( (decimal)(A25Proyectoid), "ZZZZZZZZ9");
         GxWebStd.gx_hidden_field( context, "hsh", GetEncryptedHash( forbiddenHiddens, GXKey));
         GXUtil.WriteLog("proyecto:[SendSecurityCheck value for]"+"Insert_Programaid:"+context.localUtil.Format( (decimal)(AV11Insert_Programaid), "ZZZZZZZZ9"));
         GXUtil.WriteLog("proyecto:[SendSecurityCheck value for]"+"Gx_mode:"+StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!")));
         GXUtil.WriteLog("proyecto:[SendSecurityCheck value for]"+"Proyectoid:"+context.localUtil.Format( (decimal)(A25Proyectoid), "ZZZZZZZZ9"));
      }

      protected void SendCloseFormHiddens( )
      {
         /* Send hidden variables. */
         /* Send saved values. */
         send_integrity_footer_hashes( ) ;
         GxWebStd.gx_hidden_field( context, "Z25Proyectoid", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z25Proyectoid), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "Z74Proyectonombre", Z74Proyectonombre);
         GxWebStd.gx_hidden_field( context, "Z75marco", Z75marco);
         GxWebStd.gx_hidden_field( context, "Z2Programaid", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z2Programaid), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "IsConfirmed", StringUtil.LTrim( StringUtil.NToC( (decimal)(IsConfirmed), 4, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "IsModified", StringUtil.LTrim( StringUtil.NToC( (decimal)(IsModified), 4, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "Mode", StringUtil.RTrim( Gx_mode));
         GxWebStd.gx_hidden_field( context, "gxhash_Mode", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!")), context));
         GxWebStd.gx_hidden_field( context, "nRC_GXsfl_58", StringUtil.LTrim( StringUtil.NToC( (decimal)(nGXsfl_58_idx), 4, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "N2Programaid", StringUtil.LTrim( StringUtil.NToC( (decimal)(A2Programaid), 9, 0, ".", "")));
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vTRNCONTEXT", AV9TrnContext);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vTRNCONTEXT", AV9TrnContext);
         }
         GxWebStd.gx_hidden_field( context, "vPROYECTOID", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV7Proyectoid), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "gxhash_vPROYECTOID", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV7Proyectoid), "ZZZZZZZZ9"), context));
         GxWebStd.gx_hidden_field( context, "PROYECTOID", StringUtil.LTrim( StringUtil.NToC( (decimal)(A25Proyectoid), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "vINSERT_PROGRAMAID", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV11Insert_Programaid), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "vPGMNAME", StringUtil.RTrim( AV13Pgmname));
         GxWebStd.gx_hidden_field( context, "vMODE", StringUtil.RTrim( Gx_mode));
         GxWebStd.gx_hidden_field( context, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!")), context));
         GxWebStd.gx_hidden_field( context, "MONEDA", A119moneda);
      }

      public override void RenderHtmlCloseForm( )
      {
         SendCloseFormHiddens( ) ;
         GxWebStd.gx_hidden_field( context, "GX_FocusControl", GX_FocusControl);
         SendAjaxEncryptionKey();
         SendSecurityToken(sPrefix);
         SendComponentObjects();
         SendServerCommands();
         SendState();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         context.WriteHtmlTextNl( "</form>") ;
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         include_jscripts( ) ;
      }

      public override short ExecuteStartEvent( )
      {
         standaloneStartup( ) ;
         gxajaxcallmode = (short)((isAjaxCallMode( ) ? 1 : 0));
         return gxajaxcallmode ;
      }

      public override void RenderHtmlContent( )
      {
         context.WriteHtmlText( "<div") ;
         GxWebStd.ClassAttribute( context, "gx-ct-body"+" "+(String.IsNullOrEmpty(StringUtil.RTrim( Form.Class)) ? "form-horizontal Form" : Form.Class)+"-fx");
         context.WriteHtmlText( ">") ;
         Draw( ) ;
         context.WriteHtmlText( "</div>") ;
      }

      public override void DispatchEvents( )
      {
         Process( ) ;
      }

      public override bool HasEnterEvent( )
      {
         return true ;
      }

      public override GXWebForm GetForm( )
      {
         return Form ;
      }

      public override String GetSelfLink( )
      {
         return formatLink("proyecto.aspx") + "?" + UrlEncode(StringUtil.RTrim(Gx_mode)) + "," + UrlEncode("" +AV7Proyectoid) ;
      }

      public override String GetPgmname( )
      {
         return "Proyecto" ;
      }

      public override String GetPgmdesc( )
      {
         return "Proyecto" ;
      }

      protected void InitializeNonKey0P25( )
      {
         A2Programaid = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2Programaid", StringUtil.LTrim( StringUtil.Str( (decimal)(A2Programaid), 9, 0)));
         A72Programanombre = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A72Programanombre", A72Programanombre);
         A74Proyectonombre = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A74Proyectonombre", A74Proyectonombre);
         A75marco = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A75marco", A75marco);
         Z74Proyectonombre = "";
         Z75marco = "";
         Z2Programaid = 0;
      }

      protected void InitAll0P25( )
      {
         A25Proyectoid = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A25Proyectoid", StringUtil.LTrim( StringUtil.Str( (decimal)(A25Proyectoid), 9, 0)));
         InitializeNonKey0P25( ) ;
      }

      protected void StandaloneModalInsert( )
      {
      }

      protected void InitializeNonKey0P26( )
      {
         A15Paisid = 0;
         A117codigo = "";
         A118Paisnombre = "";
         A119moneda = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A119moneda", A119moneda);
         Z15Paisid = 0;
      }

      protected void InitAll0P26( )
      {
         A132ProyectoPaisid = 0;
         InitializeNonKey0P26( ) ;
      }

      protected void StandaloneModalInsert0P26( )
      {
      }

      protected void define_styles( )
      {
         AddThemeStyleSheetFile("", context.GetTheme( )+".css", "?"+GetCacheInvalidationToken( ));
         bool outputEnabled = isOutputEnabled( ) ;
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         idxLst = 1;
         while ( idxLst <= Form.Jscriptsrc.Count )
         {
            context.AddJavascriptSource(StringUtil.RTrim( ((String)Form.Jscriptsrc.Item(idxLst))), "?20191514355681", true);
            idxLst = (int)(idxLst+1);
         }
         if ( ! outputEnabled )
         {
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
         }
         /* End function define_styles */
      }

      protected void include_jscripts( )
      {
         context.AddJavascriptSource("messages.eng.js", "?"+GetCacheInvalidationToken( ), false);
         context.AddJavascriptSource("proyecto.js", "?20191514355681", false);
         /* End function include_jscripts */
      }

      protected void init_level_properties26( )
      {
         edtProyectoPaisid_Enabled = defedtProyectoPaisid_Enabled;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtProyectoPaisid_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtProyectoPaisid_Enabled), 5, 0)), !bGXsfl_58_Refreshing);
      }

      protected void init_default_properties( )
      {
         lblTitle_Internalname = "TITLE";
         divTitlecontainer_Internalname = "TITLECONTAINER";
         bttBtn_first_Internalname = "BTN_FIRST";
         bttBtn_previous_Internalname = "BTN_PREVIOUS";
         bttBtn_next_Internalname = "BTN_NEXT";
         bttBtn_last_Internalname = "BTN_LAST";
         bttBtn_select_Internalname = "BTN_SELECT";
         divToolbarcell_Internalname = "TOOLBARCELL";
         edtProgramaid_Internalname = "PROGRAMAID";
         edtProgramanombre_Internalname = "PROGRAMANOMBRE";
         edtProyectonombre_Internalname = "PROYECTONOMBRE";
         edtmarco_Internalname = "MARCO";
         lblTitleproyectopais_Internalname = "TITLEPROYECTOPAIS";
         edtProyectoPaisid_Internalname = "PROYECTOPAISID";
         edtPaisid_Internalname = "PAISID";
         edtcodigo_Internalname = "CODIGO";
         edtPaisnombre_Internalname = "PAISNOMBRE";
         divProyectopaistable_Internalname = "PROYECTOPAISTABLE";
         divFormcontainer_Internalname = "FORMCONTAINER";
         bttBtn_enter_Internalname = "BTN_ENTER";
         bttBtn_cancel_Internalname = "BTN_CANCEL";
         bttBtn_delete_Internalname = "BTN_DELETE";
         divMaintable_Internalname = "MAINTABLE";
         Form.Internalname = "FORM";
         imgprompt_2_Internalname = "PROMPT_2";
         imgprompt_15_Internalname = "PROMPT_15";
         subGridproyecto_proyectopais_Internalname = "GRIDPROYECTO_PROYECTOPAIS";
      }

      public override void initialize_properties( )
      {
         context.SetDefaultTheme("Carmine");
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         init_default_properties( ) ;
         Form.Headerrawhtml = "";
         Form.Background = "";
         Form.Textcolor = 0;
         Form.Backcolor = (int)(0xFFFFFF);
         Form.Caption = "Proyecto";
         edtPaisnombre_Jsonclick = "";
         edtcodigo_Jsonclick = "";
         imgprompt_15_Visible = 1;
         imgprompt_15_Link = "";
         imgprompt_2_Visible = 1;
         edtPaisid_Jsonclick = "";
         edtProyectoPaisid_Jsonclick = "";
         subGridproyecto_proyectopais_Class = "Grid";
         subGridproyecto_proyectopais_Backcolorstyle = 0;
         subGridproyecto_proyectopais_Allowcollapsing = 0;
         subGridproyecto_proyectopais_Allowselection = 0;
         edtPaisnombre_Enabled = 0;
         edtcodigo_Enabled = 0;
         edtPaisid_Enabled = 1;
         edtProyectoPaisid_Enabled = 1;
         subGridproyecto_proyectopais_Header = "";
         bttBtn_delete_Enabled = 0;
         bttBtn_delete_Visible = 1;
         bttBtn_cancel_Visible = 1;
         bttBtn_enter_Enabled = 1;
         bttBtn_enter_Visible = 1;
         edtmarco_Enabled = 1;
         edtProyectonombre_Jsonclick = "";
         edtProyectonombre_Enabled = 1;
         edtProgramanombre_Jsonclick = "";
         edtProgramanombre_Enabled = 0;
         imgprompt_2_Visible = 1;
         imgprompt_2_Link = "";
         edtProgramaid_Jsonclick = "";
         edtProgramaid_Enabled = 1;
         bttBtn_select_Visible = 1;
         bttBtn_last_Visible = 1;
         bttBtn_next_Visible = 1;
         bttBtn_previous_Visible = 1;
         bttBtn_first_Visible = 1;
         context.GX_msglist.DisplayMode = 1;
         if ( context.isSpaRequest( ) )
         {
            enableJsOutput();
         }
      }

      protected void dynload_actions( )
      {
         /* End function dynload_actions */
      }

      protected void gxnrGridproyecto_proyectopais_newrow( )
      {
         GxWebStd.set_html_headers( context, 0, "", "");
         Gx_mode = "INS";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!")), context));
         SubsflControlProps_5826( ) ;
         while ( nGXsfl_58_idx <= nRC_GXsfl_58 )
         {
            standaloneNotModal( ) ;
            standaloneModal( ) ;
            standaloneNotModal0P26( ) ;
            standaloneModal0P26( ) ;
            init_web_controls( ) ;
            dynload_actions( ) ;
            SendRow0P26( ) ;
            nGXsfl_58_idx = (short)(nGXsfl_58_idx+1);
            sGXsfl_58_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_58_idx), 4, 0)), 4, "0");
            SubsflControlProps_5826( ) ;
         }
         context.GX_webresponse.AddString(context.httpAjaxContext.getJSONContainerResponse( Gridproyecto_proyectopaisContainer));
         /* End function gxnrGridproyecto_proyectopais_newrow */
      }

      protected void init_web_controls( )
      {
         /* End function init_web_controls */
      }

      public void Valid_Programaid( int GX_Parm1 ,
                                    String GX_Parm2 )
      {
         A2Programaid = GX_Parm1;
         A72Programanombre = GX_Parm2;
         /* Using cursor T000P16 */
         pr_datastore1.execute(12, new Object[] {A2Programaid});
         if ( (pr_datastore1.getStatus(12) == 101) )
         {
            GX_msglist.addItem("No matching 'Programa'.", "ForeignKeyNotFound", 1, "PROGRAMAID");
            AnyError = 1;
            GX_FocusControl = edtProgramaid_Internalname;
         }
         A72Programanombre = T000P16_A72Programanombre[0];
         pr_datastore1.close(12);
         dynload_actions( ) ;
         if ( AnyError == 1 )
         {
            A72Programanombre = "";
         }
         isValidOutput.Add(A72Programanombre);
         isValidOutput.Add(context.GX_msglist.ToJavascriptSource());
         isValidOutput.Add(context.httpAjaxContext.ajax_rsp_get_hiddens( ));
         context.GX_webresponse.AddString(isValidOutput.ToJSonString());
         wbTemp = context.ResponseContentType( "application/json");
      }

      public void Valid_Paisid( int GX_Parm1 ,
                                String GX_Parm2 ,
                                String GX_Parm3 ,
                                String GX_Parm4 )
      {
         A15Paisid = GX_Parm1;
         A117codigo = GX_Parm2;
         A118Paisnombre = GX_Parm3;
         A119moneda = GX_Parm4;
         /* Using cursor T000P27 */
         pr_datastore1.execute(18, new Object[] {A15Paisid});
         if ( (pr_datastore1.getStatus(18) == 101) )
         {
            GX_msglist.addItem("No matching 'Pais'.", "ForeignKeyNotFound", 1, "PAISID");
            AnyError = 1;
            GX_FocusControl = edtPaisid_Internalname;
         }
         A117codigo = T000P27_A117codigo[0];
         A118Paisnombre = T000P27_A118Paisnombre[0];
         A119moneda = T000P27_A119moneda[0];
         pr_datastore1.close(18);
         dynload_actions( ) ;
         if ( AnyError == 1 )
         {
            A117codigo = "";
            A118Paisnombre = "";
            A119moneda = "";
         }
         isValidOutput.Add(A117codigo);
         isValidOutput.Add(A118Paisnombre);
         isValidOutput.Add(A119moneda);
         isValidOutput.Add(context.GX_msglist.ToJavascriptSource());
         isValidOutput.Add(context.httpAjaxContext.ajax_rsp_get_hiddens( ));
         context.GX_webresponse.AddString(isValidOutput.ToJSonString());
         wbTemp = context.ResponseContentType( "application/json");
      }

      public override bool SupportAjaxEvent( )
      {
         return true ;
      }

      public override void InitializeDynEvents( )
      {
         setEventMetadata("ENTER","{handler:'UserMainFullajax',iparms:[{postForm:true},{av:'Gx_mode',fld:'vMODE',pic:'@!',hsh:true},{av:'AV7Proyectoid',fld:'vPROYECTOID',pic:'ZZZZZZZZ9',hsh:true}]");
         setEventMetadata("ENTER",",oparms:[]}");
         setEventMetadata("REFRESH","{handler:'Refresh',iparms:[{av:'Gx_mode',fld:'vMODE',pic:'@!',hsh:true},{av:'AV7Proyectoid',fld:'vPROYECTOID',pic:'ZZZZZZZZ9',hsh:true},{av:'AV11Insert_Programaid',fld:'vINSERT_PROGRAMAID',pic:'ZZZZZZZZ9'},{av:'A25Proyectoid',fld:'PROYECTOID',pic:'ZZZZZZZZ9'}]");
         setEventMetadata("REFRESH",",oparms:[]}");
         setEventMetadata("AFTER TRN","{handler:'E120P2',iparms:[{av:'Gx_mode',fld:'vMODE',pic:'@!',hsh:true},{av:'AV9TrnContext',fld:'vTRNCONTEXT',pic:''}]");
         setEventMetadata("AFTER TRN",",oparms:[]}");
         return  ;
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
         pr_default.close(1);
         pr_datastore1.close(18);
         pr_datastore1.close(2);
         pr_datastore1.close(12);
      }

      public override void initialize( )
      {
         sPrefix = "";
         wcpOGx_mode = "";
         Z74Proyectonombre = "";
         Z75marco = "";
         scmdbuf = "";
         gxfirstwebparm = "";
         gxfirstwebparm_bkp = "";
         GXKey = "";
         PreviousTooltip = "";
         PreviousCaption = "";
         Form = new GXWebForm();
         GX_FocusControl = "";
         lblTitle_Jsonclick = "";
         ClassString = "";
         StyleString = "";
         TempTags = "";
         bttBtn_first_Jsonclick = "";
         bttBtn_previous_Jsonclick = "";
         bttBtn_next_Jsonclick = "";
         bttBtn_last_Jsonclick = "";
         bttBtn_select_Jsonclick = "";
         sImgUrl = "";
         A72Programanombre = "";
         A74Proyectonombre = "";
         A75marco = "";
         lblTitleproyectopais_Jsonclick = "";
         bttBtn_enter_Jsonclick = "";
         bttBtn_cancel_Jsonclick = "";
         bttBtn_delete_Jsonclick = "";
         Gridproyecto_proyectopaisContainer = new GXWebGrid( context);
         Gridproyecto_proyectopaisColumn = new GXWebColumn();
         A117codigo = "";
         A118Paisnombre = "";
         sMode26 = "";
         sStyleString = "";
         AV13Pgmname = "";
         A119moneda = "";
         forbiddenHiddens = "";
         hsh = "";
         sMode25 = "";
         sEvt = "";
         EvtGridId = "";
         EvtRowId = "";
         sEvtType = "";
         GXCCtl = "";
         AV9TrnContext = new SdtTransactionContext(context);
         AV10WebSession = context.GetSession();
         AV12TrnContextAtt = new SdtTransactionContext_Attribute(context);
         Z72Programanombre = "";
         T000P7_A72Programanombre = new String[] {""} ;
         T000P8_A25Proyectoid = new int[1] ;
         T000P8_A72Programanombre = new String[] {""} ;
         T000P8_A74Proyectonombre = new String[] {""} ;
         T000P8_A75marco = new String[] {""} ;
         T000P8_A2Programaid = new int[1] ;
         T000P9_A72Programanombre = new String[] {""} ;
         T000P10_A25Proyectoid = new int[1] ;
         T000P6_A25Proyectoid = new int[1] ;
         T000P6_A74Proyectonombre = new String[] {""} ;
         T000P6_A75marco = new String[] {""} ;
         T000P6_A2Programaid = new int[1] ;
         T000P11_A25Proyectoid = new int[1] ;
         T000P12_A25Proyectoid = new int[1] ;
         T000P5_A25Proyectoid = new int[1] ;
         T000P5_A74Proyectonombre = new String[] {""} ;
         T000P5_A75marco = new String[] {""} ;
         T000P5_A2Programaid = new int[1] ;
         T000P13_A25Proyectoid = new int[1] ;
         T000P16_A72Programanombre = new String[] {""} ;
         T000P17_A13Planid = new int[1] ;
         T000P18_A7Proyecto_Comunidadid = new int[1] ;
         T000P19_A4Indicadorid = new int[1] ;
         T000P20_A25Proyectoid = new int[1] ;
         T000P21_A25Proyectoid = new int[1] ;
         T000P21_A132ProyectoPaisid = new short[1] ;
         T000P21_A15Paisid = new int[1] ;
         T000P4_A117codigo = new String[] {""} ;
         T000P4_A118Paisnombre = new String[] {""} ;
         T000P4_A119moneda = new String[] {""} ;
         T000P22_A117codigo = new String[] {""} ;
         T000P22_A118Paisnombre = new String[] {""} ;
         T000P22_A119moneda = new String[] {""} ;
         T000P23_A25Proyectoid = new int[1] ;
         T000P23_A132ProyectoPaisid = new short[1] ;
         T000P3_A25Proyectoid = new int[1] ;
         T000P3_A132ProyectoPaisid = new short[1] ;
         T000P3_A15Paisid = new int[1] ;
         T000P2_A25Proyectoid = new int[1] ;
         T000P2_A132ProyectoPaisid = new short[1] ;
         T000P2_A15Paisid = new int[1] ;
         T000P27_A117codigo = new String[] {""} ;
         T000P27_A118Paisnombre = new String[] {""} ;
         T000P27_A119moneda = new String[] {""} ;
         T000P28_A25Proyectoid = new int[1] ;
         T000P28_A132ProyectoPaisid = new short[1] ;
         Gridproyecto_proyectopaisRow = new GXWebRow();
         subGridproyecto_proyectopais_Linesclass = "";
         ROClassString = "";
         sDynURL = "";
         FormProcess = "";
         bodyStyle = "";
         isValidOutput = new GxUnknownObjectCollection();
         pr_datastore1 = new DataStoreProvider(context, new GeneXus.Programs.proyecto__datastore1(),
            new Object[][] {
                new Object[] {
               T000P4_A117codigo, T000P4_A118Paisnombre, T000P4_A119moneda
               }
               , new Object[] {
               T000P5_A25Proyectoid, T000P5_A74Proyectonombre, T000P5_A75marco, T000P5_A2Programaid
               }
               , new Object[] {
               T000P6_A25Proyectoid, T000P6_A74Proyectonombre, T000P6_A75marco, T000P6_A2Programaid
               }
               , new Object[] {
               T000P7_A72Programanombre
               }
               , new Object[] {
               T000P8_A25Proyectoid, T000P8_A72Programanombre, T000P8_A74Proyectonombre, T000P8_A75marco, T000P8_A2Programaid
               }
               , new Object[] {
               T000P9_A72Programanombre
               }
               , new Object[] {
               T000P10_A25Proyectoid
               }
               , new Object[] {
               T000P11_A25Proyectoid
               }
               , new Object[] {
               T000P12_A25Proyectoid
               }
               , new Object[] {
               T000P13_A25Proyectoid
               }
               , new Object[] {
               }
               , new Object[] {
               }
               , new Object[] {
               T000P16_A72Programanombre
               }
               , new Object[] {
               T000P17_A13Planid
               }
               , new Object[] {
               T000P18_A7Proyecto_Comunidadid
               }
               , new Object[] {
               T000P19_A4Indicadorid
               }
               , new Object[] {
               T000P20_A25Proyectoid
               }
               , new Object[] {
               T000P22_A117codigo, T000P22_A118Paisnombre, T000P22_A119moneda
               }
               , new Object[] {
               T000P27_A117codigo, T000P27_A118Paisnombre, T000P27_A119moneda
               }
            }
         );
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.proyecto__default(),
            new Object[][] {
                new Object[] {
               T000P2_A25Proyectoid, T000P2_A132ProyectoPaisid, T000P2_A15Paisid
               }
               , new Object[] {
               T000P3_A25Proyectoid, T000P3_A132ProyectoPaisid, T000P3_A15Paisid
               }
               , new Object[] {
               T000P21_A25Proyectoid, T000P21_A132ProyectoPaisid, T000P21_A15Paisid
               }
               , new Object[] {
               T000P23_A25Proyectoid, T000P23_A132ProyectoPaisid
               }
               , new Object[] {
               }
               , new Object[] {
               }
               , new Object[] {
               }
               , new Object[] {
               T000P28_A25Proyectoid, T000P28_A132ProyectoPaisid
               }
            }
         );
         AV13Pgmname = "Proyecto";
      }

      private short nIsMod_26 ;
      private short nRC_GXsfl_58 ;
      private short nGXsfl_58_idx=1 ;
      private short Z132ProyectoPaisid ;
      private short nRcdDeleted_26 ;
      private short nRcdExists_26 ;
      private short GxWebError ;
      private short gxcookieaux ;
      private short IsConfirmed ;
      private short IsModified ;
      private short AnyError ;
      private short nKeyPressed ;
      private short initialized ;
      private short subGridproyecto_proyectopais_Backcolorstyle ;
      private short A132ProyectoPaisid ;
      private short subGridproyecto_proyectopais_Allowselection ;
      private short subGridproyecto_proyectopais_Allowhovering ;
      private short subGridproyecto_proyectopais_Allowcollapsing ;
      private short subGridproyecto_proyectopais_Collapsed ;
      private short nBlankRcdCount26 ;
      private short RcdFound26 ;
      private short nBlankRcdUsr26 ;
      private short RcdFound25 ;
      private short GX_JID ;
      private short Gx_BScreen ;
      private short subGridproyecto_proyectopais_Backstyle ;
      private short gxajaxcallmode ;
      private short wbTemp ;
      private int wcpOAV7Proyectoid ;
      private int Z25Proyectoid ;
      private int Z2Programaid ;
      private int N2Programaid ;
      private int Z15Paisid ;
      private int A2Programaid ;
      private int A15Paisid ;
      private int AV7Proyectoid ;
      private int trnEnded ;
      private int bttBtn_first_Visible ;
      private int bttBtn_previous_Visible ;
      private int bttBtn_next_Visible ;
      private int bttBtn_last_Visible ;
      private int bttBtn_select_Visible ;
      private int edtProgramaid_Enabled ;
      private int imgprompt_2_Visible ;
      private int edtProgramanombre_Enabled ;
      private int edtProyectonombre_Enabled ;
      private int edtmarco_Enabled ;
      private int bttBtn_enter_Visible ;
      private int bttBtn_enter_Enabled ;
      private int bttBtn_cancel_Visible ;
      private int bttBtn_delete_Visible ;
      private int bttBtn_delete_Enabled ;
      private int edtProyectoPaisid_Enabled ;
      private int edtPaisid_Enabled ;
      private int edtcodigo_Enabled ;
      private int edtPaisnombre_Enabled ;
      private int subGridproyecto_proyectopais_Selectedindex ;
      private int subGridproyecto_proyectopais_Selectioncolor ;
      private int subGridproyecto_proyectopais_Hoveringcolor ;
      private int fRowAdded ;
      private int A25Proyectoid ;
      private int AV11Insert_Programaid ;
      private int AV14GXV1 ;
      private int subGridproyecto_proyectopais_Backcolor ;
      private int subGridproyecto_proyectopais_Allbackcolor ;
      private int imgprompt_15_Visible ;
      private int defedtProyectoPaisid_Enabled ;
      private int idxLst ;
      private long GRIDPROYECTO_PROYECTOPAIS_nFirstRecordOnPage ;
      private String sPrefix ;
      private String sGXsfl_58_idx="0001" ;
      private String wcpOGx_mode ;
      private String scmdbuf ;
      private String gxfirstwebparm ;
      private String gxfirstwebparm_bkp ;
      private String Gx_mode ;
      private String GXKey ;
      private String PreviousTooltip ;
      private String PreviousCaption ;
      private String GX_FocusControl ;
      private String edtProgramaid_Internalname ;
      private String divMaintable_Internalname ;
      private String divTitlecontainer_Internalname ;
      private String lblTitle_Internalname ;
      private String lblTitle_Jsonclick ;
      private String ClassString ;
      private String StyleString ;
      private String divFormcontainer_Internalname ;
      private String divToolbarcell_Internalname ;
      private String TempTags ;
      private String bttBtn_first_Internalname ;
      private String bttBtn_first_Jsonclick ;
      private String bttBtn_previous_Internalname ;
      private String bttBtn_previous_Jsonclick ;
      private String bttBtn_next_Internalname ;
      private String bttBtn_next_Jsonclick ;
      private String bttBtn_last_Internalname ;
      private String bttBtn_last_Jsonclick ;
      private String bttBtn_select_Internalname ;
      private String bttBtn_select_Jsonclick ;
      private String edtProgramaid_Jsonclick ;
      private String sImgUrl ;
      private String imgprompt_2_Internalname ;
      private String imgprompt_2_Link ;
      private String edtProgramanombre_Internalname ;
      private String edtProgramanombre_Jsonclick ;
      private String edtProyectonombre_Internalname ;
      private String edtProyectonombre_Jsonclick ;
      private String edtmarco_Internalname ;
      private String divProyectopaistable_Internalname ;
      private String lblTitleproyectopais_Internalname ;
      private String lblTitleproyectopais_Jsonclick ;
      private String bttBtn_enter_Internalname ;
      private String bttBtn_enter_Jsonclick ;
      private String bttBtn_cancel_Internalname ;
      private String bttBtn_cancel_Jsonclick ;
      private String bttBtn_delete_Internalname ;
      private String bttBtn_delete_Jsonclick ;
      private String subGridproyecto_proyectopais_Header ;
      private String sMode26 ;
      private String edtProyectoPaisid_Internalname ;
      private String edtPaisid_Internalname ;
      private String edtcodigo_Internalname ;
      private String edtPaisnombre_Internalname ;
      private String sStyleString ;
      private String AV13Pgmname ;
      private String forbiddenHiddens ;
      private String hsh ;
      private String sMode25 ;
      private String sEvt ;
      private String EvtGridId ;
      private String EvtRowId ;
      private String sEvtType ;
      private String GXCCtl ;
      private String imgprompt_15_Internalname ;
      private String sGXsfl_58_fel_idx="0001" ;
      private String subGridproyecto_proyectopais_Class ;
      private String subGridproyecto_proyectopais_Linesclass ;
      private String imgprompt_15_Link ;
      private String ROClassString ;
      private String edtProyectoPaisid_Jsonclick ;
      private String edtPaisid_Jsonclick ;
      private String edtcodigo_Jsonclick ;
      private String edtPaisnombre_Jsonclick ;
      private String sDynURL ;
      private String FormProcess ;
      private String bodyStyle ;
      private String subGridproyecto_proyectopais_Internalname ;
      private bool entryPointCalled ;
      private bool toggleJsOutput ;
      private bool wbErr ;
      private bool bGXsfl_58_Refreshing=false ;
      private bool returnInSub ;
      private String Z74Proyectonombre ;
      private String Z75marco ;
      private String A72Programanombre ;
      private String A74Proyectonombre ;
      private String A75marco ;
      private String A117codigo ;
      private String A118Paisnombre ;
      private String A119moneda ;
      private String Z72Programanombre ;
      private IGxSession AV10WebSession ;
      private GxUnknownObjectCollection isValidOutput ;
      private GXWebGrid Gridproyecto_proyectopaisContainer ;
      private GXWebRow Gridproyecto_proyectopaisRow ;
      private GXWebColumn Gridproyecto_proyectopaisColumn ;
      private IGxDataStore dsDataStore1 ;
      private IGxDataStore dsDefault ;
      private IDataStoreProvider pr_datastore1 ;
      private String[] T000P7_A72Programanombre ;
      private int[] T000P8_A25Proyectoid ;
      private String[] T000P8_A72Programanombre ;
      private String[] T000P8_A74Proyectonombre ;
      private String[] T000P8_A75marco ;
      private int[] T000P8_A2Programaid ;
      private String[] T000P9_A72Programanombre ;
      private int[] T000P10_A25Proyectoid ;
      private int[] T000P6_A25Proyectoid ;
      private String[] T000P6_A74Proyectonombre ;
      private String[] T000P6_A75marco ;
      private int[] T000P6_A2Programaid ;
      private int[] T000P11_A25Proyectoid ;
      private int[] T000P12_A25Proyectoid ;
      private int[] T000P5_A25Proyectoid ;
      private String[] T000P5_A74Proyectonombre ;
      private String[] T000P5_A75marco ;
      private int[] T000P5_A2Programaid ;
      private int[] T000P13_A25Proyectoid ;
      private String[] T000P16_A72Programanombre ;
      private int[] T000P17_A13Planid ;
      private int[] T000P18_A7Proyecto_Comunidadid ;
      private int[] T000P19_A4Indicadorid ;
      private IDataStoreProvider pr_default ;
      private int[] T000P20_A25Proyectoid ;
      private int[] T000P21_A25Proyectoid ;
      private short[] T000P21_A132ProyectoPaisid ;
      private int[] T000P21_A15Paisid ;
      private String[] T000P4_A117codigo ;
      private String[] T000P4_A118Paisnombre ;
      private String[] T000P4_A119moneda ;
      private String[] T000P22_A117codigo ;
      private String[] T000P22_A118Paisnombre ;
      private String[] T000P22_A119moneda ;
      private int[] T000P23_A25Proyectoid ;
      private short[] T000P23_A132ProyectoPaisid ;
      private int[] T000P3_A25Proyectoid ;
      private short[] T000P3_A132ProyectoPaisid ;
      private int[] T000P3_A15Paisid ;
      private int[] T000P2_A25Proyectoid ;
      private short[] T000P2_A132ProyectoPaisid ;
      private int[] T000P2_A15Paisid ;
      private String[] T000P27_A117codigo ;
      private String[] T000P27_A118Paisnombre ;
      private String[] T000P27_A119moneda ;
      private int[] T000P28_A25Proyectoid ;
      private short[] T000P28_A132ProyectoPaisid ;
      private GXWebForm Form ;
      private SdtTransactionContext AV9TrnContext ;
      private SdtTransactionContext_Attribute AV12TrnContextAtt ;
   }

   public class proyecto__datastore1 : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
         ,new ForEachCursor(def[2])
         ,new ForEachCursor(def[3])
         ,new ForEachCursor(def[4])
         ,new ForEachCursor(def[5])
         ,new ForEachCursor(def[6])
         ,new ForEachCursor(def[7])
         ,new ForEachCursor(def[8])
         ,new ForEachCursor(def[9])
         ,new UpdateCursor(def[10])
         ,new UpdateCursor(def[11])
         ,new ForEachCursor(def[12])
         ,new ForEachCursor(def[13])
         ,new ForEachCursor(def[14])
         ,new ForEachCursor(def[15])
         ,new ForEachCursor(def[16])
         ,new ForEachCursor(def[17])
         ,new ForEachCursor(def[18])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmT000P8 ;
          prmT000P8 = new Object[] {
          new Object[] {"@Proyectoid",SqlDbType.Int,9,0}
          } ;
          Object[] prmT000P7 ;
          prmT000P7 = new Object[] {
          new Object[] {"@Programaid",SqlDbType.Int,9,0}
          } ;
          Object[] prmT000P9 ;
          prmT000P9 = new Object[] {
          new Object[] {"@Programaid",SqlDbType.Int,9,0}
          } ;
          Object[] prmT000P10 ;
          prmT000P10 = new Object[] {
          new Object[] {"@Proyectoid",SqlDbType.Int,9,0}
          } ;
          Object[] prmT000P6 ;
          prmT000P6 = new Object[] {
          new Object[] {"@Proyectoid",SqlDbType.Int,9,0}
          } ;
          Object[] prmT000P11 ;
          prmT000P11 = new Object[] {
          new Object[] {"@Proyectoid",SqlDbType.Int,9,0}
          } ;
          Object[] prmT000P12 ;
          prmT000P12 = new Object[] {
          new Object[] {"@Proyectoid",SqlDbType.Int,9,0}
          } ;
          Object[] prmT000P5 ;
          prmT000P5 = new Object[] {
          new Object[] {"@Proyectoid",SqlDbType.Int,9,0}
          } ;
          Object[] prmT000P13 ;
          prmT000P13 = new Object[] {
          new Object[] {"@Proyectonombre",SqlDbType.VarChar,100,0} ,
          new Object[] {"@marco",SqlDbType.VarChar,2000,0} ,
          new Object[] {"@Programaid",SqlDbType.Int,9,0}
          } ;
          Object[] prmT000P14 ;
          prmT000P14 = new Object[] {
          new Object[] {"@Proyectonombre",SqlDbType.VarChar,100,0} ,
          new Object[] {"@marco",SqlDbType.VarChar,2000,0} ,
          new Object[] {"@Programaid",SqlDbType.Int,9,0} ,
          new Object[] {"@Proyectoid",SqlDbType.Int,9,0}
          } ;
          Object[] prmT000P15 ;
          prmT000P15 = new Object[] {
          new Object[] {"@Proyectoid",SqlDbType.Int,9,0}
          } ;
          Object[] prmT000P17 ;
          prmT000P17 = new Object[] {
          new Object[] {"@Proyectoid",SqlDbType.Int,9,0}
          } ;
          Object[] prmT000P18 ;
          prmT000P18 = new Object[] {
          new Object[] {"@Proyectoid",SqlDbType.Int,9,0}
          } ;
          Object[] prmT000P19 ;
          prmT000P19 = new Object[] {
          new Object[] {"@Proyectoid",SqlDbType.Int,9,0}
          } ;
          Object[] prmT000P20 ;
          prmT000P20 = new Object[] {
          } ;
          Object[] prmT000P4 ;
          prmT000P4 = new Object[] {
          new Object[] {"@Paisid",SqlDbType.Int,9,0}
          } ;
          Object[] prmT000P22 ;
          prmT000P22 = new Object[] {
          new Object[] {"@Paisid",SqlDbType.Int,9,0}
          } ;
          Object[] prmT000P16 ;
          prmT000P16 = new Object[] {
          new Object[] {"@Programaid",SqlDbType.Int,9,0}
          } ;
          Object[] prmT000P27 ;
          prmT000P27 = new Object[] {
          new Object[] {"@Paisid",SqlDbType.Int,9,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("T000P4", "SELECT [codigo], [nombre] AS Paisnombre, [moneda] FROM dbo.[Pais] WITH (NOLOCK) WHERE [id] = @Paisid ",true, GxErrorMask.GX_NOMASK, false, this,prmT000P4,1,0,true,false )
             ,new CursorDef("T000P5", "SELECT [id] AS Proyectoid, [nombre] AS Proyectonombre, [marco], [idPrograma] AS Programaid FROM dbo.[Proyecto] WITH (UPDLOCK) WHERE [id] = @Proyectoid ",true, GxErrorMask.GX_NOMASK, false, this,prmT000P5,1,0,true,false )
             ,new CursorDef("T000P6", "SELECT [id] AS Proyectoid, [nombre] AS Proyectonombre, [marco], [idPrograma] AS Programaid FROM dbo.[Proyecto] WITH (NOLOCK) WHERE [id] = @Proyectoid ",true, GxErrorMask.GX_NOMASK, false, this,prmT000P6,1,0,true,false )
             ,new CursorDef("T000P7", "SELECT [nombre] AS Programanombre FROM dbo.[Programa] WITH (NOLOCK) WHERE [id] = @Programaid ",true, GxErrorMask.GX_NOMASK, false, this,prmT000P7,1,0,true,false )
             ,new CursorDef("T000P8", "SELECT TM1.[id] AS Proyectoid, T2.[nombre] AS Programanombre, TM1.[nombre] AS Proyectonombre, TM1.[marco], TM1.[idPrograma] AS Programaid FROM (dbo.[Proyecto] TM1 WITH (NOLOCK) INNER JOIN dbo.[Programa] T2 WITH (NOLOCK) ON T2.[id] = TM1.[idPrograma]) WHERE TM1.[id] = @Proyectoid ORDER BY TM1.[id]  OPTION (FAST 100)",true, GxErrorMask.GX_NOMASK, false, this,prmT000P8,100,0,true,false )
             ,new CursorDef("T000P9", "SELECT [nombre] AS Programanombre FROM dbo.[Programa] WITH (NOLOCK) WHERE [id] = @Programaid ",true, GxErrorMask.GX_NOMASK, false, this,prmT000P9,1,0,true,false )
             ,new CursorDef("T000P10", "SELECT [id] AS Proyectoid FROM dbo.[Proyecto] WITH (NOLOCK) WHERE [id] = @Proyectoid  OPTION (FAST 1)",true, GxErrorMask.GX_NOMASK, false, this,prmT000P10,1,0,true,false )
             ,new CursorDef("T000P11", "SELECT TOP 1 [id] AS Proyectoid FROM dbo.[Proyecto] WITH (NOLOCK) WHERE ( [id] > @Proyectoid) ORDER BY [id]  OPTION (FAST 1)",true, GxErrorMask.GX_NOMASK, false, this,prmT000P11,1,0,true,true )
             ,new CursorDef("T000P12", "SELECT TOP 1 [id] AS Proyectoid FROM dbo.[Proyecto] WITH (NOLOCK) WHERE ( [id] < @Proyectoid) ORDER BY [id] DESC  OPTION (FAST 1)",true, GxErrorMask.GX_NOMASK, false, this,prmT000P12,1,0,true,true )
             ,new CursorDef("T000P13", "INSERT INTO dbo.[Proyecto]([nombre], [marco], [idPrograma]) VALUES(@Proyectonombre, @marco, @Programaid); SELECT SCOPE_IDENTITY()", GxErrorMask.GX_NOMASK,prmT000P13)
             ,new CursorDef("T000P14", "UPDATE dbo.[Proyecto] SET [nombre]=@Proyectonombre, [marco]=@marco, [idPrograma]=@Programaid  WHERE [id] = @Proyectoid", GxErrorMask.GX_NOMASK,prmT000P14)
             ,new CursorDef("T000P15", "DELETE FROM dbo.[Proyecto]  WHERE [id] = @Proyectoid", GxErrorMask.GX_NOMASK,prmT000P15)
             ,new CursorDef("T000P16", "SELECT [nombre] AS Programanombre FROM dbo.[Programa] WITH (NOLOCK) WHERE [id] = @Programaid ",true, GxErrorMask.GX_NOMASK, false, this,prmT000P16,1,0,true,false )
             ,new CursorDef("T000P17", "SELECT TOP 1 [id] AS Planid FROM dbo.[Plan] WITH (NOLOCK) WHERE [idProyecto] = @Proyectoid ",true, GxErrorMask.GX_NOMASK, false, this,prmT000P17,1,0,true,true )
             ,new CursorDef("T000P18", "SELECT TOP 1 [id] AS Proyecto_Comunidadid FROM dbo.[Proyecto-Comunidad] WITH (NOLOCK) WHERE [idProyecto] = @Proyectoid ",true, GxErrorMask.GX_NOMASK, false, this,prmT000P18,1,0,true,true )
             ,new CursorDef("T000P19", "SELECT TOP 1 [id] AS Indicadorid FROM dbo.[Indicador] WITH (NOLOCK) WHERE [idProyecto] = @Proyectoid ",true, GxErrorMask.GX_NOMASK, false, this,prmT000P19,1,0,true,true )
             ,new CursorDef("T000P20", "SELECT [id] AS Proyectoid FROM dbo.[Proyecto] WITH (NOLOCK) ORDER BY [id]  OPTION (FAST 100)",true, GxErrorMask.GX_NOMASK, false, this,prmT000P20,100,0,true,false )
             ,new CursorDef("T000P22", "SELECT [codigo], [nombre] AS Paisnombre, [moneda] FROM dbo.[Pais] WITH (NOLOCK) WHERE [id] = @Paisid ",true, GxErrorMask.GX_NOMASK, false, this,prmT000P22,1,0,true,false )
             ,new CursorDef("T000P27", "SELECT [codigo], [nombre] AS Paisnombre, [moneda] FROM dbo.[Pais] WITH (NOLOCK) WHERE [id] = @Paisid ",true, GxErrorMask.GX_NOMASK, false, this,prmT000P27,1,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((String[]) buf[0])[0] = rslt.getVarchar(1) ;
                ((String[]) buf[1])[0] = rslt.getVarchar(2) ;
                ((String[]) buf[2])[0] = rslt.getVarchar(3) ;
                return;
             case 1 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getVarchar(2) ;
                ((String[]) buf[2])[0] = rslt.getVarchar(3) ;
                ((int[]) buf[3])[0] = rslt.getInt(4) ;
                return;
             case 2 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getVarchar(2) ;
                ((String[]) buf[2])[0] = rslt.getVarchar(3) ;
                ((int[]) buf[3])[0] = rslt.getInt(4) ;
                return;
             case 3 :
                ((String[]) buf[0])[0] = rslt.getVarchar(1) ;
                return;
             case 4 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getVarchar(2) ;
                ((String[]) buf[2])[0] = rslt.getVarchar(3) ;
                ((String[]) buf[3])[0] = rslt.getVarchar(4) ;
                ((int[]) buf[4])[0] = rslt.getInt(5) ;
                return;
             case 5 :
                ((String[]) buf[0])[0] = rslt.getVarchar(1) ;
                return;
             case 6 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 7 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 8 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 9 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 12 :
                ((String[]) buf[0])[0] = rslt.getVarchar(1) ;
                return;
             case 13 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 14 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 15 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 16 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 17 :
                ((String[]) buf[0])[0] = rslt.getVarchar(1) ;
                ((String[]) buf[1])[0] = rslt.getVarchar(2) ;
                ((String[]) buf[2])[0] = rslt.getVarchar(3) ;
                return;
             case 18 :
                ((String[]) buf[0])[0] = rslt.getVarchar(1) ;
                ((String[]) buf[1])[0] = rslt.getVarchar(2) ;
                ((String[]) buf[2])[0] = rslt.getVarchar(3) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 1 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 2 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 3 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 4 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 5 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 6 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 7 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 8 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 9 :
                stmt.SetParameter(1, (String)parms[0]);
                stmt.SetParameter(2, (String)parms[1]);
                stmt.SetParameter(3, (int)parms[2]);
                return;
             case 10 :
                stmt.SetParameter(1, (String)parms[0]);
                stmt.SetParameter(2, (String)parms[1]);
                stmt.SetParameter(3, (int)parms[2]);
                stmt.SetParameter(4, (int)parms[3]);
                return;
             case 11 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 12 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 13 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 14 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 15 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 17 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 18 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
       }
    }

    public String getDataStoreName( )
    {
       return "DATASTORE1";
    }

 }

 public class proyecto__default : DataStoreHelperBase, IDataStoreHelper
 {
    public ICursor[] getCursors( )
    {
       cursorDefinitions();
       return new Cursor[] {
        new ForEachCursor(def[0])
       ,new ForEachCursor(def[1])
       ,new ForEachCursor(def[2])
       ,new ForEachCursor(def[3])
       ,new UpdateCursor(def[4])
       ,new UpdateCursor(def[5])
       ,new UpdateCursor(def[6])
       ,new ForEachCursor(def[7])
     };
  }

  private static CursorDef[] def;
  private void cursorDefinitions( )
  {
     if ( def == null )
     {
        Object[] prmT000P21 ;
        prmT000P21 = new Object[] {
        new Object[] {"@Proyectoid",SqlDbType.Int,9,0} ,
        new Object[] {"@ProyectoPaisid",SqlDbType.SmallInt,4,0}
        } ;
        Object[] prmT000P23 ;
        prmT000P23 = new Object[] {
        new Object[] {"@Proyectoid",SqlDbType.Int,9,0} ,
        new Object[] {"@ProyectoPaisid",SqlDbType.SmallInt,4,0}
        } ;
        Object[] prmT000P3 ;
        prmT000P3 = new Object[] {
        new Object[] {"@Proyectoid",SqlDbType.Int,9,0} ,
        new Object[] {"@ProyectoPaisid",SqlDbType.SmallInt,4,0}
        } ;
        Object[] prmT000P2 ;
        prmT000P2 = new Object[] {
        new Object[] {"@Proyectoid",SqlDbType.Int,9,0} ,
        new Object[] {"@ProyectoPaisid",SqlDbType.SmallInt,4,0}
        } ;
        Object[] prmT000P24 ;
        prmT000P24 = new Object[] {
        new Object[] {"@Proyectoid",SqlDbType.Int,9,0} ,
        new Object[] {"@ProyectoPaisid",SqlDbType.SmallInt,4,0} ,
        new Object[] {"@Paisid",SqlDbType.Int,9,0}
        } ;
        Object[] prmT000P25 ;
        prmT000P25 = new Object[] {
        new Object[] {"@Paisid",SqlDbType.Int,9,0} ,
        new Object[] {"@Proyectoid",SqlDbType.Int,9,0} ,
        new Object[] {"@ProyectoPaisid",SqlDbType.SmallInt,4,0}
        } ;
        Object[] prmT000P26 ;
        prmT000P26 = new Object[] {
        new Object[] {"@Proyectoid",SqlDbType.Int,9,0} ,
        new Object[] {"@ProyectoPaisid",SqlDbType.SmallInt,4,0}
        } ;
        Object[] prmT000P28 ;
        prmT000P28 = new Object[] {
        new Object[] {"@Proyectoid",SqlDbType.Int,9,0}
        } ;
        def= new CursorDef[] {
            new CursorDef("T000P2", "SELECT [Proyectoid] AS Proyectoid, [ProyectoPaisid], [Paisid] AS Paisid FROM [ProyectoProyectoPais] WITH (UPDLOCK) WHERE [Proyectoid] = @Proyectoid AND [ProyectoPaisid] = @ProyectoPaisid ",true, GxErrorMask.GX_NOMASK, false, this,prmT000P2,1,0,true,false )
           ,new CursorDef("T000P3", "SELECT [Proyectoid] AS Proyectoid, [ProyectoPaisid], [Paisid] AS Paisid FROM [ProyectoProyectoPais] WITH (NOLOCK) WHERE [Proyectoid] = @Proyectoid AND [ProyectoPaisid] = @ProyectoPaisid ",true, GxErrorMask.GX_NOMASK, false, this,prmT000P3,1,0,true,false )
           ,new CursorDef("T000P21", "SELECT [Proyectoid] AS Proyectoid, [ProyectoPaisid], [Paisid] AS Paisid FROM [ProyectoProyectoPais] WITH (NOLOCK) WHERE [Proyectoid] = @Proyectoid and [ProyectoPaisid] = @ProyectoPaisid ORDER BY [Proyectoid], [ProyectoPaisid] ",true, GxErrorMask.GX_NOMASK, false, this,prmT000P21,11,0,true,false )
           ,new CursorDef("T000P23", "SELECT [Proyectoid] AS Proyectoid, [ProyectoPaisid] FROM [ProyectoProyectoPais] WITH (NOLOCK) WHERE [Proyectoid] = @Proyectoid AND [ProyectoPaisid] = @ProyectoPaisid ",true, GxErrorMask.GX_NOMASK, false, this,prmT000P23,1,0,true,false )
           ,new CursorDef("T000P24", "INSERT INTO [ProyectoProyectoPais]([Proyectoid], [ProyectoPaisid], [Paisid]) VALUES(@Proyectoid, @ProyectoPaisid, @Paisid)", GxErrorMask.GX_NOMASK,prmT000P24)
           ,new CursorDef("T000P25", "UPDATE [ProyectoProyectoPais] SET [Paisid]=@Paisid  WHERE [Proyectoid] = @Proyectoid AND [ProyectoPaisid] = @ProyectoPaisid", GxErrorMask.GX_NOMASK,prmT000P25)
           ,new CursorDef("T000P26", "DELETE FROM [ProyectoProyectoPais]  WHERE [Proyectoid] = @Proyectoid AND [ProyectoPaisid] = @ProyectoPaisid", GxErrorMask.GX_NOMASK,prmT000P26)
           ,new CursorDef("T000P28", "SELECT [Proyectoid] AS Proyectoid, [ProyectoPaisid] FROM [ProyectoProyectoPais] WITH (NOLOCK) WHERE [Proyectoid] = @Proyectoid ORDER BY [Proyectoid], [ProyectoPaisid] ",true, GxErrorMask.GX_NOMASK, false, this,prmT000P28,11,0,true,false )
        };
     }
  }

  public void getResults( int cursor ,
                          IFieldGetter rslt ,
                          Object[] buf )
  {
     switch ( cursor )
     {
           case 0 :
              ((int[]) buf[0])[0] = rslt.getInt(1) ;
              ((short[]) buf[1])[0] = rslt.getShort(2) ;
              ((int[]) buf[2])[0] = rslt.getInt(3) ;
              return;
           case 1 :
              ((int[]) buf[0])[0] = rslt.getInt(1) ;
              ((short[]) buf[1])[0] = rslt.getShort(2) ;
              ((int[]) buf[2])[0] = rslt.getInt(3) ;
              return;
           case 2 :
              ((int[]) buf[0])[0] = rslt.getInt(1) ;
              ((short[]) buf[1])[0] = rslt.getShort(2) ;
              ((int[]) buf[2])[0] = rslt.getInt(3) ;
              return;
           case 3 :
              ((int[]) buf[0])[0] = rslt.getInt(1) ;
              ((short[]) buf[1])[0] = rslt.getShort(2) ;
              return;
           case 7 :
              ((int[]) buf[0])[0] = rslt.getInt(1) ;
              ((short[]) buf[1])[0] = rslt.getShort(2) ;
              return;
     }
  }

  public void setParameters( int cursor ,
                             IFieldSetter stmt ,
                             Object[] parms )
  {
     switch ( cursor )
     {
           case 0 :
              stmt.SetParameter(1, (int)parms[0]);
              stmt.SetParameter(2, (short)parms[1]);
              return;
           case 1 :
              stmt.SetParameter(1, (int)parms[0]);
              stmt.SetParameter(2, (short)parms[1]);
              return;
           case 2 :
              stmt.SetParameter(1, (int)parms[0]);
              stmt.SetParameter(2, (short)parms[1]);
              return;
           case 3 :
              stmt.SetParameter(1, (int)parms[0]);
              stmt.SetParameter(2, (short)parms[1]);
              return;
           case 4 :
              stmt.SetParameter(1, (int)parms[0]);
              stmt.SetParameter(2, (short)parms[1]);
              stmt.SetParameter(3, (int)parms[2]);
              return;
           case 5 :
              stmt.SetParameter(1, (int)parms[0]);
              stmt.SetParameter(2, (int)parms[1]);
              stmt.SetParameter(3, (short)parms[2]);
              return;
           case 6 :
              stmt.SetParameter(1, (int)parms[0]);
              stmt.SetParameter(2, (short)parms[1]);
              return;
           case 7 :
              stmt.SetParameter(1, (int)parms[0]);
              return;
     }
  }

}

}
