/*
               File: SmoothNavMenuDataDP
        Description: Smooth Nav Menu Data DP
             Author: GeneXus C# Generator version 16_0_0-127771
       Generated on: 1/5/2019 13:54:58.44
       Program type: Callable routine
          Main DBMS: SQL Server
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class smoothnavmenudatadp : GXProcedure
   {
      public smoothnavmenudatadp( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         IsMain = true;
         context.SetDefaultTheme("Carmine");
      }

      public smoothnavmenudatadp( IGxContext context )
      {
         this.context = context;
         IsMain = false;
      }

      public void release( )
      {
      }

      public void execute( out GXBaseCollection<SdtSmoothNavMenuData_SmoothNavMenuItem> aP0_Gxm2rootcol )
      {
         this.Gxm2rootcol = new GXBaseCollection<SdtSmoothNavMenuData_SmoothNavMenuItem>( context, "SmoothNavMenuItem", "SAPWEB08") ;
         initialize();
         executePrivate();
         aP0_Gxm2rootcol=this.Gxm2rootcol;
      }

      public GXBaseCollection<SdtSmoothNavMenuData_SmoothNavMenuItem> executeUdp( )
      {
         this.Gxm2rootcol = new GXBaseCollection<SdtSmoothNavMenuData_SmoothNavMenuItem>( context, "SmoothNavMenuItem", "SAPWEB08") ;
         initialize();
         executePrivate();
         aP0_Gxm2rootcol=this.Gxm2rootcol;
         return Gxm2rootcol ;
      }

      public void executeSubmit( out GXBaseCollection<SdtSmoothNavMenuData_SmoothNavMenuItem> aP0_Gxm2rootcol )
      {
         smoothnavmenudatadp objsmoothnavmenudatadp;
         objsmoothnavmenudatadp = new smoothnavmenudatadp();
         objsmoothnavmenudatadp.Gxm2rootcol = new GXBaseCollection<SdtSmoothNavMenuData_SmoothNavMenuItem>( context, "SmoothNavMenuItem", "SAPWEB08") ;
         objsmoothnavmenudatadp.context.SetSubmitInitialConfig(context);
         objsmoothnavmenudatadp.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objsmoothnavmenudatadp);
         aP0_Gxm2rootcol=this.Gxm2rootcol;
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((smoothnavmenudatadp)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Design", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         Gxm1smoothnavmenudata = new SdtSmoothNavMenuData_SmoothNavMenuItem(context);
         Gxm2rootcol.Add(Gxm1smoothnavmenudata, 0);
         Gxm1smoothnavmenudata.gxTpr_Id = "Programa";
         Gxm1smoothnavmenudata.gxTpr_Title = "Programa";
         Gxm1smoothnavmenudata.gxTpr_Description = "Programas";
         Gxm1smoothnavmenudata.gxTpr_Link = "http://localhost:8083/SAPWEB08.NetEnvironment/wwprograma.aspx";
         Gxm1smoothnavmenudata = new SdtSmoothNavMenuData_SmoothNavMenuItem(context);
         Gxm2rootcol.Add(Gxm1smoothnavmenudata, 0);
         Gxm1smoothnavmenudata.gxTpr_Id = "Proyecto";
         Gxm1smoothnavmenudata.gxTpr_Title = "Proyecto";
         Gxm1smoothnavmenudata.gxTpr_Description = "Crear Proyectos y asociar paises y comunidades";
         Gxm1smoothnavmenudata.gxTpr_Link = "http://localhost:8083/SAPWEB08.NetEnvironment/wwproyecto.aspx";
         Gxm1smoothnavmenudata = new SdtSmoothNavMenuData_SmoothNavMenuItem(context);
         Gxm2rootcol.Add(Gxm1smoothnavmenudata, 0);
         Gxm1smoothnavmenudata.gxTpr_Id = "Indicadores";
         Gxm1smoothnavmenudata.gxTpr_Title = "Indicadores";
         Gxm1smoothnavmenudata.gxTpr_Description = "Asociar indicadores a los proyectos";
         Gxm1smoothnavmenudata.gxTpr_Link = "http://localhost:8083/SAPWEB08.NetEnvironment/wwindicador.aspx";
         Gxm1smoothnavmenudata = new SdtSmoothNavMenuData_SmoothNavMenuItem(context);
         Gxm2rootcol.Add(Gxm1smoothnavmenudata, 0);
         Gxm1smoothnavmenudata.gxTpr_Id = "Actividades";
         Gxm1smoothnavmenudata.gxTpr_Title = "Actividades";
         Gxm1smoothnavmenudata.gxTpr_Description = "Asocie actividades a los indicadores";
         Gxm1smoothnavmenudata.gxTpr_Link = "http://localhost:8083/SAPWEB08.NetEnvironment/wwactividad.aspx";
         Gxm1smoothnavmenudata = new SdtSmoothNavMenuData_SmoothNavMenuItem(context);
         Gxm2rootcol.Add(Gxm1smoothnavmenudata, 0);
         Gxm1smoothnavmenudata.gxTpr_Id = "Planes de Acci�n";
         Gxm1smoothnavmenudata.gxTpr_Title = "Planes de Acci�n";
         Gxm1smoothnavmenudata.gxTpr_Description = "Planifique las actividades a realizar";
         Gxm1smoothnavmenudata.gxTpr_Link = "http://localhost:8083/SAPWEB08.NetEnvironment/wwplan.aspx";
         Gxm1smoothnavmenudata = new SdtSmoothNavMenuData_SmoothNavMenuItem(context);
         Gxm2rootcol.Add(Gxm1smoothnavmenudata, 0);
         Gxm1smoothnavmenudata.gxTpr_Id = "Informes";
         Gxm1smoothnavmenudata.gxTpr_Title = "Informes";
         Gxm1smoothnavmenudata.gxTpr_Description = "Informes de las actividades realizadas";
         Gxm1smoothnavmenudata.gxTpr_Link = "http://localhost:8083/SAPWEB08.NetEnvironment/wwinforme_actividad.aspx";
         Gxm1smoothnavmenudata = new SdtSmoothNavMenuData_SmoothNavMenuItem(context);
         Gxm2rootcol.Add(Gxm1smoothnavmenudata, 0);
         Gxm1smoothnavmenudata.gxTpr_Id = "Reportes";
         Gxm1smoothnavmenudata.gxTpr_Title = "Reportes";
         Gxm1smoothnavmenudata.gxTpr_Description = "Reportes mensuales, ITT y todas las variables";
         Gxm1smoothnavmenudata.gxTpr_Link = "";
         Gxm3smoothnavmenudata_items = new SdtSmoothNavMenuData_SmoothNavMenuItem(context);
         Gxm1smoothnavmenudata.gxTpr_Items.Add(Gxm3smoothnavmenudata_items, 0);
         Gxm3smoothnavmenudata_items.gxTpr_Id = "Mensual";
         Gxm3smoothnavmenudata_items.gxTpr_Title = "Mensual";
         Gxm3smoothnavmenudata_items.gxTpr_Description = "Reporte Mensual del Proyecto relacionado a su perfil";
         Gxm3smoothnavmenudata_items.gxTpr_Link = "#";
         Gxm3smoothnavmenudata_items = new SdtSmoothNavMenuData_SmoothNavMenuItem(context);
         Gxm1smoothnavmenudata.gxTpr_Items.Add(Gxm3smoothnavmenudata_items, 0);
         Gxm3smoothnavmenudata_items.gxTpr_Id = "ITT";
         Gxm3smoothnavmenudata_items.gxTpr_Title = "ITT";
         Gxm3smoothnavmenudata_items.gxTpr_Description = "Reporte ITT del Proyecto relacionado a su perfil";
         Gxm3smoothnavmenudata_items.gxTpr_Link = "#";
         Gxm3smoothnavmenudata_items = new SdtSmoothNavMenuData_SmoothNavMenuItem(context);
         Gxm1smoothnavmenudata.gxTpr_Items.Add(Gxm3smoothnavmenudata_items, 0);
         Gxm3smoothnavmenudata_items.gxTpr_Id = "Todas las Variables";
         Gxm3smoothnavmenudata_items.gxTpr_Title = "Todas las Variables";
         Gxm3smoothnavmenudata_items.gxTpr_Description = "Reporte *Todas las Variables* del Proyecto relacionado a su perfil";
         Gxm3smoothnavmenudata_items.gxTpr_Link = "#";
         this.cleanup();
      }

      public override void cleanup( )
      {
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         Gxm1smoothnavmenudata = new SdtSmoothNavMenuData_SmoothNavMenuItem(context);
         Gxm3smoothnavmenudata_items = new SdtSmoothNavMenuData_SmoothNavMenuItem(context);
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private GXBaseCollection<SdtSmoothNavMenuData_SmoothNavMenuItem> aP0_Gxm2rootcol ;
      private GXBaseCollection<SdtSmoothNavMenuData_SmoothNavMenuItem> Gxm2rootcol ;
      private SdtSmoothNavMenuData_SmoothNavMenuItem Gxm1smoothnavmenudata ;
      private SdtSmoothNavMenuData_SmoothNavMenuItem Gxm3smoothnavmenudata_items ;
   }

}
