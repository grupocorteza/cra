/*
				   File: type_SdtSmoothNavMenuData_SmoothNavMenuItem
			Description: SmoothNavMenuData
				 Author: Nemo for C# version 16.0.0.127771
		   Generated on: 1/5/2019 11:47:14 AM
		   Program type: Callable routine
			  Main DBMS: SQL Server
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using GeneXus.Http.Server;
using System.Reflection;
using System.Xml.Serialization;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Web.Services.Protocols;


namespace GeneXus.Programs{
	[XmlSerializerFormat]
	[XmlRoot(ElementName="SmoothNavMenuItem")]
	[XmlType(TypeName="SmoothNavMenuItem" , Namespace="SAPWEB08" )]
	[Serializable]
	public class SdtSmoothNavMenuData_SmoothNavMenuItem : GxUserType
	{
		public SdtSmoothNavMenuData_SmoothNavMenuItem( )
		{
			/* Constructor for serialization */
			gxTv_SdtSmoothNavMenuData_SmoothNavMenuItem_Id = "";

			gxTv_SdtSmoothNavMenuData_SmoothNavMenuItem_Title = "";

			gxTv_SdtSmoothNavMenuData_SmoothNavMenuItem_Description = "";

			gxTv_SdtSmoothNavMenuData_SmoothNavMenuItem_Link = "";

			gxTv_SdtSmoothNavMenuData_SmoothNavMenuItem_Linktarget = "";

		}

		public SdtSmoothNavMenuData_SmoothNavMenuItem(IGxContext context)
		{
			this.context = context;
			initialize();
		}

		#region Json
		private static Hashtable mapper;
		public override String JsonMap(String value)
		{
			if (mapper == null)
			{
				mapper = new Hashtable();
			}
			return (String)mapper[value]; ;
		}

		public override void ToJSON()
		{
			ToJSON(true) ;
			return;
		}

		public override void ToJSON(bool includeState)
		{
			AddObjectProperty("Id", gxTpr_Id, false);
			AddObjectProperty("Title", gxTpr_Title, false);
			AddObjectProperty("Description", gxTpr_Description, false);
			AddObjectProperty("Link", gxTpr_Link, false);
			AddObjectProperty("LinkTarget", gxTpr_Linktarget, false);
			AddObjectProperty("Items", gxTv_SdtSmoothNavMenuData_SmoothNavMenuItem_Items, false);  
			return;
		}
		#endregion

		#region Properties

		[SoapElement(ElementName="Id")]
		[XmlElement(ElementName="Id")]
		public String gxTpr_Id
		{
			get { 
				return gxTv_SdtSmoothNavMenuData_SmoothNavMenuItem_Id; 
			}
			set { 
				gxTv_SdtSmoothNavMenuData_SmoothNavMenuItem_Id = value;
				SetDirty("Id");
			}
		}


		[SoapElement(ElementName="Title")]
		[XmlElement(ElementName="Title")]
		public String gxTpr_Title
		{
			get { 
				return gxTv_SdtSmoothNavMenuData_SmoothNavMenuItem_Title; 
			}
			set { 
				gxTv_SdtSmoothNavMenuData_SmoothNavMenuItem_Title = value;
				SetDirty("Title");
			}
		}


		[SoapElement(ElementName="Description")]
		[XmlElement(ElementName="Description")]
		public String gxTpr_Description
		{
			get { 
				return gxTv_SdtSmoothNavMenuData_SmoothNavMenuItem_Description; 
			}
			set { 
				gxTv_SdtSmoothNavMenuData_SmoothNavMenuItem_Description = value;
				SetDirty("Description");
			}
		}


		[SoapElement(ElementName="Link")]
		[XmlElement(ElementName="Link")]
		public String gxTpr_Link
		{
			get { 
				return gxTv_SdtSmoothNavMenuData_SmoothNavMenuItem_Link; 
			}
			set { 
				gxTv_SdtSmoothNavMenuData_SmoothNavMenuItem_Link = value;
				SetDirty("Link");
			}
		}


		[SoapElement(ElementName="LinkTarget")]
		[XmlElement(ElementName="LinkTarget")]
		public String gxTpr_Linktarget
		{
			get { 
				return gxTv_SdtSmoothNavMenuData_SmoothNavMenuItem_Linktarget; 
			}
			set { 
				gxTv_SdtSmoothNavMenuData_SmoothNavMenuItem_Linktarget = value;
				SetDirty("Linktarget");
			}
		}


		[SoapElement(ElementName="Items" )]
		[XmlArray(ElementName="Items"  )]
		[XmlArrayItemAttribute(ElementName="SmoothNavMenuItem" , IsNullable=false )]
		public GXBaseCollection<GeneXus.Programs.SdtSmoothNavMenuData_SmoothNavMenuItem> gxTpr_Items_GXBaseCollection
		{
			get {
				if ( gxTv_SdtSmoothNavMenuData_SmoothNavMenuItem_Items == null )
				{
					gxTv_SdtSmoothNavMenuData_SmoothNavMenuItem_Items = new GXBaseCollection<GeneXus.Programs.SdtSmoothNavMenuData_SmoothNavMenuItem>( context, "SmoothNavMenuData", "");
				}
				return gxTv_SdtSmoothNavMenuData_SmoothNavMenuItem_Items;
			}
			set {
				if ( gxTv_SdtSmoothNavMenuData_SmoothNavMenuItem_Items == null )
				{
					gxTv_SdtSmoothNavMenuData_SmoothNavMenuItem_Items = new GXBaseCollection<GeneXus.Programs.SdtSmoothNavMenuData_SmoothNavMenuItem>( context, "SmoothNavMenuData", "");
				}
				gxTv_SdtSmoothNavMenuData_SmoothNavMenuItem_Items = value;
			}
		}

		[SoapIgnore]
		[XmlIgnore]
		public GXBaseCollection<GeneXus.Programs.SdtSmoothNavMenuData_SmoothNavMenuItem> gxTpr_Items
		{
			get {
				if ( gxTv_SdtSmoothNavMenuData_SmoothNavMenuItem_Items == null )
				{
					gxTv_SdtSmoothNavMenuData_SmoothNavMenuItem_Items = new GXBaseCollection<GeneXus.Programs.SdtSmoothNavMenuData_SmoothNavMenuItem>( context, "SmoothNavMenuData", "");
				}
				return gxTv_SdtSmoothNavMenuData_SmoothNavMenuItem_Items ;
			}
			set {
				gxTv_SdtSmoothNavMenuData_SmoothNavMenuItem_Items = value;
				SetDirty("Items");
			}
		}

		public void gxTv_SdtSmoothNavMenuData_SmoothNavMenuItem_Items_SetNull()
		{
			gxTv_SdtSmoothNavMenuData_SmoothNavMenuItem_Items = null;
			return  ;
		}

		public bool gxTv_SdtSmoothNavMenuData_SmoothNavMenuItem_Items_IsNull()
		{
			if (gxTv_SdtSmoothNavMenuData_SmoothNavMenuItem_Items == null)
			{
				return true ;
			}
			return false ;
		}



		#endregion

		#region Initialization

		public void initialize( )
		{
			gxTv_SdtSmoothNavMenuData_SmoothNavMenuItem_Id = "";
			gxTv_SdtSmoothNavMenuData_SmoothNavMenuItem_Title = "";
			gxTv_SdtSmoothNavMenuData_SmoothNavMenuItem_Description = "";
			gxTv_SdtSmoothNavMenuData_SmoothNavMenuItem_Link = "";
			gxTv_SdtSmoothNavMenuData_SmoothNavMenuItem_Linktarget = "";

			return  ;
		}



		#endregion

		#region Declaration

		protected String gxTv_SdtSmoothNavMenuData_SmoothNavMenuItem_Id;
		protected String gxTv_SdtSmoothNavMenuData_SmoothNavMenuItem_Title;
		protected String gxTv_SdtSmoothNavMenuData_SmoothNavMenuItem_Description;
		protected String gxTv_SdtSmoothNavMenuData_SmoothNavMenuItem_Link;
		protected String gxTv_SdtSmoothNavMenuData_SmoothNavMenuItem_Linktarget;
		protected GXBaseCollection<GeneXus.Programs.SdtSmoothNavMenuData_SmoothNavMenuItem> gxTv_SdtSmoothNavMenuData_SmoothNavMenuItem_Items = null; 


		#endregion
	}
	#region Rest interface
	[DataContract(Name=@"SmoothNavMenuItem", Namespace="SAPWEB08")]
	public class SdtSmoothNavMenuData_SmoothNavMenuItem_RESTInterface : GxGenericCollectionItem<SdtSmoothNavMenuData_SmoothNavMenuItem>, System.Web.SessionState.IRequiresSessionState
	{
		public SdtSmoothNavMenuData_SmoothNavMenuItem_RESTInterface( ) : base()
		{
		}

		public SdtSmoothNavMenuData_SmoothNavMenuItem_RESTInterface( SdtSmoothNavMenuData_SmoothNavMenuItem psdt ) : base(psdt)
		{
		}

		#region Rest Properties
		[DataMember(Name="Id", Order=0)]
		public String gxTpr_Id
		{
			get { 
				return StringUtil.RTrim( sdt.gxTpr_Id);
			}
			set { 
				sdt.gxTpr_Id = value;
			}
		}

		[DataMember(Name="Title", Order=1)]
		public String gxTpr_Title
		{
			get { 
				return StringUtil.RTrim( sdt.gxTpr_Title);
			}
			set { 
				sdt.gxTpr_Title = value;
			}
		}

		[DataMember(Name="Description", Order=2)]
		public String gxTpr_Description
		{
			get { 
				return StringUtil.RTrim( sdt.gxTpr_Description);
			}
			set { 
				sdt.gxTpr_Description = value;
			}
		}

		[DataMember(Name="Link", Order=3)]
		public String gxTpr_Link
		{
			get { 
				return StringUtil.RTrim( sdt.gxTpr_Link);
			}
			set { 
				sdt.gxTpr_Link = value;
			}
		}

		[DataMember(Name="LinkTarget", Order=4)]
		public String gxTpr_Linktarget
		{
			get { 
				return StringUtil.RTrim( sdt.gxTpr_Linktarget);
			}
			set { 
				sdt.gxTpr_Linktarget = value;
			}
		}

		[DataMember(Name="Items", Order=5)]
		public  GxGenericCollection<GeneXus.Programs.SdtSmoothNavMenuData_SmoothNavMenuItem_RESTInterface> gxTpr_Items
		{
			get { 
				return new GxGenericCollection<GeneXus.Programs.SdtSmoothNavMenuData_SmoothNavMenuItem_RESTInterface>(sdt.gxTpr_Items);
			}
			set { 
				value.LoadCollection(sdt.gxTpr_Items);
			}
		}


		#endregion

		public SdtSmoothNavMenuData_SmoothNavMenuItem sdt
		{
			get { 
				return (SdtSmoothNavMenuData_SmoothNavMenuItem)Sdt;
			}
			set { 
				Sdt = value;
			}
		}

		[OnDeserializing]
		void checkSdt( StreamingContext ctx )
		{
			if ( sdt == null )
			{
				sdt = new SdtSmoothNavMenuData_SmoothNavMenuItem() ;
			}
		}
	}
	#endregion
}