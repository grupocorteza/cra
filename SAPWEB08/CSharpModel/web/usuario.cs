/*
               File: Usuario
        Description: Usuario
             Author: GeneXus C# Generator version 16_0_0-127771
       Generated on: 1/4/2019 19:2:11.19
       Program type: Callable routine
          Main DBMS: SQL Server
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Xml.Serialization;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class usuario : GXDataArea, System.Web.SessionState.IRequiresSessionState
   {
      protected void INITENV( )
      {
         if ( GxWebError != 0 )
         {
            return  ;
         }
      }

      protected void INITTRN( )
      {
         initialize_properties( ) ;
         entryPointCalled = false;
         gxfirstwebparm = GetNextPar( );
         gxfirstwebparm_bkp = gxfirstwebparm;
         gxfirstwebparm = DecryptAjaxCall( gxfirstwebparm);
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         if ( StringUtil.StrCmp(gxfirstwebparm, "dyncall") == 0 )
         {
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            dyncall( GetNextPar( )) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxExecAct_"+"gxLoad_12") == 0 )
         {
            A15Paisid = (int)(NumberUtil.Val( GetNextPar( ), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A15Paisid", StringUtil.LTrim( StringUtil.Str( (decimal)(A15Paisid), 9, 0)));
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxLoad_12( A15Paisid) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxExecAct_"+"gxLoad_13") == 0 )
         {
            A20Puestoid = (int)(NumberUtil.Val( GetNextPar( ), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A20Puestoid", StringUtil.LTrim( StringUtil.Str( (decimal)(A20Puestoid), 9, 0)));
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxLoad_13( A20Puestoid) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxEvt") == 0 )
         {
            setAjaxEventMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxfirstwebparm = GetNextPar( );
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxfullajaxEvt") == 0 )
         {
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxfirstwebparm = GetNextPar( );
         }
         else
         {
            if ( ! IsValidAjaxCall( false) )
            {
               GxWebError = 1;
               return  ;
            }
            gxfirstwebparm = gxfirstwebparm_bkp;
         }
         if ( ! entryPointCalled && ! ( isAjaxCallMode( ) || isFullAjaxMode( ) ) )
         {
            Gx_mode = gxfirstwebparm;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!")), context));
            if ( StringUtil.StrCmp(gxfirstwebparm, "viewer") != 0 )
            {
               AV7Usuarioid = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV7Usuarioid", StringUtil.LTrim( StringUtil.Str( (decimal)(AV7Usuarioid), 9, 0)));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vUSUARIOID", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV7Usuarioid), "ZZZZZZZZ9"), context));
            }
         }
         if ( context.isSpaRequest( ) )
         {
            enableJsOutput();
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.GetCookie( "GX_SESSION_ID"))) )
         {
            gxcookieaux = context.SetCookie( "GX_SESSION_ID", Encrypt64( Crypto.GetEncryptionKey( ), Crypto.GetServerKey( )), "", (DateTime)(DateTime.MinValue), "", 0);
         }
         GXKey = Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
         toggleJsOutput = isJsOutputEnabled( );
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         init_web_controls( ) ;
         if ( toggleJsOutput )
         {
            if ( context.isSpaRequest( ) )
            {
               enableJsOutput();
            }
         }
         if ( ! context.isSpaRequest( ) )
         {
            Form.Meta.addItem("generator", "GeneXus C# 16_0_0-127771", 0) ;
            Form.Meta.addItem("description", "Usuario", 0) ;
         }
         context.wjLoc = "";
         context.nUserReturn = 0;
         context.wbHandled = 0;
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
         }
         GX_FocusControl = edtusuario_Internalname;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         wbErr = false;
         context.SetDefaultTheme("Carmine");
         if ( ! context.IsLocalStorageSupported( ) )
         {
            context.PushCurrentUrl();
         }
      }

      public usuario( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsDataStore1 = context.GetDataStore("DataStore1");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("Carmine");
      }

      public usuario( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsDataStore1 = context.GetDataStore("DataStore1");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( String aP0_Gx_mode ,
                           int aP1_Usuarioid )
      {
         this.Gx_mode = aP0_Gx_mode;
         this.AV7Usuarioid = aP1_Usuarioid;
         executePrivate();
      }

      void executePrivate( )
      {
         isStatic = false;
         webExecute();
      }

      protected override void createObjects( )
      {
      }

      public override void webExecute( )
      {
         if ( initialized == 0 )
         {
            createObjects();
            initialize();
         }
         INITENV( ) ;
         INITTRN( ) ;
         if ( ( GxWebError == 0 ) && ! isAjaxCallMode( ) )
         {
            MasterPageObj = (GXMasterPage) ClassLoader.GetInstance("rwdmasterpage", "GeneXus.Programs.rwdmasterpage", new Object[] {new GxContext( context.handle, context.DataStores, context.HttpContext)});
            MasterPageObj.setDataArea(this,false);
            ValidateSpaRequest();
            MasterPageObj.webExecute();
            if ( ( GxWebError == 0 ) && context.isAjaxRequest( ) )
            {
               enableOutput();
               if ( ! context.isAjaxRequest( ) )
               {
                  context.GX_webresponse.AppendHeader("Cache-Control", "no-store");
               }
               if ( ! context.WillRedirect( ) )
               {
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               }
               else
               {
                  if ( context.isAjaxRequest( ) )
                  {
                     disableOutput();
                  }
                  RenderHtmlHeaders( ) ;
                  context.Redirect( context.wjLoc );
                  context.DispatchAjaxCommands();
               }
            }
         }
         this.cleanup();
      }

      protected void fix_multi_value_controls( )
      {
      }

      protected void Draw( )
      {
         if ( context.isAjaxRequest( ) )
         {
            disableOutput();
         }
         if ( ! GxWebStd.gx_redirect( context) )
         {
            disable_std_buttons( ) ;
            enableDisable( ) ;
            set_caption( ) ;
            /* Form start */
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "Section", "left", "top", " "+"data-gx-base-lib=\"bootstrapv3\""+" "+"data-abstract-form"+" ", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, divMaintable_Internalname, 1, 0, "px", 0, "px", "WWAdvancedContainer", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12 col-sm-8 col-sm-offset-2", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, divTitlecontainer_Internalname, 1, 0, "px", 0, "px", "TableTop", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12", "left", "top", "", "", "div");
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTitle_Internalname, "Usuario", "", "", lblTitle_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "Title", 0, "", 1, 1, 0, "HLP_Usuario.htm");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12", "left", "top", "", "", "div");
            ClassString = "ErrorViewer";
            StyleString = "";
            GxWebStd.gx_msg_list( context, "", context.GX_msglist.DisplayMode, StyleString, ClassString, "", "false");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12 col-sm-8 col-sm-offset-2", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, divFormcontainer_Internalname, 1, 0, "px", 0, "px", "FormContainer", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, divToolbarcell_Internalname, 1, 0, "px", 0, "px", "col-xs-12 col-sm-9 col-sm-offset-3 ToolbarCellClass", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "gx-action-group ActionGroup", "left", "top", " "+"data-gx-actiongroup-type=\"toolbar\""+" ", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "btn-group", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "gx-button", "left", "top", "", "", "div");
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 21,'',false,'',0)\"";
            ClassString = "BtnFirst";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtn_first_Internalname, "", "", bttBtn_first_Jsonclick, 5, "", "", StyleString, ClassString, bttBtn_first_Visible, 0, "standard", "'"+""+"'"+",false,"+"'"+"EFIRST."+"'", TempTags, "", context.GetButtonType( ), "HLP_Usuario.htm");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "gx-button", "left", "top", "", "", "div");
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 23,'',false,'',0)\"";
            ClassString = "BtnPrevious";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtn_previous_Internalname, "", "", bttBtn_previous_Jsonclick, 5, "", "", StyleString, ClassString, bttBtn_previous_Visible, 0, "standard", "'"+""+"'"+",false,"+"'"+"EPREVIOUS."+"'", TempTags, "", context.GetButtonType( ), "HLP_Usuario.htm");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "gx-button", "left", "top", "", "", "div");
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 25,'',false,'',0)\"";
            ClassString = "BtnNext";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtn_next_Internalname, "", "", bttBtn_next_Jsonclick, 5, "", "", StyleString, ClassString, bttBtn_next_Visible, 0, "standard", "'"+""+"'"+",false,"+"'"+"ENEXT."+"'", TempTags, "", context.GetButtonType( ), "HLP_Usuario.htm");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "gx-button", "left", "top", "", "", "div");
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 27,'',false,'',0)\"";
            ClassString = "BtnLast";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtn_last_Internalname, "", "", bttBtn_last_Jsonclick, 5, "", "", StyleString, ClassString, bttBtn_last_Visible, 0, "standard", "'"+""+"'"+",false,"+"'"+"ELAST."+"'", TempTags, "", context.GetButtonType( ), "HLP_Usuario.htm");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "gx-button", "left", "top", "", "", "div");
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 29,'',false,'',0)\"";
            ClassString = "BtnSelect";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtn_select_Internalname, "", "Select", bttBtn_select_Jsonclick, 5, "Select", "", StyleString, ClassString, bttBtn_select_Visible, 0, "standard", "'"+""+"'"+",false,"+"'"+"ESELECT."+"'", TempTags, "", 2, "HLP_Usuario.htm");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12 FormCellAdvanced", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "form-group gx-form-group", "left", "top", ""+" data-gx-for=\""+edtUsuarioid_Internalname+"\"", "", "div");
            /* Attribute/Variable Label */
            GxWebStd.gx_label_element( context, edtUsuarioid_Internalname, "id", "col-sm-3 AttributeLabel", 1, true);
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-sm-9 gx-attribute", "left", "top", "", "", "div");
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtUsuarioid_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A17Usuarioid), 9, 0, ".", "")), ((edtUsuarioid_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( (decimal)(A17Usuarioid), "ZZZZZZZZ9")) : context.localUtil.Format( (decimal)(A17Usuarioid), "ZZZZZZZZ9")), "", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtUsuarioid_Jsonclick, 0, "Attribute", "", "", "", "", 1, edtUsuarioid_Enabled, 0, "number", "1", 9, "chr", 1, "row", 9, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_Usuario.htm");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12 FormCell", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "form-group gx-form-group", "left", "top", ""+" data-gx-for=\""+edtusuario_Internalname+"\"", "", "div");
            /* Attribute/Variable Label */
            GxWebStd.gx_label_element( context, edtusuario_Internalname, "usuario", "col-sm-3 AttributeLabel", 1, true);
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-sm-9 gx-attribute", "left", "top", "", "", "div");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 39,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtusuario_Internalname, A120usuario, StringUtil.RTrim( context.localUtil.Format( A120usuario, "")), TempTags+" onchange=\""+""+";gx.evt.onchange(this, event)\" "+" onblur=\""+";gx.evt.onblur(this,39);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtusuario_Jsonclick, 0, "Attribute", "", "", "", "", 1, edtusuario_Enabled, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_Usuario.htm");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12 FormCell", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "form-group gx-form-group", "left", "top", ""+" data-gx-for=\""+edtpassword_Internalname+"\"", "", "div");
            /* Attribute/Variable Label */
            GxWebStd.gx_label_element( context, edtpassword_Internalname, "password", "col-sm-3 AttributeLabel", 1, true);
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-sm-9 gx-attribute", "left", "top", "", "", "div");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 44,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtpassword_Internalname, A121password, StringUtil.RTrim( context.localUtil.Format( A121password, "")), TempTags+" onchange=\""+""+";gx.evt.onchange(this, event)\" "+" onblur=\""+";gx.evt.onblur(this,44);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtpassword_Jsonclick, 0, "Attribute", "", "", "", "", 1, edtpassword_Enabled, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_Usuario.htm");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12 FormCell", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "form-group gx-form-group", "left", "top", ""+" data-gx-for=\""+edtactivo_Internalname+"\"", "", "div");
            /* Attribute/Variable Label */
            GxWebStd.gx_label_element( context, edtactivo_Internalname, "activo", "col-sm-3 AttributeLabel", 1, true);
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-sm-9 gx-attribute", "left", "top", "", "", "div");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 49,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtactivo_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A122activo), 1, 0, ".", "")), ((edtactivo_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( (decimal)(A122activo), "9")) : context.localUtil.Format( (decimal)(A122activo), "9")), TempTags+" onchange=\""+"gx.num.valid_integer( this,',');"+";gx.evt.onchange(this, event)\" "+" onblur=\""+";gx.evt.onblur(this,49);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtactivo_Jsonclick, 0, "Attribute", "", "", "", "", 1, edtactivo_Enabled, 0, "number", "1", 1, "chr", 1, "row", 1, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_Usuario.htm");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12 FormCell", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "form-group gx-form-group", "left", "top", ""+" data-gx-for=\""+edtPaisid_Internalname+"\"", "", "div");
            /* Attribute/Variable Label */
            GxWebStd.gx_label_element( context, edtPaisid_Internalname, "Pa�s", "col-sm-3 AttributeLabel", 1, true);
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-sm-9 gx-attribute", "left", "top", "", "", "div");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 54,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtPaisid_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A15Paisid), 9, 0, ".", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(A15Paisid), "ZZZZZZZZ9")), TempTags+" onchange=\""+"gx.num.valid_integer( this,',');"+";gx.evt.onchange(this, event)\" "+" onblur=\""+";gx.evt.onblur(this,54);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtPaisid_Jsonclick, 0, "Attribute", "", "", "", "", 1, edtPaisid_Enabled, 1, "number", "1", 9, "chr", 1, "row", 9, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_Usuario.htm");
            /* Static images/pictures */
            ClassString = "gx-prompt Image";
            StyleString = "";
            sImgUrl = (String)(context.GetImagePath( "prompt.gif", "", context.GetTheme( )));
            GxWebStd.gx_bitmap( context, imgprompt_15_Internalname, sImgUrl, imgprompt_15_Link, "", "", context.GetTheme( ), imgprompt_15_Visible, 1, "", "", 0, 0, 0, "", 0, "", 0, 0, 0, "", "", StyleString, ClassString, "", "", "", "", "", "", "", 1, false, false, context.GetImageSrcSet( sImgUrl), "HLP_Usuario.htm");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12 FormCell", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "form-group gx-form-group", "left", "top", ""+" data-gx-for=\""+edtPuestoid_Internalname+"\"", "", "div");
            /* Attribute/Variable Label */
            GxWebStd.gx_label_element( context, edtPuestoid_Internalname, "Puestoid", "col-sm-3 AttributeLabel", 1, true);
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-sm-9 gx-attribute", "left", "top", "", "", "div");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 59,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtPuestoid_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A20Puestoid), 9, 0, ".", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(A20Puestoid), "ZZZZZZZZ9")), TempTags+" onchange=\""+"gx.num.valid_integer( this,',');"+";gx.evt.onchange(this, event)\" "+" onblur=\""+";gx.evt.onblur(this,59);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtPuestoid_Jsonclick, 0, "Attribute", "", "", "", "", 1, edtPuestoid_Enabled, 1, "number", "1", 9, "chr", 1, "row", 9, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_Usuario.htm");
            /* Static images/pictures */
            ClassString = "gx-prompt Image";
            StyleString = "";
            sImgUrl = (String)(context.GetImagePath( "prompt.gif", "", context.GetTheme( )));
            GxWebStd.gx_bitmap( context, imgprompt_20_Internalname, sImgUrl, imgprompt_20_Link, "", "", context.GetTheme( ), imgprompt_20_Visible, 1, "", "", 0, 0, 0, "", 0, "", 0, 0, 0, "", "", StyleString, ClassString, "", "", "", "", "", "", "", 1, false, false, context.GetImageSrcSet( sImgUrl), "HLP_Usuario.htm");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12 FormCell", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "form-group gx-form-group", "left", "top", ""+" data-gx-for=\""+edtUsuariocorreo_Internalname+"\"", "", "div");
            /* Attribute/Variable Label */
            GxWebStd.gx_label_element( context, edtUsuariocorreo_Internalname, "correo", "col-sm-3 AttributeLabel", 1, true);
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-sm-9 gx-attribute", "left", "top", "", "", "div");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 64,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtUsuariocorreo_Internalname, A123Usuariocorreo, StringUtil.RTrim( context.localUtil.Format( A123Usuariocorreo, "")), TempTags+" onchange=\""+""+";gx.evt.onchange(this, event)\" "+" onblur=\""+";gx.evt.onblur(this,64);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtUsuariocorreo_Jsonclick, 0, "Attribute", "", "", "", "", 1, edtUsuariocorreo_Enabled, 0, "text", "", 80, "chr", 1, "row", 100, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_Usuario.htm");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12", "Center", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "gx-action-group Confirm", "left", "top", " "+"data-gx-actiongroup-type=\"toolbar\""+" ", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "gx-button", "left", "top", "", "", "div");
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 69,'',false,'',0)\"";
            ClassString = "BtnEnter";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtn_enter_Internalname, "", "Confirm", bttBtn_enter_Jsonclick, 5, "Confirm", "", StyleString, ClassString, bttBtn_enter_Visible, bttBtn_enter_Enabled, "standard", "'"+""+"'"+",false,"+"'"+"EENTER."+"'", TempTags, "", context.GetButtonType( ), "HLP_Usuario.htm");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "gx-button", "left", "top", "", "", "div");
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 71,'',false,'',0)\"";
            ClassString = "BtnCancel";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtn_cancel_Internalname, "", "Cancel", bttBtn_cancel_Jsonclick, 1, "Cancel", "", StyleString, ClassString, bttBtn_cancel_Visible, 1, "standard", "'"+""+"'"+",false,"+"'"+"ECANCEL."+"'", TempTags, "", context.GetButtonType( ), "HLP_Usuario.htm");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "gx-button", "left", "top", "", "", "div");
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 73,'',false,'',0)\"";
            ClassString = "BtnDelete";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtn_delete_Internalname, "", "Delete", bttBtn_delete_Jsonclick, 5, "Delete", "", StyleString, ClassString, bttBtn_delete_Visible, bttBtn_delete_Enabled, "standard", "'"+""+"'"+",false,"+"'"+"EDELETE."+"'", TempTags, "", context.GetButtonType( ), "HLP_Usuario.htm");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "Center", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            fix_multi_value_controls( ) ;
         }
         /* Execute Exit event if defined. */
      }

      protected void UserMain( )
      {
         standaloneStartup( ) ;
      }

      protected void UserMainFullajax( )
      {
         INITENV( ) ;
         INITTRN( ) ;
         UserMain( ) ;
         Draw( ) ;
         SendCloseFormHiddens( ) ;
      }

      protected void standaloneStartup( )
      {
         standaloneStartupServer( ) ;
         disable_std_buttons( ) ;
         enableDisable( ) ;
         Process( ) ;
      }

      protected void standaloneStartupServer( )
      {
         /* Execute Start event if defined. */
         context.wbGlbDoneStart = 0;
         /* Execute user event: Start */
         E110C2 ();
         context.wbGlbDoneStart = 1;
         assign_properties_default( ) ;
         if ( AnyError == 0 )
         {
            if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
            {
               /* Read saved SDTs. */
               /* Read variables values. */
               A17Usuarioid = (int)(context.localUtil.CToN( cgiGet( edtUsuarioid_Internalname), ".", ","));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A17Usuarioid", StringUtil.LTrim( StringUtil.Str( (decimal)(A17Usuarioid), 9, 0)));
               A120usuario = cgiGet( edtusuario_Internalname);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A120usuario", A120usuario);
               A121password = cgiGet( edtpassword_Internalname);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A121password", A121password);
               if ( ( ( context.localUtil.CToN( cgiGet( edtactivo_Internalname), ".", ",") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtactivo_Internalname), ".", ",") > Convert.ToDecimal( 9 )) ) )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "ACTIVO");
                  AnyError = 1;
                  GX_FocusControl = edtactivo_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  wbErr = true;
                  A122activo = 0;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A122activo", StringUtil.Str( (decimal)(A122activo), 1, 0));
               }
               else
               {
                  A122activo = (short)(context.localUtil.CToN( cgiGet( edtactivo_Internalname), ".", ","));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A122activo", StringUtil.Str( (decimal)(A122activo), 1, 0));
               }
               if ( ( ( context.localUtil.CToN( cgiGet( edtPaisid_Internalname), ".", ",") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtPaisid_Internalname), ".", ",") > Convert.ToDecimal( 999999999 )) ) )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "PAISID");
                  AnyError = 1;
                  GX_FocusControl = edtPaisid_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  wbErr = true;
                  A15Paisid = 0;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A15Paisid", StringUtil.LTrim( StringUtil.Str( (decimal)(A15Paisid), 9, 0)));
               }
               else
               {
                  A15Paisid = (int)(context.localUtil.CToN( cgiGet( edtPaisid_Internalname), ".", ","));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A15Paisid", StringUtil.LTrim( StringUtil.Str( (decimal)(A15Paisid), 9, 0)));
               }
               if ( ( ( context.localUtil.CToN( cgiGet( edtPuestoid_Internalname), ".", ",") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtPuestoid_Internalname), ".", ",") > Convert.ToDecimal( 999999999 )) ) )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "PUESTOID");
                  AnyError = 1;
                  GX_FocusControl = edtPuestoid_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  wbErr = true;
                  A20Puestoid = 0;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A20Puestoid", StringUtil.LTrim( StringUtil.Str( (decimal)(A20Puestoid), 9, 0)));
               }
               else
               {
                  A20Puestoid = (int)(context.localUtil.CToN( cgiGet( edtPuestoid_Internalname), ".", ","));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A20Puestoid", StringUtil.LTrim( StringUtil.Str( (decimal)(A20Puestoid), 9, 0)));
               }
               A123Usuariocorreo = cgiGet( edtUsuariocorreo_Internalname);
               n123Usuariocorreo = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A123Usuariocorreo", A123Usuariocorreo);
               n123Usuariocorreo = (String.IsNullOrEmpty(StringUtil.RTrim( A123Usuariocorreo)) ? true : false);
               /* Read saved values. */
               Z17Usuarioid = (int)(context.localUtil.CToN( cgiGet( "Z17Usuarioid"), ".", ","));
               Z120usuario = cgiGet( "Z120usuario");
               Z121password = cgiGet( "Z121password");
               Z122activo = (short)(context.localUtil.CToN( cgiGet( "Z122activo"), ".", ","));
               Z123Usuariocorreo = cgiGet( "Z123Usuariocorreo");
               n123Usuariocorreo = (String.IsNullOrEmpty(StringUtil.RTrim( A123Usuariocorreo)) ? true : false);
               Z15Paisid = (int)(context.localUtil.CToN( cgiGet( "Z15Paisid"), ".", ","));
               Z20Puestoid = (int)(context.localUtil.CToN( cgiGet( "Z20Puestoid"), ".", ","));
               IsConfirmed = (short)(context.localUtil.CToN( cgiGet( "IsConfirmed"), ".", ","));
               IsModified = (short)(context.localUtil.CToN( cgiGet( "IsModified"), ".", ","));
               Gx_mode = cgiGet( "Mode");
               N15Paisid = (int)(context.localUtil.CToN( cgiGet( "N15Paisid"), ".", ","));
               N20Puestoid = (int)(context.localUtil.CToN( cgiGet( "N20Puestoid"), ".", ","));
               AV7Usuarioid = (int)(context.localUtil.CToN( cgiGet( "vUSUARIOID"), ".", ","));
               AV11Insert_Paisid = (int)(context.localUtil.CToN( cgiGet( "vINSERT_PAISID"), ".", ","));
               AV12Insert_Puestoid = (int)(context.localUtil.CToN( cgiGet( "vINSERT_PUESTOID"), ".", ","));
               AV14Pgmname = cgiGet( "vPGMNAME");
               Gx_mode = cgiGet( "vMODE");
               /* Read subfile selected row values. */
               /* Read hidden variables. */
               GXKey = Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
               forbiddenHiddens = "hsh" + "Usuario";
               A17Usuarioid = (int)(context.localUtil.CToN( cgiGet( edtUsuarioid_Internalname), ".", ","));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A17Usuarioid", StringUtil.LTrim( StringUtil.Str( (decimal)(A17Usuarioid), 9, 0)));
               forbiddenHiddens = forbiddenHiddens + context.localUtil.Format( (decimal)(A17Usuarioid), "ZZZZZZZZ9");
               forbiddenHiddens = forbiddenHiddens + context.localUtil.Format( (decimal)(AV11Insert_Paisid), "ZZZZZZZZ9");
               forbiddenHiddens = forbiddenHiddens + context.localUtil.Format( (decimal)(AV12Insert_Puestoid), "ZZZZZZZZ9");
               forbiddenHiddens = forbiddenHiddens + StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"));
               hsh = cgiGet( "hsh");
               if ( ( ! ( ( A17Usuarioid != Z17Usuarioid ) ) || ( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) ) && ! GXUtil.CheckEncryptedHash( forbiddenHiddens, hsh, GXKey) )
               {
                  GXUtil.WriteLog("usuario:[SecurityCheckFailed value for]"+"Usuarioid:"+context.localUtil.Format( (decimal)(A17Usuarioid), "ZZZZZZZZ9"));
                  GXUtil.WriteLog("usuario:[SecurityCheckFailed value for]"+"Insert_Paisid:"+context.localUtil.Format( (decimal)(AV11Insert_Paisid), "ZZZZZZZZ9"));
                  GXUtil.WriteLog("usuario:[SecurityCheckFailed value for]"+"Insert_Puestoid:"+context.localUtil.Format( (decimal)(AV12Insert_Puestoid), "ZZZZZZZZ9"));
                  GXUtil.WriteLog("usuario:[SecurityCheckFailed value for]"+"Gx_mode:"+StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!")));
                  GxWebError = 1;
                  context.HttpContext.Response.StatusDescription = 403.ToString();
                  context.HttpContext.Response.StatusCode = 403;
                  context.WriteHtmlText( "<title>403 Forbidden</title>") ;
                  context.WriteHtmlText( "<h1>403 Forbidden</h1>") ;
                  context.WriteHtmlText( "<p /><hr />") ;
                  GXUtil.WriteLog("send_http_error_code " + 403.ToString());
                  AnyError = 1;
                  return  ;
               }
               standaloneNotModal( ) ;
            }
            else
            {
               standaloneNotModal( ) ;
               if ( StringUtil.StrCmp(gxfirstwebparm, "viewer") == 0 )
               {
                  Gx_mode = "DSP";
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!")), context));
                  A17Usuarioid = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A17Usuarioid", StringUtil.LTrim( StringUtil.Str( (decimal)(A17Usuarioid), 9, 0)));
                  getEqualNoModal( ) ;
                  Gx_mode = "DSP";
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!")), context));
                  disable_std_buttons( ) ;
                  standaloneModal( ) ;
               }
               else
               {
                  if ( StringUtil.StrCmp(Gx_mode, "DSP") == 0 )
                  {
                     sMode12 = Gx_mode;
                     Gx_mode = "UPD";
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!")), context));
                     Gx_mode = sMode12;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!")), context));
                  }
                  standaloneModal( ) ;
                  if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
                  {
                     getByPrimaryKey( ) ;
                     if ( RcdFound12 == 1 )
                     {
                        if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
                        {
                           /* Confirm record */
                           CONFIRM_0C0( ) ;
                           if ( AnyError == 0 )
                           {
                              GX_FocusControl = bttBtn_enter_Internalname;
                              context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                           }
                        }
                     }
                     else
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_noinsert", ""), 1, "USUARIOID");
                        AnyError = 1;
                        GX_FocusControl = edtUsuarioid_Internalname;
                        context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                     }
                  }
               }
            }
         }
      }

      protected void Process( )
      {
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            /* Read Transaction buttons. */
            sEvt = cgiGet( "_EventName");
            EvtGridId = cgiGet( "_EventGridId");
            EvtRowId = cgiGet( "_EventRowId");
            if ( StringUtil.Len( sEvt) > 0 )
            {
               sEvtType = StringUtil.Left( sEvt, 1);
               sEvt = StringUtil.Right( sEvt, (short)(StringUtil.Len( sEvt)-1));
               if ( StringUtil.StrCmp(sEvtType, "M") != 0 )
               {
                  if ( StringUtil.StrCmp(sEvtType, "E") == 0 )
                  {
                     sEvtType = StringUtil.Right( sEvt, 1);
                     if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                     {
                        sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                        if ( StringUtil.StrCmp(sEvt, "START") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: Start */
                           E110C2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "AFTER TRN") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: After Trn */
                           E120C2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "ENTER") == 0 )
                        {
                           context.wbHandled = 1;
                           if ( StringUtil.StrCmp(Gx_mode, "DSP") != 0 )
                           {
                              btn_enter( ) ;
                           }
                           /* No code required for Cancel button. It is implemented as the Reset button. */
                        }
                     }
                     else
                     {
                     }
                  }
                  context.wbHandled = 1;
               }
            }
         }
      }

      protected void AfterTrn( )
      {
         if ( trnEnded == 1 )
         {
            /* Execute user event: After Trn */
            E120C2 ();
            trnEnded = 0;
            standaloneNotModal( ) ;
            standaloneModal( ) ;
            if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  )
            {
               /* Clear variables for new insertion. */
               InitAll0C12( ) ;
               standaloneNotModal( ) ;
               standaloneModal( ) ;
            }
         }
      }

      public override String ToString( )
      {
         return "" ;
      }

      public GxContentInfo GetContentInfo( )
      {
         return (GxContentInfo)(null) ;
      }

      protected void disable_std_buttons( )
      {
         bttBtn_delete_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_delete_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_delete_Visible), 5, 0)), true);
         bttBtn_first_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_first_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_first_Visible), 5, 0)), true);
         bttBtn_previous_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_previous_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_previous_Visible), 5, 0)), true);
         bttBtn_next_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_next_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_next_Visible), 5, 0)), true);
         bttBtn_last_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_last_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_last_Visible), 5, 0)), true);
         bttBtn_select_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_select_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_select_Visible), 5, 0)), true);
         if ( ( StringUtil.StrCmp(Gx_mode, "DSP") == 0 ) || ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 ) )
         {
            bttBtn_delete_Visible = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_delete_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_delete_Visible), 5, 0)), true);
            if ( StringUtil.StrCmp(Gx_mode, "DSP") == 0 )
            {
               bttBtn_enter_Visible = 0;
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_enter_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_enter_Visible), 5, 0)), true);
            }
            DisableAttributes0C12( ) ;
         }
      }

      protected void set_caption( )
      {
         if ( ( IsConfirmed == 1 ) && ( AnyError == 0 ) )
         {
            if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_confdelete", ""), 0, "", true);
            }
            else
            {
               GX_msglist.addItem(context.GetMessage( "GXM_mustconfirm", ""), 0, "", true);
            }
         }
      }

      protected void CONFIRM_0C0( )
      {
         BeforeValidate0C12( ) ;
         if ( AnyError == 0 )
         {
            if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
            {
               OnDeleteControls0C12( ) ;
            }
            else
            {
               CheckExtendedTable0C12( ) ;
               CloseExtendedTableCursors0C12( ) ;
            }
         }
         if ( AnyError == 0 )
         {
            IsConfirmed = 1;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "IsConfirmed", StringUtil.LTrim( StringUtil.Str( (decimal)(IsConfirmed), 4, 0)));
         }
      }

      protected void ResetCaption0C0( )
      {
      }

      protected void E110C2( )
      {
         /* Start Routine */
         if ( ! new isauthorized(context).executeUdp(  AV14Pgmname) )
         {
            CallWebObject(formatLink("notauthorized.aspx") + "?" + UrlEncode(StringUtil.RTrim(AV14Pgmname)));
            context.wjLocDisableFrm = 1;
         }
         AV9TrnContext.FromXml(AV10WebSession.Get("TrnContext"), null, "TransactionContext", "SAPWEB08");
         AV11Insert_Paisid = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV11Insert_Paisid", StringUtil.LTrim( StringUtil.Str( (decimal)(AV11Insert_Paisid), 9, 0)));
         AV12Insert_Puestoid = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV12Insert_Puestoid", StringUtil.LTrim( StringUtil.Str( (decimal)(AV12Insert_Puestoid), 9, 0)));
         if ( ( StringUtil.StrCmp(AV9TrnContext.gxTpr_Transactionname, AV14Pgmname) == 0 ) && ( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) )
         {
            AV15GXV1 = 1;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15GXV1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV15GXV1), 8, 0)));
            while ( AV15GXV1 <= AV9TrnContext.gxTpr_Attributes.Count )
            {
               AV13TrnContextAtt = ((SdtTransactionContext_Attribute)AV9TrnContext.gxTpr_Attributes.Item(AV15GXV1));
               if ( StringUtil.StrCmp(AV13TrnContextAtt.gxTpr_Attributename, "Paisid") == 0 )
               {
                  AV11Insert_Paisid = (int)(NumberUtil.Val( AV13TrnContextAtt.gxTpr_Attributevalue, "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV11Insert_Paisid", StringUtil.LTrim( StringUtil.Str( (decimal)(AV11Insert_Paisid), 9, 0)));
               }
               else if ( StringUtil.StrCmp(AV13TrnContextAtt.gxTpr_Attributename, "Puestoid") == 0 )
               {
                  AV12Insert_Puestoid = (int)(NumberUtil.Val( AV13TrnContextAtt.gxTpr_Attributevalue, "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV12Insert_Puestoid", StringUtil.LTrim( StringUtil.Str( (decimal)(AV12Insert_Puestoid), 9, 0)));
               }
               AV15GXV1 = (int)(AV15GXV1+1);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15GXV1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV15GXV1), 8, 0)));
            }
         }
      }

      protected void E120C2( )
      {
         /* After Trn Routine */
         if ( ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 ) && ! AV9TrnContext.gxTpr_Callerondelete )
         {
            CallWebObject(formatLink("wwusuario.aspx") );
            context.wjLocDisableFrm = 1;
         }
         context.setWebReturnParms(new Object[] {});
         context.setWebReturnParmsMetadata(new Object[] {});
         context.wjLocDisableFrm = 1;
         context.nUserReturn = 1;
         returnInSub = true;
         if (true) return;
      }

      protected void ZM0C12( short GX_JID )
      {
         if ( ( GX_JID == 11 ) || ( GX_JID == 0 ) )
         {
            if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
            {
               Z120usuario = T000C3_A120usuario[0];
               Z121password = T000C3_A121password[0];
               Z122activo = T000C3_A122activo[0];
               Z123Usuariocorreo = T000C3_A123Usuariocorreo[0];
               Z15Paisid = T000C3_A15Paisid[0];
               Z20Puestoid = T000C3_A20Puestoid[0];
            }
            else
            {
               Z120usuario = A120usuario;
               Z121password = A121password;
               Z122activo = A122activo;
               Z123Usuariocorreo = A123Usuariocorreo;
               Z15Paisid = A15Paisid;
               Z20Puestoid = A20Puestoid;
            }
         }
         if ( GX_JID == -11 )
         {
            Z17Usuarioid = A17Usuarioid;
            Z120usuario = A120usuario;
            Z121password = A121password;
            Z122activo = A122activo;
            Z123Usuariocorreo = A123Usuariocorreo;
            Z15Paisid = A15Paisid;
            Z20Puestoid = A20Puestoid;
         }
      }

      protected void standaloneNotModal( )
      {
         edtUsuarioid_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtUsuarioid_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtUsuarioid_Enabled), 5, 0)), true);
         imgprompt_15_Link = ((StringUtil.StrCmp(Gx_mode, "DSP")==0) ? "" : "javascript:"+"gx.popup.openPrompt('"+"gx00d0.aspx"+"',["+"{Ctrl:gx.dom.el('"+"PAISID"+"'), id:'"+"PAISID"+"'"+",IOType:'out'}"+"],"+"null"+","+"'', false"+","+"false"+");");
         imgprompt_20_Link = ((StringUtil.StrCmp(Gx_mode, "DSP")==0) ? "" : "javascript:"+"gx.popup.openPrompt('"+"gx0080.aspx"+"',["+"{Ctrl:gx.dom.el('"+"PUESTOID"+"'), id:'"+"PUESTOID"+"'"+",IOType:'out'}"+"],"+"null"+","+"'', false"+","+"false"+");");
         edtUsuarioid_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtUsuarioid_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtUsuarioid_Enabled), 5, 0)), true);
         bttBtn_delete_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_delete_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_delete_Enabled), 5, 0)), true);
         if ( ! (0==AV7Usuarioid) )
         {
            A17Usuarioid = AV7Usuarioid;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A17Usuarioid", StringUtil.LTrim( StringUtil.Str( (decimal)(A17Usuarioid), 9, 0)));
         }
         if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) && ! (0==AV11Insert_Paisid) )
         {
            edtPaisid_Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtPaisid_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtPaisid_Enabled), 5, 0)), true);
         }
         else
         {
            edtPaisid_Enabled = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtPaisid_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtPaisid_Enabled), 5, 0)), true);
         }
         if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) && ! (0==AV12Insert_Puestoid) )
         {
            edtPuestoid_Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtPuestoid_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtPuestoid_Enabled), 5, 0)), true);
         }
         else
         {
            edtPuestoid_Enabled = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtPuestoid_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtPuestoid_Enabled), 5, 0)), true);
         }
      }

      protected void standaloneModal( )
      {
         if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) && ! (0==AV12Insert_Puestoid) )
         {
            A20Puestoid = AV12Insert_Puestoid;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A20Puestoid", StringUtil.LTrim( StringUtil.Str( (decimal)(A20Puestoid), 9, 0)));
         }
         if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) && ! (0==AV11Insert_Paisid) )
         {
            A15Paisid = AV11Insert_Paisid;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A15Paisid", StringUtil.LTrim( StringUtil.Str( (decimal)(A15Paisid), 9, 0)));
         }
         if ( StringUtil.StrCmp(Gx_mode, "DSP") == 0 )
         {
            bttBtn_enter_Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_enter_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_enter_Enabled), 5, 0)), true);
         }
         else
         {
            bttBtn_enter_Enabled = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_enter_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_enter_Enabled), 5, 0)), true);
         }
         if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) && ( Gx_BScreen == 0 ) )
         {
            AV14Pgmname = "Usuario";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14Pgmname", AV14Pgmname);
         }
      }

      protected void Load0C12( )
      {
         /* Using cursor T000C6 */
         pr_datastore1.execute(4, new Object[] {A17Usuarioid});
         if ( (pr_datastore1.getStatus(4) != 101) )
         {
            RcdFound12 = 1;
            A120usuario = T000C6_A120usuario[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A120usuario", A120usuario);
            A121password = T000C6_A121password[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A121password", A121password);
            A122activo = T000C6_A122activo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A122activo", StringUtil.Str( (decimal)(A122activo), 1, 0));
            A123Usuariocorreo = T000C6_A123Usuariocorreo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A123Usuariocorreo", A123Usuariocorreo);
            n123Usuariocorreo = T000C6_n123Usuariocorreo[0];
            A15Paisid = T000C6_A15Paisid[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A15Paisid", StringUtil.LTrim( StringUtil.Str( (decimal)(A15Paisid), 9, 0)));
            A20Puestoid = T000C6_A20Puestoid[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A20Puestoid", StringUtil.LTrim( StringUtil.Str( (decimal)(A20Puestoid), 9, 0)));
            ZM0C12( -11) ;
         }
         pr_datastore1.close(4);
         OnLoadActions0C12( ) ;
      }

      protected void OnLoadActions0C12( )
      {
         AV14Pgmname = "Usuario";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14Pgmname", AV14Pgmname);
      }

      protected void CheckExtendedTable0C12( )
      {
         Gx_BScreen = 1;
         standaloneModal( ) ;
         AV14Pgmname = "Usuario";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14Pgmname", AV14Pgmname);
         /* Using cursor T000C4 */
         pr_datastore1.execute(2, new Object[] {A15Paisid});
         if ( (pr_datastore1.getStatus(2) == 101) )
         {
            GX_msglist.addItem("No matching 'Pais'.", "ForeignKeyNotFound", 1, "PAISID");
            AnyError = 1;
            GX_FocusControl = edtPaisid_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         pr_datastore1.close(2);
         /* Using cursor T000C5 */
         pr_datastore1.execute(3, new Object[] {A20Puestoid});
         if ( (pr_datastore1.getStatus(3) == 101) )
         {
            GX_msglist.addItem("No matching 'Puesto'.", "ForeignKeyNotFound", 1, "PUESTOID");
            AnyError = 1;
            GX_FocusControl = edtPuestoid_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         pr_datastore1.close(3);
      }

      protected void CloseExtendedTableCursors0C12( )
      {
         pr_datastore1.close(2);
         pr_datastore1.close(3);
      }

      protected void enableDisable( )
      {
      }

      protected void gxLoad_12( int A15Paisid )
      {
         /* Using cursor T000C7 */
         pr_datastore1.execute(5, new Object[] {A15Paisid});
         if ( (pr_datastore1.getStatus(5) == 101) )
         {
            GX_msglist.addItem("No matching 'Pais'.", "ForeignKeyNotFound", 1, "PAISID");
            AnyError = 1;
            GX_FocusControl = edtPaisid_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         GxWebStd.set_html_headers( context, 0, "", "");
         context.GX_webresponse.AddString("[[");
         context.GX_webresponse.AddString("");
         context.GX_webresponse.AddString("]");
         if ( (pr_datastore1.getStatus(5) == 101) )
         {
            context.GX_webresponse.AddString(",");
            context.GX_webresponse.AddString("101");
         }
         context.GX_webresponse.AddString("]");
         pr_datastore1.close(5);
      }

      protected void gxLoad_13( int A20Puestoid )
      {
         /* Using cursor T000C8 */
         pr_datastore1.execute(6, new Object[] {A20Puestoid});
         if ( (pr_datastore1.getStatus(6) == 101) )
         {
            GX_msglist.addItem("No matching 'Puesto'.", "ForeignKeyNotFound", 1, "PUESTOID");
            AnyError = 1;
            GX_FocusControl = edtPuestoid_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         GxWebStd.set_html_headers( context, 0, "", "");
         context.GX_webresponse.AddString("[[");
         context.GX_webresponse.AddString("");
         context.GX_webresponse.AddString("]");
         if ( (pr_datastore1.getStatus(6) == 101) )
         {
            context.GX_webresponse.AddString(",");
            context.GX_webresponse.AddString("101");
         }
         context.GX_webresponse.AddString("]");
         pr_datastore1.close(6);
      }

      protected void GetKey0C12( )
      {
         /* Using cursor T000C9 */
         pr_datastore1.execute(7, new Object[] {A17Usuarioid});
         if ( (pr_datastore1.getStatus(7) != 101) )
         {
            RcdFound12 = 1;
         }
         else
         {
            RcdFound12 = 0;
         }
         pr_datastore1.close(7);
      }

      protected void getByPrimaryKey( )
      {
         /* Using cursor T000C3 */
         pr_datastore1.execute(1, new Object[] {A17Usuarioid});
         if ( (pr_datastore1.getStatus(1) != 101) )
         {
            ZM0C12( 11) ;
            RcdFound12 = 1;
            A17Usuarioid = T000C3_A17Usuarioid[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A17Usuarioid", StringUtil.LTrim( StringUtil.Str( (decimal)(A17Usuarioid), 9, 0)));
            A120usuario = T000C3_A120usuario[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A120usuario", A120usuario);
            A121password = T000C3_A121password[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A121password", A121password);
            A122activo = T000C3_A122activo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A122activo", StringUtil.Str( (decimal)(A122activo), 1, 0));
            A123Usuariocorreo = T000C3_A123Usuariocorreo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A123Usuariocorreo", A123Usuariocorreo);
            n123Usuariocorreo = T000C3_n123Usuariocorreo[0];
            A15Paisid = T000C3_A15Paisid[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A15Paisid", StringUtil.LTrim( StringUtil.Str( (decimal)(A15Paisid), 9, 0)));
            A20Puestoid = T000C3_A20Puestoid[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A20Puestoid", StringUtil.LTrim( StringUtil.Str( (decimal)(A20Puestoid), 9, 0)));
            Z17Usuarioid = A17Usuarioid;
            sMode12 = Gx_mode;
            Gx_mode = "DSP";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!")), context));
            Load0C12( ) ;
            if ( AnyError == 1 )
            {
               RcdFound12 = 0;
               InitializeNonKey0C12( ) ;
            }
            Gx_mode = sMode12;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!")), context));
         }
         else
         {
            RcdFound12 = 0;
            InitializeNonKey0C12( ) ;
            sMode12 = Gx_mode;
            Gx_mode = "DSP";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!")), context));
            standaloneModal( ) ;
            Gx_mode = sMode12;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!")), context));
         }
         pr_datastore1.close(1);
      }

      protected void getEqualNoModal( )
      {
         GetKey0C12( ) ;
         if ( RcdFound12 == 0 )
         {
         }
         else
         {
         }
         getByPrimaryKey( ) ;
      }

      protected void move_next( )
      {
         RcdFound12 = 0;
         /* Using cursor T000C10 */
         pr_datastore1.execute(8, new Object[] {A17Usuarioid});
         if ( (pr_datastore1.getStatus(8) != 101) )
         {
            while ( (pr_datastore1.getStatus(8) != 101) && ( ( T000C10_A17Usuarioid[0] < A17Usuarioid ) ) )
            {
               pr_datastore1.readNext(8);
            }
            if ( (pr_datastore1.getStatus(8) != 101) && ( ( T000C10_A17Usuarioid[0] > A17Usuarioid ) ) )
            {
               A17Usuarioid = T000C10_A17Usuarioid[0];
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A17Usuarioid", StringUtil.LTrim( StringUtil.Str( (decimal)(A17Usuarioid), 9, 0)));
               RcdFound12 = 1;
            }
         }
         pr_datastore1.close(8);
      }

      protected void move_previous( )
      {
         RcdFound12 = 0;
         /* Using cursor T000C11 */
         pr_datastore1.execute(9, new Object[] {A17Usuarioid});
         if ( (pr_datastore1.getStatus(9) != 101) )
         {
            while ( (pr_datastore1.getStatus(9) != 101) && ( ( T000C11_A17Usuarioid[0] > A17Usuarioid ) ) )
            {
               pr_datastore1.readNext(9);
            }
            if ( (pr_datastore1.getStatus(9) != 101) && ( ( T000C11_A17Usuarioid[0] < A17Usuarioid ) ) )
            {
               A17Usuarioid = T000C11_A17Usuarioid[0];
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A17Usuarioid", StringUtil.LTrim( StringUtil.Str( (decimal)(A17Usuarioid), 9, 0)));
               RcdFound12 = 1;
            }
         }
         pr_datastore1.close(9);
      }

      protected void btn_enter( )
      {
         nKeyPressed = 1;
         GetKey0C12( ) ;
         if ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )
         {
            /* Insert record */
            GX_FocusControl = edtusuario_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            Insert0C12( ) ;
            if ( AnyError == 1 )
            {
               GX_FocusControl = "";
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            }
         }
         else
         {
            if ( RcdFound12 == 1 )
            {
               if ( A17Usuarioid != Z17Usuarioid )
               {
                  A17Usuarioid = Z17Usuarioid;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A17Usuarioid", StringUtil.LTrim( StringUtil.Str( (decimal)(A17Usuarioid), 9, 0)));
                  GX_msglist.addItem(context.GetMessage( "GXM_getbeforeupd", ""), "CandidateKeyNotFound", 1, "USUARIOID");
                  AnyError = 1;
                  GX_FocusControl = edtUsuarioid_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               }
               else if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
               {
                  delete( ) ;
                  AfterTrn( ) ;
                  GX_FocusControl = edtusuario_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               }
               else
               {
                  /* Update record */
                  Update0C12( ) ;
                  GX_FocusControl = edtusuario_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               }
            }
            else
            {
               if ( A17Usuarioid != Z17Usuarioid )
               {
                  /* Insert record */
                  GX_FocusControl = edtusuario_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  Insert0C12( ) ;
                  if ( AnyError == 1 )
                  {
                     GX_FocusControl = "";
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  }
               }
               else
               {
                  if ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 )
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_recdeleted", ""), 1, "USUARIOID");
                     AnyError = 1;
                     GX_FocusControl = edtUsuarioid_Internalname;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  }
                  else
                  {
                     /* Insert record */
                     GX_FocusControl = edtusuario_Internalname;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                     Insert0C12( ) ;
                     if ( AnyError == 1 )
                     {
                        GX_FocusControl = "";
                        context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                     }
                  }
               }
            }
         }
         AfterTrn( ) ;
         if ( ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 ) || ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 ) )
         {
            if ( AnyError == 0 )
            {
               context.nUserReturn = 1;
            }
         }
      }

      protected void btn_delete( )
      {
         if ( A17Usuarioid != Z17Usuarioid )
         {
            A17Usuarioid = Z17Usuarioid;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A17Usuarioid", StringUtil.LTrim( StringUtil.Str( (decimal)(A17Usuarioid), 9, 0)));
            GX_msglist.addItem(context.GetMessage( "GXM_getbeforedlt", ""), 1, "USUARIOID");
            AnyError = 1;
            GX_FocusControl = edtUsuarioid_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         else
         {
            delete( ) ;
            AfterTrn( ) ;
            GX_FocusControl = edtusuario_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         if ( AnyError != 0 )
         {
         }
      }

      protected void CheckOptimisticConcurrency0C12( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
         {
            /* Using cursor T000C2 */
            pr_datastore1.execute(0, new Object[] {A17Usuarioid});
            if ( (pr_datastore1.getStatus(0) == 103) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_lock", new   object[]  {"USUARIO"}), "RecordIsLocked", 1, "");
               AnyError = 1;
               return  ;
            }
            Gx_longc = false;
            if ( (pr_datastore1.getStatus(0) == 101) || ( StringUtil.StrCmp(Z120usuario, T000C2_A120usuario[0]) != 0 ) || ( StringUtil.StrCmp(Z121password, T000C2_A121password[0]) != 0 ) || ( Z122activo != T000C2_A122activo[0] ) || ( StringUtil.StrCmp(Z123Usuariocorreo, T000C2_A123Usuariocorreo[0]) != 0 ) || ( Z15Paisid != T000C2_A15Paisid[0] ) )
            {
               Gx_longc = true;
            }
            if ( Gx_longc || ( Z20Puestoid != T000C2_A20Puestoid[0] ) )
            {
               if ( StringUtil.StrCmp(Z120usuario, T000C2_A120usuario[0]) != 0 )
               {
                  GXUtil.WriteLog("usuario:[seudo value changed for attri]"+"usuario");
                  GXUtil.WriteLogRaw("Old: ",Z120usuario);
                  GXUtil.WriteLogRaw("Current: ",T000C2_A120usuario[0]);
               }
               if ( StringUtil.StrCmp(Z121password, T000C2_A121password[0]) != 0 )
               {
                  GXUtil.WriteLog("usuario:[seudo value changed for attri]"+"password");
                  GXUtil.WriteLogRaw("Old: ",Z121password);
                  GXUtil.WriteLogRaw("Current: ",T000C2_A121password[0]);
               }
               if ( Z122activo != T000C2_A122activo[0] )
               {
                  GXUtil.WriteLog("usuario:[seudo value changed for attri]"+"activo");
                  GXUtil.WriteLogRaw("Old: ",Z122activo);
                  GXUtil.WriteLogRaw("Current: ",T000C2_A122activo[0]);
               }
               if ( StringUtil.StrCmp(Z123Usuariocorreo, T000C2_A123Usuariocorreo[0]) != 0 )
               {
                  GXUtil.WriteLog("usuario:[seudo value changed for attri]"+"Usuariocorreo");
                  GXUtil.WriteLogRaw("Old: ",Z123Usuariocorreo);
                  GXUtil.WriteLogRaw("Current: ",T000C2_A123Usuariocorreo[0]);
               }
               if ( Z15Paisid != T000C2_A15Paisid[0] )
               {
                  GXUtil.WriteLog("usuario:[seudo value changed for attri]"+"Paisid");
                  GXUtil.WriteLogRaw("Old: ",Z15Paisid);
                  GXUtil.WriteLogRaw("Current: ",T000C2_A15Paisid[0]);
               }
               if ( Z20Puestoid != T000C2_A20Puestoid[0] )
               {
                  GXUtil.WriteLog("usuario:[seudo value changed for attri]"+"Puestoid");
                  GXUtil.WriteLogRaw("Old: ",Z20Puestoid);
                  GXUtil.WriteLogRaw("Current: ",T000C2_A20Puestoid[0]);
               }
               GX_msglist.addItem(context.GetMessage( "GXM_waschg", new   object[]  {"USUARIO"}), "RecordWasChanged", 1, "");
               AnyError = 1;
               return  ;
            }
         }
      }

      protected void Insert0C12( )
      {
         BeforeValidate0C12( ) ;
         if ( AnyError == 0 )
         {
            CheckExtendedTable0C12( ) ;
         }
         if ( AnyError == 0 )
         {
            ZM0C12( 0) ;
            CheckOptimisticConcurrency0C12( ) ;
            if ( AnyError == 0 )
            {
               AfterConfirm0C12( ) ;
               if ( AnyError == 0 )
               {
                  BeforeInsert0C12( ) ;
                  if ( AnyError == 0 )
                  {
                     /* Using cursor T000C12 */
                     pr_datastore1.execute(10, new Object[] {A120usuario, A121password, A122activo, n123Usuariocorreo, A123Usuariocorreo, A15Paisid, A20Puestoid});
                     A17Usuarioid = T000C12_A17Usuarioid[0];
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A17Usuarioid", StringUtil.LTrim( StringUtil.Str( (decimal)(A17Usuarioid), 9, 0)));
                     pr_datastore1.close(10);
                     dsDataStore1.SmartCacheProvider.SetUpdated("USUARIO") ;
                     if ( AnyError == 0 )
                     {
                        /* Start of After( Insert) rules */
                        /* End of After( Insert) rules */
                        if ( AnyError == 0 )
                        {
                           /* Save values for previous() function. */
                           GX_msglist.addItem(context.GetMessage( "GXM_sucadded", ""), "SuccessfullyAdded", 0, "", true);
                           ResetCaption0C0( ) ;
                        }
                     }
                  }
                  else
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                     AnyError = 1;
                  }
               }
            }
            else
            {
               Load0C12( ) ;
            }
            EndLevel0C12( ) ;
         }
         CloseExtendedTableCursors0C12( ) ;
      }

      protected void Update0C12( )
      {
         BeforeValidate0C12( ) ;
         if ( AnyError == 0 )
         {
            CheckExtendedTable0C12( ) ;
         }
         if ( AnyError == 0 )
         {
            CheckOptimisticConcurrency0C12( ) ;
            if ( AnyError == 0 )
            {
               AfterConfirm0C12( ) ;
               if ( AnyError == 0 )
               {
                  BeforeUpdate0C12( ) ;
                  if ( AnyError == 0 )
                  {
                     /* Using cursor T000C13 */
                     pr_datastore1.execute(11, new Object[] {A120usuario, A121password, A122activo, n123Usuariocorreo, A123Usuariocorreo, A15Paisid, A20Puestoid, A17Usuarioid});
                     pr_datastore1.close(11);
                     dsDataStore1.SmartCacheProvider.SetUpdated("USUARIO") ;
                     if ( (pr_datastore1.getStatus(11) == 103) )
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_lock", new   object[]  {"USUARIO"}), "RecordIsLocked", 1, "");
                        AnyError = 1;
                     }
                     DeferredUpdate0C12( ) ;
                     if ( AnyError == 0 )
                     {
                        /* Start of After( update) rules */
                        /* End of After( update) rules */
                        if ( AnyError == 0 )
                        {
                           if ( ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 ) || ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 ) )
                           {
                              if ( AnyError == 0 )
                              {
                                 context.nUserReturn = 1;
                              }
                           }
                        }
                     }
                     else
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                        AnyError = 1;
                     }
                  }
               }
            }
            EndLevel0C12( ) ;
         }
         CloseExtendedTableCursors0C12( ) ;
      }

      protected void DeferredUpdate0C12( )
      {
      }

      protected void delete( )
      {
         BeforeValidate0C12( ) ;
         if ( AnyError == 0 )
         {
            CheckOptimisticConcurrency0C12( ) ;
         }
         if ( AnyError == 0 )
         {
            OnDeleteControls0C12( ) ;
            AfterConfirm0C12( ) ;
            if ( AnyError == 0 )
            {
               BeforeDelete0C12( ) ;
               if ( AnyError == 0 )
               {
                  /* No cascading delete specified. */
                  /* Using cursor T000C14 */
                  pr_datastore1.execute(12, new Object[] {A17Usuarioid});
                  pr_datastore1.close(12);
                  dsDataStore1.SmartCacheProvider.SetUpdated("USUARIO") ;
                  if ( AnyError == 0 )
                  {
                     /* Start of After( delete) rules */
                     /* End of After( delete) rules */
                     if ( AnyError == 0 )
                     {
                        if ( ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 ) || ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 ) )
                        {
                           if ( AnyError == 0 )
                           {
                              context.nUserReturn = 1;
                           }
                        }
                     }
                  }
                  else
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                     AnyError = 1;
                  }
               }
            }
         }
         sMode12 = Gx_mode;
         Gx_mode = "DLT";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!")), context));
         EndLevel0C12( ) ;
         Gx_mode = sMode12;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!")), context));
      }

      protected void OnDeleteControls0C12( )
      {
         standaloneModal( ) ;
         if ( AnyError == 0 )
         {
            /* Delete mode formulas */
            AV14Pgmname = "Usuario";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14Pgmname", AV14Pgmname);
         }
      }

      protected void EndLevel0C12( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
         {
            pr_datastore1.close(0);
         }
         if ( AnyError == 0 )
         {
            BeforeComplete0C12( ) ;
         }
         if ( AnyError == 0 )
         {
            pr_datastore1.close(1);
            context.CommitDataStores("usuario",pr_default);
            if ( AnyError == 0 )
            {
               ConfirmValues0C0( ) ;
            }
            /* After transaction rules */
            /* Execute 'After Trn' event if defined. */
            trnEnded = 1;
         }
         else
         {
            pr_datastore1.close(1);
            context.RollbackDataStores("usuario",pr_default);
         }
         IsModified = 0;
         if ( AnyError != 0 )
         {
            context.wjLoc = "";
            context.nUserReturn = 0;
         }
      }

      public void ScanStart0C12( )
      {
         /* Scan By routine */
         /* Using cursor T000C15 */
         pr_datastore1.execute(13);
         RcdFound12 = 0;
         if ( (pr_datastore1.getStatus(13) != 101) )
         {
            RcdFound12 = 1;
            A17Usuarioid = T000C15_A17Usuarioid[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A17Usuarioid", StringUtil.LTrim( StringUtil.Str( (decimal)(A17Usuarioid), 9, 0)));
         }
         /* Load Subordinate Levels */
      }

      protected void ScanNext0C12( )
      {
         /* Scan next routine */
         pr_datastore1.readNext(13);
         RcdFound12 = 0;
         if ( (pr_datastore1.getStatus(13) != 101) )
         {
            RcdFound12 = 1;
            A17Usuarioid = T000C15_A17Usuarioid[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A17Usuarioid", StringUtil.LTrim( StringUtil.Str( (decimal)(A17Usuarioid), 9, 0)));
         }
      }

      protected void ScanEnd0C12( )
      {
         pr_datastore1.close(13);
      }

      protected void AfterConfirm0C12( )
      {
         /* After Confirm Rules */
      }

      protected void BeforeInsert0C12( )
      {
         /* Before Insert Rules */
      }

      protected void BeforeUpdate0C12( )
      {
         /* Before Update Rules */
      }

      protected void BeforeDelete0C12( )
      {
         /* Before Delete Rules */
      }

      protected void BeforeComplete0C12( )
      {
         /* Before Complete Rules */
      }

      protected void BeforeValidate0C12( )
      {
         /* Before Validate Rules */
      }

      protected void DisableAttributes0C12( )
      {
         edtUsuarioid_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtUsuarioid_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtUsuarioid_Enabled), 5, 0)), true);
         edtusuario_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtusuario_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtusuario_Enabled), 5, 0)), true);
         edtpassword_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtpassword_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtpassword_Enabled), 5, 0)), true);
         edtactivo_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtactivo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtactivo_Enabled), 5, 0)), true);
         edtPaisid_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtPaisid_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtPaisid_Enabled), 5, 0)), true);
         edtPuestoid_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtPuestoid_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtPuestoid_Enabled), 5, 0)), true);
         edtUsuariocorreo_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtUsuariocorreo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtUsuariocorreo_Enabled), 5, 0)), true);
      }

      protected void send_integrity_lvl_hashes0C12( )
      {
      }

      protected void assign_properties_default( )
      {
      }

      protected void ConfirmValues0C0( )
      {
      }

      public override void RenderHtmlHeaders( )
      {
         GxWebStd.gx_html_headers( context, 0, "", "", Form.Meta, Form.Metaequiv, true);
      }

      public override void RenderHtmlOpenForm( )
      {
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.WriteHtmlText( "<title>") ;
         context.SendWebValue( Form.Caption) ;
         context.WriteHtmlTextNl( "</title>") ;
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         if ( StringUtil.Len( sDynURL) > 0 )
         {
            context.WriteHtmlText( "<BASE href=\""+sDynURL+"\" />") ;
         }
         define_styles( ) ;
         MasterPageObj.master_styles();
         if ( ( ( context.GetBrowserType( ) == 1 ) || ( context.GetBrowserType( ) == 5 ) ) && ( StringUtil.StrCmp(context.GetBrowserVersion( ), "7.0") == 0 ) )
         {
            context.AddJavascriptSource("json2.js", "?"+context.GetBuildNumber( 127771), false);
         }
         context.AddJavascriptSource("jquery.js", "?"+context.GetBuildNumber( 127771), false);
         context.AddJavascriptSource("gxtimezone.js", "?"+context.GetBuildNumber( 127771), false);
         context.AddJavascriptSource("gxgral.js", "?"+context.GetBuildNumber( 127771), false);
         context.AddJavascriptSource("gxcfg.js", "?2019141921292", false);
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.WriteHtmlText( Form.Headerrawhtml) ;
         context.CloseHtmlHeader();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         FormProcess = " data-HasEnter=\"true\" data-Skiponenter=\"false\"";
         context.WriteHtmlText( "<body ") ;
         bodyStyle = "" + "background-color:" + context.BuildHTMLColor( Form.Backcolor) + ";color:" + context.BuildHTMLColor( Form.Textcolor) + ";";
         bodyStyle = bodyStyle + "-moz-opacity:0;opacity:0;";
         if ( ! ( String.IsNullOrEmpty(StringUtil.RTrim( Form.Background)) ) )
         {
            bodyStyle = bodyStyle + " background-image:url(" + context.convertURL( Form.Background) + ")";
         }
         context.WriteHtmlText( " "+"class=\"form-horizontal Form\""+" "+ "style='"+bodyStyle+"'") ;
         context.WriteHtmlText( FormProcess+">") ;
         context.skipLines(1);
         context.WriteHtmlTextNl( "<form id=\"MAINFORM\" autocomplete=\"off\" name=\"MAINFORM\" method=\"post\" tabindex=-1  class=\"form-horizontal Form\" data-gx-class=\"form-horizontal Form\" novalidate action=\""+formatLink("usuario.aspx") + "?" + UrlEncode(StringUtil.RTrim(Gx_mode)) + "," + UrlEncode("" +AV7Usuarioid)+"\">") ;
         GxWebStd.gx_hidden_field( context, "_EventName", "");
         GxWebStd.gx_hidden_field( context, "_EventGridId", "");
         GxWebStd.gx_hidden_field( context, "_EventRowId", "");
         context.WriteHtmlText( "<input type=\"submit\" title=\"submit\" style=\"display:none\" disabled>") ;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, "FORM", "Class", "form-horizontal Form", true);
         toggleJsOutput = isJsOutputEnabled( );
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
      }

      protected void send_integrity_footer_hashes( )
      {
         GXKey = Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
         forbiddenHiddens = "hsh" + "Usuario";
         forbiddenHiddens = forbiddenHiddens + context.localUtil.Format( (decimal)(A17Usuarioid), "ZZZZZZZZ9");
         forbiddenHiddens = forbiddenHiddens + context.localUtil.Format( (decimal)(AV11Insert_Paisid), "ZZZZZZZZ9");
         forbiddenHiddens = forbiddenHiddens + context.localUtil.Format( (decimal)(AV12Insert_Puestoid), "ZZZZZZZZ9");
         forbiddenHiddens = forbiddenHiddens + StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"));
         GxWebStd.gx_hidden_field( context, "hsh", GetEncryptedHash( forbiddenHiddens, GXKey));
         GXUtil.WriteLog("usuario:[SendSecurityCheck value for]"+"Usuarioid:"+context.localUtil.Format( (decimal)(A17Usuarioid), "ZZZZZZZZ9"));
         GXUtil.WriteLog("usuario:[SendSecurityCheck value for]"+"Insert_Paisid:"+context.localUtil.Format( (decimal)(AV11Insert_Paisid), "ZZZZZZZZ9"));
         GXUtil.WriteLog("usuario:[SendSecurityCheck value for]"+"Insert_Puestoid:"+context.localUtil.Format( (decimal)(AV12Insert_Puestoid), "ZZZZZZZZ9"));
         GXUtil.WriteLog("usuario:[SendSecurityCheck value for]"+"Gx_mode:"+StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!")));
      }

      protected void SendCloseFormHiddens( )
      {
         /* Send hidden variables. */
         /* Send saved values. */
         send_integrity_footer_hashes( ) ;
         GxWebStd.gx_hidden_field( context, "Z17Usuarioid", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z17Usuarioid), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "Z120usuario", Z120usuario);
         GxWebStd.gx_hidden_field( context, "Z121password", Z121password);
         GxWebStd.gx_hidden_field( context, "Z122activo", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z122activo), 1, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "Z123Usuariocorreo", Z123Usuariocorreo);
         GxWebStd.gx_hidden_field( context, "Z15Paisid", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z15Paisid), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "Z20Puestoid", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z20Puestoid), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "IsConfirmed", StringUtil.LTrim( StringUtil.NToC( (decimal)(IsConfirmed), 4, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "IsModified", StringUtil.LTrim( StringUtil.NToC( (decimal)(IsModified), 4, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "Mode", StringUtil.RTrim( Gx_mode));
         GxWebStd.gx_hidden_field( context, "gxhash_Mode", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!")), context));
         GxWebStd.gx_hidden_field( context, "N15Paisid", StringUtil.LTrim( StringUtil.NToC( (decimal)(A15Paisid), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "N20Puestoid", StringUtil.LTrim( StringUtil.NToC( (decimal)(A20Puestoid), 9, 0, ".", "")));
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vTRNCONTEXT", AV9TrnContext);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vTRNCONTEXT", AV9TrnContext);
         }
         GxWebStd.gx_hidden_field( context, "vUSUARIOID", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV7Usuarioid), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "gxhash_vUSUARIOID", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV7Usuarioid), "ZZZZZZZZ9"), context));
         GxWebStd.gx_hidden_field( context, "vINSERT_PAISID", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV11Insert_Paisid), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "vINSERT_PUESTOID", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV12Insert_Puestoid), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "vPGMNAME", StringUtil.RTrim( AV14Pgmname));
         GxWebStd.gx_hidden_field( context, "vMODE", StringUtil.RTrim( Gx_mode));
         GxWebStd.gx_hidden_field( context, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!")), context));
      }

      public override void RenderHtmlCloseForm( )
      {
         SendCloseFormHiddens( ) ;
         GxWebStd.gx_hidden_field( context, "GX_FocusControl", GX_FocusControl);
         SendAjaxEncryptionKey();
         SendSecurityToken(sPrefix);
         SendComponentObjects();
         SendServerCommands();
         SendState();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         context.WriteHtmlTextNl( "</form>") ;
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         include_jscripts( ) ;
      }

      public override short ExecuteStartEvent( )
      {
         standaloneStartup( ) ;
         gxajaxcallmode = (short)((isAjaxCallMode( ) ? 1 : 0));
         return gxajaxcallmode ;
      }

      public override void RenderHtmlContent( )
      {
         context.WriteHtmlText( "<div") ;
         GxWebStd.ClassAttribute( context, "gx-ct-body"+" "+(String.IsNullOrEmpty(StringUtil.RTrim( Form.Class)) ? "form-horizontal Form" : Form.Class)+"-fx");
         context.WriteHtmlText( ">") ;
         Draw( ) ;
         context.WriteHtmlText( "</div>") ;
      }

      public override void DispatchEvents( )
      {
         Process( ) ;
      }

      public override bool HasEnterEvent( )
      {
         return true ;
      }

      public override GXWebForm GetForm( )
      {
         return Form ;
      }

      public override String GetSelfLink( )
      {
         return formatLink("usuario.aspx") + "?" + UrlEncode(StringUtil.RTrim(Gx_mode)) + "," + UrlEncode("" +AV7Usuarioid) ;
      }

      public override String GetPgmname( )
      {
         return "Usuario" ;
      }

      public override String GetPgmdesc( )
      {
         return "Usuario" ;
      }

      protected void InitializeNonKey0C12( )
      {
         A15Paisid = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A15Paisid", StringUtil.LTrim( StringUtil.Str( (decimal)(A15Paisid), 9, 0)));
         A20Puestoid = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A20Puestoid", StringUtil.LTrim( StringUtil.Str( (decimal)(A20Puestoid), 9, 0)));
         A120usuario = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A120usuario", A120usuario);
         A121password = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A121password", A121password);
         A122activo = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A122activo", StringUtil.Str( (decimal)(A122activo), 1, 0));
         A123Usuariocorreo = "";
         n123Usuariocorreo = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A123Usuariocorreo", A123Usuariocorreo);
         n123Usuariocorreo = (String.IsNullOrEmpty(StringUtil.RTrim( A123Usuariocorreo)) ? true : false);
         Z120usuario = "";
         Z121password = "";
         Z122activo = 0;
         Z123Usuariocorreo = "";
         Z15Paisid = 0;
         Z20Puestoid = 0;
      }

      protected void InitAll0C12( )
      {
         A17Usuarioid = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A17Usuarioid", StringUtil.LTrim( StringUtil.Str( (decimal)(A17Usuarioid), 9, 0)));
         InitializeNonKey0C12( ) ;
      }

      protected void StandaloneModalInsert( )
      {
      }

      protected void define_styles( )
      {
         AddThemeStyleSheetFile("", context.GetTheme( )+".css", "?"+GetCacheInvalidationToken( ));
         bool outputEnabled = isOutputEnabled( ) ;
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         idxLst = 1;
         while ( idxLst <= Form.Jscriptsrc.Count )
         {
            context.AddJavascriptSource(StringUtil.RTrim( ((String)Form.Jscriptsrc.Item(idxLst))), "?201914192135", true);
            idxLst = (int)(idxLst+1);
         }
         if ( ! outputEnabled )
         {
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
         }
         /* End function define_styles */
      }

      protected void include_jscripts( )
      {
         context.AddJavascriptSource("messages.eng.js", "?"+GetCacheInvalidationToken( ), false);
         context.AddJavascriptSource("usuario.js", "?201914192135", false);
         /* End function include_jscripts */
      }

      protected void init_default_properties( )
      {
         lblTitle_Internalname = "TITLE";
         divTitlecontainer_Internalname = "TITLECONTAINER";
         bttBtn_first_Internalname = "BTN_FIRST";
         bttBtn_previous_Internalname = "BTN_PREVIOUS";
         bttBtn_next_Internalname = "BTN_NEXT";
         bttBtn_last_Internalname = "BTN_LAST";
         bttBtn_select_Internalname = "BTN_SELECT";
         divToolbarcell_Internalname = "TOOLBARCELL";
         edtUsuarioid_Internalname = "USUARIOID";
         edtusuario_Internalname = "USUARIO";
         edtpassword_Internalname = "PASSWORD";
         edtactivo_Internalname = "ACTIVO";
         edtPaisid_Internalname = "PAISID";
         edtPuestoid_Internalname = "PUESTOID";
         edtUsuariocorreo_Internalname = "USUARIOCORREO";
         divFormcontainer_Internalname = "FORMCONTAINER";
         bttBtn_enter_Internalname = "BTN_ENTER";
         bttBtn_cancel_Internalname = "BTN_CANCEL";
         bttBtn_delete_Internalname = "BTN_DELETE";
         divMaintable_Internalname = "MAINTABLE";
         Form.Internalname = "FORM";
         imgprompt_15_Internalname = "PROMPT_15";
         imgprompt_20_Internalname = "PROMPT_20";
      }

      public override void initialize_properties( )
      {
         context.SetDefaultTheme("Carmine");
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         init_default_properties( ) ;
         Form.Headerrawhtml = "";
         Form.Background = "";
         Form.Textcolor = 0;
         Form.Backcolor = (int)(0xFFFFFF);
         Form.Caption = "Usuario";
         bttBtn_delete_Enabled = 0;
         bttBtn_delete_Visible = 1;
         bttBtn_cancel_Visible = 1;
         bttBtn_enter_Enabled = 1;
         bttBtn_enter_Visible = 1;
         edtUsuariocorreo_Jsonclick = "";
         edtUsuariocorreo_Enabled = 1;
         imgprompt_20_Visible = 1;
         imgprompt_20_Link = "";
         edtPuestoid_Jsonclick = "";
         edtPuestoid_Enabled = 1;
         imgprompt_15_Visible = 1;
         imgprompt_15_Link = "";
         edtPaisid_Jsonclick = "";
         edtPaisid_Enabled = 1;
         edtactivo_Jsonclick = "";
         edtactivo_Enabled = 1;
         edtpassword_Jsonclick = "";
         edtpassword_Enabled = 1;
         edtusuario_Jsonclick = "";
         edtusuario_Enabled = 1;
         edtUsuarioid_Jsonclick = "";
         edtUsuarioid_Enabled = 0;
         bttBtn_select_Visible = 1;
         bttBtn_last_Visible = 1;
         bttBtn_next_Visible = 1;
         bttBtn_previous_Visible = 1;
         bttBtn_first_Visible = 1;
         context.GX_msglist.DisplayMode = 1;
         if ( context.isSpaRequest( ) )
         {
            enableJsOutput();
         }
      }

      protected void dynload_actions( )
      {
         /* End function dynload_actions */
      }

      protected void init_web_controls( )
      {
         /* End function init_web_controls */
      }

      public void Valid_Paisid( int GX_Parm1 )
      {
         A15Paisid = GX_Parm1;
         /* Using cursor T000C16 */
         pr_datastore1.execute(14, new Object[] {A15Paisid});
         if ( (pr_datastore1.getStatus(14) == 101) )
         {
            GX_msglist.addItem("No matching 'Pais'.", "ForeignKeyNotFound", 1, "PAISID");
            AnyError = 1;
            GX_FocusControl = edtPaisid_Internalname;
         }
         pr_datastore1.close(14);
         dynload_actions( ) ;
         isValidOutput.Add(context.GX_msglist.ToJavascriptSource());
         isValidOutput.Add(context.httpAjaxContext.ajax_rsp_get_hiddens( ));
         context.GX_webresponse.AddString(isValidOutput.ToJSonString());
         wbTemp = context.ResponseContentType( "application/json");
      }

      public void Valid_Puestoid( int GX_Parm1 )
      {
         A20Puestoid = GX_Parm1;
         /* Using cursor T000C17 */
         pr_datastore1.execute(15, new Object[] {A20Puestoid});
         if ( (pr_datastore1.getStatus(15) == 101) )
         {
            GX_msglist.addItem("No matching 'Puesto'.", "ForeignKeyNotFound", 1, "PUESTOID");
            AnyError = 1;
            GX_FocusControl = edtPuestoid_Internalname;
         }
         pr_datastore1.close(15);
         dynload_actions( ) ;
         isValidOutput.Add(context.GX_msglist.ToJavascriptSource());
         isValidOutput.Add(context.httpAjaxContext.ajax_rsp_get_hiddens( ));
         context.GX_webresponse.AddString(isValidOutput.ToJSonString());
         wbTemp = context.ResponseContentType( "application/json");
      }

      public override bool SupportAjaxEvent( )
      {
         return true ;
      }

      public override void InitializeDynEvents( )
      {
         setEventMetadata("ENTER","{handler:'UserMainFullajax',iparms:[{postForm:true},{av:'Gx_mode',fld:'vMODE',pic:'@!',hsh:true},{av:'AV7Usuarioid',fld:'vUSUARIOID',pic:'ZZZZZZZZ9',hsh:true}]");
         setEventMetadata("ENTER",",oparms:[]}");
         setEventMetadata("REFRESH","{handler:'Refresh',iparms:[{av:'Gx_mode',fld:'vMODE',pic:'@!',hsh:true},{av:'AV7Usuarioid',fld:'vUSUARIOID',pic:'ZZZZZZZZ9',hsh:true},{av:'A17Usuarioid',fld:'USUARIOID',pic:'ZZZZZZZZ9'},{av:'AV11Insert_Paisid',fld:'vINSERT_PAISID',pic:'ZZZZZZZZ9'},{av:'AV12Insert_Puestoid',fld:'vINSERT_PUESTOID',pic:'ZZZZZZZZ9'}]");
         setEventMetadata("REFRESH",",oparms:[]}");
         setEventMetadata("AFTER TRN","{handler:'E120C2',iparms:[{av:'Gx_mode',fld:'vMODE',pic:'@!',hsh:true},{av:'AV9TrnContext',fld:'vTRNCONTEXT',pic:''}]");
         setEventMetadata("AFTER TRN",",oparms:[]}");
         return  ;
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
         pr_datastore1.close(1);
         pr_datastore1.close(14);
         pr_datastore1.close(15);
      }

      public override void initialize( )
      {
         sPrefix = "";
         wcpOGx_mode = "";
         Z120usuario = "";
         Z121password = "";
         Z123Usuariocorreo = "";
         scmdbuf = "";
         gxfirstwebparm = "";
         gxfirstwebparm_bkp = "";
         GXKey = "";
         PreviousTooltip = "";
         PreviousCaption = "";
         Form = new GXWebForm();
         GX_FocusControl = "";
         lblTitle_Jsonclick = "";
         ClassString = "";
         StyleString = "";
         TempTags = "";
         bttBtn_first_Jsonclick = "";
         bttBtn_previous_Jsonclick = "";
         bttBtn_next_Jsonclick = "";
         bttBtn_last_Jsonclick = "";
         bttBtn_select_Jsonclick = "";
         A120usuario = "";
         A121password = "";
         sImgUrl = "";
         A123Usuariocorreo = "";
         bttBtn_enter_Jsonclick = "";
         bttBtn_cancel_Jsonclick = "";
         bttBtn_delete_Jsonclick = "";
         AV14Pgmname = "";
         forbiddenHiddens = "";
         hsh = "";
         sMode12 = "";
         sEvt = "";
         EvtGridId = "";
         EvtRowId = "";
         sEvtType = "";
         AV9TrnContext = new SdtTransactionContext(context);
         AV10WebSession = context.GetSession();
         AV13TrnContextAtt = new SdtTransactionContext_Attribute(context);
         T000C6_A17Usuarioid = new int[1] ;
         T000C6_A120usuario = new String[] {""} ;
         T000C6_A121password = new String[] {""} ;
         T000C6_A122activo = new short[1] ;
         T000C6_A123Usuariocorreo = new String[] {""} ;
         T000C6_n123Usuariocorreo = new bool[] {false} ;
         T000C6_A15Paisid = new int[1] ;
         T000C6_A20Puestoid = new int[1] ;
         T000C4_A15Paisid = new int[1] ;
         T000C5_A20Puestoid = new int[1] ;
         T000C7_A15Paisid = new int[1] ;
         T000C8_A20Puestoid = new int[1] ;
         T000C9_A17Usuarioid = new int[1] ;
         T000C3_A17Usuarioid = new int[1] ;
         T000C3_A120usuario = new String[] {""} ;
         T000C3_A121password = new String[] {""} ;
         T000C3_A122activo = new short[1] ;
         T000C3_A123Usuariocorreo = new String[] {""} ;
         T000C3_n123Usuariocorreo = new bool[] {false} ;
         T000C3_A15Paisid = new int[1] ;
         T000C3_A20Puestoid = new int[1] ;
         T000C10_A17Usuarioid = new int[1] ;
         T000C11_A17Usuarioid = new int[1] ;
         T000C2_A17Usuarioid = new int[1] ;
         T000C2_A120usuario = new String[] {""} ;
         T000C2_A121password = new String[] {""} ;
         T000C2_A122activo = new short[1] ;
         T000C2_A123Usuariocorreo = new String[] {""} ;
         T000C2_n123Usuariocorreo = new bool[] {false} ;
         T000C2_A15Paisid = new int[1] ;
         T000C2_A20Puestoid = new int[1] ;
         T000C12_A17Usuarioid = new int[1] ;
         T000C15_A17Usuarioid = new int[1] ;
         sDynURL = "";
         FormProcess = "";
         bodyStyle = "";
         T000C16_A15Paisid = new int[1] ;
         isValidOutput = new GxUnknownObjectCollection();
         T000C17_A20Puestoid = new int[1] ;
         pr_datastore1 = new DataStoreProvider(context, new GeneXus.Programs.usuario__datastore1(),
            new Object[][] {
                new Object[] {
               T000C2_A17Usuarioid, T000C2_A120usuario, T000C2_A121password, T000C2_A122activo, T000C2_A123Usuariocorreo, T000C2_n123Usuariocorreo, T000C2_A15Paisid, T000C2_A20Puestoid
               }
               , new Object[] {
               T000C3_A17Usuarioid, T000C3_A120usuario, T000C3_A121password, T000C3_A122activo, T000C3_A123Usuariocorreo, T000C3_n123Usuariocorreo, T000C3_A15Paisid, T000C3_A20Puestoid
               }
               , new Object[] {
               T000C4_A15Paisid
               }
               , new Object[] {
               T000C5_A20Puestoid
               }
               , new Object[] {
               T000C6_A17Usuarioid, T000C6_A120usuario, T000C6_A121password, T000C6_A122activo, T000C6_A123Usuariocorreo, T000C6_n123Usuariocorreo, T000C6_A15Paisid, T000C6_A20Puestoid
               }
               , new Object[] {
               T000C7_A15Paisid
               }
               , new Object[] {
               T000C8_A20Puestoid
               }
               , new Object[] {
               T000C9_A17Usuarioid
               }
               , new Object[] {
               T000C10_A17Usuarioid
               }
               , new Object[] {
               T000C11_A17Usuarioid
               }
               , new Object[] {
               T000C12_A17Usuarioid
               }
               , new Object[] {
               }
               , new Object[] {
               }
               , new Object[] {
               T000C15_A17Usuarioid
               }
               , new Object[] {
               T000C16_A15Paisid
               }
               , new Object[] {
               T000C17_A20Puestoid
               }
            }
         );
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.usuario__default(),
            new Object[][] {
            }
         );
         AV14Pgmname = "Usuario";
      }

      private short Z122activo ;
      private short GxWebError ;
      private short gxcookieaux ;
      private short IsConfirmed ;
      private short IsModified ;
      private short AnyError ;
      private short nKeyPressed ;
      private short initialized ;
      private short A122activo ;
      private short RcdFound12 ;
      private short GX_JID ;
      private short Gx_BScreen ;
      private short gxajaxcallmode ;
      private short wbTemp ;
      private int wcpOAV7Usuarioid ;
      private int Z17Usuarioid ;
      private int Z15Paisid ;
      private int Z20Puestoid ;
      private int N15Paisid ;
      private int N20Puestoid ;
      private int A15Paisid ;
      private int A20Puestoid ;
      private int AV7Usuarioid ;
      private int trnEnded ;
      private int bttBtn_first_Visible ;
      private int bttBtn_previous_Visible ;
      private int bttBtn_next_Visible ;
      private int bttBtn_last_Visible ;
      private int bttBtn_select_Visible ;
      private int A17Usuarioid ;
      private int edtUsuarioid_Enabled ;
      private int edtusuario_Enabled ;
      private int edtpassword_Enabled ;
      private int edtactivo_Enabled ;
      private int edtPaisid_Enabled ;
      private int imgprompt_15_Visible ;
      private int edtPuestoid_Enabled ;
      private int imgprompt_20_Visible ;
      private int edtUsuariocorreo_Enabled ;
      private int bttBtn_enter_Visible ;
      private int bttBtn_enter_Enabled ;
      private int bttBtn_cancel_Visible ;
      private int bttBtn_delete_Visible ;
      private int bttBtn_delete_Enabled ;
      private int AV11Insert_Paisid ;
      private int AV12Insert_Puestoid ;
      private int AV15GXV1 ;
      private int idxLst ;
      private String sPrefix ;
      private String wcpOGx_mode ;
      private String scmdbuf ;
      private String gxfirstwebparm ;
      private String gxfirstwebparm_bkp ;
      private String Gx_mode ;
      private String GXKey ;
      private String PreviousTooltip ;
      private String PreviousCaption ;
      private String GX_FocusControl ;
      private String edtusuario_Internalname ;
      private String divMaintable_Internalname ;
      private String divTitlecontainer_Internalname ;
      private String lblTitle_Internalname ;
      private String lblTitle_Jsonclick ;
      private String ClassString ;
      private String StyleString ;
      private String divFormcontainer_Internalname ;
      private String divToolbarcell_Internalname ;
      private String TempTags ;
      private String bttBtn_first_Internalname ;
      private String bttBtn_first_Jsonclick ;
      private String bttBtn_previous_Internalname ;
      private String bttBtn_previous_Jsonclick ;
      private String bttBtn_next_Internalname ;
      private String bttBtn_next_Jsonclick ;
      private String bttBtn_last_Internalname ;
      private String bttBtn_last_Jsonclick ;
      private String bttBtn_select_Internalname ;
      private String bttBtn_select_Jsonclick ;
      private String edtUsuarioid_Internalname ;
      private String edtUsuarioid_Jsonclick ;
      private String edtusuario_Jsonclick ;
      private String edtpassword_Internalname ;
      private String edtpassword_Jsonclick ;
      private String edtactivo_Internalname ;
      private String edtactivo_Jsonclick ;
      private String edtPaisid_Internalname ;
      private String edtPaisid_Jsonclick ;
      private String sImgUrl ;
      private String imgprompt_15_Internalname ;
      private String imgprompt_15_Link ;
      private String edtPuestoid_Internalname ;
      private String edtPuestoid_Jsonclick ;
      private String imgprompt_20_Internalname ;
      private String imgprompt_20_Link ;
      private String edtUsuariocorreo_Internalname ;
      private String edtUsuariocorreo_Jsonclick ;
      private String bttBtn_enter_Internalname ;
      private String bttBtn_enter_Jsonclick ;
      private String bttBtn_cancel_Internalname ;
      private String bttBtn_cancel_Jsonclick ;
      private String bttBtn_delete_Internalname ;
      private String bttBtn_delete_Jsonclick ;
      private String AV14Pgmname ;
      private String forbiddenHiddens ;
      private String hsh ;
      private String sMode12 ;
      private String sEvt ;
      private String EvtGridId ;
      private String EvtRowId ;
      private String sEvtType ;
      private String sDynURL ;
      private String FormProcess ;
      private String bodyStyle ;
      private bool entryPointCalled ;
      private bool toggleJsOutput ;
      private bool wbErr ;
      private bool n123Usuariocorreo ;
      private bool returnInSub ;
      private bool Gx_longc ;
      private String Z120usuario ;
      private String Z121password ;
      private String Z123Usuariocorreo ;
      private String A120usuario ;
      private String A121password ;
      private String A123Usuariocorreo ;
      private IGxSession AV10WebSession ;
      private GxUnknownObjectCollection isValidOutput ;
      private IGxDataStore dsDataStore1 ;
      private IGxDataStore dsDefault ;
      private IDataStoreProvider pr_datastore1 ;
      private int[] T000C6_A17Usuarioid ;
      private String[] T000C6_A120usuario ;
      private String[] T000C6_A121password ;
      private short[] T000C6_A122activo ;
      private String[] T000C6_A123Usuariocorreo ;
      private bool[] T000C6_n123Usuariocorreo ;
      private int[] T000C6_A15Paisid ;
      private int[] T000C6_A20Puestoid ;
      private int[] T000C4_A15Paisid ;
      private int[] T000C5_A20Puestoid ;
      private int[] T000C7_A15Paisid ;
      private int[] T000C8_A20Puestoid ;
      private int[] T000C9_A17Usuarioid ;
      private int[] T000C3_A17Usuarioid ;
      private String[] T000C3_A120usuario ;
      private String[] T000C3_A121password ;
      private short[] T000C3_A122activo ;
      private String[] T000C3_A123Usuariocorreo ;
      private bool[] T000C3_n123Usuariocorreo ;
      private int[] T000C3_A15Paisid ;
      private int[] T000C3_A20Puestoid ;
      private int[] T000C10_A17Usuarioid ;
      private int[] T000C11_A17Usuarioid ;
      private int[] T000C2_A17Usuarioid ;
      private String[] T000C2_A120usuario ;
      private String[] T000C2_A121password ;
      private short[] T000C2_A122activo ;
      private String[] T000C2_A123Usuariocorreo ;
      private bool[] T000C2_n123Usuariocorreo ;
      private int[] T000C2_A15Paisid ;
      private int[] T000C2_A20Puestoid ;
      private int[] T000C12_A17Usuarioid ;
      private IDataStoreProvider pr_default ;
      private int[] T000C15_A17Usuarioid ;
      private int[] T000C16_A15Paisid ;
      private int[] T000C17_A20Puestoid ;
      private GXWebForm Form ;
      private SdtTransactionContext AV9TrnContext ;
      private SdtTransactionContext_Attribute AV13TrnContextAtt ;
   }

   public class usuario__datastore1 : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
         ,new ForEachCursor(def[2])
         ,new ForEachCursor(def[3])
         ,new ForEachCursor(def[4])
         ,new ForEachCursor(def[5])
         ,new ForEachCursor(def[6])
         ,new ForEachCursor(def[7])
         ,new ForEachCursor(def[8])
         ,new ForEachCursor(def[9])
         ,new ForEachCursor(def[10])
         ,new UpdateCursor(def[11])
         ,new UpdateCursor(def[12])
         ,new ForEachCursor(def[13])
         ,new ForEachCursor(def[14])
         ,new ForEachCursor(def[15])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmT000C6 ;
          prmT000C6 = new Object[] {
          new Object[] {"@Usuarioid",SqlDbType.Int,9,0}
          } ;
          Object[] prmT000C4 ;
          prmT000C4 = new Object[] {
          new Object[] {"@Paisid",SqlDbType.Int,9,0}
          } ;
          Object[] prmT000C5 ;
          prmT000C5 = new Object[] {
          new Object[] {"@Puestoid",SqlDbType.Int,9,0}
          } ;
          Object[] prmT000C7 ;
          prmT000C7 = new Object[] {
          new Object[] {"@Paisid",SqlDbType.Int,9,0}
          } ;
          Object[] prmT000C8 ;
          prmT000C8 = new Object[] {
          new Object[] {"@Puestoid",SqlDbType.Int,9,0}
          } ;
          Object[] prmT000C9 ;
          prmT000C9 = new Object[] {
          new Object[] {"@Usuarioid",SqlDbType.Int,9,0}
          } ;
          Object[] prmT000C3 ;
          prmT000C3 = new Object[] {
          new Object[] {"@Usuarioid",SqlDbType.Int,9,0}
          } ;
          Object[] prmT000C10 ;
          prmT000C10 = new Object[] {
          new Object[] {"@Usuarioid",SqlDbType.Int,9,0}
          } ;
          Object[] prmT000C11 ;
          prmT000C11 = new Object[] {
          new Object[] {"@Usuarioid",SqlDbType.Int,9,0}
          } ;
          Object[] prmT000C2 ;
          prmT000C2 = new Object[] {
          new Object[] {"@Usuarioid",SqlDbType.Int,9,0}
          } ;
          Object[] prmT000C12 ;
          prmT000C12 = new Object[] {
          new Object[] {"@usuario",SqlDbType.VarChar,50,0} ,
          new Object[] {"@password",SqlDbType.VarChar,50,0} ,
          new Object[] {"@activo",SqlDbType.SmallInt,1,0} ,
          new Object[] {"@Usuariocorreo",SqlDbType.VarChar,100,0} ,
          new Object[] {"@Paisid",SqlDbType.Int,9,0} ,
          new Object[] {"@Puestoid",SqlDbType.Int,9,0}
          } ;
          Object[] prmT000C13 ;
          prmT000C13 = new Object[] {
          new Object[] {"@usuario",SqlDbType.VarChar,50,0} ,
          new Object[] {"@password",SqlDbType.VarChar,50,0} ,
          new Object[] {"@activo",SqlDbType.SmallInt,1,0} ,
          new Object[] {"@Usuariocorreo",SqlDbType.VarChar,100,0} ,
          new Object[] {"@Paisid",SqlDbType.Int,9,0} ,
          new Object[] {"@Puestoid",SqlDbType.Int,9,0} ,
          new Object[] {"@Usuarioid",SqlDbType.Int,9,0}
          } ;
          Object[] prmT000C14 ;
          prmT000C14 = new Object[] {
          new Object[] {"@Usuarioid",SqlDbType.Int,9,0}
          } ;
          Object[] prmT000C15 ;
          prmT000C15 = new Object[] {
          } ;
          Object[] prmT000C16 ;
          prmT000C16 = new Object[] {
          new Object[] {"@Paisid",SqlDbType.Int,9,0}
          } ;
          Object[] prmT000C17 ;
          prmT000C17 = new Object[] {
          new Object[] {"@Puestoid",SqlDbType.Int,9,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("T000C2", "SELECT [id] AS Usuarioid, [usuario], [password], [activo], [correo], [idPais] AS Paisid, [idPuesto] AS Puestoid FROM dbo.[Usuario] WITH (UPDLOCK) WHERE [id] = @Usuarioid ",true, GxErrorMask.GX_NOMASK, false, this,prmT000C2,1,0,true,false )
             ,new CursorDef("T000C3", "SELECT [id] AS Usuarioid, [usuario], [password], [activo], [correo], [idPais] AS Paisid, [idPuesto] AS Puestoid FROM dbo.[Usuario] WITH (NOLOCK) WHERE [id] = @Usuarioid ",true, GxErrorMask.GX_NOMASK, false, this,prmT000C3,1,0,true,false )
             ,new CursorDef("T000C4", "SELECT [id] AS Paisid FROM dbo.[Pais] WITH (NOLOCK) WHERE [id] = @Paisid ",true, GxErrorMask.GX_NOMASK, false, this,prmT000C4,1,0,true,false )
             ,new CursorDef("T000C5", "SELECT [id] AS Puestoid FROM dbo.[Puesto] WITH (NOLOCK) WHERE [id] = @Puestoid ",true, GxErrorMask.GX_NOMASK, false, this,prmT000C5,1,0,true,false )
             ,new CursorDef("T000C6", "SELECT TM1.[id] AS Usuarioid, TM1.[usuario], TM1.[password], TM1.[activo], TM1.[correo], TM1.[idPais] AS Paisid, TM1.[idPuesto] AS Puestoid FROM dbo.[Usuario] TM1 WITH (NOLOCK) WHERE TM1.[id] = @Usuarioid ORDER BY TM1.[id]  OPTION (FAST 100)",true, GxErrorMask.GX_NOMASK, false, this,prmT000C6,100,0,true,false )
             ,new CursorDef("T000C7", "SELECT [id] AS Paisid FROM dbo.[Pais] WITH (NOLOCK) WHERE [id] = @Paisid ",true, GxErrorMask.GX_NOMASK, false, this,prmT000C7,1,0,true,false )
             ,new CursorDef("T000C8", "SELECT [id] AS Puestoid FROM dbo.[Puesto] WITH (NOLOCK) WHERE [id] = @Puestoid ",true, GxErrorMask.GX_NOMASK, false, this,prmT000C8,1,0,true,false )
             ,new CursorDef("T000C9", "SELECT [id] AS Usuarioid FROM dbo.[Usuario] WITH (NOLOCK) WHERE [id] = @Usuarioid  OPTION (FAST 1)",true, GxErrorMask.GX_NOMASK, false, this,prmT000C9,1,0,true,false )
             ,new CursorDef("T000C10", "SELECT TOP 1 [id] AS Usuarioid FROM dbo.[Usuario] WITH (NOLOCK) WHERE ( [id] > @Usuarioid) ORDER BY [id]  OPTION (FAST 1)",true, GxErrorMask.GX_NOMASK, false, this,prmT000C10,1,0,true,true )
             ,new CursorDef("T000C11", "SELECT TOP 1 [id] AS Usuarioid FROM dbo.[Usuario] WITH (NOLOCK) WHERE ( [id] < @Usuarioid) ORDER BY [id] DESC  OPTION (FAST 1)",true, GxErrorMask.GX_NOMASK, false, this,prmT000C11,1,0,true,true )
             ,new CursorDef("T000C12", "INSERT INTO dbo.[Usuario]([usuario], [password], [activo], [correo], [idPais], [idPuesto]) VALUES(@usuario, @password, @activo, @Usuariocorreo, @Paisid, @Puestoid); SELECT SCOPE_IDENTITY()", GxErrorMask.GX_NOMASK,prmT000C12)
             ,new CursorDef("T000C13", "UPDATE dbo.[Usuario] SET [usuario]=@usuario, [password]=@password, [activo]=@activo, [correo]=@Usuariocorreo, [idPais]=@Paisid, [idPuesto]=@Puestoid  WHERE [id] = @Usuarioid", GxErrorMask.GX_NOMASK,prmT000C13)
             ,new CursorDef("T000C14", "DELETE FROM dbo.[Usuario]  WHERE [id] = @Usuarioid", GxErrorMask.GX_NOMASK,prmT000C14)
             ,new CursorDef("T000C15", "SELECT [id] AS Usuarioid FROM dbo.[Usuario] WITH (NOLOCK) ORDER BY [id]  OPTION (FAST 100)",true, GxErrorMask.GX_NOMASK, false, this,prmT000C15,100,0,true,false )
             ,new CursorDef("T000C16", "SELECT [id] AS Paisid FROM dbo.[Pais] WITH (NOLOCK) WHERE [id] = @Paisid ",true, GxErrorMask.GX_NOMASK, false, this,prmT000C16,1,0,true,false )
             ,new CursorDef("T000C17", "SELECT [id] AS Puestoid FROM dbo.[Puesto] WITH (NOLOCK) WHERE [id] = @Puestoid ",true, GxErrorMask.GX_NOMASK, false, this,prmT000C17,1,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getVarchar(2) ;
                ((String[]) buf[2])[0] = rslt.getVarchar(3) ;
                ((short[]) buf[3])[0] = rslt.getShort(4) ;
                ((String[]) buf[4])[0] = rslt.getVarchar(5) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(5);
                ((int[]) buf[6])[0] = rslt.getInt(6) ;
                ((int[]) buf[7])[0] = rslt.getInt(7) ;
                return;
             case 1 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getVarchar(2) ;
                ((String[]) buf[2])[0] = rslt.getVarchar(3) ;
                ((short[]) buf[3])[0] = rslt.getShort(4) ;
                ((String[]) buf[4])[0] = rslt.getVarchar(5) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(5);
                ((int[]) buf[6])[0] = rslt.getInt(6) ;
                ((int[]) buf[7])[0] = rslt.getInt(7) ;
                return;
             case 2 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 3 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 4 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getVarchar(2) ;
                ((String[]) buf[2])[0] = rslt.getVarchar(3) ;
                ((short[]) buf[3])[0] = rslt.getShort(4) ;
                ((String[]) buf[4])[0] = rslt.getVarchar(5) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(5);
                ((int[]) buf[6])[0] = rslt.getInt(6) ;
                ((int[]) buf[7])[0] = rslt.getInt(7) ;
                return;
             case 5 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 6 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 7 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 8 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 9 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 10 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 13 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 14 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 15 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 1 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 2 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 3 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 4 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 5 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 6 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 7 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 8 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 9 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 10 :
                stmt.SetParameter(1, (String)parms[0]);
                stmt.SetParameter(2, (String)parms[1]);
                stmt.SetParameter(3, (short)parms[2]);
                if ( (bool)parms[3] )
                {
                   stmt.setNull( 4 , SqlDbType.VarChar );
                }
                else
                {
                   stmt.SetParameter(4, (String)parms[4]);
                }
                stmt.SetParameter(5, (int)parms[5]);
                stmt.SetParameter(6, (int)parms[6]);
                return;
             case 11 :
                stmt.SetParameter(1, (String)parms[0]);
                stmt.SetParameter(2, (String)parms[1]);
                stmt.SetParameter(3, (short)parms[2]);
                if ( (bool)parms[3] )
                {
                   stmt.setNull( 4 , SqlDbType.VarChar );
                }
                else
                {
                   stmt.SetParameter(4, (String)parms[4]);
                }
                stmt.SetParameter(5, (int)parms[5]);
                stmt.SetParameter(6, (int)parms[6]);
                stmt.SetParameter(7, (int)parms[7]);
                return;
             case 12 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 14 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 15 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
       }
    }

    public String getDataStoreName( )
    {
       return "DATASTORE1";
    }

 }

 public class usuario__default : DataStoreHelperBase, IDataStoreHelper
 {
    public ICursor[] getCursors( )
    {
       cursorDefinitions();
       return new Cursor[] {
     };
  }

  private static CursorDef[] def;
  private void cursorDefinitions( )
  {
     if ( def == null )
     {
        def= new CursorDef[] {
        };
     }
  }

  public void getResults( int cursor ,
                          IFieldGetter rslt ,
                          Object[] buf )
  {
     switch ( cursor )
     {
     }
  }

  public void setParameters( int cursor ,
                             IFieldSetter stmt ,
                             Object[] parms )
  {
     switch ( cursor )
     {
     }
  }

}

}
